function createTestPanelTypeTable(test_panel_types, table) {
  if (!table) {
    const hotElement = document.querySelector('#test-panel-type-table');
    const hotSettings = {
      data: test_panel_types,
      columns: [
        {
          data: 'name',
          editor: false,
          renderer: 'html',
          width: 350,
        },
        {
          data: 'alias',
          editor: false,
          renderer: 'html',
          width: 250,
        },
        {
          data: 'category',
          editor: false,
          type: 'text',
          width: 250,
        },
        {
          data: 'num_test_types',
          editor: false,
        },
        {
          data: 'in_house_lab_locations',
          editor: false,
          type: 'text',
          width: 250,
        },
        {
          data: 'lab_departments',
          editor: false,
          type: 'text',
          width: 250,
        },
        {
          data: 'price',
          type: 'numeric',
          numericFormat: {
            pattern: '$0,0.00',
            culture: 'en-US' // this is the default culture, set up for USD
          },
          allowEmpty: false
        },
      ],
      stretchH: 'all',
      //width: 806,
      height: 700,
      autoColumnSize: true,
      autoWrapRow: true,
      manualRowResize: false,
      manualColumnResize: false,
      columnSorting: true,
      sortIndicator: true,
      rowHeaders: false,
      colHeaders: true,
      nestedHeaders: [
        [
          'Name',
          'Alias',
          'Category',
          'Test Count',
          'In-House Lab Locations',
          'Lab Department/Section',
          'Price (Self-Pay)',
        ],
      ],
      manualRowMove: false,
      manualColumnMove: false,
      contextMenu: false,
      filters: true,
      dropdownMenu: ['filter_by_condition', 'filter_action_bar'],
      afterGetColHeader: function(col, TH) {
        TH.className = 'center';
      },
    };

    return new Handsontable(hotElement, hotSettings);
  } else {
    // load new data without replacing old table
    table.getPlugin('filters').clearConditions();
    table.loadData(accounts);
    return table;
  }
}
