from django.contrib import admin
from rest_framework.authtoken.admin import TokenAdmin
from . import models

# Register your models here.

admin.site.register(models.TestMethod)
admin.site.register(models.TestTarget)
admin.site.register(models.CLIASampleType)
admin.site.register(models.OffsiteLaboratory)
admin.site.register(models.Instrument)
admin.site.register(models.Payer)


TokenAdmin.raw_id_fields = ('user',)
