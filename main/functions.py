import datetime
import pytz
from django.core.exceptions import ValidationError

import clients.models as clients_models


def convert_timezone_to_utc(timezone, datetime_obj):
    try:
        if datetime_obj is None:
            return None

        # if datetime.date (instead of datetime.datetime), then convert to datetime (midnight)
        if datetime_obj.__class__.__name__ == 'date':
            datetime_obj = datetime.datetime.combine(datetime_obj, datetime.datetime.min.time())

        # must convert from tenant's timezone to UTC
        localized_datetime = timezone.localize(datetime_obj)
        utc_datetime = localized_datetime.astimezone(pytz.utc)

        return utc_datetime
    except AttributeError as e:
        print(e)
        raise Exception


def convert_tenant_timezone_to_utc(request, datetime_obj):
    """
    For use in view functions

    1. Check if datetime_obj exists (is not None) and that datetime_obj is actually a datetime object -
    if not, then convert to datetime (midnight that day)

    2. Convert datetime of tenant-specific timezone to UTC

    3. If there's an error, raise Exception

    Date ranges are time agnostic, meaning 12:00AM for both the start range and the end range
    Date ranges are also inclusive, because that's how people think.
    We'll ignore the convention and counterargument:
    https://stackoverflow.com/questions/9795391/is-there-a-standard-for-inclusive-exclusive-ends-of-time-intervals
    """
    try:

        tenant_timezone = request.tenant.timezone

        if datetime_obj is None:
            return None

        # if datetime.date (instead of datetime.datetime), then convert to datetime (midnight)
        if datetime_obj.__class__.__name__ == 'date':
            datetime_obj = datetime.datetime.combine(datetime_obj, datetime.datetime.min.time())

        # must convert from tenant's timezone to UTC
        localized_datetime = tenant_timezone.localize(datetime_obj)
        utc_datetime = localized_datetime.astimezone(pytz.utc)

        return utc_datetime
    except AttributeError as e:
        print(e)
        raise Exception


def convert_utc_to_timezone(timezone, datetime_obj):
    try:
        if datetime_obj is None:
            return None

        # if datetime.date (instead of datetime.datetime), then convert to datetime (midnight)
        if datetime_obj.__class__.__name__ == 'date':
            datetime_obj = datetime.datetime.combine(datetime_obj, datetime.datetime.min.time())

        # must convert from UTC to tenant's timezone
        tenant_datetime = datetime_obj.astimezone(timezone)
        return tenant_datetime
    except AttributeError as e:
        print(e)
        raise Exception


def convert_utc_to_tenant_timezone(request, datetime_obj):
    """
    The opposite of the above function
    """
    try:
        tenant_timezone = request.tenant.timezone

        if datetime_obj is None:
            return None

        # if datetime.date (instead of datetime.datetime), then convert to datetime (midnight)
        if datetime_obj.__class__.__name__ == 'date':
            datetime_obj = datetime.datetime.combine(datetime_obj, datetime.datetime.min.time())

        # must convert from UTC to tenant's timezone
        tenant_datetime = datetime_obj.astimezone(tenant_timezone)
        return tenant_datetime
    except AttributeError as e:
        print(e)
        raise Exception


def convert_utc_to_tenant_timezone_with_workers(tenant_id, datetime_obj):
    """
    Same as above function, just uses workers
    """
    try:
        tenant = clients_models.Client.objects.get(id=tenant_id)
        tenant_timezone = tenant.timezone

        if datetime_obj is None:
            return None

        # if datetime.date (instead of datetime.datetime), then convert to datetime (midnight)
        if datetime_obj.__class__.__name__ == 'date':
            datetime_obj = datetime.datetime.combine(datetime_obj, datetime.datetime.min.time())

        # must convert from UTC to tenant's timezone
        tenant_datetime = datetime_obj.astimezone(tenant_timezone)
        return tenant_datetime.strftime("%m-%d-%Y, %I:%M%p (%Z)")
    except AttributeError as e:
        print(e)
        raise Exception


def date_string_or_none(datetime_obj):
    """
    :param datetime:
    :return: String representation of date %m/%d/%Y if object exists, None if not
    """
    try:
        datetime_string = datetime_obj.strftime('%m/%d/%Y')
    except AttributeError:
        datetime_string = None
    return datetime_string


