from django.conf.urls import url
from django.urls import path

from . import views

urlpatterns = [
    path('', views.LandingPageView.as_view(), name='landing_page'),
    path('login/', views.LoginView.as_view(), name='login'),
    path('logout/', views.LogoutView.as_view(), name='logout'),
    path('settings/forgot-password/', views.ForgotPasswordView.as_view(), name='forgot_password'),
    path('404/', views.Test404View.as_view(), name='test_404'),
    path('500/', views.Test500View.as_view(), name='test_500'),
    path('dashboard/', views.DashboardView.as_view(), name='dashboard'),
    path('order_inbox/delete/', views.DeleteInboxView.as_view(), name='inbox_delete'),
    path('order_inbox/<str:order_code>/delete/', views.DeleteInboxOrderView.as_view(), name='inbox_order_delete'),
    path('settings/', views.SettingsView.as_view(), name='settings'),
    path('advanced_settings/', views.AdvancedSettingsView.as_view(), name='advanced_settings'),
    path('setup/', views.SetupView.as_view(), name='setup'),
    path('setup/laboratory_info/', views.LaboratoryUpdateView.as_view(), name='laboratory_update'),
    path('two_factor_auth', views.TwoFactorAuthView.as_view(), name='two_factor_auth'),
    path('two_factor_auth/setup', views.TwoFactorAuthSetupView.as_view(), name='two_factor_auth_setup'),

    # AWS debug
    path('aws_debug/', views.AWSDebugView.as_view(), name='aws_debug'),
    path('aws_debug/identifier/', views.AWSDebugIdentifierView.as_view(), name='aws_debug_identifiers'),

    # employees
    path('users/', views.UserListView.as_view(), name='user_list'),
    path('users/create/', views.UserCreateView.as_view(), name='user_create'),
    path('lab_admins/<str:lab_admin_uuid>/update/', views.LabAdminUpdateView.as_view(),
         name='lab_admin_update'),
    path('lab_admins/<str:lab_admin_uuid>/delete/', views.LabAdminDeleteView.as_view(),
         name='lab_admin_delete'),
    path('lab_collaborators/<str:lab_collaborator_uuid>/update/', views.LabCollaboratorUpdateView.as_view(),
         name='lab_collaborator_update'),
    path('lab_collaborators/<str:lab_collaborator_uuid>/delete/', views.LabCollaboratorDeleteView.as_view(),
         name='lab_collaborator_delete'),

    # providers
    path('provider/<str:provider_uuid>/signature/', views.ProviderSignatureUpdateView.as_view(), name='provider_signature'),
    path('provider/<str:provider_uuid>/delete_signature/', views.ProviderSignatureDeleteView.as_view(), name='provider_delete_signature'),

    # clia sample types
    path('clia_sample_types/<str:clia_sample_type_uuid>/', views.CLIASampleTypeDetailsView.as_view(),
         name='clia_sample_type_details'),

    # FDA Test List
    path('clia_test_types/', views.CLIATestTypeListView.as_view(), name='clia_test_type_list'),

    # billing
    path('billing/', views.BillingView.as_view(), name='billing'),

    # medication list
    path('medication_list/', views.MedicationListView.as_view(), name='medication_list'),

    # autocomplete views
    path('test_method_autocomplete/', views.TestMethodAutocomplete.as_view(), name='test_method_autocomplete'),
    path('test_target_autocomplete/', views.TestTargetAutocomplete.as_view(), name='test_target_autocomplete'),
    path('clia_test_type_autocomplete/', views.CliaTestTypeAutocomplete.as_view(), name='clia_test_type_autocomplete'),
    path('offsite_laboratory_autocomplete/', views.OffsiteLaboratoryAutocomplete.as_view(),
         name='offsite_laboratory_autocomplete'),
    path('instrument_autocomplete/', views.InstrumentAutocomplete.as_view(), name='instrument_autocomplete'),
    path('test_type_specialty_autocomplete/', views.TestTypeSpecialtyAutocomplete.as_view(),
         name='test_type_specialty_autocomplete'),
    path('payer_autocomplete/', views.PayerAutocomplete.as_view(),
         name='payer_autocomplete'),

    # initialize_data
    path('initialize_data/', views.InitializeDataView.as_view(), name='initialize_data'),

    # payer data
    path('payers/', views.PayerListView.as_view(), name='payer_list'),
    path('payers/create/', views.PayerCreateView.as_view(), name='payer_create'),
    path('payers/<str:payer_uuid>/', views.PayerDetailsView.as_view(), name='payer_details'),
    path('payers/<str:payer_uuid>/update/', views.PayerUpdateView.as_view(), name='payer_update'),
    path('payer/archived/', views.PayerArchivedListView.as_view(), name='payer_archived_list'),
    path('payers/<str:payer_uuid>/delete/', views.PayerDeleteView.as_view(), name='payer_delete'),
    path('payers/<str:payer_uuid>/undo_delete/', views.PayerUndoArchiveView.as_view(), name='payer_undo_delete'),

]
