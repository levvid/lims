from . import models
from rest_framework import serializers


class UserSerializer(serializers.HyperlinkedModelSerializer):
    """
    Serializer for the lab_tenant.models.User class
    """
    class Meta:
        model = models.User
        fields = ('first_name', 'last_name', 'email', 'user_type', 'uuid')


class CollectorUserSerializer(serializers.HyperlinkedModelSerializer):
    """
    Serializer used for creating collectors
    """
    first_name = serializers.CharField(required=True)
    last_name = serializers.CharField(required=True)
    username = serializers.CharField(required=False)
    email = serializers.EmailField(required=False, allow_blank=True, allow_null=True)

    class Meta:
        model = models.User
        fields = (
            # required
            'first_name',
            'last_name',
            # optional
            'email',
            'username',
            # read only
            'id'
        )


class PatientUserSerializer(serializers.HyperlinkedModelSerializer):
    """
    Serializer used for creating patients
    """
    first_name = serializers.CharField(required=True)
    last_name = serializers.CharField(required=True)
    email = serializers.EmailField(required=False, allow_blank=True, allow_null=True)

    class Meta:
        model = models.User
        fields = (
            'first_name',
            'last_name',
            'email',
        )

