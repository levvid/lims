import base64
import boto3
import django
import os
import tempfile
import uuid

from django.utils import timezone
from tenant_schemas.utils import tenant_context

os.environ['DJANGO_SETTINGS_MODULE'] = 'main.settings'
django.setup()

from . import models
from clients import models as clients_models

DATETIME_FORMAT = '%Y%m%d%H%M'
PROCESSING_ID_DICT = {'False': 'P',
                      'True': 'D'}
TRANSACTION_TYPE = 'CG'  # Charge
VERSION_ID = '2.5.1'
SOURCE_BUCKET = 'media/private/'
DESTINATION_BUCKET = 'Mirth/Integrations/'
AWS_ACCESS_KEY_ID = os.environ.get('S3_ACCESS_KEY')
AWS_SECRET_ACCESS_KEY = os.environ.get('S3_SECRET_KEY')

S3_BUCKET = 'dendi-lis'
RESULTS_OUT_DESTINATION = 'Mirth/Integrations/Results Outbound'

MSH = {
    'message_type': 'ORU^R01^ORU_RO1',
    'sending_application': "DENDI",
    'accept_acknowledgement': 'AL',
    'application_acknowledgement': 'AL',
    'message_profile_identifier': "LRI_NG_RN_Profile^^2.16.840.1.113883.9.20^ISO"
}
PID = {
    '': None,
}


def send_results_out_hl7(tenant_id, order_uuid):
    """
    Generate Results Out HL7 message and upload to S3 bucket
    :param tenant_id:
    :param order_uuid:
    :return:
    """
    tenant = clients_models.Client.objects.get(id=tenant_id)
    with tenant_context(tenant):
        order = models.Order.objects.get(uuid=order_uuid)

        # file saved as '<order.accession_number>.hl7'
        file_name = "{}/{}/{}.hl7".format(
            RESULTS_OUT_DESTINATION,
            tenant.name,
            order.accession_number
        )
        # generate hl7 message
        hl7_message = get_results_out_message(order, tenant)

        # upload to s3 folder
        with tempfile.NamedTemporaryFile() as temp:
            temp.write(hl7_message.encode())
            temp.seek(0)

            s3 = boto3.client(
                's3',
                aws_access_key_id=AWS_ACCESS_KEY_ID,
                aws_secret_access_key=AWS_SECRET_ACCESS_KEY,
            )
            s3.upload_file(temp.name, S3_BUCKET, file_name)


def get_results_out_message(order, tenant):
    message_control_id = uuid.uuid4()

    message_header = get_msh(tenant, message_control_id, order.account)
    patient_identification = get_pid(order)
    order_common = get_orc(order)

    message_contents = [message_header,
                        patient_identification,
                        order_common]

    observation_request_counter = 1
    test_panels = order.test_panels.all().prefetch_related('test_set__result', 'test_set__test_type',
                                                           'test_panel_type', 'test_set__sample',
                                                           'tests')
    for test_panel in test_panels:
        observation_request = get_obr(order, test_panel, observation_request_counter)
        message_contents.append(observation_request)
        observation_request_counter += 1

        observation_result_counter = 1
        tests = test_panel.tests.all().prefetch_related('result', 'test_type')
        for test in tests:
            observation_result = get_obx(order, test, observation_result_counter, tenant)
            message_contents.append(observation_result)
            observation_result_counter += 1

        # last test panel, add encoded pdf
        if observation_request_counter == len(test_panels) + 1 and order.account.results_out_include_base64_pdf:
            pdf_attachment = get_pdf_obx(order, tests.first(), observation_result_counter, tenant)
            message_contents.append(pdf_attachment)

    message = ''.join(message_contents)
    return message


def get_pdf_obx(order, test, set_id, tenant):
    observation_identifier = "PDF^{}.pdf".format(order.accession_number)

    # download and encode pdf to base 64
    report_pdf = order.reports.first().pdf_file.open()
    base64_encoded_pdf = base64.b64encode(report_pdf.read())
    observation_value = "^AP^^Base64^{}".format(base64_encoded_pdf)
    performing_organization_address = "{}^{}^{}^{}".format(
        tenant.address_1,
        tenant.address_2.split(',')[0],
        tenant.address_2.split(',')[1].strip().split(' ')[0],
        tenant.address_2.split(',')[1].strip().split(' ')[1],
    )

    text = "OBX|{}|ED|{}||{}||||||F|||{}|||||||||{}|{}|\r".format(
        set_id,
        observation_identifier,
        observation_value,
        test.sample.collection_date.strftime(DATETIME_FORMAT),
        tenant.name,
        performing_organization_address
    )

    return text


def get_obx(order, test, set_id, tenant):
    test_type = test.test_type
    test_method = test_type.test_method
    observation_identifier = "{}^{}".format(
        test_type.id,
        test_type.name
    )
    result = test.result.get()
    sample = test.sample
    observation_value = result.result_quantitative
    if not observation_value:
        observation_value = ''
    result_units_of_measurement = test.test_type.unit

    if test_type.result_type == 'Qualitative':
        result_unit_reference_range = ''
    else:
        result_quantitative = result.result_quantitative
        if test_method.is_within_range:
            try:
                test_range = test_type.test_type_ranges.get()
                low = test_range.range_low
                high = test_range.range_high
            except models.TestTypeRange.DoesNotExist:
                low = None
                high = None
            if low and high:
                result_unit_reference_range = '{} - {}'.format(low, high)
                if result_quantitative:
                    if result_quantitative < low:
                        abnormal_flags = 'L'
                    elif result_quantitative < high:
                        abnormal_flags = 'N'
                    elif result_quantitative > high:
                        abnormal_flags = 'H'
                    else:
                        abnormal_flags = 'A'
                else:
                    abnormal_flags = ''
            else:
                result_unit_reference_range = ''
                abnormal_flags = ''
        else:
            test_range = test_type.test_type_ranges.filter(default=True).get()
            if test_method.positive_above_cutoff:
                cutoff = test_range.range_low
                result_unit_reference_range = '> {}'.format(cutoff)
                if result_quantitative and cutoff:
                    if result_quantitative < cutoff:
                        abnormal_flags = 'N'
                    else:
                        abnormal_flags = 'H'
                else:
                    abnormal_flags = ''
            else:
                cutoff = test_range.range_high
                result_unit_reference_range = '< {}'.format(cutoff)
                if result_quantitative and cutoff:
                    if result_quantitative < cutoff:
                        abnormal_flags = 'L'
                    else:
                        abnormal_flags = 'N'
                else:
                    abnormal_flags = ''

    performing_organization_address = "{}^{}^{}^{}".format(
        tenant.address_1,
        tenant.address_2.split(',')[0],
        tenant.address_2.split(',')[1].strip().split(' ')[0],
        tenant.address_2.split(',')[1].strip().split(' ')[1],
    )

    text = "OBX|{}|NM|{}||{}|{}|{}|{}|||F|||{}||||||||{}|{}|\r".format(
        set_id,
        observation_identifier,
        observation_value,
        result_units_of_measurement,
        result_unit_reference_range,
        abnormal_flags,
        sample.collection_date.strftime(DATETIME_FORMAT),
        tenant.name,
        performing_organization_address
    )

    return text


def get_obr(order, test_panel, set_id):
    placer_order_number = order.alternate_id
    filler_order_number = order.accession_number
    universal_service_id = "{}^{}".format(
        test_panel.test_panel_type.alternate_id,
        test_panel.test_panel_type.name
    )
    sample = test_panel.tests.first().sample
    ordering_provider = "{}^{}^{}".format(
        order.provider.npi,
        order.provider.user.last_name,
        order.provider.user.first_name,
    )
    result_reported_date = test_panel.tests.filter(initial_report_date__isnull=False).\
        order_by('-initial_report_date').first().initial_report_date.strftime(DATETIME_FORMAT)

    text = "OBR|{}|{}|{}|{}||{}||||||||||{}||||||{}|||F|\r".format(
        set_id,
        placer_order_number,
        filler_order_number,
        universal_service_id,
        sample.collection_date.strftime(DATETIME_FORMAT),
        ordering_provider,
        result_reported_date
    )

    return text


def get_orc(order):
    filler_order_number = order.accession_number
    placer_order_number = order.alternate_id
    ordering_provider = "{}^{}^{}".format(
        order.provider.npi,
        order.provider.user.last_name,
        order.provider.user.first_name,
    )
    text = "ORC|NW|{}|{}|||||||||{}|\r".format(
        placer_order_number,
        filler_order_number,
        ordering_provider
    )
    return text


def get_msh(tenant, message_control_id, account, msh_dict=None):
    """
    Utilized elements: 1 Field Separator, 2 Encoding Characters, 3 Sending Application, 4 Sending Facility,
        5 Receiving Application, 6 Receiving Facility, 7 Date/Time Of Message, 9 Message Type, 10 Message Control ID,
        11 Processing ID, 12 Version ID
    :param tenant:
    :param offsite_laboratory:
    :param message_control_id:
    :return:
    """
    if msh_dict is None:
        msh_dict = MSH

    if account.alternate_id:
        receiving_facility = account.alternate_id
    else:
        receiving_facility = account.id

    message_datetime = timezone.now().strftime(DATETIME_FORMAT)
    text = "MSH|^~\&|{}|{}|MIRTH|{}|{}||{}|{}|{}|{}|||{}|{}|||||{}|\r".format(
        msh_dict['sending_application'],
        tenant.results_outbound_id,
        str(receiving_facility),
        message_datetime,
        msh_dict['message_type'],
        message_control_id,
        PROCESSING_ID_DICT[os.environ.get('DEBUG')],
        VERSION_ID,
        msh_dict['accept_acknowledgement'],
        msh_dict['application_acknowledgement'],
        msh_dict['message_profile_identifier']
        )
    return text


def get_pid(order):
    """
    Utilized elements: 3 Patient Identifier List (Internal ID), 5 Patient Name, 7 Date/Time of Birth, 8 Administrative Sex,
    11 Patient Address, 19 SSN Number
    :return:
    """
    patient = order.patient
    patient_identifier_list = patient.uuid  # change to accession number, YYMMDDxxxx - max 12 digits preferred (14 actual)

    middle_initial = get_value_or_blank(order.patient.middle_initial)
    suffix = get_value_or_blank(order.patient.suffix)
    patient_name = "{}^{}^{}^^^".format(patient.user.last_name, patient.user.first_name, middle_initial, suffix)

    birth_datetime = patient.birth_date.strftime(DATETIME_FORMAT)
    administrative_sex = patient.sex

    address = patient.address1.split(',') if patient.address1 else []
    if len(address) == 3:
        address_1 = address[0]
        city = address[1]
        state = address[2]
    else:
        address_1 = ''
        city = ''
        state = ''
    zip_code = ''  # not in DB
    patient_address = '{}^{}^{}^{}^{}'.format(address_1, patient.address2, city, state,
                                              zip_code) if patient.address1 else ''

    patient_ssn = "{}".format(get_value_or_blank(patient.social_security))

    text = "PID|1|{}|||{}||{}|{}|||{}||||||||{}|||||||||||||||||||||||||||||||||||\r".format(
        patient_identifier_list,
        patient_name,
        birth_datetime,
        administrative_sex,
        patient_address,
        patient_ssn
    )
    return text


def get_value_or_blank(value):
    """
    Return empty string if value is None
    :param address:
    :return:
    """
    if value:
        return value
    else:
        return ''


if __name__ == '__main__':
    tenant_id = 164
    order_uuid = '522ae772-7fd3-4ffb-b899-b594655d5783'
    send_results_out_hl7(tenant_id, order_uuid)
