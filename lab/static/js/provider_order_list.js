function createRecentOrderTable(recent_orders, table) {
  recent_orders.forEach(element => {
    element.code.toString = () => element.code.text;
  });

  if (!table) {
    const hotElement = document.querySelector('#recent-order-table');
    const hotSettings = {
      data: recent_orders,
      columns: [
        {
          data: 'code',
          editor: false,
          renderer: linkRenderer,
        },
        {
          data: 'name',
          editor: false,
          renderer: linkRenderer,
          width: 150,
        },
        {
          data: 'status',
          editor: false,
          type: 'text',
          className: 'htRight',
        },
        {
          data: 'submitted_date',
          editor: false,
          type: 'date',
          dateFormat: 'MM/DD/YYYY',
          correctFormat: true,
          className: 'htRight',
        },
        {
          data: 'received_date',
          editor: false,
          type: 'date',
          dateFormat: 'MM/DD/YYYY',
          correctFormat: true,
          className: 'htRight',
        },
        {
          data: 'completed_date',
          editor: false,
          type: 'date',
          dateFormat: 'MM/DD/YYYY',
          correctFormat: true,
          className: 'htRight',
        },
        {
          data: 'report_date',
          editor: false,
          type: 'date',
          dateFormat: 'MM/DD/YYYY',
          correctFormat: true,
          className: 'htRight',
        },
      ],
      stretchH: 'all',
      //width: 806,
      height: 200,
      autoColumnSize: true,
      autoWrapRow: true,
      manualRowResize: false,
      manualColumnResize: false,
      columnSorting: true,
      sortIndicator: true,
      rowHeaders: false,
      colHeaders: true,
      nestedHeaders: [
        [
          'Order Code',
          'Patient Name',
          'Status',
          'Submitted Date',
          'Received Date',
          'Completed Date',
          'Report Date',
        ],
      ],
      manualRowMove: false,
      manualColumnMove: false,
      contextMenu: false,
      filters: true,
      dropdownMenu: ['filter_by_condition', 'filter_action_bar'],
      afterGetColHeader: function(col, TH) {
        TH.className = 'center';
      },
    };

    return new Handsontable(hotElement, hotSettings);
  } else {
    // load new data without replacing old table
    table.getPlugin('filters').clearConditions();
    table.loadData(recent_orders);
    return table;
  }
}
