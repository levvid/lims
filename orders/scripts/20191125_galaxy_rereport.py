import os
import django



os.environ['DJANGO_SETTINGS_MODULE'] = 'main.settings'
django.setup()

from tenant_schemas.utils import tenant_context
import clients.models as clients_models
from orders import models, functions, generate_report_docraptor, generate_report, generate_results_out_hl7, generate_billing_hl7
from orders import tasks


def update_orders(tenant, order_uuids):
    with tenant_context(tenant):
        for order_uuid in order_uuids:
            print('Generating report for order uuid {}'.format(order_uuid))
            tasks.generate_order_report.apply_async(
                args=[tenant.id, order_uuid, False],
                queue='default'
            )
            print('Done generating report for order uuid {}'.format(order_uuid))


if __name__ == '__main__':
    tenant = clients_models.Client.objects.get(id=6)
    order_uuids = ['5fc25838-4c1e-4fcc-9994-d88f8697e8a9',
                    '65bda6f3-97bf-4082-8e36-29e78d11336e',
                    'c716db9a-b7b4-48ed-9a95-1e1c982b8ad2',
                    '9a7e72c0-faf4-444c-9a37-86e9c2de1b01',
                    '38e91d18-4a58-4a4f-bb35-09b9b3493795',
                    'a6753bcd-805a-4242-9ebe-c3bb108d7364',
                    'e947a4d0-b7fb-4828-ad54-b5da1d36ec4d',
                    '85a20822-7c1a-4353-a340-475d47b441b4',
                    '1dc339b4-d9eb-4d34-82e1-757a5f582509',
                    '5b97b0e7-80c6-4d68-9e16-ab3fa6360ac6',
                    'eaaffa99-a5e8-4605-a1b8-316d76e3290d',
                    '8cfab390-9fd6-4724-876a-4ff4f61dbbd2',
                    '7d34d31b-7931-4c07-b25d-6dc1b08700c1',
                    '7bb08829-807e-4c6d-900c-65ea2623c6da',
                    '3efa9ff6-7d2a-45e2-b9b8-3cb39f4e0e33',
                    'f0e4e1db-31c8-47e4-bc65-77c5f0cb9a8e',]
    update_orders(tenant, order_uuids)


