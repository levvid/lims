import os
import django
import logging
import sys

os.environ['DJANGO_SETTINGS_MODULE'] = 'main.settings'
django.setup()

from django.db import transaction
from tenant_schemas.utils import tenant_context

from clients import models as clients_models
from accounts import models as accounts_models


@transaction.atomic
def main():
    """
    Account.report_preference is set to blank for existing Accounts.
    Set it to 'Partial Reporting'

    :return:
    """
    tenants = clients_models.Client.objects.exclude(schema_name='public').all()

    for tenant in tenants:
        logging.debug("Account objects for schema - {}".format(tenant.schema_name))
        with tenant_context(tenant):
            account_list = []
            accounts = accounts_models.Account.objects.filter(report_preference='')
            logging.debug("{} accounts with report_preference==''".format(len(accounts)))
            for account in accounts:
                account.report_preference = 'Partial Reporting'
                account_list.append(account)
            accounts_models.Account.objects.bulk_update(account_list, ['report_preference'])


if __name__ == '__main__':
    logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)
    main()
