from django.contrib import admin
from django.urls import path
from django.views.generic import TemplateView
from . import views

urlpatterns = [
    path('', views.LandingPageView.as_view(), name='landing_page'),

    # SEO
    path('robots.txt/', TemplateView.as_view(template_name='robots.txt', content_type='text/plain')),
    path('sitemap.xml/', TemplateView.as_view(template_name='sitemap.xml', content_type='text/plain')),

    path('create_tenant', views.CreateTenantView.as_view(), name='create_tenant'),
    path('pricing/', views.PricingPageView.as_view(), name='pricing_page'),
    path('product/', views.ProductPageView.as_view(), name='product_page'),
    path('case_studies/', views.CaseStudiesPageView.as_view(), name='case_studies_page'),
    path('careers/', views.CareersListPageView.as_view(), name='careers_list_page'),
    path('careers/<str:job_name>/', views.CareersDetailsPageView.as_view(), name='careers_details_page'),
    path('admin/', admin.site.urls),
    path('login/', views.LoginView.as_view(), name='public_login'),

    # sign up activation
    path('confirm_email/<str:unregistered_user_uuid>/<str:token>/', views.ConfirmEmailView.as_view(),
         name='confirm_email'),
    path('email_verification_sent/', views.EmailVerificationSent.as_view(), name='email_verification_sent'),
    path('email_confirmed/<str:unregistered_user_uuid>/', views.EmailConfirmedView.as_view(), name='email_confirmed'),
    path('client/create/<str:unregistered_user_uuid>/', views.ClientCreateView.as_view(), name='client_create'),
    path('client/create/2/<str:unregistered_user_uuid>/', views.ClientCreate2View.as_view(), name='client_create_2'),
    path('client/create/3/<str:unregistered_user_uuid>/', views.ClientCreate3View.as_view(), name='client_create_3'),
    path('account_pending/', views.AccountPendingView.as_view(), name='account_pending')
]