from lab_tenant.models import User
from rest_framework import viewsets
from . import models
from . import serializers


class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited
    """
    queryset = User.objects.all()
    serializer_class = serializers.UserSerializer


class UserMedicationAutocompleteViewSet(viewsets.ModelViewSet):
    """
    External autocomplete API endpoint that allows medications to be searched
    q: String
    """
    serializer_class = serializers.UserMedicationSerializer
    http_method_names = ['get']
    lookup_field = 'uuid'

    def get_queryset(self):
        # currently don't filter by user_id
        user_id = self.request.query_params.get('user_id')
        query = self.request.query_params.get('q')

        medications = models.TestTarget.objects.filter(is_medication=True).all()
        if query:
            medications = medications.filter(name__icontains=query) | \
                          medications.filter(common_name__icontains=query)
            medications = medications.distinct()
        return medications
