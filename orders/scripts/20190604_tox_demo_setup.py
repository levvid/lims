import datetime
import django

import logging
import os
import random
import uuid
import sys

from django.db import transaction
from django.utils import timezone
from tenant_schemas.utils import tenant_context

os.environ['DJANGO_SETTINGS_MODULE'] = 'main.settings'
django.setup()

from clients import models as client_models
from .. import models
from accounts import models as accounts_models
from lab import models as lab_models
from lab_tenant import models as lab_tenant_models
from patients import models as patients_models

from orders.functions import generate_order_code, generate_sample_code, get_sample_barcode
from tasks import initialize_client


"""
Script to set up toxicology demo TestTypes, Tests for Toxicology lab

"""
DEMO = True
NUM_ORDERS = 50
INSTRUMENT_SERIAL_NUMBER = 'aiwthn23sriatk'
INTERNAL_INSTRUMENT_NAME = 'LCMS-8050'

# schema variables
SCHEMA_NAME = 'newstar'
LABORATORY_NAME = 'Newstar'
EMAIL = 'jbaek@dendisoftware.com'
FIRST_NAME = 'Jihoon'
LAST_NAME = 'Baek'
PASSWORD = 'dendipower!123A'
SCHEMA_CONTEXT = {'client_name': LABORATORY_NAME,
                  'schema_name': SCHEMA_NAME,
                  'email': EMAIL,
                  'first_name': FIRST_NAME,
                  'last_name': LAST_NAME,
                  'password': PASSWORD,
                  'lab_type': '',
                  }


@transaction.atomic
def main():
    try:
        tenant = client_models.Client.objects.get(schema_name=SCHEMA_NAME)
        logging.debug('Using schema: {}'.format(SCHEMA_NAME))
    except client_models.Client.DoesNotExist:
        logging.debug('Creating schema: {}'.format(SCHEMA_NAME))
        initialize_client(SCHEMA_CONTEXT)
        tenant = client_models.Client.objects.get(schema_name=SCHEMA_NAME)
        tenant.timezone = 'US/Eastern'
        tenant.save(update_fields=['timezone'])

        #logging.debug('Creating users')
        #create_users(tenant)

    logging.debug('Initializing test targets')
    # don't run initial_targets() function
    # initialize_targets()

    logging.debug('Initializing required instruments')
    initialize_instruments()

    with tenant_context(tenant):
        logging.debug('Initializing instrument integrations')
        initialize_instrument_integrations()

        # make test types
        logging.debug('Initializing test types')
        utm_test_types = initialize_test_types()

        # make test panel
        logging.debug('Initializing test panel types')
        initialize_test_panel_types(utm_test_types)

        if DEMO:
            # make account
            logging.debug('Initializing dummy accounts')
            initialize_dummy_accounts()

            # make patients
            logging.debug('Initializing dummy patients')
            initialize_dummy_patients()

            # make orders
            logging.debug('Initializing {} dummy orders'.format(NUM_ORDERS))
            initialize_dummy_orders(NUM_ORDERS)


def create_users(tenant):
    with tenant_context(tenant):
        # create LabEmployee User type
        user_obj = lab_tenant_models.User(last_name='Baek',
                                          first_name='Jihoon',
                                          username='jbaek@dendisoftware.com',
                                          email='jbaek@dendisoftware.com',
                                          user_type=1)
        # save User object first
        user_obj.set_password(PASSWORD)
        user_obj.save()
        lab_admin_obj = lab_tenant_models.LabAdmin(user=user_obj)
        lab_admin_obj.save()


def initialize_instruments():
    tox_methodology = lab_models.TestMethod.objects.get(name='LC-MS')
    instrument, created = lab_models.Instrument.objects.update_or_create(
        name='LCMS-8050',
        manufacturer='Shimadzu',
        method=tox_methodology)
    return instrument


def initialize_instrument_integrations():
    instrument_integration, created = lab_tenant_models.InstrumentIntegration.objects.update_or_create(
        internal_instrument_name=INTERNAL_INSTRUMENT_NAME,
        serial_number=INSTRUMENT_SERIAL_NUMBER,
        instrument=lab_models.Instrument.objects.get(name='LCMS-8050')
    )
    return instrument_integration


"""
def initialize_targets():
    # this is done if TestTarget is NOT a tox target
    lab_models.TestTarget.objects.update_or_create(name='Methamphetamines', is_tox_target=True)
    lab_models.TestTarget.objects.update_or_create(name='Cannabinoids (THC)', is_tox_target=True)
    lab_models.TestTarget.objects.update_or_create(name='Morphine', is_tox_target=True)
    lab_models.TestTarget.objects.update_or_create(name='Oxycodone', is_tox_target=True)
"""


def initialize_test_types():
    """
    Create 18 utm TestTypes
    :return:
    """
    tox_methodology = lab_models.TestMethod.objects.get(name='LC-MS')
    tox_specialty = lab_models.CLIATestTypeSpecialty.objects.get(specialty_name='Toxicology/TDM')
    urine_sample_type = lab_models.CLIASampleType.objects.get(name='Urine')

    utm_test_types = []
    obj, created = models.TestType.objects.update_or_create(
        name='Methamphetamine',
        test_method=tox_methodology,
        specialty=tox_specialty,
        unit='ng/mL',
        test_target=lab_models.TestTarget.objects.get(name='Methamphetamine'),
        defaults={
             'result_type': 'Qualitative',
             'required_samples': {str(urine_sample_type.id): 1}
         })
    utm_test_types.append(obj)
    obj, created = models.TestType.objects.update_or_create(
        name='Cocaine',
        test_method=tox_methodology,
        specialty=tox_specialty,
        unit='ng/mL',
        test_target=lab_models.TestTarget.objects.get(name='Cocaine'),
        defaults={
             'result_type': 'Qualitative',
             'required_samples': {str(urine_sample_type.id): 1}
         })
    utm_test_types.append(obj)
    obj, created = models.TestType.objects.update_or_create(
        name='Morphine',
        test_method=tox_methodology,
        specialty=tox_specialty,
        unit='ng/mL',
        test_target=lab_models.TestTarget.objects.get(name='Morphine'),
        defaults={
            'result_type': 'Qualitative',
            'required_samples': {str(urine_sample_type.id): 1}
        })
    utm_test_types.append(obj)
    obj, created = models.TestType.objects.update_or_create(
        name='Oxycodone',
        test_method=tox_methodology,
        specialty=tox_specialty,
        unit='ng/mL',
        test_target=lab_models.TestTarget.objects.get(name='Oxycodone'),
        defaults={
            'result_type': 'Qualitative',
            'required_samples': {str(urine_sample_type.id): 1}
        })
    utm_test_types.append(obj)
    return utm_test_types


def initialize_test_panel_types(utm_test_types):
    """
    Create the Toxicology Screening Panel and assign TestTypes to it
    :param utm_test_types:
    :return:
    """
    test_panel_type, created = models.TestPanelType.objects.update_or_create(
        name='Toxicology Screening Panel',
        menu_category_id=1)

    for test_type in utm_test_types:
        test_panel_type.test_types.add(test_type)


def initialize_dummy_accounts():
    accounts_models.Account.objects.update_or_create(name='Atlanta Pain Clinic',
                                                     address1='1204 Broad St, Atlanta, GA')
    accounts_models.Account.objects.update_or_create(name='Georgia Addiction Center',
                                                     address1='1324 Franklin St, Marietta, GA')
    accounts_models.Account.objects.update_or_create(name='Nashville Center for Emotional Health',
                                                     address1='342 Price Blvd, Nashville, TN')
    accounts_models.Account.objects.update_or_create(name='Fremont Behavioral Health',
                                                     address1='543 Hopson St, Cincinnati, OH')
    accounts_models.Account.objects.update_or_create(name='Van Ness Orthopedic Associates',
                                                     address1='2881 Mission St, San Francisco, CA')
    accounts_models.Account.objects.update_or_create(name='Sacred Heart Hospital',
                                                     address1='34255 Churchfield Ln, Charlotte, NC')
    accounts_models.Account.objects.update_or_create(name='Mission Addiction Center',
                                                     address1='559 Light Ave, Cold Springs, KY')
    accounts_models.Account.objects.update_or_create(name='Holly Springs Behavioral Center',
                                                     address1='789 Dark Ave, Herndon, VA')
    accounts_models.Account.objects.update_or_create(name='Cleveland Mental Health Clinic',
                                                     address1='7894 Hidden Valley Dr, Cleveland, OH')


def initialize_dummy_patients():
    user_uuid = str(uuid.uuid4())
    first_name = 'Jonathon'
    last_name = 'Williams'
    username = first_name + '-' + last_name + '-' + user_uuid
    user, created = lab_tenant_models.User.objects.update_or_create(first_name=first_name,
                                                                    last_name=last_name,
                                                                    user_type=20,
                                                                    defaults={
                                                                      'username': username
                                                                    })
    patients_models.Patient.objects.update_or_create(birth_date='1992-04-14',
                                                     sex='M',
                                                     user=user,
                                                     )

    user_uuid = str(uuid.uuid4())
    first_name = 'Sally'
    last_name = 'Hopson'
    username = first_name + '-' + last_name + '-' + user_uuid
    user, created = lab_tenant_models.User.objects.update_or_create(first_name=first_name,
                                                                    last_name=last_name,
                                                                    user_type=20,
                                                                    defaults={
                                                                        'username': username
                                                                    })
    patients_models.Patient.objects.update_or_create(birth_date='1994-03-13',
                                                     sex='F',
                                                     user=user,
                                                     )

    user_uuid = str(uuid.uuid4())
    first_name = 'Jerome'
    last_name = 'Hills'
    username = first_name + '-' + last_name + '-' + user_uuid
    user, created = lab_tenant_models.User.objects.update_or_create(first_name=first_name,
                                                                    last_name=last_name,
                                                                    user_type=20,
                                                                    defaults={
                                                                        'username': username
                                                                    })
    patients_models.Patient.objects.update_or_create(birth_date='1991-04-29',
                                                     sex='F',
                                                     user=user,
                                                     )

    user_uuid = str(uuid.uuid4())
    first_name = 'Joanna'
    last_name = 'Smith'
    username = first_name + '-' + last_name + '-' + user_uuid
    user, created = lab_tenant_models.User.objects.update_or_create(first_name=first_name,
                                                                    last_name=last_name,
                                                                    user_type=20,
                                                                    defaults={
                                                                        'username': username
                                                                    })
    patients_models.Patient.objects.update_or_create(birth_date='1996-09-14',
                                                     sex='F',
                                                     user=user,
                                                     )

    user_uuid = str(uuid.uuid4())
    first_name = 'Gordon'
    last_name = 'Gibson'
    username = first_name + '-' + last_name + '-' + user_uuid
    user, created = lab_tenant_models.User.objects.update_or_create(first_name=first_name,
                                                                    last_name=last_name,
                                                                    user_type=20,
                                                                    defaults={
                                                                        'username': username
                                                                    })
    patients_models.Patient.objects.update_or_create(birth_date='2012-05-19',
                                                     sex='M',
                                                     user=user,
                                                     )

    user_uuid = str(uuid.uuid4())
    first_name = 'Lawrence'
    last_name = 'Garvey'
    username = first_name + '-' + last_name + '-' + user_uuid
    user, created = lab_tenant_models.User.objects.update_or_create(first_name=first_name,
                                                                    last_name=last_name,
                                                                    user_type=20,
                                                                    defaults={
                                                                        'username': username
                                                                    })
    patients_models.Patient.objects.update_or_create(birth_date='1991-09-14',
                                                     sex='M',
                                                     user=user,
                                                     )

    user_uuid = str(uuid.uuid4())
    first_name = 'Juanita'
    last_name = 'Peters'
    username = first_name + '-' + last_name + '-' + user_uuid
    user, created = lab_tenant_models.User.objects.update_or_create(first_name=first_name,
                                                                    last_name=last_name,
                                                                    user_type=20,
                                                                    defaults={
                                                                        'username': username
                                                                    })
    patients_models.Patient.objects.update_or_create(birth_date='1992-04-12',
                                                     sex='M',
                                                     user=user,
                                                     )

    user_uuid = str(uuid.uuid4())
    first_name = 'Eric'
    last_name = 'Nelson'
    username = first_name + '-' + last_name + '-' + user_uuid
    user, created = lab_tenant_models.User.objects.update_or_create(first_name=first_name,
                                                                    last_name=last_name,
                                                                    user_type=20,
                                                                    defaults={
                                                                        'username': username
                                                                    })
    patients_models.Patient.objects.update_or_create(birth_date='1992-04-14',
                                                     sex='F',
                                                     user=user,
                                                     )

    user_uuid = str(uuid.uuid4())
    first_name = 'Fernando'
    last_name = 'Hernandez'
    username = first_name + '-' + last_name + '-' + user_uuid
    user, created = lab_tenant_models.User.objects.update_or_create(first_name=first_name,
                                                                    last_name=last_name,
                                                                    user_type=20,
                                                                    defaults={
                                                                        'username': username
                                                                    })
    patients_models.Patient.objects.update_or_create(birth_date='1982-02-10',
                                                     sex='M',
                                                     user=user,
                                                     )

    user_uuid = str(uuid.uuid4())
    first_name = 'Visesh'
    last_name = 'Prasad'
    username = first_name + '-' + last_name + '-' + user_uuid
    user, created = lab_tenant_models.User.objects.update_or_create(first_name=first_name,
                                                                    last_name=last_name,
                                                                    user_type=20,
                                                                    defaults={
                                                                        'username': username
                                                                    })
    patients_models.Patient.objects.update_or_create(birth_date='1974-08-12',
                                                     sex='M',
                                                     user=user,
                                                     )

    user_uuid = str(uuid.uuid4())
    first_name = 'Katie'
    last_name = 'Bennett'
    username = first_name + '-' + last_name + '-' + user_uuid
    user, created = lab_tenant_models.User.objects.update_or_create(first_name=first_name,
                                                                    last_name=last_name,
                                                                    user_type=20,
                                                                    defaults={
                                                                        'username': username
                                                                    })
    patients_models.Patient.objects.update_or_create(birth_date='1994-07-12',
                                                     sex='F',
                                                     user=user,
                                                     )


def initialize_dummy_orders(num_orders):
    urine_sample_type = lab_models.CLIASampleType.objects.get(name='Urine')
    test_panel_type = models.TestPanelType.objects.get(name='Toxicology Screening Panel')

    for order_count in range(num_orders):
        latest_order_code = generate_order_code()
        rand_account = accounts_models.Account.objects.get(id=random.randint(1, 9))
        rand_patient = patients_models.Patient.objects.get(id=random.randint(1, 11))
        logging.debug('Order {}: {} {} '.format(order_count, rand_account.name, rand_patient.user.first_name))
        order = models.Order.objects.create(
            provider=None,
            account=rand_account,
            patient=rand_patient,
            code=latest_order_code,
            research_consent=False,
            patient_result_request=False,

        )
        order.received_date = timezone.now()
        order.save()

        latest_sample_code = generate_sample_code(order.code, urine_sample_type)
        sample = models.Sample.objects.create(
            order=order,
            code=latest_sample_code,
            clia_sample_type=urine_sample_type,
            collection_date=timezone.now()
        )

        test_panel = models.TestPanel.objects.create(
            order=order,
            test_panel_type=test_panel_type,
            expected_revenue=test_panel_type.cost,
        )

        for test_type in test_panel_type.test_types.all():
            test = models.Test.objects.create(
                order=order,
                sample=sample,
                test_panel=test_panel,
                test_type=test_type,
                due_date=datetime.date.today() + datetime.timedelta(days=test_type.due_date_days)
            )
            result_obj = models.Result.objects.create(
                order=order,
                test=test
            )
        order.submitted = True
        order.save()


if __name__ == '__main__':
    logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)
    main()
