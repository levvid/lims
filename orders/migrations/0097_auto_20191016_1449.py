# Generated by Django 2.2.2 on 2019-10-16 14:49

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('orders', '0096_order_creating_preliminary_report'),
    ]

    operations = [
        migrations.AlterField(
            model_name='observation',
            name='data_in',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='observations', to='orders.DataIn'),
        ),
    ]
