import uuid as uuid_lib

from django.db import models
from django.utils import timezone
from django.contrib.postgres.fields import CICharField
from django.db.models.functions import Lower

from lab_tenant.models import User

from lab_tenant.models import AuditLogModel
from auditlog.registry import auditlog
from auditlog.models import AuditlogHistoryField
from main.storage_backends import PrivateMediaStorage


class Account(AuditLogModel):
    """
    Referring Providers
    """
    code = models.CharField(max_length=12, blank=True)
    uuid = models.UUIDField(
        db_index=True,
        default=uuid_lib.uuid4,
        editable=False
    )
    # IDs from different LISs
    alternate_id = models.CharField(max_length=32, blank=True)
    name = models.CharField(max_length=255, blank=True)
    user = models.OneToOneField(User, on_delete=models.SET_NULL, null=True)
    address1 = models.CharField(max_length=255, blank=True)
    address2 = models.CharField(max_length=255, blank=True)
    contact_person = models.CharField(max_length=255, blank=True)
    phone_number = models.CharField(max_length=255, blank=True)
    fax_number = models.CharField(max_length=255, blank=True)
    email = models.EmailField(null=True, blank=True)
    report_preference = models.CharField(max_length=20,
                                         choices=(('Final Reporting Only', 'Final Reporting Only'),
                                                  ('Partial Reporting', 'Partial Reporting')),
                                         default='Partial Reporting',
                                         blank=True)
    excluded_test_panels = models.ManyToManyField('orders.TestPanelType', related_name='excluded_accounts')
    # toggle send hl7 message
    results_out = models.BooleanField(default=False)
    results_out_include_base64_pdf = models.BooleanField(default=True)
    results_out_allow_partial = models.BooleanField(default=False)
    # Link of PDF in HL7 Message (default FALSE)
    results_out_show_pdf_link = models.BooleanField(default=False)
    requisition_form = models.FileField(storage=PrivateMediaStorage(), null=True)
    zip_code = models.CharField(max_length=128, blank=True)
    account_manages_signatures = models.BooleanField(default=False)
    emr_account_number = models.CharField(max_length=255, blank=True)
    notes = models.TextField(blank=True)

    class Meta:
        db_table = 'account'
        ordering = [Lower('name')]

    def __str__(self):
        if self.address1:
            return "{}".format(self.name)
        else:
            return self.name


class AccountAlias(AuditLogModel):
    account = models.ForeignKey('Account', related_name='account_aliases',
                                on_delete=models.SET_NULL, null=True)
    name = CICharField(max_length=255, blank=True)

    def __str__(self):
        return self.name


class Provider(AuditLogModel):
    """

    """
    uuid = models.UUIDField(
        db_index=True,
        default=uuid_lib.uuid4,
        editable=False
    )
    # IDs from different LISs
    alternate_id = models.CharField(max_length=32, blank=True)
    user = models.OneToOneField(User, on_delete=models.CASCADE, null=True)
    # providers can be part of multiple Accounts
    accounts = models.ManyToManyField(Account, related_name='providers')
    # Jr, II, etc.
    suffix = models.CharField(max_length=255, blank=True)
    # MD, RN, CNA, etc.
    title = models.CharField(max_length=255, blank=True)
    # national physician identifier
    npi = models.CharField(max_length=10, blank=True)
    # other identifier (for non US providers)
    id_num = models.CharField(max_length=255, blank=True)
    phone_number = models.CharField(max_length=255, blank=True)
    fax_number = models.CharField(max_length=255, blank=True)
    signature = models.FileField(storage=PrivateMediaStorage(), null=True)

    class Meta:
        db_table = 'provider'
        ordering = [Lower('user__last_name')]

    def __str__(self):
        if self.npi:
            return "{} {}, {} - {}".format(self.user.first_name, self.user.last_name, self.title, self.npi)
        else:
            return "{}, {}".format(self.user.last_name, self.user.first_name)


class Collector(AuditLogModel):
    """
    """
    uuid = models.UUIDField(
        db_index=True,
        default=uuid_lib.uuid4,
        editable=False
    )
    # IDs from different LISs
    alternate_id = models.CharField(max_length=32, blank=True)
    user = models.OneToOneField(User, on_delete=models.CASCADE, null=True, related_name='collector')
    # providers can be part of multiple Accounts
    accounts = models.ManyToManyField(Account, related_name='collectors')

    class Meta:
        ordering = [Lower('user__last_name')]


class CriticalContact(AuditLogModel):
    account = models.ForeignKey('Account', null=True, on_delete=models.SET_NULL, related_name='critical_contacts')
    alternate_id = models.CharField(max_length=32, blank=True)
    last_name = models.CharField(max_length=255, blank=True)
    first_name = models.CharField(max_length=255, blank=True)
    phone_number = models.CharField(max_length=255, blank=True)
    fax_number = models.CharField(max_length=255, blank=True)
    email = models.EmailField(null=True, blank=True)

    def __str__(self):
        return '{}, {}'.format(self.last_name, self.first_name)



######################################################################################################################
# Django AuditLogs

auditlog.register(Account)
auditlog.register(Provider)
auditlog.register(Collector)
auditlog.register(CriticalContact)
