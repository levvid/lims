import django
import logging
import os
import sys

from django.db import transaction
from tenant_schemas.utils import tenant_context

os.environ['DJANGO_SETTINGS_MODULE'] = 'main.settings'
django.setup()

from clients import models as client_models
from .. import models


@transaction.atomic
def main():
    """
    :return:
    """

    tenants = client_models.Client.objects.exclude(schema_name='public')

    for tenant in tenants:
        with tenant_context(tenant):
            reflex_list = models.TestTypeReflex.objects.all()

            for reflex in reflex_list:
                import pdb; pdb.set_trace()


if __name__ == '__main__':
    logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)
    main()
