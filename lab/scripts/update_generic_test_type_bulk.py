import django
import logging
import os
import sys
import difflib

os.environ['DJANGO_SETTINGS_MODULE'] = 'main.settings'
django.setup()

from .. import models


def update_url_link():
    generic_test_list = models.CLIATestType.objects.all()

    for test in generic_test_list:
        test.url_link = '<a href="/generic_test_types/{}">{}</a>'.format(test.uuid, test.test_system_name)
        logging.debug(test.analyte_name + ' link saved.')
        test.save()


if __name__ == '__main__':
    logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)
    # main()
    update_url_link()
