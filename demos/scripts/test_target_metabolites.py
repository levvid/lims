import django

import logging
import os
import sys

from django.db import transaction
from django.shortcuts import get_object_or_404

os.environ['DJANGO_SETTINGS_MODULE'] = 'main.settings'
django.setup()

from lab import models as lab_models

TEST_TARGET_METABOLITES = [
    {
        'name': 'Alprazolam',
        'metabolites': ['alpha-Hydroxyalprazolam']
    },
    {
        'name': 'Buprenorphine',
        'metabolites': ['Norbuprenorphine']
    },
    {
        'name': 'Suboxone',
        'metabolites': ['Norbuprenorphine']  # because Suboxone contains Buprenorphine
    },
    {
        'name': 'Fentanyl',
        'metabolites': ['Norfentanyl']
    },
    {
        'name': 'Meperidine',
        'metabolites': ['Normeperidine']
    },
    {
        'name': 'Methadone',
        'metabolites': ['EDDP']
    },
    {
        'name': 'Oxycodone',
        'metabolites': ['Noroxycodone', 'Noroxymorphone']
    },
    {
        'name': 'Propoxyphene',
        'metabolites': ['Norpropoxyphene']
    },
    {
        'name': 'Tramadol',
        'metabolites': ['O-Desmethyltramadol']
    },
    {
        'name': 'Amitriptyline',
        'metabolites': ['Nortriptyline']
    },
    {
        'name': 'Clomipramine',
        'metabolites': ['N-Desmethylclomipramine']
    },
    {
        'name': 'Doxepin',
        'metabolites': ['Desmethyldoxepin']
    },
    {
        'name': 'Imipramine',
        'metabolites': ['Desipramine']
    },
    {
        'name': 'Diazepam',
        'metabolites': ['Oxazepam', 'Temazepam', 'Nordiazepam']
    },
    {
        'name': 'Clonazepam',
        'metabolites': ['7-Aminoclonazepam']
    },
    {
        'name': 'THC',
        'metabolites': ['THC-COOH']
    },
    {
        'name': 'Hydrocodone',
        'metabolites': ['Norhydrocodone', 'Hydromorphone']
    },
    {
        # Methylphenidate (Ritalin) is special because we test primarily for its metabolite instead of the drug itself
        'name': 'Methylphenidate',
        'metabolites': ['Ritalinic Acid']
    },
    {
        'name': 'Cocaine',
        'metabolites': ['Benzoylecgonine']
    },
    {
        'name': 'Ethyl Alcohol',
        'metabolites': ['Ethyl Glucuronide', 'Ethyl Sulfate']
    },
    {
        'name': 'Alcohol',
        'metabolites': ['Ethyl Glucuronide', 'Ethyl Sulfate']
    },
    {
        'name': 'Morphine',
        'metabolites': ['6-Acetylmorphine']
    },
    {
        'name': 'Bupropion',
        'metabolites': ['Hydroxybupropion']
    },
    {
        'name': 'Fluoxetine',
        'metabolites': ['Seproxetine', 'Norfluoxetine']
    },
    {
        'name': 'Ketamine',
        'metabolites': ['Norketamine']
    },

    {
        'name': 'Oxymorphone',
        'metabolites': ['Noroxymorphone']
    },
    {
        'name': 'Naltrexone',
        'metabolites': ['6-B-Nalretxol']
    },
    {
        'name': 'Tapentadol',
        'metabolites': ['Hydroxy Tapentadol', 'N-Desmethyltapentadol']
    },
    {
        'name': 'Risperidone',
        'metabolites': ['9-Hydroxyrisperidone']
    },
    {
        'name': 'Vicks Inhaler',
        'metabolites': ['L Methamphetamine']
    },
    {
        'name': 'Desoxyn',
        'metabolites': ['D Methamphetamine']
    },
]


@transaction.atomic
def add_drug_metabolites():
    """
    Add metabolites to test targets that have them
    :return:
    """
    for test_target in TEST_TARGET_METABOLITES:
        parent_drug_name = test_target.get('name')
        parent_drug = get_object_or_404(lab_models.TestTarget, name=parent_drug_name)

        metabolite_names = test_target.get('metabolites')

        if metabolite_names:
            metabolites_qs = lab_models.TestTarget.objects.filter(name__in=metabolite_names)

            if metabolites_qs:  # failsafe for future additions to metabolites
                # add metabolites to parent drug set
                parent_drug.metabolites.set(metabolites_qs)


if __name__ == '__main__':
    logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)
    add_drug_metabolites()
