import django
import docraptor
import os
import tempfile
import time

from django.template.loader import render_to_string
from django.utils import timezone

from clients import models as clients_models
from accounts import models as accounts_models

os.environ['DJANGO_SETTINGS_MODULE'] = 'main.settings'
django.setup()

BUCKET_NAME = 'dendi-lis'

docraptor.configuration.username = os.environ['DOCRAPTOR_API']
docraptor.configuration.debug = True

if os.environ.get('DEBUG') == 'False':
    DEBUG = False
else:
    DEBUG = True

"""
Use docraptor Async create document call to generate account requisition forms and save them to s3
"""


def generate_account_requisition_pdf(tenant_id, account_uuid):
    tenant = clients_models.Client.objects.get(id=tenant_id)
    account = accounts_models.Account.objects.get(uuid=account_uuid)

    save_file_name = "requisition_forms/{}/{}/{}.pdf".format(tenant.name, account.uuid, timezone.now())

    account_providers = accounts_models.Provider.objects.filter(accounts=account)

    print(account_providers)

    reports = [1]

    full_address = account.address1.split(',')

    if len(full_address) > 1:
        address1 = full_address[0].upper()
        city_state = ', '.join(full_address[1:]).upper()
    else:
        address1 = ' '.join(full_address).upper()
        city_state = None
    rendered_html = render_to_string('account_details_requisition_form.html', {'reports': reports,
                                                                               'account': account,
                                                                               'address1': address1,
                                                                               'city_state': city_state,
                                                                               'account_providers': account_providers})
    doc_api = docraptor.DocApi()

    print(rendered_html)
    html = """<table name="Example Worksheet">
          <tr style="font-size: 16px">
            <td colspan="2">Big Row</td>
          </tr>
          <tr>
            <td style="color: blue">Blue Cell</td>
            <td style="-xls-format:currency_dollar">1000</td>
          </tr>
        </table>"""
    response = doc_api.create_async_doc({
        "test": DEBUG,  # test documents are free but watermarked
        "document_content": rendered_html,  # supply content directly
        "name": save_file_name,
        "document_type": "pdf",
        # "javascript": True,  # enable JavaScript processing
        "prince_options": {
            "media": 'screen',
            "javascript": True,  # use Prince's js option to access custom Prince addScriptFunc function for the date
        },
    })

    # async loop
    while True:
        status_response = doc_api.get_async_doc_status(response.status_id)
        if status_response.status == "completed":
            doc_response = doc_api.get_async_doc(status_response.download_id)
            with tempfile.NamedTemporaryFile() as temp:
                temp.write(doc_response)
                account.requisition_form.save(save_file_name, temp)
            break
        elif status_response.status == "failed":
            print("FAILED")
            print(status_response)
            break
        else:
            time.sleep(1)
