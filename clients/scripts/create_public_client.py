import django
import logging
import os
import sys

os.environ['DJANGO_SETTINGS_MODULE'] = 'main.settings'
django.setup()

from .. import models

DEVELOPMENT_DOMAIN = 'localhost'
PRODUCTION_DOMAIN = 'dendisoftware.com'
DOMAIN_DICT = {'production': PRODUCTION_DOMAIN,
               'development': DEVELOPMENT_DOMAIN}


def main(domain=DEVELOPMENT_DOMAIN):
    public_tenant = models.Client(domain_url=domain,
                                  schema_name='public',
                                  name='Dendi Software',
                                  paid_until='3000-01-01',
                                  on_trial=False)
    public_tenant.save()


if __name__ == '__main__':
    if len(sys.argv) == 1:
        print("Please specify production or development")
    elif len(sys.argv) == 2 and sys.argv[1].lower() in ['production', 'development']:
        target_env = sys.argv[1].lower()
        logging.info("Initializing data for {}".format(target_env))
        main(domain=DOMAIN_DICT[target_env])
    else:
        print("Unknown arguments")