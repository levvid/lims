import os
import datetime

import django
import logging


os.environ['DJANGO_SETTINGS_MODULE'] = 'main.settings'
django.setup()

from tenant_schemas.utils import tenant_context
import clients.models as clients_models
import orders.models as orders_models

MAX_DAYS_IN_TRANSIT = 14
RUN_FOR_ALL_TENANTS = False
SPECIFIC_TENANTS_SCHEMA_NAMES = ['ildp']
TENANT_SCHEMAS_TO_EXCLUDE = ['public', 'galaxy']


def archive_orders():
    """
    :return:
    """
    if RUN_FOR_ALL_TENANTS:
        all_tenants = clients_models.Client.objects.all()
        SPECIFIC_TENANTS_SCHEMA_NAMES.clear()
        for tenant in all_tenants:
            SPECIFIC_TENANTS_SCHEMA_NAMES.append(tenant.schema_name)

    tenants_to_archive = clients_models.Client.objects.filter(schema_name__in=SPECIFIC_TENANTS_SCHEMA_NAMES)\
        .exclude(schema_name__in=TENANT_SCHEMAS_TO_EXCLUDE)

    for tenant in tenants_to_archive:
        logging.debug('Archiving orders for schema {} - {}'.format(tenant.schema_name, tenant.name))
        current_utc_datetime = datetime.datetime.utcnow()
        last_datetime_in_transit = current_utc_datetime - datetime.timedelta(days=MAX_DAYS_IN_TRANSIT)
        with tenant_context(tenant):
            try:
                # get a list of orders to archive
                orders_to_archive = orders_models.Order.objects\
                    .filter(submitted_date__lte=last_datetime_in_transit, received_date__isnull=True).values_list('id')
                num_archived_orders = orders_models.Order.objects\
                    .filter(submitted_date__lte=last_datetime_in_transit, received_date__isnull=True)\
                    .update(is_active=False)

                # archive associated Tests, TestPanels and Samples
                orders_models.Sample.objects.filter(order__id__in=orders_to_archive).update(is_active=False)
                orders_models.TestPanel.objects.filter(order__id__in=orders_to_archive).update(is_active=False)
                orders_models.Test.objects.filter(order__id__in=orders_to_archive).update(is_active=False)
                orders_models.Result.objects.filter(order__id__in=orders_to_archive).update(is_active=False)


                logging.debug('Archived {} orders'.format(num_archived_orders))
                logging.warning('Archived {} orders'.format(num_archived_orders))

                # this is just a sanity check for logging - should be zero
                orders = orders_models.Order.objects.filter(submitted_date__lte=last_datetime_in_transit,
                                                            received_date__isnull=True).order_by('-submitted_date')
                logging.warning('Num of orders in transit before {} is {}'.format(last_datetime_in_transit, len(orders)))
            except django.db.utils.ProgrammingError:
                logging.warning('Model orders does not exist for schema {}'.format(tenant.schema_name))


if __name__ == '__main__':
    archive_orders()

