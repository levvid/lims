import django
import os

os.environ['DJANGO_SETTINGS_MODULE'] = 'main.settings'
django.setup()

from orders import models as orders_models

def main():
    """
    This script assumes test targets and test categories have been filled in order in
    lab.scripts.initialize_lis_shared_data
    :return:
    """
    fill_test_types_lab_services()

def fill_test_types_lab_services():
    test_type_1 = orders_models.TestType.objects.create(
        name="Bartonella henselae IFA Serology, IgG",
        test_category_id=3,
        test_target_id=3,
        isotype='IgG'
    )
    test_type_2 = orders_models.TestType.objects.create(
        name="Bartonella quintana IFA Serology, IgG",
        test_category_id=3,
        test_target_id=2,
        isotype='IgG'
    )
    test_type_3 = orders_models.TestType.objects.create(
        name="Borrelia burgdorferi ELISA IgG",
        test_category_id=5,
        test_target_id=1,
        isotype='IgG'
    )
    test_type_4 = orders_models.TestType.objects.create(
        name="Borrelia burgdorferi ELISA, IgM",
        test_category_id=5,
        test_target_id=1,
        isotype='IgM'
    )
    test_type_5 = orders_models.TestType.objects.create(
        name="Borrelia burgdorferi Western Blot IgG",
        test_category_id=4,
        test_target_id=1,
        isotype='IgG'
    )
    test_type_6 = orders_models.TestType.objects.create(
        name="Borrelia burgdorferi Western Blot IgM",
        test_category_id=4,
        test_target_id=1,
        isotype='IgM'
    )
    test_type_7 = orders_models.TestType.objects.create(
        name="Anaplasma spp PCR",
        test_category_id=1,
        test_target_id=8,
    )
    test_type_8 = orders_models.TestType.objects.create(
        name="Babesia/Theileria spp PCR",
        test_category_id=1,
        test_target_id=7,
    )
    test_type_9 = orders_models.TestType.objects.create(
        name="Bartonella spp PCR",
        test_category_id=1,
        test_target_id=6,
    )
    test_type_10 = orders_models.TestType.objects.create(
        name="Ehrlichia spp PCR",
        test_category_id=1,
        test_target_id=5,
    )
    test_type_11 = orders_models.TestType.objects.create(
        name="Rickettsia spp PCR",
        test_category_id=1,
        test_target_id=4,
    )
    test_type_12 = orders_models.TestType.objects.create(
        name="Bartonella spp ePCR™ Blood Draw",
        test_category_id=2,
        test_target_id=6,
    )
    test_type_13 = orders_models.TestType.objects.create(
        name="Bartonella spp ePCR™ Fresh/Frozen Tissue",
        test_category_id=2,
        test_target_id=6,
    )
    test_type_14 = orders_models.TestType.objects.create(
        name="Bartonella spp ePCR™ Non-blood Fluid",
        test_category_id=2,
        test_target_id=6,
    )

    lab_service_1 = orders_models.LabServiceType.objects.create(
        name="Lyme Western Blot IgM",
        cost=115,
        category="Individual Serology Test",
        notes="",
        category_order=6
    )
    lab_service_1.test_types.add(test_type_6)
    lab_service_1.save()
    lab_service_2 = orders_models.LabServiceType.objects.create(
        name="Lyme Western Blot IgG",
        cost=115,
        category="Individual Serology Test",
        notes="",
        category_order=5
    )
    lab_service_2.test_types.add(test_type_5)
    lab_service_2.save()
    lab_service_3 = orders_models.LabServiceType.objects.create(
        name="Lyme ELISA IgM",
        cost=42.5,
        category="Individual Serology Test",
        notes="",
        category_order=4
    )
    lab_service_3.test_types.add(test_type_4)
    lab_service_3.save()
    lab_service_4 = orders_models.LabServiceType.objects.create(
        name="Lyme ELISA IgG",
        cost=42.5,
        category="Individual Serology Test",
        notes="",
        category_order=3
    )
    lab_service_4.test_types.add(test_type_3)
    lab_service_4.save()
    lab_service_5 = orders_models.LabServiceType.objects.create(
        name="B. henselae IFA Serology IgG",
        cost=140,
        category="Individual Serology Test",
        notes="",
        category_order=2
    )
    lab_service_5.test_types.add(test_type_1)
    lab_service_5.save()
    lab_service_6 = orders_models.LabServiceType.objects.create(
        name="B. quintana IFA Serology IgG",
        cost=140,
        category="Individual Serology Test",
        notes="",
        category_order=1
    )
    lab_service_6.test_types.add(test_type_2)
    lab_service_6.save()
    lab_service_7 = orders_models.LabServiceType.objects.create(
        name="Rickettsia spp. PCR",
        cost=230,
        category="Individual PCR Test",
        notes="",
        category_order=5
    )
    lab_service_7.test_types.add(test_type_11)
    lab_service_7.save()
    lab_service_8 = orders_models.LabServiceType.objects.create(
        name="Ehrlichia spp. PCR",
        cost=230,
        category="Individual PCR Test",
        notes="",
        category_order=4
    )
    lab_service_8.test_types.add(test_type_10)
    lab_service_8.save()
    lab_service_9 = orders_models.LabServiceType.objects.create(
        name="Anaplasma spp. PCR",
        cost=230,
        category="Individual PCR Test",
        notes="",
        category_order=3
    )
    lab_service_9.test_types.add(test_type_7)
    lab_service_9.save()
    lab_service_10 = orders_models.LabServiceType.objects.create(
        name="Bartonella spp. PCR",
        cost=310,
        category="Individual PCR Test",
        notes="Paraffin-embedded tissue only",
        category_order=1
    )
    lab_service_10.test_types.add(test_type_9)
    lab_service_10.save()
    lab_service_11 = orders_models.LabServiceType.objects.create(
        name="Bartonella ePCR Triple Blood Draw + IFA Serology Panel IgG",
        cost=995.00,
        category="Test Panel",
        notes="B. henselae and B. quintana",
        category_order=1
    )
    lab_service_11.test_types.add(test_type_12)
    lab_service_11.test_types.add(test_type_1)
    lab_service_11.test_types.add(test_type_2)
    lab_service_11.save()
    lab_service_12 = orders_models.LabServiceType.objects.create(
        name="Bartonella IFA Serology Panel IgG",
        cost=260.00,
        category="Test Panel",
        notes="B. henselae and B. quintana",
        category_order=2
    )
    lab_service_12.test_types.add(test_type_1)
    lab_service_12.test_types.add(test_type_2)
    lab_service_12.save()
    lab_service_13 = orders_models.LabServiceType.objects.create(
        name="Bartonella ePCR Triple Blood Draw",
        cost=910.00,
        category="Test Panel",
        notes="",
        category_order=3
    )
    lab_service_13.test_types.add(test_type_12)
    lab_service_13.save()
    lab_service_14 = orders_models.LabServiceType.objects.create(
        name="Bartonella ePCR Single Blood Draw",
        cost=505.00,
        category="Test Panel",
        notes="",
        category_order=4
    )
    lab_service_14.test_types.add(test_type_12)
    lab_service_14.save()
    lab_service_15 = orders_models.LabServiceType.objects.create(
        name="Lyme ELISA Panel IgG & IgM; reflex to WB Panel IgG & IgM",
        cost=290.00,
        category="Test Panel",
        notes="",
        category_order=10
    )
    lab_service_15.test_types.add(test_type_3)
    lab_service_15.test_types.add(test_type_4)
    lab_service_15.test_types.add(test_type_5)
    lab_service_15.test_types.add(test_type_6)
    lab_service_15.save()
    lab_service_16 = orders_models.LabServiceType.objects.create(
        name="Lyme WB Panel IgG & IgM",
        cost=230.00,
        category="Test Panel",
        notes="",
        category_order=9
    )
    lab_service_16.test_types.add(test_type_5)
    lab_service_16.test_types.add(test_type_6)
    lab_service_16.save()
    lab_service_17 = orders_models.LabServiceType.objects.create(
        name="Lyme ELISA Panel IgG & IgM",
        cost=85.00,
        category="Test Panel",
        notes="",
        category_order=8
    )
    lab_service_17.test_types.add(test_type_3)
    lab_service_17.test_types.add(test_type_4)
    lab_service_17.save()
    lab_service_18 = orders_models.LabServiceType.objects.create(
        name="Tick-borne Disease PCR Panel",
        cost=615.00,
        category="Test Panel",
        notes="Babesia spp., Anaplasma spp. Ehrlichia spp. Rickettsia spp.",
        category_order=7
    )
    lab_service_18.test_types.add(test_type_8)
    lab_service_18.test_types.add(test_type_7)
    lab_service_18.test_types.add(test_type_10)
    lab_service_18.test_types.add(test_type_11)
    lab_service_18.save()
    lab_service_19 = orders_models.LabServiceType.objects.create(
        name="Babesia spp. PCR",
        cost=230.00,
        category="Individual PCR Test",
        notes="",
        category_order=2
    )
    lab_service_19.test_types.add(test_type_8)
    lab_service_19.save()
    lab_service_20 = orders_models.LabServiceType.objects.create(
        name="Bartonella ePCR Non-blood fluid",
        cost=375.00,
        category="Test Panel",
        notes="Research Use Only - signed consent required",
        category_order=5,
        research_consent_required=True
    )
    lab_service_20.test_types.add(test_type_14)
    lab_service_20.save()
    lab_service_21 = orders_models.LabServiceType.objects.create(
        name="Bartonella ePCR Fresh/Frozen Tissue",
        cost=458.00,
        category="Test Panel",
        notes="Research Use Only - signed consent required",
        category_order=6,
        research_consent_required=True
    )
    lab_service_21.test_types.add(test_type_13)
    lab_service_21.save()
if __name__ == '__main__':
    main()
