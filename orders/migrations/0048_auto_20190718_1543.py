# Generated by Django 2.2.2 on 2019-07-18 15:43

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('orders', '0047_auto_20190718_1533'),
    ]

    operations = [
        migrations.RenameField(
            model_name='testtype',
            old_name='qc_required',
            new_name='is_approval_required',
        ),
    ]
