import django
import logging
import os
import sys

from django.db import transaction
from tenant_schemas.utils import tenant_context

os.environ['DJANGO_SETTINGS_MODULE'] = 'main.settings'
django.setup()

from clients import models as client_models
from patients import models as patients_models

from orders.functions import get_sample_barcode
from .. import models


def main():
    """
    Clear all patient payer info from each order, then delete the payers
    :return:
    """
    clear_order_payers()
    delete_payers()


def delete_payers():
    """
    Script that deletes the patient payer info for each order
    :return:
    """
    tenants = client_models.Client.objects.exclude(schema_name='public').order_by('schema_name')
    #tenants = client_models.Client.objects.filter(schema_name='ildp1')

    for tenant in tenants:
        with tenant_context(tenant):

            logging.debug("Tenant: {} - deleting patient payers".format(tenant))
            patients_models.PatientPayer.all_objects.all().delete()
            logging.debug("Tenant: {} - deleting payers".format(tenant))
            patients_models.Payer.all_objects.all().delete()


@transaction.atomic
def clear_order_payers():
    """
    Script that clears the patient payer info for each order
    :return:
    """
    tenants = client_models.Client.objects.exclude(schema_name='public').order_by('schema_name')

    for tenant in tenants:
        with tenant_context(tenant):

            orders = models.Order.all_objects.all()

            updated_order_list = []

            for order in orders:
                order.primary_payer = None
                order.secondary_payer = None
                order.tertiary_payer = None
                order.workers_comp_payer = None

                updated_order_list.append(order)

                logging.debug("Tenant: {} - deleting patient payer info for order: {}"
                              .format(tenant, order.id))

            models.Order.objects.bulk_update(updated_order_list, ['primary_payer',
                                                                  'secondary_payer',
                                                                  'tertiary_payer',
                                                                  'workers_comp_payer'])


if __name__ == '__main__':
    logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)
    main()

