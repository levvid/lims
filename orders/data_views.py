import csv
import datetime
import decimal
import io
import os

import boto3
import pytz
import statistics

from django.http import HttpResponse, JsonResponse
from django.urls import reverse
from django.db.models.functions import Lower
from django.utils.html import format_html

from . import models
from accounts import models as accounts_models
from lab import models as lab_models
from lab_tenant import models as lab_tenant_models
from main.functions import date_string_or_none, convert_tenant_timezone_to_utc, convert_utc_to_tenant_timezone, \
    convert_utc_to_timezone
from orders import functions

DEFAULT_CUTOFF = 500
AWS_ACCESS_KEY_ID = os.environ.get('S3_ACCESS_KEY')
AWS_SECRET_ACCESS_KEY = os.environ.get('S3_SECRET_KEY')
SOURCE_BUCKET = 'dendi-lis'
DEV_ENVIRONMENT_TENANT = 'Dendi Test Development'
STAGING_ENVIRONMENT_TENANT = 'Dendi Test Staging'


def recent_orders(request, test_panel_filter, test_filter, date_type, start_year, start_month, start_day,
                  end_year, end_month, end_day, first_name, last_name, middle, sex, birth_date_string, accession_number,
                  account_filter, cutoff=DEFAULT_CUTOFF):
    """
    data view for order list table.
    :param request:
    :return:
    """
    timezone = request.tenant.timezone
    # date ranges are also in the format of [A, B) - start is inclusive and end is exclusive
    # date filters are time agnostic, so must convert from tenant-specific timezone to UTC
    start_date = convert_tenant_timezone_to_utc(request, datetime.datetime(start_year, start_month, start_day))
    end_date = convert_tenant_timezone_to_utc(request,
                                              datetime.datetime(end_year, end_month, end_day) + datetime.timedelta(
                                                  days=1))

    orders = models.Order.objects.filter(submitted=True). \
        order_by('-submitted_date', '-code'). \
        prefetch_related('provider', 'account', 'provider__user', 'test_panels', 'test_panels__test_panel_type',
                         'test_panels__tests', 'test_panels__test_panel_type__test_panel_type_aliases',
                         'patient', 'patient__user')

    if date_type == 'Received':
        orders = orders.filter(received_date__gte=start_date, received_date__lt=end_date)
    elif date_type == 'Submitted':
        orders = orders.filter(submitted_date__gte=start_date, submitted_date__lt=end_date)
    elif date_type == 'Reported':
        orders = orders.filter(report_date__gte=start_date, report_date__lt=end_date)

    if test_filter != 'All':
        orders = orders.filter(
            tests__test_type__uuid=test_filter,
            tests__is_active=True,
        ).distinct()

    if first_name != '_':
        orders = orders.filter(patient__user__first_name__istartswith=first_name)
    if last_name != '_':
        orders = orders.filter(patient__user__last_name__istartswith=last_name)
    if middle != '_':
        orders = orders.filter(patient__middle_initial=middle)
    if sex != '_':
        orders = orders.filter(patient__sex=sex)
    if birth_date_string != '_':
        birth_date = datetime.datetime.strptime(birth_date_string, '%m-%d-%Y')
        orders = orders.filter(patient__birth_date=birth_date)
    if accession_number != '_':
        orders = orders.filter(accession_number__istartswith=accession_number)
    if test_panel_filter != '_':
        test_panel_contains = orders.filter(test_panels__test_panel_type__name__icontains=test_panel_filter)
        test_panel_alias_contains = orders.filter(test_panels__test_panel_type__test_panel_type_aliases__name__icontains=test_panel_filter)
        # union of 2 querysets returns distinct set
        orders = test_panel_contains.union(test_panel_alias_contains)
    if account_filter != '_':
        account_name_contains = orders.filter(account__name__icontains=account_filter)
        account_alias_contains = orders.filter(account__account_aliases__name__icontains=account_filter)
        orders = account_name_contains.union(account_alias_contains)

    # limit results requested by providers
    if request.user.user_type == 10:
        provider = accounts_models.Provider.objects.get(user__id=request.user.id)
        orders = orders.filter(provider=provider)
    elif request.user.user_type == 9:
        account = accounts_models.Account.objects.get(user__id=request.user.id)
        orders = orders.filter(account=account)

    results = []
    order_count = orders.count()
    order_list = orders.all()[:cutoff]
    for order in order_list:
        if order.accession_number:
            accession_number = str(order.accession_number)
        else:
            accession_number = ''

        if order.patient:
            patient_text = order.patient.user.last_name + ', ' + order.patient.user.first_name
            # patient DOB doesn't need to be converted to datetime
            birth_date = date_string_or_none(order.patient.birth_date)
            if order.patient.is_active:
                patient = {
                    'text': patient_text,
                    'url': '<a href="/patients/{}">{}</a>'.format(order.patient.uuid, patient_text)
                }
            else:
                patient = patient_text
        else:
            patient = ""
            birth_date = ""

        # if account is active, show link, if not then don't show link
        if order.account:
            if order.account.is_active:
                account = {
                    'text': order.account.name,
                    'url': '<a href="/accounts/{}">{}</a>'.format(order.account.uuid, order.account.name),
                }
            else:
                account = {
                    'text': order.account.name,
                    'url': order.account.name,
                }
            if order.account.account_aliases.all():
                account_aliases = order.account.account_aliases.all()
                account_alias_list = []
                for account_alias in account_aliases:
                    account_alias_list.append(str(account_alias))

                account_alias_str = ', '.join(account_alias_list)
                if len(account_alias_str) > 30:
                    account_alias_str = account_alias_str[:30] + '...'
            else:
                account_alias_str = ''
        else:
            account = {
                'text': '',
                'url': '#'
            }
            account_alias_str = ''

        # if provider is active, show link, if not then don't show link
        if order.provider:
            provider_text = order.provider.user.last_name + ', ' + order.provider.user.first_name
            if order.provider.is_active:
                provider = {
                    'text': provider_text,
                    'url': '<a href="/providers/{}">{}</a>'.format(order.provider.uuid, provider_text),
                }
            else:
                provider = {
                    'text': provider_text,
                    'url': provider_text,
                }
        else:
            provider = {
                'text': '',
                'url': '',
            }
        # if patient result is requested, then show check mark, otherwise show empty string
        if order.patient_result_request:
            order.patient_result_request = u'\u2713'
        else:
            order.patient_result_request = ''

        # if research consent is required, then show check mark, otherwise show empty string
        if order.research_consent:
            order.research_consent = u'\u2713'
        else:
            order.research_consent = ''

        if order.test_panels.all():
            test_panels = order.test_panels.all()
            test_panels_list = []
            test_panels_alias_list = []

            for test_panel in test_panels:
                test_panel_type = test_panel.test_panel_type
                if test_panel.test_panel_type.name:
                    # truncate after 22 characters
                    test_panel_type_name = (test_panel.test_panel_type.name[:25] + '...') \
                        if len(test_panel.test_panel_type.name) > 25 else test_panel.test_panel_type.name
                    test_panels_list.append(test_panel_type_name)
                for alias in test_panel_type.test_panel_type_aliases.all():
                    test_panels_alias_list.append(str(alias))

            test_panels_alias_str = ', '.join(test_panels_alias_list)
            if len(test_panels_alias_str) > 30:
                test_panels_alias_str = test_panels_alias_str[:30] + '...'
            test_panels_str = '<br> '.join(test_panels_list)
        else:
            test_panels_str = ''
            test_panels_alias_str = ''

        submitted_date = date_string_or_none(convert_utc_to_timezone(timezone, order.submitted_date))
        received_date = date_string_or_none(convert_utc_to_timezone(timezone, order.received_date))
        completed_date = date_string_or_none(convert_utc_to_timezone(timezone, order.completed_date))
        report_date = date_string_or_none(convert_utc_to_timezone(timezone, order.report_date))
        results.append({
            'code': {
                'text': order.code,
                'url': '<a href="/orders/{}">{}</a>'.format(order.code, order.code),
            },
            'accession_number': accession_number,
            'birth_date': birth_date,
            'patient': patient,
            'provider': provider,
            'account': account,
            'account_alias': account_alias_str,
            'test_panels': test_panels_str,
            'test_panels_alias': test_panels_alias_str,
            'submitted_date': submitted_date,
            'received_date': received_date,
            'completed_date': completed_date,
            'report_date': report_date,
            'status': order.status,
            'patient_result_request': order.patient_result_request,
            'research_consent': order.research_consent,
        })
    response = {
        'count': order_count,
        'returned_count': len(results),
        'results': results,
    }
    return JsonResponse(response, safe=False)


def archived_orders(request, test_filter, date_type, start_year, start_month, start_day, end_year, end_month, end_day):
    """
    data view for archived order list table.
    :param request:
    :return:
    """

    # date ranges are also in the format of [A, B) - start is inclusive and end is exclusive
    # date filters are time agnostic, so must convert from tenant-specific timezone to UTC
    start_date = convert_tenant_timezone_to_utc(request, datetime.datetime(start_year, start_month, start_day))
    end_date = convert_tenant_timezone_to_utc(request,
                                              datetime.datetime(end_year, end_month, end_day) + datetime.timedelta(
                                                  days=1))

    orders = models.Order.all_objects.filter(
        submitted=True,
        is_active=False
    ). \
        order_by('-submitted_date', '-code'). \
        prefetch_related('provider', 'account', 'provider__user', 'test_panels', 'test_panels__tests',
                         'patient', 'patient__user')

    if date_type == 'Received':
        orders = orders.filter(received_date__gte=start_date, received_date__lt=end_date)
    elif date_type == 'Submitted':
        orders = orders.filter(submitted_date__gte=start_date, submitted_date__lt=end_date)
    elif date_type == 'Reported':
        orders = orders.filter(report_date__gte=start_date, report_date__lt=end_date)

    if test_filter != 'All':
        orders = orders.filter(
            tests__test_type__uuid=test_filter,
            tests__is_active=True,
        ).distinct()

    # limit results requested by providers
    if request.user.user_type == 10:
        provider = accounts_models.Provider.objects.get(user__id=request.user.id)
        orders = orders.filter(provider=provider)
    elif request.user.user_type == 9:
        account = accounts_models.Account.objects.get(user__id=request.user.id)
        orders = orders.filter(account=account)
    elif request.user.user_type == 11:
        collector = accounts_models.Collector.objects.get(user__id=request.user.id)
        collector_accounts = collector.accounts.all()
        order_list = []
        for account in collector_accounts:
            order_list.append(orders.filter(account=account))
        # merge querysets
        orders = order_list[0].union(*order_list[1:])

    results = []

    for order in orders:

        if order.accession_number:
            accession_number = str(order.accession_number)
        else:
            accession_number = ''

        if order.patient:
            patient_text = order.patient.user.last_name + ', ' + order.patient.user.first_name
            if order.patient.is_active:
                patient = '<!--{}--><a href="/patients/{}">{}</a>'.format(
                    patient_text,
                    order.patient.uuid,
                    patient_text
                )
            else:
                patient = patient_text
        else:
            patient = ""

        # if account is active, show link, if not then don't show link
        if order.account:
            if order.account.is_active:
                account = {
                    'text': order.account.name,
                    'url': '<a href="/accounts/{}">{}</a>'.format(order.account.uuid, order.account.name),
                }
            else:
                account = {
                    'text': order.account.name,
                    'url': order.account.name,
                }
        else:
            account = {
                'text': '',
                'url': '#'
            }

        # if provider is active, show link, if not then don't show link
        if order.provider:
            provider_text = order.provider.user.last_name + ', ' + order.provider.user.first_name
            if order.provider.is_active:
                provider = {
                    'text': provider_text,
                    'url': '<a href="/providers/{}">{}</a>'.format(order.provider.uuid, provider_text),
                }
            else:
                provider = {
                    'text': provider_text,
                    'url': provider_text,
                }
        else:
            provider = {
                'text': '',
                'url': '',
            }

        order_undo_archive_link = format_html(
            '<a href="/orders/{}/undo_delete/"><i class="far fa-trash-restore"></i></a>',
            order.code
        )

        # if patient result is requested, then show check mark, otherwise show empty string
        if order.patient_result_request:
            order.patient_result_request = u'\u2713'
        else:
            order.patient_result_request = ''

        # if research consent is required, then show check mark, otherwise show empty string
        if order.research_consent:
            order.research_consent = u'\u2713'
        else:
            order.research_consent = ''

        # patient DOB doesn't need to be converted to datetime
        if order.patient:
            birth_date = date_string_or_none(order.patient.birth_date)
        else:
            birth_date = ''

        submitted_date = date_string_or_none(convert_utc_to_tenant_timezone(request, order.submitted_date))
        received_date = date_string_or_none(convert_utc_to_tenant_timezone(request, order.received_date))
        completed_date = date_string_or_none(convert_utc_to_tenant_timezone(request, order.completed_date))
        report_date = date_string_or_none(convert_utc_to_tenant_timezone(request, order.report_date))
        results.append({
            'code': order.code,
            'accession_number': accession_number,
            'birth_date': birth_date,
            'patient': patient,
            'provider': provider,
            'account': account,
            'submitted_date': submitted_date,
            'received_date': received_date,
            'completed_date': completed_date,
            'report_date': report_date,
            'status': order.status,
            'patient_result_request': order.patient_result_request,
            'order_undo_archive_link': order_undo_archive_link,
        })

    return JsonResponse(results, safe=False)


def accession_check(request, test_filter, start_year, start_month, start_day, end_year, end_month, end_day):
    """
    data view for order list table.
    :param request:
    :return:
    """
    start_date = convert_tenant_timezone_to_utc(request, datetime.datetime(start_year, start_month, start_day))
    end_date = convert_tenant_timezone_to_utc(request,
                                              datetime.datetime(end_year, end_month, end_day) + datetime.timedelta(
                                                  days=1))

    orders = models.Order.objects.filter(submitted=True, received_date__gte=start_date,
                                         received_date__lt=end_date).order_by('-submitted_date', '-code'). \
        prefetch_related('provider', 'account', 'provider__user', 'test_panels', 'test_panels__tests',
                         'patient', 'patient__user', 'samples')

    if test_filter != 'All':
        orders = orders.filter(
            test_panels__tests__test_type__uuid=test_filter,
            test_panels__is_active=True,
            test_panels__tests__is_active=True,
        ).distinct()

    # limit results requested by providers
    if request.user.user_type == 10:
        provider = accounts_models.Provider.objects.get(user__id=request.user.id)
        orders = orders.filter(provider=provider)
    elif request.user.user_type == 9:
        account = accounts_models.Account.objects.get(user__id=request.user.id)
        orders = orders.filter(account=account)
    elif request.user.user_type == 11:
        collector = accounts_models.Collector.objects.get(user__id=request.user.id)
        collector_accounts = collector.accounts.all()
        order_list = []
        for account in collector_accounts:
            order_list.append(orders.filter(account=account))
        # merge querysets
        orders = order_list[0].union(*order_list[1:])

    results = []

    for order in orders:
        # if account is active, show link, if not then don't show link
        if order.account.is_active:
            account = {
                'text': order.account.name,
                'url': '<a href="/accounts/{}">{}</a>'.format(order.account.uuid, order.account.name),
            }
        else:
            account = {
                'text': order.account.name,
                'url': order.account.name,
            }

        # if provider is active, show link, if not then don't show link
        if order.provider:
            provider_text = order.provider.user.last_name + ', ' + order.provider.user.first_name
            if order.provider.is_active:
                provider = {
                    'text': provider_text,
                    'url': '<a href="/providers/{}">{}</a>'.format(order.provider.uuid, provider_text),
                }
            else:
                provider = {
                    'text': provider_text,
                    'url': provider_text,
                }
        else:
            provider = {
                'text': '',
                'url': '',
            }

        if order.patient:
            patient_text = order.patient.user.last_name + ', ' + order.patient.user.first_name
            if order.patient.is_active:
                patient = {
                    'text': patient_text,
                    'url': '<a href="/patients/{}">{}</a>'.format(order.patient.uuid, patient_text)
                }
            else:
                patient = {
                    'text': patient_text,
                    'url': patient_text
                }
        else:
            patient = {
                'text': '',
                'url': '',
            }

        collection_date_list = []
        for sample in order.samples.all():
            collection_date = date_string_or_none(convert_utc_to_tenant_timezone(request, sample.collection_date))
            collection_date_list.append(collection_date)

        collection_dates = '\n '.join(collection_date_list)
        patient_birth_date = date_string_or_none(order.patient.birth_date)
        submitted_date = date_string_or_none(convert_utc_to_tenant_timezone(request, order.submitted_date))
        received_date = date_string_or_none(convert_utc_to_tenant_timezone(request, order.received_date))
        completed_date = date_string_or_none(convert_utc_to_tenant_timezone(request, order.completed_date))
        report_date = date_string_or_none(convert_utc_to_tenant_timezone(request, order.report_date))

        results.append({
            'code': {
                'text': order.code,
                'url': '<a href="/orders/{}">{}</a>'.format(order.code, order.code),
            },
            'accession_number': order.accession_number,
            'provider': provider,
            'account': account,
            'patient': patient,
            'tests': '\n '.join([str(x.test_panel_type.name) for x in order.test_panels.all()]),
            'samples': '\n '.join([str(x.code) for x in order.samples.all()]),
            'collection_dates': collection_dates,
            'patient_birth_date': patient_birth_date,
            'submitted_date': submitted_date,
            'received_date': received_date,
            'completed_date': completed_date,
            'report_date': report_date,
            'status': order.status,
        })

    return JsonResponse(results, safe=False)


def workqueue_table(request, start_year, start_month, start_day, end_year, end_month, end_day):
    """
    data view for workqueue page table
    :param request:
    :return:
    """
    start_date = convert_tenant_timezone_to_utc(request, datetime.datetime(start_year, start_month, start_day))
    end_date = convert_tenant_timezone_to_utc(request,
                                              datetime.datetime(end_year, end_month, end_day) + datetime.timedelta(
                                                  days=1))

    orders = models.Order.objects.filter(submitted=True,
                                         received_date__isnull=False,
                                         completed=False,
                                         submitted_date__gte=start_date,
                                         submitted_date__lt=end_date). \
        order_by('-submitted_date', '-code'). \
        prefetch_related('provider', 'account', 'provider__user', 'test_panels')

    # limit results requested by providers
    if request.user.user_type == 10:
        provider = accounts_models.Provider.objects.get(user__id=request.user.id)
        orders = orders.filter(provider=provider)
    elif request.user.user_type == 9:
        account = accounts_models.Account.objects.get(user__id=request.user.id)
        orders = orders.filter(account=account)
    results = []

    for order in orders:
        # if account is active, show link, if not then don't show link
        if order.account.is_active:
            account = '<!--{}--><a href="/accounts/{}">{}</a>'.format(order.account.name,
                                                                      order.account.uuid,
                                                                      order.account.name)
        else:
            account = order.account.name

        # if provider is active, show link, if not then don't show link
        if order.provider:
            provider_text = order.provider.user.last_name + ', ' + order.provider.user.first_name
            if order.provider.is_active:
                provider = '<!--{}--><a href="/providers/{}">{}</a>'.format(provider_text,
                                                                            order.provider.uuid,
                                                                            provider_text)
            else:
                provider = provider_text
        else:
            provider = ""

        submitted_date = date_string_or_none(convert_utc_to_tenant_timezone(request, order.submitted_date))

        num_tests_pending = order.tests.filter(result__result__isnull=True).count()
        results.append({
            'code': '<a href="/orders/{}">{}</a>'.format(order.code, order.code),
            'accession_number': order.accession_number,
            'provider': provider,
            'account': account,
            'submitted_date': submitted_date,
            'status': order.status,
            'num_tests_pending': num_tests_pending
        })

    return JsonResponse(results, safe=False)


def workqueue_issues_table(request, test_filter, date_type, start_year, start_month, start_day, end_year, end_month,
                           end_day):
    """
    data view for order issues table.
    :param request:
    :return:
    """

    # date filters are time agnostic, so must convert from tenant-specific timezone to UTC
    start_date = convert_tenant_timezone_to_utc(request, datetime.datetime(start_year, start_month, start_day))
    end_date = convert_tenant_timezone_to_utc(request,
                                              datetime.datetime(end_year, end_month, end_day) + datetime.timedelta(
                                                  days=1))

    order_issues = models.OrderIssue.objects.filter(is_resolved=False, order__submitted=True).order_by(
        '-order__submitted_date', '-order__code'). \
        prefetch_related('order', 'order__provider', 'order__account')

    # only show issues that are marked as ok to show for providers/clinics
    # limit results requested by providers
    if request.user.user_type == 10:
        provider = accounts_models.Provider.objects.get(user__id=request.user.id)
        order_issues = order_issues.filter(order__provider=provider, is_public=True)
    elif request.user.user_type == 9:
        account = accounts_models.Account.objects.get(user__id=request.user.id)
        order_issues = order_issues.filter(order__account=account, is_public=True)

    if date_type == 'Received':
        order_issues = order_issues.filter(order__received_date__gte=start_date, order__received_date__lt=end_date)
    elif date_type == 'Submitted':
        order_issues = order_issues.filter(order__submitted_date__gte=start_date, order__submitted_date__lt=end_date)
    elif date_type == 'Reported':
        order_issues = order_issues.filter(order__report_date__gte=start_date, order__report_date__lt=end_date)

    if test_filter != 'All':
        order_issues = order_issues.filter(issue_type=test_filter)

    results = []

    for order_issue in order_issues:
        # if account is active, show link, if not then don't show link
        if order_issue.order.account:
            if order_issue.order.account.is_active:
                account = {
                    'text': order_issue.order.account.name,
                    'url': '<a href="/accounts/{}">{}</a>'.format(order_issue.order.account.uuid,
                                                                  order_issue.order.account.name),
                }
            else:
                account = {
                    'text': order_issue.order.account.name,
                    'url': order_issue.order.account.name,
                }
        else:
            account = {
                'text': '',
                'url': '#'
            }

        # if provider is active, show link, if not then don't show link
        if order_issue.order.provider:
            provider_text = order_issue.order.provider.user.last_name + ', ' + order_issue.order.provider.user.first_name
            if order_issue.order.provider.is_active:
                provider = {
                    'text': provider_text,
                    'url': '<a href="/providers/{}">{}</a>'.format(order_issue.order.provider.uuid, provider_text),
                }
            else:
                provider = {
                    'text': provider_text,
                    'url': provider_text,
                }
        else:
            provider = {
                'text': '',
                'url': '',
            }

        # truncate string if length is more than 75 characters
        issue_description = (order_issue.description[:75] + '...') if len(
            order_issue.description) > 75 else order_issue.description

        # if account or provider view, don't show link
        if not (request.user.user_type == 9 or request.user.user_type == 10):
            issue_description = format_html('<a href="/orders/{}/issues/" target="_blank">{}</a>',
                                            order_issue.order.code, issue_description)

        results.append({
            'code': {
                'text': order_issue.order.code,
                'url': '<a href="/orders/{}">{}</a>'.format(order_issue.order.code, order_issue.order.code),
            },
            'accession_number': order_issue.order.accession_number,
            'provider': provider,
            'account': account,
            'status': order_issue.order.status,
            # use get_FOO_display() to get choice labels
            'issue_type': order_issue.get_issue_type_display(),
            'issue_description': issue_description,
        })

    return JsonResponse(results, safe=False)


def workqueue_test_count(request, test_method_id):
    """
    data view to return number of pending tests given a test method
    :param request:
    :param test_method_id:
    :return:
    """
    pending_test_count = models.Test.objects.filter(
        result__result__isnull=True,
        order__is_active=True,
        order__submitted=True,
        test_type__test_method__id=test_method_id
    ).count()
    return JsonResponse(pending_test_count, safe=False)


def workqueue_test_table(request, test_method, start_year, start_month, start_day, end_year, end_month, end_day):
    """
    data view for workqueue test table (only tests)
    :param request:
    :return:
    """
    start_date = convert_tenant_timezone_to_utc(request, datetime.datetime(start_year, start_month, start_day))
    end_date = convert_tenant_timezone_to_utc(request,
                                              datetime.datetime(end_year, end_month, end_day) + datetime.timedelta(
                                                  days=1))

    if test_method == 'All':
        pending_tests = models.Test.objects.filter(order__submitted=True,
                                                   order__is_active=True,
                                                   initial_report_date__isnull=True,
                                                   due_date__gte=start_date,
                                                   due_date__lt=end_date) \
            .order_by('due_date', 'order__code') \
            .prefetch_related('order', 'order__account', 'order__provider', 'order__provider__user', 'sample',
                              'test_type')
    else:
        test_method_obj = lab_models.TestMethod.objects.get(name=test_method)
        pending_tests = models.Test.objects.filter(order__submitted=True,
                                                   order__is_active=True,
                                                   initial_report_date__isnull=True,
                                                   due_date__gte=start_date,
                                                   due_date__lt=end_date,
                                                   test_type__test_method=test_method_obj) \
            .order_by('due_date', 'order__code') \
            .prefetch_related('order', 'order__account', 'order__provider', 'order__provider__user', 'sample',
                              'test_type')

    # limit results requested by providers
    if request.user.user_type == 10:
        provider = accounts_models.Provider.objects.get(user__id=request.user.id)
        pending_tests = pending_tests.filter(order__provider=provider)

    results = []

    for test in pending_tests:
        # if account is active, show link, if not then don't show link
        order = test.order
        if order.account:
            if order.account.is_active:
                account = {
                    'text': order.account.name,
                    'url': '<a href="/accounts/{}">{}</a>'.format(order.account.uuid, order.account.name),
                }
            else:
                account = {
                    'text': order.account.name,
                    'url': order.account.name,
                }
        else:
            account = {
                'text': '',
                'url': '#'
            }

        # if provider is active, show link, if not then don't show link
        if order.provider:
            provider_text = order.provider.user.last_name + ', ' + order.provider.user.first_name
            if order.provider.is_active:
                provider = {
                    'text': provider_text,
                    'url': '<a href="/providers/{}">{}</a>'.format(order.provider.uuid, provider_text),
                }
            else:
                provider = {
                    'text': provider_text,
                    'url': provider_text,
                }
        else:
            provider = {
                'text': '',
                'url': '',
            }

        order_submitted_date = date_string_or_none(convert_utc_to_tenant_timezone(request, test.order.submitted_date))
        # due date isn't a datetime currently
        due_date = date_string_or_none(test.due_date)

        if order.patient_result_request:
            patient_result_request = u'\u2713'
        else:
            patient_result_request = ''

        results.append({
            'code': {
                'text': order.code,
                'url': '<a href="/orders/{}">{}</a>'.format(order.code, order.code),
            },
            'accession_number': order.accession_number,
            'test_name': test.test_type.name,
            'account': account,
            'provider': provider,
            'sample': test.sample.code,
            'order_submitted_date': order_submitted_date,
            'due_date': due_date,
            'patient_result_request': patient_result_request
        })

    return JsonResponse(results, safe=False)


def test_results_table(request, result, target_uuid, account_uuid, start_year, start_month, start_day, end_year,
                       end_month, end_day):
    """
    data view for test results table (only tests)
    :param request:
    :return:
    """
    start_date = convert_tenant_timezone_to_utc(request, datetime.datetime(start_year, start_month, start_day))
    end_date = convert_tenant_timezone_to_utc(request,
                                              datetime.datetime(end_year, end_month, end_day) + datetime.timedelta(
                                                  days=1))

    # exclude results that have been reported but still pending actual values
    test_list = models.Test.objects.filter(order__submitted=True,
                                           order__is_active=True,
                                           is_approved=True,
                                           # can be approved but not reported
                                           initial_report_date__gte=start_date,
                                           # using datetime, so really this is searching the end_date + 1 (+4:00)
                                           initial_report_date__lt=end_date, ) \
        .exclude(result__result='.') \
        .order_by('due_date', 'order__code') \
        .prefetch_related('order', 'order__account', 'order__provider', 'order__provider__user', 'sample',
                          'result', 'test_type', 'test_type__test_target')

    # filter further by test target
    if target_uuid != 'All':
        test_target = lab_models.TestTarget.objects.get(uuid=target_uuid)
        test_list = test_list.filter(test_type__test_target=test_target)

    # filter by account
    if account_uuid != 'All':
        account = accounts_models.Account.objects.get(uuid=account_uuid)
        test_list = test_list.filter(order__account=account)

    if result != 'All':
        if result == 'Positive':
            result = '+'
        if result == 'Negative':
            result = '-'
        if result == 'Equivocal':
            result = '='
        if result == 'Indeterminate':
            result = '?'
        test_list = test_list.filter(result__result=result)

    # limit results requested by providers
    if request.user.user_type == 10:
        provider = accounts_models.Provider.objects.get(user__id=request.user.id)
        test_list = test_list.filter(order__provider=provider)
    elif request.user.user_type == 9:
        account = accounts_models.Account.objects.get(user__id=request.user.id)
        test_list = test_list.filter(order__account=account)

    results = []

    for test in test_list:
        # if account is active, show link, if not then don't show link
        order = test.order
        if order.account:
            if order.account.is_active:
                account = {
                    'text': order.account.name,
                    'url': '<a href="/accounts/{}">{}</a>'.format(order.account.uuid, order.account.name),
                }
            else:
                account = {
                    'text': order.account.name,
                    'url': order.account.name,
                }
        else:
            account = {
                'text': '',
                'url': '#'
            }

        # if provider is active, show link, if not then don't show link
        if order.provider:
            provider_text = order.provider.user.last_name + ', ' + order.provider.user.first_name
            if order.provider.is_active:
                provider = {
                    'text': provider_text,
                    'url': '<a href="/providers/{}">{}</a>'.format(order.provider.uuid, provider_text),
                }
            else:
                provider = {
                    'text': provider_text,
                    'url': provider_text,
                }
        else:
            provider = {
                'text': '',
                'url': '',
            }

        test_report_date = date_string_or_none(convert_utc_to_tenant_timezone(request, test.approved_date))
        test_result = test.result.get().result

        # it's much faster to do this than using default result_label property
        if test_result == '+':
            test_result = 'Positive'
        if test_result == '-':
            test_result = 'Negative'
        if test_result == '=':
            test_result = 'Equivocal'
        if test_result == '?':
            test_result = 'Indeterminate'

        results.append({
            'code': {
                'text': order.code,
                'url': '<a href="/orders/{}">{}</a>'.format(order.code, order.code),
            },
            'accession_number': order.accession_number,
            'test_name': test.test_type.name,
            'test_target': str(test.test_type.test_target),
            'account': account,
            'provider': provider,
            'sample': test.sample.code,
            'test_report_date': test_report_date,
            'test_result': test_result
        })

    return JsonResponse(results, safe=False)


def workqueue_pending_approval_table(request, start_year, start_month, start_day, end_year, end_month, end_day):
    """
    data view for pending approval workflow table
    :param request:
    :return:
    """
    start_date = convert_tenant_timezone_to_utc(request, datetime.datetime(start_year, start_month, start_day))
    end_date = convert_tenant_timezone_to_utc(request,
                                              datetime.datetime(end_year, end_month, end_day) + datetime.timedelta(
                                                  days=1))

    orders = models.Order.objects.filter(submitted=True,
                                         completed=True,
                                         reported=False,
                                         submitted_date__gte=start_date,
                                         submitted_date__lt=end_date). \
        order_by('-submitted_date', '-code'). \
        prefetch_related('provider', 'provider__user', 'account', 'test_panels')

    # limit results requested by providers
    if request.user.user_type == 10:
        provider = accounts_models.Provider.objects.get(user__id=request.user.id)
        orders = orders.filter(provider=provider)
    elif request.user.user_type == 9:
        account = accounts_models.Account.objects.get(user__id=request.user.id)
        orders = orders.filter(account=account)
    results = []

    for order in orders:

        if order.accession_number:
            accession_number = str(order.accession_number)
        else:
            accession_number = ''

        # if account is active, show link, if not then don't show link
        if order.account.is_active:
            account = {
                'text': order.account.name,
                'url': '<a href="/accounts/{}">{}</a>'.format(order.account.uuid, order.account.name),
            }
        else:
            account = {
                'text': order.account.name,
                'url': order.account.name,
            }

        # if provider is active, show link, if not then don't show link
        if order.provider:
            provider_text = order.provider.user.last_name + ', ' + order.provider.user.first_name
            if order.provider.is_active:
                provider = {
                    'text': provider_text,
                    'url': '<a href="/providers/{}">{}</a>'.format(order.provider.uuid, provider_text),
                }
            else:
                provider = {
                    'text': provider_text,
                    'url': provider_text,
                }
        else:
            provider = {
                'text': '',
                'url': '',
            }

        approve_link = format_html('<a href="/orders/{}/approve/"><i class="far fa-directions"></i></a>',
                                   order.code)

        submitted_date = date_string_or_none(convert_utc_to_tenant_timezone(request, order.submitted_date))
        received_date = date_string_or_none(convert_utc_to_tenant_timezone(request, order.received_date))
        completed_date = date_string_or_none(convert_utc_to_tenant_timezone(request, order.completed_date))

        results.append({
            'code': {
                'text': order.code,
                'url': '<a href="/orders/{}">{}</a>'.format(order.code, order.code),
            },
            'accession_number': accession_number,
            'provider': provider,
            'account': account,
            'submitted_date': submitted_date,
            'received_date': received_date,
            'completed_date': completed_date,
            'status': order.status,
            'approve_link': approve_link
        })

    return JsonResponse(results, safe=False)


def workqueue_unreceived_table(request, start_year, start_month, start_day, end_year, end_month, end_day):
    """
    data view for in transit workflow table
    :param request:
    :return:
    """
    start_date = convert_tenant_timezone_to_utc(request, datetime.datetime(start_year, start_month, start_day))
    end_date = convert_tenant_timezone_to_utc(request,
                                              datetime.datetime(end_year, end_month, end_day) + datetime.timedelta(
                                                  days=1))

    orders = models.Order.objects.filter(submitted=True,
                                         received_date__isnull=True, submitted_date__gte=start_date,
                                         submitted_date__lt=end_date). \
        order_by('-submitted_date', '-code'). \
        prefetch_related('provider', 'provider__user', 'account', 'test_panels')
    # limit results requested by providers
    if request.user.user_type == 10:
        provider = accounts_models.Provider.objects.get(user__id=request.user.id)
        orders = orders.filter(provider=provider)
    elif request.user.user_type == 9:
        account = accounts_models.Account.objects.get(user__id=request.user.id)
        orders = orders.filter(account=account)
    elif request.user.user_type == 11:
        collector = accounts_models.Collector.objects.get(user__id=request.user.id)
        collector_accounts = collector.accounts.all()
        order_list = []
        for account in collector_accounts:
            order_list.append(orders.filter(account=account))
        # merge querysets
        orders = order_list[0].union(*order_list[1:])

    results = []

    for order in orders:
        report = models.Report.objects.filter(order=order).order_by('-created_date').first()

        # if account is active, show link, if not then don't show link
        if order.account:
            if order.account.is_active:
                account = {
                    'text': order.account.name,
                    'url': '<a href="/accounts/{}">{}</a>'.format(order.account.uuid, order.account.name),
                }
            else:
                account = {
                    'text': order.account.name,
                    'url': order.account.name,
                }
        else:
            account = {
                'text': '',
                'url': '#'
            }

        # if provider is active, show link, if not then don't show link
        if order.provider:
            provider_text = order.provider.user.last_name + ', ' + order.provider.user.first_name
            if order.provider.is_active:
                provider = {
                    'text': provider_text,
                    'url': '<a href="/providers/{}">{}</a>'.format(order.provider.uuid, provider_text),
                }
            else:
                provider = {
                    'text': provider_text,
                    'url': provider_text,
                }
        else:
            provider = {
                'text': '',
                'url': '',
            }

        submitted_date = date_string_or_none(convert_utc_to_tenant_timezone(request, order.submitted_date))

        results.append({
            'code': {
                'text': order.code,
                'url': '<a href="/orders/{}">{}</a>'.format(order.code, order.code),
            },
            'provider': provider,
            'account': account,
            'submitted_date': submitted_date,
            'status': order.status
        })

    return JsonResponse(results, safe=False)


def provider_inbox_table(request, start_year, start_month, start_day, end_year, end_month, end_day):
    """
    data view for provider inbox table
    :param request:
    :return:
    """

    orders = models.Order.objects.filter(reported=True,
                                         is_archived_by_provider=False). \
        order_by('-submitted_date', '-code'). \
        prefetch_related('provider', 'provider__user', 'reports', 'patient', 'patient__user')
    # limit results requested by providers
    if request.user.user_type == 10:
        provider = accounts_models.Provider.objects.get(user__id=request.user.id)
        orders = orders.filter(provider=provider)
    if request.user.user_type == 9:
        account = accounts_models.Account.objects.get(user__id=request.user.id)
        orders = orders.filter(account=account)
    if request.user.user_type == 11:
        collector = accounts_models.Collector.objects.get(user__id=request.user.id)
        orders = orders.filter(account__collectors=collector)

    results = []

    for order in orders:
        report = models.Report.objects.filter(order=order).order_by('-created_date').first()

        try:
            pdf_url = report.pdf_file.url

            pdf_link = format_html('<a href="{}" target="_blank"><i class="fas fa-search"></i></a>',
                                   pdf_url)
        except AttributeError:
            pdf_link = '<a href="{}">{}</a>'.format('#', '')

        submitted_date = date_string_or_none(convert_utc_to_tenant_timezone(request, order.submitted_date))
        received_date = date_string_or_none(convert_utc_to_tenant_timezone(request, order.received_date))
        report_date = date_string_or_none(convert_utc_to_tenant_timezone(request, order.report_date))

        # format name
        if order.patient.middle_initial:
            name_string = "{}, {} {}.".format(order.patient.user.last_name,
                                              order.patient.user.first_name,
                                              order.patient.middle_initial)
        else:
            name_string = "{}, {}".format(order.patient.user.last_name,
                                          order.patient.user.first_name)
        patient_name = '<!--{}--><a href="/patients/{}" target="_blank">{}</a>'.format(name_string,
                                                                                       order.patient.uuid,
                                                                                       name_string)

        # clear from inbox
        archive_inbox_checkbox = format_html('<input type="checkbox" '
                                             'name="is_archived" '
                                             'id="{}" value="{}">',
                                             order.uuid, order.uuid)

        results.append({
            'code': '<a href="/orders/{}" target="_blank">{}</a>'.format(order.code, order.code),
            'report_pdf': pdf_link,
            'archive_inbox_checkbox': archive_inbox_checkbox,
            'patient_name': patient_name,
            'submitted_date': submitted_date,
            'received_date': received_date,
            'report_date': report_date,
            'status': order.status,
        })

    return JsonResponse(results, safe=False)


def workqueue_reported_table(request, start_year, start_month, start_day, end_year, end_month, end_day,
                             first_name, last_name, middle, sex, birth_date_string, accession_number):
    """
    data view for reported workflow table
    :param request:
    :return:
    """
    start_date = convert_tenant_timezone_to_utc(request, datetime.datetime(start_year, start_month, start_day))
    end_date = convert_tenant_timezone_to_utc(request,
                                              datetime.datetime(end_year, end_month, end_day) + datetime.timedelta(
                                                  days=1))

    orders = models.Order.objects.filter(reported=True, submitted_date__gte=start_date,
                                         submitted_date__lt=end_date). \
        order_by('-submitted_date', '-code'). \
        prefetch_related('provider', 'provider__user', 'account', 'test_panels')
    # limit results requested by providers
    if request.user.user_type == 10:
        provider = accounts_models.Provider.objects.get(user__id=request.user.id)
        orders = orders.filter(provider=provider)
    elif request.user.user_type == 9:
        account = accounts_models.Account.objects.get(user__id=request.user.id)
        orders = orders.filter(account=account)

    if first_name != '_':
        orders = orders.filter(patient__user__first_name__istartswith=first_name)
    if last_name != '_':
        orders = orders.filter(patient__user__last_name__istartswith=last_name)
    if middle != '_':
        orders = orders.filter(patient__middle_initial=middle)
    if sex != '_':
        orders = orders.filter(patient__sex=sex)
    if birth_date_string != '_':
        birth_date = datetime.datetime.strptime(birth_date_string, '%m-%d-%Y')
        orders = orders.filter(patient__birth_date=birth_date)
    if accession_number != '_':
        orders = orders.filter(accession_number__istartswith=accession_number)

    results = []

    for order in orders:
        report = models.Report.objects.filter(order=order).order_by('-created_date').first()

        if order.accession_number:
            accession_number = str(order.accession_number)
        else:
            accession_number = ''

        # if account is active, show link, if not then don't show link
        if order.account.is_active:
            account = '<!--{}--><a href="/accounts/{}">{}</a>'.format(order.account.name,
                                                                      order.account.uuid,
                                                                      order.account.name)
        else:
            account = order.account.name

        # if provider is active, show link, if not then don't show link
        if order.provider:
            provider_text = order.provider.user.last_name + ', ' + order.provider.user.first_name
            if order.provider.is_active:
                provider = '<!--{}--><a href="/providers/{}">{}</a>'.format(provider_text,
                                                                            order.provider.uuid,
                                                                            provider_text)
            else:
                provider = provider_text
        else:
            provider = ""

        if order.patient:
            patient_text = order.patient.user.last_name + ', ' + order.patient.user.first_name
            if order.patient.is_active:
                patient = '<!--{}--><a href="/patients/{}">{}</a>'.format(patient_text,
                                                                          order.patient.uuid,
                                                                          patient_text)
            else:
                patient = patient_text
        else:
            patient = ""

        try:
            pdf_url = report.pdf_file.url

            pdf_link = format_html('<a href="{}" target="_blank"><i class="fas fa-search"></i></a>',
                                   pdf_url)
        except AttributeError:
            pdf_link = '<a href="{}">{}</a>'.format('#', '')

        submitted_date = date_string_or_none(convert_utc_to_tenant_timezone(request, order.submitted_date))
        received_date = date_string_or_none(convert_utc_to_tenant_timezone(request, order.received_date))
        completed_date = date_string_or_none(convert_utc_to_tenant_timezone(request, order.completed_date))
        report_date = date_string_or_none(convert_utc_to_tenant_timezone(request, order.report_date))

        results.append({
            'code': '<a href="/orders/{}">{}</a>'.format(order.code, order.code),
            'accession_number': accession_number,
            'patient': patient,
            'provider': provider,
            'account': account,
            'report_pdf': pdf_link,
            'submitted_date': submitted_date,
            'received_date': received_date,
            'completed_date': completed_date,
            'report_date': report_date,
            'status': order.status,
            'approved_user': '',
        })

    return JsonResponse(results, safe=False)


def account_reported_orders_table(request, account_uuid):
    """
    data view for reported orders workflow table
    :param request:
    :return:
    """
    orders = models.Order.objects.filter(reported=True,
                                         account__uuid=account_uuid). \
        order_by('-submitted_date', '-code'). \
        prefetch_related('provider', 'provider__user', 'account')

    results = []

    for order in orders:
        report = models.Report.objects.filter(order=order).order_by('-created_date').first()
        # if account is active, show link, if not then don't show link
        if order.account.is_active:
            account = {
                'text': order.account.name,
                'url': '<a href="/accounts/{}">{}</a>'.format(order.account.uuid, order.account.name),
            }
        else:
            account = {
                'text': order.account.name,
                'url': order.account.name,
            }

        # if provider is active, show link, if not then don't show link
        if order.provider:
            provider_text = order.provider.user.last_name + ', ' + order.provider.user.first_name
            if order.provider.is_active:
                provider = {
                    'text': provider_text,
                    'url': '<a href="/providers/{}">{}</a>'.format(order.provider.uuid, provider_text),
                }
            else:
                provider = {
                    'text': provider_text,
                    'url': provider_text,
                }
        else:
            provider = {
                'text': '',
                'url': '',
            }

        try:
            pdf_url = report.pdf_file.url

            pdf_link = format_html('<a href="{}" target="_blank"><i class="fas fa-search"></i></a>',
                                   pdf_url)
        except AttributeError:
            pdf_link = '<a href="{}">{}</a>'.format('#', '')

        submitted_date = date_string_or_none(convert_utc_to_tenant_timezone(request, order.submitted_date))
        received_date = date_string_or_none(convert_utc_to_tenant_timezone(request, order.received_date))
        completed_date = date_string_or_none(convert_utc_to_tenant_timezone(request, order.completed_date))
        report_date = date_string_or_none(convert_utc_to_tenant_timezone(request, order.report_date))

        results.append({
            'code': {
                'text': order.code,
                'url': '<a href="/orders/{}">{}</a>'.format(order.code, order.code),
            },
            'provider': provider,
            'account': account,
            'report_pdf': pdf_link,
            'submitted_date': submitted_date,
            'received_date': received_date,
            'completed_date': completed_date,
            'report_date': report_date,
            'status': order.status,
        })

    return JsonResponse(results, safe=False)


def reference_range_table(request, test_type_uuid, sex_filter, account_name_filter):
    """
    data view for reference range table
    Used in TestTypeDetails and ReferenceRangeCreate views
    :param request:
    :return:
    """
    def get_age_range_string(reference_range):
        min_age_range_string = ''
        max_age_range_string = ''
        age_range_string = ''
        if reference_range.min_age_total == 0 and reference_range.max_age_total == 0:
            return age_range_string
        if reference_range.min_age_total > 0:
            if reference_range.min_age_years:
                min_age_range_string += '{} years '.format(reference_range.min_age_years)
            if reference_range.min_age_months:
                min_age_range_string += '{} months '.format(reference_range.min_age_months)
            if reference_range.min_age_weeks:
                min_age_range_string += '{} weeks '.format(reference_range.min_age_weeks)
            if reference_range.min_age_days:
                min_age_range_string += '{} days '.format(reference_range.min_age_days)
        else:
            min_age_range_string = 'Birth '
        if reference_range.max_age_total > 0:
            if reference_range.max_age_years:
                max_age_range_string += '{} years '.format(reference_range.max_age_years)
            if reference_range.max_age_months:
                max_age_range_string += '{} months '.format(reference_range.max_age_months)
            if reference_range.max_age_weeks:
                max_age_range_string += '{} weeks '.format(reference_range.max_age_weeks)
            if reference_range.max_age_days:
                max_age_range_string += '{} days '.format(reference_range.max_age_days)

        age_range_string = '{}- {}'.format(min_age_range_string, max_age_range_string)
        return age_range_string

    sex_dict = {'NB': '', 'M': 'Male', 'F': 'Female'}

    test_type = models.TestType.objects.get(uuid=test_type_uuid)
    unit_of_measurement = test_type.unit
    test_type_reference_ranges = test_type.test_type_ranges.all().order_by('-sex', 'min_age_total')

    if account_name_filter != '_':
        test_type_reference_ranges = test_type_reference_ranges.filter(accounts__name__icontains=account_name_filter)
    if sex_filter != 'All':
        test_type_reference_ranges = test_type_reference_ranges.filter(sex=sex_filter)

    test_type_reference_ranges = test_type_reference_ranges.distinct()

    results = []
    for reference_range in test_type_reference_ranges:
        if reference_range.default:
            reference_range_type = 'Default'
        else:
            reference_range_type = 'Rule'
        if reference_range.range_low:
            range_low = "{} {}".format(reference_range.range_low, unit_of_measurement)
        else:
            range_low = ""
        if reference_range.range_high:
            range_high = "{} {}".format(reference_range.range_high, unit_of_measurement)
        else:
            range_high = ""
        if reference_range.critical_low:
            critical_low = "{} {}".format(reference_range.critical_low, unit_of_measurement)
        else:
            critical_low = ""
        if reference_range.critical_high:
            critical_high = "{} {}".format(reference_range.critical_high, unit_of_measurement)
        else:
            critical_high = ""
        if reference_range.accounts:
            account_objects = reference_range.accounts.all()
            accounts = ', '.join(
                ['<!--{}--><a href="/accounts/{}">{}</a>'.format(account.name, account.uuid, account.name)
                    for account in account_objects])
        else:
            accounts = ''

        results.append(
            {
                'reference_range_type': reference_range_type,
                'sex': sex_dict[reference_range.sex],
                'accounts': accounts,
                'low': range_low,
                'high': range_high,
                'critical_high': critical_high,
                'critical_low': critical_low,
                'age_range': get_age_range_string(reference_range),
                'comment': reference_range.report_comment,
                'critical_comment': reference_range.critical_report_comment,
                'actions':
                    "<a href='/test_types/{}/ranges/{}/update' "
                    "class='btn btn-warning'><i class='fas fa-pencil-alt'></i>"
                    "<a href='/test_types/{}/ranges/{}/delete' "
                    "class='btn btn-danger prompt-delete'><i class='far fa-trash-alt'></i>".format(
                    test_type.uuid, reference_range.uuid,
                    test_type.uuid, reference_range.uuid,
                ),
            }
        )

    return JsonResponse(results, safe=False)


def test_panel_type_table(request, name, category, alias, lab_department):
    """
    data view for test panel type table
    :param request:
    :return:
    """
    test_panel_types = models.TestPanelType.objects.order_by(Lower('name'), 'test_panel_category__name'). \
        prefetch_related('test_types', 'test_panel_category')
    if name != '_':
        name = name.strip()
        test_panel_types = test_panel_types.filter(name__icontains=name)
    if category != '_':
        category = category.strip()
        test_panel_types = test_panel_types.filter(test_panel_category__name__icontains=category,
                                                   test_panel_category__is_active=True)
    if alias != '_':
        alias = alias.strip()
        test_panel_types = test_panel_types.filter(test_panel_type_aliases__name__icontains=alias,
                                                   test_panel_type_aliases__is_active=True)
    if lab_department != '_':
        lab_department = lab_department.strip()
        test_panel_types = test_panel_types.filter(lab_departments__name__icontains=lab_department,
                                                   lab_departments__is_active=True)
    test_panel_types = test_panel_types.distinct()

    results = []
    for test_panel_type in test_panel_types:
        if test_panel_type.test_panel_category:
            category = test_panel_type.test_panel_category.name
        else:
            category = ''

        # show alternate id if panel has it
        if test_panel_type.alternate_id:
            test_panel_type_label = "({}) {}".format(test_panel_type.alternate_id,
                                                     test_panel_type.name)
        else:
            test_panel_type_label = test_panel_type.name
        in_house_lab_locations = ''
        if test_panel_type.in_house_lab_locations.all():
            for in_house_lab_location in test_panel_type.in_house_lab_locations.all():
                in_house_lab_locations += '{}\n'.format(in_house_lab_location.name)

        lab_departments = ''
        if test_panel_type.lab_departments.all():
            for lab_department in test_panel_type.lab_departments.all():
                lab_departments += '{}\n'.format(lab_department.name)

        if test_panel_type.test_panel_type_aliases.all():
            aliases = test_panel_type.test_panel_type_aliases.all()
            alias_list = []
            for alias in aliases:
                alias_list.append(str(alias))

            alias_str = ', '.join(alias_list)
        else:
            alias_str = ''

        test_panel_type_data = {
            'name': '<!--{}--><a href="/test_profile_types/{}">{}</a>'.format(
                test_panel_type.name,
                test_panel_type.uuid,
                test_panel_type_label
            ),
            'alias': alias_str,
            'category': category,
            'price': test_panel_type.price,
            'in_house_lab_locations': in_house_lab_locations,
            'lab_departments': lab_departments,
            'num_test_types': test_panel_type.test_types.count()
        }
        results.append(test_panel_type_data)
    return JsonResponse(results, safe=False)


def super_test_panel_type_table(request):
    """
    data view for super test panel type table
    :param request:
    :return:
    """
    super_test_panel_types = models.SuperTestPanelType.objects.order_by(Lower('name')). \
        prefetch_related('test_panel_types', 'test_panel_types__test_types')

    results = []

    for super_test_panel_type in super_test_panel_types:

        test_types_count = models.TestType.objects.filter(testpaneltype__super_test_panel_types=super_test_panel_type).distinct().count()
        test_panel_types_count = models.TestPanelType.objects.filter(super_test_panel_types=super_test_panel_type).distinct().count()

        super_test_panel_type_data = {
            'name': '<a href="/super_test_profile_types/{}">{}</a>'.format(
                super_test_panel_type.uuid,
                super_test_panel_type.name,
            ),
            'test_panel_type_count': test_panel_types_count,
            'test_type_count':  test_types_count
        }

        results.append(super_test_panel_type_data)
    return JsonResponse(results, safe=False)


def batch_table(request):
    """
    data view for test batch table
    :param request:
    :return:
    """
    batches = models.Batch.objects.all(). \
        prefetch_related('test_panel_type', 'instrument_integration'). \
        order_by('-created_date')

    results = []
    for batch in batches:
        batch_data = {
            'name': '<!--{}--><a href="/batches/{}">{}</a>'.format(
                batch.name,
                batch.uuid,
                batch.name
            ),
            'num_samples': batch.samples.count(),
            'test_panel_type': batch.test_panel_type.name,
            'instrument_integration': batch.instrument_integration.internal_instrument_name,
            'created_datetime': convert_utc_to_tenant_timezone(request, batch.created_date).strftime(
                '%Y-%m-%d %I:%M %p')
        }
        results.append(batch_data)
    return JsonResponse(results, safe=False)


def control_table(request):
    """
    :param request:
    :return:
    """
    controls = models.Control.objects.all()

    results = []
    for control in controls:
        control_data = {
            'name': '<!--{}--><a href="/controls/{}">{}</a>'.format(
                control.name,
                control.uuid,
                control.name
            ),
            'code': control.code,
        }
        results.append(control_data)
    return JsonResponse(results, safe=False)


def control_levey_jennings_chart(request, control_uuid, instrument_integration, test_target, start_year,
                                 start_month, start_day, end_year, end_month, end_day):
    """

    :param request:
    :return:
    """
    response = {
        'data': [],
        'unit_of_measurement': '',
        'mean': '',
        'standard_deviation': ''
    }

    start_date = convert_tenant_timezone_to_utc(request, datetime.datetime(start_year, start_month, start_day))
    end_date = convert_tenant_timezone_to_utc(request,
                                              datetime.datetime(end_year, end_month, end_day) + datetime.timedelta(
                                                  days=1))
    control = models.Control.objects.get(uuid=control_uuid)
    try:
        instrument_integration_object = lab_tenant_models.InstrumentIntegration.objects.get(
            internal_instrument_name=instrument_integration,
        )
    except lab_tenant_models.InstrumentIntegration.DoesNotExist:
        return JsonResponse(response, safe=False)

    observations = models.Observation.objects.filter(
        control=control,
        target_name=test_target,
        instrument_integration__internal_instrument_name=instrument_integration,
        observation_date__gte=start_date,
        observation_date__lt=end_date,
    ).all().order_by('observation_date')
    results = []
    for observation in observations:
        observation_date = convert_utc_to_tenant_timezone(request, observation.observation_date)
        observation_data = [int(observation_date.timestamp()) * 1000, float(observation.result_value)]
        results.append(observation_data)

    # standard deviation
    try:
        standard_deviation = statistics.stdev([x[1] for x in results])
    except statistics.StatisticsError:
        standard_deviation = 0
    try:
        mean = statistics.mean([x[1] for x in results])
    except statistics.StatisticsError:
        mean = 0
    try:
        known_value = float(control.known_values.get(test_target__name=test_target).concentration)
    except models.ControlKnownValue.DoesNotExist:
        known_value = 0
    except:
        known_value = 0

    response = {
        'data': results,
        'unit_of_measurement': instrument_integration_object.instrument.unit_of_measurement,
        'mean': mean,
        'standard_deviation': standard_deviation,
        'known_value': known_value,
    }
    return JsonResponse(response, safe=False)


def test_type_table(request, name, alias, category, specialty, method, target):
    """
    data view for test type table
    :param request:
    :return:
    """
    test_types = models.TestType.objects.filter().order_by(Lower('name'), 'test_method__name').\
        prefetch_related('test_method', 'test_target', 'specialty', 'test_type_categories')
    if name != '_':
        name = name.strip()
        test_types = test_types.filter(name__icontains=name)
    if alias != '_':
        alias = alias.strip()
        test_types = test_types.filter(test_type_aliases__name__icontains=alias,
                                       test_type_aliases__is_active=True)
    if category != '_':
        category = category.strip()
        test_types = test_types.filter(test_type_categories__name__icontains=category,
                                       test_type_categories__is_active=True)
    if specialty != '_':
        specialty = specialty.strip()
        test_types = test_types.filter(specialty__specialty_name__icontains=specialty,
                                       specialty__is_active=True)
    if method != '_':
        method = method.strip()
        test_types = test_types.filter(test_method__name__icontains=method,
                                       test_method__is_active=True)
    if target != '_':
        target = target.strip()
        test_types = test_types.filter(test_target__name__icontains=target,
                                       test_target__is_active=True)

    test_types = test_types.distinct()

    results = []
    for test_type in test_types:
        if test_type.test_target:
            target_name = test_type.test_target.name
        else:
            target_name = ''

        if test_type.specialty:
            specialty = test_type.specialty.specialty_name
        else:
            specialty = ''

        if test_type.test_method:
            test_method_name = test_type.test_method.name
        else:
            test_method_name = ''

        if test_type.test_type_categories.all():
            test_type_category_list = [test_type_category.name for test_type_category in test_type.test_type_categories.all()]
            test_type_categories = ', '.join(test_type_category_list)
        else:
            test_type_categories = ''

        if test_type.container_type:
            container_type = test_type.container_type.name
        else:
            container_type = ''

        if test_type.min_volume:
            min_volume = test_type.min_volume
        elif test_type.min_volume == decimal.Decimal('0.00'):
            min_volume = '0'
        else:
            min_volume = ''

        if test_type.test_type_aliases.all():
            aliases = test_type.test_type_aliases.all()
            alias_list = []
            for alias in aliases:
                alias_list.append(str(alias))

            alias_str = ', '.join(alias_list)
        else:
            alias_str = ''
        if len(alias_str) > 30:
            alias_str = alias_str[:30] + '...'

        test_type_data = {
            'name': '<!--{}--><a href="/test_types/{}">{}</a>'.format(
                test_type.name,
                test_type.uuid,
                test_type.name
            ),
            'alias': alias_str,
            'specialty': specialty,
            'method': test_method_name,
            'target': target_name,
            'test_type_categories': test_type_categories,
            'container_type': container_type,
            'min_volume': min_volume,
        }
        results.append(test_type_data)
    return JsonResponse(results, safe=False)


def container_type_table(request):
    """
    data view for container type table
    :param request:
    :return:
    """
    container_types = models.ContainerType.objects.all().order_by('name')

    results = []
    for container_type in container_types:
        if container_type.volume:
            volume_string = str(container_type.volume)
        else:
            volume_string = ''

        container_type_data = {
            'name': '<!--{}--><a href="/container_types/{}">{}</a>'.format(
                container_type.name,
                container_type.uuid,
                container_type.name
            ),
            'volume': str(volume_string)
        }
        results.append(container_type_data)
    return JsonResponse(results, safe=False)


def test_type_category_table(request):
    """
    data view for test type category table
    :param request:
    :return:
    """
    test_type_categories = models.TestTypeCategory.objects.filter().order_by(Lower('name'))

    results = []

    for test_type_category in test_type_categories:
        test_type_category_data = {
            'name': {
                'text': test_type_category.name,
                'url': '<a href="/test_type_categories/{}/">{}</a>'.format(test_type_category.uuid, test_type_category.name)
            }
        }
        results.append(test_type_category_data)

    return JsonResponse(results, safe=False)


def sample_table(request, target_uuid, method_uuid, start_year, start_month, start_day, end_year, end_month, end_day):
    """
    data view for test results table (only tests)
    :param request:
    :return:
    """
    start_date = convert_tenant_timezone_to_utc(request, datetime.datetime(start_year, start_month, start_day))
    end_date = convert_tenant_timezone_to_utc(request,
                                              datetime.datetime(end_year, end_month, end_day) + datetime.timedelta(
                                                  days=1))

    if method_uuid != 'All':
        test_method = lab_models.TestMethod.objects.get(uuid=method_uuid)

        sample_list = models.Sample.objects.filter(order__submitted=True,
                                                   order__is_active=True,
                                                   tests__test_type__test_method=test_method) \
            .order_by('-order__code').distinct() \
            .prefetch_related('order', 'tests', 'tests__test_type',
                              'tests__test_type__test_target',
                              'tests__test_type__test_method')
    else:
        sample_list = models.Sample.objects.filter(order__submitted=True,
                                                   order__is_active=True) \
            .order_by('-order__code') \
            .prefetch_related('order', 'tests', 'tests__test_type',
                              'tests__test_type__test_target')

    # does sample itself have a received date? otherwise, use the associated orders' received date
    sample_list_with_received_date = sample_list.filter(received_date__isnull=False,
                                                        received_date__gte=start_date,
                                                        received_date__lt=end_date)
    sample_list_without_received_date = sample_list.filter(received_date__isnull=True,
                                                           order__received_date__gte=start_date,
                                                           order__received_date__lt=end_date)

    # union of 2 querysets (cannot add querysets)
    sample_list_combined = sample_list_with_received_date | sample_list_without_received_date

    # filter further by test target
    if target_uuid != 'All':
        test_target = lab_models.TestTarget.objects.get(uuid=target_uuid)
        sample_list_combined = sample_list.filter(tests__test_type__test_target=test_target)

    results = []

    for sample in sample_list_combined:
        # can have multiple test targets per sample
        sample_tests = sample.tests.all()
        sample_test_target_list = []

        for test in sample_tests:
            sample_test_target_list.append(test.test_type.test_target.name)
        sample_test_targets = ', '.join(sample_test_target_list)

        # if sample has a received date
        if sample.received_date:
            received_date = date_string_or_none(convert_utc_to_tenant_timezone(request, sample.received_date))
        else:
            received_date = date_string_or_none(convert_utc_to_tenant_timezone(request, sample.order.received_date))

        results.append({
            'sample_code': sample.code,
            'order_code': {
                'text': sample.order.code,
                'url': '<a href="/orders/{}">{}</a>'.format(sample.order.code, sample.order.code),
            },
            'accession_number': sample.order.accession_number,
            'received_date': received_date,
            'test_targets': sample_test_targets,
        })

    return JsonResponse(results, safe=False)


def sample_type_table(request):
    sample_types = lab_models.CLIASampleType.objects.all().order_by('name')
    results = []
    for sample_type in sample_types:
        sample_type_data = {
            'name': {
                'text': sample_type.name,
                'url': '<a href="/clia_sample_types/{}">{}</a>'.format(sample_type.uuid, sample_type.name)
            }
        }
        results.append(sample_type_data)
    return JsonResponse(results, safe=False)


def provider_reported_orders_table(request, provider_uuid):
    """
    data view for provider reported orders passed workflow table
    :param request:
    :return:
    """
    orders = models.Order.objects.filter(reported=True,
                                         provider__uuid=provider_uuid). \
        order_by('-submitted_date', '-code'). \
        prefetch_related('provider', 'provider__user', 'account')

    results = []

    for order in orders:
        report = models.Report.objects.filter(order=order).order_by('-created_date').first()

        # if provider is active, show link, if not then don't show link
        if order.provider:
            provider = str(order.provider)
        else:
            provider = ''

        try:
            pdf_url = report.pdf_file.url

            pdf_link = format_html('<a href="{}" target="_blank"><i class="fas fa-search"></i></a>',
                                   pdf_url)
        except AttributeError:
            pdf_link = '<a href="{}">{}</a>'.format('#', '')

        submitted_date = date_string_or_none(convert_utc_to_tenant_timezone(request, order.submitted_date))
        received_date = date_string_or_none(convert_utc_to_tenant_timezone(request, order.received_date))
        completed_date = date_string_or_none(convert_utc_to_tenant_timezone(request, order.completed_date))
        report_date = date_string_or_none(convert_utc_to_tenant_timezone(request, order.report_date))

        results.append({
            'code': {
                'text': order.code,
                'url': '<a href="/orders/{}">{}</a>'.format(order.code, order.code),
            },
            'provider': provider,
            'account': order.account.name,
            'report_pdf': pdf_link,
            'submitted_date': submitted_date,
            'received_date': received_date,
            'completed_date': completed_date,
            'report_date': report_date,
            'status': order.status,
        })

    return JsonResponse(results, safe=False)


def patient_reported_orders_table(request, patient_uuid):
    """
    data view for provider reported orders passed workflow table
    :param request:
    :return:
    """
    orders = models.Order.objects.filter(reported=True,
                                         patient__uuid=patient_uuid). \
        order_by('-submitted_date', '-code'). \
        prefetch_related('patient', 'patient__user', 'provider', 'account')

    results = []

    for order in orders:
        report = models.Report.objects.filter(order=order).order_by('-created_date').first()

        # if provider is active, show link, if not then don't show link
        if order.provider:
            provider = str(order.provider)
        else:
            provider = ''

        try:
            pdf_url = report.pdf_file.url

            pdf_link = format_html('<a href="{}" target="_blank"><i class="fas fa-search"></i></a>',
                                   pdf_url)
        except AttributeError:
            pdf_link = '<a href="{}">{}</a>'.format('#', '')

        submitted_date = date_string_or_none(convert_utc_to_tenant_timezone(request, order.submitted_date))
        received_date = date_string_or_none(convert_utc_to_tenant_timezone(request, order.received_date))
        completed_date = date_string_or_none(convert_utc_to_tenant_timezone(request, order.completed_date))
        report_date = date_string_or_none(convert_utc_to_tenant_timezone(request, order.report_date))

        results.append({
            'code': {
                'text': order.code,
                'url': '<a href="/orders/{}">{}</a>'.format(order.code, order.code),
            },
            'provider': provider,
            'account': order.account.name,
            'report_pdf': pdf_link,
            'submitted_date': submitted_date,
            'received_date': received_date,
            'completed_date': completed_date,
            'report_date': report_date,
            'status': order.status,
        })

    return JsonResponse(results, safe=False)


def operational_report_table(request):
    """
    Dataview for handsontable on Operational Reports page
    :param request:
    :return:
    """
    results = []
    reports = models.OperationalReport.objects.filter(user=request.user).order_by('-created_datetime').all()

    for report in reports:
        try:
            report_url = report.report.url
            report_link = '<a href="{}" target="_blank">{}</a>'.format(report_url, 'Download')
        except AttributeError:
            report_link = '<a href="{}" target="_blank">{}</a>'.format('#', '')
        except ValueError:
            report_link = '<a href="{}" target="_blank">{}</a>'.format('#', '')

        created_datetime = convert_utc_to_tenant_timezone(request, report.created_datetime).strftime(
            "%B %d, %Y %H:%M %Z")

        # don't convert timezones, since the start and end dates aren't meant to be interpreted in that way
        # convert to date string
        start_date = date_string_or_none(report.start_date)
        end_date = date_string_or_none(report.end_date)

        results.append({
            'created_datetime': created_datetime,
            'start_date': start_date,
            'end_date': end_date,
            'date_filter': report.date_filter,
            'report_format': report.report_format,
            'report': report_link,
            'status': report.status,
        })
    return JsonResponse(results, safe=False)


def data_in_table(request, instrument_integration_filter, data_type, start_year, start_month, start_day,
                  end_year, end_month, end_day):
    """
    Dataview for handsontable on data in list
    :param request:
    :return:
    """
    # date ranges are also in the format of [A, B) - start is inclusive and end is exclusive
    # date filters are time agnostic, so must convert from tenant-specific timezone to UTC
    start_date = convert_tenant_timezone_to_utc(request, datetime.datetime(start_year, start_month, start_day))
    end_date = convert_tenant_timezone_to_utc(request,
                                              datetime.datetime(end_year, end_month, end_day) + datetime.timedelta(
                                                  days=1))

    data_list = models.DataIn.objects.order_by('-created_datetime').all().prefetch_related('instrument_integration', )
    if data_type == 'Unprocessed':
        data_list = data_list.filter(is_completed=False, is_queued=False)
    elif data_type == 'All':
        pass
    elif data_type == 'Processing':
        data_list = data_list.filter(is_completed=False, is_queued=True)
    else:
        data_list = data_list.filter(is_completed=True, is_queued=False)

    data_list = data_list.filter(created_datetime__gte=start_date, created_datetime__lt=end_date)

    if instrument_integration_filter != 'All':
        data_list = data_list.filter(instrument_integration__uuid=instrument_integration_filter)

    results = []

    for data in data_list:
        try:
            created_datetime = convert_utc_to_tenant_timezone(request, data.created_datetime).strftime(
                "%B %d, %Y %H:%M %Z")
        except AttributeError:
            created_datetime = None

        download_url = data.file.url
        download_link = format_html('<a href="{}" target="_blank"><i class="far fa-file-download"></i></a>',
                                    download_url)

        process_url = data.uuid
        process_link = format_html('<a href="{}" target="_blank"><i class="far fa-file-import"></i></a>', process_url)

        results.append({
            'status': data.status,
            'created_datetime': created_datetime,
            'original_file_name': data.original_file_name,
            'source': data.instrument_integration.internal_instrument_name,
            'download': download_link,
            'process': process_link,
        })

    return JsonResponse(results, safe=False)


def parse_result_file(data_in_uuid):
    """
    route to correct parsing function depending on instrument or test panel
    :param request:
    :param data_in_uuid:
    :return:
    """
    data_in = models.DataIn.objects.get(uuid=data_in_uuid)
    if data_in.instrument_integration.instrument.name == 'QuantStudio 12K Flex Real-Time PCR System':
        return parse_quantstudio_12k_result(data_in_uuid)
    elif data_in.instrument_integration.instrument.name == 'LCMS-8050 Triple Quadrupole Liquid Chromatograph Mass Spectrometer':
        return parse_shimadzu_8000_result(data_in_uuid)
    elif data_in.instrument_integration.instrument.name == 'LCMS-8040 Triple Quadrupole Liquid Chromatograph Mass Spectrometer':
        return parse_shimadzu_8000_result(data_in_uuid)
    elif data_in.instrument_integration.instrument.name == 'Ascent':
        return parse_ascent_result(data_in_uuid)
    else:
        return []


def parse_ascent_result(data_in_uuid):
    """
    function used to parse acsent mass spec data from Mako
    :param data_in_uuid:
    :return:
    """
    data_in = models.DataIn.objects.get(uuid=data_in_uuid)
    instrument_integration = data_in.instrument_integration
    positive_above_cutoff = instrument_integration.instrument.method.positive_above_cutoff
    result_dict = dict()
    cutoff_dict = dict()

    with data_in.file.open(mode="r") as csvfile:
        csv_reader = csv.reader(io.StringIO(csvfile.read().decode('utf-8', 'ignore')), delimiter=',')
        for row in csv_reader:
            if len(row) == 1 or row[0] == 'Batch':  # skip header rows
                continue
            elif row:
                sample_name = row[3]
                target_name = row[4]
                try:
                    result_value = decimal.Decimal(row[10])
                except decimal.InvalidOperation:
                    result_value = None

                try:
                    cutoff = cutoff_dict[target_name]
                except KeyError:
                    try:
                        calibration = instrument_integration.instrument_calibrations.get(target_name=target_name)
                        if calibration.use_test_cutoff_values:
                            if positive_above_cutoff:  # fault tolerance for cutoff not existing
                                cutoff = decimal.Decimal(
                                    calibration.test_type.test_type_ranges.filter(default=True).get().range_high)
                            else:
                                cutoff = decimal.Decimal(
                                    calibration.test_type.test_type_ranges.filter(default=True).get().range_low)
                        else:
                            cutoff = calibration.cutoff_value
                    except models.InstrumentCalibration.DoesNotExist:
                        cutoff = None
                    cutoff_dict[target_name] = cutoff

                try:
                    difference = result_value - cutoff
                except TypeError:
                    difference = None

                if difference is None and result_value is None and cutoff:
                    result_qualitative = 'Negative'
                elif difference is None and cutoff is None:
                    result_qualitative = 'Undetermined'
                elif difference < decimal.Decimal('0.00'):
                    if positive_above_cutoff:
                        result_qualitative = 'Negative'
                    else:
                        result_qualitative = 'Positive'
                elif difference > decimal.Decimal('0.00'):
                    if positive_above_cutoff:
                        result_qualitative = 'Positive'
                    else:
                        result_qualitative = 'Negative'
                elif difference == decimal.Decimal('0.00'):
                    result_qualitative = 'Equivocal'
                else:
                    result_qualitative = 'Undetermined'

                result = {
                    'sample_name': sample_name,
                    'target_name': target_name,
                    'result_value': result_value,
                    'cutoff': cutoff,
                    'difference': difference,
                    'result_qualitative': result_qualitative,
                    'message': row[9]

                }

                try:
                    result_dict[sample_name][target_name].append(result)
                except KeyError:
                    try:
                        result_dict[sample_name][target_name] = []
                        result_dict[sample_name][target_name].append(result)
                    except KeyError:
                        result_dict[sample_name] = {target_name: [result]}
    results = list(zip(result_dict.keys(), result_dict.values()))
    return results


def parse_shimadzu_8000_result(data_in_uuid):
    data_in = models.DataIn.objects.get(uuid=data_in_uuid)
    instrument_integration = data_in.instrument_integration
    positive_above_cutoff = instrument_integration.instrument.method.positive_above_cutoff
    result_dict = dict()
    cutoff_dict = dict()

    target_name = ''
    with data_in.file.open(mode="r") as csvfile:
        csv_reader = csv.reader(io.StringIO(csvfile.read().decode('utf-8', 'ignore')), delimiter=',')
        for row in csv_reader:
            if row:
                if row[0] == 'Name':  # row containing target_name
                    target_name = row[1].strip()
                elif row[0].isdigit():  # row containing data
                    sample_name = row[4].strip().replace(' ', '-')
                    try:
                        result_value = decimal.Decimal(row[16])
                    except decimal.InvalidOperation:
                        result_value = None

                    try:
                        cutoff = cutoff_dict[target_name]
                    except KeyError:
                        try:
                            calibration = instrument_integration.instrument_calibrations.get(target_name=target_name)
                            cutoff = calibration.cutoff_value
                            cutoff_dict[target_name] = cutoff
                        except models.InstrumentCalibration.DoesNotExist:
                            cutoff = None
                            cutoff_dict[target_name] = cutoff

                    try:
                        difference = result_value - cutoff
                    except TypeError:
                        difference = None

                    if difference is None and result_value is None and cutoff:
                        result_qualitative = 'Negative'
                    elif difference is None and cutoff is None:
                        result_qualitative = 'Undetermined'
                    elif difference < decimal.Decimal('0.00'):
                        if positive_above_cutoff:
                            result_qualitative = 'Negative'
                        else:
                            result_qualitative = 'Positive'
                    elif difference > decimal.Decimal('0.00'):
                        if positive_above_cutoff:
                            result_qualitative = 'Positive'
                        else:
                            result_qualitative = 'Negative'
                    elif difference == decimal.Decimal('0.00'):
                        result_qualitative = 'Equivocal'
                    else:
                        result_qualitative = 'Undetermined'

                    result = {
                        'sample_name': sample_name,
                        'target_name': target_name,
                        'result_value': result_value,
                        'cutoff': cutoff,
                        'difference': difference,
                        'result_qualitative': result_qualitative,
                        'message': row[9]

                    }

                    try:
                        result_dict[sample_name][target_name].append(result)
                    except KeyError:
                        try:
                            result_dict[sample_name][target_name] = []
                            result_dict[sample_name][target_name].append(result)
                        except KeyError:
                            result_dict[sample_name] = {target_name: [result]}

    results = list(zip(result_dict.keys(), result_dict.values()))
    return results


def parse_quantstudio_12k_result(data_in_uuid):
    data_in = models.DataIn.objects.get(uuid=data_in_uuid)
    instrument_integration = data_in.instrument_integration
    positive_above_cutoff = instrument_integration.instrument.method.positive_above_cutoff
    result_dict = dict()
    cutoff_dict = dict()

    with data_in.file.open(mode="r") as csvfile:
        csv_reader = csv.reader(io.StringIO(csvfile.read().decode('utf-8')), delimiter='	')
        for row in csv_reader:
            # data rows have 14 elements
            if len(row) == 14 and row[1] != '' and row[0] != 'Well':
                omit = row[2]
                if omit == 'FALSE':
                    sample_name = row[3]
                    target_name = row[4]
                    try:
                        result_value = decimal.Decimal(row[8])
                    except decimal.InvalidOperation:  # result may be 'Undetermined'
                        result_value = None

                    try:
                        cutoff = cutoff_dict[target_name]
                    except KeyError:
                        try:
                            calibration = instrument_integration.instrument_calibrations.get(target_name=target_name)
                            cutoff = calibration.cutoff_value
                            cutoff_dict[target_name] = cutoff
                        except models.InstrumentCalibration.DoesNotExist:
                            cutoff = decimal.Decimal('0.00')
                            cutoff_dict[target_name] = cutoff

                    try:
                        difference = result_value - cutoff
                    except TypeError:
                        difference = None

                    if difference is None:
                        result_qualitative = 'Undetermined'
                    elif difference < decimal.Decimal('0.00'):
                        if positive_above_cutoff:
                            result_qualitative = 'Negative'
                        else:
                            result_qualitative = 'Positive'
                    elif difference > decimal.Decimal('0.00'):
                        if positive_above_cutoff:
                            result_qualitative = 'Positive'
                        else:
                            result_qualitative = 'Negative'
                    elif difference == decimal.Decimal('0.00'):
                        result_qualitative = 'Equivocal'
                    else:
                        result_qualitative = 'Undetermined'

                    result = {
                        'sample_name': sample_name,
                        'target_name': target_name,
                        'result_value': result_value,
                        'cutoff': cutoff,
                        'difference': difference,
                        'result_qualitative': result_qualitative,
                        'well': row[0],
                        'well_position': row[1],
                        'crt': row[8],
                        'amp_score': row[9],
                        'crt_mean': row[10],
                        'crt_sd': row[11],
                        'noise': row[12],
                        'spike': row[13],
                        'message': '',
                    }

                    try:
                        result_dict[sample_name][target_name].append(result)
                    except KeyError:
                        try:
                            result_dict[sample_name][target_name] = []
                            result_dict[sample_name][target_name].append(result)
                        except KeyError:
                            result_dict[sample_name] = {target_name: [result]}

    results = list(zip(result_dict.keys(), result_dict.values()))
    return results


def observation_table(request, control_uuid, target_name_filter, start_year, start_month, start_day,
                      end_year, end_month, end_day, cutoff=DEFAULT_CUTOFF):
    """
    observations table dataview
    :param request:
    :param control_uuid: UUID
    :param target_name_filter: String
    :param start_year:
    :param start_month:
    :param start_day:
    :param end_year:
    :param end_month:
    :param end_day:
    :return:
    """
    control = models.Control.objects.get(uuid=control_uuid)
    observations = control.observations.all().prefetch_related('data_in__instrument_integration').\
        order_by('-observation_date')
    start_date = convert_tenant_timezone_to_utc(request, datetime.datetime(start_year, start_month, start_day))
    end_date = convert_tenant_timezone_to_utc(request,
                                              datetime.datetime(end_year, end_month, end_day) + datetime.timedelta(
                                                  days=1))

    if start_date:
        observations = observations.filter(observation_date__gte=start_date)
    if end_date:
        observations = observations.filter(observation_date__lte=end_date)
    if target_name_filter and target_name_filter is not '_':
        observations = observations.filter(target_name__icontains=target_name_filter)

    observations_total = observations.count()
    observations = observations[:cutoff]
    results = []

    for observation in observations:
        results.append(
            {
                'observation_date': convert_utc_to_tenant_timezone(request, observation.observation_date).strftime('%m-%d-%Y %H:%M %p'),
                'instrument': observation.instrument_integration.internal_instrument_name,
                'target_name': observation.target_name,
                'value': observation.result_value,
                'qualitative_result': observation.result_qualitative,
                'import_filename': observation.data_in.original_file_name,
            }
        )

    response = {
        'results': results,
        'observations_total': observations_total,
        'returned_count': observations.count()
    }
    return JsonResponse(response, safe=False)


def billing_messages_table(request, account_filter, start_year, start_month, start_day, end_year, end_month, end_day, cutoff=DEFAULT_CUTOFF):
    """
    Billing messages data view
    :param account_filter:
    :param request:
    :param start_year:
    :param start_month:
    :param start_day:
    :param end_year:
    :param end_month:
    :param end_day:
    :return:
    """
    start_date = convert_tenant_timezone_to_utc(request, datetime.datetime(start_year, start_month, start_day))
    end_date = convert_tenant_timezone_to_utc(request,
                                              datetime.datetime(end_year, end_month, end_day) + datetime.timedelta(
                                                  days=1))

    billing_messages = models.BillingMessage.objects.filter(created_date__gte=start_date, created_date__lt=end_date). \
        order_by('-created_date').prefetch_related('order', 'order__account', 'order__samples', 'order__tests')

    if account_filter != 'All':
        billing_messages = billing_messages.filter(order__account__uuid=account_filter)

    billing_messages_total = billing_messages.count()
    billing_messages = billing_messages[:cutoff]
    results = []

    for billing_message in billing_messages:
        order = billing_message.order
        # if account is active, show link, if not then don't show link
        if billing_message.order.account.is_active:
            account = {
                'text': billing_message.order.account.name,
                'url': '<a href="/accounts/{}">{}</a>'.format(order.account.uuid, order.account.name),
            }
        else:
            account = {
                'text': billing_message.order.account.name,
                'url': billing_message.order.account.name,
            }
        created_date = date_string_or_none(convert_utc_to_tenant_timezone(request, billing_message.created_date))

        s3 = boto3.client('s3',
                          aws_access_key_id=AWS_ACCESS_KEY_ID,
                          aws_secret_access_key=AWS_SECRET_ACCESS_KEY,
                          )
        if os.environ.get('DEBUG') == 'False' and not os.environ.get('STAGING') == 'True':  # production environment
            url = billing_message.hl7_message
            hl7_file_prefix = 'hl7_messages_processed/'
            url = str(url).replace('hl7_messages/', hl7_file_prefix)
            billing_message.hl7_message = url
            order_document_name = 'Mirth/Integrations/Billing Outbound/{}/requisition_forms/{}.pdf'.format(
                request.tenant.name,
                order.accession_number)
        elif os.environ.get('STAGING') == 'True':  # staging environment
            url = billing_message.hl7_message
            hl7_file_prefix = 'hl7_messages_processed/'
            url = str(url).replace('hl7_messages/', hl7_file_prefix)
            billing_message.hl7_message = url
            order_document_name = 'Mirth/Integrations/Billing Outbound/{}/requisition_forms/{}.pdf'.format(
                STAGING_ENVIRONMENT_TENANT,
                order.accession_number)
        else:  # dev
            order_document_name = 'Mirth/Integrations/Billing Outbound/{}/requisition_forms/{}.pdf'.format(DEV_ENVIRONMENT_TENANT,
                                                                                                order.accession_number)

        hl7_url = billing_message.hl7_message.url
        hl7_download_link = format_html('<a href="{}" target="_blank"><i class="far fa-file-download"></i></a>',
                                        hl7_url)

        order_document_url = functions.create_presigned_url(s3, SOURCE_BUCKET, order_document_name)
        order_document_download_link = format_html(
            '<a href="{}" target="_blank"><i class="far fa-file-download"></i></a>',
            order_document_url)

        status = 'Sent' if billing_message.is_sent else 'Not Sent'

        results.append({
            'order_code': {
                'text': billing_message.order.code,
                'url': '<a href="/orders/{}">{}</a>'.format(order.code, order.code),
            },
            'accession_number': order.accession_number,
            'account': account,
            'test_count': order.tests.count(),
            'sample_count': order.samples.count(),
            'created_date': created_date,
            'hl7_download_link': hl7_download_link,
            'order_document_download_link': order_document_download_link,
            'status': status,
        })

    response = {
        'results': results,
        'billing_messages_total': billing_messages_total,
        'returned_count': billing_messages.count()
    }
    return JsonResponse(response, safe=False)
