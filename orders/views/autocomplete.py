from dal import autocomplete
from orders import models


class OrderAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs = models.Order.objects.filter(submitted=True).order_by('-submitted_date')

        provider = self.forwarded.get('provider', None)
        if provider:
            qs = qs.filter(provider=provider)

        account = self.forwarded.get('account', None)
        if account:
            qs = qs.filter(account=account)

        if self.q:
            qs = qs.filter(code__icontains=self.q) | \
                 qs.filter(account__name__icontains=self.q) | \
                 qs.filter(samples__barcode__icontains=self.q[:-1]) | \
                 qs.filter(patient__user__last_name__icontains=self.q) | \
                 qs.filter(patient__user__first_name__icontains=self.q)
            qs = qs.distinct()
        return qs


class OrderAccessionNumberSwapAutocomplete(autocomplete.Select2QuerySetView):
    """
    Autocomplete view used to select order to swap Accession Number
    """
    def get_queryset(self):
        original_order = models.Order.objects.get(id=int(self.forwarded['self']))

        qs = models.Order.objects.filter(
            submitted=True,
            received_date__isnull=False,
            completed=False,
            received_date__date=original_order.received_date.date()
        ).order_by('-accession_number', '-received_date')

        if self.q:
            qs = qs.filter(code__icontains=self.q) | \
                 qs.filter(accession_number__icontains=self.q)
            qs = qs.distinct()
        return qs

    def get_result_label(self, item):
        if item.accession_number:
            return "{} (Order Code {})".format(item.accession_number, item.code, )
        else:
            return "Order Code {}".format(item.code)


class UnreceivedOrderAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs = models.Order.objects.filter(submitted=True,
                                         received_date__isnull=True).order_by('-submitted_date')
        if self.q:
            qs = qs.filter(code__icontains=self.q) | \
                 qs.filter(account__name__icontains=self.q) | \
                 qs.filter(samples__barcode__icontains=self.q[:-1]) | \
                 qs.filter(patient__user__last_name__icontains=self.q) | \
                 qs.filter(patient__user__first_name__icontains=self.q)
            qs = qs.distinct()
        return qs


class TestTypeAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs = models.TestType.objects.all()

        if self.q:
            qs = qs.filter(name__istartswith=self.q)
        return qs


class OrderTestCreateTestTypeAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        order = self.forwarded.get('order', None)

        # limit displayed test types to ones that are not already on the order
        if order:
            order = models.Order.objects.prefetch_related('tests').get(id=order)
            order_tests = order.tests.prefetch_related('test_type').all()
            order_test_types = [test.test_type.id for test in order_tests]

            qs = models.TestType.objects.exclude(id__in=order_test_types)
        else:
            qs = qs.none()

        if self.q:
            qs = qs.filter(name__istartswith=self.q)
        return qs


class TestTypeCategoryAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs = models.TestTypeCategory.objects.all()

        if self.q:
            qs = qs.filter(name__istartswith=self.q)
        return qs


class OrderSampleAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        order = self.forwarded.get('order', None)
        qs = models.Sample.objects.all()
        if order:
            qs = qs.filter(order=order)
        else:
            qs = qs.none()

        if self.q:
            qs = qs.filter(code__icontains=self.q)
        return qs


class OrderTestTypeAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        order = self.forwarded.get('order', None)
        qs = models.TestType.objects.none()
        if order:
            order = models.Order.objects.prefetch_related('test_panels').get(id=order)
            test_panels = order.test_panels.all()
            for test_panel in test_panels:
                qs = qs | test_panel.test_panel_type.test_types
        else:
            qs = qs.none()

        if self.q:
            qs = qs.filter(test_type__name__icontains=self.q)
        qs = qs.distinct()
        return qs


class SampleAutocomplete(autocomplete.Select2QuerySetView):
    """
    shows pending samples
    """
    def get_queryset(self):
        test_panel_type = self.forwarded.get('test_panel_type', None)
        qs = models.Sample.objects.all()
        if test_panel_type:
            qs = models.Sample.objects.filter(order__test_panels__test_panel_type=test_panel_type)

        if self.q:
            qs = qs.filter(code__icontains=self.q)
        qs = qs.distinct().order_by('-id')
        return qs


class TestPanelTypeAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs = models.TestPanelType.objects.all()

        if self.q:
            qs = qs.filter(name__icontains=self.q)
        qs = qs.distinct()
        return qs


class TestPanelTypeToExcludeAutocomplete(autocomplete.Select2QuerySetView):
    """
    Used to exclude Test Panels from specific accounts
    """
    def get_queryset(self):
        qs = models.TestPanelType.objects.filter(exclude_from_menu=False).all()

        if self.q:
            qs = qs.filter(name__icontains=self.q)
        qs = qs.distinct()
        return qs


class TestPanelTypeCategoryAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs = models.TestPanelCategory.objects.all()

        if self.q:
            qs = qs.filter(name__icontains=self.q)
        qs = qs.distinct()
        return qs


class ControlAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs = models.Control.objects.all()

        if self.q:
            qs = qs.filter(name__icontains=self.q) | qs.filter(code__icontains=self.q)
        qs = qs.distinct()
        return qs

    def get_result_label(self, item):
        return "{}: {}".format(item.code, item.name)


class ContainerTypeAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs = models.ContainerType.objects.all()

        if self.q:
            qs = qs.filter(name__icontains=self.q)
        return qs
