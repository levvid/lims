import django
import logging
import os
import boto3

os.environ['DJANGO_SETTINGS_MODULE'] = 'main.settings'
django.setup()

from clients import models


def main():
    print('Beginning conversion...')

    # check to see if production db
    production_env = models.Client.objects.filter(domain_url='dendisoftware.com')
    print(production_env)

    if production_env:
        # replace all URLs to staging URLs instead of dendisoftware.com URLs
        for client in models.Client.objects.all():
            client.domain_url = client.domain_url.replace('dendisoftware.com', 'dendi-staging.com')
            print(client.domain_url)
            client.save()
    else:
        logging.info('This script was not launched in a production environment. Please try again.')


def start_replication_task():

    """
    If we want to automate the production-to-staging data migration...not completed quite yet
    :return:
    """

    client = boto3.client('dms',
                          aws_access_key_id=os.environ.get('S3_ACCESS_KEY'),
                          aws_secret_access_key=os.environ.get('S3_SECRET_KEY'),
                          region_name='us-east-1')

    # restarts the AWS DMS replication task
    # https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/dms.html#DatabaseMigrationService.Client.start_replication_task
    response = client.refresh_schemas(
        # ARN is given by the DMS task details
        ReplicationTaskArn='arn:aws:dms:us-east-1:410812144272:task:PXBCXGHJXGRHRNYHLSEH2ZIYNQ',
        StartReplicationTaskType='start-replication',
    )
    return response


if __name__ == '__main__':
    print('Starting script...')
    main()
    # start_replication_task()

