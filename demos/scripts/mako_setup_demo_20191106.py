import logging
import sys

from ..scripts.mako_setup_20191106 import main

if __name__ == '__main__':
    """
    Calls the mako setup script with demo=True
    """
    logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)
    main(demo=True)
