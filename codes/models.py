import uuid as uuid_lib

from django.db import models

from lab_tenant.models import AuditLogModel
from auditlog.registry import auditlog
from auditlog.models import AuditlogHistoryField


# Create your models here.
class ICD10CM(AuditLogModel):
    uuid = models.UUIDField(
        db_index=True,
        default=uuid_lib.uuid4,
        editable=False
    )
    category = models.CharField(max_length=64, null=True)
    diagnosis_code = models.CharField(max_length=64, null=True)
    full_code = models.CharField(max_length=64, null=True)
    abbreviated_description = models.CharField(max_length=255, null=True)
    full_description = models.CharField(max_length=255, null=True)
    category_title = models.CharField(max_length=255, null=True)
    is_expired = models.BooleanField(default=False, db_index=True)

    class Meta:
        db_table = 'icd10cm'

    def __str__(self):
        if len(self.full_code) > 3:  # add period to codes
            return'{}.{} {}'.format(self.full_code[:3], self.full_code[3:], self.abbreviated_description)
        else:
            return "{} {}".format(self.full_code, self.abbreviated_description)

    @property
    def code_with_period(self):
        if len(self.full_code) > 3:  # add period to codes
            return'{}.{}'.format(self.full_code[:3], self.full_code[3:])
        else:
            return self.full_code


class ProcedureCodeType(AuditLogModel):
    name = models.CharField(max_length=32, unique=True, db_index=True)
    description = models.CharField(max_length=512, null=True)


class ProcedureCode(AuditLogModel):
    code = models.CharField(max_length=32, unique=True, db_index=True)
    description = models.TextField()
    icd10_codes = models.ManyToManyField(ICD10CM, related_name='procedure_codes')
    expired = models.BooleanField(default=False)
    expired_date = models.DateField(null=True)
    procedure_code_type = models.ForeignKey(
        ProcedureCodeType,
        on_delete=models.SET_NULL,
        null=True,
        related_name='procedure_codes')

    def __str__(self):
        cutoff = 64
        if len(self.description) > cutoff:
            return "{} - {}".format(self.code, self.description[:cutoff])
        else:
            return "{} - {}...".format(self.code, self.description[:cutoff])

######################################################################################################################
# Django AuditLogs

auditlog.register(ICD10CM)
auditlog.register(ProcedureCode)
auditlog.register(ProcedureCodeType)
