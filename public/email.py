import os
import sendgrid
from sendgrid.helpers.mail import Email, Content, Mail

# import environment variables from dotenv
from dotenv import load_dotenv
# environment variables saved before project root directory in local environment
dotenv_path = '.env'
load_dotenv(dotenv_path)


def send_email_sendgrid(from_email,
                        to_email,
                        subject,
                        content,
                        attachment=False):
    sg = sendgrid.SendGridAPIClient(apikey=os.environ.get('SENDGRID_API_KEY'))
    from_email = Email(from_email)
    to_email = Email(to_email)
    subject = subject
    content = Content('text/html', content)
    mail = Mail(from_email, subject, to_email, content)
    if attachment:
        mail.add_attachment(attachment)
    response = sg.client.mail.send.post(request_body=mail.get())

    return response