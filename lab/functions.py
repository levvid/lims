import datetime
import decimal

from main.functions import convert_utc_to_tenant_timezone, convert_tenant_timezone_to_utc


def galaxy_ifa_parse(form_result, result_info_label, result_info_label_prefix):
    """
    Customized for Galaxy Diagnostics. Not meant for general use
    :param result_label: IFA titer value
    :param result_label_prefix: prefix (lte, lt, gte, gt)
    :return: result_value

    result --> result value

    Galaxy: A reactive titer is anything greater than or equal to 1:64.
    We report out 6 titers: <1:32, 1:32, 1:64, 1:128, 1:256, ≥1:256.

    """
    if form_result:
        return form_result
    elif result_info_label:
        try:
            titer_value = int(result_info_label.split(':')[-1])
            if titer_value >= 64:
                result_value = '+'
            else:
                result_value = '-'
            return result_value
        except ValueError:
            return None
