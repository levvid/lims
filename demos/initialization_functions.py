import logging
import uuid

from clients import models as client_models
from orders import models as orders_models
from accounts import models as accounts_models
from lab import models as lab_models
from lab_tenant import models as lab_tenant_models
from patients import models as patients_models

"""
These functions should be used under tenant_context

Soon to be deprecated by import_functions
"""


def create_providers(provider_dict_list):
    """
    create providers given a list of dictionaries with the keys: first_name, last_name, email, npi
    :param provider_dict_list: [dict()]
    :return:
    """
    for provider_dict in provider_dict_list:
        user_uuid = str(uuid.uuid4())
        alternate_id = provider_dict.get('alternate_id', '')
        first_name = provider_dict.get('first_name')
        last_name = provider_dict.get('last_name')
        provider_email = provider_dict.get('email')
        provider_npi = provider_dict.get('npi')
        provider_title = provider_dict.get('title', '')
        username = provider_email
        user, created = lab_tenant_models.User.objects.update_or_create(first_name=first_name,
                                                                        last_name=last_name,
                                                                        user_type=10,
                                                                        email=provider_email,
                                                                        defaults={
                                                                            'username': username
                                                                        })
        user.set_password(str(user_uuid))
        user.save()

        # create providers
        provider, created = accounts_models.Provider.objects.get_or_create(alternate_id=alternate_id,
                                                                           npi=provider_npi,
                                                                           user=user,
                                                                           title=provider_title,
                                                                           defaults={
                                                                             'uuid': user_uuid,
                                                                           })
        if created:
            logging.debug('Provider ' + str(provider) + ' created.')


def create_users(user_dict_list):
    """
    create users given a list of dictionaries with the keys: last_name, first_name, username, email, user_type (Int)
    :param user_dict_list:
    :return:
    """
    for user_dict in user_dict_list:
        user_obj = lab_tenant_models.User(last_name=user_dict['last_name'],
                                          first_name=user_dict['first_name'],
                                          username=user_dict['email'],
                                          email=user_dict['email'],
                                          user_type=user_dict['user_type'])
        # save User object first
        user_obj.set_password(user_dict['password'])
        user_obj.save()
        lab_admin_obj = lab_tenant_models.LabAdmin(user=user_obj)
        lab_admin_obj.save()


def create_accounts(account_dict_list):
    """
    create accounts given a list of dictionaries with keys: name, address1
    :param account_dict_list:
    :return:
    """
    for account_dict in account_dict_list:
        account, created = accounts_models.Account.objects.update_or_create(
            alternate_id=account_dict.get('alternate_id', ''),
            name=account_dict.get('name'),
            address1=account_dict.get('address1'),
            phone_number=account_dict.get('phone_number', ''),
            fax_number=account_dict.get('fax_number', ''),
            email=account_dict.get('email', '')
        )
        if account_dict.get('username', '') and created:
            user_obj = lab_tenant_models.User(
                user_type=9,
                first_name=account_dict.get('name')[:30],
                username=account_dict.get('username'),
                email=account_dict.get('email', ''),
            )
            user_obj.set_password(account_dict.get('password'))
            user_obj.save()
            account.user = user_obj
            account.save(update_fields=['user'])
        print(account_dict.get('name') + ' created.')


def create_patients(patient_dict_list):
    """

    :param patient_dict_list:
    :return:
    """
    for patient_dict in patient_dict_list:
        user_uuid = str(uuid.uuid4())
        alternate_id = patient_dict.get('alternate_id', '')
        first_name = patient_dict['first_name']
        last_name = patient_dict['last_name']
        username = first_name + '-' + last_name + '-' + user_uuid
        user, created = lab_tenant_models.User.objects.update_or_create(first_name=first_name,
                                                                        last_name=last_name,
                                                                        user_type=20,
                                                                        defaults={
                                                                            'username': username
                                                                        })
        patients_models.Patient.objects.update_or_create(alternate_id=alternate_id,
                                                         birth_date=patient_dict['birth_date'],
                                                         sex=patient_dict['sex'],
                                                         user=user,
                                                         )


def create_instruments(instrument_dict_list):
    """

    :param instrument_dict_list:
    :return:
    """
    for instrument_dict in instrument_dict_list:
        methodology = lab_models.TestMethod.objects.get(name=instrument_dict['method'])
        instrument, created = lab_models.Instrument.objects.update_or_create(
            name=instrument_dict['name'],
            manufacturer=instrument_dict['manufacturer'],
            method=methodology,
            defaults={
                'unit_of_measurement': instrument_dict.get('unit_of_measurement', '')
            }
        )


def create_instrument_integrations(instrument_integration_dict_list):
    for instrument_integration_dict in instrument_integration_dict_list:
        instrument_integration, created = lab_tenant_models.InstrumentIntegration.objects.update_or_create(
            internal_instrument_name=instrument_integration_dict['internal_instrument_name'],
            serial_number=instrument_integration_dict['serial_number'],
            instrument=lab_models.Instrument.objects.get(name=instrument_integration_dict['instrument'])
        )


def create_controls(control_dict_list):
    for control_dict in control_dict_list:
        orders_models.Control.objects.create(
            name=control_dict['name'],
            code=control_dict['code']
        )


def create_data_in_demo(data_in_dict_list):
    for data_in_dict in data_in_dict_list:
        orders_models.DataIn.objects.create(
            instrument_integration_id=data_in_dict['instrument_integration_id'],
            original_file_name=data_in_dict['original_file_name'],
            file=data_in_dict['file'],
            created_datetime=data_in_dict['created_datetime']
        )


def create_test_types(test_type_dict_list, create_panel=False, tox=False):
    """

    :param test_type_dict_list:
    :param create_panel:
    :param tox: if test_target needs to be created, is_tox_target will be assigned as tox
    :return:
    """
    test_types = []
    offsite_laboratory_dict = dict()
    for test_type_dict in test_type_dict_list:
        required_samples = {lab_models.CLIASampleType.objects.get(name=key).id: value for (key, value) in
                            test_type_dict['required_samples'].items()}
        try:
            test_target = lab_models.TestTarget.objects.get(name=test_type_dict['test_target'])
        except lab_models.TestTarget.DoesNotExist:
            logging.debug("{} test target does not exist".format(test_type_dict['test_target']))
            raise

        # offsite laboratory
        offsite_laboratory_name = test_type_dict.get('offsite', None)
        if offsite_laboratory_name:
            try:
                offsite_laboratory_id = offsite_laboratory_dict[offsite_laboratory_name]
            except KeyError:
                offsite_laboratory_dict[offsite_laboratory_name] = lab_models.OffsiteLaboratory.objects.get(name=offsite_laboratory_name).id
                offsite_laboratory_id = offsite_laboratory_dict[offsite_laboratory_name]
        else:
            offsite_laboratory_id = None

        test_type = orders_models.TestType(
            name=test_type_dict['name'],
            test_method=lab_models.TestMethod.objects.get(name=test_type_dict['test_method']),
            specialty=lab_models.CLIATestTypeSpecialty.objects.get(specialty_name=test_type_dict['specialty']),
            test_target=test_target,
            result_type=test_type_dict['result_type'],
            required_samples=required_samples,
            unit=test_type_dict.get('unit_of_measurement', ''),
            round_figures=test_type_dict.get('round_figures', 2),
            alternate_id=test_type_dict.get('code', None),
            transaction_code=test_type_dict.get('transaction_code', ''),
            offsite_laboratory_id=offsite_laboratory_id
        )
        test_types.append(test_type)
    test_types = orders_models.TestType.objects.bulk_create(test_types)

    if create_panel:
        for test_type_dict, test_type in zip(test_type_dict_list, test_types):
            test_profile_category = orders_models.TestProfileCategory.objects.get(name=test_type_dict['group'])
            test_panel_type = orders_models.TestPanelType.objects.create(
                name=test_type_dict['name'],
                test_profile_category=test_profile_category
            )
            test_panel_type.test_types.add(test_type)

            group_panel_type = orders_models.TestPanelType.objects.get(name=test_type_dict['group'])
            group_panel_type.test_types.add(test_type)
    return test_types


def create_test_type_ranges(test_type_range_dict_list):
    for test_type_range_dict in test_type_range_dict_list:
        test_type = orders_models.TestType.objects.get(name=test_type_range_dict['name'])
        orders_models.TestTypeRange.objects.create(
            test_type=test_type,
            default=test_type_range_dict['default'],
            range_low=test_type_range_dict['range_low'],
            range_high=test_type_range_dict['range_high'],
        )


def create_test_targets(test_target_dict_list):
    for test_target_dict in test_target_dict_list:
        lab_models.TestTarget.objects.update_or_create(
            name=test_target_dict['name'],
            is_tox_target=test_target_dict['is_tox_target']
        )


def increment_schema_name(schema_name):
    client = client_models.Client.objects.filter(schema_name__istartswith=schema_name).order_by('-id').first()
    numerical_characters = [x for x in client.schema_name if x.isdigit()]
    if numerical_characters:
        number = str(1 + int(''.join(numerical_characters)))
    else:
        number = '0'
    schema_name = str(''.join([x for x in schema_name if not x.isdigit()])) + number
    return schema_name


def get_unique_schema_name(schema_name):
    """
    check if schema_name is available, if not, return incremented schema_name
    :param schema_name:
    :return: schema_name
    """
    try:
        tenant = client_models.Client.objects.get(schema_name=schema_name)
        schema_name = increment_schema_name(schema_name)
        return schema_name
    except client_models.Client.DoesNotExist:
        return schema_name