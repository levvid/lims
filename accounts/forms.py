from django import forms
from django.core.validators import validate_email
from django.core.exceptions import ValidationError
from django.template.defaultfilters import slugify
from dal import autocomplete, forward

from . import models
from lab_tenant import models as lab_tenant_models
from orders import models as orders_models

ACCOUNT_TYPES = (('Existing Account', 'Existing Account'), ('New Account', 'New Account'),)
PROVIDER_TYPES = (('Existing Provider', 'Existing Provider'), ('New Provider', 'New Provider'),)


class AccountForm(forms.ModelForm):
    class Meta:
        model = models.Account
        fields = []

    providers = forms.ModelMultipleChoiceField(
        queryset=models.Provider.objects.all(),
        widget=autocomplete.ModelSelect2Multiple(url='all_provider_autocomplete'),
        required=False,
    )

    account_name = forms.CharField(max_length=255, label='Name', required=True,
                                   error_messages={'required': 'Name is required.'})
    aliases = forms.CharField(label='Aliases (separate multiple with a new line)',
                              required=False,
                              widget=forms.Textarea(attrs={'class': 'form-control'}))
    alternate_id = forms.CharField(max_length=32, label='Alternate ID', required=False)
    account_address1 = forms.CharField(max_length=255, label='Address', required=True,
                                       error_messages={'required': 'Address is required.'})
    account_address2 = forms.CharField(max_length=255, label='Apartment, Suite, Unit', required=False)
    account_zip_code = forms.CharField(max_length=128, label='Zip Code', required=False)
    contact_person = forms.CharField(max_length=255, label='Contact Person', required=False)
    account_phone = forms.CharField(max_length=255, label='Phone', required=False)
    account_fax = forms.CharField(max_length=255, label='Fax', required=False)
    account_email = forms.CharField(max_length=255, label='Email', required=False,
                                    widget=forms.EmailInput(
                                        attrs={'class': 'form-control'})
                                    )
    account_username = forms.CharField(max_length=255, label='Username', required=False,
                                       widget=forms.TextInput(
                                          attrs={'class': 'form-control'}
                                       ))
    account_password = forms.CharField(widget=forms.PasswordInput(attrs={'class': 'form-control'}),
                                       label='Set Password',
                                       required=False)
    account_password_confirm = forms.CharField(widget=forms.PasswordInput(attrs={'class': 'form-control'}),
                                               label='Confirm Password',
                                               required=False)
    report_preference = forms.ChoiceField(label='Report Preference',
                                          choices=(('Partial Reporting', 'Partial Reporting'),
                                                   ('Final Reporting Only', 'Final Reporting Only')),
                                          required=True,
                                          widget=forms.Select(attrs={'class': 'form-control'}))
    account_manages_signatures = forms.BooleanField(initial=False,
                                                    label='Account Manages Signatures on File',
                                                    widget=forms.CheckboxInput(attrs={'data-toggle': 'toggle'}),
                                                    required=False)
    notes = forms.CharField(label='Internal Notes', widget=forms.Textarea(
        attrs={'cols': '80',
               'rows': '5'}
    ), required=False)
    results_out_show_pdf_link = forms.BooleanField(label='Link of PDF in HL7 Message', required=False)
    emr_account_number = forms.CharField(max_length=255, label='EMR Account Number', required=False,
                                         widget=forms.TextInput(
                                          attrs={'class': 'form-control'}
                                      ))

    def clean_account_name(self):
        account_name = self.cleaned_data.get('account_name')

        matching_account_name = models.Account.objects.filter(name=account_name)

        if self.instance:
            matching_account_name = matching_account_name.exclude(name=self.instance.name)
            if matching_account_name:
                self.add_error('account_name', forms.ValidationError('Account name already exists.'))
        return account_name

    def clean_account_username(self):
        account_username = self.cleaned_data.get('account_username')
        # no special characters unless email
        try:
            validate_email(account_username)
        except ValidationError as e:
            account_username = slugify(account_username)

        if account_username:
            # case-sensitivity: usernames should always be saved as lowercase
            # otherwise does not throw IntegrityError on duplicate but case-different usernames
            account_username = account_username.lower()
            matching_account_username = lab_tenant_models.User.objects.filter(username__iexact=account_username)

            if self.instance.user:
                matching_account_username = matching_account_username.exclude(id=self.instance.user.id)
            if matching_account_username:
                self.add_error('account_username', forms.ValidationError(
                    'This username already exists in our system.'))
        return account_username

    def clean_aliases(self):
        aliases = self.cleaned_data.get('aliases').splitlines()
        aliases = [alias.strip() for alias in aliases]

        lower_case_aliases = set([alias.lower() for alias in aliases])
        if len(aliases) != len(lower_case_aliases):
            self.add_error('aliases', forms.ValidationError('Please make sure there are no duplicates in the list.'))

        matching_aliases = models.AccountAlias.objects.filter(name__in=aliases)

        if self.instance and aliases:
            matching_aliases = matching_aliases.exclude(account=self.instance)
            if matching_aliases:
                for alias in matching_aliases:
                    self.add_error('aliases', forms.ValidationError('This alias already exists: {}'.format(str(alias))))
        return aliases

    def clean(self):
        account_password = self.cleaned_data['account_password']
        account_password_confirm = self.cleaned_data['account_password_confirm']

        if account_password and not account_password_confirm:
            self.add_error('account_password_confirm', forms.ValidationError('Please confirm your password.'))
        elif account_password and account_password_confirm:
            if account_password != account_password_confirm:
                self.add_error('account_password', forms.ValidationError('Both passwords should match.'))


class ProviderForm(forms.ModelForm):
    class Meta:
        model = models.Provider
        fields = []

    accounts = forms.ModelMultipleChoiceField(
        queryset=models.Account.objects.all(),
        widget=autocomplete.ModelSelect2Multiple(url='account_autocomplete'),
        required=True,
    )
    country = forms.ChoiceField(choices=(('US', 'US'),
                                         ('Other', 'Other')),
                                widget=forms.RadioSelect(),
                                initial='US',
                                label="Provider Country")

    first_name = forms.CharField(max_length=255, label='First Name', required=True,
                                 error_messages={'required': 'First name is required.'})
    last_name = forms.CharField(max_length=255, label='Last Name', required=True,
                                error_messages={'required': 'Last name is required.'})
    npi = forms.CharField(max_length=255, label='NPI', required=False)
    provider_id_num = forms.CharField(max_length=255, label='Identification Number', required=False)
    provider_suffix = forms.CharField(max_length=255, label='Suffix (Jr, II)', required=False)
    provider_title = forms.CharField(max_length=255, label='Title (MD, RN)', required=False)
    email = forms.EmailField(widget=forms.EmailInput(
                             attrs={'class': 'form-control'}),
                             required=False)
    phone_number = forms.CharField(max_length=255, label='Phone Number', required=False)

    provider_username = forms.CharField(max_length=255, label='Username', required=False,
                                        widget=forms.TextInput(
                                            attrs={'class': 'form-control'}
                                        ))
    provider_password = forms.CharField(widget=forms.PasswordInput(attrs={'class': 'form-control'}),
                                        label='Set Password',
                                        required=False)
    provider_password_confirm = forms.CharField(widget=forms.PasswordInput(attrs={'class': 'form-control'}),
                                                label='Confirm Password',
                                                required=False)

    def clean_npi(self):
        npi = self.cleaned_data.get('npi')

        if npi:
            # clean NPI
            npi = npi.replace('_', '')
            if not len(npi) == 10:
                self.add_error('npi', forms.ValidationError('NPI must be exactly 10 digits.'))

            matching_npi = models.Provider.objects.filter(npi=npi)

            if self.instance:
                # if update
                matching_npi = matching_npi.exclude(npi=self.instance.npi)
                if matching_npi:
                    self.add_error('npi', forms.ValidationError('A provider with this NPI already exists.'))
        return npi

    def clean_provider_username(self):
        provider_username = self.cleaned_data.get('provider_username')
        # no special characters unless email
        try:
            validate_email(provider_username)
        except ValidationError as e:
            provider_username = slugify(provider_username)

        if provider_username:
            # case-sensitivity: usernames should always be saved as lowercase
            # otherwise does not throw IntegrityError on duplicate but case-different usernames
            provider_username = provider_username.lower()
            matching_provider_username = lab_tenant_models.User.objects.filter(username__iexact=provider_username)

            if self.instance.user:
                matching_provider_username = matching_provider_username.exclude(id=self.instance.user.id)
            if matching_provider_username:
                self.add_error('provider_username', forms.ValidationError(
                    'This username already exists in our system.'))
        return provider_username

    def clean(self):
        provider_password = self.cleaned_data['provider_password']
        provider_password_confirm = self.cleaned_data['provider_password_confirm']

        if provider_password and not provider_password_confirm:
            self.add_error('provider_password_confirm', forms.ValidationError('Please confirm your password.'))
        elif provider_password and provider_password_confirm:
            if provider_password != provider_password_confirm:
                self.add_error('provider_password', forms.ValidationError('Both passwords should match.'))

        if self.cleaned_data.get('country') == 'US':
            if not self.cleaned_data.get('npi'):
                self.cleaned_data['provider_id_num'] = ''
                self.add_error('npi', forms.ValidationError('NPI is required.'))
        else:
            self.cleaned_data['npi'] = ''


class CollectorForm(forms.ModelForm):
    class Meta:
        model = models.Collector
        fields = []

    accounts = forms.ModelMultipleChoiceField(
        queryset=models.Account.objects.all(),
        widget=autocomplete.ModelSelect2Multiple(url='collector_account_autocomplete'),
        required=True,
        label='Accounts',
    )
    first_name = forms.CharField(
        max_length=255, label='First Name', required=True,
        error_messages={'required': 'First name is required.'})
    last_name = forms.CharField(
        max_length=255, label='Last Name', required=True,
        error_messages={'required': 'Last name is required.'})
    email = forms.EmailField(widget=forms.EmailInput(
        attrs={'class': 'form-control'}),
        required=False)
    username = forms.CharField(
        max_length=255, label='Username', required=False,
        widget=forms.TextInput(
            attrs={'class': 'form-control'}
        ))
    password = forms.CharField(
        widget=forms.PasswordInput(attrs={'class': 'form-control'}),
        label='Set Password',
        required=False)
    password_confirm = forms.CharField(
        widget=forms.PasswordInput(attrs={'class': 'form-control'}),
        label='Confirm Password',
        required=False)

    def clean_username(self):
        username = self.cleaned_data.get('username')
        # no special characters unless email
        try:
            validate_email(username)
        except ValidationError as e:
            username = slugify(username)

        if username:
            # case-sensitivity: usernames should always be saved as lowercase
            # otherwise does not throw IntegrityError on duplicate but case-different usernames
            username = username.lower()
            matching_username = lab_tenant_models.User.objects.filter(username__iexact=username)
            if self.instance.user:
                matching_username = matching_username.exclude(id=self.instance.user.id)
            if matching_username:
                self.add_error('username', forms.ValidationError(
                    'This username already exists in our system.'))
        return username

    def clean(self):
        password = self.cleaned_data['password']
        password_confirm = self.cleaned_data['password_confirm']

        if password and not password_confirm:
            self.add_error('password_confirm', forms.ValidationError('Please confirm your password.'))
        elif password and password_confirm:
            if password != password_confirm:
                self.add_error('password', forms.ValidationError('Both passwords should match.'))


class OrderAccountForm(forms.Form):

    account_type = forms.ChoiceField(choices=ACCOUNT_TYPES, widget=forms.RadioSelect(), initial=ACCOUNT_TYPES[0])
    existing_account = forms.ModelChoiceField(
        queryset=models.Account.objects.all(),
        widget=autocomplete.ModelSelect2(url='account_autocomplete'),
        required=False,
    )
    account_name = forms.CharField(max_length=255, label='Account Name', required=False)
    account_address1 = forms.CharField(max_length=255, label='Mailing Address', required=False)
    account_address2 = forms.CharField(max_length=255, label='Apartment, Suite, Unit', required=False)
    account_zip_code = forms.CharField(max_length=128, label='Zip Code', required=False)
    contact_person = forms.CharField(max_length=255, label='Contact Person', required=False)
    account_phone = forms.CharField(max_length=255, label='Phone', required=False)
    account_fax = forms.CharField(max_length=255, label='Fax', required=False)
    account_email = forms.EmailField(max_length=255, label='Email', required=False,
                                     widget=forms.EmailInput(
                                        attrs={'class': 'form-control'})
                                     )

    def clean(self):
        cleaned_data = super(OrderAccountForm, self).clean()
        account_type = self.cleaned_data['account_type']
        existing_account = self.cleaned_data['existing_account']
        account_name = self.cleaned_data['account_name']

        # if user selects "existing account" and one isn't chosen
        if account_type == 'Existing Account' and not existing_account:
            self.add_error('existing_account', forms.ValidationError('Please select an existing account.'))

        if account_type == 'New Account':
            if not account_name:
                self.add_error('account_name', forms.ValidationError('Account name is required.'))

        return cleaned_data


class OrderProviderForm(forms.Form):

    provider_type = forms.ChoiceField(choices=PROVIDER_TYPES, widget=forms.RadioSelect(), initial=PROVIDER_TYPES[0])
    existing_provider = forms.ModelChoiceField(
        queryset=models.Provider.objects.all(),
        widget=autocomplete.ModelSelect2(
            url='provider_order_autocomplete',
            forward=(forward.Field('account', 'account'),),
        ),
        required=False,
        label='Existing Provider'
    )
    account = forms.ModelChoiceField(
        queryset=models.Account.objects.all(),
        widget=autocomplete.ModelSelect2(url='account_autocomplete'),
    )
    first_name = forms.CharField(max_length=255, label='First Name', required=False)
    last_name = forms.CharField(max_length=255, label='Last Name', required=False)
    suffix = forms.CharField(max_length=255, label='Suffix (Jr, II)', required=False)
    title = forms.CharField(max_length=255, label='Title (MD, RN)', required=False)
    npi = forms.CharField(max_length=255, label='NPI', required=False)
    email = forms.EmailField(widget=forms.EmailField(widget=forms.EmailInput(attrs={'class': 'form-control'})))
    phone_number = forms.CharField(max_length=255, label='Phone Number', required=False)


class CriticalContactForm(forms.Form):
    first_name = forms.CharField(
        max_length=255, label='First Name', required=True,
        error_messages={'required': 'First name is required.'})
    last_name = forms.CharField(
        max_length=255, label='Last Name', required=True,
        error_messages={'required': 'Last name is required.'})
    phone_number = forms.CharField(max_length=255, label='Phone', required=False)
