from django.utils import timezone

CONTROLS = [
    {
        'name': 'Control 1',
        'code': 'C-001',
    },
    {
        'name': 'Control 2',
        'code': 'C-002',
    },
]
DATA_IN_LIST = [
    {
        'instrument_integration_id': 1,
        'original_file_name': 'quantstudio_01-56.txt',
        'file': 'instrument_output_debug/demo/quantstudio_01-56.txt',
        'created_datetime': timezone.now(),
    },
    {
        'instrument_integration_id': 1,
        'original_file_name': 'quantstudio_03-56.txt',
        'file': 'instrument_output_debug/demo/quantstudio_03-56.txt',
        'created_datetime': timezone.now(),
    }
]

INSTRUMENT_INTEGRATIONS = [
    {
        'internal_instrument_name': 'QuantStudio 12K',
        'serial_number': 'aiwthn23sriatk',
        'instrument': 'QuantStudio 12K Flex Real-Time PCR System',
    }
]

TEST_TYPES = [
    {
        'name': 'Escherichia coli - Urinary Tract Microbiota TaqMan™ Assay',
        'test_method': 'qPCR',
        'specialty': 'Bacteriology',
        'test_target': 'Escherichia coli',
        'result_type': 'Qualitative',
        'required_samples': {'Urine': 1}
    },
    {
        'name': 'Proteus mirabilis - Urinary Tract Microbiota TaqMan™ Assay',
        'test_method': 'qPCR',
        'specialty': 'Bacteriology',
        'test_target': 'Proteus mirabilis',
        'result_type': 'Qualitative',
        'required_samples': {'Urine': 1}
    },
    {
        'name': 'Enterococcus faecalis - Urinary Tract Microbiota TaqMan™ Assay',
        'test_method': 'qPCR',
        'specialty': 'Bacteriology',
        'test_target': 'Enterococcus faecalis',
        'result_type': 'Qualitative',
        'required_samples': {'Urine': 1}
    },
    {
        'name': 'Pseudomonas aeruginosa - Urinary Tract Microbiota TaqMan™ Assay',
        'test_method': 'qPCR',
        'specialty': 'Bacteriology',
        'test_target': 'Pseudomonas aeruginosa',
        'result_type': 'Qualitative',
        'required_samples': {'Urine': 1}
    },
    {
        'name': 'Streptococcus agalactiae - Urinary Tract Microbiota TaqMan™ Assay',
        'test_method': 'qPCR',
        'specialty': 'Bacteriology',
        'test_target': 'Streptococcus agalactiae',
        'result_type': 'Qualitative',
        'required_samples': {'Urine': 1}
    },
    {
        'name': 'Morganella morganii - Urinary Tract Microbiota TaqMan™ Assay',
        'test_method': 'qPCR',
        'specialty': 'Bacteriology',
        'test_target': 'Morganella morganii',
        'result_type': 'Qualitative',
        'required_samples': {'Urine': 1}
    },
    {
        'name': 'Providencia stuartii - Urinary Tract Microbiota TaqMan™ Assay',
        'test_method': 'qPCR',
        'specialty': 'Bacteriology',
        'test_target': 'Providencia stuartii',
        'result_type': 'Qualitative',
        'required_samples': {'Urine': 1}
    },
    {
        'name': 'Klebsiella oxytoca - Urinary Tract Microbiota TaqMan™ Assay',
        'test_method': 'qPCR',
        'specialty': 'Bacteriology',
        'test_target': 'Klebsiella oxytoca',
        'result_type': 'Qualitative',
        'required_samples': {'Urine': 1}
    },
    {
        'name': 'Citrobacter freundii - Urinary Tract Microbiota TaqMan™ Assay',
        'test_method': 'qPCR',
        'specialty': 'Bacteriology',
        'test_target': 'Citrobacter freundii',
        'result_type': 'Qualitative',
        'required_samples': {'Urine': 1}
    },
    {
        'name': 'Enterobacter cloacae - Urinary Tract Microbiota TaqMan™ Assay',
        'test_method': 'qPCR',
        'specialty': 'Bacteriology',
        'test_target': 'Enterobacter cloacae',
        'result_type': 'Qualitative',
        'required_samples': {'Urine': 1}
    },
    {
        'name': 'Enterobacter aerogenes - Urinary Tract Microbiota TaqMan™ Assay',
        'test_method': 'qPCR',
        'specialty': 'Bacteriology',
        'test_target': 'Enterobacter aerogenes',
        'result_type': 'Qualitative',
        'required_samples': {'Urine': 1}
    },
    {
        'name': 'Enterococcus faecium - Urinary Tract Microbiota TaqMan™ Assay',
        'test_method': 'qPCR',
        'specialty': 'Bacteriology',
        'test_target': 'Enterococcus faecium',
        'result_type': 'Qualitative',
        'required_samples': {'Urine': 1}
    },
    {
        'name': 'Acinetobacter baumannii - Urinary Tract Microbiota TaqMan™ Assay',
        'test_method': 'qPCR',
        'specialty': 'Bacteriology',
        'test_target': 'Acinetobacter baumannii',
        'result_type': 'Qualitative',
        'required_samples': {'Urine': 1}
    },
    {
        'name': 'Staphylococcus saprophyticus - Urinary Tract Microbiota TaqMan™ Assay',
        'test_method': 'qPCR',
        'specialty': 'Bacteriology',
        'test_target': 'Staphylococcus saprophyticus',
        'result_type': 'Qualitative',
        'required_samples': {'Urine': 1}
    },
    {
        'name': 'Proteus vulgaris - Urinary Tract Microbiota TaqMan™ Assay',
        'test_method': 'qPCR',
        'specialty': 'Bacteriology',
        'test_target': 'Proteus vulgaris',
        'result_type': 'Qualitative',
        'required_samples': {'Urine': 1}
    },
    {
        'name': 'Candida albicans - Urinary Tract Microbiota TaqMan™ Assay',
        'test_method': 'qPCR',
        'specialty': 'Bacteriology',
        'test_target': 'Candida albicans',
        'result_type': 'Qualitative',
        'required_samples': {'Urine': 1}
    },
    {
        'name': 'Xeno (spike-in control) - Urinary Tract Microbiota TaqMan™ Assay',
        'test_method': 'qPCR',
        'specialty': 'Bacteriology',
        'test_target': 'Xeno (spike-in control)',
        'result_type': 'Qualitative',
        'required_samples': {'Urine': 1}
    },
    {
        'name': 'Klebsiella pneumoniae - Urinary Tract Microbiota TaqMan™ Assay',
        'test_method': 'qPCR',
        'specialty': 'Bacteriology',
        'test_target': 'Klebsiella pneumoniae',
        'result_type': 'Qualitative',
        'required_samples': {'Urine': 1}
    },
]