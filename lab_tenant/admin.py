from django.contrib import admin
from django import forms

from main.tenant_admin import tenant_admin_site

from . import models


# this allows you to add/update Users on the admin dashboard instead of using python manage.py createsuperuser

class UserModelForm(forms.ModelForm):

    user_type = forms.IntegerField(label='User type (1 for lab admin, 2 for lab employees, 10 for provider, 20 for patient)',)

    def save(self, commit=True):
        # Save the provided password in hashed format
        user = super(UserModelForm, self).save(commit=False)
        user_type = self.cleaned_data.get('user_type', None)

        if user_type:
            user.user_type = user_type

        if commit:
            user.save()
        return user

    class Meta:
        model = models.User
        fields = '__all__'


class UserModelAdmin(admin.ModelAdmin):
    form = UserModelForm

    fieldsets = (
        (None, {
            'fields': ('email', 'user_type', 'last_name', 'first_name')
        }),
    )

# Register your models here.
tenant_admin_site.register(models.User, UserModelAdmin)
tenant_admin_site.register(models.OffsiteLaboratoryIntegration)
