from django.contrib import admin
from django.contrib.auth.models import User

# Register your models here.
tenant_admin_site = admin.AdminSite(name="tenant-admin")

tenant_admin_site.register(User)