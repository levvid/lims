from main.tenant_admin import tenant_admin_site

from . import models

# Register your models here.
tenant_admin_site.register(models.Account)
tenant_admin_site.register(models.Provider)