import os
from celery import Celery
from kombu import Queue

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'main.settings')

app = Celery('lis')
app.config_from_object('django.conf:settings', namespace='CELERY')
app.autodiscover_tasks()
app.conf.update(
    {
        'task_queues': (Queue('default', routing_key='default.#'),
                        Queue('identifiers', routing_key='identifiers.#')),
        'task_routes':
            {
                'lis.orders.tasks.receive_order': {'queue': 'identifiers'},
                'lis.orders.tasks.assign_order_code': {'queue': 'identifiers'},
            },
        'imports':
            ('orders.tasks',),
    }
)
