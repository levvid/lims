function createProviderPatientTable(patients, table) {
  patients.forEach(element => {
    element.name.toString = () => element.name.text;
  });

  if (!table) {
    const hotElement = document.querySelector('#provider-patient-table');
    const hotSettings = {
      data: patients,
      columns: [
        {
          data: 'name',
          editor: false,
          renderer: linkRenderer,
        },
        {
          data: 'sex',
          editor: false,
          type: 'text',
        },
        {
          data: 'birth_date',
          editor: false,
          type: 'date',
          dateFormat: 'MM/DD/YYYY',
          correctFormat: true,
        },
      ],
      stretchH: 'all',
      //width: 806,
      height: 200,
      autoColumnSize: true,
      autoWrapRow: true,
      manualRowResize: false,
      manualColumnResize: false,
      columnSorting: true,
      sortIndicator: true,
      rowHeaders: false,
      colHeaders: true,
      nestedHeaders: [
        [
          'Name',
          'Sex',
          'Birth Date',
        ],
      ],
      manualRowMove: false,
      manualColumnMove: false,
      contextMenu: false,
      filters: true,
      dropdownMenu: ['filter_by_condition', 'filter_action_bar'],
      afterGetColHeader: function(col, TH) {
        TH.className = 'center';
      },
    };

    return new Handsontable(hotElement, hotSettings);
  } else {
    // load new data without replacing old table
    table.getPlugin('filters').clearConditions();
    table.loadData(accounts);
    return table;
  }
}
