from channels.auth import AuthMiddlewareStack
from channels.sessions import SessionMiddlewareStack
from channels.routing import ProtocolTypeRouter, URLRouter
import orders.routing

application = ProtocolTypeRouter({
    # Empty for now (http->django views is added by default)
    'websocket': SessionMiddlewareStack(
        URLRouter(
            orders.routing.websocket_urlpatterns
        )
    )
})