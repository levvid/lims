# Generated by Django 2.2.2 on 2019-10-18 00:25

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('orders', '0098_control_instrument_integrations'),
    ]

    operations = [
        migrations.AddField(
            model_name='orderissue',
            name='is_visible_to_provider',
            field=models.BooleanField(default=False),
        ),
    ]
