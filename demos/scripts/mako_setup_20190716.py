import datetime
import decimal
import django

import logging
import os
import random
import sys

from django.db import transaction
from django.utils import timezone
from tenant_schemas.utils import tenant_context

os.environ['DJANGO_SETTINGS_MODULE'] = 'main.settings'
django.setup()

from clients import models as client_models
from orders import models as orders_models
from accounts import models as accounts_models
from lab import models as lab_models
from lab_tenant import models as lab_tenant_models
from patients import models as patients_models
from orders.functions import generate_order_code, generate_sample_code, get_sample_barcode

# import dummy data
from demos.shared_data.shared_data import INSTRUMENTS, TEST_TARGETS
from demos.dummy_data.common import ADDITIONAL_USERS, PROVIDERS, ACCOUNTS, PATIENTS
from demos.dummy_data.toxicology import CONTROLS, DATA_IN_LIST, INSTRUMENT_INTEGRATIONS, TEST_TYPES, \
    TEST_PANEL_GROUPS

from demos.initialization_functions import create_providers, create_accounts, create_users, create_instruments, \
    create_instrument_integrations, create_patients, create_controls, create_data_in_demo, create_test_types, \
    create_test_targets

from tasks import initialize_client

"""
Script to set up a new schema for demo purposes
"""

DEMO = True
NUM_ORDERS = 10

# schema variables
SCHEMA_NAME = 'mako1'
LABORATORY_NAME = 'Mako Medical'
LABORATORY_TIMEZONE = 'US/Central'
EMAIL = 'sjung@dendisoftware.com'
FIRST_NAME = 'Simon'
LAST_NAME = 'Jung'
PASSWORD = 'dendipower123123'
SCHEMA_CONTEXT = {'client_name': LABORATORY_NAME,
                  'schema_name': SCHEMA_NAME,
                  'email': EMAIL,
                  'first_name': FIRST_NAME,
                  'last_name': LAST_NAME,
                  'password': PASSWORD,
                  'lab_type': '',
                  'address_1': '8122 Sawyer Brown Rd., Ste 210',
                  'address_2': 'Nashville, TN 37221',
                  'phone_number': '(844) 261-9529',
                  'fax_number': '(615) 630-7798',
                  'clia_number': '44D208045',
                  'clia_director': 'Danielle Gibson, MD',
                  }

SCHEMA_SETTINGS = {"require_icd10": True,
                   "skip_patient_history": True}


@transaction.atomic
def main():
    try:
        tenant = client_models.Client.objects.get(schema_name=SCHEMA_NAME)
        logging.debug('Using schema: {}'.format(SCHEMA_NAME))
    except client_models.Client.DoesNotExist:
        logging.debug('Creating schema: {}'.format(SCHEMA_NAME))
        initialize_client(SCHEMA_CONTEXT)
        tenant = client_models.Client.objects.get(schema_name=SCHEMA_NAME)
        tenant.timezone = LABORATORY_TIMEZONE
        tenant.settings = SCHEMA_SETTINGS
        tenant.save(update_fields=['timezone', 'settings'])

        logging.debug('Creating users')
        with tenant_context(tenant):
            create_users(ADDITIONAL_USERS)

    logging.debug('Initializing test targets')
    create_test_targets(TEST_TARGETS)

    logging.debug('Initializing required instruments')
    create_instruments(INSTRUMENTS)

    with tenant_context(tenant):
        logging.debug('Initializing instrument integrations')
        create_instrument_integrations(INSTRUMENT_INTEGRATIONS)

        # make test panel
        logging.debug('Initializing test panel types')
        initialize_test_panel_types()

        # make test types
        logging.debug('Initializing test types')
        test_types = create_test_types(TEST_TYPES, create_panel=True, tox=True)
        initialize_all_inclusive_test_panel_type()

        logging.debug('Initializing instrument calibrations')
        initialize_instrument_calibrations()

        if DEMO:
            # make account
            logging.debug('Initializing dummy accounts')
            create_accounts(ACCOUNTS)

            # make patients
            logging.debug('Initializing dummy patients')
            create_patients(PATIENTS)

            # make providers
            logging.debug("Initializing dummy providers")
            create_providers(PROVIDERS)

            # make orders
            logging.debug('Initializing {} dummy orders'.format(NUM_ORDERS))
            initialize_dummy_orders(NUM_ORDERS)

            # make some in transit orders
            #logging.debug("Initializing in transit orders")
            #initialize_in_transit_orders()

            # make controls
            logging.debug("Initializing controls")
            create_controls(CONTROLS)

            # add data-in
            logging.debug("Initializing data in demo")
            create_data_in_demo(DATA_IN_LIST)

            # make dummy control observations
            logging.debug("Initializing control observations")
            initialize_dummy_observations()


def initialize_test_panel_types():
    """
    Create group panel types
    :param test_types:
    :return:
    """
    for test_panel_group in TEST_PANEL_GROUPS:
        menu_category, created = orders_models.TestPanelCategory.objects.update_or_create(
            name=test_panel_group,
        )
        test_panel_type, created = orders_models.TestPanelType.objects.update_or_create(
            name=test_panel_group,
            menu_category=menu_category,
            category_order=1
        )


def initialize_all_inclusive_test_panel_type():
    # All inclusive panel
    test_panel_type, created = orders_models.TestPanelType.objects.update_or_create(
        name='All analyte test',
        menu_category_id=1
    )
    test_types = orders_models.TestType.objects.all()
    for test_type in test_types:
        test_panel_type.test_types.add(test_type)


def initialize_instrument_calibrations():
    ms_instruments = lab_tenant_models.InstrumentIntegration.objects.filter(
        internal_instrument_name__icontains='Shimadzu'
    )
    for ms_instrument in ms_instruments:
        for test_type_dict in TEST_TYPES:
            test_type = orders_models.TestType.objects.get(name=test_type_dict['name'])
            orders_models.InstrumentCalibration.objects.create(
                instrument_integration=ms_instrument,
                test_type=test_type,
                target_name=test_type.test_target.name,
                cutoff_value=test_type_dict.get('cutoff')
            )


def initialize_dummy_orders(num_orders):
    urine_sample_type = lab_models.CLIASampleType.objects.get(name='Urine')
    test_panel_type = orders_models.TestPanelType.objects.get(name='All analyte test')

    days_back = 20
    received_date_start = timezone.now() - datetime.timedelta(days=days_back)
    average_order_count = int(num_orders / days_back)

    for order_count in range(num_orders):
        latest_order_code = generate_order_code()
        rand_account = accounts_models.Account.objects.get(id=random.randint(1, 9))
        rand_patient = patients_models.Patient.objects.get(id=random.randint(1, 11))
        logging.debug('Order {}: {} {} '.format(order_count, rand_account.name, rand_patient.user.first_name))
        order = orders_models.Order.objects.create(
            provider_id=random.randint(1, 5),
            account=rand_account,
            patient=rand_patient,
            code=latest_order_code,
            research_consent=False,
            patient_result_request=False,

        )
        order.received_date = received_date_start
        order.save(update_fields=['received_date'])

        latest_sample_code = generate_sample_code(order.code, urine_sample_type)
        sample = orders_models.Sample.objects.create(
            order=order,
            code=latest_sample_code,
            clia_sample_type=urine_sample_type,
            collection_date=timezone.now()
        )
        sample_barcode = get_sample_barcode(sample.id)
        sample.barcode = sample_barcode
        sample.save(update_fields=['barcode'])

        test_panel = orders_models.TestPanel.objects.create(
            order=order,
            test_panel_type=test_panel_type,
            expected_revenue=test_panel_type.cost,
        )

        for test_type in test_panel_type.test_types.all():
            test = orders_models.Test.objects.create(
                order=order,
                sample=sample,
                test_panel=test_panel,
                test_type=test_type,
                due_date=datetime.date.today() + datetime.timedelta(days=test_type.due_date_days)
            )
            result_obj = orders_models.Result.objects.create(
                order=order,
                test=test
            )
        order.submitted = True
        order.save(update_fields=['submitted'])

        if random.randint(1, average_order_count + 1) == average_order_count + 1:
            if received_date_start + datetime.timedelta(days=1, hours=2) < timezone.now():
                received_date_start = received_date_start + datetime.timedelta(days=1, hours=2)


def initialize_in_transit_orders():
    urine_sample_type = lab_models.CLIASampleType.objects.get(name='Urine')
    test_panel_type = orders_models.TestPanelType.objects.get(name='All analyte test')

    latest_order_code = generate_order_code()
    rand_account = accounts_models.Account.objects.get(id=random.randint(1, 9))
    rand_patient = patients_models.Patient.objects.get(id=random.randint(1, 11))
    logging.debug('In transit order Order: {} {} '.format(rand_account.name, rand_patient.user.first_name))
    order = orders_models.Order.objects.create(
        provider_id=1,
        account=rand_account,
        patient=rand_patient,
        code=latest_order_code,
        research_consent=False,
        patient_result_request=False,

    )

    latest_sample_code = generate_sample_code(order.code, urine_sample_type)
    sample = orders_models.Sample.objects.create(
        order=order,
        code=latest_sample_code,
        clia_sample_type=urine_sample_type,
        collection_date=timezone.now()
    )
    sample_barcode = get_sample_barcode(sample.id)
    sample.barcode = sample_barcode
    sample.save(update_fields=['barcode'])

    test_panel = orders_models.TestPanel.objects.create(
        order=order,
        test_panel_type=test_panel_type,
        expected_revenue=test_panel_type.cost,
    )

    for test_type in test_panel_type.test_types.all():
        test = orders_models.Test.objects.create(
            order=order,
            sample=sample,
            test_panel=test_panel,
            test_type=test_type,
            due_date=datetime.date.today() + datetime.timedelta(days=test_type.due_date_days)
        )
        result_obj = orders_models.Result.objects.create(
            order=order,
            test=test
        )
    order.submitted = True
    order.save(update_fields=['submitted'])


def initialize_dummy_observations():
    """
    create dummy observations for QC charting purposes
    :return:
    """
    days_back = 30
    date_counter = timezone.now() - datetime.timedelta(days=days_back)
    data_in = orders_models.DataIn.objects.first()
    controls = orders_models.Control.objects.all()
    targets = ['Cocaine', 'Lorazepam', 'Carisoprodol', 'Meprobamate']

    control_concentrations = [50.0, 100.0, 250.0, 500.0, 1000.0, 2000.0, 5000.0]

    while date_counter < timezone.now():
        control_index = 0
        for control in controls:
            for target in targets:
                orders_models.Observation.objects.create(
                    control=control,
                    target_name=target,
                    data_in=data_in,
                    result_value=random.uniform(control_concentrations[control_index] - 5.0,
                                                control_concentrations[control_index] + 5.0),
                    observation_date=date_counter,
                )
            control_index += 1
        date_counter += datetime.timedelta(days=1)


if __name__ == '__main__':
    logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)
    main()
