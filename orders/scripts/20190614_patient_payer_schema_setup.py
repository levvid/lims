import datetime
import decimal
import django

import logging
import os
import random
import uuid
import sys

from django.db import transaction
from django.utils import timezone
from tenant_schemas.utils import tenant_context

os.environ['DJANGO_SETTINGS_MODULE'] = 'main.settings'
django.setup()

from clients import models as client_models
from .. import models
from accounts import models as accounts_models
from lab import models as lab_models
from lab_tenant import models as lab_tenant_models
from patients import models as patients_models

from orders.functions import generate_order_code, generate_sample_code, get_sample_barcode

from tasks import initialize_client

"""
Script to set up US Healthtek demo TestTypes, Tests
"""

DEMO = True
NUM_ORDERS = 10
QUANTSTUDIO_SERIAL_NUMBER = 'aiwthn23sriatk'
INTERNAL_INSTRUMENT_NAME = 'QuantStudio 12K'

# schema variables
SCHEMA_NAME = 'patientpayer0'
LABORATORY_NAME = 'Patient Payer'
EMAIL = 'sjung@dendisoftware.com'
FIRST_NAME = 'Simon'
LAST_NAME = 'Jung'
PASSWORD = 'dendipower123123'
SCHEMA_CONTEXT = {'client_name': LABORATORY_NAME,
                  'schema_name': SCHEMA_NAME,
                  'email': EMAIL,
                  'first_name': FIRST_NAME,
                  'last_name': LAST_NAME,
                  'password': PASSWORD,
                  'lab_type': '',
                  }


@transaction.atomic
def main():
    try:
        tenant = client_models.Client.objects.get(schema_name=SCHEMA_NAME)
        logging.debug('Using schema: {}'.format(SCHEMA_NAME))
    except client_models.Client.DoesNotExist:
        logging.debug('Creating schema: {}'.format(SCHEMA_NAME))
        initialize_client(SCHEMA_CONTEXT)
        tenant = client_models.Client.objects.get(schema_name=SCHEMA_NAME)
        tenant.timezone = 'US/Eastern'
        tenant.settings = {"require_icd10": True,
                           "skip_patient_history": True}
        tenant.save(update_fields=['timezone', 'settings'])

        logging.debug('Creating users')
        create_users(tenant)

    logging.debug('Initializing test targets')
    initialize_targets()

    logging.debug('Initializing required instruments')
    initialize_instruments()

    with tenant_context(tenant):
        logging.debug('Initializing instrument integrations')
        initialize_instrument_integrations()

        # make test types
        logging.debug('Initializing test types')
        utm_test_types = initialize_test_types()

        # make test panel
        logging.debug('Initializing test panel types')
        initialize_test_panel_types(utm_test_types)

        logging.debug('Initializing instrument calibrations')
        initialize_instrument_calibrations()

        if DEMO:
            # make account
            logging.debug('Initializing dummy accounts')
            initialize_dummy_accounts()

            # make patients
            logging.debug('Initializing dummy patients')
            initialize_dummy_patients()

            # make providers
            logging.debug("Initializing dummy providers")
            initialize_dummy_providers()

            # make orders
            logging.debug('Initializing {} dummy orders'.format(NUM_ORDERS))
            initialize_dummy_orders(NUM_ORDERS)

            # make some in transit orders
            logging.debug("Initializing in transit orders")
            initialize_in_transit_orders()

            # add data-in
            logging.debug("Initializing data in demo")
            initialize_data_in_demo()


def create_users(tenant):
    with tenant_context(tenant):
        # create LabEmployee User type
        user_obj = lab_tenant_models.User(last_name='Baek',
                                          first_name='Jihoon',
                                          username='jbaek@dendisoftware.com',
                                          email='jbaek@dendisoftware.com',
                                          user_type=1)
        # save User object first
        user_obj.set_password(PASSWORD)
        user_obj.save()
        lab_admin_obj = lab_tenant_models.LabAdmin(user=user_obj)
        lab_admin_obj.save()

        # create LabEmployee User type
        user_obj = lab_tenant_models.User(last_name='Mulonga',
                                          first_name='Gibson',
                                          username='gmulonga@dendisoftware.com',
                                          email='gmulonga@dendisoftware.com',
                                          user_type=1)
        # save User object first
        user_obj.set_password(PASSWORD)
        user_obj.save()
        lab_admin_obj = lab_tenant_models.LabAdmin(user=user_obj)
        lab_admin_obj.save()


def initialize_instruments():
    qpcr_methodology = lab_models.TestMethod.objects.get(name='qPCR')
    instrument, created = lab_models.Instrument.objects.update_or_create(
        name='QuantStudio 12K Flex Real-Time PCR System',
        manufacturer='Applied Biosystems™',
        method=qpcr_methodology)
    return instrument


def initialize_instrument_integrations():
    instrument_integration, created = lab_tenant_models.InstrumentIntegration.objects.update_or_create(
        internal_instrument_name=INTERNAL_INSTRUMENT_NAME,
        serial_number=QUANTSTUDIO_SERIAL_NUMBER,
        instrument=lab_models.Instrument.objects.get(name='QuantStudio 12K Flex Real-Time PCR System')
    )
    return instrument_integration


def initialize_targets():
    lab_models.TestTarget.objects.update_or_create(name='Proteus mirabilis', is_tox_target=False)
    lab_models.TestTarget.objects.update_or_create(name='Enterococcus faecalis', is_tox_target=False)
    lab_models.TestTarget.objects.update_or_create(name='Pseudomonas aeruginosa', is_tox_target=False)
    lab_models.TestTarget.objects.update_or_create(name='Streptococcus agalactiae', is_tox_target=False)
    lab_models.TestTarget.objects.update_or_create(name='Morganella morganii', is_tox_target=False)
    lab_models.TestTarget.objects.update_or_create(name='Providencia stuartii', is_tox_target=False)
    lab_models.TestTarget.objects.update_or_create(name='Klebsiella oxytoca', is_tox_target=False)
    lab_models.TestTarget.objects.update_or_create(name='Citrobacter freundii', is_tox_target=False)
    lab_models.TestTarget.objects.update_or_create(name='Enterobacter cloacae', is_tox_target=False)
    lab_models.TestTarget.objects.update_or_create(name='Enterobacter aerogenes', is_tox_target=False)
    lab_models.TestTarget.objects.update_or_create(name='Enterococcus faecium', is_tox_target=False)
    lab_models.TestTarget.objects.update_or_create(name='Acinetobacter baumannii', is_tox_target=False)
    lab_models.TestTarget.objects.update_or_create(name='Staphylococcus saprophyticus', is_tox_target=False)
    lab_models.TestTarget.objects.update_or_create(name='Proteus vulgaris', is_tox_target=False)
    lab_models.TestTarget.objects.update_or_create(name='Candida albicans', is_tox_target=False)
    lab_models.TestTarget.objects.update_or_create(name='Xeno (spike-in control)', is_tox_target=False)
    lab_models.TestTarget.objects.update_or_create(name='Klebsiella pneumoniae', is_tox_target=False)
    lab_models.TestTarget.objects.update_or_create(name='Escherichia coli', is_tox_target=False)


def initialize_test_types():
    """
    Create 18 utm TestTypes
    :return:
    """
    qpcr_methodology = lab_models.TestMethod.objects.get(name='qPCR')
    bacteriology_specialty = lab_models.CLIATestTypeSpecialty.objects.get(specialty_name='Bacteriology')
    urine_sample_type = lab_models.CLIASampleType.objects.get(name='Urine')

    utm_test_types = []
    obj, created = models.TestType.objects.update_or_create(
        name='Escherichia coli - Urinary Tract Microbiota TaqMan™ Assay',
        test_method=qpcr_methodology,
        specialty=bacteriology_specialty,
        test_target=lab_models.TestTarget.objects.get(name='Escherichia coli'),
        defaults={
             'result_type': 'Qualitative',
             'required_samples': {str(urine_sample_type.id): 1}
         })
    utm_test_types.append(obj)
    obj, created = models.TestType.objects.update_or_create(
        name='Proteus mirabilis - Urinary Tract Microbiota TaqMan™ Assay',
        test_method=qpcr_methodology,
        specialty=bacteriology_specialty,
        test_target=lab_models.TestTarget.objects.get(name='Proteus mirabilis'),
        defaults={
             'result_type': 'Qualitative',
             'required_samples': {str(urine_sample_type.id): 1}
         })
    utm_test_types.append(obj)
    obj, created = models.TestType.objects.update_or_create(
        name='Enterococcus faecalis - Urinary Tract Microbiota TaqMan™ Assay',
        test_method=qpcr_methodology,
        specialty=bacteriology_specialty,
        test_target=lab_models.TestTarget.objects.get(name='Enterococcus faecalis'),
        defaults={
             'result_type': 'Qualitative',
             'required_samples': {str(urine_sample_type.id): 1}
         })
    utm_test_types.append(obj)
    obj, created = models.TestType.objects.update_or_create(
        name='Pseudomonas aeruginosa - Urinary Tract Microbiota TaqMan™ Assay',
        test_method=qpcr_methodology,
        specialty=bacteriology_specialty,
        test_target=lab_models.TestTarget.objects.get(name='Pseudomonas aeruginosa'),
        defaults={
             'result_type': 'Qualitative',
             'required_samples': {str(urine_sample_type.id): 1}
         })
    utm_test_types.append(obj)
    obj, created = models.TestType.objects.update_or_create(
        name='Streptococcus agalactiae - Urinary Tract Microbiota TaqMan™ Assay',
        test_method=qpcr_methodology,
        specialty=bacteriology_specialty,
        test_target=lab_models.TestTarget.objects.get(name='Streptococcus agalactiae'),
        defaults={
             'result_type': 'Qualitative',
             'required_samples': {str(urine_sample_type.id): 1}
         })
    utm_test_types.append(obj)
    obj, created = models.TestType.objects.update_or_create(
        name='Morganella morganii - Urinary Tract Microbiota TaqMan™ Assay',
        test_method=qpcr_methodology,
        specialty=bacteriology_specialty,
        test_target=lab_models.TestTarget.objects.get(name='Morganella morganii'),
        defaults={
             'result_type': 'Qualitative',
             'required_samples': {str(urine_sample_type.id): 1}
         })
    utm_test_types.append(obj)
    obj, created = models.TestType.objects.update_or_create(
        name='Providencia stuartii - Urinary Tract Microbiota TaqMan™ Assay',
        test_method=qpcr_methodology,
        specialty=bacteriology_specialty,
        test_target=lab_models.TestTarget.objects.get(name='Providencia stuartii'),
        defaults={
             'result_type': 'Qualitative',
             'required_samples': {str(urine_sample_type.id): 1}
         })
    utm_test_types.append(obj)
    obj, created = models.TestType.objects.update_or_create(
        name='Klebsiella oxytoca - Urinary Tract Microbiota TaqMan™ Assay',
        test_method=qpcr_methodology,
        specialty=bacteriology_specialty,
        test_target=lab_models.TestTarget.objects.get(name='Klebsiella oxytoca'),
        defaults={
             'result_type': 'Qualitative',
             'required_samples': {str(urine_sample_type.id): 1}
         })
    utm_test_types.append(obj)
    obj, created = models.TestType.objects.update_or_create(
        name='Citrobacter freundii - Urinary Tract Microbiota TaqMan™ Assay',
        test_method=qpcr_methodology,
        specialty=bacteriology_specialty,
        test_target=lab_models.TestTarget.objects.get(name='Citrobacter freundii'),
        defaults={
             'result_type': 'Qualitative',
             'required_samples': {str(urine_sample_type.id): 1}
         })
    utm_test_types.append(obj)
    obj, created = models.TestType.objects.update_or_create(
        name='Enterobacter cloacae - Urinary Tract Microbiota TaqMan™ Assay',
        test_method=qpcr_methodology,
        specialty=bacteriology_specialty,
        test_target=lab_models.TestTarget.objects.get(name='Enterobacter cloacae'),
        defaults={
             'result_type': 'Qualitative',
             'required_samples': {str(urine_sample_type.id): 1}
         })
    utm_test_types.append(obj)
    obj, created = models.TestType.objects.update_or_create(
        name='Enterobacter aerogenes - Urinary Tract Microbiota TaqMan™ Assay',
        test_method=qpcr_methodology,
        specialty=bacteriology_specialty,
        test_target=lab_models.TestTarget.objects.get(name='Enterobacter aerogenes'),
        defaults={
             'result_type': 'Qualitative',
             'required_samples': {str(urine_sample_type.id): 1}
         })
    utm_test_types.append(obj)
    obj, created = models.TestType.objects.update_or_create(
        name='Enterococcus faecium - Urinary Tract Microbiota TaqMan™ Assay',
        test_method=qpcr_methodology,
        specialty=bacteriology_specialty,
        test_target=lab_models.TestTarget.objects.get(name='Enterococcus faecium'),
        defaults={
             'result_type': 'Qualitative',
             'required_samples': {str(urine_sample_type.id): 1}
         })
    utm_test_types.append(obj)
    obj, created = models.TestType.objects.update_or_create(
        name='Acinetobacter baumannii - Urinary Tract Microbiota TaqMan™ Assay',
        test_method=qpcr_methodology,
        specialty=bacteriology_specialty,
        test_target=lab_models.TestTarget.objects.get(name='Acinetobacter baumannii'),
        defaults={
             'result_type': 'Qualitative',
             'required_samples': {str(urine_sample_type.id): 1}
         })
    utm_test_types.append(obj)
    obj, created = models.TestType.objects.update_or_create(
        name='Staphylococcus saprophyticus - Urinary Tract Microbiota TaqMan™ Assay',
        test_method=qpcr_methodology,
        specialty=bacteriology_specialty,
        test_target=lab_models.TestTarget.objects.get(name='Staphylococcus saprophyticus'),
        defaults={
             'result_type': 'Qualitative',
             'required_samples': {str(urine_sample_type.id): 1}
         })
    utm_test_types.append(obj)
    obj, created = models.TestType.objects.update_or_create(
        name='Proteus vulgaris - Urinary Tract Microbiota TaqMan™ Assay',
        test_method=qpcr_methodology,
        specialty=bacteriology_specialty,
        test_target=lab_models.TestTarget.objects.get(name='Proteus vulgaris'),
        defaults={
             'result_type': 'Qualitative',
             'required_samples': {str(urine_sample_type.id): 1}
         })
    utm_test_types.append(obj)
    obj, created = models.TestType.objects.update_or_create(
        name='Candida albicans - Urinary Tract Microbiota TaqMan™ Assay',
        test_method=qpcr_methodology,
        specialty=bacteriology_specialty,
        test_target=lab_models.TestTarget.objects.get(name='Candida albicans'),
        defaults={
             'result_type': 'Qualitative',
             'required_samples': {str(urine_sample_type.id): 1}
         })
    utm_test_types.append(obj)
    obj, created = models.TestType.objects.update_or_create(
        name='Xeno (spike-in control) - Urinary Tract Microbiota TaqMan™ Assay',
        test_method=qpcr_methodology,
        specialty=bacteriology_specialty,
        test_target=lab_models.TestTarget.objects.get(name='Xeno (spike-in control)'),
        defaults={
             'result_type': 'Qualitative',
             'required_samples': {str(urine_sample_type.id): 1}
         })
    utm_test_types.append(obj)
    obj, created = models.TestType.objects.update_or_create(
        name='Klebsiella pneumoniae - Urinary Tract Microbiota TaqMan™ Assay',
        test_method=qpcr_methodology,
        specialty=bacteriology_specialty,
        test_target=lab_models.TestTarget.objects.get(name='Klebsiella pneumoniae'),
        defaults={
             'result_type': 'Qualitative',
             'required_samples': {str(urine_sample_type.id): 1}
         })
    utm_test_types.append(obj)
    return utm_test_types


def initialize_test_panel_types(utm_test_types):
    """
    Create the Urinary Tract Microbiota TaqMan™ Assay Panel and assign TestTypes to it
    :param utm_test_types:
    :return:
    """
    test_panel_type, created = models.TestPanelType.objects.update_or_create(
        name='Urinary Tract Microbiota TaqMan™ Assay Panel',
        menu_category_id=1)

    for test_type in utm_test_types:
        test_panel_type.test_types.add(test_type)


def initialize_dummy_accounts():
    accounts_models.Account.objects.update_or_create(name='Durham Clinic',
                                                     address1='1204 Broad St, Durham, NC')
    accounts_models.Account.objects.update_or_create(name='Chapel Hill Clinic',
                                                     address1='1324 Franklin St, Chapel Hill, NC')
    accounts_models.Account.objects.update_or_create(name='Raleigh Center for Health',
                                                     address1='342 Radurleigh Rd, Raleigh, NC')
    accounts_models.Account.objects.update_or_create(name='AnyTestNow',
                                                     address1='543 Hopson St, Cary, NC')
    accounts_models.Account.objects.update_or_create(name='Center for Clinic',
                                                     address1='2415 Foundation Cir, Durham, NC')
    accounts_models.Account.objects.update_or_create(name='Sacred Heart Hospital',
                                                     address1='34255 Valve, Charlotte, NC')
    accounts_models.Account.objects.update_or_create(name='Mission Tests',
                                                     address1='789 Dark ave, Raleigh, NC')
    accounts_models.Account.objects.update_or_create(name='Holly Springs Clinic',
                                                     address1='789 Dark ave, Holly Springs, NC')
    accounts_models.Account.objects.update_or_create(name='Cors Clinic for Health',
                                                     address1='7894 Hidden Valley ave, Raleigh, NC')


def initialize_instrument_calibrations():
    instrument_integration = lab_tenant_models.InstrumentIntegration.objects.get(internal_instrument_name=INTERNAL_INSTRUMENT_NAME)
    test_types = models.TestType.objects.all()

    cutoff = decimal.Decimal('25.50')
    for test_type in test_types:
        models.InstrumentCalibration.objects.update_or_create(instrument_integration=instrument_integration,
                                                              test_type=test_type,
                                                              target_name=test_type.test_target.name,
                                                              defaults={'cutoff_value': cutoff}
                                                              )
        cutoff = cutoff + decimal.Decimal('0.23')


def initialize_dummy_providers():
    user_uuid = str(uuid.uuid4())
    first_name = 'Gregory'
    last_name = 'House'
    provider_email = 'GHouse@gmail.com'
    provider_npi = '1234567890'
    username = provider_email
    user, created = lab_tenant_models.User.objects.update_or_create(first_name=first_name,
                                                                    last_name=last_name,
                                                                    user_type=10,
                                                                    email=provider_email,
                                                                    defaults={
                                                                        'username': username
                                                                    })
    user.set_password(str(user_uuid))
    user.save()
    provider = accounts_models.Provider(user=user,
                                        npi=provider_npi,
                                        uuid=user_uuid
                                        )
    provider.save()

    user_uuid = str(uuid.uuid4())
    first_name = 'Helen'
    last_name = 'Taussig'
    provider_email = 'HTaussig@gmail.com'
    provider_npi = '0123456789'
    username = provider_email
    user, created = lab_tenant_models.User.objects.update_or_create(first_name=first_name,
                                                                    last_name=last_name,
                                                                    user_type=10,
                                                                    email=provider_email,
                                                                    defaults={
                                                                        'username': username
                                                                    })
    user.set_password(str(user_uuid))
    user.save()
    provider = accounts_models.Provider(user=user,
                                        npi=provider_npi,
                                        uuid=user_uuid
                                        )
    provider.save()

    user_uuid = str(uuid.uuid4())
    first_name = 'Zora'
    last_name = 'Janzekovic'
    provider_email = 'ZJanzekovic@gmail.com'
    provider_npi = '2345678901'
    username = provider_email
    user, created = lab_tenant_models.User.objects.update_or_create(first_name=first_name,
                                                                    last_name=last_name,
                                                                    user_type=10,
                                                                    email=provider_email,
                                                                    defaults={
                                                                        'username': username
                                                                    })
    user.set_password(str(user_uuid))
    user.save()
    provider = accounts_models.Provider(user=user,
                                        npi=provider_npi,
                                        uuid=user_uuid
                                        )
    provider.save()

    user_uuid = str(uuid.uuid4())
    first_name = 'Virginia'
    last_name = 'Apgar'
    provider_email = 'VApgar@gmail.com'
    provider_npi = '3456789012'
    username = provider_email
    user, created = lab_tenant_models.User.objects.update_or_create(first_name=first_name,
                                                                    last_name=last_name,
                                                                    user_type=10,
                                                                    email=provider_email,
                                                                    defaults={
                                                                        'username': username
                                                                    })
    user.set_password(str(user_uuid))
    user.save()
    provider = accounts_models.Provider(user=user,
                                        npi=provider_npi,
                                        uuid=user_uuid
                                        )
    provider.save()

    user_uuid = str(uuid.uuid4())
    first_name = 'Victor'
    last_name = 'McKusick'
    provider_email = 'VMckusic@gmail.com'
    provider_npi = '4567890123'
    username = provider_email
    user, created = lab_tenant_models.User.objects.update_or_create(first_name=first_name,
                                                                    last_name=last_name,
                                                                    user_type=10,
                                                                    email=provider_email,
                                                                    defaults={
                                                                        'username': username
                                                                    })
    user.set_password(str(user_uuid))
    user.save()
    provider = accounts_models.Provider(user=user,
                                        npi=provider_npi,
                                        uuid=user_uuid
                                        )
    provider.save()


def initialize_dummy_patients():
    user_uuid = str(uuid.uuid4())
    first_name = 'Simon'
    last_name = 'Jung'
    username = first_name + '-' + last_name + '-' + user_uuid
    user, created = lab_tenant_models.User.objects.update_or_create(first_name=first_name,
                                                                    last_name=last_name,
                                                                    user_type=20,
                                                                    defaults={
                                                                      'username': username
                                                                    })
    patients_models.Patient.objects.update_or_create(birth_date='1992-04-14',
                                                     sex='M',
                                                     user=user,
                                                     )

    user_uuid = str(uuid.uuid4())
    first_name = 'Sally'
    last_name = 'Hopson'
    username = first_name + '-' + last_name + '-' + user_uuid
    user, created = lab_tenant_models.User.objects.update_or_create(first_name=first_name,
                                                                    last_name=last_name,
                                                                    user_type=20,
                                                                    defaults={
                                                                        'username': username
                                                                    })
    patients_models.Patient.objects.update_or_create(birth_date='1994-03-13',
                                                     sex='F',
                                                     user=user,
                                                     )

    user_uuid = str(uuid.uuid4())
    first_name = 'Missy'
    last_name = 'Hills'
    username = first_name + '-' + last_name + '-' + user_uuid
    user, created = lab_tenant_models.User.objects.update_or_create(first_name=first_name,
                                                                    last_name=last_name,
                                                                    user_type=20,
                                                                    defaults={
                                                                        'username': username
                                                                    })
    patients_models.Patient.objects.update_or_create(birth_date='1991-04-29',
                                                     sex='F',
                                                     user=user,
                                                     )

    user_uuid = str(uuid.uuid4())
    first_name = 'Joanna'
    last_name = 'Smith'
    username = first_name + '-' + last_name + '-' + user_uuid
    user, created = lab_tenant_models.User.objects.update_or_create(first_name=first_name,
                                                                    last_name=last_name,
                                                                    user_type=20,
                                                                    defaults={
                                                                        'username': username
                                                                    })
    patients_models.Patient.objects.update_or_create(birth_date='1996-09-14',
                                                     sex='F',
                                                     user=user,
                                                     )

    user_uuid = str(uuid.uuid4())
    first_name = 'Dendi'
    last_name = 'Jung'
    username = first_name + '-' + last_name + '-' + user_uuid
    user, created = lab_tenant_models.User.objects.update_or_create(first_name=first_name,
                                                                    last_name=last_name,
                                                                    user_type=20,
                                                                    defaults={
                                                                        'username': username
                                                                    })
    patients_models.Patient.objects.update_or_create(birth_date='2012-05-19',
                                                     sex='M',
                                                     user=user,
                                                     )

    user_uuid = str(uuid.uuid4())
    first_name = 'Jose'
    last_name = 'Guzman'
    username = first_name + '-' + last_name + '-' + user_uuid
    user, created = lab_tenant_models.User.objects.update_or_create(first_name=first_name,
                                                                    last_name=last_name,
                                                                    user_type=20,
                                                                    defaults={
                                                                        'username': username
                                                                    })
    patients_models.Patient.objects.update_or_create(birth_date='1991-09-14',
                                                     sex='M',
                                                     user=user,
                                                     )

    user_uuid = str(uuid.uuid4())
    first_name = 'Juan'
    last_name = 'Peter'
    username = first_name + '-' + last_name + '-' + user_uuid
    user, created = lab_tenant_models.User.objects.update_or_create(first_name=first_name,
                                                                    last_name=last_name,
                                                                    user_type=20,
                                                                    defaults={
                                                                        'username': username
                                                                    })
    patients_models.Patient.objects.update_or_create(birth_date='1992-04-12',
                                                     sex='M',
                                                     user=user,
                                                     )

    user_uuid = str(uuid.uuid4())
    first_name = 'Arya'
    last_name = 'Stark'
    username = first_name + '-' + last_name + '-' + user_uuid
    user, created = lab_tenant_models.User.objects.update_or_create(first_name=first_name,
                                                                    last_name=last_name,
                                                                    user_type=20,
                                                                    defaults={
                                                                        'username': username
                                                                    })
    patients_models.Patient.objects.update_or_create(birth_date='1992-04-14',
                                                     sex='F',
                                                     user=user,
                                                     )

    user_uuid = str(uuid.uuid4())
    first_name = 'Fernando'
    last_name = 'Hernendez'
    username = first_name + '-' + last_name + '-' + user_uuid
    user, created = lab_tenant_models.User.objects.update_or_create(first_name=first_name,
                                                                    last_name=last_name,
                                                                    user_type=20,
                                                                    defaults={
                                                                        'username': username
                                                                    })
    patients_models.Patient.objects.update_or_create(birth_date='1982-02-10',
                                                     sex='M',
                                                     user=user,
                                                     )

    user_uuid = str(uuid.uuid4())
    first_name = 'Visesh'
    last_name = 'Prasad'
    username = first_name + '-' + last_name + '-' + user_uuid
    user, created = lab_tenant_models.User.objects.update_or_create(first_name=first_name,
                                                                    last_name=last_name,
                                                                    user_type=20,
                                                                    defaults={
                                                                        'username': username
                                                                    })
    patients_models.Patient.objects.update_or_create(birth_date='1974-08-12',
                                                     sex='M',
                                                     user=user,
                                                     )

    user_uuid = str(uuid.uuid4())
    first_name = 'Katie'
    last_name = 'Slanders'
    username = first_name + '-' + last_name + '-' + user_uuid
    user, created = lab_tenant_models.User.objects.update_or_create(first_name=first_name,
                                                                    last_name=last_name,
                                                                    user_type=20,
                                                                    defaults={
                                                                        'username': username
                                                                    })
    patients_models.Patient.objects.update_or_create(birth_date='1994-07-12',
                                                     sex='F',
                                                     user=user,
                                                     )


def initialize_dummy_orders(num_orders):
    urine_sample_type = lab_models.CLIASampleType.objects.get(name='Urine')
    test_panel_type = models.TestPanelType.objects.get(name='Urinary Tract Microbiota TaqMan™ Assay Panel')

    days_back = 60
    received_date_start = timezone.now() - datetime.timedelta(days=days_back)
    average_order_count = int(num_orders / days_back)

    for order_count in range(num_orders):
        latest_order_code = generate_order_code()
        rand_account = accounts_models.Account.objects.get(id=random.randint(1, 9))
        rand_patient = patients_models.Patient.objects.get(id=random.randint(1, 11))
        logging.debug('Order {}: {} {} '.format(order_count, rand_account.name, rand_patient.user.first_name))
        order = models.Order.objects.create(
            provider_id=random.randint(1, 5),
            account=rand_account,
            patient=rand_patient,
            code=latest_order_code,
            research_consent=False,
            patient_result_request=False,

        )
        order.received_date = received_date_start
        order.save(update_fields=['received_date'])

        latest_sample_code = generate_sample_code(order.code, urine_sample_type)
        sample = models.Sample.objects.create(
            order=order,
            code=latest_sample_code,
            clia_sample_type=urine_sample_type,
            collection_date=timezone.now()
        )
        sample_barcode = get_sample_barcode(sample.id)
        sample.barcode = sample_barcode
        sample.save(update_fields=['barcode'])

        test_panel = models.TestPanel.objects.create(
            order=order,
            test_panel_type=test_panel_type,
            expected_revenue=test_panel_type.cost,
        )

        for test_type in test_panel_type.test_types.all():
            test = models.Test.objects.create(
                order=order,
                sample=sample,
                test_panel=test_panel,
                test_type=test_type,
                due_date=datetime.date.today() + datetime.timedelta(days=test_type.due_date_days)
            )
            result_obj = models.Result.objects.create(
                order=order,
                test=test
            )
        order.submitted = True
        order.save(update_fields=['submitted'])

        if random.randint(1, average_order_count + 1) == average_order_count + 1:
            if received_date_start + datetime.timedelta(days=1, hours=2) < timezone.now():
                received_date_start = received_date_start + datetime.timedelta(days=1, hours=2)


def initialize_in_transit_orders():
    urine_sample_type = lab_models.CLIASampleType.objects.get(name='Urine')
    test_panel_type = models.TestPanelType.objects.get(name='Urinary Tract Microbiota TaqMan™ Assay Panel')

    latest_order_code = generate_order_code()
    rand_account = accounts_models.Account.objects.get(id=random.randint(1, 9))
    rand_patient = patients_models.Patient.objects.get(id=random.randint(1, 11))
    logging.debug('In transit order Order: {} {} '.format(rand_account.name, rand_patient.user.first_name))
    order = models.Order.objects.create(
        provider_id=1,
        account=rand_account,
        patient=rand_patient,
        code=latest_order_code,
        research_consent=False,
        patient_result_request=False,

    )

    latest_sample_code = generate_sample_code(order.code, urine_sample_type)
    sample = models.Sample.objects.create(
        order=order,
        code=latest_sample_code,
        clia_sample_type=urine_sample_type,
        collection_date=timezone.now()
    )
    sample_barcode = get_sample_barcode(sample.id)
    sample.barcode = sample_barcode
    sample.save(update_fields=['barcode'])

    test_panel = models.TestPanel.objects.create(
        order=order,
        test_panel_type=test_panel_type,
        expected_revenue=test_panel_type.cost,
    )

    for test_type in test_panel_type.test_types.all():
        test = models.Test.objects.create(
            order=order,
            sample=sample,
            test_panel=test_panel,
            test_type=test_type,
            due_date=datetime.date.today() + datetime.timedelta(days=test_type.due_date_days)
        )
        result_obj = models.Result.objects.create(
            order=order,
            test=test
        )
    order.submitted = True
    order.save(update_fields=['submitted'])


def initialize_data_in_demo():
    data_in = models.DataIn.objects.create(
        instrument_integration_id=1,
        original_file_name='quantstudio_01-56.txt',
        file='instrument_output_debug/demo/quantstudio_01-56.txt',
        created_datetime=timezone.now()
    )


if __name__ == '__main__':
    logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)
    main()
