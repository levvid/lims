import django
import logging
import os
import sys

from django.db import transaction
from tenant_schemas.utils import tenant_context

os.environ['DJANGO_SETTINGS_MODULE'] = 'main.settings'
django.setup()

from clients import models as client_models
from .. import models


@transaction.atomic
def main():
    tenants = client_models.Client.objects.exclude(schema_name='public')

    for tenant in tenants:
        with tenant_context(tenant):
            provider_list = models.Provider.objects.all()

            for provider in provider_list:
                user = provider.user

                # update username with email - if not, a username is auto-generated
                # see lab/models.py
                if user.email:
                    user.username = user.email

                # default provider password is the provider UUID
                user.set_password(str(provider.uuid))

                logging.debug("Converting {} - {}".format(tenant.name, provider))
                user.save()


if __name__ == '__main__':
    logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)
    main()
