# Generated by Django 2.2.2 on 2019-10-21 17:14

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('orders', '0100_auto_20191018_0058'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='testpaneltype',
            name='exclude_from_menu',
        ),
    ]
