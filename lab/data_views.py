import datetime

from django.http import JsonResponse
from django.utils.html import format_html

from main.functions import convert_utc_to_tenant_timezone, date_string_or_none

from . import models
from patients import models as patients_models
from orders import models as orders_models
from accounts import models as accounts_models

DEFAULT_CUTOFF = 500

def clia_test_type_table(request, complexity, specialty, start_year, start_month, start_day,
                         end_year, end_month, end_day, text_filter):
    """
    data view for test type table
    :param request:
    :return:
    """
    # date inclusive search and no need to convert timezones, since this isn't our own data
    start_date = datetime.date(start_year, start_month, start_day)
    end_date = datetime.date(end_year, end_month, end_day) + datetime.timedelta(days=1)
    test_types = models.CLIATestType.objects.filter(date_effective__gte=start_date,
                                                    date_effective__lt=end_date).order_by('test_system_name')

    if specialty != 'All':
        test_types = test_types.filter(specialty_id=specialty)
    if complexity != 'All':
        test_types = test_types.filter(complexity=complexity)
    if text_filter != 'null':
        test_types = test_types.filter(document_number__icontains=text_filter) | \
                     test_types.filter(test_system_id__icontains=text_filter) | \
                     test_types.filter(test_system_name__icontains=text_filter) | \
                     test_types.filter(analyte_name__icontains=text_filter)
        test_types = test_types.distinct()

    results = []
    for test_type in test_types:
        if test_type.analyte_name:
            target_name = test_type.analyte_name
        else:
            target_name = ''

        test_type_data = {
            'name': test_type.test_system_name,
            'target': target_name,
            'complexity': test_type.complexity.title(),
            'import_link': '<a href="/test_types/import/{}/">Import</a>'.format(
                test_type.uuid,
            )
        }

        results.append(test_type_data)
    return JsonResponse(results, safe=False)


def medication_table_filtered(request, name, common_name):
    medications = models.TestTarget.objects.filter(is_medication=True).filter().order_by('name')
    if name != '_':
        medications = medications.filter(name__istartswith=name)
    if common_name != '_':
        medications = medications.filter(common_name__icontains=common_name)

    results = []

    for medication in medications:
        common_name_string = ''

        if medication.common_name:
            common_name_string = ', '.join(medication.common_name)

        medication_data = {
            'name': medication.name,
            'common_name': common_name_string
        }

        results.append(medication_data)

    return JsonResponse(results, safe=False)


def payer_table(request, payer_name, payer_type, cutoff=DEFAULT_CUTOFF):
    """
    Tenant payers data view

    :return:
    """
    payers = patients_models.Payer.objects.all().exclude(name='')

    if payer_name != '_':
        payers = payers.filter(name__istartswith=payer_name)

    if payer_type != 'All':
        payers = payers.filter(payer_type=payer_type)

    total_payer_count = payers.count()
    payers = payers[:cutoff]
    results = []

    for payer in payers:
        patient_list = patients_models.PatientPayer.objects.filter(payer=payer).values_list('patient').distinct()
        # get associated accounts - only associated with patient if an order has been created through this account
        account_list = orders_models.Order.objects.filter(patient__in=patient_list).values_list('account').distinct()
        account_count = accounts_models.Account.objects.filter(id__in=account_list)
        results.append({
            'payer_name': {
                'text': payer.name,
                'url': '<a href="/payers/{}/">{}</a>'.format(payer.uuid, payer.name),
            },
            'payer_type': payer.payer_type,
            'payer_code': payer.payer_code,
            'alternate_id': payer.alternate_id,
            'patient_count': patients_models.PatientPayer.objects.filter(payer=payer).count(),

        })

    response = {
        'results': results,
        'returned_payer_count': payers.count(),
        'total_payer_count': total_payer_count
    }
    return JsonResponse(response, safe=False)


def archived_payers(request, payer_name, payer_type, cutoff=DEFAULT_CUTOFF):
    """
    Tenant payers data view

    :return:
    """
    payers = patients_models.Payer.all_objects.exclude(name='').filter(is_active=False)

    if payer_name != '_':
        payers = payers.filter(name__istartswith=payer_name)

    if payer_type != 'All':
        payers = payers.filter(payer_type=payer_type)

    total_payer_count = payers.count()
    payers = payers[:cutoff]
    results = []

    for payer in payers:
        payer_undo_archive_link = format_html(
            '<a href="/payers/{}/undo_delete/"><i class="far fa-trash-restore"></i></a>',
            payer.uuid
        )

        results.append({
            'payer_name': payer.name,
            'payer_type': payer.payer_type,
            'payer_code': payer.payer_code,
            'patient_count': patients_models.PatientPayer.objects.filter(payer=payer).count(),
            'alternate_id': payer.alternate_id,
            'payer_undo_archive_link': payer_undo_archive_link
        })

    response = {
        'results': results,
        'returned_payer_count': payers.count(),
        'total_payer_count': total_payer_count
    }
    return JsonResponse(response, safe=False)
