from django.db import models
from django.contrib.auth.models import BaseUserManager, UserManager


# As can be seen here: https://github.com/django/django/blob/master/django/shortcuts.py#L83,
# the get_object_or_404 method uses the _get_queryset method, which in turn uses a default manager.
# According to the official Django documentation, the default manager is called "objects" (as in Model.objects.all())

# However, if you use custom Manager objects, take note that the first Manager Django encounters (in the order in which
# they’re defined in the model) has a special status. Django interprets the first Manager defined in a class as the
# “default” Manager, and several parts of Django (including dumpdata) will use that Manager exclusively for that model.

# For our purposes, we override Django's default manager (objects). This means that
# get_object_or_404 and get_list_or_404 methods both return only active objects.

# Managers are used in AuditLogModel, which every model in our app inherits from

class ActiveManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().filter(is_active=True)


class AllManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().all()


class ActiveUserManager(UserManager):
    def get_queryset(self):
        return super().get_queryset().filter(is_active=True)

    def get_by_natural_key(self, username):
        case_insensitive_username_field = '{}__iexact'.format(self.model.USERNAME_FIELD)
        return self.get(**{case_insensitive_username_field: username})


class AllUserManager(UserManager):
    def get_queryset(self):
        return super().get_queryset().all()

