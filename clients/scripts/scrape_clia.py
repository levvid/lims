import csv
import django
import os
import datetime, time
import requests
from django.db import transaction

os.environ['DJANGO_SETTINGS_MODULE'] = 'main.settings'
django.setup()

from .. import models
from lab import models as lab_models

from urllib.request import urlopen
from bs4 import BeautifulSoup


# CLIA state list (excluding CA, FL, IL, OH, TX)
STATE_LIST = ["AL", "AK", "AZ", "AR", "CO", "CT", "DE", "GA", "HI", "ID", "IN", "IA",
              "KS", "KY", "LA", "ME", "MT", "NE", "NV", "NH", "NJ", "NM", "NY", "NC", "ND", "OK", "OR", "MD",
              "MA", "MI", "MN", "MS", "MO", "PA", "RI", "SC", "SD", "TN", "UT", "VT", "VA", "WA", "WV", "WI", "WY"]
STATES_TO_QUERY_BY_ZIPCODE = ["FL", "IL", "OH", "TX", "CA"]

# format url with state abbreviation and zipcode.
# pass '' for zipcode to query by state
STATE_ZIPCODE_LOOKUP_URL = "https://www.cms.gov/apps/clia/clia_start.asp?CLIANum=&LabName=&GeoCity=&state={}&GeoZip={}&appType=%25&isSubmitted=clia2"


######################################################################################################################


def scrape_clia_labs(clia_url):
    """
    Scrape CLIA laboratory demographics from a search url
    :param clia_url: String
    :return: [[clinic_name, lab_type, certification_type, phone_number, certificate_expiration, clia_number,
                street, city, state, zip_code]]
    """
    print('Scraping the CLIA website...')
    request_headers = {
        'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
        'accept-encoding': 'gzip, deflate, br',
        'accept-language': 'en-US,en;q=0.8',
        'upgrade-insecure-requests': '1',
        'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36'
    }

    with requests.Session() as session:
        response = session.get(clia_url, headers=request_headers, timeout=10)
        if not response:  # HTTP Error
            print('HTTP error: {}'.format(response.status_code))
            error = 'Code: {}, url: {}'.format(response.status_code, clia_url)
            return error
        soup = BeautifulSoup(response.text, 'html.parser')
        rows = soup.select('tr.rowcolor1') + soup.select('tr.rowcolor2')

        # comma delimited but many names have commas in them, so quotation marks used --> "Dendi, LLC"
        clinic_name_list = [row.findAll('h5')[0].text.strip() for row in rows]
        certificate_type_list = [' '.join(row.findAll('td')[0].text.strip().split()) for row in rows]
        phone_number_list = [' '.join(row.findAll('td')[2].text.strip().split()) for row in rows]
        certificate_expiration_list = [' '.join(row.findAll('td')[3].text.strip().split()) for row in rows]
        lab_type_list = [' '.join(row.findAll('td')[4].text.strip().split()) for row in rows]
        clia_number_list = [row.findAll('td')[1].findAll(text=True)[-1] for row in rows]

        # address list
        street_list = []
        city_list = []
        state_list = []
        zip_code_list = []

        for row in rows:
            # address for each lab
            address_parts = row.findAll('td')[1].findAll(text=True)[1:-1]

            address_str = list(map(str.strip, address_parts))

            street_str = address_str[0]
            city_str = address_str[-1].split(',')[0]
            state_str = address_str[-1].split(',')[1].strip().split(' ')[0]
            zip_code_str = address_str[-1].split(',')[1].strip().split(' ')[1]

            street_list.append(street_str)
            city_list.append(city_str)
            state_list.append(state_str)
            zip_code_list.append(zip_code_str)

        # CLIA number is the last sibling - if there are no more nextSibling objects

        # zip everything together
        lab_list = list(zip(clinic_name_list, lab_type_list, certificate_type_list, phone_number_list,
                            certificate_expiration_list, clia_number_list,
                            street_list, city_list, state_list, zip_code_list))
        return lab_list


def check_difference_and_save(lab):
    """
    Check for the lab name - if exists, then don't add. If it doesn't exist, then create new one
    :param lab: [clinic_name, lab_type, certification_type, phone_number, certificate_expiration, clia_number,
                street, city, state, zip_code
    :param state:
    :return: bool: lab_created boolean
    """
    try:
        lab_name = lab[0]
        lab_type = lab[1]
        lab_cert_type = lab[2]
        lab_phone = lab[3]
        lab_cert_expiration_date = datetime.datetime.strptime(lab[4], "%m/%d/%Y").strftime("%Y-%m-%d")
        lab_clia_number = lab[5]
        lab_address = lab[6]
        lab_city = lab[7]
        lab_state = lab[8]
        lab_zip_code = lab[9]

        # if already exists (by CLIA number, which should be unique and immutable), then update
        # if doesn't exist, create new record
        obj, created = models.CLIALabs.objects.update_or_create(
            clia_number=lab_clia_number,
            defaults={'name': lab_name, 'lab_type': lab_type,
                      'phone_number': lab_phone, 'certificate_type': lab_cert_type,
                      'certificate_expiration_date': lab_cert_expiration_date,
                      'address': lab_address, 'city': lab_city, 'state': lab_state, 'zip_code': lab_zip_code}
        )

        if created:
            return True

        else:
            print('CLIA Lab already exists: ' + str(obj))
            return False
    except Exception as e:
        print(e)
        return False


def get_lab_clianumbers():
    """
    Query database for existing laboratories and return their clia_numbers in a dict
    """
    print("Retrieving existing labs clia_numbers...")
    lab_clia_numbers = models.CLIALabs.objects.values("clia_number")
    clia_numbers = []
    for clia_num_dict in lab_clia_numbers:
        clia_numbers.append(clia_num_dict['clia_number'])
    print("{} lab clia_numbers retrieved successfully!".format(len(clia_numbers)))
    return clia_numbers


def save_labs(lab_list):
    new_lab_counter = 0
    for lab in lab_list:
        created = check_difference_and_save(lab)
        if created:
            new_lab_counter += 1
            print('New CLIA Lab ' + str(new_lab_counter) + ' added to database: ' + lab[8] + ' - ' + lab[0])
        else:
            print('CLIA Lab already exists: {} {}'.format(lab[8], lab[0]))
    return new_lab_counter


@transaction.atomic
def main():
    """
    large states are queried by zip code as per CMS directions:
    When searching for labs in California, Florida, Illinois, Ohio, and Texas,
    you must enter a zip code due to the large number of labs in each of those states
    :return:
    """
    states = STATE_LIST
    large_states = STATES_TO_QUERY_BY_ZIPCODE
    new_lab_counter = 0
    error_tracker = []

    existing_labs_clia_numbers = get_lab_clianumbers()

    for state in large_states:
        print("Scraping for labs in {}".format(state))
        zip_codes = lab_models.USZipCode.objects.filter(state=state,
                                                        decommissioned=False).all()
        for zip_code in zip_codes:
            new_labs = []
            try:
                lab_list = scrape_clia_labs(STATE_ZIPCODE_LOOKUP_URL.format(state, zip_code.zipcode))
                if isinstance(lab_list, list):  # if no error was returned
                    for lab in lab_list:
                        if lab[5] not in existing_labs_clia_numbers:  # lab is new
                            new_labs.append(lab)
                else:  # HTTP error occurred
                    error_tracker.append(lab_list)
            except Exception as e:
                print(e)
            new_lab_counter += save_labs(new_labs)

    for state in states:
        new_labs = []
        print("Scraping for labs in {}".format(state))
        # loop through each state but skip without failing if there's an error
        try:
            lab_list = scrape_clia_labs(STATE_ZIPCODE_LOOKUP_URL.format(state, ""))
            if isinstance(lab_list, list):  # if no error was returned
                for lab in lab_list:
                    if lab[5] not in existing_labs_clia_numbers:  # lab is new
                        new_labs.append(lab)
            else:  # HTTP error occurred
                error_tracker.append(lab_list)
        except Exception as e:
            print(e)

        new_lab_counter += save_labs(new_labs)

    update_log = models.CLIALabsUpdateLog(new_lab_count=0)
    if new_lab_counter:
        update_log.new_lab_count = new_lab_counter
    if len(error_tracker) > 0:
        update_log.error_count = len(error_tracker)
        update_log.errors = error_tracker
    update_log.save()


if __name__ == '__main__':
    # run using python -m clients.scripts.scrape_clia
    print('Running...')
    main()
