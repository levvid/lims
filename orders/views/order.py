import datetime
import decimal
import copy
import math
import pathlib
import uuid

from braces.views import LoginRequiredMixin
from django.db.models import IntegerField, BigIntegerField
from django.db.models.functions import Cast
from django_tables2 import RequestConfig
from django.contrib import messages
from django.db import transaction
from django.forms import formset_factory
from django.http import HttpResponseRedirect
from django.shortcuts import render, redirect, get_object_or_404
from django.views.generic import View
from django.urls import reverse
from django.utils import timezone
from django.core.exceptions import ObjectDoesNotExist

from orders import forms, generate_requisition_form
from orders import generate_billing_hl7
from orders import models
from orders import filters
from orders import tasks

from lab_tenant.views import LabMemberMixin, LISLoginRequiredMixin
from accounts import models as accounts_models
from lab import models as lab_models
from lab_tenant.models import User
from patients import models as patients_models

from orders.functions import add_result, get_test_panel_forms, get_order_reflex, get_sample_barcode, \
    get_test_method_abbreviated_names, get_result_forms, report_order, approve_all_test_results, save_test_results, \
    update_test_due_dates
from main.functions import convert_utc_to_tenant_timezone, convert_tenant_timezone_to_utc
from main.settings import DATA_UPLOAD_MAX_MEMORY_SIZE, FILE_UPLOAD_MAX_MEMORY_SIZE

ALLOWED_FILE_EXTENSIONS = ['png', 'svg', 'pdf', 'jpeg', 'jpg']


class OrderListView(LISLoginRequiredMixin, View):

    def get(self, request):
        orders = models.Order.objects.filter(submitted=True).exclude(issues__is_resolved=False)
        if request.user.user_type == 10:
            provider = accounts_models.Provider.objects.get(user__id=request.user.id)
            orders = orders.filter(provider=provider)
            order_autocomplete_form = forms.OrderAutocompleteForm(initial={'provider': provider})
        elif request.user.user_type == 9:
            account = accounts_models.Account.objects.get(user__id=request.user.id)
            orders = orders.filter(account=account)
            order_autocomplete_form = forms.OrderAutocompleteForm(initial={'account': account})
        else:
            order_autocomplete_form = forms.OrderAutocompleteForm()

        test_types = models.TestType.objects.all()

        # table filtering
        order_filter = filters.OrderFilter(request.GET, queryset=orders)
        code_filter = order_filter.data.get('code')
        if code_filter:
            orders = orders.filter(code__icontains=code_filter)

        num_in_transit = orders.filter(submitted=True, received_date__isnull=True).count()
        num_pending_results = orders.filter(submitted=True, received_date__isnull=False,
                                            completed=False).count()
        # orders with reports that have not been sent
        num_pending_qa = orders.filter(submitted=True, received_date__isnull=False,
                                       completed=True, reported=False).count()
        context = {'orders': orders,
                   'num_pending_results': num_pending_results,
                   'num_pending_qa': num_pending_qa,
                   'num_in_transit': num_in_transit,
                   'form': order_autocomplete_form,
                   'order_filter': order_filter,
                   'test_types': test_types,}

        return render(request, 'order_list.html', context=context)

    def post(self, request):
        order_autocomplete_form = forms.OrderAutocompleteForm(request.POST)

        if order_autocomplete_form.is_valid():
            order = order_autocomplete_form.cleaned_data['order']
            return redirect('order_details', order_code=order.code)


class OrderDetailsView(LISLoginRequiredMixin, View):

    def is_pending_research_consent(self, request, order_code):
        has_research_test = False
        order = get_object_or_404(models.Order, code=order_code)

        if order.research_consent:
            return False

        test_panels = order.test_panels.all()
        for test_panel in test_panels:
            tests = test_panel.tests.all().prefetch_related('test_type')
            for test in tests:
                test_type = test.test_type
                if test_type and test_type.is_research_only:
                    has_research_test = True
        if has_research_test and not order.research_consent:
            return True
        else:
            return False

    def missing_medical_necessity_codes(self, request, order_code):
        missing_codes = False

        order = get_object_or_404(models.Order, code=order_code)
        icd10_codes = order.icd10_codes.all()
        test_panels = order.test_panels.all().prefetch_related('tests', 'tests__test_type',
                                                               'tests__test_type__icd10_codes')

        for test_panel in test_panels:
            tests = test_panel.tests.all()
            for test in tests:
                if test.test_type:
                    necessity_codes = test.test_type.icd10_codes.all()
                    intersection = set(icd10_codes).intersection(necessity_codes)
                    if not intersection and necessity_codes:
                        missing_codes = True

        return missing_codes

    def submit_approver_and_generate_report(self, request, order, report_approved_by_form):
        if 'preliminary_report_button' in request.POST and report_approved_by_form.is_valid():
            num_tests = order.tests.filter(is_approved=True).count()
            # Only throws an error if there is "nothing to report" e.g.
            # the number of approved tests is 0 AND there are no unresolved, public issues.
            if num_tests == 0 and not order.has_unresolved_public_issues:
                messages.error(request, "Order must contain at least one approved result "
                                        "or contain at least one unresolved public issue.")
                return redirect('order_details', order_code=order.code)
            else:
                report_approved_by = report_approved_by_form.cleaned_data.get('user')
                report_order(request, order, report_approved_by, preliminary=True)
                messages.success(request, "The report has been updated.")
                return redirect('order_details', order_code=order.code)
        if 'order_report_button' in request.POST and report_approved_by_form.is_valid():
            num_tests = order.tests.filter(is_approved=True).count()
            if num_tests == 0 and not order.has_unresolved_public_issues:
                messages.error(request, "Order must contain at least one approved result "
                                        "or contain at least one unresolved public issue.")
                return redirect('order_details', order_code=order.code)
            else:
                report_approved_by = report_approved_by_form.cleaned_data.get('user')
                success = report_order(request, order, report_approved_by, preliminary=False)
                if success:
                    messages.success(request, "The report for this order is being generated.")
                return redirect('order_details', order_code=order.code)
        # return anyways so the rest of the form doesn't go through
        return redirect('order_details', order_code=order.code)

    def get(self, request, order_code):
        order = get_object_or_404(models.Order, code=order_code)
        order.submitted_date = convert_utc_to_tenant_timezone(request, order.submitted_date)
        order.received_date = convert_utc_to_tenant_timezone(request, order.received_date)
        order.completed_date = convert_utc_to_tenant_timezone(request, order.completed_date)
        order.report_date = convert_utc_to_tenant_timezone(request, order.report_date)

        has_reflex, reflex_list = get_order_reflex(order.code)

        reports = models.Report.objects.filter(
            order=order, preliminary=False).order_by('-created_date')
        preliminary_reports = models.Report.objects.filter(
            order=order, preliminary=True).order_by('-created_date')

        if request.user.user_type == 10:
            request_provider = accounts_models.Provider.objects.get(user__id=request.user.id)
            if order.provider.id != request_provider.id:
                return redirect('dashboard')
        if request.user.user_type == 9:
            request_account = accounts_models.Account.objects.get(user__id=request.user.id)
            if order.account.id != request_account.id:
                return redirect('dashboard')
        if request.user.user_type == 11:
            request_collector = accounts_models.Collector.objects.get(user__id=request.user.id)
            if order.account not in request_collector.accounts.all():
                return redirect('dashboard')

        provider = order.provider
        account = order.account
        patient = order.patient

        # PatientPayer objects for the order
        primary_patient_payer = models.OrderPatientPayer.objects.filter(order=order,
                                                                        is_primary=True).order_by('created_date').last()
        secondary_patient_payer = models.OrderPatientPayer.objects.filter(order=order,
                                                                          is_secondary=True).order_by(
            'created_date').last()
        tertiary_patient_payer = models.OrderPatientPayer.objects.filter(order=order,
                                                                         is_tertiary=True).order_by(
            'created_date').last()
        wc_patient_payer = models.OrderPatientPayer.objects.filter(order=order,
                                                                   is_workers_comp=True).order_by('created_date').last()
        # PatientGuarantor object for the order
        patient_guarantor = models.OrderPatientGuarantor.objects.filter(order=order).last()

        try:
            patient_status = order.patient_status.get()
            prescribed_medication_history = patients_models.PatientPrescribedMedicationHistory.objects.filter(
                order_id=order, provider_id=provider, patient_id=patient). \
                prefetch_related('prescribed_medications').order_by('-created_date').first()
            if prescribed_medication_history:
                prescribed_medications = prescribed_medication_history.prescribed_medications.all()
            else:
                prescribed_medications = None
        except patients_models.PatientStatus.DoesNotExist:
            patient_status = None
            prescribed_medications = None

        test_panels = order.test_panels.all().prefetch_related('test_panel_type', 'test_panel_type__procedure_codes')
        samples = order.samples.order_by('collection_date').all().prefetch_related('containers')

        order_test_methods = {}
        for sample in samples:
            sample.collection_date = convert_utc_to_tenant_timezone(request, sample.collection_date)
            test_methods = get_test_method_abbreviated_names(sample)
            order_test_methods.update({sample.code: test_methods})
            for container in sample.containers.all():
                container.collection_date = convert_utc_to_tenant_timezone(request, container.collection_date)

        # Get procedure codes associated with the test_panel_types in this order.
        procedure_codes_unique = set()
        procedure_codes_queries = [test_panel.test_panel_type.procedure_codes.all() for test_panel in test_panels]
        for query in procedure_codes_queries:
            procedure_codes_unique = procedure_codes_unique.union(query)
        procedure_codes = [procedure_code.code for procedure_code in procedure_codes_unique]
        procedure_codes.sort()

        icd10_codes = order.icd10_codes.all()
        tests = order.tests.all()
        editable_order_status_list = ['Unreceived', 'Pending Results']
        context = get_result_forms(request, order_code)

        approved_tests = tests.filter(is_approved=True).all()
        # comment form
        comment_form = forms.OrderCommentForm()

        order_comments = order.comments.all().order_by('-created_date')
        for order_comment in order_comments:
            if len(order_comment.history.all()) > 1:
                last_comment = order_comment.history.order_by('-timestamp').first()
                order_comment.last_modified_date = last_comment.timestamp
                actor_id = last_comment.actor_id
                order_comment.last_modified_user = User.all_objects.get(id=actor_id)

        auto_print_label = request.session.get('print_labels', False)
        use_zebra_printer = request.tenant.settings.get('use_zebra_printer', False)
        enable_document_uploading = request.tenant.settings.get('enable_document_uploading', False)
        enable_document_scanning = request.tenant.settings.get('enable_document_scanning', False)
        request.session['print_labels'] = False

        documents = models.OrderAdditionalDocuments.objects.filter(order=order)

        # research consent pending flag
        if self.is_pending_research_consent(request, order_code):
            messages.error(request, "This order is missing research consent.")
        if self.missing_medical_necessity_codes(request, order_code):
            messages.error(request, "This order is missing required medical necessity codes.")

        context.update({'order': order,
                        'primary_patient_payer': primary_patient_payer,
                        'secondary_patient_payer': secondary_patient_payer,
                        'tertiary_patient_payer': tertiary_patient_payer,
                        'wc_patient_payer': wc_patient_payer,
                        'patient_guarantor': patient_guarantor,
                        'reports': reports,
                        'preliminary_reports': preliminary_reports,
                        'account': account,
                        'provider': provider,
                        'patient': patient,
                        'patient_status': patient_status,
                        'prescribed_medications': prescribed_medications,
                        'test_panels': test_panels,
                        'samples': samples,
                        'icd10_codes': icd10_codes,
                        'procedure_codes': procedure_codes,
                        'tests': tests,
                        'approved_tests': approved_tests,
                        'editable_order_status_list': editable_order_status_list,
                        'comment_form': comment_form,
                        'order_comments': order_comments,
                        'has_reflex': has_reflex,
                        'reflex_list': reflex_list,
                        'auto_print_label': auto_print_label,
                        'use_zebra_printer': use_zebra_printer,
                        'order_test_methods': order_test_methods,
                        'documents': documents,
                        'enable_document_scanning': enable_document_scanning,
                        'enable_document_uploading': enable_document_uploading})

        return render(request, 'order_detail.html', context)

    @transaction.atomic
    def post(self, request, order_code):
        order = get_object_or_404(models.Order, code=order_code)
        if 'order_comment_button' in request.POST:
            comment_form = forms.OrderCommentForm(request.POST)
            if comment_form.is_valid():
                comment = models.OrderComment(
                    order=order,
                    user=request.user,
                    comment=comment_form.cleaned_data['comment']
                )
                comment.save()
                return redirect('order_details', order_code=order_code)
            else:
                print("Did not find file")
                return redirect('order_details', order_code=order_code)
        if 'order_additional_documents' in request.POST:
            uploaded_document = request.FILES.get('order_document', False)
            if uploaded_document:
                # validate file before saving
                # size
                if uploaded_document.size > FILE_UPLOAD_MAX_MEMORY_SIZE:
                    print('This is the size: {}'.format(uploaded_document.size))
                    messages.error(request,
                                   'This file\'s size is {} MB, which is greater the maximum allowed size of {} MB.'
                                   .format(math.ceil(uploaded_document.size / (1024 * 1024)),
                                           math.ceil((FILE_UPLOAD_MAX_MEMORY_SIZE / (1024 * 1024)))))
                    return redirect('order_details', order_code=order_code)
                document_extension = pathlib.Path(uploaded_document.name).suffix.lower()
                if any(ext in document_extension for ext in ALLOWED_FILE_EXTENSIONS):
                    order_document = models.OrderDocuments(
                        order=order,
                        user=request.user,
                        document_name=uploaded_document.name
                    )
                    order_document.document.save(uploaded_document.name, uploaded_document)
                    messages.success(request, 'File uploaded successfully: {}.'.format(uploaded_document.name))
                    return redirect('order_details', order_code=order_code)
                else:
                    messages.error(request,
                                   'Uploaded file must have a valid extension. Allowed filetypes are: PDF, '
                                   'JPEG, JPG, PNG, and SVG.')
            else:
                messages.error(request, 'Please choose a file before uploading.')

        context = get_result_forms(request, order_code)

        ################################################################################################################
        # preliminary and report order buttons
        report_approved_by_form = context['report_approved_by_form']
        if 'preliminary_report_button' in request.POST or 'order_report_button' in request.POST:
            return self.submit_approver_and_generate_report(request, order, report_approved_by_form)
        ################################################################################################################
        # save test results
        no_form_errors = save_test_results(request, order, context)
        if 'complete_order_button' in request.POST and no_form_errors:
            return redirect('order_complete', order_code=order_code)

        return redirect('order_details', order_code=order_code)


class OrderNextView(LISLoginRequiredMixin, View):
    def get(self, request, order_code):
        order = get_object_or_404(models.Order, code=order_code)

        next_order = models.Order.objects. \
            filter(
            submitted_date__gt=order.submitted_date,
            received_date__isnull=False). \
            exclude(issues__is_resolved=False). \
            order_by('submitted_date').first()

        if next_order:
            return redirect('order_details', order_code=next_order.code)
        else:
            messages.error(request, 'This is the most recent received order. There are no more orders ahead of it.')
            return redirect('order_details', order_code=order.code)


class OrderPreviousView(LISLoginRequiredMixin, View):
    def get(self, request, order_code):
        order = get_object_or_404(models.Order, code=order_code)

        previous_order = models.Order.objects. \
            filter(
            submitted_date__lt=order.submitted_date,
            received_date__isnull=False). \
            exclude(
            issues__is_resolved=False,
            id=order.id
        ). \
            order_by('-submitted_date').first()

        if previous_order:
            return redirect('order_details', order_code=previous_order.code)
        else:
            messages.error(request, 'This is the oldest received order. There are no more orders before it.')
            return redirect('order_details', order_code=order.code)


class OrderNextAccessionView(LISLoginRequiredMixin, View):
    def get(self, request, order_code):
        order = get_object_or_404(models.Order, code=order_code)

        next_order = models.Order.objects. \
            annotate(
            accession_number_int=Cast('accession_number', BigIntegerField())
        ). \
            filter(received_date__isnull=False, accession_number_int__gt=int(order.accession_number)). \
            exclude(
            issues__is_resolved=False,
            id=order.id
        ). \
            order_by('accession_number_int').first()

        if next_order:
            return redirect('order_details', order_code=next_order.code)
        else:
            messages.error(request, 'This is the most recent order accession. There are no more orders ahead of it.')
            return redirect('order_details', order_code=order.code)


class OrderPreviousAccessionView(LISLoginRequiredMixin, View):
    def get(self, request, order_code):
        order = get_object_or_404(models.Order, code=order_code)

        previous_order = models.Order.objects. \
            annotate(
            accession_number_int=Cast('accession_number', BigIntegerField())
        ). \
            filter(received_date__isnull=False, accession_number_int__lt=int(order.accession_number)). \
            exclude(
            issues__is_resolved=False,
            id=order.id
        ). \
            order_by('-accession_number_int').first()

        if previous_order:
            return redirect('order_details', order_code=previous_order.code)
        else:
            messages.error(request, 'This is the oldest order accession. There are no more orders before it.')
            return redirect('order_details', order_code=order.code)


class OrderCompleteView(LabMemberMixin, View):
    def get(self, request, order_code):
        order = get_object_or_404(models.Order, code=order_code)

        if not order.completed:
            num_completed_results = order.results.filter(result__isnull=False).count()
            # if order has no completed tests and no unresolved public issues, throw error
            if num_completed_results == 0 and not order.has_unresolved_public_issues:
                messages.error(
                    request, "Order must contain at least one approved result "
                             "or contain at least one unresolved public issue.")
                return redirect('order_details', order_code=order_code)

        order.completed = True
        order.completed_date = convert_utc_to_tenant_timezone(request, timezone.now())
        order.save(update_fields=['completed', 'completed_date'])
        return redirect('order_details', order_code=order_code)


class OrderArchivedListView(LISLoginRequiredMixin, View):
    def get(self, request):
        test_types = models.TestType.objects.all()

        context = {
            'test_types': test_types,
        }
        return render(request, 'order_archived_list.html', context=context)


class OrderEditView(LabMemberMixin, View):
    def get(self, request, order_code):
        order = get_object_or_404(models.Order, code=order_code)
        order.completed = False
        order.save(update_fields=['completed'])
        generate_requisition_form.generate_requisition_PDF(request, order.uuid)

        return redirect('order_details', order_code=order_code)


class OrderUpdateView(LabMemberMixin, View):
    def get(self, request, order_code):
        order = get_object_or_404(models.Order, code=order_code)

        order_received_date = convert_utc_to_tenant_timezone(request, order.received_date)
        received_date = order_received_date.date().strftime('%m-%d-%Y')
        received_time = order_received_date.time().strftime('%H:%M')

        # convert received date to tenant timezone
        form = forms.OrderUpdateForm(initial={'received_date': received_date,
                                              'received_time': received_time,
                                              'research_consent': order.research_consent,
                                              'patient_result_request': order.patient_result_request,
                                              'account': order.account,
                                              'provider': order.provider,
                                              'patient': order.patient,
                                              'icd10_codes': order.icd10_codes.filter(),
                                              'swap_accession_number_with': order,
                                              'alternate_id': order.alternate_id},
                                     order_id=order.id)
        return render(request, 'order_update.html', {'form': form,
                                                     'order': order})

    @transaction.atomic
    def post(self, request, order_code):
        order = get_object_or_404(models.Order, code=order_code)
        form = forms.OrderUpdateForm(request.POST, order_id=order.id)

        if form.is_valid():
            order = get_object_or_404(models.Order, code=order_code)
            received_date = form.cleaned_data['received_date']
            received_time = form.cleaned_data['received_time']
            research_consent = form.cleaned_data['research_consent']
            patient_result_request = form.cleaned_data['patient_result_request']
            account = form.cleaned_data['account']
            provider = form.cleaned_data['provider']
            patient = form.cleaned_data['patient']
            chosen_icd10_codes = form.cleaned_data['icd10_codes']
            swap_accession_number_with = form.cleaned_data['swap_accession_number_with']
            alternate_id = form.cleaned_data['alternate_id']

            # remove all icd codes that aren't in the form
            for icd10_code in order.icd10_codes.all():
                if icd10_code not in chosen_icd10_codes:
                    order.icd10_codes.remove(icd10_code)

            # add all icd codes that are in the form, but aren't already included
            for icd10_code in chosen_icd10_codes:
                if icd10_code not in order.icd10_codes.all():
                    order.icd10_codes.add(icd10_code)

            # combine received date and time and convert to UTC
            order.received_date = convert_tenant_timezone_to_utc(request, datetime.datetime.combine(received_date,
                                                                                                    received_time))
            order.research_consent = research_consent
            order.patient_result_request = patient_result_request
            order.account = account
            order.provider = provider
            order.patient = patient
            order.alternate_id = alternate_id
            order.save()

            tests = order.tests.all().prefetch_related('test_type')
            test_list = []
            if order.received_date:
                for test in tests:
                    test_type = test.test_type
                    if test_type.turnaround_time_unit == 'Days':
                        test.due_date = received_date + datetime.timedelta(days=test_type.turnaround_time)
                    elif test_type.turnaround_time_unit == 'Hours':
                        test.due_date = received_date + datetime.timedelta(hours=test_type.turnaround_time)
                    else:  # Minutes
                        test.due_date = received_date + datetime.timedelta(minutes=test_type.turnaround_time)
                    test.save()
                    test_list.append(test)
            models.Test.objects.bulk_update(test_list, ['due_date'])
            generate_requisition_form.generate_requisition_PDF(request, order.uuid)

            if swap_accession_number_with and swap_accession_number_with.id != order.id:
                # set primary accession number to a temp variable
                accession_number_a = order.accession_number
                order.accession_number = accession_number_a + '!'
                order.save(update_fields=['accession_number'])
                # set secondary accession number to accession number a
                accession_number_b = swap_accession_number_with.accession_number
                swap_accession_number_with.accession_number = accession_number_a
                swap_accession_number_with.save(update_fields=['accession_number'])
                # set primary order accession number
                order.accession_number = accession_number_b
                order.save(update_fields=['accession_number'])
                messages.success(request,
                                 'Order updated successfully. <br>Accession Number: {} was swapped with '
                                 'Accession Number: {}. <br><br>Please reprint sample labels for the affected orders.'.format(
                                     accession_number_a,
                                     accession_number_b
                                 ))
            else:
                messages.success(request, 'Order updated successfully.')
        else:
            return render(request, 'order_update.html', {'form': form,
                                                         'order': order})
        return redirect('order_details', order_code)


class OrderPatientPayerUpdateView(LISLoginRequiredMixin, View):
    def get(self, request, order_code):
        order = get_object_or_404(models.Order, code=order_code)
        patient = order.patient
        primary_patient_payer = patient.order_patient_payers.filter(is_primary=True, order=order).last()
        secondary_patient_payer = patient.order_patient_payers.filter(is_secondary=True, order=order).last()
        tertiary_patient_payer = patient.order_patient_payers.filter(is_tertiary=True, order=order).last()
        workers_comp_payer = patient.order_patient_payers.filter(is_workers_comp=True, order=order).last()
        patient_guarantor = models.OrderPatientGuarantor.objects.filter(patient=patient, order=order).last()

        # this refers to the form field choices
        if primary_patient_payer:
            insurance_type = 'Saved Insurance'
        else:
            insurance_type = 'None'
        if secondary_patient_payer:
            secondary_insurance_type = 'Saved Insurance'
        else:
            secondary_insurance_type = 'None'
        if tertiary_patient_payer:
            tertiary_insurance_type = 'Saved Insurance'
        else:
            tertiary_insurance_type = 'None'
        if workers_comp_payer:
            workers_comp_type = 'Saved Workers Comp'
        else:
            workers_comp_type = 'None'
        if patient_guarantor:
            patient_guarantor_type = 'Saved Guarantor'
        else:
            patient_guarantor_type = 'None'

        form = forms.PatientPayerForm(initial={'patient': patient,
                                               'insurance_type': insurance_type,
                                               'secondary_insurance_type': secondary_insurance_type,
                                               'tertiary_insurance_type': tertiary_insurance_type,
                                               'workers_comp_type': workers_comp_type,
                                               'patient_guarantor_type': patient_guarantor_type})

        context = {'form': form,
                   'order': order,
                   'patient': patient,
                   'primary_patient_payer': primary_patient_payer,
                   'secondary_patient_payer': secondary_patient_payer,
                   'tertiary_patient_payer': tertiary_patient_payer,
                   'workers_comp_payer': workers_comp_payer,
                   'patient_guarantor': patient_guarantor}
        return render(request, 'order_update_patient_insurance.html', context)

    @transaction.atomic
    def post(self, request, order_code):
        order = get_object_or_404(models.Order, code=order_code)
        patient = order.patient
        primary_patient_payer = patient.order_patient_payers.filter(is_primary=True, order=order).last()
        secondary_patient_payer = patient.order_patient_payers.filter(is_secondary=True, order=order).last()
        tertiary_patient_payer = patient.order_patient_payers.filter(is_tertiary=True, order=order).last()
        workers_comp_payer = patient.order_patient_payers.filter(is_workers_comp=True, order=order).last()
        patient_guarantor = models.OrderPatientGuarantor.objects.filter(patient=patient, order=order).last()

        # this refers to the form field choices
        if primary_patient_payer:
            insurance_type = 'Saved Insurance'
        else:
            insurance_type = 'None'
        if secondary_patient_payer:
            secondary_insurance_type = 'Saved Insurance'
        else:
            secondary_insurance_type = 'None'
        if tertiary_patient_payer:
            tertiary_insurance_type = 'Saved Insurance'
        else:
            tertiary_insurance_type = 'None'
        if workers_comp_payer:
            workers_comp_type = 'Saved Workers Comp'
        else:
            workers_comp_type = 'None'
        if patient_guarantor:
            patient_guarantor_type = 'Saved Guarantor'
        else:
            patient_guarantor_type = 'None'

        form = forms.PatientPayerForm(request.POST, initial={'patient': patient,
                                                             'insurance_type': insurance_type,
                                                             'secondary_insurance_type': secondary_insurance_type,
                                                             'tertiary_insurance_type': tertiary_insurance_type,
                                                             'workers_comp_type': workers_comp_type,
                                                             'patient_guarantor_type': patient_guarantor_type})

        context = {'form': form,
                   'order': order,
                   'patient': patient,
                   'primary_patient_payer': primary_patient_payer,
                   'secondary_patient_payer': secondary_patient_payer,
                   'tertiary_patient_payer': tertiary_patient_payer,
                   'workers_comp_payer': workers_comp_payer,
                   'patient_guarantor': patient_guarantor}

        if form.is_valid():
            # by default, only the OrderPatientPayer is updated
            maintain_patient_payer = form.cleaned_data.get('maintain_patient_payer')

            if not maintain_patient_payer:
                patients_models.PatientPayer.objects.filter(patient=patient,
                                                            is_primary=True).update(is_active=False)
                patients_models.PatientPayer.objects.filter(patient=patient,
                                                            is_secondary=True).update(is_active=False)
                patients_models.PatientPayer.objects.filter(patient=patient,
                                                            is_tertiary=True).update(is_active=False)
                patients_models.PatientPayer.objects.filter(patient=patient,
                                                            is_workers_comp=True).update(is_active=False)
                patients_models.PatientGuarantor.objects.filter(patient=patient).update(is_active=False)

            ########################################################################################################
            # Primary payer
            # if PatientPayer is updated...
            # if no insurance info on OrderPatientPayer, deleting all PatientPayer objects for this patient works
            # if there is saved insurance, then delete all PatientPayers and copy over the last OrderPatientPayer...
            # to create a copy.
            # if there is new insurance, then delete all PatientPayers and copy over the newly-created OrderPatientPayer

            # if patient_payer is None, update to reflect self-pay
            if not form.cleaned_data.get('insurance_type'):
                models.OrderPatientPayer.objects.filter(order=order,
                                                        patient=patient,
                                                        is_primary=True).update(is_active=False)

            # update or create new primary payer
            elif form.cleaned_data.get('insurance_type') == 'New Insurance':
                order_patient_payer = form.cleaned_data.get('payer')
                if not order_patient_payer:
                    order_patient_payer = patients_models.Payer.objects.create(
                        name=form.cleaned_data.get('payer_name'),
                        payer_type=form.cleaned_data.get('patient_payer_type'))

                models.OrderPatientPayer.objects.create(order=order,
                                                        patient=patient,
                                                        # payer type
                                                        is_primary=True,
                                                        payer=order_patient_payer,
                                                        payer_type=order_patient_payer.payer_type,
                                                        # form fields
                                                        group_number=form.cleaned_data.get(
                                                            'group_number'),
                                                        member_id=form.cleaned_data.get(
                                                            'member_id'),
                                                        subscriber_last_name=form.cleaned_data.get(
                                                            'subscriber_last_name'),
                                                        subscriber_first_name=form.cleaned_data.get(
                                                            'subscriber_first_name'),
                                                        subscriber_middle_initial=form.cleaned_data.get(
                                                            'subscriber_middle_initial'),
                                                        subscriber_suffix=form.cleaned_data.get(
                                                            'subscriber_suffix'),
                                                        subscriber_birth_date=form.cleaned_data.get(
                                                            'subscriber_birth_date'),
                                                        subscriber_address1=form.cleaned_data.get(
                                                            'subscriber_address1'),
                                                        subscriber_address2=form.cleaned_data.get(
                                                            'subscriber_address2'),
                                                        subscriber_zip_code=form.cleaned_data.get(
                                                            'subscriber_zip_code'),
                                                        subscriber_sex=form.cleaned_data.get(
                                                            'subscriber_sex'),
                                                        subscriber_relationship=form.cleaned_data.get(
                                                            'subscriber_relationship'))

                if not maintain_patient_payer:
                    patients_models.PatientPayer.objects.create(patient=patient,
                                                                # payer type
                                                                is_primary=True,
                                                                payer=order_patient_payer,
                                                                payer_type=order_patient_payer.payer_type,
                                                                # form fields
                                                                group_number=form.cleaned_data.get(
                                                                    'group_number'),
                                                                member_id=form.cleaned_data.get(
                                                                    'member_id'),
                                                                subscriber_last_name=form.cleaned_data.get(
                                                                    'subscriber_last_name'),
                                                                subscriber_first_name=form.cleaned_data.get(
                                                                    'subscriber_first_name'),
                                                                subscriber_middle_initial=form.cleaned_data.get(
                                                                    'subscriber_middle_initial'),
                                                                subscriber_suffix=form.cleaned_data.get(
                                                                    'subscriber_suffix'),
                                                                subscriber_birth_date=form.cleaned_data.get(
                                                                    'subscriber_birth_date'),
                                                                subscriber_address1=form.cleaned_data.get(
                                                                    'subscriber_address1'),
                                                                subscriber_address2=form.cleaned_data.get(
                                                                    'subscriber_address2'),
                                                                subscriber_zip_code=form.cleaned_data.get(
                                                                    'subscriber_zip_code'),
                                                                subscriber_sex=form.cleaned_data.get(
                                                                    'subscriber_sex'),
                                                                subscriber_relationship=form.cleaned_data.get(
                                                                    'subscriber_relationship'))

            elif form.cleaned_data.get('insurance_type') == 'Saved Insurance' and not maintain_patient_payer:
                # even if no changes, the saved OrderPatientPayer (last) data should be exactly copied over
                order_patient_payer = models.OrderPatientPayer.objects.filter(order=order,
                                                                              patient=patient,
                                                                              is_primary=True).last()

                patients_models.PatientPayer.objects.update_or_create(patient=patient,
                                                                      is_primary=True,
                                                                      payer=order_patient_payer.payer,
                                                                      defaults={
                                                                          'payer_type': order_patient_payer.payer_type,
                                                                          'group_number': order_patient_payer.group_number,
                                                                          'member_id': order_patient_payer.member_id,
                                                                          'subscriber_last_name': order_patient_payer.subscriber_last_name,
                                                                          'subscriber_first_name': order_patient_payer.subscriber_first_name,
                                                                          'subscriber_middle_initial': order_patient_payer.subscriber_middle_initial,
                                                                          'subscriber_suffix': order_patient_payer.subscriber_suffix,
                                                                          'subscriber_birth_date': order_patient_payer.subscriber_birth_date,
                                                                          'subscriber_address1': order_patient_payer.subscriber_address1,
                                                                          'subscriber_address2': order_patient_payer.subscriber_address2,
                                                                          'subscriber_zip_code': order_patient_payer.subscriber_zip_code,
                                                                          'subscriber_sex': order_patient_payer.subscriber_sex,
                                                                          'subscriber_relationship': order_patient_payer.subscriber_relationship})

            ########################################################################################################
            # Secondary payer
            if not form.cleaned_data.get('secondary_insurance_type'):
                models.OrderPatientPayer.objects.filter(order=order,
                                                        patient=patient,
                                                        is_secondary=True).update(is_active=False)

            # update or create new primary payer
            elif form.cleaned_data.get('secondary_insurance_type') == 'New Insurance':
                order_patient_payer = form.cleaned_data.get('secondary_payer')
                if not order_patient_payer:
                    order_patient_payer = patients_models.Payer.objects.create(
                        name=form.cleaned_data.get('secondary_payer_name'),
                        payer_type=form.cleaned_data.get('secondary_patient_payer_type'))

                models.OrderPatientPayer.objects.create(order=order,
                                                        patient=patient,
                                                        # payer type
                                                        is_secondary=True,
                                                        payer=order_patient_payer,
                                                        payer_type=order_patient_payer.payer_type,
                                                        # form fields
                                                        group_number=form.cleaned_data.get(
                                                            'secondary_group_number'),
                                                        member_id=form.cleaned_data.get(
                                                            'secondary_member_id'),
                                                        subscriber_last_name=form.cleaned_data.get(
                                                            'secondary_subscriber_last_name'),
                                                        subscriber_first_name=form.cleaned_data.get(
                                                            'secondary_subscriber_first_name'),
                                                        subscriber_middle_initial=form.cleaned_data.get(
                                                            'secondary_subscriber_middle_initial'),
                                                        subscriber_suffix=form.cleaned_data.get(
                                                            'secondary_subscriber_suffix'),
                                                        subscriber_birth_date=form.cleaned_data.get(
                                                            'secondary_subscriber_birth_date'),
                                                        subscriber_address1=form.cleaned_data.get(
                                                            'secondary_subscriber_address1'),
                                                        subscriber_address2=form.cleaned_data.get(
                                                            'secondary_subscriber_address2'),
                                                        subscriber_zip_code=form.cleaned_data.get(
                                                            'secondary_subscriber_zip_code'),
                                                        subscriber_sex=form.cleaned_data.get(
                                                            'secondary_subscriber_sex'),
                                                        subscriber_relationship=form.cleaned_data.get(
                                                            'secondary_subscriber_relationship'))

                if not maintain_patient_payer:
                    patients_models.PatientPayer.objects.create(patient=patient,
                                                                # payer type
                                                                is_secondary=True,
                                                                payer=order_patient_payer,
                                                                payer_type=order_patient_payer.payer_type,
                                                                # form fields
                                                                group_number=form.cleaned_data.get(
                                                                    'secondary_group_number'),
                                                                member_id=form.cleaned_data.get(
                                                                    'secondary_member_id'),
                                                                subscriber_last_name=form.cleaned_data.get(
                                                                    'secondary_subscriber_last_name'),
                                                                subscriber_first_name=form.cleaned_data.get(
                                                                    'secondary_subscriber_first_name'),
                                                                subscriber_middle_initial=form.cleaned_data.get(
                                                                    'secondary_subscriber_middle_initial'),
                                                                subscriber_suffix=form.cleaned_data.get(
                                                                    'secondary_subscriber_suffix'),
                                                                subscriber_birth_date=form.cleaned_data.get(
                                                                    'secondary_subscriber_birth_date'),
                                                                subscriber_address1=form.cleaned_data.get(
                                                                    'secondary_subscriber_address1'),
                                                                subscriber_address2=form.cleaned_data.get(
                                                                    'secondary_subscriber_address2'),
                                                                subscriber_zip_code=form.cleaned_data.get(
                                                                    'secondary_subscriber_zip_code'),
                                                                subscriber_sex=form.cleaned_data.get(
                                                                    'secondary_subscriber_sex'),
                                                                subscriber_relationship=form.cleaned_data.get(
                                                                    'secondary_subscriber_relationship'))

            elif form.cleaned_data.get('secondary_insurance_type') == 'Saved Insurance' and not maintain_patient_payer:
                # even if no changes, the saved OrderPatientPayer (last) data should be exactly copied over
                order_patient_payer = models.OrderPatientPayer.objects.filter(order=order,
                                                                              patient=patient,
                                                                              is_secondary=True).last()

                patients_models.PatientPayer.objects.update_or_create(patient=patient,
                                                                      is_secondary=True,
                                                                      payer=order_patient_payer.payer,
                                                                      defaults={
                                                                          'payer_type': order_patient_payer.payer_type,
                                                                          'group_number': order_patient_payer.group_number,
                                                                          'member_id': order_patient_payer.member_id,
                                                                          'subscriber_last_name': order_patient_payer.subscriber_last_name,
                                                                          'subscriber_first_name': order_patient_payer.subscriber_first_name,
                                                                          'subscriber_middle_initial': order_patient_payer.subscriber_middle_initial,
                                                                          'subscriber_suffix': order_patient_payer.subscriber_suffix,
                                                                          'subscriber_birth_date': order_patient_payer.subscriber_birth_date,
                                                                          'subscriber_address1': order_patient_payer.subscriber_address1,
                                                                          'subscriber_address2': order_patient_payer.subscriber_address2,
                                                                          'subscriber_zip_code': order_patient_payer.subscriber_zip_code,
                                                                          'subscriber_sex': order_patient_payer.subscriber_sex,
                                                                          'subscriber_relationship': order_patient_payer.subscriber_relationship})

            ########################################################################################################
            # Tertiary payer
            if not form.cleaned_data.get('tertiary_insurance_type'):
                models.OrderPatientPayer.objects.filter(order=order,
                                                        patient=patient,
                                                        is_tertiary=True).update(is_active=False)

            # update or create new primary payer
            elif form.cleaned_data.get('tertiary_insurance_type') == 'New Insurance':
                order_patient_payer = form.cleaned_data.get('tertiary_payer')
                if not order_patient_payer:
                    order_patient_payer = patients_models.Payer.objects.create(
                        name=form.cleaned_data.get('tertiary_payer_name'),
                        payer_type=form.cleaned_data.get('tertiary_patient_payer_type'))

                models.OrderPatientPayer.objects.create(order=order,
                                                        patient=patient,
                                                        # payer type
                                                        is_tertiary=True,
                                                        payer=order_patient_payer,
                                                        payer_type=order_patient_payer.payer_type,
                                                        # form fields
                                                        group_number=form.cleaned_data.get(
                                                            'tertiary_group_number'),
                                                        member_id=form.cleaned_data.get(
                                                            'tertiary_member_id'),
                                                        subscriber_last_name=form.cleaned_data.get(
                                                            'tertiary_subscriber_last_name'),
                                                        subscriber_first_name=form.cleaned_data.get(
                                                            'tertiary_subscriber_first_name'),
                                                        subscriber_middle_initial=form.cleaned_data.get(
                                                            'tertiary_subscriber_middle_initial'),
                                                        subscriber_suffix=form.cleaned_data.get(
                                                            'tertiary_subscriber_suffix'),
                                                        subscriber_birth_date=form.cleaned_data.get(
                                                            'tertiary_subscriber_birth_date'),
                                                        subscriber_address1=form.cleaned_data.get(
                                                            'tertiary_subscriber_address1'),
                                                        subscriber_address2=form.cleaned_data.get(
                                                            'tertiary_subscriber_address2'),
                                                        subscriber_zip_code=form.cleaned_data.get(
                                                            'tertiary_subscriber_zip_code'),
                                                        subscriber_sex=form.cleaned_data.get(
                                                            'tertiary_subscriber_sex'),
                                                        subscriber_relationship=form.cleaned_data.get(
                                                            'tertiary_subscriber_relationship'))

                if not maintain_patient_payer:
                    patients_models.PatientPayer.objects.create(patient=patient,
                                                                # payer type
                                                                is_tertiary=True,
                                                                payer=order_patient_payer,
                                                                payer_type=order_patient_payer.payer_type,
                                                                # form fields
                                                                group_number=form.cleaned_data.get(
                                                                    'tertiary_group_number'),
                                                                member_id=form.cleaned_data.get(
                                                                    'tertiary_member_id'),
                                                                subscriber_last_name=form.cleaned_data.get(
                                                                    'tertiary_subscriber_last_name'),
                                                                subscriber_first_name=form.cleaned_data.get(
                                                                    'tertiary_subscriber_first_name'),
                                                                subscriber_middle_initial=form.cleaned_data.get(
                                                                    'tertiary_subscriber_middle_initial'),
                                                                subscriber_suffix=form.cleaned_data.get(
                                                                    'tertiary_subscriber_suffix'),
                                                                subscriber_birth_date=form.cleaned_data.get(
                                                                    'tertiary_subscriber_birth_date'),
                                                                subscriber_address1=form.cleaned_data.get(
                                                                    'tertiary_subscriber_address1'),
                                                                subscriber_address2=form.cleaned_data.get(
                                                                    'tertiary_subscriber_address2'),
                                                                subscriber_zip_code=form.cleaned_data.get(
                                                                    'tertiary_subscriber_zip_code'),
                                                                subscriber_sex=form.cleaned_data.get(
                                                                    'tertiary_subscriber_sex'),
                                                                subscriber_relationship=form.cleaned_data.get(
                                                                    'tertiary_subscriber_relationship'))

            elif form.cleaned_data.get('tertiary_insurance_type') == 'Saved Insurance' and not maintain_patient_payer:
                # even if no changes, the saved OrderPatientPayer (last) data should be exactly copied over
                order_patient_payer = models.OrderPatientPayer.objects.filter(order=order,
                                                                              patient=patient,
                                                                              is_tertiary=True).last()
                patients_models.PatientPayer.objects.update_or_create(patient=patient,
                                                                      is_tertiary=True,
                                                                      payer=order_patient_payer.payer,
                                                                      defaults={
                                                                          'payer_type': order_patient_payer.payer_type,
                                                                          'group_number': order_patient_payer.group_number,
                                                                          'member_id': order_patient_payer.member_id,
                                                                          'subscriber_last_name': order_patient_payer.subscriber_last_name,
                                                                          'subscriber_first_name': order_patient_payer.subscriber_first_name,
                                                                          'subscriber_middle_initial': order_patient_payer.subscriber_middle_initial,
                                                                          'subscriber_suffix': order_patient_payer.subscriber_suffix,
                                                                          'subscriber_birth_date': order_patient_payer.subscriber_birth_date,
                                                                          'subscriber_address1': order_patient_payer.subscriber_address1,
                                                                          'subscriber_address2': order_patient_payer.subscriber_address2,
                                                                          'subscriber_zip_code': order_patient_payer.subscriber_zip_code,
                                                                          'subscriber_sex': order_patient_payer.subscriber_sex,
                                                                          'subscriber_relationship': order_patient_payer.subscriber_relationship})

            ########################################################################################################
            # Workers' Comp payer
            if not form.cleaned_data.get('workers_comp_type'):
                models.OrderPatientPayer.objects.filter(order=order,
                                                        patient=patient,
                                                        is_workers_comp=True).update(is_active=False)

            # update or create new primary payer
            elif form.cleaned_data.get('workers_comp_type') == 'New Workers Comp':
                order_patient_payer = form.cleaned_data.get('workers_comp_payer')
                if not order_patient_payer:
                    order_patient_payer = patients_models.Payer.objects.create(
                        name=form.cleaned_data.get('workers_comp_payer_name'),
                        payer_type=form.cleaned_data.get('workers_comp_type'))

                models.OrderPatientPayer.objects.create(order=order,
                                                        patient=patient,
                                                        # payer type
                                                        is_workers_comp=True,
                                                        payer=order_patient_payer,
                                                        payer_type=order_patient_payer.payer_type,
                                                        # form fields
                                                        workers_comp_claim_id=form.cleaned_data.get(
                                                            'workers_comp_claim_id'),
                                                        workers_comp_injury_date=form.cleaned_data.get(
                                                            'workers_comp_injury_date'),
                                                        workers_comp_adjuster=form.cleaned_data.get(
                                                            'workers_comp_adjuster'))

                if not maintain_patient_payer:
                    patients_models.PatientPayer.objects.create(patient=patient,
                                                                # payer type
                                                                is_workers_comp=True,
                                                                payer=order_patient_payer,
                                                                payer_type=order_patient_payer.payer_type,
                                                                # form fields
                                                                workers_comp_claim_id=form.cleaned_data.get(
                                                                    'workers_comp_claim_id'),
                                                                workers_comp_injury_date=form.cleaned_data.get(
                                                                    'workers_comp_injury_date'),
                                                                workers_comp_adjuster=form.cleaned_data.get(
                                                                    'workers_comp_adjuster'))

            elif form.cleaned_data.get('workers_comp_type') == 'Saved Workers Comp' and not maintain_patient_payer:
                # even if no changes, the saved OrderPatientPayer (last) data should be exactly copied over
                order_patient_payer = models.OrderPatientPayer.objects.filter(order=order,
                                                                              patient=patient,
                                                                              is_workers_comp=True).last()

                patients_models.PatientPayer.objects.update_or_create(patient=patient,
                                                                      is_workers_comp=True,
                                                                      payer=order_patient_payer.payer,
                                                                      defaults={
                                                                          'payer_type': order_patient_payer.payer_type,
                                                                          'workers_comp_claim_id': order_patient_payer.workers_comp_claim_id,
                                                                          'workers_comp_injury_date': order_patient_payer.workers_comp_injury_date,
                                                                          'workers_comp_adjuster': order_patient_payer.workers_comp_adjuster})

            ########################################################################################################
            # Order Patient Guarantor
            # if patient guarantor type is falsey (None)
            if not form.cleaned_data.get('patient_guarantor_type'):
                models.OrderPatientGuarantor.objects.filter(order=order,
                                                            patient=patient).update(is_active=False)

            elif form.cleaned_data.get('patient_guarantor_type') == 'New Guarantor':
                # generate random string for username and password initially
                user_uuid = str(uuid.uuid4())
                username = user_uuid
                # create User object
                user_obj = User(first_name=form.cleaned_data.get('first_name'),
                                last_name=form.cleaned_data.get('last_name'),
                                username=username,
                                # PatientGuantorType has user type of 21
                                user_type=21,
                                email=form.cleaned_data.get('email'))
                user_obj.set_password(user_uuid)
                # create User object first before creating Patient
                user_obj.save()

                models.OrderPatientGuarantor.objects.create(order=order,
                                                            patient=patient,
                                                            user=user_obj,
                                                            # form fields
                                                            relationship_to_patient=form.cleaned_data.get(
                                                                'relationship_to_patient'),
                                                            suffix=form.cleaned_data.get('suffix'),
                                                            middle_initial=form.cleaned_data.get('middle_initial'),
                                                            sex=form.cleaned_data.get('sex'),
                                                            birth_date=form.cleaned_data.get('birth_date'),
                                                            address1=form.cleaned_data.get('address1'),
                                                            address2=form.cleaned_data.get('address2'),
                                                            zip_code=form.cleaned_data.get('zip_code'),
                                                            phone_number=form.cleaned_data.get('phone_number'),
                                                            social_security=form.cleaned_data.get(
                                                                'social_security_number'))

                if not maintain_patient_payer:
                    patients_models.PatientGuarantor.objects.create(patient=patient,
                                                                    user=user_obj,
                                                                    # form fields
                                                                    relationship_to_patient=form.cleaned_data.get(
                                                                        'relationship_to_patient'),
                                                                    suffix=form.cleaned_data.get('suffix'),
                                                                    middle_initial=form.cleaned_data.get(
                                                                        'middle_initial'),
                                                                    sex=form.cleaned_data.get('sex'),
                                                                    birth_date=form.cleaned_data.get('birth_date'),
                                                                    address1=form.cleaned_data.get('address1'),
                                                                    address2=form.cleaned_data.get('address2'),
                                                                    zip_code=form.cleaned_data.get('zip_code'),
                                                                    phone_number=form.cleaned_data.get('phone_number'),
                                                                    social_security=form.cleaned_data.get(
                                                                        'social_security_number'))

            elif form.cleaned_data.get('patient_guarantor_type') == 'Saved Guarantor' and not maintain_patient_payer:
                # even if no changes, the saved OrderPatientPayer (last) data should be exactly copied over
                order_patient_guarantor = models.OrderPatientGuarantor.objects.filter(order=order,
                                                                                      patient=patient).last()

                patients_models.PatientGuarantor.all_objects.update_or_create(user=order_patient_guarantor.user,
                                                                              defaults={'is_active': True,
                                                                                        'patient': order_patient_guarantor.patient,
                                                                                        'relationship_to_patient': order_patient_guarantor.relationship_to_patient,
                                                                                        'suffix': order_patient_guarantor.suffix,
                                                                                        'middle_initial': order_patient_guarantor.middle_initial,
                                                                                        'sex': order_patient_guarantor.sex,
                                                                                        'birth_date': order_patient_guarantor.birth_date,
                                                                                        'address1': order_patient_guarantor.address1,
                                                                                        'address2': order_patient_guarantor.address2,
                                                                                        'zip_code': order_patient_guarantor.zip_code,
                                                                                        'phone_number': order_patient_guarantor.phone_number,
                                                                                        'social_security': order_patient_guarantor.social_security})

            ########################################################################################################
            # if form is valid
            return redirect('order_details', order_code=order_code)
        else:
            # if form is invalid
            return render(request, 'order_update_patient_insurance.html', context)


class OrderAmendReportView(LabMemberMixin, View):
    def get(self, request, order_code):
        order = get_object_or_404(models.Order, code=order_code)
        order.completed = False
        order.reported = False
        order.save(update_fields=['completed', 'reported'])

        return redirect('order_details', order_code=order_code)


class OrderReceivedView(LabMemberMixin, View):
    def get(self, request, order_code):
        order = get_object_or_404(models.Order, code=order_code)
        order.received_date = timezone.now()
        order.save(update_fields=['received_date'])

        update_test_due_dates(order)

        if not order.accession_number and request.tenant.settings.get('enable_accession_number', False):
            accession_number_result = tasks.assign_accession_number.apply_async(
                args=[order.id, request.tenant.id],
                queue='identifiers')
            accession_number = accession_number_result.get()

        return redirect('order_details', order_code=order_code)


class OrderRefreshPDFView(LabMemberMixin, View):
    """
    Remake and overwrite the order's report PDF
    """

    def get(self, request, order_code):
        order = get_object_or_404(models.Order, code=order_code)
        report_order(request, order, None, preliminary=True)
        messages.success(request, "The report has been updated.")
        return redirect('order_details', order_code=order_code)


class OrderReportPreview(LabMemberMixin, View):
    def get(self, request, order_code):
        order = get_object_or_404(models.Order, code=order_code)

        context = {
            'order': order,
        }
        return render(request, 'order_pdf_report.html', context)


class OrderReportView(LabMemberMixin, View):
    def get(self, request, order_code):
        order = get_object_or_404(models.Order, code=order_code)
        # report the order (final report, not preliminary)
        success = report_order(request, order, None, preliminary=False)
        if success:
            messages.success(request, "The report for this order is being generated.")
        return redirect('order_details', order_code=order_code)


class OrderRequisitionFormView(LISLoginRequiredMixin, View):
    def get(self, request, order_code):
        order = get_object_or_404(models.Order, code=order_code)

        generate_requisition_form.generate_requisition_PDF(request, order.uuid)
        messages.success(request, "Requisition form successfully generated.")
        return redirect('order_details', order_code=order_code)


class OrderBillingMessageView(LabMemberMixin, View):
    def get(self, request, order_code):
        order = get_object_or_404(models.Order, code=order_code)

        generate_billing_hl7.send_billing_message_message(request.tenant.id, order.uuid)
        messages.success(request, "Billing message successfully generated.")
        return redirect('order_details', order_code=order_code)


class OrderNotesView(LabMemberMixin, View):
    def get(self, request, order_code):
        order = get_object_or_404(models.Order, code=order_code)
        form = forms.OrderNotesForm(initial={'notes': order.notes})
        return render(request, 'order_notes.html', {'form': form,
                                                    'order': order})

    def post(self, request, order_code):
        order = get_object_or_404(models.Order, code=order_code)
        form = forms.OrderNotesForm(request.POST)

        if form.is_valid():
            order.notes = form.cleaned_data.get('notes')
            order.save(update_fields=['notes'])
            return redirect('order_details', order_code=order_code)
        else:
            return render(request, 'order_notes.html', {'form': form,
                                                        'order': order})


class OrderIssuesListView(LabMemberMixin, View):
    def get(self, request, order_code):
        order = get_object_or_404(models.Order, code=order_code)
        # get issues for this particular order
        issues = models.OrderIssue.objects.filter(order=order)
        unresolved_order_issues = issues.filter(is_resolved=False)
        resolved_order_issues = issues.filter(is_resolved=True).order_by('-resolved_datetime')

        comment_forms = [forms.OrderIssueResolvedForm(initial={'comment': issue.resolver_comment})
                         for issue in resolved_order_issues]
        resolved_order_issues_data_and_forms = zip(resolved_order_issues,
                                                   comment_forms)

        # get correct datetime/timezones
        for issue in unresolved_order_issues:
            issue.created_datetime = convert_utc_to_tenant_timezone(request, issue.created_date)
        for issue in resolved_order_issues:
            issue.created_datetime = convert_utc_to_tenant_timezone(request, issue.created_date)
            issue.resolved_datetime = convert_utc_to_tenant_timezone(request, issue.resolved_datetime)

        return render(request, 'order_issue_list.html', {'order': order,
                                                         'unresolved_order_issues': unresolved_order_issues,
                                                         'resolved_order_issues_data_and_formset':
                                                             resolved_order_issues_data_and_forms})


class OrderIssuesCreateView(LabMemberMixin, View):
    # allows users to submit issues regarding an order
    def get(self, request, order_code):
        order = get_object_or_404(models.Order, code=order_code)
        form = forms.OrderIssueForm(initial={'order': order}, order_uuid=order.uuid)
        return render(request, 'order_issue_create.html', {'order': order,
                                                           'form': form})

    def post(self, request, order_code):
        order = get_object_or_404(models.Order, code=order_code)
        form = forms.OrderIssueForm(request.POST, order_uuid=order.uuid)

        if form.is_valid():
            issue_type = form.cleaned_data.get('issue_type')
            description = form.cleaned_data.get('description')
            is_public = form.cleaned_data.get('is_public')
            samples = form.cleaned_data.get('samples')
            issue_creator = request.user
            order_issue = models.OrderIssue(order=order,
                                            issue_type=issue_type,
                                            is_public=is_public,
                                            description=description,
                                            issue_creator=issue_creator)
            order_issue.save()

            # add related samples
            order_issue.samples.set(samples, clear=True)
            return redirect('order_issue_list', order_code)
        else:
            return render(request, 'order_issue_create.html', {'form': form,
                                                               'order': order})


class OrderIssuesUpdateView(LabMemberMixin, View):
    # allows users to submit issues regarding an order
    def get(self, request, order_code, order_issue_uuid):
        order = get_object_or_404(models.Order, code=order_code)
        order_issue = get_object_or_404(models.OrderIssue, uuid=order_issue_uuid)

        form = forms.OrderIssueForm(initial={'order': order,
                                             'issue_type': order_issue.issue_type,
                                             'is_public': order_issue.is_public,
                                             'description': order_issue.description,
                                             'samples': order_issue.samples.filter()}, order_uuid=order.uuid)
        return render(request, 'order_issue_create.html', {'order': order,
                                                           'form': form,
                                                           'update': True})

    def post(self, request, order_code, order_issue_uuid):
        order = get_object_or_404(models.Order, code=order_code)
        order_issue = get_object_or_404(models.OrderIssue, uuid=order_issue_uuid)
        form = forms.OrderIssueForm(request.POST, order_uuid=order.uuid)

        if form.is_valid():
            issue_type = form.cleaned_data.get('issue_type')
            description = form.cleaned_data.get('description')
            is_public = form.cleaned_data.get('is_public')
            samples = form.cleaned_data.get('samples')

            order_issue.order = order
            order_issue.issue_type = issue_type
            order_issue.is_public = is_public
            order_issue.description = description
            order_issue.save()

            # add related samples
            order_issue.samples.set(samples, clear=True)

            return redirect('order_issue_list', order_code)
        else:
            return render(request, 'order_issue_create.html', {'form': form,
                                                               'order': order,
                                                               'update': True})


class OrderIssuesResolveView(LabMemberMixin, View):
    def get(self, request, order_code, order_issue_uuid):
        order = get_object_or_404(models.Order, code=order_code)
        order_issue = get_object_or_404(models.OrderIssue, uuid=order_issue_uuid)
        # resolve issue and save
        order_issue.is_resolved = True
        order_issue.issue_resolver = request.user
        # resolved datetime saved as UTC due to datetime.datetime.now()
        order_issue.resolved_datetime = datetime.datetime.now()
        order_issue.save()
        return redirect('order_issue_list', order_code=order.code)

    @transaction.atomic
    def post(self, request, order_code, order_issue_uuid):
        order = get_object_or_404(models.Order, code=order_code)
        order_issue = get_object_or_404(models.OrderIssue, uuid=order_issue_uuid)

        if request.user != order_issue.issue_resolver:
            messages.error(request, "Only the user who resolved the issue can add a comment.")
            return redirect('order_issue_list', order_code=order.code)

        form = forms.OrderIssueResolvedForm(request.POST)

        if form.is_valid():
            order_issue.resolver_comment = form.cleaned_data['comment']
            order_issue.save()
            return redirect('order_issue_list', order_code=order_code)
        return redirect('order_issue_list', order_code=order_code)


class OrderDeleteView(LabMemberMixin, View):
    def get(self, request, order_code):
        order = get_object_or_404(models.Order, code=order_code)
        order.is_active = False
        order.save()

        # also set is_active = False for associated Test, TestPanel, Sample?
        models.Sample.objects.filter(order__id=order.id).update(is_active=False)
        models.TestPanel.objects.filter(order__id=order.id).update(is_active=False)
        models.Test.objects.filter(order__id=order.id).update(is_active=False)
        models.Result.objects.filter(order__id=order.id).update(is_active=False)

        messages.success(request, 'Order: {} archived successfully.'.format(order.code))
        return redirect('order_list')


class OrderUndoDeleteView(LabMemberMixin, View):
    def get(self, request, order_code):
        order = models.Order.all_objects.get(code=order_code)
        order.is_active = True
        order.save()

        # also set is_active = False for associated Test, TestPanel, Sample?
        models.Sample.all_objects.filter(order__id=order.id).update(is_active=True)
        models.TestPanel.all_objects.filter(order__id=order.id).update(is_active=True)
        models.Test.all_objects.filter(order__id=order.id).update(is_active=True)
        models.Result.all_objects.filter(order__id=order.id).update(is_active=True)

        messages.success(request, 'Order: {} unarchived.'.format(order.code))
        return redirect('archived_order_list')


class OrderSamplesView(LabMemberMixin, View):
    def get(self, request, order_code):
        order = get_object_or_404(models.Order, code=order_code)
        samples = order.samples.all().order_by('collection_date')
        form = forms.SampleForm()

        require_collected_by = request.tenant.settings.get('require_collected_by', False)

        return render(request, 'order_samples.html', {'order': order,
                                                      'samples': samples,
                                                      'require_collected_by': require_collected_by,
                                                      'form': form})

    @transaction.atomic
    def post(self, request, order_code):
        def sample_code(order_code, sample_type):
            samples = models.Sample.objects.filter(order__code=order_code,
                                                   clia_sample_type=sample_type)
            if samples:
                code = order_code + '-' + sample_type.name[0].upper() + '-' + str(len(samples) + 1)
            else:
                code = order_code + '-' + sample_type.name[0].upper() + '-1'
            return code

        order = get_object_or_404(models.Order, code=order_code)
        samples = order.samples.all().order_by('collection_date')
        require_collected_by = request.tenant.settings.get('require_collected_by', False)
        form = forms.SampleForm(request.POST, require_collected_by=require_collected_by)

        if form.is_valid():
            sample_type = form.cleaned_data.get('sample_type')
            collection_date = form.cleaned_data.get('collection_date')
            collection_time = form.cleaned_data.get('collection_time')

            if collection_time:
                collection_date = datetime.datetime.combine(collection_date, collection_time)
            else:
                collection_date = datetime.datetime.combine(
                    collection_date, datetime.datetime.min.time())
            # convert tenant timezone to UTC before saving
            collection_date = convert_tenant_timezone_to_utc(request, collection_date)

            if sample_type and collection_date:
                latest_sample_code = sample_code(order.code, sample_type)
                sample = models.Sample.objects.create(
                    order=order,
                    code=latest_sample_code,
                    clia_sample_type=sample_type,
                    collection_date=collection_date,
                    collection_temperature=form.cleaned_data.get('collection_temperature'),
                    collection_temperature_unit=form.cleaned_data.get(
                        'collection_temperature_unit'),
                    barcode=form.cleaned_data.get('barcode'),
                    collection_user=form.cleaned_data.get('collection_user'),
                    draw_type=form.cleaned_data.get('draw_type')
                )
                sample.barcode = get_sample_barcode(sample.id)
                sample.save(update_fields=['barcode'])

            return redirect('order_samples', order_code=order.code)
        else:
            return render(request, 'order_samples.html', {'order': order,
                                                          'samples': samples,
                                                          'form': form})


class OrderTestPanelUpdateView(LabMemberMixin, View):
    def get(self, request, order_code):
        order = get_object_or_404(models.Order, code=order_code)
        account = order.account
        provider = order.provider
        context = get_test_panel_forms(request, order=order, account=account, provider=provider)
        context['order'] = order

        return render(request, 'order_test_panel_update.html', context)

    def post(self, request, order_code):
        order = get_object_or_404(models.Order, code=order_code)
        account = order.account
        provider = order.provider
        context = get_test_panel_forms(request, order=order, account=account, provider=provider)
        context['order'] = order

        test_panel_categories = context['test_panel_categories']
        test_panel_data_and_formset_dict = context['test_panel_data_and_formset_dict']
        test_panel_formset_dict = context['test_panel_formset_dict']

        requested_service_counter = 0
        requested_test_panels = []
        # add requested services to order
        order_samples = order.samples.all()
        existing_test_panel_types = [x.test_panel_type for x in order.test_panels.all()]
        for test_panel_category in test_panel_categories:
            formset = test_panel_formset_dict[test_panel_category]
            if formset.is_valid():
                for test_panel_data, test_panel_form in test_panel_data_and_formset_dict[test_panel_category]:
                    test_panel_type = test_panel_form.cleaned_data['test_panel_type']
                    if test_panel_form.cleaned_data['checked']:
                        requested_service_counter += 1
                        if test_panel_type not in existing_test_panel_types:
                            test_panel = models.TestPanel.objects.create(
                                order=order,
                                test_panel_type=test_panel_type,
                                expected_revenue=test_panel_type.cost
                            )

                            for test_type in test_panel_type.test_types.all():
                                required_samples = test_type.required_samples
                                if required_samples:
                                    for required_sample_type_id in required_samples:
                                        samples = order_samples.filter(
                                            clia_sample_type__id=required_sample_type_id).order_by('code')[
                                                  :required_samples[required_sample_type_id]]
                                        for sample in samples:
                                            test_obj, result_obj = add_result(
                                                request, order, sample, test_panel, test_type)
                                else:
                                    for sample in order_samples:
                                        test_obj, result_obj = add_result(
                                            request, order, sample, test_panel, test_type)
                    else:
                        # check if unchecked test panel was in order
                        if test_panel_type in existing_test_panel_types:
                            test_panels = order.test_panels.filter(
                                test_panel_type=test_panel_type).all()
                            for test_panel in test_panels:
                                test_panel.is_active = False
                                test_panel.save()
                                for test in test_panel.tests.all():
                                    results = test.result.filter()
                                    for result in results:
                                        result.is_active = False
                                        result.save()
                                    test.is_active = False
                                    test.save()
        if requested_service_counter == 0:
            messages.error(request, "Please select at least one test profile.")
            return render(request, 'order_test_panel_update.html', context)
        messages.success(request, "Test profiles updated.")
        return redirect('/orders/{}'.format(order.code))


class OrderTestCreateView(LISLoginRequiredMixin, View):
    def get(self, request, order_code):
        order = get_object_or_404(models.Order, code=order_code)
        form = forms.OrderResultForm(initial={'order': order}, order_uuid=order.uuid)

        return render(request, 'order_test_create.html', {'order': order, 'form': form})

    @transaction.atomic
    def post(self, request, order_code):
        order = get_object_or_404(models.Order, code=order_code)
        form = forms.OrderResultForm(request.POST, order_uuid=order.uuid)

        if form.is_valid():
            sample = form.cleaned_data['sample']
            test_type = form.cleaned_data['test_type']
            days_incubated = form.cleaned_data['days_incubated']
            due_date = form.cleaned_data['due_date']

            test_obj = models.Test.objects.create(
                order=order,
                sample=sample,
                test_type=test_type,
                due_date=due_date
            )
            result_obj = models.Result.objects.create(
                order=order,
                test=test_obj,
                days_incubated=days_incubated
            )
            if test_type.is_research_only:
                result_obj.reference = 'RUO'
                result_obj.save(update_fields=['reference'])
            return redirect('order_details', order_code=order.code)

        return render(request, 'order_test_create.html', {'order': order, 'form': form})


class OrderTestDeleteView(LISLoginRequiredMixin, View):
    def get(self, request, order_code, test_uuid):
        order = get_object_or_404(models.Order, code=order_code)
        test = get_object_or_404(models.Test, uuid=test_uuid)

        results = test.result.filter()

        for result in results:
            result.is_active = False
            result.save()

        test.is_active = False
        test.save()

        return redirect('order_details', order_code=order.code)


class OrderTestApprovedView(LabMemberMixin, View):
    def get(self, request, order_code, test_uuid):
        order = get_object_or_404(models.Order, code=order_code)
        test = get_object_or_404(models.Test, uuid=test_uuid)
        test.is_approved = True
        test.approved_user = request.user
        test.approved_date = timezone.now()
        test.save(update_fields=['is_approved', 'approved_user', 'approved_date'])
        return redirect('order_details', order_code=order.code)


class OrderTestMethodApprovedView(LabMemberMixin, View):
    def get(self, request, order_code, test_method):
        order = get_object_or_404(models.Order, code=order_code)

        tests = order.tests.filter(
            test_type__test_method__name=test_method,
            is_approved=False,
            result__result__isnull=False
        ).prefetch_related(
            'result',
            'result',
            'test_type__test_method'
        )

        if tests:
            test_list = []
            for test in tests:
                test.is_approved = True
                test.approved_user = request.user
                test.approved_date = timezone.now()
                test_list.append(test)
            models.Test.objects.bulk_update(test_list, ['is_approved', 'approved_user', 'approved_date'])
        messages.success(request, "{} tests have been approved.".format(test_method))
        return redirect('order_details', order_code=order.code)


class OrderTestsApproveView(LabMemberMixin, View):
    """
    Approve all non null result Tests in a given order.
    """

    def get(self, request, order_code):
        order = get_object_or_404(models.Order, code=order_code)
        approve_all_test_results(request, order)
        messages.success(request, "All tests with results have been approved.")
        return redirect('order_details', order_code=order.code)


class OrderTestMethodApprovalRevertView(LabMemberMixin, View):
    """
    Revert Approval for an order's test method
    """

    def get(self, request, order_code, test_method):
        order = get_object_or_404(models.Order, code=order_code)

        tests = order.tests.filter(
            test_type__test_method__name=test_method,
            is_approved=True
        ).prefetch_related(
            'test_type__test_method'
        )

        if tests:
            test_list = []
            for test in tests:
                test.is_approved = False
                test.initial_report_date = None
                test_list.append(test)
            models.Test.objects.bulk_update(test_list, ['is_approved', 'initial_report_date'])
            messages.info(request, "{} approval has been reverted.".format(test_method))
        return redirect('order_details', order_code=order.code)


class OrderReflexListView(LabMemberMixin, View):
    def get(self, request, order_code):
        order = get_object_or_404(models.Order, code=order_code)
        has_reflex, reflex_list = get_order_reflex(order_code)
        return render(request, 'order_reflex_list.html', {'order': order,
                                                          'reflex_list': reflex_list})


class OrderReflexTestCreateView(LabMemberMixin, View):
    def get(self, request, order_code, test_type_uuid):
        order = get_object_or_404(models.Order, code=order_code)
        test_type = get_object_or_404(models.TestType, uuid=test_type_uuid)
        form = forms.OrderResultForm(initial={'order': order,
                                              'test_type': test_type},
                                     order_uuid=order.uuid)
        return render(request, 'order_test_create.html', {'order': order, 'form': form})

    @transaction.atomic
    def post(self, request, order_code, test_type_uuid):
        order = get_object_or_404(models.Order, code=order_code)
        form = forms.OrderResultForm(request.POST, order_uuid=order.uuid)

        if form.is_valid():
            sample = form.cleaned_data['sample']
            test_type = form.cleaned_data['test_type']
            days_incubated = form.cleaned_data['days_incubated']
            due_date = form.cleaned_data['due_date']

            test_obj = models.Test.objects.create(
                order=order,
                sample=sample,
                test_type=test_type,
                due_date=due_date
            )
            result_obj = models.Result.objects.create(
                order=order,
                test=test_obj,
                days_incubated=days_incubated
            )
            if test_type.is_research_only:
                result_obj.reference = 'RUO'
                result_obj.save()
            return redirect('order_details', order_code=order.code)


class OrderApproveAndReportView(LabMemberMixin, View):

    def get(self, request, order_code):
        order = get_object_or_404(models.Order, code=order_code)
        approve_all_test_results(request, order)
        # report the order
        success = report_order(request, order, None, preliminary=False)
        if success:
            messages.success(request, "Order: {} has been successfully approved and reported.".format(order.code))
        return redirect('order_approve', order_code=order.code)


class OrderApproveView(LabMemberMixin, View):
    """
    Save & approve results, then generate a preliminary report (order is not reported yet)
    """

    def get(self, request, order_code):
        order = get_object_or_404(models.Order, code=order_code)
        approve_all_test_results(request, order)

        next_order = models.Order.objects.filter(completed=True,
                                                 reported=False,
                                                 created_date__gte=order.created_date).exclude(id=order.id) \
            .order_by('created_date').first()

        previous_order = models.Order.objects.filter(completed=True,
                                                     reported=False,
                                                     created_date__lte=order.created_date).exclude(id=order.id) \
            .order_by('-created_date').first()

        order.submitted_date = convert_utc_to_tenant_timezone(request, order.submitted_date)
        order.received_date = convert_utc_to_tenant_timezone(request, order.received_date)
        order.completed_date = convert_utc_to_tenant_timezone(request, order.completed_date)
        order.report_date = convert_utc_to_tenant_timezone(request, order.report_date)

        reports = models.Report.objects.filter(
            order=order, preliminary=False).order_by('-created_date')
        preliminary_reports = models.Report.objects.filter(
            order=order, preliminary=True).order_by('-created_date')

        # create preliminary report if none exists
        if not preliminary_reports:
            report_order(request, order, None, preliminary=True)

        editable_order_status_list = ['Unreceived', 'Pending Results', 'Pending Test Approval']

        context = get_result_forms(request, order.code)
        context.update({'order': order,
                        'editable_order_status_list': editable_order_status_list,
                        'reports': reports,
                        'preliminary_reports': preliminary_reports,
                        'next_order': next_order,
                        'previous_order': previous_order,
                        'order_approval_page': True,
                        })

        return render(request, 'order_approve.html', context=context)

    @transaction.atomic
    def post(self, request, order_code):
        order = get_object_or_404(models.Order, code=order_code)

        next_order = models.Order.objects.filter(completed=True,
                                                 reported=False,
                                                 created_date__gte=order.created_date).exclude(id=order.id) \
            .order_by('created_date').first()

        previous_order = models.Order.objects.filter(completed=True,
                                                     reported=False,
                                                     created_date__lte=order.created_date).exclude(id=order.id) \
            .order_by('-created_date').first()

        order.submitted_date = convert_utc_to_tenant_timezone(request, order.submitted_date)
        order.received_date = convert_utc_to_tenant_timezone(request, order.received_date)
        order.completed_date = convert_utc_to_tenant_timezone(request, order.completed_date)
        order.report_date = convert_utc_to_tenant_timezone(request, order.report_date)

        reports = models.Report.objects.filter(
            order=order, preliminary=False).order_by('-created_date')
        preliminary_reports = models.Report.objects.filter(
            order=order, preliminary=True).order_by('-created_date')

        editable_order_status_list = ['Unreceived', 'Pending Results', 'Pending Test Approval']

        context = get_result_forms(request, order.code)
        context.update({'order': order,
                        'editable_order_status_list': editable_order_status_list,
                        'reports': reports,
                        'preliminary_reports': preliminary_reports,
                        'next_order': next_order,
                        'previous_order': previous_order,
                        'order_approval_page': True,
                        })

        # validate report_approved_by form
        # if no user is specified, save current user
        report_approved_by_form = context['report_approved_by_form']
        if report_approved_by_form.is_valid():
            report_approved_by = report_approved_by_form.cleaned_data.get('user')
        else:
            report_approved_by = request.user

        save_test_results(request, order, context)
        approve_all_test_results(request, order)

        if 'save_approve_report_button' in request.POST:
            success = report_order(request, order, report_approved_by, preliminary=False)
            if success:
                messages.success(request, "Order: {} has been successfully approved and reported.".format(order.code))

                if next_order:
                    return redirect('order_approve', order_code=next_order.code)
                elif previous_order:
                    return redirect('order_approve', order_code=previous_order.code)
                else:
                    messages.info(request, "There are no more reports to approve.".format(order.code))
                    return render(request, 'order_approve.html', context=context)
            else:
                return redirect('order_approve', order_code=order.code)
        else:
            report_order(request, order, report_approved_by, preliminary=True)
            return redirect('order_approve', order_code=order.code)


class OrderAccessionNumberUpdateView(LabMemberMixin, View):
    """
    View that allows Lab members to update an Order's Accession number.
    Exposed via enable_accession_number_editing advanced setting.
    """
    def get(self, request, order_code):
        order = get_object_or_404(models.Order, code=order_code)
        form = forms.OrderAccessionNumberForm(
            initial={'accession_number': order.accession_number},
            order_id=order.id
        )
        context = {
            'form': form,
            'order': order,
        }
        return render(request, 'order_accession_number_update.html', context=context)

    def post(self, request, order_code):
        order = get_object_or_404(models.Order, code=order_code)
        form = forms.OrderAccessionNumberForm(request.POST, order_id=order.id)
        context = {
            'form': form,
            'order': order,
        }
        if form.is_valid():
            accession_number = form.cleaned_data['accession_number']
            order.accession_number = accession_number
            order.save()
            return redirect('order_details', order_code=order.code)
        else:
            return render(request, 'order_accession_number_update.html', context=context)


class OrderCommentUpdateView(LabMemberMixin, View):
    def get(self, request, order_code, order_comment_uuid):
        order = get_object_or_404(models.Order, code=order_code)
        order_comment = get_object_or_404(models.OrderComment, uuid=order_comment_uuid)

        form = forms.OrderCommentForm(initial={'comment': order_comment.comment})

        return render(request, 'order_comment_create.html', {'order': order,
                                                             'order_comment': order_comment,
                                                             'form': form})

    def post(self, request, order_code, order_comment_uuid):
        order = get_object_or_404(models.Order, code=order_code)
        order_comment = get_object_or_404(models.OrderComment, uuid=order_comment_uuid)

        form = forms.OrderCommentForm(request.POST)

        if form.is_valid():
            order_comment.comment = form.cleaned_data.get('comment')
            order_comment.save()

            # if coming from accessioning pages
            redirect_to = request.POST.get('redirect_to')
            if redirect_to:
                return redirect(redirect_to)
            return redirect('order_details', order_code=order.code)

        return render(request, 'order_comment_create.html', {'order': order,
                                                             'order_comment': order_comment,
                                                             'form': form})


class OrderCommentDeleteView(LabMemberMixin, View):

    def get(self, request, order_code, order_comment_uuid):
        order = get_object_or_404(models.Order, code=order_code)
        order_comment = get_object_or_404(models.OrderComment, uuid=order_comment_uuid)

        order_comment.is_active = False
        order_comment.save()

        # if coming from accessioning pages
        redirect_to = request.GET.get('redirect_to')
        if redirect_to:
            return redirect(redirect_to)
        return redirect('order_details', order_code=order.code)


class OrderTestPanelCommentCreateView(LabMemberMixin, View):
    def get(self, request, order_code, test_panel_uuid):
        order = get_object_or_404(models.Order, code=order_code)
        test_panel = get_object_or_404(models.TestPanel, uuid=test_panel_uuid)

        form = forms.TestPanelCommentForm()
        return render(request, 'order_comment_create.html', {'form': form,
                                                             'order': order,
                                                             'test_panel': test_panel})

    def post(self, request, order_code, test_panel_uuid):
        order = get_object_or_404(models.Order, code=order_code)
        test_panel = get_object_or_404(models.TestPanel, uuid=test_panel_uuid)
        form = forms.TestPanelCommentForm(request.POST)

        if form.is_valid():
            comment = form.cleaned_data.get('comment')
            is_public = form.cleaned_data.get('is_public')

            comment_obj = models.OrderTestPanelComment(order=order,
                                                       test_panel=test_panel,
                                                       comment=comment,
                                                       is_public=is_public)
            comment_obj.save()

            redirect_to = request.GET.get('redirect_to')
            if redirect_to:
                return redirect(redirect_to)
            return redirect('order_details', order_code=order_code)

        return render(request, 'order_comment_create.html', {'form': form,
                                                             'order': order,
                                                             'test_panel': test_panel})


class OrderTestPanelCommentUpdateView(LabMemberMixin, View):
    def get(self, request, order_code, test_panel_uuid, comment_uuid):
        order = get_object_or_404(models.Order, code=order_code)
        test_panel = get_object_or_404(models.TestPanel, uuid=test_panel_uuid)
        comment = get_object_or_404(models.OrderTestPanelComment, uuid=comment_uuid)

        form = forms.TestPanelCommentForm(initial={'comment': comment.comment,
                                                   'is_public': comment.is_public})

        return render(request, 'order_comment_create.html', {'form': form,
                                                             'order': order,
                                                             'test_panel': test_panel,
                                                             'update': True})

    def post(self, request, order_code, test_panel_uuid, comment_uuid):
        order = get_object_or_404(models.Order, code=order_code)
        test_panel = get_object_or_404(models.TestPanel, uuid=test_panel_uuid)
        comment_obj = get_object_or_404(models.OrderTestPanelComment, uuid=comment_uuid)

        form = forms.TestPanelCommentForm(request.POST)

        if form.is_valid():
            comment = form.cleaned_data.get('comment')
            is_public = form.cleaned_data.get('is_public')

            comment_obj.comment = comment
            comment_obj.is_public = is_public
            comment_obj.save()

            redirect_to = request.GET.get('redirect_to')
            if redirect_to:
                return redirect(redirect_to)
            return redirect('order_details', order_code=order_code)

        return render(request, 'order_comment_create.html', {'form': form,
                                                             'order': order,
                                                             'test_panel': test_panel})


class OrderTestPanelCommentDeleteView(LabMemberMixin, View):
    def get(self, request, order_code, test_panel_uuid, comment_uuid):
        comment_obj = get_object_or_404(models.OrderTestPanelComment, uuid=comment_uuid)
        comment_obj.is_active = False
        comment_obj.save()

        messages.success(request, "Comment successfully archived.")

        redirect_to = request.GET.get('redirect_to')
        if redirect_to:
            return redirect(redirect_to)
        return redirect('order_details', order_code=order_code)


class OrderTestCommentCreateView(LabMemberMixin, View):
    def get(self, request, order_code, test_uuid):
        order = get_object_or_404(models.Order, code=order_code)
        test = get_object_or_404(models.Test, uuid=test_uuid)

        form = forms.TestCommentForm()
        return render(request, 'order_comment_create.html', {'form': form,
                                                             'order': order,
                                                             'test': test})

    def post(self, request, order_code, test_uuid):
        order = get_object_or_404(models.Order, code=order_code)
        test = get_object_or_404(models.Test, uuid=test_uuid)

        form = forms.TestCommentForm(request.POST)

        if form.is_valid():
            comment = form.cleaned_data.get('comment')
            is_public = form.cleaned_data.get('is_public')

            comment_obj = models.OrderTestComment(order=order,
                                                  test=test,
                                                  comment=comment,
                                                  is_public=is_public)
            comment_obj.save()

            redirect_to = request.GET.get('redirect_to')
            if redirect_to:
                return redirect(redirect_to)
            return redirect('order_details', order_code=order_code)

        return render(request, 'order_comment_create.html', {'form': form,
                                                             'order': order,
                                                             'test': test})


class OrderTestCommentUpdateView(LabMemberMixin, View):
    def get(self, request, order_code, test_uuid, comment_uuid):
        order = get_object_or_404(models.Order, code=order_code)
        test = get_object_or_404(models.Test, uuid=test_uuid)
        comment = get_object_or_404(models.OrderTestComment, uuid=comment_uuid)

        form = forms.TestCommentForm(initial={'comment': comment.comment,
                                              'is_public': comment.is_public})

        return render(request, 'order_comment_create.html', {'form': form,
                                                             'order': order,
                                                             'test': test,
                                                             'update': True})

    def post(self, request, order_code, test_uuid, comment_uuid):
        order = get_object_or_404(models.Order, code=order_code)
        test = get_object_or_404(models.Test, uuid=test_uuid)
        comment_obj = get_object_or_404(models.OrderTestComment, uuid=comment_uuid)

        form = forms.TestCommentForm(request.POST)

        if form.is_valid():
            comment = form.cleaned_data.get('comment')
            is_public = form.cleaned_data.get('is_public')

            comment_obj.comment = comment
            comment_obj.is_public = is_public
            comment_obj.save()

            redirect_to = request.GET.get('redirect_to')
            if redirect_to:
                return redirect(redirect_to)
            return redirect('order_details', order_code=order_code)

        return render(request, 'order_comment_create.html', {'form': form,
                                                             'order': order,
                                                             'test': test})


class OrderTestCommentDeleteView(LabMemberMixin, View):
    def get(self, request, order_code, test_uuid, comment_uuid):
        comment_obj = get_object_or_404(models.OrderTestComment, uuid=comment_uuid)
        comment_obj.is_active = False
        comment_obj.save()

        messages.success(request, "Comment successfully archived.")

        redirect_to = request.GET.get('redirect_to')
        if redirect_to:
            return redirect(redirect_to)
        return redirect('order_details', order_code=order_code)
