function autofill_zip_code(autocomplete, zip_code_id) {
    autocomplete.setFields(
            ['address_components', 'formatted_address']);
    // get zip code and autofill zipcode field
    autocomplete.addListener('place_changed', function() {
        var place = autocomplete.getPlace();

        var zip_code = place.formatted_address.split(',')[2].trim().split(' ')[1];
        document.getElementById(zip_code_id).value = zip_code;
    });
}