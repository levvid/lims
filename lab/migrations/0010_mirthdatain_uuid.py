# Generated by Django 2.0.2 on 2019-05-28 14:09

from django.db import migrations, models
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('lab', '0009_mirthdatain_original_file_name'),
    ]

    operations = [
        migrations.AddField(
            model_name='mirthdatain',
            name='uuid',
            field=models.UUIDField(db_index=True, default=uuid.uuid4, editable=False),
        ),
    ]
