import os
import uuid

from django.utils import timezone

from . import models
from clients import models as clients_models
from lab_tenant import models as lab_tenant_models
from patients import models as patients_models

DATETIME_FORMAT = '%Y%m%d%H%M'
MESSAGE_TYPE = 'ORM'
PROCESSING_ID_DICT = {'False': 'P',
                      'True': 'D'}
SENDING_APPLICATION = 'DENDI'
VERSION_ID = '2.5.1'


def get_order_out_message(order_id, tenant_id):
    """
    Generate an order out hl7 message for each offsite laboratory
    :param order_id:
    :return: Dict {offsite_laboratory_id: {'message': hl7_message,
                                           'uuid': message_uuid}}
    A dictionary of hl7 messages or None
    """
    order = models.Order.objects.get(id=order_id)
    tests = order.tests.all()
    offsite_tests = [x for x in tests if x.test_type and x.test_type.offsite_laboratory]

    if offsite_tests:
        message_dict = {}
        offsite_laboratories = list(set([x.test_type.offsite_laboratory for x in offsite_tests]))

        for offsite_laboratory in offsite_laboratories:
            test_list = [x for x in offsite_tests if x.test_type.offsite_laboratory == offsite_laboratory]

            message_control_id = uuid.uuid4()
            # message header
            message = get_msh(tenant_id, offsite_laboratory, message_control_id)
            # PID
            message += get_pid(order.patient_id)
            # Common Order Segment
            message += get_orc(order.id)
            # OBR - Observation Request
            obr_set_id = 1
            for offsite_test in test_list:
                message += get_obr(order.id, offsite_test, obr_set_id)
                obr_set_id += 1

            message_dict[offsite_laboratory.id] = {'message': message,
                                                   'uuid': message_control_id}
        return message_dict
    else:
        return None


def get_msh(tenant_id, offsite_laboratory, message_control_id):
    tenant = clients_models.Client.objects.get(id=tenant_id)

    integration = lab_tenant_models.OffsiteLaboratoryIntegration.objects.filter(offsite_laboratory=offsite_laboratory).get()
    if integration:
        sending_facility = integration.sending_facility_id
    else:
        sending_facility = "{}^{}".format(tenant.name, tenant.id)
    message_datetime = timezone.now().strftime(DATETIME_FORMAT)
    text = 'MSH|^~\&|{}|{}|{}|{}|{}||{}|{}|{}|{}\r'.format(SENDING_APPLICATION,
                                                           sending_facility,
                                                           offsite_laboratory.receiving_application,
                                                           offsite_laboratory.receiving_facility,
                                                           message_datetime,
                                                           MESSAGE_TYPE,
                                                           message_control_id,
                                                           PROCESSING_ID_DICT[os.environ.get('DEBUG')],
                                                           VERSION_ID)
    return text


def get_pid(patient_id):
    """
    Utilized elements: 3 Patient Identifier List, 5 Patient Name, 7 Date/Time of Birth, 8 Administrative Sex,
    19 SSN Number
    :param patient_id:
    :return:
    """
    patient = patients_models.Patient.objects.get(id=patient_id)
    patient_identifier_list = patient.uuid
    patient_name = "{}^{}^^^^".format(patient.user.last_name, patient.user.first_name)
    birth_datetime = patient.birth_date.strftime(DATETIME_FORMAT)
    administrative_sex = patient.sex
    patient_ssn = "{}".format(patient.social_security)

    text = "PID||{}|||{}||{}|{}|||||||||||{}|||||||||||||||||||||||||||||||||||\r".format(
        patient_identifier_list,
        patient_name,
        birth_datetime,
        administrative_sex,
        patient_ssn

    )
    return text


def get_orc(order_id):
    """
    Utilized elements:
    :param order_id:
    :return:
    """
    order = models.Order.objects.get(id=order_id)
    order_datetime = order.submitted_date.strftime(DATETIME_FORMAT)
    if order.provider:
        provider = order.provider
        if provider.npi:
            provider_id = provider.npi
        elif provider.id_num:
            provider_id = provider.id_num
        else:
            provider_id = ''

        ordering_provider = "{}^{}^{}^^^{}".format(provider_id, provider.user.last_name, provider.user.first_name,
                                                   provider_id)
    else:
        ordering_provider = ''
    enterer_location = ''

    text = "ORC|NW|{}|||||||{}|||{}|{}||{}\r".format(
        order.code,
        order_datetime,
        ordering_provider,
        enterer_location,
        order_datetime,
    )

    return text


def get_obr(order_id, offsite_test, obr_set_id):
    """
    www.hl7.eu/refactored/segOBR.html
    :param order_id:
    :return:
    """
    order = models.Order.objects.get(id=order_id)
    universal_service_identifier = "{}^{}".format(offsite_test.test_type.offsite_test_id, offsite_test.test_type.name)
    if order.provider:
        provider = order.provider
        if provider.npi:
            provider_id = provider.npi
        elif provider.id_num:
            provider_id = provider.id_num
        else:
            provider_id = ''

        ordering_provider = "{}^{}^{}^^^{}".format(provider_id, provider.user.last_name, provider.user.first_name,
                                                   provider_id)
    else:
        ordering_provider = ''

    text = "OBR|{}|{}||{}||||||||||||{}".format(
        obr_set_id,
        order.code,
        universal_service_identifier,
        ordering_provider
    )

    return text
