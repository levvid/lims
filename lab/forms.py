from django import forms
from timezone_field import TimeZoneFormField

from . import models
from lab_tenant import models as lab_tenant_models

#####################################################################################################################
PASSWORD_LENGTH = 6

#####################################################################################################################


class LoginForm(forms.Form):

    username = forms.CharField()
    password = forms.CharField()


class TwoFactorAuthForm(forms.Form):
    two_factor_code = forms.CharField(max_length=6, label='Two-factor code', required=True)


class TwoFactorAuthSetupForm(forms.Form):
    two_factor_shared_secret = forms.CharField(max_length=16, label='Shared Secret', required=True)
    two_factor_verify_add = forms.BooleanField(
        label='I have added the 2FA secret key to my Google Authenticator App.',
        initial=False,
        widget=forms.CheckboxInput(),
        required=True)


class ForgotPasswordForm(forms.Form):
    username = forms.CharField(max_length=255, label='Username', required=True)

    def clean_username(self):
        username = self.cleaned_data.get('username').strip()
        if not lab_tenant_models.User.objects.filter(username__iexact=username):
            raise forms.ValidationError('We could not find your username in the system. Please try again.')
        return username


class SettingsPasswordForm(forms.Form):

    current_password = forms.CharField(widget=forms.PasswordInput())
    new_password = forms.CharField(widget=forms.PasswordInput())
    confirm_new_password = forms.CharField(widget=forms.PasswordInput())

    def clean_new_password(self):
        new_password = self.cleaned_data['new_password']
        if len(new_password) < PASSWORD_LENGTH:
            raise forms.ValidationError(
                'Password must be at least {} characters long'.format(
                    PASSWORD_LENGTH))
        return new_password

    def clean_confirm_new_password(self):
        confirm_new_password = self.cleaned_data['confirm_new_password']
        return confirm_new_password

    def clean(self):
        if not self.cleaned_data.get('new_password') == self.cleaned_data.get(
                'confirm_new_password'):
            self.add_error(
                'confirm_new_password',
                forms.ValidationError('New passwords should match.'),
            )


class AdvancedSettingsForm(forms.Form):
    use_docraptor = forms.BooleanField(
        label='Use experimental pdf generation engine (DocRaptor)',
        initial=False,
        widget=forms.CheckboxInput(attrs={'data-toggle': 'toggle'}),
        required=False)
    skip_insurance = forms.BooleanField(
        label='Skip patient insurance/payer information during order create',
        initial=False,
        widget=forms.CheckboxInput(attrs={'data-toggle': 'toggle'}),
        required=False)
    skip_patient_history = forms.BooleanField(
        label='Skip patient history during order create',
        initial=False,
        widget=forms.CheckboxInput(attrs={'data-toggle': 'toggle'}),
        required=False)
    require_icd10 = forms.BooleanField(
        label='Require ICD10 diagnosis codes during order create',
        initial=False,
        widget=forms.CheckboxInput(attrs={'data-toggle': 'toggle'}),
        required=False)
    require_collected_by = forms.BooleanField(
        label='Require specimen collector information during order create',
        initial=False,
        widget=forms.CheckboxInput(attrs={'data-toggle': 'toggle'}),
        required=False)
    hide_instrument_integration = forms.BooleanField(
        label='Hide features related to instrument integrations',
        initial=False,
        widget=forms.CheckboxInput(attrs={'data-toggle': 'toggle'}),
        required=False)
    show_prescribed_medication = forms.BooleanField(
        label='Show prescribed medications and POCT screening in requisition form',
        initial=False,
        widget=forms.CheckboxInput(attrs={'data-toggle': 'toggle'}),
        required=False)
    hide_fda_tests = forms.BooleanField(
        label='Hide FDA Approved Tests Import feature',
        initial=False,
        widget=forms.CheckboxInput(attrs={'data-toggle': 'toggle'}),
        required=False)
    hide_specimen_validity = forms.BooleanField(
        label='Hide Specimen Validity related features',
        initial=False,
        widget=forms.CheckboxInput(attrs={'data-toggle': 'toggle'}),
        required=False)
    show_research_consent = forms.BooleanField(
        label='Show option to consent to research use only (RUO) tests on Order Create',
        initial=False,
        widget=forms.CheckboxInput(attrs={'data-toggle': 'toggle'}),
        required=False)
    show_patient_result_request = forms.BooleanField(
        label='Show option for patient to request results on Order Create',
        initial=False,
        widget=forms.CheckboxInput(attrs={'data-toggle': 'toggle'}),
        required=False)
    enable_accession_number = forms.BooleanField(
        label='Assign sequential accession numbers to orders when received',
        initial=False,
        widget=forms.CheckboxInput(attrs={'data-toggle': 'toggle'}),
        required=False)
    enable_date_encoded_accession_number = forms.BooleanField(
        label='Use date encoded accession numbers with format YYMMDD#####',
        initial=False,
        widget=forms.CheckboxInput(attrs={'data-toggle': 'toggle'}),
        required=False)
    enable_accession_number_editing = forms.BooleanField(
        label='Enable editing of accession numbers',
        initial=False,
        widget=forms.CheckboxInput(attrs={'data-toggle': 'toggle'}),
        required=False)
    enable_patient_vitals = forms.BooleanField(
        label='Show patient vitals on Order Create',
        initial=False,
        widget=forms.CheckboxInput(attrs={'data-toggle': 'toggle'}),
        required=False)
    use_zebra_printer = forms.BooleanField(
        label='Use zebra label printer',
        initial=False,
        widget=forms.CheckboxInput(attrs={'data-toggle': 'toggle'}),
        required=False)
    show_abn_on_req_form = forms.BooleanField(
        label='Show ABN on Requisition Form',
        initial=False,
        widget=forms.CheckboxInput(attrs={'data-toggle': 'toggle'}),
        required=False)
    enable_individual_analytes = forms.BooleanField(
        label='Enable selecting of individual analytes',
        initial=False,
        widget=forms.CheckboxInput(attrs={'data-toggle': 'toggle'}),
        required=False)
    enable_document_scanning = forms.BooleanField(
        label='Enable document scanning',
        initial=False,
        widget=forms.CheckboxInput(attrs={'data-toggle': 'toggle'}),
        required=False)
    enable_document_uploading = forms.BooleanField(
        label='Enable uploading of additional documents',
        initial=False,
        widget=forms.CheckboxInput(attrs={'data-toggle': 'toggle'}),
        required=False)
    use_accession_number_barcode = forms.BooleanField(
        label='Use accession number for barcodes',
        initial=False,
        widget=forms.CheckboxInput(attrs={'data-toggle': 'toggle'}),
        required=False)
    show_ten_digit_barcode = forms.BooleanField(
        label='Show sample label with 10 digit barcode',
        initial=False,
        widget=forms.CheckboxInput(attrs={'data-toggle': 'toggle'}),
        required=False)
    hide_report_approved_by = forms.BooleanField(
        label='Hide report approved by field',
        initial=False,
        widget=forms.CheckboxInput(attrs={'data-toggle': 'toggle'}),
        required=False)
    confirm_order_receipt = forms.BooleanField(
        label='Confirm receipt of order',
        initial=False,
        widget=forms.CheckboxInput(attrs={'data-toggle': 'toggle'}),
        required=False)
    automatically_print_labels = forms.BooleanField(
        label='Automatically print labels',
        initial=False,
        widget=forms.CheckboxInput(attrs={'data-toggle': 'toggle'}),
        required=False)
    use_two_factor_auth = forms.BooleanField(
        label='Use two-factor authentication',
        initial=False,
        widget=forms.CheckboxInput(attrs={'data-toggle': 'toggle'}),
        required=False)


class UserCreateForm(forms.Form):

    last_name = forms.CharField(max_length=255, label='Last Name', required=True,
                                error_messages={'required': 'Last name is required.'})
    first_name = forms.CharField(max_length=255, label='First Name', required=True,
                                 error_messages={'required': 'First name is required.'})
    email = forms.CharField(max_length=255, label='Email', required=True,
                            widget=forms.EmailInput(attrs={'class': 'form-control'}))

    password = forms.CharField(widget=forms.PasswordInput())
    confirm_password = forms.CharField(widget=forms.PasswordInput())

    user_type = forms.ChoiceField(choices=((2, 'Collaborator'), (3, 'Collector/Phlebotomist'), (1, 'Admin')),
                                  required=True)

    def clean_password(self):
        password = self.cleaned_data['password']
        if len(password) < PASSWORD_LENGTH:
            raise forms.ValidationError(
                'Password must be at least {} characters long'.format(PASSWORD_LENGTH))
        return password

    def clean(self):
        email = self.cleaned_data['email']

        if lab_tenant_models.User.objects.filter(username=email):
            self.add_error('email', forms.ValidationError('A user with the email {} already exists.'.format(email)))

        if not self.cleaned_data.get('password') == self.cleaned_data.get('confirm_password'):
            self.add_error('confirm_password', forms.ValidationError('Passwords should match.'))


class UserUpdateForm(forms.Form):

    last_name = forms.CharField(max_length=255, label='Last Name', required=True,
                                error_messages={'required': 'Last name is required.'})
    first_name = forms.CharField(max_length=255, label='First Name', required=True,
                                 error_messages={'required': 'First name is required.'})

    password = forms.CharField(widget=forms.PasswordInput(), label='New Password', required=False)
    confirm_password = forms.CharField(widget=forms.PasswordInput(), label='Confirm New Password', required=False)

    user_type = forms.ChoiceField(choices=((2, 'Collaborator'), (1, 'Admin')),
                                  required=True)

    def clean_password(self):
        password = self.cleaned_data['password']
        if password and (len(password) < PASSWORD_LENGTH):
            raise forms.ValidationError(
                'Password must be at least {} characters long.'.format(
                    PASSWORD_LENGTH))
        return password

    def clean_confirm_password(self):
        confirm_password = self.cleaned_data['confirm_password']
        return confirm_password

    def clean(self):
        password = self.cleaned_data.get('password')
        confirm_password = self.cleaned_data.get('confirm_password')

        if password and confirm_password:
            if not self.cleaned_data.get('password') == self.cleaned_data.get(
                    'confirm_password'):
                self.add_error(
                    'confirm_password',
                    forms.ValidationError('New passwords should match.'),
                )


class ManageProviderUpdateForm(UserUpdateForm):
    # this field is not required
    user_type = forms.ChoiceField(required=False)


class LaboratoryUpdateForm(forms.Form):
    clia_number = forms.CharField(max_length=255, label='CLIA Number', required=False)
    clia_director = forms.CharField(max_length=255, label="Laboratory Director", required=False)
    phone_number = forms.CharField(max_length=255, label='Phone', required=False)
    fax_number = forms.CharField(max_length=255, label='Fax', required=False)
    website = forms.CharField(max_length=255, label='Website', required=False)
    address_1 = forms.CharField(max_length=255, label='Address', required=False)
    address_2 = forms.CharField(max_length=255, label='Apartment, Suite, Unit', required=False)
    timezone = TimeZoneFormField(label='Laboratory Time Zone')
