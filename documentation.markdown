<h3>App descriptions:</h3>

- accounts - Client's accounts and providers
- *clients - Dendi's clients (laboratories)
- codes - Medical codes
- *lab - laboratory lis views, test category, test target
- main - settings
- orders - Client's orders, tests, samples, procedures
- patients - Client's Patients
- *public - Public views (dendisoftware.com without subdomain)

*(marked with asterisks) data for these apps is shared in public schema.

<h3>DB migrations:</h3>

```
- python manage.py makemigrations
- python manage.py migrate_schemas
```

<h3>SSL instructions:</h3> 

The current SSL certificate is one purchased from Digicert. Digicert was the only vendor to offer a
combined SANS and a wildcard certificate. You can usually either have a certificate for a root/apex/naked domain OR
a wildcard, but not both. Only vendors like Digicert or Cloudflare allow you to do this.

For the production environment, the DNS for dendisoftware.com is entirely hosted by DNSimple. Even though 
we "own" the domain on AWS, the nameservers are all using DNSimple, and it seems like there was an 
integration with Heroku as well. See link: https://dnsimple.com/a/83739/domains/dendisoftware.com/records
<br><br>
For the staging environment, the DNS for dendi-lis-staging.com is hosted by Google. 
See link: https://domains.google.com/m/registrar/dendi-lis-staging.com/dns?hl=en&authuser=1 for details.

- Digicert SSL - certificate is manually installed on Heroku app (see settings)

- Comodo SSL - purchased by accident on DNSimple, should be deactivated and unavailable

- Route 53 - holds the domain but doesn't administer DNS

- DNSimple - administers the DNS (nameserver or NS) and the mailserver (MX). This means that the nameservers listed on
Route 53 are pointing to DNSimple nameservers.

- Google Domains - hosts the staging website (dendi-lis-staging.com). 

Further explanations & examples can be seen on the below sites: 
- https://www.caffeinecoding.com/setting-up-subdomains-on-one-express-app-in-heroku/
- https://gabebw.com/blog/2016/06/06/how-to-host-sites-on-a-subdomain-with-heroku

<h3>Renaming a DB on PostgreSQL</h3>
Renaming can be tricky because of this issue: https://stackoverflow.com/questions/17449420/postgresql-unable-to-drop-database-because-of-some-auto-connections-to-db
<br><Br>
Follow this example to rename a database:<br>
```
ALTER DATABASE lisproduction RENAME TO lis;

REVOKE CONNECT ON DATABASE lisproduction FROM public;

SELECT pg_terminate_backend(pg_stat_activity.pid)
FROM pg_stat_activity
WHERE pg_stat_activity.datname = 'lisproduction';
```

<h3>Model Managers</h3>
As can be seen here: https://github.com/django/django/blob/master/django/shortcuts.py#L83,
the get_object_or_404 method uses the _get_queryset method, which in turn uses a default manager.
According to the official Django documentation, the default manager is called "objects" (as in Model.objects.all())

However, if you use custom Manager objects, take note that the first Manager Django encounters (in the order in which
they’re defined in the model) has a special status. Django interprets the first Manager defined in a class as the
“default” Manager, and several parts of Django (including dumpdata) will use that Manager exclusively for that model.

For our purposes, we override Django's default manager (objects). This means that
get_object_or_404 and get_list_or_404 methods both return only active objects.



<h3>User Types:</h3>

There are currently 4 User types:

- Lab Admin
- Lab Collaborator
- Provider
- Patient

Authentication is User management is different for each of these types.

For usernames, when User objects are created, then depending on the User type (Lab Admin, Lab Collaborator, Provider, etc.)
the username may be different. The default is to use the email as the username. When User objects are deleted,
the username will be updated to the User's UUID.

Provider:
- Password is by default the provider's UUID


<h3>Unique Identifiers:</h3>

Ideally, we'd want to enforce data integrity at the database layer whenever possible (duplicate objects should trigger
an IntegrityError, for example). However, this isn't possible in an elegant manner due to the fact that we don't delete
objects - which means that every time an object is "deleted" and re-created, it causes an IntegrityError.

Therefore, the best way to handle data integrity is simply through forms.

CharField and TextFields should usually always be defined with blank=True. null=True should be avoided due to the fact that whenever a string-based field has null=True,
it means that there's two possible values for “no data”: NULL, and the empty string. In most cases, it’s redundant to have two possible values for “no data;”
the Django convention is to use the empty string, not NULL. One exception is when a CharField has both unique=True and blank=True set. In this
situation, null=True is required to avoid unique constraint violations when saving multiple objects with blank values.


<h3>Migration Issues</h3>

<h5>1. Migrations with NULL values that need to be non-NULLs - "django.db.utils.DatabaseError: cannot ALTER TABLE "fooapp_emailsender" because it has pending trigger events"</h5>
If you need to update a schema manually during a migration, you can write a
script to run alongside (or before/after) the actual migration. For an example of this, see orders/migrations/0052_auto_20190113_1835.py

<h5>2. django.db.utils.ProgrammingError: column "actor_id" of relation "auditlog_logentry" already exists</h5>
This happens frequently while running migrations. For some reason, even though that migration has already run, it isn't
recognized, which is why the error happens. <br><br>

<b>Here's an algorithm for making sure this is resolved:</b><br>
Fake the migration for that tenant, up until (and including) the latest migration.<br>
```python manage.py migrate_schemas --schema=tenant_name --fake ``` <br><br>
Show migrations:<br>
```python manage.py showmigrations```<br><br>
Find the the migrations that you want to unfake (the migrations that you were trying to do all along), 
and "unfake" them by running a fake migration up until the previous migration. This sounds a bit complicated, but it 
essentially means, "if you want to actually run migration #10, then fake migrate until migration #9, then migrate
for real". Note: no need for the actual migration name, just the number is needed):<br>
```python manage.py migrate_schemas --schema=tenant_name  --fake orders 0020```<br><br>
Once the migration that we actually want to happen is "unfaked", then we can run the migration:<br>
```python manage.py migrate_schemas --schema=tenant_name```

Source: https://stackoverflow.com/questions/30626886/how-to-redo-a-migration-on-django-1-8-after-using-fake


<h3>Resetting Migrations</h3>

Sometimes you need to reset migration files because they're getting too cumbersome and complicated. Perhaps the 
migrations are breaking because of backwards compatibility, or it's just taking too long. This is the best way to "reset" 
migrations:<br>

Make sure development and production environments match up - if not, deploy to make sure they do:<br>
```git push heroku master```

<h5>WARNING!</h5>
**Be very careful to do a migration on production so that development and production dbs are 
synced up before clearing any migration history (otherwise will cause lots of heartache):<br>
```heroku run python manage.py migrate_schemas```<br>

Clear the migration history for each app:<br>
```python manage.py migrate_schemas --fake accounts zero```<br>
```python manage.py migrate_schemas --fake clients zero```<br> and etc...<br><br>
Go through each of your apps migration folders and remove everything inside, except for the ```__init__.py``` file.
<br><br>
Create the initial migrations again: <br>
```python manage.py makemigrations```<br>

You may have to fake migrations for initial Django apps: <br>
```python manage.py migrate_schemas --fake contenttypes```<br>
```python manage.py migrate_schemas --fake auditlog```<br>
```python manage.py migrate_schemas --fake sites```<br>

Fake the initial migration - you won’t be able to apply the initial migration because the database table already exists:<br>
```python manage.py migrate_schemas --fake-initial```<br>

Do the above again in Heroku: <br>
```heroku run python manage.py migrate_schemas --fake-initial``` <br>

Now the development and production migrations should be reset. 


<h3>Manually Running Functions During Migrations</h3>

We can run functions manually for tasks like replacing all NULL values for a given field in a model (otherwise you'd 
have to do it with a standalone script, which is complicated since you need to run that script for every schema
in both development and production). 

You do this by using Django's RunPython:
```
def convert_null_to_blank(apps, schema_editor):
    # We get the model from the versioned app registry;
    # if we directly import it, it'll be the wrong version
    TestType = apps.get_model("orders", "TestType")
    db_alias = schema_editor.connection.alias
    test_types = TestType.objects.using(db_alias).filter()

    for test_type in test_types:
        # check for NULL values and change to blank strings
        if test_type.isotype is None:
            test_type.isotype = ''
        if test_type.manufacturer is None:
            test_type.manufacturer = ''
        if test_type.analyte is None:
            test_type.analyte = ''

        # save after edit
        test_type.save()


class Migration(migrations.Migration):

    dependencies = []

    operations = [
        migrations.RunPython(convert_null_to_blank),
    ]
```

Notice that ```apps.get_model()``` is used here. This is critical, because you want to use the model at the point
point in time from the perspective of the migration, not a future version of the model. 
<br><br>

<h3>Migration on Heroku Deployment</h3>

We automatically run a migration upon deployment to Heroku, using the Procfile (see release). If the automatic migration
fails, Heroku will release an error, and the migration will have to be performed manually. The release command is run 
immediately after a release is created, but before the release is deployed to the app’s dyno formation.

If this happens, remove the ```release: python manage.py migrate_schemas --noinput``` line from the Procfile, then
run the deployment + migration. The cause of failure will probably be one of the aforementioned reasons above (under 
the "Migration Issues" section). Fix the issue, run the migration successfully,
then convert the Procfile back to its original state. <br><br>

<h3>Timezones</h3>
Timezones have been a huge headache to deal with so far. There is no correct solution to this problem - every solution 
has its pros and cons. Preserving date integrity is especially important for us, since we want everyone to agree on exactly what date an event 
occurred in the past. We already do this on the database level by saving everything as UTC. <br><br>

The tricky part is that any given exportable report (PDF, Excel) is effectively a “hard copy” - and we want every 
version of that hard copy to return the same data for a given date range, regardless of how distributed the userbase is. 
We're sacrificing a bit of upfront user-friendliness here, for more user-friendliness down the line. 
We're also assuming that most users will not care to specify their local time or time zone in their search results. 
<br><br>
An especially important use-case for having a good timezone system is the provider portal. Providers should view 
everything from the laboratory operations’ point-of-view, rather than from their own vantage point. That way the 
communication between provider and lab is seamless - when a provider views a report, it should have the same collected 
date & reported date as when a lab director views it.
<br><br>
The downside to consistency is that users operating out of different time zones will have to “adapt” to 
the system-wide time. This may lead to an unintuitive user experience, but if it becomes a big enough issue we can always refactor. 
<br><br>
Regardless of implementation, we also have to be more explicit in terms of specifying time zones across the UI. And since 
we’re fixating on a singular timezone, we also need to turn off Django’s native timezone support, since it implicitly converts 
times to a user’s local time - we don’t want that.
<br><br>
To display tenant time in html use the as_timezone template tag:
<br>
`{% load timezone %}`<br>
`{{ collection_datetime|as_timezone:request.tenant.timezone }}`

<br>
<h3>"Delete" Prompt Using JavaScript</h3>
Oftentimes, we want to prompt the user before deleting an item. The easiest formula for doing this looks like: 

```
<a href="{% url 'sample_delete' order.code sample.uuid %}">
  <i class="fas fa-trash-alt sample-delete text-danger" data-toggle="tooltip" title="Archive sample"></i>
</a>
```

```
// confirm deleting a sample and associated tests
$('.sample-delete').click(function(event) {
  let result = confirm('Are you sure you want to archive this sample? This will also archive the associated tests with it.');
  if(!result) {
    event.preventDefault();
  }
});
```
<br>
<h3>More Date & Time-Related Topics</h3>
For most queries dealing with datetimes, we can use functions in ```main/functions.py``` to convert datetimes to 
the appropriate timezones. 
- Date ranges are time agnostic, meaning 12:00AM for both the start range and the end range
- Date ranges are also inclusive, because that's how most people think. We'll ignore the common convention and 
counterargument, since we've already been going against the convention:
https://stackoverflow.com/questions/9795391/is-there-a-standard-for-inclusive-exclusive-ends-of-time-intervals


<h3>Loading Screen (CSS)</h3>
In cases of high load times (either by GET or POST requests), use a loading screen to indicate that the request 
is actually going through. This is done by using the prebuilt loading screen template, located in base.css and
base.html (in the main div). <br><br>

It's a bit annoying to see the loading screen for every page request, so we should use it sparingly for when a page is
actually expected to be a bit slow. In our current implementation, we only use it for slow POST requests, so the 
actual loading screen is hidden by default, and then shown upon form submission (button click). If we were to use it 
for slow page GETs, the implementation would need to be updated to show the loading screen by default, then hide it
when the page is finished loading. As of March 26, 2019, we haven't implemented this yet. <br><br>

An example implementation for a POST request:<br>
```.javascript
// enable loading screen
$('#create-order-button').click(function(){
  $('#overlay').css('visibility', 'visible');
});
```

<h3>Reflex tests</h3>

Available reflex tests are shown in an alert message on the order details page. Click the alert to see which reflex
tests were triggered and to add them to the order. Currently, the available reflex tests alert does not go away when
reflex tests are added.

<h3>CLIA Lab List</h3>
All CLIA labs, regardless of application type (waiver, accreditation, compliance) will be listed in the development 
database, under ```clients.CLIALabs```. We use the development db for all of these queries, since we don't want to
take up bandwidth on the production db. The source db is updated monthly (seems to be around the 6th), so we'll run a 
cron task on the 7th of every month. 


<h3>Task Queue</h3>
Task Queue is implemented with Celery and RabbitMQ.
First time installations when running locally: Install rabbitMQ
```.python
https://www.rabbitmq.com/download.html
```

When running locally, start celery worker along with django server by using
```.python
heroku local -p 8000
```
To start an worker instance
```.python
celery -A main worker -l info
```
To scale worker instances on heroku
```.python
# Staging
heroku ps:scale -r staging worker=1
# Production
heroku ps:scale worker=1
```

<h3>SFTP</h3>
<h5>Adding a new lab</h5>
ssh into the sftp server as the sudo user:
```bash
ssh -i "dendisftp.pem" ubuntu@ec2-54-226-218-38.compute-1.amazonaws.com
```
Create a new lab account
```bash
sudo ./create_lab_sftp.sh
```
You will be prompted to set and confirm the new lab account's password and other lab information.
<h5>Connecting to the sftp server</h5>
```
sftp -oPort=1010 labname@54.226.218.38
```
Or use your favourite sftp client on port 1010

<h5>Configuration File</h5>
To modify the sftp server configuration, open the configuration file by running:
```
sudo nano /etc/ssh/sshd_config
```
<h6>Current Configuration</h6>
```bash
# More ports can be added depending either on a per lab basis, or for incoming and outgoing connections
Port 22

Match Group sftp_users
    ForceCommand internal-sftp -d /mirth
    ChrootDirectory /home/%u
    X11Forwarding no
    AllowTcpForwarding no  
    PermitTunnel no
    AllowAgentForwading no
   
```
After editing the configuration file, restart OpenSSH server by running:
```bash
sudo systemctl restart sshd
```

To access other lab directories when not logged under the lab account, use the root acount. After ssh'ing as ```ubuntu```. type in the following sudo command to;
```sudo bash``` R ```sudo -s``` to become root. Then ```cd``` as usual to any lab's directory.

<h6>Editing Set Up Script</h6>
After ssh'ing to the sftp server as A sudo user, run 
```
sudo nano create_lab_sftp.sh
```
After editing the script, set execute permission on script by running:
```
sudo chmod +x create_lab_sftp.sh
```

<h3>Google Maps Places API</h3>
For billing: https://console.developers.google.com/billing/010E50-4B5836-742700/reports?credit=1&dateType=INVOICE_DATE&from=2019-07-01&to=2019-07-31&authuser=1&organizationId=962986253262


<h3>Reference Ranges</h3>
Reference ranges are stored under TestTypeReferenceRanges.
If ranges are instrument specific, they are stored in InstrumentCalibration
<br>

<h3>Report Logic</h3>
ILDP: 
- Show "Positive or Inconsistent LC-MS Results Summary"
- Not showing "Toxicology Screening Result Summary"
- We want both the order code adn the accession number in the report
- Is sample ID required to be on the report? 

<h3>Setting up network drives for clients (Cloudberry)</h3>
- Download MSP 360 (Cloudberry) Drive https://www.cloudberrylab.com/
- Log in to cloudberry account (sjung@dendisoftware.com) to purchase license (~$40)
- Map network drive using S3 access key and secret key
- Set master console password
- Restart client computer

<h3>Referencing Jira tickets in commit message</h3>
- Denote issue key in the commit message