#!/bin/bash

python -m staging_environment.convert_production_to_staging_client
python manage.py migrate_schemas --noinput
