import django
import os

from django.db import transaction
from tenant_schemas.utils import tenant_context

os.environ['DJANGO_SETTINGS_MODULE'] = 'main.settings'
django.setup()

from clients import models as clients_models
from orders import models as orders_models

"""
Scrip to import ContainerType objects for Mako
"""

SCHEMA_NAME = 'mako'
CONTAINER_TYPES = [
    {'name': 'GOLD SST', 'volume': 5},
    {'name': 'Tiger Top SST', 'volume': 8.5},
    {'name': 'Red Top Serum (4mL)', 'volume': 4},
    {'name': 'Red Top Serum (6mL)', 'volume': 6},
    {'name': 'Red Top Serum (9mL)', 'volume': 9},
    {'name': 'Sodium Fluoride', 'volume': 6},
    {'name': 'Sodium Heparin', 'volume': 6},
    {'name': 'Plasma Lithium Heparin (4mL)', 'volume': 4},
    {'name': 'Plasma Lithium Heparin (6mL)', 'volume': 6},
    {'name': 'Plasma Lithium Heparin (9mL)', 'volume': 9},
    {'name': 'Trace Elements K2 EDTA', 'volume': 6},
    {'name': 'Trace Elements Non-Additive', 'volume': 6},
    {'name': 'Light Blue (Sodium Citrate)', 'volume': 3},
    {'name': 'Lavender', 'volume': 4},
    {'name': 'Sterile Collection Container', 'volume': 4},
    {'name': 'Sterile Urine Container', 'volume': 4},
    {'name': 'Tiger Top Urine Tube', 'volume': 4},
    {'name': 'Yellow Top Urine Tube', 'volume': 4},
    {'name': 'Para-Pak White', 'volume': 4},
    {'name': 'Para-Pak Orange', 'volume': 4},
    {'name': 'Para-Pak Green', 'volume': 4},
    {'name': 'Hemoccult Slides', 'volume': None},
    {'name': 'Stool (Unpreserved) Sterile Container', 'volume': None},
    {'name': 'UT-VTM (Purple Top)', 'volume': None},
    {'name': 'Viral Transport Media (Red Top)', 'volume': None},
    {'name': 'GenExpert Swab', 'volume': None},
    {'name': 'eSwab', 'volume': None},
    {'name': 'Yellow Top GeneXpert Urine', 'volume': None},
    {'name': 'APTIMA Urine', 'volume': None},
    {'name': 'APTIMA White Specimen Transfer Swab', 'volume': None},
    {'name': 'APTIMA Green STM Swab', 'volume': None},
    {'name': 'Pink Top GeneXpert Swab', 'volume': None},
    {'name': 'APTIMA Orange Multitest Swab', 'volume': None},
    {'name': 'Pink Top GeneXpert Swab', 'volume': None},
    {'name': 'Bactec Culture Vials', 'volume': None},
    {'name': 'Thin Prep', 'volume': None},
    {'name': 'Formalin Container', 'volume': None},
]


@transaction.atomic
def main():
    tenant = clients_models.Client.objects.get(schema_name=SCHEMA_NAME)

    with tenant_context(tenant):
        for container_type in CONTAINER_TYPES:
            orders_models.ContainerType.objects.update_or_create(
                name=container_type['name'],
                defaults={'volume': container_type['volume']})


if __name__ == '__main__':
    main()
