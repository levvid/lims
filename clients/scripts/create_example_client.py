import django
import logging
import os
import sys

os.environ['DJANGO_SETTINGS_MODULE'] = 'main.settings'
django.setup()

from .. import models

DEVELOPMENT_DOMAIN = 'localhost'
PRODUCTION_DOMAIN = 'dendisoftware.com'
STAGING_DOMAIN = 'dendi-staging.com'

DOMAIN_DICT = {'production': PRODUCTION_DOMAIN,
               'development': DEVELOPMENT_DOMAIN,
               'staging': STAGING_DOMAIN}


def main(domain=DEVELOPMENT_DOMAIN):

    tenant = models.Client(domain_url='newstar.{}'.format(domain),
                           schema_name='newstar',
                           name="Newstar Medical",
                           paid_until='3000-01-01',
                           on_trial=False)
    tenant.save()


if __name__ == '__main__':
    if len(sys.argv) == 1:
        print("Please specify production, development, or staging.")
    elif len(sys.argv) == 2 and sys.argv[1].lower() in ['production', 'development', 'staging']:
        target_env = sys.argv[1].lower()
        logging.info("Initializing data for {}".format(target_env))
        main(domain=DOMAIN_DICT[target_env])
    else:
        print("Unknown arguments")
