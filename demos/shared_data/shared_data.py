
INSTRUMENTS = [
    {
        'name': 'QuantStudio 12K Flex Real-Time PCR System',
        'manufacturer': 'Applied Biosystems™',
        'method': 'qPCR'
    },
    {
        'name': 'LCMS-8040 Triple Quadrupole Liquid Chromatograph Mass Spectrometer',
        'manufacturer': 'Shimadzu',
        'method': 'LC-MS',
        'unit_of_measurement': 'ng/mL'
    },
    {
        'name': 'LCMS-8050 Triple Quadrupole Liquid Chromatograph Mass Spectrometer',
        'manufacturer': 'Shimadzu',
        'method': 'LC-MS',
        'unit_of_measurement': 'ng/mL'
    },
    {
        'name': 'AU400 Chemistry Analyzer',
        'manufacturer': 'Olympus',
        'method': 'General Chemistry',
        'unit_of_measurement': 'ng/mL'
    }
]

TEST_TARGETS = [
    {
        'name': 'Proteus mirabilis',
        'is_tox_target': False,
    },
    {
        'name': 'Enterococcus faecalis',
        'is_tox_target': False,
    },
    {
        'name': 'Pseudomonas aeruginosa',
        'is_tox_target': False,
    },
    {
        'name': 'Streptococcus agalactiae',
        'is_tox_target': False,
    },
    {
        'name': 'Morganella morganii',
        'is_tox_target': False,
    },
    {
        'name': 'Providencia stuartii',
        'is_tox_target': False,
    },
    {
        'name': 'Klebsiella oxytoca',
        'is_tox_target': False,
    },
    {
        'name': 'Citrobacter freundii',
        'is_tox_target': False,
    },
    {
        'name': 'Enterobacter cloacae',
        'is_tox_target': False,
    },
    {
        'name': 'Enterobacter aerogenes',
        'is_tox_target': False,
    },
    {
        'name': 'Enterococcus faecium',
        'is_tox_target': False,
    },
    {
        'name': 'Acinetobacter baumannii',
        'is_tox_target': False,
    },
    {
        'name': 'Staphylococcus saprophyticus',
        'is_tox_target': False,
    },
    {
        'name': 'Proteus vulgaris',
        'is_tox_target': False,
    },
    {
        'name': 'Candida albicans',
        'is_tox_target': False,
    },
    {
        'name': 'Xeno (spike-in control)',
        'is_tox_target': False,
    },
    {
        'name': 'Klebsiella pneumoniae',
        'is_tox_target': False,
    },
    {
        'name': 'Escherichia coli',
        'is_tox_target': False,
    },
]