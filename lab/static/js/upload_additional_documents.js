function asyncSuccessFunc(base64Result) {
    var length=base64Result.getLength();
    var base64string = base64Result.getData(0,length)

    var url = 'upload_additional_documents/';
    // upload the image data from here
    makeRequest(url, base64string, false);
}

function asyncFailureFunc(errorCode, errorString) {
    alert("ErrorCode: " + errorCode + "\r" + "ErrorString:" + errorString);
}

//AJAX
function makeRequest(url, parameter, flg) {
    if (window.XMLHttpRequest) {
        xhr = new XMLHttpRequest(); // Set up the request.
        var r = '';
    } else {
        if (window.ActiveXObject) {
            try {
                xhr = new ActiveXObject("MSXML2.XMLHTTP.3.0");
            } catch (ex) {
                alert(ex);
            }
        }
    }

    if (xhr) {
        if (flg) {
            xhr.open("GET", url, true); // Open the connection.
            xhr.onreadystatechange = requestresultCat;
            xhr.setRequestHeader("If-Modified-Since", "0");
            xhr.send(null);
        } else {
            xhr.open("POST", url, false);
            if (parameter != null) {
                xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
                var csrftoken = getCookie('csrftoken');
                var document_type = document.getElementById('selected_document').value;
                var data = {
                    'scanned_document': parameter,
                    'document_type': document_type
                };
                // xhr.setRequestHeader('X-CSRFToken', csrftoken);

                // send the scanned documents to the server
                $.ajax({
                    headers: {'X-CSRFToken': csrftoken},
                    type: "POST",
                    url: url,
                    data: data,
                    success: function(result) {
                      console.log("Succeeded!!");
                    }
                });
                console.log("Scanned documents sent!");

            } else {
                xhr.send(null);
                console.log('data not sent');
            }
        }
    } else {
        AppendMessage("&lt;b&gt;Sorry, but I couldn't create an XMLHttpRequest!&lt;/b&gt; ");
    }
}

function requestresult() {
    if (xhr.readyState == 4) {
        if (xhr.status == 200) { // File(s) uploaded.
            var outMsg;
            // if (xhr.responseXML & amp; & amp; xhr.responseXML.documentElement) {
            //     outMsg = xhr.responseXML;
            //     alert(xhr.responseText);
            // }
        }
    }
}

// using jQuery
function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}


function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}


function UnloadControl() {
  //Unload the control manually
  Dynamsoft.WebTwainEnv.Unload();
}


function LoadControl() {
  //Load the control manually
  Dynamsoft.WebTwainEnv.Load();
}



