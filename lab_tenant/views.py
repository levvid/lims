from braces.views import LoginRequiredMixin, UserPassesTestMixin
from dal import autocomplete
from django.contrib import messages
from django.db import transaction, IntegrityError
from django.db import transaction
from django.db.models.functions import Lower
from django.shortcuts import render, redirect, get_object_or_404
from django.views.generic import View

from . import models
from . import forms
from orders import models as orders_models

from main.functions import convert_utc_to_tenant_timezone


class InstrumentIntegrationListView(LoginRequiredMixin, View):
    def get(self, request):
        context = {}
        return render(request, 'instrument_integration_list.html', context=context)


class InstrumentIntegrationCreateView(LoginRequiredMixin, View):
    def get(self, request):
        form = forms.InstrumentIntegrationForm(instance_id=None)
        context = {'form': form}
        return render(request, 'instrument_integration_create.html', context=context)

    @transaction.atomic
    def post(self, request):
        form = forms.InstrumentIntegrationForm(request.POST,
                                               instance_id=None)
        context = {'form': form}
        if form.is_valid():
            internal_instrument_name = form.cleaned_data['internal_instrument_name']
            instrument = form.cleaned_data['instrument']
            serial_number = form.cleaned_data['serial_number']
            after_data_import = form.cleaned_data['after_data_import']

            models.InstrumentIntegration.objects.create(
                instrument=instrument,
                internal_instrument_name=internal_instrument_name,
                serial_number=serial_number,
                after_data_import=after_data_import,
            )
            return redirect('instrument_integration_list')
        else:
            return render(request, 'instrument_integration_create.html', context=context)


class InstrumentIntegrationDetailsView(LoginRequiredMixin, View):
    def get(self, request, instrument_integration_uuid):
        instrument_integration = get_object_or_404(models.InstrumentIntegration, uuid=instrument_integration_uuid)
        # test_codes refers to the internal test mapping inside AU instruments - TestTargets are mapped to test codes
        has_test_codes = False

        # only instruments using qPCR method has relative cutoffs based on instrument.
        # Every other instrument uses a lab-defined cutoff
        is_qpcr = False
        if instrument_integration.instrument.method == 'qPCR':
            is_qpcr = True

        ################################################################################################################
        # update this code
        # RS-232 instrument list --> ['Olympus AU400', 'Olympus AU2700']
        if instrument_integration.internal_instrument_name == 'Olympus AU400':
            # don't include calibrations/cutoffs section if AU400s
            has_test_codes = True

        # show the last 50 errors
        instrument_errors = orders_models.InstrumentError.objects.filter(instrument_integration=instrument_integration).order_by('-created_date')[:50]

        for error in instrument_errors:
            error.created_date = convert_utc_to_tenant_timezone(request, error.created_date)

        return render(request, 'instrument_integration_details.html',
                      {'instrument_integration': instrument_integration,
                       'instrument_errors': instrument_errors,
                       'is_qpcr': is_qpcr,
                       'has_test_codes': has_test_codes})


class InstrumentIntegrationDeleteView(LoginRequiredMixin, View):
    def get(self, request, instrument_integration_uuid):
        instrument_integration = get_object_or_404(models.InstrumentIntegration, uuid=instrument_integration_uuid)
        instrument_integration.is_active = False
        instrument_integration.save()
        return redirect('instrument_integration_list')


class InstrumentIntegrationUpdateView(LoginRequiredMixin, View):
    def get(self, request, instrument_integration_uuid):
        instrument_integration = get_object_or_404(models.InstrumentIntegration, uuid=instrument_integration_uuid)
        form = forms.InstrumentIntegrationForm(
            initial={'instrument': instrument_integration.instrument,
                     'internal_instrument_name': instrument_integration.internal_instrument_name,
                     'serial_number': instrument_integration.serial_number,
                     'after_data_import': instrument_integration.after_data_import},
            instance_id=instrument_integration.id
        )

        context = {'instrument_integration': instrument_integration,
                   'form': form}
        return render(request, 'instrument_integration_create.html', context)

    @transaction.atomic
    def post(self, request, instrument_integration_uuid):
        instrument_integration = get_object_or_404(models.InstrumentIntegration, uuid=instrument_integration_uuid)
        form = forms.InstrumentIntegrationForm(request.POST, instance_id=instrument_integration.id)
        context = {'form': form}

        if form.is_valid():
            internal_instrument_name = form.cleaned_data['internal_instrument_name']
            instrument = form.cleaned_data['instrument']
            serial_number = form.cleaned_data['serial_number']
            after_data_import = form.cleaned_data['after_data_import']

            instrument_integration.instrument = instrument
            instrument_integration.internal_instrument_name = internal_instrument_name
            instrument_integration.serial_number = serial_number
            instrument_integration.after_data_import = after_data_import
            instrument_integration.save()

            return redirect('instrument_integration_list')
        else:
            return render(request, 'instrument_integration_create.html', context=context)


class OffsiteIntegrationListView(LoginRequiredMixin, View):
    def get(self, request):
        context = {}
        return render(request, 'offsite_integration_list.html', context=context)


class LabDepartmentListView(LoginRequiredMixin, View):
    def get(self, request):
        form = forms.LabDepartmentForm(instance_id=None)
        context = {'form': form }
        return render(request, 'lab_department_list.html', context=context)

    @transaction.atomic
    def post(self, request):
        form = forms.LabDepartmentForm(request.POST, instance_id=None)
        context = {'form': form}

        if form.is_valid():
            name = form.cleaned_data['lab_department_name']

            existing_lab_departments = models.LabDepartment.objects.filter(name=name).all().exists()
            if existing_lab_departments:
                messages.error(request, 'A Lab Department {} already exists.'.format(name))
                return render(request, 'lab_department_list.html', context)
            else:
                new_lab_department = models.LabDepartment.objects.create(name=name)
                messages.success(request, 'Lab Department {} was successfully saved.'.format(name))
                return redirect('lab_department_list')

        else:
            messages.error(request, "Lab department was invalid or already in use. Please resubmit.")
            return render(request, 'lab_department_list.html', context=context)


class LabDepartmentDetailsView(LoginRequiredMixin, View):
    def get(self, request, lab_department_uuid):
        lab_department = get_object_or_404(models.LabDepartment,
                                           uuid=lab_department_uuid)
        test_panel_types = orders_models.TestPanelType.objects.filter(
            lab_departments=lab_department)

        context = {'lab_department': lab_department,
                   'test_panel_types': test_panel_types}
        return render(request, 'lab_department_details.html', context)


class LabDepartmentUpdatetView(LoginRequiredMixin, View):
    def get(self, request, lab_department_uuid):
        lab_department = get_object_or_404(models.LabDepartment, uuid=lab_department_uuid)
        form = forms.LabDepartmentForm(initial={'lab_department_name': lab_department.name}, instance_id=lab_department.id)

        return render(request, 'lab_department_create.html', {'lab_department': lab_department,
                                                              'update': True,
                                                               'form': form})

    @transaction.atomic
    def post(self, request, lab_department_uuid):
        lab_department = get_object_or_404(models.LabDepartment, uuid=lab_department_uuid)
        form = forms.LabDepartmentForm(request.POST, instance_id=lab_department.id)

        if form.is_valid():
            name = form.cleaned_data['lab_department_name']
            lab_department.name = name

            existing_lab_department = models.LabDepartment.objects.filter(
                name=name).exclude(uuid=lab_department_uuid).all()
            if existing_lab_department:
                messages.error(request, "{} already exists as a lab department.".format(name))
                return render(request, 'lab_department_create.html', {'lab_department': lab_department,
                                                                       'update': True,
                                                                       'form': form})
            lab_department.save()
            messages.success(request, "{} lab department was successfully updated.".
                             format(lab_department.name))
            return redirect('lab_department_details', lab_department_uuid)
        else:
            return render(request, 'lab_department_create.html', {'lab_department': lab_department,
                                                                  'update': True,
                                                                  'form': form})


class LabDepartmentDeleteView(LoginRequiredMixin, View):
    def get(self, request, lab_department_uuid):
        lab_department = get_object_or_404(models.LabDepartment, uuid=lab_department_uuid)
        lab_department.is_active = False
        lab_department.save()
        messages.success(request,
                         'Lab Department {} was successfully archived.'.format(lab_department.name))
        return redirect('lab_department_list')


class InstrumentTestCodeCreateView(LoginRequiredMixin, View):
    def get(self, request, instrument_integration_uuid):
        instrument_integration = get_object_or_404(models.InstrumentIntegration, uuid=instrument_integration_uuid)
        form = forms.InstrumentTestCodeForm()
        context = {'instrument_integration': instrument_integration,
                   'form': form}
        return render(request, 'instrument_test_code_create.html', context=context)

    @transaction.atomic
    def post(self, request, instrument_integration_uuid):
        instrument_integration = get_object_or_404(models.InstrumentIntegration, uuid=instrument_integration_uuid)
        form = forms.InstrumentTestCodeForm(request.POST)
        context = {'instrument_integration': instrument_integration,
                   'form': form}

        if form.is_valid():
            # weird edge case in form, where the field name can't be the same as the model name? don't question it
            test_type = form.cleaned_data['test_type_field']
            test_code = form.cleaned_data['test_code']

            orders_models.InstrumentTestCode.objects.create(
                instrument_integration=instrument_integration,
                test_type=test_type,
                test_code=test_code
            )
            messages.success(request, "Test code saved for {}.".format(test_type.name))
            return redirect('instrument_integration_details', instrument_integration_uuid=instrument_integration_uuid)
        else:
            return render(request, 'instrument_test_code_create.html', context=context)


class InstrumentTestCodeUpdateView(LoginRequiredMixin, View):
    def get(self, request, instrument_integration_uuid, instrument_test_code_uuid):
        instrument_integration = get_object_or_404(models.InstrumentIntegration, uuid=instrument_integration_uuid)
        instrument_test_code = get_object_or_404(orders_models.InstrumentTestCode,
                                                 uuid=instrument_test_code_uuid)
        form = forms.InstrumentTestCodeForm(
            initial={'test_type_field': instrument_test_code.test_type,
                     'test_code': instrument_test_code.test_code}
        )
        context = {'instrument_integration': instrument_integration,
                   'form': form}
        return render(request, 'instrument_test_code_create.html', context=context)

    @transaction.atomic
    def post(self, request, instrument_integration_uuid, instrument_test_code_uuid):
        instrument_integration = get_object_or_404(models.InstrumentIntegration, uuid=instrument_integration_uuid)
        instrument_test_code = get_object_or_404(orders_models.InstrumentTestCode,
                                                 uuid=instrument_test_code_uuid)
        form = forms.InstrumentTestCodeForm(request.POST, instance=instrument_test_code)
        context = {'instrument_integration': instrument_integration,
                   'form': form}

        if form.is_valid():
            test_type = form.cleaned_data['test_type_field']
            test_code = form.cleaned_data['test_code']

            instrument_test_code.test_type = test_type
            instrument_test_code.test_code = test_code
            instrument_test_code.save()

            messages.success(request, "Test code updated for {}.".format(test_type.name))
            return redirect('instrument_integration_details', instrument_integration_uuid=instrument_integration_uuid)
        else:
            return render(request, 'instrument_test_code_create.html', context=context)


class InstrumentTestCodeDeleteView(LoginRequiredMixin, View):
    def get(self, request, instrument_integration_uuid, instrument_test_code_uuid):
        instrument_test_code = get_object_or_404(orders_models.InstrumentTestCode,
                                                 uuid=instrument_test_code_uuid)
        test_type = instrument_test_code.test_type.name
        instrument_test_code.is_active = False
        instrument_test_code.save()
        messages.success(request, "Test code archived for {}.".format(test_type))
        return redirect('instrument_integration_details', instrument_integration_uuid=instrument_integration_uuid)


class InstrumentCalibrationCreateView(LoginRequiredMixin, View):
    def get(self, request, instrument_integration_uuid):
        instrument_integration = get_object_or_404(models.InstrumentIntegration, uuid=instrument_integration_uuid)
        form = forms.InstrumentCalibrationForm(initial={'instrument_integration': instrument_integration},
                                               calibration=None)
        unit_of_measurement = instrument_integration.instrument.unit_of_measurement
        context = {'form': form,
                   'instrument_integration': instrument_integration,
                   'unit_of_measurement': unit_of_measurement}
        return render(request, 'instrument_calibration_create.html', context=context)

    @transaction.atomic
    def post(self, request, instrument_integration_uuid):
        instrument_integration = get_object_or_404(models.InstrumentIntegration, uuid=instrument_integration_uuid)
        form = forms.InstrumentCalibrationForm(request.POST,
                                               calibration=None)
        unit_of_measurement = instrument_integration.instrument.unit_of_measurement
        context = {'form': form,
                   'instrument_integration': instrument_integration,
                   'unit_of_measurement': unit_of_measurement}

        if form.is_valid():
            instrument_integration = form.cleaned_data['instrument_integration']
            test_type = form.cleaned_data['test_type']
            target_name = form.cleaned_data['target_name']
            cutoff_value = form.cleaned_data['cutoff_value']
            use_test_cutoff_values = form.cleaned_data['use_test_cutoff_values']

            orders_models.InstrumentCalibration.objects.create(
                instrument_integration=instrument_integration,
                test_type=test_type,
                # target name is a CharField and is just used for a given instrument,
                # this is not linked to the TestTarget object
                target_name=target_name,
                cutoff_value=cutoff_value,
                use_test_cutoff_values=use_test_cutoff_values
            )
            messages.success(request, "{} calibration saved.".format(test_type.name))
            return redirect('instrument_integration_details', instrument_integration_uuid=instrument_integration_uuid)
        else:
            return render(request, 'instrument_calibration_create.html', context=context)


class InstrumentCalibrationDetailsView(LoginRequiredMixin, View):
    def get(self, request, instrument_integration_uuid, instrument_calibration_uuid):
        instrument_calibration = get_object_or_404(orders_models.InstrumentCalibration,
                                                   uuid=instrument_calibration_uuid)
        instrument_integration = get_object_or_404(models.InstrumentIntegration,
                                                   uuid=instrument_integration_uuid)
        context = {'instrument_calibration': instrument_calibration,
                   'instrument_integration': instrument_integration}
        return render(request, 'instrument_calibration_details.html', context=context)


class InstrumentCalibrationUpdateView(LoginRequiredMixin, View):
    """View for updating instrument calibration objects.
    """

    def get(self, request, instrument_integration_uuid, instrument_calibration_uuid):
        instrument_calibration = get_object_or_404(orders_models.InstrumentCalibration,
                                                   uuid=instrument_calibration_uuid)
        instrument_integration = get_object_or_404(models.InstrumentIntegration,
                                                   uuid=instrument_integration_uuid)
        unit_of_measurement = instrument_integration.instrument.unit_of_measurement
        form = forms.InstrumentCalibrationForm(initial={
            'instrument_integration': instrument_integration,
            'test_type': instrument_calibration.test_type,
            'target_name': instrument_calibration.target_name,
            'cutoff_value': instrument_calibration.cutoff_value,
            'use_test_cutoff_values': instrument_calibration.use_test_cutoff_values,
        }, calibration=instrument_calibration)
        context = {'form': form,
                   'instrument_integration': instrument_integration,
                   'unit_of_measurement': unit_of_measurement}
        return render(request, 'instrument_calibration_create.html', context=context)

    @transaction.atomic
    def post(self, request, instrument_integration_uuid, instrument_calibration_uuid):
        instrument_calibration = get_object_or_404(orders_models.InstrumentCalibration,
                                                   uuid=instrument_calibration_uuid)
        instrument_integration = get_object_or_404(models.InstrumentIntegration, uuid=instrument_integration_uuid)
        form = forms.InstrumentCalibrationForm(request.POST, calibration=instrument_calibration)
        unit_of_measurement = instrument_integration.instrument.unit_of_measurement
        context = {'form': form,
                   'instrument_integration': instrument_integration,
                   'unit_of_measurement': unit_of_measurement}

        if form.is_valid():
            instrument_calibration.instrument_integration = form.cleaned_data['instrument_integration']
            instrument_calibration.test_type = form.cleaned_data['test_type']
            instrument_calibration.target_name = form.cleaned_data['target_name']
            instrument_calibration.cutoff_value = form.cleaned_data['cutoff_value']
            instrument_calibration.use_test_cutoff_values = form.cleaned_data['use_test_cutoff_values']
            instrument_calibration.save()
            messages.success(request,
                             "{} - {} calibration saved.".format(instrument_integration.internal_instrument_name,
                                                                 instrument_calibration.test_type.name))
            return redirect('instrument_integration_details', instrument_integration_uuid)
        else:
            return render(request, 'instrument_calibration_create.html', context=context)


class InstrumentCalibrationDeleteView(LoginRequiredMixin, View):
    def get(self, request, instrument_integration_uuid, instrument_calibration_uuid):
        instrument_calibration = get_object_or_404(orders_models.InstrumentCalibration,
                                                   uuid=instrument_calibration_uuid)
        instrument_integration = get_object_or_404(models.InstrumentIntegration, uuid=instrument_integration_uuid)
        instrument_calibration.is_active = False
        instrument_calibration.save()
        messages.success(request,
                         "{} - {} calibration archived.".format(instrument_integration.internal_instrument_name,
                                                                instrument_calibration.test_type.name))
        return redirect('instrument_integration_details', instrument_integration_uuid)


class OffsiteIntegrationCreateView(LoginRequiredMixin, View):
    def get(self, request):
        form = forms.OffsiteIntegrationForm()
        context = {'form': form}
        return render(request, 'offsite_integration_create.html', context=context)

    @transaction.atomic
    def post(self, request):
        form = forms.OffsiteIntegrationForm(request.POST)
        context = {'form': form}

        if form.is_valid():
            offsite_laboratory = form.cleaned_data['offsite_laboratory']
            sending_facility_id = form.cleaned_data['sending_facility_id']

            existing_integration = models.OffsiteLaboratoryIntegration.objects.filter(offsite_laboratory=offsite_laboratory).all()
            if existing_integration:
                messages.error(request, "There is already an existing integration for this offsite laboratory.")
                return render(request, 'offsite_integration_create.html', context)
            else:
                models.OffsiteLaboratoryIntegration.objects.create(
                    offsite_laboratory=offsite_laboratory,
                    sending_facility_id=sending_facility_id
                )
                messages.success(request, "{} integration was successfully saved.".format(offsite_laboratory.name))
                return redirect('offsite_integration_list')


class OffsiteIntegrationDetailsView(LoginRequiredMixin, View):
    def get(self, request, offsite_integration_uuid):
        offsite_integration = get_object_or_404(models.OffsiteLaboratoryIntegration, uuid=offsite_integration_uuid)
        return render(request, 'offsite_integration_details.html', {'offsite_integration': offsite_integration})


class OffsiteIntegrationDeleteView(LoginRequiredMixin, View):
    def get(self, request, offsite_integration_uuid):
        offsite_integration = get_object_or_404(models.OffsiteLaboratoryIntegration, uuid=offsite_integration_uuid)
        offsite_integration.is_active = False
        offsite_integration.save()
        return redirect('offsite_integration_list')


class OffsiteIntegrationUpdateView(LoginRequiredMixin, View):
    def get(self, request, offsite_integration_uuid):
        offsite_integration = get_object_or_404(models.OffsiteLaboratoryIntegration, uuid=offsite_integration_uuid)
        form = forms.OffsiteIntegrationForm(initial={'offsite_laboratory': offsite_integration.offsite_laboratory,
                                                     'sending_facility_id': offsite_integration.sending_facility_id})
        return render(request, 'offsite_integration_create.html', {'offsite_integration': offsite_integration,
                                                                   'form': form})

    def post(self, request, offsite_integration_uuid):
        offsite_integration = get_object_or_404(models.OffsiteLaboratoryIntegration, uuid=offsite_integration_uuid)
        form = forms.OffsiteIntegrationForm(request.POST)

        if form.is_valid():
            offsite_integration.offsite_laboratory = form.cleaned_data.get('offsite_laboratory')
            offsite_integration.sending_facility_id = form.cleaned_data.get('sending_facility_id')
            offsite_integration.save()
            messages.success(request, "{} integration was successfully saved!".
                             format(offsite_integration.offsite_laboratory.name))
            return redirect('offsite_integration_list')
        else:
            return render(request, 'offsite_integration_create.html', {'offsite_integration': offsite_integration,
                                                                       'form': form})


class InHouseLabLocationAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs = models.InHouseLabLocation.objects.all()
        if self.q:
            qs = qs.filter(name__icontains=self.q)
        return qs


class InHouseLabLocationCreateView(LoginRequiredMixin, View):
    def get(self, request):
        form = forms.InHouseLabLocationForm(instance_id=None)
        context = {'form': form}
        return render(request, 'in_house_lab_location_create.html', context=context)

    @transaction.atomic
    def post(self, request):
        form = forms.InHouseLabLocationForm(request.POST, instance_id=None)
        context = {'form': form}
        if form.is_valid():
            name = form.cleaned_data['name']
            address1 = form.cleaned_data['address1']
            address2 = form.cleaned_data['address2']
            zip_code = form.cleaned_data['zip_code']
            lab_director = form.cleaned_data['lab_director']
            alias = form.cleaned_data['alias']
            phone_number = form.cleaned_data['phone_number']
            fax_number = form.cleaned_data['fax_number']

            existing_in_house_lab_locations = models.InHouseLabLocation.objects.filter(name=name).all().exists()
            if existing_in_house_lab_locations:
                messages.error(request, 'An In-House Lab Location {} already exists.'.format(name))
                return render(request, 'in_house_lab_location_create.html', context)
            else:
                new_in_house_lab_location = models.InHouseLabLocation.objects.create(
                    name=name,
                    address1=address1,
                    address2=address2,
                    zip_code=zip_code,
                    lab_director=lab_director,
                    alias=alias,
                    phone_number=phone_number,
                    fax_number=fax_number
                )
                messages.success(request, 'In-House Lab Location {} was successfully saved.'.format(name))
                return redirect('in_house_lab_location_details', new_in_house_lab_location.uuid)
        else:
            return render(request, 'in_house_lab_location_create.html', context)


class InHouseLabLocationDetailsView(LoginRequiredMixin, View):
    def get(self, request, in_house_lab_location_uuid):
        in_house_lab_location = get_object_or_404(models.InHouseLabLocation, uuid=in_house_lab_location_uuid)
        test_panel_types = orders_models.TestPanelType.objects.filter(
            in_house_lab_locations=in_house_lab_location)

        context = {'in_house_lab_location': in_house_lab_location,
                   'test_panel_types': test_panel_types}
        return render(request, 'in_house_lab_location_details.html', context)


class InHouseLabLocationDeleteView(LoginRequiredMixin, View):
    def get(self, request, in_house_lab_location_uuid):
        in_house_lab_location = get_object_or_404(models.InHouseLabLocation, uuid=in_house_lab_location_uuid)
        in_house_lab_location.is_active = False
        in_house_lab_location.save()
        messages.success(request,
                         'In-House Lab Location {} was successfully archived.'.format(in_house_lab_location.name))
        return redirect('in_house_lab_location_list')


class InHouseLabLocationUpdateView(LoginRequiredMixin, View):
    def get(self, request, in_house_lab_location_uuid):
        in_house_lab_location = get_object_or_404(models.InHouseLabLocation, uuid=in_house_lab_location_uuid)
        form = forms.InHouseLabLocationForm(initial={'name': in_house_lab_location.name,
                                                     'address1': in_house_lab_location.address1,
                                                     'address2': in_house_lab_location.address2,
                                                     'zip_code': in_house_lab_location.zip_code,
                                                     'lab_director': in_house_lab_location.lab_director,
                                                     'alias': in_house_lab_location.alias,
                                                     'phone_number': in_house_lab_location.phone_number,
                                                     'fax_number': in_house_lab_location.fax_number},
                                            instance_id=in_house_lab_location.id)
        return render(request, 'in_house_lab_location_create.html', {'in_house_lab_location': in_house_lab_location,
                                                                     'update': True,
                                                                     'form': form})

    def post(self, request, in_house_lab_location_uuid):
        in_house_lab_location = get_object_or_404(models.InHouseLabLocation, uuid=in_house_lab_location_uuid)
        form = forms.InHouseLabLocationForm(request.POST, instance_id=in_house_lab_location.id)
        context = {'form': form}
        if form.is_valid():
            name = form.cleaned_data['name']
            in_house_lab_location.name = name
            in_house_lab_location.address1 = form.cleaned_data['address1']
            in_house_lab_location.address2 = form.cleaned_data['address2']
            in_house_lab_location.zip_code = form.cleaned_data['zip_code']
            in_house_lab_location.lab_director = form.cleaned_data['lab_director']
            in_house_lab_location.alias = form.cleaned_data['alias']
            in_house_lab_location.phone_number = form.cleaned_data['phone_number']
            in_house_lab_location.fax_number = form.cleaned_data['fax_number']

            existing_in_house_lab_locations = models.InHouseLabLocation.objects.filter(
                name=name).exclude(uuid=in_house_lab_location_uuid).all()
            if existing_in_house_lab_locations:
                messages.error(request, "{} already exists as an in-house lab location".format(name))
                return render(request, 'in_house_lab_location_create.html', context)

            in_house_lab_location.save()
            messages.success(request, "{} in-house lab location was successfully updated.".
                             format(in_house_lab_location.name))
            return redirect('in_house_lab_location_list')
        else:
            return render(request, 'in_house_lab_location_create.html',
                          {'in_house_lab_location.': in_house_lab_location,
                           'update': True,
                           'form': form})


class InHouseLabLocationListView(LoginRequiredMixin, View):
    def get(self, request):
        context = {}
        return render(request, 'in_house_lab_location_list.html', context=context)


######################################################################################################################
# User mixins for the rest of the LIS
######################################################################################################################


class LISLoginRequiredMixin(LoginRequiredMixin):
    login_url = 'login'


class LabAdminMixin(LoginRequiredMixin, UserPassesTestMixin):
    """
    Check that self.request.user is a Lab Admin.
    These pages should only be accessible by Lab Employee or Admins.
    """

    login_url = 'login'

    def test_func(self, user):
        return user.user_type == 1


class LabMemberMixin(LoginRequiredMixin, UserPassesTestMixin):
    """
    Check that self.request.user is a Lab Employee or Admin.
    These pages should only be accessible by Lab Employee or Admins.
    """

    login_url = 'login'

    def test_func(self, user):
        return user.user_type < 9


class ProviderMixin(LoginRequiredMixin, UserPassesTestMixin):
    """
    Check that self.request.user is a Provider.
    These pages should only be accessible by Providers.
    """

    login_url = 'login'

    def test_func(self, user):
        return user.user_type == 10


class LabMemberAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs = models.User.objects.filter(user_type__lt=8).all()

        if self.q:
            qs = qs.filter(first_name__icontains=self.q) | qs.filter(last_name__icontains=self.q)
        qs = qs.distinct()
        return qs


class LabDepartmentAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs = models.LabDepartment.objects.all()

        if self.q:
            qs = qs.filter(name__istartswith=self.q)
        qs = qs.distinct()
        return qs


class CollectorAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs = models.User.objects.filter(user_type=11).all()
        try:
            account_id = self.request.session['step_1_cleaned_data']['account']
        except KeyError:
            account_id = None

        if account_id:
            qs = qs.filter(collector__accounts__id=account_id)
        else:
            qs = qs.none()

        if self.q:
            qs = qs.filter(first_name__icontains=self.q) | qs.filter(last_name__icontains=self.q)
        qs = qs.distinct()
        return qs


class InstrumentIntegrationAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        control = self.forwarded.get('control', None)
        if control:
            try:
                qs = control.instrument_integrations.all()
            except:
                control = orders_models.Control.objects.get(id=int(control))
                qs = control.instrument_integrations.all()
        else:
            qs = models.InstrumentIntegration.objects.all()

        if self.q:
            qs = qs.filter(internal_instrument_name__icontains=self.q)
        qs = qs.distinct()
        return qs
