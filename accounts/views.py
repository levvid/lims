import uuid

from dal import autocomplete
from django.db import transaction, IntegrityError
from django.contrib import messages
from django.db.models import Q
from django.shortcuts import render, redirect, get_object_or_404
from django.views.generic import View

from accounts.generate_account_requisition_form import generate_account_requisition_pdf
from . import forms
from . import models
from .scripts.refresh_provider_username_password import main as fix_password
from braces.views import LoginRequiredMixin
from lab_tenant.views import LabMemberMixin, LISLoginRequiredMixin
from lab_tenant.models import User
from patients import models as patients_models

from main.functions import convert_utc_to_tenant_timezone


# Create your views here.
class AccountListView(LabMemberMixin, View):
    def get(self, request):
        return render(request, 'account_list.html', {})


# Create your views here.
class AccountArchivedListView(LabMemberMixin, View):
    def get(self, request):
        return render(request, 'account_archived_list.html', {})


class AccountDetailsView(LISLoginRequiredMixin, View):

    def get(self, request, account_uuid):
        account = get_object_or_404(models.Account, uuid=account_uuid)
        provider_list = account.providers.all()

        if request.tenant.lab_type == 'POL':
            object_name = ('Location', 'location')
        else:
            object_name = ('Account', 'account')

        return render(request, 'account_details.html', {'account': account,
                                                        'provider_list': provider_list,
                                                        'object_name': object_name})


class AccountUpdateView(LabMemberMixin, View):

    def get(self, request, account_uuid):
        account = get_object_or_404(models.Account, uuid=account_uuid)
        object_name = ('Account', 'account')

        if account.user:
            account_username = account.user.username
        else:
            account_username = ''

        account_aliases = account.account_aliases.all()
        account_aliases = [str(alias) for alias in account_aliases]
        alias_str = '\r\n'.join(account_aliases)

        form = forms.AccountForm(initial={'account_name': account.name,
                                          'aliases': alias_str,
                                          'account_address1': account.address1,
                                          'account_address2': account.address2,
                                          'account_zip_code': account.zip_code,
                                          'contact_person': account.contact_person,
                                          'account_phone': account.phone_number,
                                          'account_fax': account.fax_number,
                                          'account_email': account.email,
                                          'providers': account.providers.all(),
                                          'alternate_id': account.alternate_id,
                                          'account_username': account_username,
                                          'report_preference': account.report_preference,
                                          'account_manages_signatures': account.account_manages_signatures,
                                          'notes': account.notes,
                                          'results_out_show_pdf_link': account.results_out_show_pdf_link,
                                          'emr_account_number': account.emr_account_number
                                          }
        )
        return render(request, 'account_create.html', {'account': account,
                                                       'form': form,
                                                       'object_name': object_name,
                                                       'update': True})

    def post(self, request, account_uuid):
        account = get_object_or_404(models.Account, uuid=account_uuid)
        object_name = ('Account', 'account')
        form = forms.AccountForm(request.POST, instance=account)

        if form.is_valid():
            account.name = form.cleaned_data['account_name']
            aliases = form.cleaned_data.get('aliases')
            account.address1 = form.cleaned_data['account_address1']
            account.address2 = form.cleaned_data['account_address2']
            account.zip_code = form.cleaned_data['account_zip_code']
            account.contact_person = form.cleaned_data['contact_person']
            account.phone_number = form.cleaned_data['account_phone']
            account.fax_number = form.cleaned_data['account_fax']
            account.email = form.cleaned_data['account_email']
            account.alternate_id = form.cleaned_data['alternate_id']
            account_password = form.cleaned_data['account_password']
            account.report_preference = form.cleaned_data['report_preference']
            account.account_manages_signatures = form.cleaned_data['account_manages_signatures']
            account.notes = form.cleaned_data['notes']
            account.results_out_show_pdf_link = form.cleaned_data['results_out_show_pdf_link']
            account.emr_account_number = form.cleaned_data['emr_account_number']

            if account.user:
                account.user.username = form.cleaned_data['account_username']
                account.user.email = form.cleaned_data['account_email']
                if account_password:
                    account.user.set_password(account_password)
                account.user.save()
            else:
                if account.email:
                    account_email = account.email
                else:
                    lab_schema = request.tenant.schema_name
                    account_email = str(account_uuid) + '@' + lab_schema + '.com'
                if form.cleaned_data['account_username']:
                    username = form.cleaned_data['account_username']
                else:
                    username = account_email

                if account_password:
                    password = account_password
                else:
                    password = str(account_uuid)

                user_obj = User(user_type=9,
                                username=username,
                                email=account_email)
                user_obj.set_password(password)
                user_obj.save()
                account.user = user_obj

            # save the object after update
            account.save()

            # make sure alias list gets updated every time
            account_aliases = models.AccountAlias.objects.filter(account=account)
            for account_alias in account_aliases:
                account_alias.is_active = False
                account_alias.save()
            for alias in aliases:
                alias = alias.strip()
                if alias:
                    alias = models.AccountAlias(name=alias, account=account)
                    alias.save()

            generate_account_requisition_pdf(request.tenant.id, account_uuid)

            ############################################################################################
            # You cannot assign directly to a many-to-many field (providers)
            # Do it after you create the item first!
            # assign many-to-many field

            for provider in form.cleaned_data['providers']:
                account.providers.add(provider)

            return redirect('account_details', account_uuid=account_uuid)
        return render(request, 'account_create.html', {'account': account,
                                                       'form': form,
                                                       'object_name': object_name,
                                                       'update': True})


class AccountDeleteView(LabMemberMixin, View):

    def get(self, request, account_uuid):
        account = get_object_or_404(models.Account, uuid=account_uuid)
        account_user = account.user
        account.is_active = False
        account.save()
        # deactivate user if account is archived (username/password must be reset)
        account_user.is_active = False
        account_user.save()

        return redirect('account_list')


class AccountUndoDeleteView(LabMemberMixin, View):
    def get(self, request, account_uuid):
        account = models.Account.all_objects.get(uuid=account_uuid)
        account_user = account.user

        account.is_active = True
        account.save()

        # re-activate user if account is archived (username/password must be reset)
        account_user.is_active = True
        account_user.save()

        messages.success(request, 'Account: {} unarchived. '
                                  'Please reset the account\'s username and password.'.format(account.name))
        return redirect('account_archived_list')


class AccountCreateView(LabMemberMixin, View):

    def get(self, request):
        form = forms.AccountForm()
        if request.tenant.lab_type == 'POL':
            object_name = ('Location', 'location')
        else:
            object_name = ('Account', 'account')
        return render(request, 'account_create.html', {'form': form,
                                                       'object_name': object_name})

    def post(self, request):
        form = forms.AccountForm(request.POST)

        if request.tenant.lab_type == 'POL':
            object_name = ('Location', 'location')
        else:
            object_name = ('Account', 'account')

        if form.is_valid():
            providers = form.cleaned_data['providers']
            aliases = form.cleaned_data.get('aliases')
            name = form.cleaned_data['account_name']
            address1 = form.cleaned_data['account_address1']
            address2 = form.cleaned_data['account_address2']
            zip_code = form.cleaned_data['account_zip_code']
            contact_person = form.cleaned_data['contact_person']
            phone_number = form.cleaned_data['account_phone']
            fax_number = form.cleaned_data['account_fax']
            email = form.cleaned_data['account_email']
            alternate_id = form.cleaned_data['alternate_id']
            account_username = form.cleaned_data['account_username']
            account_password = form.cleaned_data['account_password']
            account_manages_signatures = form.cleaned_data['account_manages_signatures']
            notes = form.cleaned_data['notes']
            results_out_show_pdf_link = form.cleaned_data['results_out_show_pdf_link']
            emr_account_number = form.cleaned_data['emr_account_number']


            account_uuid = uuid.uuid4()
            if email:
                account_email = email
            else:
                lab_schema = request.tenant.schema_name
                account_email = str(account_uuid) + '@' + lab_schema + '.com'

            if account_username:
                username = account_username
            else:
                username = account_email

            if account_password:
                password = account_password
            else:
                password = str(account_uuid)

            user_obj = User(user_type=9,
                            username=username,
                            email=account_email)
            user_obj.set_password(password)
            user_obj.save()

            account_obj = models.Account(name=name,
                                         address1=address1,
                                         address2=address2,
                                         zip_code=zip_code,
                                         contact_person=contact_person,
                                         phone_number=phone_number,
                                         fax_number=fax_number,
                                         email=email,
                                         user=user_obj,
                                         alternate_id=alternate_id,
                                         uuid=account_uuid,
                                         report_preference=form.cleaned_data['report_preference'],
                                         account_manages_signatures=account_manages_signatures,
                                         notes=notes,
                                         results_out_show_pdf_link=results_out_show_pdf_link,
                                         emr_account_number=emr_account_number)
            account_obj.save()

            alias_list = []
            for alias in aliases:
                alias = alias.strip()
                if alias:
                    alias_list.append(models.AccountAlias(name=alias, account=account_obj))
            models.AccountAlias.objects.bulk_create(alias_list)

            generate_account_requisition_pdf(request.tenant.id, account_uuid)
            ############################################################################################
            # You cannot assign directly to a many-to-many field (providers)
            # Do it after you create the item first!
            # assign many-to-many field

            for provider in providers:
                account_obj.providers.add(provider)

            return redirect('account_details', account_uuid=account_uuid)
        return render(request, 'account_create.html', {'form': form,
                                                       'object_name': object_name})


class ProviderListView(LISLoginRequiredMixin, View):
    def get(self, request):
        return render(request, 'provider_list.html', {})


class ProviderArchivedListView(LabMemberMixin, View):
    def get(self, request):
        return render(request, 'provider_archived_list.html')


class ProviderDetailsView(LISLoginRequiredMixin, View):

    def get(self, request, provider_uuid):
        provider = get_object_or_404(models.Provider, uuid=provider_uuid)

        if request.tenant.lab_type == 'POL':
            tenant_type = 'Location'
        else:
            tenant_type = 'Account'

        patient_list = patients_models.Patient.objects.filter(providers=provider).\
            values('user__first_name', 'user__last_name', 'uuid', 'sex', 'birth_date')

        return render(request, 'provider_details.html', {'provider': provider,
                                                         'patient_list': patient_list,
                                                         'tenant_type': tenant_type})


class ProviderUpdateView(LISLoginRequiredMixin, View):

    def get(self, request, provider_uuid):
        provider = get_object_or_404(models.Provider, uuid=provider_uuid)

        if request.tenant.lab_type == 'POL':
            tenant_type = 'Location'
        else:
            tenant_type = 'Account'

        if provider.npi:
            provider_country = 'US'
        else:
            provider_country = 'Other'

        if provider.user:
            provider_username = provider.user.username
        else:
            provider_username = ''

        form = forms.ProviderForm(initial={'accounts': provider.accounts.all(),
                                           'country': provider_country,
                                           'first_name': provider.user.first_name,
                                           'last_name': provider.user.last_name,
                                           'provider_id_num': provider.id_num,
                                           'npi': provider.npi,
                                           'provider_suffix': provider.suffix,
                                           'provider_title': provider.title,
                                           'email': provider.user.email,
                                           'phone_number': provider.phone_number,
                                           'provider_username': provider_username})
        return render(request, 'provider_create.html', {'form': form,
                                                        'provider': provider,
                                                        'update': True,
                                                        'tenant_type': tenant_type})

    def post(self, request, provider_uuid):
        provider = get_object_or_404(models.Provider, uuid=provider_uuid)
        form = forms.ProviderForm(request.POST, instance=provider)

        if request.tenant.lab_type == 'POL':
            tenant_type = 'Location'
        else:
            tenant_type = 'Account'

        if form.is_valid():
            provider.npi = form.cleaned_data['npi']
            provider.id_num = form.cleaned_data['provider_id_num']
            provider.suffix = form.cleaned_data['provider_suffix']
            provider.title = form.cleaned_data['provider_title']
            provider.phone_number = form.cleaned_data['phone_number']
            provider.accounts.set(form.cleaned_data['accounts'])
            provider_password = form.cleaned_data['provider_password']

            if provider.user:
                # attributes tied to Django's User object
                # save foreign key-related objects separately
                provider.user.first_name = form.cleaned_data['first_name']
                provider.user.last_name = form.cleaned_data['last_name']
                provider.user.email = form.cleaned_data['email']
                provider.user.username = form.cleaned_data['provider_username']

                if provider_password:
                    provider.user.set_password(provider_password)
                provider.user.save()
            else:
                lab_schema = request.tenant.schema_name
                provider_email = form.cleaned_data['email']

                if form.cleaned_data['provider_username']:
                    username = form.cleaned_data['provider_username']
                else:
                    username = str(provider_uuid) + '@' + lab_schema + '.com'

                if provider_password:
                    password = provider_password
                else:
                    password = str(provider_uuid)

                user_obj = User(user_type=10,
                                username=username,
                                email=provider_email)
                user_obj.set_password(password)
                user_obj.save()
                provider.user = user_obj
                provider.user.save()
            provider.save()

            return redirect('provider_details', provider_uuid=provider.uuid)
        return render(request, 'provider_create.html', {'form': form,
                                                        'provider': provider,
                                                        'update': True,
                                                        'tenant_type': tenant_type})


class ProviderDeleteView(LabMemberMixin, View):

    def get(self, request, provider_uuid):
        provider = get_object_or_404(models.Provider, uuid=provider_uuid)
        provider.is_active = False
        provider.save()
        # deactivate the user as well
        provider.user.is_active = False
        provider.user.save()
        return redirect('provider_list')


class ProviderUndoDeleteView(LabMemberMixin, View):
    def get(self, request, provider_uuid):
        provider = models.Provider.all_objects.get(uuid=provider_uuid)
        provider_user = provider.user

        provider.is_active = True
        provider.save()

        # re-activate user if archived (username/password must be reset)
        provider_user.is_active = True
        provider_user.save()

        messages.success(request, 'Provider: {} {} unarchived. '
                                  'Please reset the provider\'s username and password.'.format(provider.user.first_name,
                                                                                               provider.user.last_name))
        return redirect('provider_archived_list')


class ProviderCreateView(LISLoginRequiredMixin, View):

    def get(self, request):
        form = forms.ProviderForm()

        if request.tenant.lab_type == 'POL':
            tenant_type = 'Location'
        else:
            tenant_type = 'Account'

        return render(request, 'provider_create.html', {'form': form,
                                                        'tenant_type': tenant_type})

    @transaction.atomic
    def post(self, request):
        form = forms.ProviderForm(request.POST)

        if request.tenant.lab_type == 'POL':
            tenant_type = 'Location'
        else:
            tenant_type = 'Account'

        if form.is_valid():
            accounts = form.cleaned_data['accounts']
            first_name = form.cleaned_data['first_name']
            last_name = form.cleaned_data['last_name']
            npi = form.cleaned_data['npi']
            id_num = form.cleaned_data['provider_id_num']
            suffix = form.cleaned_data['provider_suffix']
            title = form.cleaned_data['provider_title']
            email = form.cleaned_data['email']
            phone_number = form.cleaned_data['phone_number']
            provider_username = form.cleaned_data['provider_username']
            provider_password = form.cleaned_data['provider_password']

            # create User object before creating Provider, since provider has a username
            provider_uuid = uuid.uuid4()

            if provider_username:
                username = provider_username
            else:
                lab_schema = request.tenant.schema_name
                username = str(provider_uuid) + '@' + lab_schema + '.com'

            if provider_password:
                password = provider_password
            else:
                password = str(provider_uuid)

            # create User object
            user_obj = User(first_name=first_name,
                            last_name=last_name,
                            username=username,
                            user_type=10,
                            email=email)
            # setting password is different from obj attribute
            user_obj.set_password(password)
            # create User object first before creating Provider
            user_obj.save()

            provider_obj = models.Provider(user=user_obj,
                                           npi=npi,
                                           id_num=id_num,
                                           suffix=suffix,
                                           title=title,
                                           phone_number=phone_number,
                                           uuid=provider_uuid)
            # create provider
            provider_obj.save()
            provider_obj.accounts.set(accounts)

            return redirect('provider_list')

        return render(request, 'provider_create.html', {'form': form,
                                                        'tenant_type': tenant_type})


class CollectorArchivedListView(LabMemberMixin, View):
    def get(self, request):
        return render(request, 'collector_archived_list.html', {})


class CollectorUndoDeleteView(LabMemberMixin, View):
    def get(self, request, collector_uuid):
        collector = models.Collector.all_objects.get(uuid=collector_uuid)
        collector_user = collector.user

        collector.is_active = True
        collector.save(update_fields=['is_active'])

        if collector_user:
            collector_user.is_active = True
            collector_user.save(update_fields=['is_active'])

        messages.success(
            request,
            'Collector: {} {} unarchived. Please reset the collector\'s username and password.'.format(
                collector.user.first_name,
                collector.user.last_name
            )
        )
        return redirect('collector_archived_list')


class CollectorUpdateView(LabMemberMixin, View):
    def get(self, request, collector_uuid):
        collector = get_object_or_404(models.Collector, uuid=collector_uuid)
        if collector.user:
            username = collector.user.username
        else:
            username = ''

        form = forms.CollectorForm(initial={
            'accounts': collector.accounts.all(),
            'first_name': collector.user.first_name,
            'last_name': collector.user.last_name,
            'email': collector.user.email,
            'username': username
        })
        context = {
            'form': form,
            'collector': collector,
            'update': True
        }
        return render(request, 'collector_create.html', context)

    def post(self, request, collector_uuid):
        collector = get_object_or_404(models.Collector, uuid=collector_uuid)
        form = forms.CollectorForm(request.POST, instance=collector)
        context = {
            'form': form,
            'collector': collector,
            'update': True
        }

        if form.is_valid():
            collector.accounts.set(form.cleaned_data['accounts'])
            password = form.cleaned_data['password']

            if collector.user:
                collector.user.first_name = form.cleaned_data['first_name']
                collector.user.last_name = form.cleaned_data['last_name']
                collector.user.email = form.cleaned_data['email']
                collector.user.username = form.cleaned_data['username']

                if password:
                    collector.user.set_password(password)
                collector.user.save()
            else:
                lab_schema = request.tenant.schema_name
                collector_email = form.cleaned_data['email']

                if form.cleaned_data['username']:
                    username = form.cleaned_data['username']
                else:
                    username = str(collector_uuid) + '@' + lab_schema + '.com'

                if not password:
                    password = str(collector_uuid)

                user_obj = User(user_type=11,
                                username=username,
                                email=collector_email)
                user_obj.set_password(password)
                user_obj.save()
                collector.user = user_obj
                collector.user.save()
            collector.save()
            return redirect('collector_details', collector_uuid=collector.uuid)
        else:
            return render(request, 'collector_create.html', context)


class CollectorListView(LabMemberMixin, View):
    def get(self, request):
        return render(request, 'collector_list.html', {})


class CollectorCreateView(LISLoginRequiredMixin, View):
    def get(self, request):
        form = forms.CollectorForm()
        return render(request, 'collector_create.html', {'form': form})

    @transaction.atomic
    def post(self, request):
        form = forms.CollectorForm(request.POST)
        context = {'form': form}
        if form.is_valid():
            accounts = form.cleaned_data['accounts']
            first_name = form.cleaned_data['first_name']
            last_name = form.cleaned_data['last_name']
            email = form.cleaned_data['email']
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']

            # create User object before creating Provider, since provider has a username
            collector_uuid = uuid.uuid4()

            if not username:
                lab_schema = request.tenant.schema_name
                username = str(collector_uuid) + '@' + lab_schema + '.com'
            if not password:
                password = str(collector_uuid)

            # create User object
            user_obj = User(first_name=first_name,
                            last_name=last_name,
                            username=username,
                            user_type=11,
                            email=email)
            # setting password is different from obj attribute
            user_obj.set_password(password)
            # create User object first before creating Provider
            user_obj.save()
            collector_obj = models.Collector(user=user_obj)
            collector_obj.save()
            collector_obj.accounts.set(accounts)

            return redirect('collector_list')
        else:
            return render(request, 'collector_create.html', context)


class CollectorDetailsView(LabMemberMixin, View):
    def get(self, request, collector_uuid):
        collector = get_object_or_404(models.Collector, uuid=collector_uuid)
        context = {
            'collector': collector
        }
        return render(request, 'collector_details.html', context)


class CollectorDeleteView(LabMemberMixin, View):
    def get(self, request, collector_uuid):
        collector = get_object_or_404(models.Collector, uuid=collector_uuid)
        collector.is_active = False
        collector.save(update_fields=['is_active'])
        # deactivate the user as well
        collector.user.is_active = False
        collector.user.save(update_fields=['is_active'])
        return redirect('collector_list')


class CriticalContactCreateView(LabMemberMixin, View):
    def get(self, request, account_uuid):
        account = get_object_or_404(models.Account, uuid=account_uuid)
        form = forms.CriticalContactForm()

        context = {'form': form,
                   'account': account}

        return render(request, 'critical_contact_create.html', context)

    def post(self, request, account_uuid):
        account = get_object_or_404(models.Account, uuid=account_uuid)
        form = forms.CriticalContactForm(request.POST)

        context = {'form': form,
                   'account': account}

        if form.is_valid():
            last_name = form.cleaned_data.get('last_name')
            first_name = form.cleaned_data.get('first_name')
            phone_number = form.cleaned_data.get('phone_number')

            critical_contact_obj = models.CriticalContact(account=account,
                                                          last_name=last_name,
                                                          first_name=first_name,
                                                          phone_number=phone_number)
            critical_contact_obj.save()
            return redirect('account_details', account_uuid=account.uuid)
        return render(request, 'critical_contact_create.html', context)


class CriticalContactUpdateView(LabMemberMixin, View):
    def get(self, request, account_uuid, contact_uuid):
        account = get_object_or_404(models.Account, uuid=account_uuid)
        critical_contact = get_object_or_404(models.CriticalContact, uuid=contact_uuid)

        form = forms.CriticalContactForm(initial={'account': account,
                                                  'last_name': critical_contact.last_name,
                                                  'first_name': critical_contact.first_name,
                                                  'phone_number': critical_contact.phone_number})
        context = {'form': form,
                   'account': account}
        return render(request, 'critical_contact_create.html', context)

    def post(self, request, account_uuid, contact_uuid):
        account = get_object_or_404(models.Account, uuid=account_uuid)
        critical_contact = get_object_or_404(models.CriticalContact, uuid=contact_uuid)

        form = forms.CriticalContactForm(request.POST)
        context = {'form': form,
                   'account': account}

        if form.is_valid():
            critical_contact.last_name = form.cleaned_data.get('last_name')
            critical_contact.first_name = form.cleaned_data.get('first_name')
            critical_contact.phone_number = form.cleaned_data.get('phone_number')
            critical_contact.save()
            return redirect('account_details', account_uuid=account.uuid)

        return render(request, 'critical_contact_create.html', context)


class CriticalContactDeleteView(LabMemberMixin, View):
    def get(self, request, account_uuid, contact_uuid):
        account = get_object_or_404(models.Account, uuid=account_uuid)
        critical_contact = get_object_or_404(models.CriticalContact, uuid=contact_uuid)

        critical_contact.is_active = False
        critical_contact.save()

        messages.success(request,
                         'Successfully archived critical contact: {}'.format(str(critical_contact)))
        return redirect('account_details', account_uuid=account.uuid)


class FixPasswords(LabMemberMixin, View):
    def get(self, request):
        fix_password()
        return redirect('order_list')


class AccountAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs = models.Account.objects.all()

        provider = self.forwarded.get('provider', None)

        if provider:
            provider_obj = models.Provider.objects.get(id=provider)
            qs = provider_obj.accounts.all()
        # restrict options available when signed in as collector user
        if self.request.user.user_type == 11:
            collector_obj = models.Collector.objects.get(user_id=self.request.user.id)
            qs = qs.filter(collectors=collector_obj)

        if self.q:
            try:
                int_q = int(self.q)
                id_qs = qs.filter(id=int_q)
            except ValueError:
                id_qs = qs.none()
            qs = qs.filter(name__istartswith=self.q) | qs.filter(name__icontains=self.q) | \
                 qs.filter(alternate_id__icontains=self.q) | id_qs
        qs = qs.distinct()
        return qs

    def get_result_label(self, item):
        if item.alternate_id:
            return "{} - {} ({})".format(item.name, item.id, item.alternate_id)
        else:
            return "{} - {}".format(item.name, item.id)


class CollectorAccountAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs = models.Account.objects.all()

        provider = self.forwarded.get('provider', None)

        if provider:
            provider_obj = models.Provider.objects.get(id=provider)
            qs = provider_obj.accounts.all()

        # restrict options available when signed in as account user
        if self.request.user.user_type == 9:
            qs = qs.filter(user_id=self.request.user.id)
        # restrict options available when signed in as provider user
        elif self.request.user.user_type == 10:
            provider_obj = models.Provider.objects.get(user_id=self.request.user.id)
            qs = provider_obj.accounts.all()
        # restrict options available when signed in as collector user
        elif self.request.user.user_type == 11:
            collector_obj = models.Collector.objects.get(user_id=self.request.user.id)
            qs = collector_obj.accounts.all()

        if self.q:
            try:
                int_q = int(self.q)
                id_qs = qs.filter(id=int_q)
            except ValueError:
                id_qs = qs.none()
            qs = qs.filter(name__istartswith=self.q) | qs.filter(name__icontains=self.q) | \
                 qs.filter(alternate_id__icontains=self.q) | id_qs
        qs = qs.distinct()
        return qs

    def get_result_label(self, item):
        if item.alternate_id:
            return "{} - {} ({})".format(item.name, item.id, item.alternate_id)
        else:
            return "{} - {}".format(item.name, item.id)


class ProviderAutocomplete(autocomplete.Select2QuerySetView):
    """
    List of providers, given an account
    """
    def get_queryset(self):
        qs = models.Provider.objects.all()

        account = self.forwarded.get('account', None)
        account_type = self.forwarded.get('account_type', None)
        if account:
            account_obj = models.Account.objects.get(id=account)
            qs = account_obj.providers.all()
        elif account_type in ['New Account', 'New Location']:
            pass
        else:
            qs = qs.none()

        if self.q:
            qs = qs.filter(user__first_name__istartswith=self.q) | qs.filter(user__last_name__istartswith=self.q) | \
                 qs.filter(npi__icontains=self.q)
        return qs


class PrimaryCareProviderAutocomplete(autocomplete.Select2QuerySetView):
    """
    List of all providers, filtered by previously chosen list of providers
    """
    def get_queryset(self):
        qs = models.Provider.objects.all()

        providers = self.forwarded.get('providers', None)

        if providers:
            qs = qs.filter(id__in=providers)
        else:
            qs = qs.none()

        if self.q:
            qs = qs.filter(user__first_name__istartswith=self.q) | qs.filter(user__last_name__istartswith=self.q)
        return qs


class AllProviderAutocomplete(autocomplete.Select2QuerySetView):
    """
    List of all providers
    """
    def get_queryset(self):
        qs = models.Provider.objects.all()

        if self.q:
            qs = qs.filter(user__first_name__istartswith=self.q) | qs.filter(user__last_name__istartswith=self.q)
        return qs
