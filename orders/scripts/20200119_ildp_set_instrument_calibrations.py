import django
import os

os.environ['DJANGO_SETTINGS_MODULE'] = 'main.settings'
django.setup()

import clients.models as clients_models
import orders.models as orders_models
import lab_tenant.models as lab_tenant_models

from tenant_schemas.utils import tenant_context

"""
Set instrument calibrations for ILDP mass spec Shimadzu 8050 B
"""
TENANT_NAME = "ildp"
INSTRUMENT_A = "Shimadzu 8050"
INSTRUMENT_B = "Shimadzu 8050 B"


def main():
    """
    clone instrument calibrations from Shimadzu 8050 to Shimadzu 8050 B
    :return:
    """
    tenant = clients_models.Client.objects.get(schema_name=TENANT_NAME)

    with tenant_context(tenant):
        instrument_a = lab_tenant_models.InstrumentIntegration.objects.get(internal_instrument_name=INSTRUMENT_A)
        instrument_b = lab_tenant_models.InstrumentIntegration.objects.get(internal_instrument_name=INSTRUMENT_B)

        instrument_calibrations = instrument_a.instrument_calibrations.all()
        new_instruent_calibrations = []
        for instrument_calibration in instrument_calibrations:
            new_instruent_calibrations.append(
                orders_models.InstrumentCalibration(
                    instrument_integration=instrument_b,
                    test_type=instrument_calibration.test_type,
                    target_name=instrument_calibration.target_name,
                    cutoff_value=instrument_calibration.cutoff_value,
                )
            )
        orders_models.InstrumentCalibration.objects.bulk_create(new_instruent_calibrations)


if __name__ == '__main__':
    main()
