import django
import logging
import os
import sys

from django.db import transaction
from tenant_schemas.utils import tenant_context

os.environ['DJANGO_SETTINGS_MODULE'] = 'main.settings'
django.setup()

from clients import models as client_models
from lab import models as lab_models
from .. import models


def main():
    convert_clia_test_types_in_tests()
    convert_clia_test_types_in_test_panels()
    logging.debug("CLIATestType conversions complete.")


@transaction.atomic
def convert_clia_test_types_in_tests():
    """
    For each Test (not TestType), if the Test has clia_test_type, then see if there is a TestType that
    matches the name of the CLIATestType. If not, create a TestType and assign it to Test.test_type

    Then, clear the clia_test_type field for each Test

    :return:
    """

    tenants = client_models.Client.objects.exclude(schema_name='public')

    for tenant in tenants:
        with tenant_context(tenant):

            test_list = models.Test.objects.all()

            for test in test_list:
                # convert CLIATestType to TestType
                if test.clia_test_type:
                    # need to update this to an appropriate Test Method
                    if test.clia_test_type.test_method:
                        method = test.clia_test_type.test_method
                    else:
                        method = lab_models.TestMethod.objects.get(name='Other')

                    # TestType.name --> "analyte_name: test_system_name"
                    name = test.clia_test_type.analyte_name + ': ' + test.clia_test_type.test_system_name
                    complexity = test.clia_test_type.complexity
                    specialty_id = test.clia_test_type.specialty_id
                    required_samples = test.clia_test_type.required_samples

                    # if none, default to Qualitative
                    if test.clia_test_type.result_type:
                        result_type = test.clia_test_type.result_type
                    else:
                        result_type = 'Qualitative'

                    # if analyte exists in TestTarget, use that, otherwise, create a new TestTarget
                    target = lab_models.TestTarget.objects.filter(name=test.clia_test_type.analyte_name)
                    if target:
                        test_target = target.get()
                    else:
                        test_target = lab_models.TestTarget.objects.create(name=test.clia_test_type.analyte_name)

                    # create TestType object --> convert
                    test_type = models.TestType(name=name,
                                                test_target=test_target,
                                                complexity=complexity,
                                                test_method=method,
                                                specialty_id=specialty_id,
                                                required_samples=required_samples,
                                                required_clia_samples=required_samples,
                                                result_type=result_type)
                    test_type.save()

                    # "delete" the CLIA test type after converting the CLIATestType to TestType
                    test.clia_test_type.is_active = False
                    test.clia_test_type.save()

                    logging.debug("{} - {}".format(tenant, test.clia_test_type))


@transaction.atomic
def convert_clia_test_types_in_test_panels():
    """
    For each TestPanel, if the Test has clia_test_type, then see if there is a TestType that
    matches the name of the CLIATestType. If not, create a TestType and assign it to Test.test_type

    Then, clear the clia_test_type field for each Test

    :return:
    """

    tenants = client_models.Client.objects.exclude(schema_name='public')

    for tenant in tenants:
        with tenant_context(tenant):
            test_panel_type_list = models.TestPanelType.objects.all()

            for test_panel_type in test_panel_type_list:
                clia_test_types = test_panel_type.clia_test_types.all()

                for clia_test_type in clia_test_types:
                    # need to update this to an appropriate Test Method
                    if clia_test_type.test_method:
                        method = clia_test_type.test_method
                    else:
                        method = lab_models.TestMethod.objects.get(name='Other')

                    name = clia_test_type.analyte_name + ': ' + clia_test_type.test_system_name
                    complexity = clia_test_type.complexity
                    specialty_id = clia_test_type.specialty_id
                    required_samples = clia_test_type.required_samples

                    # if none, default to Qualitative
                    if clia_test_type.result_type:
                        result_type = clia_test_type.result_type
                    else:
                        result_type = 'Qualitative'

                    # if analyte exists in TestTarget, use that, otherwise, create a new TestTarget
                    target = lab_models.TestTarget.objects.filter(name=clia_test_type.analyte_name)
                    if target:
                        test_target = target.get()
                    else:
                        test_target = lab_models.TestTarget.objects.create(name=clia_test_type.analyte_name)

                    test_type = models.TestType(name=name,
                                                test_target=test_target,
                                                complexity=complexity,
                                                test_method=method,
                                                specialty_id=specialty_id,
                                                required_samples=required_samples,
                                                required_clia_samples=required_samples,
                                                result_type=result_type)
                    test_type.save()

                    # "delete" the CLIA test type after converting the CLIATestType to TestType
                    clia_test_type.is_active = False
                    clia_test_type.save()

                    logging.debug("{} - {}".format(tenant, clia_test_type))


if __name__ == '__main__':
    logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)
    main()
