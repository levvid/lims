# Generated by Django 2.2.2 on 2019-10-15 14:38

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('lab_tenant', '0006_auto_20190906_1515'),
        ('orders', '0093_auto_20191015_1429'),
    ]

    operations = [
        migrations.AddField(
            model_name='observation',
            name='instrument_integration',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='observations', to='lab_tenant.InstrumentIntegration'),
        ),
    ]
