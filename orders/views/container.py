from django.contrib import messages
from django.shortcuts import render, redirect, get_object_or_404
from django.views.generic import View

from lab_tenant.views import LabMemberMixin, LISLoginRequiredMixin
from orders import forms
from orders import models


class ContainerTypeListView(LISLoginRequiredMixin, View):
    def get(self, request):
        return render(request, 'container_type_list.html', {})


class ContainerTypeCreateView(LabMemberMixin, View):
    def get(self, request):
        form = forms.ContainerTypeForm(instance_id=None)

        context = {
            'form': form
        }
        return render(request, 'container_type_create.html', context)

    def post(self, request):
        form = forms.ContainerTypeForm(request.POST, instance_id=None)

        context = {
            'form': form
        }
        if form.is_valid():
            models.ContainerType.objects.create(
                name=form.cleaned_data['name'],
                volume=form.cleaned_data['volume'],
            )
            messages.success(request, "Container type created: {}".format(form.cleaned_data['name']))
            return redirect('container_type_list')
        else:
            return render(request, 'container_type_create.html', context)


class ContainerTypeUpdateView(LabMemberMixin, View):
    def get(self, request, container_type_uuid):
        container_type = get_object_or_404(models.ContainerType, uuid=container_type_uuid)
        form = forms.ContainerTypeForm(
            initial={
                'name': container_type.name,
                'volume': container_type.volume,
            },
            instance_id=container_type.id
        )
        context = {
            'container_type': container_type,
            'form': form,
            'update': True,
        }
        return render(request, 'container_type_create.html', context)

    def post(self, request, container_type_uuid):
        container_type = get_object_or_404(models.ContainerType, uuid=container_type_uuid)
        form = forms.ContainerTypeForm(request.POST, instance_id=container_type.id)
        context = {
            'container_type': container_type,
            'form': form,
            'update': True,
        }

        if form.is_valid():
            container_type.name = form.cleaned_data['name']
            container_type.volume = form.cleaned_data['volume']
            container_type.save()
            return redirect('container_type_details', container_type_uuid=container_type_uuid)
        else:
            return render(request, 'container_type_create.html', context)


class ContainerTypeDetailsView(LISLoginRequiredMixin, View):
    def get(self, request, container_type_uuid):
        container_type = get_object_or_404(models.ContainerType, uuid=container_type_uuid)
        context = {
            'container_type': container_type
        }
        return render(request, 'container_type_details.html', context)
