import django
import logging
import os
import sys
from django.db import transaction
from demos.scripts.ildp_setup_20190722 import initialize_test_panels_from_csv
from tenant_schemas.utils import tenant_context

os.environ['DJANGO_SETTINGS_MODULE'] = 'main.settings'
django.setup()

from clients import models as client_models

"""
Script used to work on task:
Investigate missing panels during ildp import
"""
TEST_PANELS_CSV_URL = 'https://dendi-lis.s3.amazonaws.com/client_data/ildp/Profile CSV.csv'


@transaction.atomic
def main():
    tenant = client_models.Client.objects.get(schema_name='ildp18')
    with tenant_context(tenant):
        initialize_test_panels_from_csv(TEST_PANELS_CSV_URL)


if __name__ == '__main__':
    main()
