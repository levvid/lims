import datetime
import decimal
import logging
import math
import statistics
import time

import boto3
import channels.layers
from asgiref.sync import async_to_sync
from decimal import Decimal

from botocore.exceptions import ClientError
from django.urls import reverse
from django.utils.html import format_html
from geopy import Nominatim
from geopy.extra.rate_limiter import RateLimiter

from django.core.exceptions import ValidationError
from django.contrib import messages
from django.db import transaction
from django.forms import formset_factory
from django.utils import timezone
from django.shortcuts import render, redirect, get_object_or_404
from django.db.models.functions import Lower
from geopy.exc import GeocoderTimedOut

from main.functions import convert_utc_to_tenant_timezone, convert_tenant_timezone_to_utc
from lab.functions import galaxy_ifa_parse

from orders import data_views, forms, models, tasks, generate_billing_hl7_labrcm, generate_billing_hl7
from patients import models as patients_models
from lab_tenant.models import User


def add_result(request, order, sample, test_panel, test_type=None):
    """
    deprecated by add_bulk_results
    :param order:
    :param sample:
    :param test_panel:
    :param test_type:
    :return:
    """

    # save based on tenant point-of-view
    current_time = convert_utc_to_tenant_timezone(request, timezone.now())
    current_day = current_time.date()

    if test_type.turnaround_time_unit == 'Days':
        due_date = current_day + datetime.timedelta(days=test_type.turnaround_time)
    elif test_type.turnaround_time_unit == 'Hours':
        due_date = current_day + datetime.timedelta(hours=test_type.turnaround_time)
    else:  # Minutes
        due_date = current_day + datetime.timedelta(minutes=test_type.turnaround_time)

    test_obj = models.Test.objects.create(
        order=order,
        sample=sample,
        test_type=test_type,
        due_date=due_date
    )
    test_obj.test_panels.set([test_panel])
    result_obj = models.Result.objects.create(
        order=order,
        test=test_obj,
    )
    if test_type.is_research_only:
        result_obj.reference = 'RUO'
        result_obj.save()
    return test_obj, result_obj


def add_containers(sample_volume_dict, order):
    """
    Calculate containers needed and add to order
    :param sample_volume_dict:
    :param order:
    :return:
    """
    containers = []

    for sample in sample_volume_dict:
        container_type_volume_dict = sample_volume_dict[sample]
        container_counter = 1
        for container_type in container_type_volume_dict:
            required_volume = container_type_volume_dict[container_type]
            container_max_volume = container_type.volume
            if container_type.volume:
                num_containers_needed = math.ceil(required_volume / container_max_volume)
            else:
                num_containers_needed = 1

            for container_number in range(num_containers_needed):
                container_code = "{}-{}".format(sample.code, container_counter)
                containers.append(
                    models.Container(
                        code=container_code,
                        container_type=container_type,
                        order=order,
                        sample=sample,
                        collection_date=sample.collection_date,
                    )
                )
                container_counter += 1
    container_objects = models.Container.objects.bulk_create(containers)

    for container in container_objects:
        container.barcode = get_sample_barcode(container.id, zfill=11)
    models.Container.objects.bulk_update(container_objects, ['barcode'])

    return container_objects


def add_bulk_results(request, result_list):
    """
    :param result_list: [(order, sample, test_panel, test_type)]
    :return:
    """
    # find set of test types
    distinct_result_list = []
    distinct_result_dict = {}
    for result in result_list:
        order = result[0]
        sample = result[1]
        test_type = result[3]
        test_panel = result[2]
        try:
            distinct_result_dict[order][sample][test_type].append(test_panel)
        except KeyError:
            try:
                distinct_result_dict[order][sample][test_type] = [test_panel]
            except KeyError:
                try:
                    distinct_result_dict[order][sample] = {}
                    distinct_result_dict[order][sample][test_type] = [test_panel]
                except KeyError:
                    distinct_result_dict[order] = {}
                    distinct_result_dict[order][sample] = {}
                    distinct_result_dict[order][sample][test_type] = [test_panel]

    for order in distinct_result_dict:
        samples = distinct_result_dict[order].keys()
        for sample in samples:
            test_types = distinct_result_dict[order][sample]
            for test_type in test_types:
                distinct_result_list.append((order, sample, distinct_result_dict[order][sample][test_type], test_type))

    test_objs = models.Test.objects.bulk_create([models.Test(
        order=x[0],
        sample=x[1],
        test_type=x[3]
    ) for x in distinct_result_list])  # having problem here!!!

    results = models.Result.objects.bulk_create([models.Result(
        order=x[1][0],
        test=x[0]
    ) for x in zip(test_objs, distinct_result_list)])

    result_update_list = []
    test_update_list = []
    for result, test_obj, (order, sample, test_panels, test_type) in zip(results, test_objs, distinct_result_list):
        test_obj.test_panels.set(test_panels)
        if test_type.is_research_only:
            result.reference = 'RUO'
            result_update_list.append(result)
        if order.received_date:
            # save based on tenant point-of-view
            received_datetime_tenant = convert_utc_to_tenant_timezone(request, order.received_date)
            received_date = received_datetime_tenant
            if test_type.turnaround_time_unit == 'Days':
                test_obj.due_date = received_date + datetime.timedelta(days=test_type.turnaround_time)
            elif test_type.turnaround_time_unit == 'Hours':
                test_obj.due_date = received_date + datetime.timedelta(hours=test_type.turnaround_time)
            else:  # Minutes
                test_obj.due_date = received_date + datetime.timedelta(minutes=test_type.turnaround_time)
            test_update_list.append(test_obj)

        # cutoff and upper limit of quantification
        test_method = test_type.test_method.name
        try:
            result.cutoff = test_type.test_type_ranges.filter(default=True).get().range_low
            result_update_list.append(result)
        except models.TestTypeRange.DoesNotExist:
            result.cutoff = None

        # for specimen validity, then use range high for upper limit of quantification
        try:
            if test_method == 'Specimen Validity':
                result.upper_limit_of_quantification = test_type.test_type_ranges.filter(
                    default=True).get().range_high
            else:
                result.upper_limit_of_quantification = test_type.test_type_ranges.filter(
                    default=True).get().critical_high
            result_update_list.append(result)
        except models.TestTypeRange.DoesNotExist:
            result.upper_limit_of_quantification = None

    if result_update_list:
        models.Result.objects.bulk_update(result_update_list, ['reference', 'cutoff', 'upper_limit_of_quantification'])
    if test_update_list:
        models.Test.objects.bulk_update(test_update_list, ['due_date'])
    return test_objs, results


def generate_order_code():
    """
    generate a new order code
    DEPRECATED by tasks.create_order_with_cod\e
    :return:
    """
    date_prefix = timezone.now().date().strftime('%Y')
    last_order = models.Order.all_objects.filter(code__istartswith=date_prefix).exists()

    if last_order:
        last_order = models.Order.all_objects.filter(code__istartswith=date_prefix).order_by('-code').last()
        last_code_suffix = int(last_order.code[-7:])
        code = date_prefix + '-' + str(last_code_suffix + 1).zfill(7)
    else:
        code = date_prefix + '-0000001'
    return code


def generate_sample_code(order_code, sample_type):
    try:
        samples = models.Sample.all_objects.filter(order__code=order_code,
                                                   clia_sample_type=sample_type)
        if samples:
            code = order_code + '-' + sample_type.name[0].upper() + '-' + str(len(samples) + 1)
        else:
            code = order_code + '-' + sample_type.name[0].upper() + '-1'
    except ValueError:
        samples = models.Sample.all_objects.filter(order__code=order_code,
                                                   clia_sample_type=sample_type)
        if samples:
            code = order_code + '-' + sample_type.abbreviation + '-' + str(len(samples) + 1)
        else:
            code = order_code + '-' + sample_type.abbreviation + '-1'
    return code


def get_test_forms(request, requested_panel_ids):
    """
    gets context for test selection forms

    :param request:
    :param requested_panel_ids:
    :return:
    """
    test_panel_types_data = {}
    TestFormSet_dict = {}
    test_formset_dict = {}
    test_data_and_formset_dict = {}
    test_panel_names = []

    for test_panel_id in requested_panel_ids:
        test_panel = models.TestPanelType.objects.prefetch_related('test_types').get(id=test_panel_id)
        test_panel_name = test_panel.name
        test_panel_names.append(test_panel_name)
        tests = test_panel.test_types.all()

        test_panel_types_data[test_panel_name] = [
            {
                'test_type': x,
                'checked': True,
            }
            for x in tests]

        TestFormSet_dict[test_panel_name] = formset_factory(forms.TestSelectForm)

        if request.method == 'POST':
            test_formset_dict[test_panel_name] = TestFormSet_dict[test_panel_name](
                request.POST,
                prefix='test_{}'.format(test_panel_name),
            )
            pass
        else:
            test_formset_dict[test_panel_name] = TestFormSet_dict[test_panel_name](
                prefix='test_{}'.format(test_panel_name),
                initial=test_panel_types_data[test_panel_name]
            )

        test_data_and_formset_dict[test_panel_name] = list(zip(
            test_panel_types_data[test_panel_name],
            test_formset_dict[test_panel_name]
        ))

    context = {
        'test_formset_dict': test_formset_dict,
        'test_data_and_formset_dict': test_data_and_formset_dict,
        'requested_panel_ids': requested_panel_ids,
        'test_panel_names': test_panel_names,
    }
    return context


def get_test_panel_forms(request, order=None, account=None, provider=None):
    """
    gets context for test panel selection forms.

    Excludes certain panels based on and Account.excluded_test_panels
    :param request:
    :param order:
    :param account:
    :return:
    """
    test_panel_categories = models.TestPanelCategory.objects.all()
    populated_test_panel_categories = []

    test_panel_types_data = {}
    TestPanelFormSet_dict = {}
    test_panel_formset_dict = {}
    test_panel_data_and_formset_dict = {}
    for test_panel_category in test_panel_categories:
        test_panel_types = models.TestPanelType.objects. \
            filter(test_panel_category=test_panel_category, is_exclusive=False). \
            order_by('category_order'). \
            prefetch_related('test_panel_category')
        if provider:
            # include TestPanels exclusive to this provider
            test_panel_types = test_panel_types | \
                               models.TestPanelType.objects.filter(test_panel_category=test_panel_category,
                                                                   is_exclusive=True,
                                                                   exclusive_providers=provider)

        if account:
            # include TestPanels exclusive to this account
            test_panel_types = test_panel_types.exclude(excluded_accounts=account) | \
                               models.TestPanelType.objects.filter(test_panel_category=test_panel_category,
                                                                   is_exclusive=True,
                                                                   exclusive_accounts=account)
        test_panel_types = test_panel_types.distinct()

        if order:
            test_panel_types_data[test_panel_category] = [
                {'test_panel_type': x,
                 'category': x.test_panel_category.name,
                 'category_order': x.category_order,
                 'checked': x in [x.test_panel_type for x in
                                  order.test_panels.all().prefetch_related('test_panel_type')]}
                for x in test_panel_types]
        else:
            test_panel_types_data[test_panel_category] = [
                {'test_panel_type': x,
                 'category': x.test_panel_category.name,
                 'category_order': x.category_order}
                for x in test_panel_types]

        if test_panel_types_data[test_panel_category]:
            TestPanelFormSet_dict[test_panel_category] = formset_factory(forms.TestPanelSelectForm)

            if request.method == 'POST':
                test_panel_formset_dict[test_panel_category] = TestPanelFormSet_dict[test_panel_category](
                    request.POST,
                    prefix='test_panel_{}'.format(test_panel_category)
                )
            else:
                test_panel_formset_dict[test_panel_category] = TestPanelFormSet_dict[test_panel_category](
                    prefix='test_panel_{}'.format(test_panel_category),
                    initial=test_panel_types_data[test_panel_category]
                )
            test_panel_data_and_formset_dict[test_panel_category] = list(zip(
                test_panel_types_data[test_panel_category],
                test_panel_formset_dict[test_panel_category]
            ))

            populated_test_panel_categories.append(test_panel_category)

    context = {'test_panel_formset_dict': test_panel_formset_dict,
               'test_panel_data_and_formset_dict': test_panel_data_and_formset_dict,
               'test_panel_categories': populated_test_panel_categories}
    return context


def get_order_reflex(order_code):
    def reflex_triggered(reflex, result, result_type):
        """
        :param reflex:
        :param result:
        :param result_type:
        :return: bool
        """
        if result_type == 'Qualitative':
            if reflex.reflex_criteria_qualitative == result.result:
                return True
            else:
                return False
        else:
            if '=' in reflex.quantitative_comparison_operator and reflex.reflex_criteria_quantitative == Decimal(
                result.result):
                return True
            elif '>' in reflex.quantitative_comparison_operator and \
                Decimal(result.result) > reflex.reflex_criteria_quantitative:
                return True
            elif '<' in reflex.quantitative_comparison_operator and \
                Decimal(result.result) < reflex.reflex_criteria_quantitative:
                return True
            else:
                return False

    """

    :param order_code:
    :return: has_reflex, reflex_list
    """
    order = models.Order.objects.get(code=order_code)
    results = order.results.filter(result__isnull=False,
                                   reflex_triggered=False).all()

    has_reflex = False
    reflex_list = []

    for result in results:
        test_type = result.test.test_type
        test_type_reflex_list = test_type.test_type_reflexes.all()
        for reflex in test_type_reflex_list:
            if reflex_triggered(reflex, result, test_type.result_type):
                has_reflex = True
                reflex_list.append((result, reflex, result.get_result_label(reflex.reflex_criteria_qualitative)))

    return has_reflex, reflex_list


def get_sample_barcode(sample_id, zfill=10):
    """
    Simple encryption algorithm to generate sample barcode from the sample_id
    :param sample_id
    :return: sample_barcode
    """
    output = []
    message_string = str(sample_id).zfill(zfill)  # zero pad the sample.id
    for char in message_string:
        if ord(char) > 54:
            output.append(chr(ord(char) - 7))  # to avoid x > 7 going into :...
        else:
            output.append(chr(ord(char) + 3))
    sample_barcode = ''.join(output)
    return sample_barcode


def get_result_interpretation(positive_above_cutoff, result, cutoff_value, test_method):
    """
    Get the result interpretation - whether Positive, Negative or inconsistent
    :param positive_above_cutoff: bool
    :param result: str
    :param cutoff_value: Decimal
    :param test_method: TestMethod obj
    :return:
    """

    # if result is None, return Negative
    if result is None:
        interpretation = test_method.negative_result_label
        return interpretation

    if positive_above_cutoff:
        if result > cutoff_value:
            interpretation = test_method.positive_result_label
        elif result == cutoff_value:
            interpretation = test_method.positive_result_label
        else:
            interpretation = test_method.negative_result_label
    else:
        if result < cutoff_value:
            interpretation = test_method.positive_result_label
        elif result == cutoff_value:
            interpretation = test_method.positive_result_label
        else:
            interpretation = test_method.negative_result_label

    return interpretation


def import_instrument_data(data_in_uuid, tenant_id, user_id, identifier='accession_number'):
    import timeit
    t0 = timeit.default_timer()
    data_in_object = models.DataIn.objects.get(uuid=data_in_uuid)
    instrument_integration = data_in_object.instrument_integration
    instrument_calibrations = models.InstrumentCalibration.objects. \
        filter(instrument_integration=instrument_integration). \
        prefetch_related('test_type').all()
    # modified result objects to be bulk updated at the end
    result_list = []
    results = data_views.parse_result_file(data_in_uuid)

    target_name_dict = dict()

    observation_object_list = []
    channel_layer = channels.layers.get_channel_layer()

    order_list = []

    if identifier == 'sample_code':
        sample_codes = [x[0] for x in results]
        sample_objects = models.Sample.objects.filter(code__in=sample_codes,
                                                      order__received_date__isnull=False). \
            prefetch_related('tests',
                             'tests__result',
                             'tests__test_type',
                             'tests__test_type__instrument_calibrations',
                             'tests__test_type__test_target').all()

        for index, (sample_code, sample_results_dict) in enumerate(results):
            async_to_sync(channel_layer.group_send)(str(data_in_uuid), {
                'type': 'status_update',
                'message': '<i class="fas fa-spinner"></i> Processing {}/{} samples'.format(index + 1,
                                                                                            len(results))
            })
            t2 = timeit.default_timer()
            try:
                sample = sample_objects.get(code=sample_code)
                control = None
            except models.Sample.DoesNotExist:
                sample = None
                try:
                    control = models.Control.objects.get(code=sample_code)
                except:
                    control = None

            if sample:
                order = sample.order
                tests = sample.tests.all()
                for test in tests:
                    try:
                        target_name = target_name_dict[test.test_type.id]
                    except KeyError:
                        try:
                            calibration = instrument_calibrations.get(test_type=test.test_type)
                            target_name = calibration.target_name
                            target_name_dict[test.test_type.id] = target_name
                        except models.InstrumentCalibration.DoesNotExist:
                            target_name = None

                    if target_name:
                        sample_target_observations = sample_results_dict.get(target_name)

                        if sample_target_observations:
                            order_list.append(order)
                            result = test.result.get()
                            qualitative_results = list(
                                set([x['result_qualitative'] for x in sample_target_observations]))
                            if qualitative_results == ['Positive']:
                                interpreted_result = '+'
                            elif qualitative_results == ['Negative']:
                                interpreted_result = '-'
                            else:
                                interpreted_result = '?'
                            result.result = interpreted_result

                            try:
                                result_quantitative = statistics.mean(
                                    [x['result_value'] for x in sample_target_observations if x['result_value']])
                            except statistics.StatisticsError:
                                result_quantitative = None
                            result.result_quantitative = result_quantitative
                            result.cutoff = sample_target_observations[0]['cutoff']
                            result_list.append(result)

                            for observation in sample_target_observations:
                                observation_obj = models.Observation(
                                    sample=sample,
                                    sample_name=sample_code,
                                    target_name=target_name,
                                    data_in=data_in_object,
                                    instrument_integration=instrument_integration,
                                    test=test,
                                    result=result,
                                    result_value=observation['result_value'],
                                    cutoff=observation['cutoff'],
                                    result_qualitative=observation['result_qualitative'],
                                    well=observation.get('well', ''),
                                    well_position=observation.get('well_position', '')
                                )
                                observation_object_list.append(observation_obj)
            elif control:
                for target_name in sample_results_dict:
                    sample_target_observations = sample_results_dict[target_name]
                    for observation in sample_target_observations:
                        observation_obj = models.Observation(
                            observation_date=data_in_object.created_datetime,
                            sample=None,
                            sample_name=sample_code,
                            target_name=target_name,
                            instrument_integration=instrument_integration,
                            data_in=data_in_object,
                            test=None,
                            result=None,
                            control=control,
                            result_value=observation['result_value'],
                            cutoff=observation['cutoff'],
                            result_qualitative=observation['result_qualitative'],
                            well=observation.get('well', ''),
                            well_position=observation.get('well_position', '')
                        )
                        observation_object_list.append(observation_obj)
            else:
                for target_name in sample_results_dict:
                    sample_target_observations = sample_results_dict[target_name]
                    test = None
                    result = None
                    for observation in sample_target_observations:
                        observation_obj = models.Observation(
                            sample=sample,
                            sample_name=sample_code,
                            target_name=target_name,
                            instrument_integration=instrument_integration,
                            data_in=data_in_object,
                            test=test,
                            result=result,
                            result_value=observation['result_value'],
                            cutoff=observation['cutoff'],
                            result_qualitative=observation['result_qualitative'],
                            well=observation.get('well', ''),
                            well_position=observation.get('well_position', '')
                        )
                        observation_object_list.append(observation_obj)
            t3 = timeit.default_timer()
            print("{}: {} seconds".format(sample_code,
                                          t3 - t2))
    else:
        accession_numbers = [x[0] for x in results]
        result_objects = models.Result.objects.filter(
            order__accession_number__in=accession_numbers,
            order__received_date__isnull=False
        ). \
            prefetch_related(
            'test',
            'test__test_type',
            'test__test_type__instrument_calibrations',
            'test__test_type__test_target'
        ).all()
        for index, (accession_number, results_dict) in enumerate(results):
            async_to_sync(channel_layer.group_send)(str(data_in_uuid), {
                'type': 'status_update',
                'message': '<i class="fas fa-spinner"></i> Processing {}/{} samples'.format(index + 1,
                                                                                            len(results))
            })
            t2 = timeit.default_timer()
            order_results = result_objects.filter(order__accession_number=accession_number)
            if len(order_results) == 0:
                try:
                    control = models.Control.objects.get(code=accession_number)
                except models.Control.DoesNotExist:
                    control = None

                if control:
                    for target_name in results_dict:
                        target_observations = results_dict[target_name]
                        for observation in target_observations:
                            observation_obj = models.Observation(
                                observation_date=data_in_object.created_datetime,
                                sample=None,
                                sample_name=accession_number,
                                target_name=target_name,
                                instrument_integration=instrument_integration,
                                data_in=data_in_object,
                                test=None,
                                result=None,
                                control=control,
                                result_value=observation['result_value'],
                                cutoff=observation['cutoff'],
                                result_qualitative=observation['result_qualitative'],
                                well=observation.get('well', ''),
                                well_position=observation.get('well_position', '')
                            )
                            observation_object_list.append(observation_obj)
                else:
                    for target_name in results_dict:
                        sample_target_observations = results_dict[target_name]
                        test = None
                        result = None
                        for observation in sample_target_observations:
                            observation_obj = models.Observation(
                                sample=None,
                                sample_name=accession_number,
                                target_name=target_name,
                                instrument_integration=instrument_integration,
                                data_in=data_in_object,
                                test=test,
                                result=result,
                                result_value=observation['result_value'],
                                cutoff=observation['cutoff'],
                                result_qualitative=observation['result_qualitative'],
                                well=observation.get('well', ''),
                                well_position=observation.get('well_position', '')
                            )
                            observation_object_list.append(observation_obj)
            else:
                order = models.Order.objects.get(accession_number=accession_number)
                control = None
                order_list.append(order)
                for order_result in order_results:
                    test = order_result.test
                    try:
                        target_name = target_name_dict[test.test_type.id]
                    except KeyError:
                        try:
                            calibration = instrument_calibrations.get(test_type=test.test_type)
                            target_name = calibration.target_name
                            target_name_dict[test.test_type.id] = target_name
                        except models.InstrumentCalibration.DoesNotExist:
                            target_name = None

                    if target_name:
                        target_observations = results_dict.get(target_name)

                        if target_observations:
                            result = order_result
                            qualitative_results = list(
                                set([x['result_qualitative'] for x in target_observations]))
                            if qualitative_results == ['Positive']:
                                interpreted_result = '+'
                            elif qualitative_results == ['Negative']:
                                interpreted_result = '-'
                            else:
                                interpreted_result = '?'
                            result.result = interpreted_result

                            try:
                                result_quantitative = statistics.mean(
                                    [x['result_value'] for x in target_observations if x['result_value']])
                            except statistics.StatisticsError:
                                result_quantitative = None
                            result.result_quantitative = result_quantitative
                            result.cutoff = target_observations[0]['cutoff']
                            result_list.append(result)

                            for observation in target_observations:
                                observation_obj = models.Observation(
                                    sample=test.sample,
                                    sample_name=accession_number,
                                    target_name=target_name,
                                    data_in=data_in_object,
                                    instrument_integration=instrument_integration,
                                    test=test,
                                    result=result,
                                    result_value=observation['result_value'],
                                    cutoff=observation['cutoff'],
                                    result_qualitative=observation['result_qualitative'],
                                    well=observation.get('well', ''),
                                    well_position=observation.get('well_position', '')
                                )
                                observation_object_list.append(observation_obj)

    models.Observation.objects.bulk_create(observation_object_list)
    models.Result.objects.bulk_update(result_list, ['result', 'result_quantitative', 'cutoff'])
    # after report tasks
    if instrument_integration.after_data_import == 'Automatically complete orders':
        for order in order_list:
            order.completed = True
            order.completed_date = timezone.now()
        models.Order.objects.bulk_update(order_list, ['completed', 'completed_date'])
        async_to_sync(channel_layer.group_send)(str(data_in_uuid), {
            'type': 'notification',
            'message': '<strong>All orders with imported results have been completed.</strong>'
        })
    elif instrument_integration.after_data_import == 'Automatically report orders':
        # approve all Tests
        tests_to_approve = []
        for result in result_list:
            test = result.test
            test.is_approved = True
            test.approved_user_id = user_id
            test.approved_date = timezone.now()
            tests_to_approve.append(test)
        models.Test.objects.bulk_update(tests_to_approve, ['is_approved', 'approved_user_id', 'approved_date'])

        reportable_orders = []
        for order in order_list:
            reportable = report_order_auto(tenant_id, order, user_id)
            if reportable:
                reportable_orders.append(order)
        async_to_sync(channel_layer.group_send)(str(data_in_uuid), {
            'type': 'notification',
            'message': '<strong>All orders with imported results have been reported.</strong>'.format(
                len(reportable_orders),
                len(order_list)
            )
        })

    t1 = timeit.default_timer()
    print("total: {} seconds".format(t1 - t0))


def get_test_method_abbreviated_names(sample):
    """
    :param sample: obj
    :return:
    """
    tests = sample.tests.all()
    test_methods = list(set([x.test_type.test_method for x in tests if x.test_type]))
    test_methods_abbreviated_names = []
    for test_method in test_methods:
        test_method_name = test_method.name
        if test_method.name == 'Toxicology Screening':
            test_methods_abbreviated_names.append('TS')
        elif test_method.name == 'Specimen Validity':
            test_methods_abbreviated_names.append('SV')
        elif test_method.name == 'Chemistry-Immunoassay':
            test_methods_abbreviated_names.append('CI')
        elif test_method.name == 'Waived':
            test_methods_abbreviated_names.append('W')
        else:
            test_methods_abbreviated_names.append(test_method.name)

    sorted_abbreviated_names = sorted(test_methods_abbreviated_names, key=len)
    return sorted_abbreviated_names  # sorted by length so that LC-MS is last


def get_result_forms(request, order_code):
    """
    gets context for forms
    :param request:
    :param order_code:
    :return:
    """
    order = get_object_or_404(models.Order, code=order_code)
    tests = order.tests.all().prefetch_related('test_type', 'test_type__test_method',
                                               'test_type__test_type_ranges', )
    results = order.results.filter().order_by(Lower('test__test_type__name')) \
        .prefetch_related('test', 'user', 'test__test_type',
                          'test__test_type__test_method',
                          'test__sample', 'test__test_type__test_target',
                          'test__test_type__test_type_ranges',
                          'observations', 'observations__data_in',
                          'observations__data_in__instrument_integration')
    test_methods = sorted(
        list(set([x.test_type.test_method for x in tests if x.test_type and x.test_type.test_method])),
        key=lambda y: (y.display_order, y.name))
    test_users = {}
    context = {}

    for test_method in test_methods:
        test_method_name = test_method.name
        user_form_prefix = "{}_user".format(test_method_name)
        result_form_prefix = "{}".format(test_method_name)

        ResultFormSet = formset_factory(forms.ResultForm)

        qualitative_test_data = [{'result_obj': x,
                                  'due_date': convert_utc_to_tenant_timezone(request, x.test.due_date).strftime(
                                      '%m-%d-%Y %H:%M') if x.test.due_date else x.test.due_date,
                                  'result_label': x.result_info_label,
                                  'result_label_prefix': x.result_info_label_prefix,
                                  'reference': x.reference,
                                  'result': x.result,
                                  'user': x.user}
                                 for x in results if
                                 x.test.test_type and x.test.test_type.result_type == 'Qualitative'
                                 and x.test.test_type.test_method == test_method]
        quantitative_test_data = [{'result_obj': x,
                                   'due_date': convert_utc_to_tenant_timezone(request, x.test.due_date).strftime(
                                       '%m-%d-%Y %H:%M') if x.test.due_date else x.test.due_date,
                                   'result_quantitative': x.result_quantitative,
                                   'result_label_prefix': x.result_info_label_prefix,
                                   'result_label': x.result_info_label,
                                   'reference': x.reference,
                                   'result': x.result,
                                   'user': x.user}
                                  for x in results if
                                  x.test.test_type and x.test.test_type.result_type == 'Quantitative'
                                  and x.test.test_type.test_method == test_method]

        # merge and sort both types
        test_data = sorted(qualitative_test_data + quantitative_test_data, key=lambda y: (
            # having a test target isn't currently enforced
            y['result_obj'].test.sample.code
        ))

        for test_datum in test_data:
            # convert collection date to tenant time
            test_datum['result_obj'].test.sample.collection_date = convert_utc_to_tenant_timezone(
                request,
                test_datum['result_obj'].test.sample.collection_date
            )
            # convert test due date to tenant time
            test_datum['result_obj'].test.due_date = convert_utc_to_tenant_timezone(
                request,
                test_datum['result_obj'].test.due_date
            )

        if request.method == 'POST':
            test_result_formset = ResultFormSet(request.POST, prefix=result_form_prefix,
                                                form_kwargs={'test_method': test_method})
            user_form = forms.TestPerformedByForm(request.POST, prefix=user_form_prefix)
        else:
            test_result_formset = ResultFormSet(prefix=result_form_prefix, initial=test_data,
                                                form_kwargs={'test_method': test_method})
            user_form_id = order.test_performed_by.get(test_method_name)
            if user_form_id:
                user_object = User.all_objects.get(id=user_form_id)
                user_form = forms.TestPerformedByForm(prefix=user_form_prefix,
                                                      initial={'user': user_object})
                test_users[test_method_name.replace(
                    '-', '_').replace(' ', '_').lower()] = user_object
            else:
                user_form = forms.TestPerformedByForm(prefix=user_form_prefix)

        test_data_and_formset = zip(test_data, test_result_formset)
        context.update({"{}_data_and_formset".format(
            test_method_name.replace('-', '_').replace(' ', '_').lower()): test_data_and_formset,
                        "{}_result_formset".format(
                            test_method_name.replace('-', '_').replace(' ', '_').lower()): test_result_formset,
                        "{}_user_form".format(test_method_name.replace('-', '_').replace(' ', '_').lower()): user_form})

    # the report approved by form defaults to the last User that approved any report (preliminary/final)
    # if no report has been approved before, then default to current User
    last_report = models.Report.objects.filter(order=order).order_by('-created_date').first()
    if last_report:
        last_approved_by = last_report.approved_by
    else:
        last_approved_by = request.user

    if request.method == 'POST':
        report_approved_by_form = forms.ReportApprovedByForm(request.POST)
    else:
        if request.tenant.settings.get('hide_report_approved_by'):
            report_approved_by_form = None
        else:
            report_approved_by_form = forms.ReportApprovedByForm(initial={'user': last_approved_by})

    context.update({'results': results,
                    'test_users': test_users,
                    'test_methods': test_methods,
                    'last_approved_by': last_approved_by,
                    'report_approved_by_form': report_approved_by_form})
    return context


def report_order(request, order, approved_by=None, preliminary=False):
    """
    approved by: User object
    """
    # check if every test has been approved
    test_count = order.tests.all().count()
    approved_test_count = order.tests.filter(is_approved=True).all().count()
    success = True

    # specimen validity tests have not been approved
    if test_count == approved_test_count:
        order.partial = False
    elif approved_test_count == 0 and not order.has_unresolved_public_issues:  # 0 Tests have been approved and order has no unresolved public issues
        messages.error(request, 'Please approve test(s) or add unresolved public issues before reporting.')
        success = False
        return success

    if not approved_by:
        # initialize with the last person that approved the report
        # if that doesn't exist, use request.user
        last_report = models.Report.objects.filter(order=order).order_by('-created_date').first()
        if last_report:
            approved_by = last_report.approved_by
            if not approved_by:
                approved_by = request.user
        else:
            approved_by = request.user

    report_approved_by_id = approved_by.id

    if preliminary:
        # check if there are any tests that are approved
        # create preliminary report
        num_tests = order.tests.filter(is_approved=True).count()
        if num_tests == 0 and not order.has_unresolved_public_issues:
            messages.error(request, "Order must contain at least one approved result "
                                    "or contain at least one unresolved public issue.")
        else:
            order.creating_preliminary_report = True
            order.save(update_fields=['creating_preliminary_report'])
    else:
        # if final report
        # Check account reporting preference
        pending_results_count = order.results.filter(result='.').count()
        if order.account.report_preference == 'Final Reporting Only':
            if test_count != approved_test_count:
                messages.error(request, 'Account: {} is set to Final Reporting Only. <br>'
                                        'All tests must be approved before reporting.'.format(order.account.name))
                success = False
                return success
            elif pending_results_count != 0:
                messages.error(request, 'Account: {} is set to Final Reporting Only. <br>'
                                        'A test may not have Pending result before reporting.'.format(order.account.name))
                success = False
                return success

        # set test initial report date
        approved_tests = order.tests.filter(is_approved=True, initial_report_date__isnull=True)
        approved_test_list = []
        for test in approved_tests:
            test.initial_report_date = timezone.now()
            approved_test_list.append(test)
        models.Test.objects.bulk_update(approved_test_list, ['initial_report_date'])

        order.creating_report = True
        order.save(update_fields=['creating_report', 'partial'])

    # generate report using celery task
    tasks.generate_order_report.apply_async(
        args=[request.tenant.id, str(order.uuid), report_approved_by_id, preliminary],
        queue='default'
    )
    return success


def report_order_auto(tenant_id, order, approved_by_id):
    """
    report order function used by data import functions
    approved by: UserID
    """
    # check if every test has been approved
    test_count = order.tests.all().count()
    approved_test_count = order.tests.filter(is_approved=True).all().count()
    # specimen validity tests have not been approved
    if test_count == approved_test_count:
        order.partial = False
    elif approved_test_count == 0 and not order.has_unresolved_public_issues:  # 0 Tests have been approved and order has no unresolved public issues
        return False

    report_approved_by_id = approved_by_id

    if order.account.report_preference == 'Final Reporting Only' and test_count != approved_test_count:
        return False

    # if final report
    # set test initial report date
    approved_tests = order.tests.filter(is_approved=True, initial_report_date__isnull=True)
    approved_test_list = []
    for test in approved_tests:
        test.initial_report_date = timezone.now()
        approved_test_list.append(test)
    models.Test.objects.bulk_update(approved_test_list, ['initial_report_date'])

    order.creating_report = True
    order.save(update_fields=['creating_report', 'partial'])

    # generate report using celery task
    tasks.generate_order_report.apply_async(
        args=[tenant_id, str(order.uuid), report_approved_by_id, False],
        queue='default'
    )
    return True


def approve_all_test_results(request, order):
    # approve all unapproved tests
    unapproved_tests = order.tests.filter(
        is_approved=False,
        result__result__isnull=False
    ).prefetch_related(
        'result',
        'result',
        'test_type__test_method'
    )

    if unapproved_tests:
        test_list = []
        for test in unapproved_tests:
            test.is_approved = True
            test.approved_user = request.user
            test.approved_date = convert_utc_to_tenant_timezone(request, timezone.now())
            test_list.append(test)
        models.Test.objects.bulk_update(test_list, ['is_approved', 'approved_user', 'approved_date'])


def save_test_results(request, order, context):
    no_form_errors = True
    for test_method in context['test_methods']:
        test_method_name = test_method.name
        formset_name = "{}_result_formset".format(
            test_method_name.replace('-', '_').replace(' ', '_').lower())
        data_and_formset_name = "{}_data_and_formset".format(
            test_method_name.replace('-', '_').replace(' ', '_').lower())
        user_form_name = "{}_user_form".format(
            test_method_name.replace('-', '_').replace(' ', '_').lower())

        formset = context[formset_name]
        data_and_formset = context[data_and_formset_name]
        user_form = context[user_form_name]

        if formset.is_valid():
            result_list = []
            test_list = []
            for data, result_form in data_and_formset:
                if result_form.is_valid():
                    result_obj = data['result_obj']
                    due_date = result_form.cleaned_data.get('due_date')
                    if due_date:
                        due_date = convert_tenant_timezone_to_utc(request, due_date)

                    result_quantitative = None
                    # used for Galaxy IFA and QuantStudio integration
                    result_info_label = result_form.cleaned_data.get('result_label')
                    result_info_label_prefix = result_form.cleaned_data.get('result_label_prefix')
                    reference = result_form.cleaned_data.get('reference')

                    # IFA result formset is for Galaxy only for now
                    if formset_name == 'ifa_result_formset' and request.tenant.name == 'Galaxy Diagnostics':
                        result = galaxy_ifa_parse(result_form.cleaned_data.get('result'), result_info_label,
                                                  result_info_label_prefix)

                    elif result_obj.test.test_type.result_type == 'Qualitative':
                        result = result_form.cleaned_data.get('result')
                    # using "below range" "within range", and "above range" instead of positive or negative
                    elif result_obj.test.test_type.test_method.is_within_range:
                        result_quantitative = result_form.cleaned_data.get('result_quantitative')
                        if result_quantitative:
                            result_quantitative = decimal.Decimal(result_quantitative).quantize(
                                decimal.Decimal('.00000001'), rounding=decimal.ROUND_DOWN)
                            if result_quantitative >= decimal.Decimal('99999999.99999999'):
                                result_quantitative = decimal.Decimal('99999999.99999999')
                        try:
                            low_value = decimal.Decimal(
                                result_obj.test.test_type.test_type_ranges.get().range_low)
                            high_value = decimal.Decimal(
                                result_obj.test.test_type.test_type_ranges.get().range_high)
                        except models.TestTypeRange.DoesNotExist:
                            low_value = None  # if cutoff is not provided
                            high_value = None
                        result = result_form.cleaned_data.get('result')
                        # 0.00 is treated as false value
                        has_quantitative_result = result_quantitative or result_quantitative == Decimal('0.00')
                        if not result and low_value and high_value and has_quantitative_result:
                            if result_quantitative >= low_value and result_quantitative <= high_value:
                                result = '~'
                            elif result_quantitative < low_value:
                                result = '<'
                            else:
                                result = '>'
                    else:  # Quantitative
                        try:
                            result_quantitative = decimal.Decimal(
                                result_form.cleaned_data.get('result_quantitative')).quantize(
                                decimal.Decimal('.00000001'), rounding=decimal.ROUND_DOWN)
                            if result_quantitative >= decimal.Decimal('99999999.99999999'):
                                result_quantitative = decimal.Decimal('99999999.99999999')
                        except TypeError:
                            result_quantitative = None
                        positive_above_cutoff = result_obj.test.test_type.test_method.positive_above_cutoff
                        try:
                            if positive_above_cutoff:  # fault tolerance for cutoff not existing
                                cutoff_value = decimal.Decimal(
                                    result_obj.test.test_type.test_type_ranges.filter(default=True).get().range_high)
                            else:
                                cutoff_value = decimal.Decimal(
                                    result_obj.test.test_type.test_type_ranges.filter(
                                        default=True).get().range_low)
                        except models.TestTypeRange.DoesNotExist:
                            cutoff_value = None  # if cutoff is not provided

                        #  if result is provided manually, then use that value
                        result = result_form.cleaned_data.get('result')
                        # 0.00 is treated as false value
                        has_quantitative_result = result_quantitative or result_quantitative == Decimal('0.00')
                        if (result == '.' or not result) and cutoff_value and has_quantitative_result:
                            if positive_above_cutoff:
                                if result_quantitative >= cutoff_value:
                                    result = '+'
                                else:
                                    result = '-'
                            else:  # negative above cutoff
                                if result_quantitative >= cutoff_value:
                                    result = '-'
                                else:
                                    result = '+'

                    if (result, result_info_label, result_info_label_prefix, reference, result_quantitative) != (
                        result_obj.result,
                        result_obj.result_info_label, result_obj.result_info_label_prefix, result_obj.reference,
                        result_obj.result_quantitative):
                        result_obj.result = result
                        result_obj.submitted_datetime = timezone.now()
                        result_obj.result_info_label = result_info_label
                        result_obj.result_info_label_prefix = result_info_label_prefix
                        result_obj.user = request.user
                        result_obj.reference = reference
                        result_obj.test.is_approved = False
                        result_obj.test.initial_report_date = None
                        result_obj.result_quantitative = result_quantitative
                        result_list.append(result_obj)
                    if due_date:
                        test = result_obj.test
                        test.due_date = due_date
                        test_list.append(test)
                else:
                    messages.error(request, 'Quantitative measurements can not contain non numeric characters')
                    no_form_errors = False
            models.Result.objects.bulk_update(result_list,
                                              ['result', 'submitted_datetime', 'result_info_label',
                                               'result_info_label_prefix', 'user', 'reference',
                                               'test', 'result_quantitative'])
            models.Test.objects.bulk_update(test_list,
                                            ['due_date', 'is_approved', 'initial_report_date'])
        else:
            messages.error(request, 'Invalid Datetime format detected.')
            no_form_errors = False
        if user_form.is_valid():
            form_user = user_form.cleaned_data['user']
            if form_user:
                order.test_performed_by[test_method_name] = form_user.id
                order.save()
    return no_form_errors


def get_patient_mrn(patient_id):
    """
    Generates patient MRN
    :param patient_id:
    :return: patient_mrn
    """
    output = []
    message_string = str(patient_id).zfill(8)  # zero pad the sample.id

    for char in message_string:
        if ord(char) > 54:
            output.append(chr(ord(char) - 7))  # to avoid x > 7 going into :...
        else:
            output.append(chr(ord(char) + 3))
    patient_mrn = ''.join(output)
    return patient_mrn


def get_zipcode(address_1):
    """
    Returns a zip code given a valid address
    :param address_1:
    :return: zip_code or ''
    """
    geolocator = Nominatim(user_agent='Dendi')
    geocode = RateLimiter(geolocator.geocode, min_delay_seconds=1)
    try:
        location = geocode(address_1)
    except GeocoderTimedOut:
        location = get_zipcode(address_1)
        print('Failed, retrying')

    zip_code = None
    if location:
        try:
            zip_code = location.raw.get('display_name').split(',')[-2].strip()  # get zip code from location object
        except AttributeError:
            return None
        try:
            zip_code = int(zip_code)
        except ValueError:
            zip_code = ''
    else:  # for imported addresses which have a weird format - example 1757 EAST OHIO PIKE APT 3 AMELIA, OH 45102
        address_components = address_1.split(',')
        if len(address_components) == 2:
            zip_code = address_components[1].strip().split(' ')[1]

    return zip_code


def get_accession_details(request, order, accession_order=False, message=''):
    """

    :param accession_order:
    :param message:
    :param request:
    :param order:
    :return: Accession page details in a dict
    """
    form = forms.AccessionForm()
    provider = order.provider
    account = order.account
    patient = order.patient

    # patient payer information
    primary_patient_payer = models.OrderPatientPayer.objects.filter(order=order, is_primary=True).first()
    secondary_patient_payer = models.OrderPatientPayer.objects.filter(order=order,
                                                                      is_secondary=True).first()
    tertiary_patient_payer = models.OrderPatientPayer.objects.filter(order=order, is_tertiary=True).first()
    wc_patient_payer = models.OrderPatientPayer.objects.filter(order=order, is_workers_comp=True).first()
    # PatientGuarantor object for the order
    patient_guarantor = models.OrderPatientGuarantor.objects.filter(order=order).first()

    prescribed_medication_history = patients_models.PatientPrescribedMedicationHistory.objects.filter(
        order_id=order, provider_id=provider, patient_id=patient). \
        prefetch_related('prescribed_medications').order_by('-created_date').first()
    if prescribed_medication_history:
        prescribed_medications = prescribed_medication_history.prescribed_medications.all()
    else:
        prescribed_medications = None

    icd10_codes = order.icd10_codes.all()
    use_zebra_printer = request.tenant.settings.get('use_zebra_printer', False)
    documents = models.OrderAdditionalDocuments.objects.filter(order=order)

    enable_document_uploading = request.tenant.settings.get('enable_document_uploading', False)
    enable_document_scanning = request.tenant.settings.get('enable_document_scanning', False)
    automatically_print_labels = request.tenant.settings.get('automatically_print_labels', False)

    # Get procedure codes associated with the test_panel_types in this order.
    test_panels = order.test_panels.all().prefetch_related('test_panel_type', 'test_panel_type__procedure_codes')
    procedure_codes_unique = set()
    procedure_codes_queries = [test_panel.test_panel_type.procedure_codes.all() for test_panel in test_panels]
    for query in procedure_codes_queries:
        procedure_codes_unique = procedure_codes_unique.union(query)
    procedure_codes = [procedure_code.code for procedure_code in procedure_codes_unique]
    procedure_codes.sort()

    # Get comment form and comments for this order.
    comment_form = forms.AccessioningViewCommentForm()

    order_comments = order.comments.all().order_by('-created_date')
    for order_comment in order_comments:
        if len(order_comment.history.all()) > 1:
            last_comment = order_comment.history.order_by('-timestamp').first()
            order_comment.last_modified_date = last_comment.timestamp
            actor_id = last_comment.actor_id
            order_comment.last_modified_user = User.all_objects.get(id=actor_id)

    accession_details_dict = {
        'form': form,
        'order': order,
        'provider': provider,
        'account': account,
        'patient': patient,
        'test_panels': test_panels,
        'primary_patient_payer': primary_patient_payer,
        'secondary_patient_payer': secondary_patient_payer,
        'tertiary_patient_payer': tertiary_patient_payer,
        'wc_patient_payer': wc_patient_payer,
        'patient_guarantor': patient_guarantor,
        'prescribed_medications': prescribed_medications,
        'icd10_codes': icd10_codes,
        'procedure_codes': procedure_codes,
        'comment_form': comment_form,
        'order_comments': order_comments,
        'use_zebra_printer': use_zebra_printer,
        'documents': documents,
        'enable_document_uploading': enable_document_uploading,
        'enable_document_scanning': enable_document_scanning,
        'automatically_print_labels': automatically_print_labels
    }
    if accession_order:
        accession_number = order.accession_number
        if not order.accession_number and request.tenant.settings.get('enable_accession_number', False):
            with transaction.atomic():
                accession_number_result = tasks.assign_accession_number.apply_async(
                    args=[order.id, request.tenant.id],
                    queue='identifiers',
                )
                accession_number = accession_number_result.get()
        # while true
        while not accession_number:  # wait for accession number to get assigned
            time.sleep(0.1)
        message += format_html('<br>Accession number: {}', accession_number)
        messages.success(request, message=message)

        accession_details_dict['accession_number'] = accession_number

    return accession_details_dict


def update_test_due_dates(order):
    """
    Update due dates for this order's tests
    :param: order
    :return:
    """
    tests = order.tests.all().prefetch_related('test_type')
    test_list = []
    for test in tests:
        test_type = test.test_type
        if test_type.turnaround_time_unit == 'Days':
            test.due_date = order.received_date + datetime.timedelta(days=test_type.turnaround_time)
        elif test_type.turnaround_time_unit == 'Hours':
            test.due_date = order.received_date + datetime.timedelta(hours=test_type.turnaround_time)
        else:  # Minutes
            test.due_date = order.received_date + datetime.timedelta(minutes=test_type.turnaround_time)
        test_list.append(test)
    models.Test.objects.bulk_update(test_list, ['due_date'])


def check_for_offsite_lab_tests(request, tests, is_order=False, is_container=False):
    """
    Check whether this order has tests performed at a send-out lab and inform the user
    :param is_container:
    :param is_order:
    :param request:
    :param tests:
    :return:
    """
    if tests:
        for test in tests:
            offsite_lab_names = '{}, '.format(test.test_type.offsite_laboratory.name)
        offsite_lab_names = offsite_lab_names[:-2]  # remove last comma and space
        if is_container:
            message = 'This container has tests performed off-site at {}.'.format(offsite_lab_names)
        elif is_order:
            message = 'This order has tests performed off-site at {}.'.format(offsite_lab_names)
        else:  # sample
            message = 'This order has tests performed off-site at {}.'.format(offsite_lab_names).replace('order',
                                                                                                         'sample')

        messages.info(request, message)


def bill_order(tenant, order, preliminary, environment):
    """
    Bill this order for environment=environment
    :param tenant:
    :param order:
    :param preliminary:
    :param environment:
    :return:
    """
    if tenant.name == 'Industry Lab Diagnostic Partners':
        if not order.partial:  # ILDP billing (LabRCM), do not bill partial orders
            generate_billing_hl7_labrcm.send_billing_message_message(tenant.id, order.uuid,
                                                                     preliminary=preliminary,
                                                                     environment=environment)
            order.is_billed = True
    else:  # for the rest of our clients
        generate_billing_hl7.send_billing_message_message(tenant.id, order.uuid, preliminary=preliminary,
                                                          production_environment=environment)
        order.is_billed = True


def accession_order_helper(request, barcode, accession_order):
    """
    Helper function to accession/receive orders when accession order=True
    :param request:
    :param barcode:
    :param accession_order:
    :return: message, context
    """
    order = get_object_or_404(models.Order, code=barcode)
    samples = models.Sample.objects.filter(order_id=order.id).all()
    tests = order.tests.filter(test_type__offsite_laboratory__isnull=False)
    is_order = True
    check_for_offsite_lab_tests(request, tests, is_order)

    order_test_methods = {}
    # update samples received_date for this order
    for sample in samples:
        if sample.received_date:
            pass
        else:
            sample.received_date = timezone.now()
            sample.save()
        test_methods = get_test_method_abbreviated_names(sample)
        order_test_methods.update({sample.code: test_methods})

    num_total_samples = models.Sample.objects.filter(order_id=order.id).count()
    if num_total_samples == 0 or num_total_samples > 1:
        sample_text = 'samples'
    else:
        sample_text = 'sample'

    message = 'Press \'Receive Order\' to confirm receipt'
    already_received = False
    if order.received_date:
        already_received = True
        message = format_html('Order code <a href="{}">{}</a> received as of {}.<br>Accession number: {}',
                              reverse('order_details', args=[order.code]), order.code,
                              convert_utc_to_tenant_timezone(
                                  request,
                                  order.received_date).strftime("%m-%d-%Y, %I:%M %p %Z"),
                              order.accession_number
                              )
        accession_order = False
    elif accession_order:
        order.received_date = timezone.now()
        update_test_due_dates(order)
        order.save(update_fields=['received_date'])
        message = format_html('{} {} received for order code '
                              '<a href="{}">{}</a>.', num_total_samples, sample_text,
                              reverse('order_details', args=[order.code]), order.code)
    # when an order is being accessioned in the confirmed receipt view, it does not have a received date nor is it to
    # be accessioned ergo message = ''

    context = get_accession_details(request, order, accession_order, message)
    context['samples'] = samples
    context['order_test_methods'] = order_test_methods
    context['already_received'] = already_received

    order.refresh_from_db()

    return message, context


def accession_sample_helper(request, barcode, receive_sample):
    barcode = barcode.upper()  # accounting for ZPL Code39 encoding error
    sample = get_object_or_404(models.Sample, barcode=barcode)
    order = get_object_or_404(models.Order, id=sample.order_id)
    tests = order.tests.filter(sample=sample, test_type__offsite_laboratory__isnull=False)
    check_for_offsite_lab_tests(request, tests)

    if not receive_sample and not sample.received_date:  # do not receive sample - confirm receipt views
        message = format_html('Press \'Receive Sample\' to confirm receipt for Order: {}.'.format(order.code))
        accession_order = False
        context = get_accession_details(request, order, accession_order, message)
        context['sample'] = sample
        return message, context
    if sample.received_date:
        already_received = True
    else:
        already_received = False
        sample.received_date = timezone.now()
        sample.save()
    num_received_samples = models.Sample.objects.filter(
        order_id=sample.order_id, received_date__isnull=False).count()
    num_total_samples = models.Sample.objects.filter(order_id=sample.order_id).count()
    if num_received_samples == num_total_samples:  # all samples for order order_id received
        if order.received_date:
            message = format_html('All {}/{} samples received for order code '
                                  '<a href="{}">{}</a> as of {}.<br>Accession number: {}', num_received_samples,
                                  num_total_samples,
                                  reverse('order_details', args=[order.code]),
                                  sample.order.code,
                                  convert_utc_to_tenant_timezone(
                                      request,
                                      order.received_date)
                                  .strftime("%m-%d-%Y, %I:%M %p %Z"), order.accession_number)
            accession_order = False
        else:
            order.received_date = timezone.now()
            order.save(update_fields=['received_date'])
            update_test_due_dates(order)
            message = format_html('All {}/{} samples received for order code '
                                  '<a href="{}">{}</a>.', num_received_samples,
                                  num_total_samples, reverse('order_details', args=[order.code]),
                                  sample.order.code)
            accession_order = True
        context = get_accession_details(request, order, accession_order, message)
        sample.refresh_from_db()
        context['sample'] = sample
        context['already_received'] = already_received
        order.refresh_from_db()
        return message, context
    else:  # not all samples received
        message = format_html('{}/{} samples received for order code '
                              '<a href="{}">{}</a>.', num_received_samples,
                              num_total_samples, reverse('order_details', args=[order.code]),
                              sample.order.code)

        context = get_accession_details(request, order)
        sample.refresh_from_db()
        context['sample'] = sample
        context['already_received'] = already_received
        order.refresh_from_db()
        return message, context


def accession_container_helper(request, barcode, receive_container):
    """
    Helper function to accession/receive orders when accession order=True
    :param request:
    :param barcode:
    :param receive_container:
    :return:
    """
    barcode = barcode.upper()  # accounting for ZPL Code39 encoding error
    container = get_object_or_404(models.Container, barcode=barcode)
    sample = get_object_or_404(models.Sample, id=container.sample_id)
    order = get_object_or_404(models.Order, id=sample.order_id)
    tests = order.tests.filter(sample=sample, test_type__offsite_laboratory__isnull=False)
    is_order = False
    is_container = True
    check_for_offsite_lab_tests(request, tests, is_order, is_container)

    if not receive_container and not container.received_date:  # do not receive container - confirm receipt views
        message = format_html('Press \'Receive Container\' to confirm receipt for order code: {}.'.format(order.code))
        context = get_accession_details(request, order)
        context['sample'] = sample
        context['container'] = container
        return message, context
    if container.received_date:
        already_received = True
    else:
        already_received = False
        container.received_date = timezone.now()
        container.save()
        container.refresh_from_db()

    # check the number of containers for this sample
    num_received_containers = models.Container.objects.filter(
        sample_id=sample.id, received_date__isnull=False).count()
    num_total_containers = models.Container.objects.filter(sample_id=sample.id).count()

    accession_order = False
    if num_received_containers == num_total_containers:  # all containers for this sample have been received
        if sample.received_date:  # not all samples received for the order but all containers for this sample
            # ALREADY received
            message = format_html('All {}/{} containers received for sample ID: {}<br>'
                                  'Order code: '
                                  '<a href="{}">{}</a><br>Accession number: {}<br>Received datetime: {}', num_received_containers,
                                  num_total_containers, sample.code,
                                  reverse('order_details', args=[order.code]),
                                  sample.order.code, sample.order.accession_number,
                                  convert_utc_to_tenant_timezone(
                                      request,
                                      sample.received_date)
                                  .strftime("%m-%d-%Y, %I:%M %p %Z"), )
        else:  # not all samples received for the order but all containers for this sample being currently received
            sample.received_date = timezone.now()
            sample.save(update_fields=['received_date'])
            message = format_html('All {}/{} containers received for sample ID: {} of order code: '
                                  '<a href="{}">{}</a>.', num_received_containers,
                                  num_total_containers, sample.code, reverse('order_details', args=[order.code]),
                                  sample.order.code)
            sample.refresh_from_db()
    else:  # not all containers received
        message = format_html('{}/{} containers received for sample ID: {}<br>'
                              'Order code: '
                              '<a href="{}">{}</a>', num_received_containers,
                              num_total_containers, sample.code, reverse('order_details', args=[order.code]),
                              sample.order.code)
    # check the number of received samples for the order
    num_received_samples = models.Sample.objects.filter(
        order_id=sample.order_id, received_date__isnull=False).count()
    num_total_samples = models.Sample.objects.filter(order_id=sample.order_id).count()

    if num_received_samples == num_total_samples:  # all samples for order order_id received
        if order.received_date:
            message = format_html(
                'All {}/{} containers received for sample ID: {}.<br>All {}/{} samples received for '
                'order code: <a href="{}">{}</a>. <br>Accession number: {}<br>Received datetime: {}', num_received_containers,
                num_total_containers, sample.code, num_received_samples,
                num_total_samples,
                reverse('order_details', args=[order.code]),
                sample.order.code, sample.order.accession_number,
                convert_utc_to_tenant_timezone(
                    request,
                    order.received_date).strftime("%m-%d-%Y, %I:%M %p %Z"), )
            accession_order = False
        else:
            order.received_date = timezone.now()
            order.save(update_fields=['received_date'])
            update_test_due_dates(order)
            message = format_html('All {}/{} containers received for sample ID: {}.<br>All {}/{} samples received '
                                  'for order code: <a href="{}">{}</a>.', num_received_containers,
                                  num_total_containers, sample.code, num_received_samples,
                                  num_total_samples, reverse('order_details', args=[order.code]),
                                  sample.order.code)
            accession_order = True

    context = get_accession_details(request, order, accession_order, message)
    context['sample'] = sample
    context['container'] = container
    context['already_received'] = already_received
    order.refresh_from_db()

    return message, context


def create_presigned_url(s3_client, bucket_name, object_name, expiration=3600):
    """Generate a presigned URL to share an S3 object

    :param s3_client:
    :param bucket_name: string
    :param object_name: string
    :param expiration: Time in seconds for the presigned URL to remain valid
    :return: Presigned URL as string. If error, returns None.
    """
    # Generate a presigned URL for the S3 object
    try:
        response = s3_client.generate_presigned_url('get_object',
                                                    Params={'Bucket': bucket_name,
                                                            'Key': object_name},
                                                    ExpiresIn=expiration)
    except ClientError as e:
        print('Error creating presigned url')
        logging.error(e)
        return None

    # The response contains the presigned URL
    return response


def test_type_range_form_data(form):
    sex = form.cleaned_data.get('sex')
    min_age_days = form.cleaned_data.get('min_age_days')
    min_age_weeks = form.cleaned_data.get('min_age_weeks')
    min_age_months = form.cleaned_data.get('min_age_months')
    min_age_years = form.cleaned_data.get('min_age_years')
    max_age_days = form.cleaned_data.get('max_age_days')
    max_age_weeks = form.cleaned_data.get('max_age_weeks')
    max_age_months = form.cleaned_data.get('max_age_months')
    max_age_years = form.cleaned_data.get('max_age_years')
    accounts = form.cleaned_data.get('accounts')

    # get the total number of days int he max and min
    min_age_total = min_age_days + 7 * min_age_weeks + 30 * min_age_months + 365 * min_age_years
    max_age_total = max_age_days + 7 * max_age_weeks + 30 * max_age_months + 365 * max_age_years

    range_low = form.cleaned_data.get('range_low')
    range_high = form.cleaned_data.get('range_high')
    critical_low = form.cleaned_data.get('critical_low')
    critical_high = form.cleaned_data.get('critical_high')
    report_comment = form.cleaned_data.get('report_comment')
    critical_report_comment = form.cleaned_data.get('critical_report_comment')

    return {'sex': sex,
            'min_age_days': min_age_days,
            'min_age_weeks': min_age_weeks,
            'min_age_months': min_age_months,
            'min_age_years': min_age_years,
            'max_age_days': max_age_days,
            'max_age_weeks': max_age_weeks,
            'max_age_months': max_age_months,
            'max_age_years': max_age_years,
            'min_age_total': min_age_total,
            'max_age_total': max_age_total,
            'range_low': range_low,
            'range_high': range_high,
            'critical_low': critical_low,
            'critical_high': critical_high,
            'report_comment': report_comment,
            'critical_report_comment': critical_report_comment,
            'accounts': accounts}
