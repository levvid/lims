# Generated by Django 2.2.2 on 2019-11-27 00:31

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import main.storage_backends
import uuid


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('orders', '0104_auto_20191105_2016'),
    ]

    operations = [
        migrations.CreateModel(
            name='OrderDocuments',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_date', models.DateTimeField(auto_now_add=True)),
                ('modified_date', models.DateTimeField(auto_now=True)),
                ('is_active', models.BooleanField(default=True)),
                ('uuid', models.UUIDField(db_index=True, default=uuid.uuid4, editable=False)),
                ('document', models.FileField(null=True, storage=main.storage_backends.PrivateMediaStorage(), upload_to='')),
                ('order', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='documents', to='orders.Order')),
                ('user', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
