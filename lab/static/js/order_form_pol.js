$(document).ready(function() {
  // Patient Birth Date Selector
  let patientBirthDate = new Pikaday({
    field: $('#patient-birth-date')[0],
    format: 'YYYY-MM-DD',
    maxDate: moment().toDate(),
    minDate: moment("19200101", "YYYYMMDD"),
    yearRange: 120,
  });

  // Add datepicker and form-control class
  let collectionDate = $("input[id*='collection_date']");
  collectionDate.each(function() {
    new Pikaday({
      field: $(this)[0],
      format: 'YYYY-MM-DD',
      maxDate: moment().toDate()
    });
  });

  // When add sample is clicked, add date picker for last collection date input
  $('div.sample-form > .add-row').first().click(function() {
    let collectionDate = $("input[id*='collection_date']");
    let sampleType = $("select[id*='sample_type']");
    new Pikaday({
      field: collectionDate.last()[0],
      format: 'YYYY-MM-DD',
      maxDate: moment().toDate()
    });
    collectionDate.last().addClass("form-control");
    sampleType.last().addClass("form-control");
  });


  // Google Maps Autocomplete
  const options = {
    // only return results with a precise address
    types: ['address']
  };
  let accountAddress = document.getElementById('account-address');
  let autocomplete1 = new google.maps.places.Autocomplete(accountAddress, options);
  let patientAddress = document.getElementById('patient-address');
  let autocomplete2 = new google.maps.places.Autocomplete(patientAddress, options);


  //////////////////////////////////////////////////////////////////////////////////////
  $('ul').css({'list-style': 'none'});

  // check for existing or new accounts
  if($("input[name='location_type']:checked").val() === 'Existing Location') {
      $('.new-location').hide();
      $('.existing-location').show();
    } else {
      $('.new-location').show();
      $('.existing-location').hide();
    }

  // if new account is selected, then show the new form section
  $('input[name="location_type"]').click(function(){
    if ($(this).is(':checked')) {
      if($(this).val() === 'Existing Location') {
        // if "existing account" is chosen, hide the div and empty out all of the text fields
        $('.new-location').hide().find('input:text').val('');
        $('.existing-location').show();
      } else {
        $('.new-location').show();
        $('.existing-location').hide();
      }
    }
  });

  // check for existing or new provider
  if($("input[name='provider_type']:checked").val() === 'Existing Provider') {
      $('.new-provider').hide();
      $('.existing-provider').show();
    } else if($("input[name='provider_type']:checked").val() === 'New Provider') {
      $('.new-provider').show();
      $('.existing-provider').hide();
    } else {
      $('.new-provider').hide();
      $('.existing-provider').hide();
    }

  // if new account is selected, then show the new form section
  $('input[name="provider_type"]').click(function(){
    if ($(this).is(':checked')) {
      if($(this).val() === 'Existing Provider') {
         // if "existing provider" is chosen, hide the div and empty out all of the text fields
        $('.new-provider').hide().find('input:text').val('');
        $('.existing-provider').show();
      } else if ($(this).val() === 'New Provider') {
        $('.new-provider').show();
        $('.existing-provider').hide();
      } else {
        $('.new-provider').hide().find('input:text').val('');
        $('.existing-provider').hide();
      }
    }
  });

  // check for provider country type
  if($("input[name='provider_country']:checked").val() === 'US') {
      $('.provider-other').hide();
      $('.provider-us').show();
    } else {
      $('.provider-other').show();
      $('.provider-us').hide();
    }

  // if provider country is selected, then show the section
  $('input[name="provider_country"]').click(function(){
    if ($(this).is(':checked')) {
      if($(this).val() === 'US') {
         // if "existing provider" is chosen, hide the div and empty out all of the text fields
        $('.provider-other').hide().find('input:text').val('');
        $('.provider-us').show();
      } else {
        $('.provider-other').show();
        $('.provider-us').hide();
      }
    }
  });

  // check for existing or new patient
  if($("input[name='patient_type']:checked").val() === 'Existing Patient') {
      $('.new-patient').hide();
      $('.existing-patient').show();
    } else {
      $('.new-patient').show();
      $('.existing-patient').hide();
    }

  // if new account is selected, then show the new form section
  $('input[name="patient_type"]').click(function(){
    if ($(this).is(':checked')) {
      if($(this).val() === 'Existing Patient') {
        // if "existing patient" is chosen, hide the div and empty out all of the text fields
        $('.new-patient').hide().find('input:text').val('');
        $('.existing-patient').show();
      } else {
        $('.new-patient').show();
        $('.existing-patient').hide();
      }
    }
  });








});
