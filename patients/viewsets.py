import uuid
from rest_framework import viewsets
from . import serializers
from . import models
from lab_tenant.models import User
from rest_framework.permissions import IsAuthenticated
from lab import serializers as lab_serializers
from accounts import models as accounts_models
from django.core.exceptions import ValidationError


class PatientCreateViewSet(viewsets.ModelViewSet):
    """
    External API endpoint that allows patients to be created
    Required Fields
    first_name : String : First name of patient
    last_name : String : Last name of patient
    sex : String : Sex of patient (M for Male, F for Female, or NB for Non-Binary)
    birth_date: String : "YYYY-MM-DD" Birth date of patient

    Optional Fields
    providers_uuids : [String] : providers associated with patient (at least 1 uuid required)
    middle_initial : String
    suffix : String
    address1 : String
    address2 : String
    zip_code : String
    primary_provider_uuid : String : must be in providers_uuids
    email : String
    phone_number : String
    social_security : String : ###-##-####
    drivers_license : String
    """
    serializer_class = serializers.PatientCreateSerializer
    http_method_names = ['post']
    lookup_field = 'uuid'


class PatientPayerViewSet(viewsets.ModelViewSet):
    """
    External API endpoint that allows patient payer information to be created
    GET:
    optional query params:
    patient_uuid
    ----

    POST:
    Required Fields
    patient_uuid
    payer_type (('Self-Pay', 'Self-Pay'), ('Commercial', 'Commercial'),
                                           ('Medicare', 'Medicare'), ('Medicaid', 'Medicaid'),
                                           ('Workers Comp', 'Workers\' Compensation'),
                                           ('Other', 'Other'))
    Optional
    payer_uuid or payer_name : Use payer_uuid to use existing payer or provide payer_name to create a new Payer object
    member_id:
    group_number
    payer_priority - (('Primary', 'Primary'), ('Secondary', 'Secdonary'), ('Tertiary', Tertiary'))
    subscriber_last_name
    subscriber_first_name
    subscriber_middle_initial
    subscriber_suffix
    subscriber_relationship (('Self', 'Self'), ('Spouse', 'Spouse'),
                                                        ('Parent', 'Parent'), ('Other', 'Other'))
    subscriber_birth_date
    subscriber_address1
    subscriber_address2
    subscriber_zip_code
    subscriber_sex (('M', 'Male'), ('F', 'Female'), ('NB', 'Non-Binary'))
    subscriber_phone_number
    subscriber_social_security
    workers_comp_injury_date
    workers_comp_adjuster
    workers_comp_claim_id
    ----

    PUT:
    To make a PUT request, append UUID of PatientPayer to end of API url
    ex. mylab.dendisoftware.com/api/v1/patient_payers/1e264289-7e8e-40dd-bf50-af169ed1cbf7
    - Only pass attributes you want to update.
    - If subscriber_relationship is 'Self', all subscriber fields will use
    - To update payer_priority (is_primary, is_secondary, etc), pass in a payer_priority field.
    ----
    """
    serializer_class = serializers.PatientPayerSerializer
    http_method_names = ['post', 'get', 'put']
    lookup_field = 'uuid'

    def get_queryset(self):
        patient_payers = models.PatientPayer.objects.all()
        patient_uuid = self.request.query_params.get('patient_uuid')
        if patient_uuid:
            try:
                uuid.UUID(patient_uuid, version=4)
                patient_payers = patient_payers.filter(patient__uuid=patient_uuid)
            except ValidationError:
                patient_payers = patient_payers.none()
            except ValueError:
                patient_payers = patient_payers.none()
        return patient_payers


class PatientGuarantorViewSet(viewsets.ModelViewSet):
    """
    External API endpoint that allows patient guarantor information to be viewed, created, and updated
    GET:
    optional query params:
    patient_uuid
    ----

    POST:
    Required
    patient_uuid
    relationship_to_patient (('Parent', 'Parent'), ('Spouse', 'Spouse'), ('Dependent', 'Dependent'), ('Other', 'Other'))
    first_name
    last_name
    sex (('M', 'Male'), ('F', 'Female'), ('NB', 'Non-Binary'))
    birth_date

    Optional
    middle_initial
    suffix
    address1
    address2
    zip_code
    social_security
    phone_number
    ----

    PUT:
    To make a PUT request, append UUID of PatientGuarantor to end of API url
    ex. mylab.dendisoftware.com/api/v1/patient_guarantors/1e264289-7e8e-40dd-bf50-af169ed1cbf7
    - Only pass attributes you want to update.
    ----
    """
    serializer_class = serializers.PatientGuarantorSerializer
    http_method_names = ['post', 'put', 'get']
    lookup_field = 'uuid'

    def get_queryset(self):
        patient_guarantors = models.PatientGuarantor.objects.all()
        patient_uuid = self.request.query_params.get('patient_uuid')
        if patient_uuid:
            try:
                uuid.UUID(patient_uuid, version=4)
                patient_guarantors = patient_guarantors.filter(patient__uuid=patient_uuid)
            except ValidationError:
                patient_guarantors = patient_guarantors.none()
            except ValueError:
                patient_guarantors = patient_guarantors.none()
        return patient_guarantors


class UserPatientViewSet(viewsets.ModelViewSet):
    """
    External API endpoint that allows Patients relevant for a specific user to be viewed
    user_id Required: Int
    first_name : String : First name of patient
    last_name : String : Last name of patient
    sex : String : Sex of patient (M for Male, F for Female, or NB for Non-Binary)
    birth_date: String : 'YYYY-MM-DD' Birth date of patient
    uuid: String : Patient Unique identifier in Dendi LIS
    alternate_id: String : External ID
    accession_number: String
    sample_code: String
    drivers_license : String
    """
    serializer_class = serializers.UserPatientSerializer
    http_method_names = ['get', 'put', 'post']
    lookup_field = 'code'

    def get_queryset(self):
        # possible query parameters
        user_id = self.request.query_params.get('user_id')
        first_name = self.request.query_params.get('first_name')
        last_name = self.request.query_params.get('last_name')
        sex = self.request.query_params.get('sex')
        birth_date = self.request.query_params.get('birth_date')  # YYYY-MM-DD format
        uuid = self.request.query_params.get('uuid')
        alternate_id = self.request.query_params.get('alternate_id')
        accession_number = self.request.query_params.get('accession_number')
        sample_code = self.request.query_params.get('sample_code')
        drivers_license = self.request.query_params.get('drivers_license')

        patients = models.Patient.objects.all()
        if user_id:
            try:
                user = User.objects.get(id=user_id)
            except User.DoesNotExist:
                return patients.none()
            except ValueError:
                return patients.none()
            if user.user_type == 1:
                pass
            elif user.user_type == 9:  # Account user
                try:
                    account = accounts_models.Account.objects.get(user=user)
                except accounts_models.Account.DoesNotExist:
                    return patients.none()
                patients = patients.filter(providers__accounts=account)
            elif user.user_type == 10:  # Provider user
                try:
                    provider = accounts_models.Provider.objects.get(user=user)
                except accounts_models.Provider.DoesNotExist:
                    return patients.none()
                patients = patients.filter(providers=provider)
            else:
                patients = patients.none()
        else:  # This api must be given a user id, return empty queryset otherwise
            patients = patients.none()

        if first_name:
            patients = patients.filter(user__first_name__istartswith=first_name)
        if last_name:
            patients = patients.filter(user__last_name__istartswith=last_name)
        if sex:
            patients = patients.filter(sex__iexact=sex)
        if birth_date:
            patients = patients.filter(birth_date=birth_date)
        if uuid:
            try:
                patients = patients.get(uuid=uuid)
            except ValidationError:
                patients = patients.none()
        if alternate_id:
            patients = patients.filter(alternate_id__istartswith=alternate_id)
        if accession_number:
            patients = patients.filter(orders__accession_number__istartswith=accession_number)
        if sample_code:
            patients = patients.filter(orders__samples__code__istartswith=sample_code)
        if drivers_license:
            patients = patients.filter(drivers_license__istartswith=drivers_license)
        return patients


class PayerViewSet(viewsets.ModelViewSet):
    """
    External API endpoint that allows Payers to be Created and viewed
    Required Fields:
    name
    payer_type: choices=(('Commercial', 'Commercial'), ('Medicare', 'Medicare'), ('Medicaid', 'Medicaid'),
        ('Workers Comp', 'Workers\' Compensation'), ('Other', 'Other'))
    Optional Fields:
    payer_code
    alternate_id
    abbreviated_name
    address_1
    address_2
    city
    state
    zip_code
    phone_number
    """
    serializer_class = serializers.PayerSerializer
    http_method_names = ['get', 'post']
    lookup_field = 'uuid'
    queryset = models.Payer.objects.all()


class PayerAutocompleteViewSet(viewsets.ModelViewSet):
    """
    External API endpoint that allows Payers Autocomplete
    q Optional
    """
    serializer_class = serializers.PayerAutocompleteSerializer
    http_method_names = ['get']
    lookup_field = 'uuid'

    def get_queryset(self):
        payers = models.Payer.objects.all()
        query = self.request.query_params.get('q')
        if query:
            payers = payers.filter(name__istartswith=query)
        return payers
