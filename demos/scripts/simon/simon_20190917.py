import django
import logging
import os
import sys
from django.db import connection, transaction
from demos.scripts.ildp_setup_20190722 import initialize_historical_patient_results_from_csvs
from tenant_schemas.utils import tenant_context

os.environ['DJANGO_SETTINGS_MODULE'] = 'main.settings'
django.setup()

from clients import models as client_models

"""
Script used to work on task:
Import historical patient results
"""

HISTORICAL_PATIENT_RESULTS_CSV_URLS = [
    'https://dendi-lis.s3.amazonaws.com/client_data/ildp/August+2018.csv',
    'https://dendi-lis.s3.amazonaws.com/client_data/ildp/September+2018.csv',
    'https://dendi-lis.s3.amazonaws.com/client_data/ildp/October+2018.csv',
    'https://dendi-lis.s3.amazonaws.com/client_data/ildp/November+2018.csv',
    'https://dendi-lis.s3.amazonaws.com/client_data/ildp/December+2018.csv',
    'https://dendi-lis.s3.amazonaws.com/client_data/ildp/February+2019.csv',
    'https://dendi-lis.s3.amazonaws.com/client_data/ildp/January+2019.csv',
    'https://dendi-lis.s3.amazonaws.com/client_data/ildp/March+2019.csv',
    'https://dendi-lis.s3.amazonaws.com/client_data/ildp/April+2019.csv',
    'https://dendi-lis.s3.amazonaws.com/client_data/ildp/May+2019.csv',
    'https://dendi-lis.s3.amazonaws.com/client_data/ildp/June+2019.csv',
    'https://dendi-lis.s3.amazonaws.com/client_data/ildp/July+2019.csv',
]

HISTORICAL_PATIENT_RESULTS_CSV_URL = [
    'https://dendi-lis.s3.amazonaws.com/client_data/ildp/August+2018.csv',
]


def main():
    import timeit
    t0 = timeit.default_timer()
    schema_name = 'ildpa'
    tenant = client_models.Client.objects.get(schema_name=schema_name)
    print("schema: {}".format(schema_name))
    with tenant_context(tenant):
        for csv_url in HISTORICAL_PATIENT_RESULTS_CSV_URLS:
            initialize_historical_patient_results_from_csvs([csv_url])
        #initialize_historical_patient_results_from_csvs(HISTORICAL_PATIENT_RESULTS_CSV_URLS)
    t1 = timeit.default_timer()
    print("{} seconds".format(t1-t0))


if __name__ == '__main__':
    main()
