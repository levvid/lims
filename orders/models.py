import boto3
import uuid as uuid_lib

from django.conf import settings
from django.db import models
from django.db.models.functions import Lower
from django.urls import reverse
from django.utils import timezone
from django.contrib.postgres.fields import ArrayField, CICharField

from jsonfield import JSONField

from main.storage_backends import PrivateMediaStorage, IntegrationMediaStorage

from lab_tenant.models import AuditLogModel, User
from auditlog.registry import auditlog
from auditlog.models import AuditlogHistoryField

from main.settings import ALLOWED_MAX_DIGITS, ALLOWED_DECIMAL_PLACES


class Counter(AuditLogModel):
    name = models.CharField(max_length=32)
    value = models.CharField(max_length=32, null=True)
    int_value = models.BigIntegerField(default=0)


class Order(AuditLogModel):
    """
    Orders are made up of tests
    """
    patient = models.ForeignKey('patients.Patient', related_name='orders',
                                on_delete=models.CASCADE, blank=True, null=True)
    provider = models.ForeignKey('accounts.Provider', related_name='orders',
                                 on_delete=models.CASCADE, blank=True, null=True)
    account = models.ForeignKey('accounts.Account', related_name='orders',
                                on_delete=models.CASCADE, blank=True, null=True)
    icd10_codes = models.ManyToManyField('codes.ICD10CM')

    uuid = models.UUIDField(
        db_index=True,
        default=uuid_lib.uuid4,
        editable=False
    )
    # alternate id from EMR placed orders
    alternate_id = models.CharField(max_length=32, blank=True)
    code = models.CharField(max_length=255, null=True)
    accession_number = models.CharField(max_length=16, null=True, unique=True)
    research_consent = models.BooleanField(default=False)
    patient_result_request = models.BooleanField(default=False)
    notes = models.TextField(blank=True)
    # dates
    submitted_date = models.DateTimeField(default=timezone.now)
    received_date = models.DateTimeField(null=True, db_index=True)
    offsite_lab_received_date = models.DateTimeField(null=True)
    completed_date = models.DateTimeField(null=True)
    report_date = models.DateTimeField(null=True)
    # status booleans
    submitted = models.BooleanField(default=False)
    completed = models.BooleanField(default=False)
    partial = models.BooleanField(default=True)
    reported = models.BooleanField(default=False)
    reporting_error = models.BooleanField(default=False)
    creating_report = models.BooleanField(default=False)
    creating_preliminary_report = models.BooleanField(default=False)
    allow_comments = models.BooleanField('allow comments', default=True)
    # for provider inbox --> use null=True because some records will be empty
    is_archived_by_provider = models.BooleanField(null=True, default=False)
    # other fields
    test_performed_by = JSONField(default={})
    requisition_form = models.FileField(storage=PrivateMediaStorage(), null=True)
    is_billed = models.BooleanField(default=False)

    @property
    def expected_revenue(self):
        return sum([x.expected_revenue for x in self.test_panels.all() if x.expected_revenue])

    @property
    def received_revenue(self):
        return sum([x.received_revenue for x in self.test_panels.all() if x.received_revenue])

    @property
    def status(self):
        if not self.is_active:
            return 'Deleted'
        elif not self.submitted:
            return 'Creating'
        elif self.submitted and not self.received_date:
            return 'Unreceived'
        elif self.submitted and self.received_date and not self.completed:
            return 'Pending Results'
        elif self.completed and not self.reported:
            return 'Pending Test Approval'
        elif self.completed and self.reported and self.creating_report:
            return 'Reporting'
        elif self.completed and self.reported and not self.partial:
            return 'Reported'
        elif self.completed and self.reported and self.partial:
            return 'Reported (Partial)'
        elif self.completed and self.reporting_error:
            return 'Reporting Error'

    @property
    def has_unresolved_issues(self):
        if self.issues.filter(is_resolved=False):
            return True

    @property
    def has_unresolved_public_issues(self):
        if self.issues.filter(is_resolved=False, is_public=True):
            return True

    @property
    def has_public_issues(self):
        if self.issues.filter(is_public=True):
            return True

    @property
    def is_valid(self):
        """
        returns True if all Specimen Validity Tests associated to this Sample have results of Within Range
        :return: bool
        """
        specimen_validity_tests = self.tests. \
            filter(test_type__test_method__name='Specimen Validity'). \
            prefetch_related('result',
                             'sample').all()
        results = [x.result.get().result for x in specimen_validity_tests]
        if list(set(results)) == ['~']:
            return True
        else:
            return False

    @property
    def validity_string(self):
        """
        :return: String
        """
        specimen_validity_tests = self.tests. \
            filter(test_type__test_method__name='Specimen Validity'). \
            prefetch_related('result',
                             'sample').all()
        results = list(set([x.result.get().result for x in specimen_validity_tests]))
        if results == [None]:
            return 'Pending Results'
        elif '<' in results:
            return 'Outside normal range'
        elif '>' in results:
            return 'Outside normal range'
        elif results == ['~']:
            return 'Within normal range'
        elif not results:
            return 'N/A'
        else:
            return 'Pending Results'

    class Meta:
        db_table = 'order'

    def __str__(self):
        return "{} - {}".format(self.code, self.patient)


class OrderComment(AuditLogModel):
    """

    """
    order = models.ForeignKey('Order', related_name='comments', on_delete=models.CASCADE)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, null=True)
    comment = models.TextField(blank=True)
    history = AuditlogHistoryField()


class OrderTestPanelComment(AuditLogModel):
    order = models.ForeignKey('Order', null=True,
                              related_name='order_test_panel_comments', on_delete=models.SET_NULL)
    test_panel = models.ForeignKey('TestPanel', null=True,
                                   related_name='order_test_panel_comments', on_delete=models.SET_NULL)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, null=True)
    comment = models.TextField(blank=True)
    # also determines whether the issue may be displayed on report
    is_public = models.BooleanField(default=False)


class OrderTestComment(AuditLogModel):
    order = models.ForeignKey('Order', null=True,
                              related_name='order_test_comments', on_delete=models.SET_NULL)
    test = models.ForeignKey('Test', null=True, related_name='order_test_comments', on_delete=models.SET_NULL)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, null=True)
    comment = models.TextField(blank=True)
    # also determines whether the issue may be displayed on report
    is_public = models.BooleanField(default=False)


class OrderIssue(AuditLogModel):
    order = models.ForeignKey('Order', related_name='issues', on_delete=models.CASCADE)
    issue_type = models.CharField(max_length=32, choices=(('test_not_performed', 'Test Not Performed'),
                                                          ('demographics', 'Patient Demographics'),
                                                          ('billing', 'Billing/Coding'),
                                                          ('medication', 'Medication'),
                                                          ('other', 'Other')))
    description = models.TextField(blank=True)
    is_resolved = models.BooleanField(default=False)
    # also determines whether the issue may be displayed on report
    is_public = models.BooleanField(default=False)
    resolved_datetime = models.DateTimeField(null=True)
    samples = models.ManyToManyField('Sample', related_name='order_issues')
    issue_creator = models.ForeignKey(User, null=True, on_delete=models.SET_NULL, related_name='created_order_issues')
    issue_resolver = models.ForeignKey(User, null=True, on_delete=models.SET_NULL, related_name='resolved_order_issues')
    resolver_comment = models.TextField(blank=True)


class OrderPatientPayer(AuditLogModel):
    """
    A patient's payers at the time of order creation. This is necessary because there can only be one
    primary/secondary/tertiary PatientPayer object for each patient. This means that if you update the
    primary/secondary/tertiary PatientPayer object for a given patient, the patient payer info for all orders associated
    with that patient historically will also change retroactively
    """
    patient = models.ForeignKey('patients.Patient', related_name='order_patient_payers', null=True,
                                on_delete=models.SET_NULL)
    order = models.ForeignKey('Order', related_name='order_patient_payers', null=True, on_delete=models.SET_NULL)

    # required field
    payer_type = models.CharField(max_length=64,
                                  choices=(('Self-Pay', 'Self-Pay'), ('Commercial', 'Commercial'),
                                           ('Medicare', 'Medicare'), ('Medicaid', 'Medicaid'),
                                           ('Workers Comp', 'Workers\' Compensation'),
                                           ('Other', 'Other')), default='Commercial')

    # status fields
    is_primary = models.BooleanField(default=False)
    is_secondary = models.BooleanField(default=False)
    is_tertiary = models.BooleanField(default=False)
    is_workers_comp = models.BooleanField(default=False)

    payer = models.ForeignKey('patients.Payer', related_name='order_patient_payers', null=True,
                              on_delete=models.SET_NULL)

    member_id = models.CharField(max_length=256, blank=True)
    group_number = models.CharField(max_length=256, blank=True)

    # subscriber name provided if different from patient
    subscriber_last_name = models.CharField(max_length=255, blank=True)
    subscriber_first_name = models.CharField(max_length=255, blank=True)
    subscriber_middle_initial = models.CharField(max_length=10, blank=True)
    # Jr, II, etc.
    subscriber_suffix = models.CharField(max_length=255, blank=True)
    subscriber_birth_date = models.DateField(null=True, blank=True)
    subscriber_address1 = models.CharField(max_length=255, blank=True)
    subscriber_address2 = models.CharField(max_length=255, blank=True)
    subscriber_zip_code = models.CharField(max_length=255, blank=True)
    subscriber_sex = models.CharField(max_length=10, blank=True,
                                      choices=(('M', 'Male'), ('F', 'Female'), ('NB', 'Non-Binary')))
    subscriber_phone_number = models.CharField(max_length=255, blank=True)
    # social security numbers are in the format: AAA-GG-SSSS
    subscriber_social_security = models.CharField(max_length=11, blank=True, null=True, default=None)
    # subscriber relationship to patient
    subscriber_relationship = models.CharField(max_length=64, blank=True,
                                               choices=(('Self', 'Self'), ('Spouse', 'Spouse'),
                                                        ('Parent', 'Parent'), ('Other', 'Other')), default='Self')

    # for workers' comp use only
    workers_comp_injury_date = models.DateTimeField(null=True)
    workers_comp_adjuster = models.CharField(max_length=255, blank=True)
    workers_comp_claim_id = models.CharField(max_length=64, blank=True)

    @property
    def subscriber_name(self):
        # only applies if subscriber relationship isn't "Self"
        if self.subscriber_relationship != 'Self':
            if self.subscriber_middle_initial:
                subscriber_name = '{} {}. {} {}'.format(self.subscriber_first_name, self.subscriber_middle_initial,
                                                        self.subscriber_last_name, self.subscriber_suffix)
            else:
                subscriber_name = '{} {} {}'.format(self.subscriber_first_name,
                                                    self.subscriber_last_name, self.subscriber_suffix)
            return subscriber_name

    def __str__(self):
        if self.payer:
            return "{}: {}".format(self.payer.name, self.member_id)
        else:
            return "{}: {}".format(self.payer_type, self.member_id)


class OrderPatientGuarantor(AuditLogModel):
    alternate_id = models.CharField(max_length=32, blank=True)
    # cascade delete user if guarantor is deleted
    # a user can be associated with multiple OrderPatientGuarantor objects
    user = models.ForeignKey(User, null=True, on_delete=models.SET_NULL)
    patient = models.ForeignKey('patients.Patient', related_name='order_guarantors',
                                null=True, on_delete=models.SET_NULL)
    order = models.ForeignKey('Order', related_name='order_guarantors', null=True, on_delete=models.SET_NULL)
    middle_initial = models.CharField(max_length=10, blank=True)
    # Jr, II, etc.
    suffix = models.CharField(max_length=255, blank=True)
    birth_date = models.DateField(null=True, blank=True)
    address1 = models.CharField(max_length=255, blank=True)
    address2 = models.CharField(max_length=255, blank=True)
    zip_code = models.CharField(max_length=128, blank=True)
    sex = models.CharField(max_length=10, blank=True,
                           choices=(('M', 'Male'), ('F', 'Female'), ('NB', 'Non-Binary')))
    phone_number = models.CharField(max_length=255, blank=True)
    # social security numbers are in the format: AAA-GG-SSSS
    social_security = models.CharField(max_length=11, blank=True, default=None)
    relationship_to_patient = models.CharField(max_length=64, blank=True,
                                               choices=(('Parent', 'Parent'), ('Spouse', 'Spouse'),
                                                        ('Dependent', 'Dependent'), ('Other', 'Other')))

    def __str__(self):
        return '{} {}'.format(self.user.first_name, self.user.last_name)


class Sample(AuditLogModel):
    """

    """
    order = models.ForeignKey('Order', related_name='samples', on_delete=models.CASCADE, null=True)
    uuid = models.UUIDField(
        db_index=True,
        default=uuid_lib.uuid4,
        editable=False
    )
    code = models.CharField(max_length=255, null=True)
    clia_sample_type = models.ForeignKey('lab.CLIASampleType', related_name='clia_samples', on_delete=models.CASCADE,
                                         null=True)
    draw_type = models.CharField(max_length=32, blank=True)
    collection_date = models.DateTimeField(null=True)
    received_date = models.DateTimeField(null=True)
    offsite_lab_received_date = models.DateTimeField(null=True)
    barcode = models.CharField(max_length=128, null=True, unique=True, blank=True)

    collection_temperature = models.DecimalField(decimal_places=2, max_digits=5, null=True)
    collection_temperature_unit = models.CharField(max_length=2, blank=True)
    collection_temperature_is_within_range = models.BooleanField(null=True, default=None)
    collection_user = models.ForeignKey(
        settings.AUTH_USER_MODEL, on_delete=models.CASCADE, null=True)

    # location

    class Meta:
        db_table = 'sample'

    def __str__(self):
        try:
            return "<{} {}>".format(self.clia_sample_type.name, self.code)
        except AttributeError:
            return "<{}>".format(self.code)

    @property
    def is_valid(self):
        """
        returns True if all Specimen Validity Tests associated to this Sample have results of Within Range
        :return: bool
        """
        specimen_validity_tests = self.order.tests. \
            filter(sample=self, test_type__test_method__name='Specimen Validity'). \
            prefetch_related('result',
                             'sample').all()
        results = [x.result.get().result for x in specimen_validity_tests]
        if list(set(results)) == ['~']:
            return True
        else:
            return False

    @property
    def validity_string(self):
        """
        :return: String
        """
        specimen_validity_tests = self.order.tests. \
            filter(sample=self, test_type__test_method__name='Specimen Validity'). \
            prefetch_related('result',
                             'sample').all()
        results = list(set([x.result.get().result for x in specimen_validity_tests]))
        if results == [None]:
            return 'Pending Results'
        elif '<' in results:
            return 'Outside normal range'
        elif '>' in results:
            return 'Outside normal range'
        elif results == ['~']:
            return 'Within normal range'
        elif not results:
            return 'N/A'
        else:
            return 'Pending Results'


class Control(AuditLogModel):
    """Control sample"""
    name = models.CharField(max_length=128)
    code = models.CharField(max_length=128, unique=True)
    is_single_analyte_control = models.BooleanField(default=False)
    instrument_integrations = models.ManyToManyField('lab_tenant.InstrumentIntegration')
    lot_number = models.CharField(max_length=128, blank=True)
    expiration_date = models.DateField(null=True, blank=True)


class ControlKnownValue(AuditLogModel):
    """
    Defines known analyte concentrations of a sample
    """
    control = models.ForeignKey(Control, related_name='known_values', on_delete=models.SET_NULL, null=True)
    test_target = models.ForeignKey('lab.TestTarget', related_name='known_values', on_delete=models.SET_NULL, null=True)
    concentration = models.DecimalField(blank=True, max_digits=ALLOWED_MAX_DIGITS, decimal_places=ALLOWED_DECIMAL_PLACES, null=True)
    unit = models.CharField(max_length=64, blank=True, default='')


class Report(AuditLogModel):
    """
    Object representing result report
    """
    order = models.ForeignKey('Order', related_name='reports', on_delete=models.CASCADE)
    # S3 Key of report pdf
    pdf_file = models.FileField(storage=PrivateMediaStorage(), null=True)
    amended = models.BooleanField(default=False)
    preliminary = models.BooleanField(default=False)
    approved_by = models.ForeignKey(User, null=True, on_delete=models.SET_NULL)

    class Meta:
        db_table = 'report'


class Payment(AuditLogModel):
    """

    """
    order = models.ForeignKey('Order', related_name='payments', on_delete=models.CASCADE)
    amount = models.DecimalField(max_digits=5, decimal_places=2, null=True)

    class Meta:
        db_table = 'payment'


class TestPanelType(AuditLogModel):
    """
    Predefined TestPanel. AKA Laboratory's offerings
    """
    uuid = models.UUIDField(
        db_index=True,
        default=uuid_lib.uuid4,
        editable=False
    )
    alternate_id = models.CharField(max_length=128, null=True)
    name = models.CharField(max_length=255, blank=True)
    # a test panel should never cost more than $99k
    cost = models.DecimalField(max_digits=7, decimal_places=2, null=True)
    price = models.DecimalField(max_digits=7, decimal_places=2, null=True)
    test_types = models.ManyToManyField('TestType')
    test_panel_category = models.ForeignKey('TestPanelCategory', related_name='test_panel_types',
                                            on_delete=models.CASCADE,
                                            null=True, blank=True)
    notes = models.TextField(blank=True)
    # Order of test types on test requisition form
    category_order = models.IntegerField(null=True, blank=True)
    research_consent_required = models.BooleanField(default=False)
    is_exclusive = models.BooleanField(default=False)
    lab_departments = models.ManyToManyField('lab_tenant.LabDepartment', related_name='test_panel_types')
    exclusive_providers = models.ManyToManyField('accounts.Provider')
    exclusive_accounts = models.ManyToManyField('accounts.Account')
    procedure_codes = models.ManyToManyField('codes.ProcedureCode')
    in_house_lab_locations = models.ManyToManyField('lab_tenant.InHouseLabLocation')
    offsite_laboratory = models.ForeignKey('lab.OffsiteLaboratory', related_name='test_panel_types',
                                           on_delete=models.SET_NULL,
                                           null=True)

    class Meta:
        db_table = 'test_panel_type'
        ordering = [Lower('name')]

    @property
    def active_test_types(self):
        return self.test_types.all()

    def __str__(self):
        return self.name


class TestPanelTypeAlias(AuditLogModel):
    test_panel_type = models.ForeignKey('TestPanelType', related_name='test_panel_type_aliases',
                                        on_delete=models.SET_NULL, null=True)
    name = CICharField(max_length=255, blank=True)

    def __str__(self):
        return self.name


class TestPanelTypeComment(AuditLogModel):
    test_panel_type = models.ForeignKey('TestPanelType', related_name='test_panel_type_comments', null=True, on_delete=models.SET_NULL)
    comment = models.TextField(blank=True)
    # also determines whether the issue may be displayed on report
    is_public = models.BooleanField(default=False)


class SuperTestPanelType(AuditLogModel):
    """
    Category of TestPanel objects
    """
    name = models.CharField(max_length=255, blank=True)
    test_panel_types = models.ManyToManyField('TestPanelType', related_name='super_test_panel_types')
    is_exclusive = models.BooleanField(default=False)
    exclusive_providers = models.ManyToManyField('accounts.Provider')
    exclusive_accounts = models.ManyToManyField('accounts.Account')


class TestPanelCategory(AuditLogModel):
    """
    User defined categories
    think of as test panel menu categories
    """
    name = models.CharField(max_length=128)

    class Meta:
        db_table = 'test_panel_type_category'

    def __str__(self):
        return self.name


class TestPanel(AuditLogModel):
    """
    An individual object of the TestPanelType
    A order-able TestPanel offering.
    Many TestPanels can be done on one sample
    TestPanels are made up of Tests
    """
    uuid = models.UUIDField(
        db_index=True,
        default=uuid_lib.uuid4,
        editable=False
    )
    order = models.ForeignKey('Order', related_name='test_panels', on_delete=models.CASCADE)
    test_panel_type = models.ForeignKey(
        TestPanelType, related_name='test_panels', on_delete=models.CASCADE)

    expected_revenue = models.DecimalField(max_digits=5, decimal_places=2, null=True)
    adjustment = models.DecimalField(max_digits=5, decimal_places=2, null=True)
    received_revenue = models.DecimalField(max_digits=5, decimal_places=2, null=True)
    batch_date = models.DateTimeField(null=True, blank=True)

    class Meta:
        db_table = 'test_panel'

    def __str__(self):
        return "<TestPanel {} for Order {}>".format(self.test_panel_type.name, self.order.code)


class ContainerType(AuditLogModel):
    name = models.CharField(max_length=255, unique=True)
    # in milliliters
    volume = models.DecimalField(max_digits=18, decimal_places=9, null=True)

    def __str__(self):
        return self.name


class Container(AuditLogModel):
    """
    A Sample may be comprised of 0, 1, or more containers.
    """
    container_type = models.ForeignKey(ContainerType, related_name='containers', on_delete=models.CASCADE)
    code = models.CharField(max_length=255, null=True)
    order = models.ForeignKey('Order', related_name='containers', on_delete=models.CASCADE)
    sample = models.ForeignKey('Sample', related_name='containers', on_delete=models.CASCADE)
    barcode = models.CharField(max_length=128, null=True, unique=True, blank=True)
    collection_date = models.DateTimeField(null=True)
    received_date = models.DateTimeField(null=True)
    offsite_lab_received_date = models.DateTimeField(null=True)

    def __str__(self):
        return "<Container: {}>".format(self.container_type)


class Test(AuditLogModel):
    """
    An individual object of the TestType
    Each Test has a result
    Each Test is on one sample

    """
    uuid = models.UUIDField(
        db_index=True,
        default=uuid_lib.uuid4,
        editable=False
    )
    order = models.ForeignKey('Order', related_name='tests', on_delete=models.CASCADE)
    sample = models.ForeignKey('Sample', related_name='tests', on_delete=models.CASCADE)
    test_panels = models.ManyToManyField('TestPanel', related_name='tests')
    test_type = models.ForeignKey('TestType', related_name='tests',
                                  on_delete=models.CASCADE, null=True)
    completed = models.BooleanField(default=False)
    is_approved = models.BooleanField(default=False)
    approved_user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, null=True)
    due_date = models.DateTimeField(null=True, blank=True)
    approved_date = models.DateTimeField(null=True, blank=True)
    initial_report_date = models.DateTimeField(null=True, blank=True)
    batch_date = models.DateTimeField(null=True, blank=True)

    class Meta:
        db_table = 'test'

    def __str__(self):
        return "<Test {} on Sample {}>".format(self.test_type.name, self.sample.code)


class TestType(AuditLogModel):
    """
    Predefined Test.
    """
    uuid = models.UUIDField(
        db_index=True,
        default=uuid_lib.uuid4,
        editable=False
    )
    alternate_id = models.CharField(max_length=128, null=True)
    name = models.CharField(max_length=512, null=True)
    test_method = models.ForeignKey(
        'lab.TestMethod', related_name='test_types', on_delete=models.SET_NULL, null=True)
    test_target = models.ForeignKey(
        'lab.TestTarget', related_name='test_types', on_delete=models.SET_NULL, null=True)
    icd10_codes = models.ManyToManyField('codes.ICD10CM')
    isotype = models.CharField(max_length=64, blank=True)
    band = models.CharField(max_length=64, blank=True)
    manufacturer = models.CharField(max_length=64, blank=True)
    result_type = models.CharField(max_length=32, choices=(('Quantitative', 'Quantitative'),
                                                           ('Qualitative', 'Qualitative')), default='Qualitative')
    turnaround_time = models.IntegerField(default=14, null=True, blank=True)
    turnaround_time_unit = models.CharField(default='Days',
                                            max_length=8,
                                            choices=(('Days', 'Days'),
                                                     ('Hours', 'Hours'),
                                                     ('Minutes', 'Minutes')))
    is_research_only = models.BooleanField(default=False)
    offsite_laboratory = models.ForeignKey('lab.OffsiteLaboratory', related_name='test_types',
                                           on_delete=models.SET_NULL,
                                           null=True)
    offsite_test_id = models.CharField(max_length=255, blank=True)
    is_approval_required = models.BooleanField(default=False)
    transaction_code = models.CharField(max_length=32, blank=True)
    unit = models.CharField(max_length=64, blank=True)
    # number of decimal places to round by
    use_round_figures = models.BooleanField(default=False)
    round_figures = models.IntegerField(default=2)
    reference_prefix = models.CharField(max_length=32, blank=True)

    required_samples = JSONField(null=True, blank=True)

    complexity = models.CharField(max_length=128, blank=True)
    notes = models.TextField(blank=True)
    specialty = models.ForeignKey('lab.CLIATestTypeSpecialty', related_name='test_types', on_delete=models.SET_NULL,
                                  null=True)
    # in milliliters
    min_volume = models.DecimalField(max_digits=18, decimal_places=9, null=True)
    container_type = models.ForeignKey(ContainerType, related_name='test_types', on_delete=models.SET_NULL, null=True)
    is_internal = models.BooleanField(default=False)
    loinc_code = models.CharField(max_length=16, blank=True)

    class Meta:
        db_table = 'test_type'
        ordering = [Lower('name')]

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        # Qualitative results shouldn't have units of measurement
        if self.result_type == 'Qualitative':
            self.unit = ''
        super().save(*args, **kwargs)  # Call the "real" save() method.

    def expected_range_string(self):
        test_type_range = self.test_type_ranges.filter(default=True).get()
        string = "{} - {} {}".format(round(test_type_range.range_low, self.round_figures),
                                     round(test_type_range.range_high, self.round_figures),
                                     self.unit)
        return string


class TestTypeAlias(AuditLogModel):
    test_type = models.ForeignKey('TestType', related_name='test_type_aliases', on_delete=models.SET_NULL, null=True)
    name = CICharField(max_length=255, blank=True)

    def __str__(self):
        return self.name


class TestTypeCategory(AuditLogModel):
    alternate_id = models.CharField(max_length=128, null=True)
    name = models.CharField(max_length=512, null=True)
    test_types = models.ManyToManyField(TestType, related_name='test_type_categories')

    def __str__(self):
        return self.name


class InstrumentCalibration(AuditLogModel):
    instrument_integration = models.ForeignKey('lab_tenant.InstrumentIntegration',
                                               related_name='instrument_calibrations',
                                               on_delete=models.CASCADE)
    test_type = models.ForeignKey(TestType, related_name='instrument_calibrations', on_delete=models.CASCADE)
    target_name = models.CharField(max_length=64, blank=True)
    cutoff_value = models.DecimalField(max_digits=ALLOWED_MAX_DIGITS, decimal_places=ALLOWED_DECIMAL_PLACES, null=True)
    use_test_cutoff_values = models.BooleanField(default=False)


class TestTypeRange(AuditLogModel):
    """
    Reference ranges that are tied to test types
    """
    uuid = models.UUIDField(
        db_index=True,
        default=uuid_lib.uuid4,
        editable=False
    )
    test_type = models.ForeignKey('TestType', related_name='test_type_ranges', on_delete=models.CASCADE,
                                  null=True)
    accounts = models.ManyToManyField('accounts.Account', related_name='test_type_ranges')
    # add CLIA generic tests from lab app
    clia_test_type = models.ForeignKey('lab.CLIATestType', related_name='clia_test_type_ranges',
                                       on_delete=models.CASCADE,
                                       null=True)

    # There are default reference ranges which are always used (marked by default flag),
    # except when a patient meets the conditions (age and/or sex) for using a non-default reference range,
    # in which case the non-default reference range will be used.
    # Default reference ranges apply to ages (0-99) and are assigned to no particular gender (hence non-binary)
    default = models.BooleanField(default=False)

    range_low = models.DecimalField(max_digits=ALLOWED_MAX_DIGITS, decimal_places=ALLOWED_DECIMAL_PLACES, null=True)
    range_high = models.DecimalField(max_digits=ALLOWED_MAX_DIGITS, decimal_places=ALLOWED_DECIMAL_PLACES, null=True)
    # critical (panic) values
    # critical_low must be lower than range_low, and critical_high must be higher than range_high
    critical_low = models.DecimalField(max_digits=ALLOWED_MAX_DIGITS, decimal_places=ALLOWED_DECIMAL_PLACES, null=True)
    critical_high = models.DecimalField(max_digits=ALLOWED_MAX_DIGITS, decimal_places=ALLOWED_DECIMAL_PLACES, null=True)

    # every reference range must specify a gender - choosing 'Non-Binary' will apply to all genders
    sex = models.CharField(max_length=10, choices=(('M', 'Male'), ('F', 'Female'), ('NB', 'Non-Binary')),
                           default='NB')
    # if the age isn't chosen, it defaults to 0 for all min and max ages (days, weeks, months, years)
    # year = 365 days, month = 30 days, and week = 7 days
    min_age_total = models.IntegerField(default=0)  # total for min age in days
    max_age_total = models.IntegerField(default=0)  # total for max age in days
    min_age_days = models.IntegerField(default=0)
    min_age_months = models.IntegerField(default=0)
    min_age_weeks = models.IntegerField(default=0)
    min_age_years = models.IntegerField(default=0)
    max_age_days = models.IntegerField(default=0)
    max_age_weeks = models.IntegerField(default=0)
    max_age_months = models.IntegerField(default=0)
    max_age_years = models.IntegerField(default=0)

    # comment that goes on the report
    report_comment = models.TextField(blank=True)
    # if critical value is reached, include comment on report
    critical_report_comment = models.TextField(blank=True)

    def __str__(self):
        if self.default:
            if self.test_type:
                return 'Default Range ({})'.format(self.test_type)
            else:
                return 'Default Range ({})'.format(self.clia_test_type)
        else:
            if self.test_type:
                return 'Additional Range ({})'.format(self.test_type)
            else:
                return 'Additional Range ({})'.format(self.clia_test_type)


class TestTypeReflex(AuditLogModel):
    """
    Note: reflex tests are ordered conditionally based on results and rules defined here.
    They are independent from reference ranges altogether.

    """
    test_type = models.ForeignKey(
        'TestType', related_name='test_type_reflexes', on_delete=models.CASCADE, null=True)
    reflex_test_type = models.ForeignKey(
        'TestType', related_name='test_type_reflexed', on_delete=models.CASCADE, null=True)
    # can be positive/negative (reactive/non reactive), equivocal, indeterminate
    reflex_criteria_qualitative = models.CharField(max_length=255, choices=(('+', 'Positive/Reactive'),
                                                                            ('-', 'Negative/Non-Reactive'),
                                                                            ('=', 'Equivocal'),
                                                                            ('?', 'Indeterminate')))
    reflex_criteria_quantitative = models.DecimalField(max_digits=ALLOWED_MAX_DIGITS, decimal_places=ALLOWED_DECIMAL_PLACES, null=True)
    # greater than, less than, equal to
    quantitative_comparison_operator = models.CharField(max_length=255, null=True, choices=(('>=', '>='),
                                                                                            ('>', '>'),
                                                                                            ('<=',
                                                                                             '<='),
                                                                                            ('<', '<'),
                                                                                            ('=', '=')))
    # internal notes
    notes = models.TextField(blank=True)


class TestTypeComment(AuditLogModel):
    test_type = models.ForeignKey('TestType', related_name='test_type_comments', null=True, on_delete=models.SET_NULL)
    comment = models.TextField(blank=True)
    # also determines whether the issue may be displayed on report
    is_public = models.BooleanField(default=False)


class Result(AuditLogModel):
    """

    """
    uuid = models.UUIDField(
        db_index=True,
        default=uuid_lib.uuid4,
        editable=False
    )
    submitted_datetime = models.DateTimeField(null=True)
    order = models.ForeignKey('Order', related_name='results', on_delete=models.CASCADE)
    test = models.ForeignKey('Test', related_name='result', on_delete=models.CASCADE)
    # Result.result is a "qualitative" field, like "+", "-"
    result = models.CharField(max_length=32, null=True, default='.')
    result_quantitative = models.DecimalField(null=True, max_digits=ALLOWED_MAX_DIGITS, decimal_places=ALLOWED_DECIMAL_PLACES,)
    # this is only applicable to qPCR for now --> different from reference ranges
    cutoff = models.DecimalField(blank=True, max_digits=ALLOWED_MAX_DIGITS, decimal_places=ALLOWED_DECIMAL_PLACES, null=True)
    result_info_label = models.CharField(max_length=254, null=True)
    upper_limit_of_quantification = models.DecimalField(blank=True, max_digits=ALLOWED_MAX_DIGITS, decimal_places=ALLOWED_DECIMAL_PLACES, null=True)
    # soon to be deprecated
    reference = models.CharField(max_length=32, null=True, default='-')
    result_info_label_prefix = models.CharField(max_length=32, blank=True)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, null=True)
    days_incubated = models.CharField(max_length=8, null=True, blank=True)
    reflex_triggered = models.BooleanField(default=False)

    class Meta:
        db_table = 'result'

    @property
    def result_label(self):
        if self.result:
            if self.result == '+':
                if self.test.test_type:
                    return self.test.test_type.test_method.positive_result_label
                else:
                    return 'Positive'
            elif self.result == '-':
                if self.test.test_type:
                    return self.test.test_type.test_method.negative_result_label
                else:
                    return 'Negative'
            elif self.result == '=':
                if self.test.test_type:
                    return self.test.test_type.test_method.equivocal_result_label
                else:
                    return 'Equivocal'
            elif self.result == '?':
                return 'Indeterminate'
            elif self.result == '.':
                return 'Pending'
            elif self.result == '<':
                return 'Below Range'
            elif self.result == '>':
                return 'Above Range'
            elif self.result == '~':
                return 'Within Range'
            else:
                return self.result
        else:
            return ''

    def get_result_label(self, result):
        if result:
            if result == '+':
                if self.test.test_type:
                    return self.test.test_type.test_method.positive_result_label
                else:
                    return 'Positive'
            elif result == '-':
                if self.test.test_type:
                    return self.test.test_type.test_method.negative_result_label
                else:
                    return 'Negative'
            elif result == '=':
                if self.test.test_type:
                    return self.test.test_type.test_method.equivocal_result_label
                else:
                    return 'Equivocal'
            elif result == '?':
                return 'Indeterminate'
            elif result == '.':
                return 'Pending'
            else:
                return result
        else:
            return ''

    @property
    def reference_label(self):
        if self.reference:
            if self.reference == '+':
                if self.test.test_type:
                    return self.test.test_type.test_method.positive_result_label
                else:
                    return 'Positive'
            elif self.reference == '-':
                if self.test.test_type:
                    return self.test.test_type.test_method.negative_result_label
                else:
                    return 'Negative'
            elif self.reference == '=':
                if self.test.test_type:
                    return self.test.test_type.test_method.equivocal_result_label
                else:
                    return 'Equivocal'
            else:
                return self.reference
        else:
            return ''


class ReportPDFFooter(AuditLogModel):
    """
    For FDA disclaimer
    """
    uuid = models.UUIDField(
        db_index=True,
        default=uuid_lib.uuid4,
        editable=False
    )
    test_method = models.ForeignKey(
        'lab.TestMethod', related_name='footer', on_delete=models.CASCADE)
    footer_text = models.TextField(null=True)

    class Meta:
        db_table = 'report_pdf_footer'


class OperationalReport(AuditLogModel):
    """

    """
    uuid = models.UUIDField(
        db_index=True,
        default=uuid_lib.uuid4,
        editable=False
    )
    start_date = models.DateField(null=False)
    end_date = models.DateField(null=False)
    date_filter = models.CharField(max_length=32, blank=True)
    created_datetime = models.DateTimeField(default=timezone.now)
    report = models.FileField(storage=PrivateMediaStorage(), null=True)
    report_format = models.CharField(max_length=32, blank=True)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, null=True)
    is_queued = models.BooleanField(default=False)
    is_completed = models.BooleanField(default=False)
    has_error = models.BooleanField(default=False)
    error_message = models.CharField(max_length=128, blank=True, default='')

    @property
    def status(self):
        if self.has_error:
            return 'Error {}'.format(self.error_message)
        elif self.is_completed and not self.is_queued:
            return 'Ready'
        elif not self.is_completed and not self.is_queued:
            return 'Unprocessed'
        elif not self.is_completed and self.is_queued:
            return 'Creating'
        else:
            return 'Unknown'


class Batch(AuditLogModel):
    name = models.CharField(max_length=255, blank=True)
    test_panel_type = models.ForeignKey(
        TestPanelType, related_name='batches', on_delete=models.CASCADE)
    instrument_integration = models.ForeignKey('lab_tenant.InstrumentIntegration', related_name='batches',
                                               on_delete=models.CASCADE)
    controls = models.ManyToManyField(Control, related_name='batches')
    samples = models.ManyToManyField(Sample, related_name='batches')
    load_sheet = models.FileField(storage=PrivateMediaStorage(), null=True)
    load_sheet_384 = models.FileField(storage=PrivateMediaStorage(), null=True)
    plate_layout_96 = models.FileField(storage=PrivateMediaStorage(), null=True)


class DataIn(models.Model):
    """
    data in from mirth connect
    """
    uuid = models.UUIDField(
        db_index=True,
        default=uuid_lib.uuid4,
        editable=False,
    )
    file = models.FileField(storage=PrivateMediaStorage(), null=True)
    created_datetime = models.DateTimeField(null=True)
    processed_datetime = models.DateTimeField(null=True)
    instrument_integration = models.ForeignKey('lab_tenant.InstrumentIntegration',
                                               related_name='data_in',
                                               on_delete=models.CASCADE)
    is_queued = models.BooleanField(default=False)
    is_completed = models.BooleanField(default=False)
    original_file_name = models.CharField(max_length=256, blank=True)
    has_error = models.BooleanField(default=False)
    error_message = models.CharField(max_length=128, blank=True, default='')

    @property
    def status(self):
        if self.has_error:
            return 'Error {}'.format(self.error_message)
        elif self.is_completed and not self.is_queued:
            return 'Processed'
        elif not self.is_completed and not self.is_queued:
            return 'Unprocessed'
        elif not self.is_completed and self.is_queued:
            return 'Processing'
        else:
            return 'Unknown'


class InstrumentTestCode(AuditLogModel):
    """
    Instruments (such as the AU400) have built-in test codes in the device which have to be mapped to
    TestTypes and TestTargets
    """

    instrument_integration = models.ForeignKey('lab_tenant.InstrumentIntegration', related_name='instrument_test_codes',
                                               on_delete=models.SET_NULL, null=True)
    test_code = models.CharField(blank=True, max_length=128)
    test_type = models.ForeignKey('TestType', related_name='instrument_test_codes',
                                  on_delete=models.SET_NULL, null=True)


class InstrumentError(AuditLogModel):
    """
    All errors for any instrument
    """
    instrument_integration = models.ForeignKey('lab_tenant.InstrumentIntegration', related_name='instrument_errors',
                                               on_delete=models.SET_NULL, null=True)
    error_text = models.TextField(blank=True)


class Observation(AuditLogModel):
    """
    an observation made by an instrument
    """
    sample = models.ForeignKey(Sample, related_name='observations', on_delete=models.CASCADE, null=True)
    control = models.ForeignKey(Control, related_name='observations', on_delete=models.SET_NULL, null=True)
    sample_name = models.CharField(blank=True, max_length=64)
    target_name = models.CharField(blank=True, max_length=64)
    test_target = models.ForeignKey('lab.TestTarget', related_name='observations', on_delete=models.SET_NULL, null=True)
    data_in = models.ForeignKey(DataIn, related_name='observations', on_delete=models.CASCADE, null=True)
    instrument_integration = models.ForeignKey('lab_tenant.InstrumentIntegration',
                                               related_name='observations',
                                               on_delete=models.SET_NULL,
                                               null=True)
    result = models.ForeignKey(Result, related_name='observations', on_delete=models.CASCADE, null=True)
    test = models.ForeignKey(Test, related_name='observations', on_delete=models.CASCADE, null=True)
    result_value = models.DecimalField(blank=True, max_digits=ALLOWED_MAX_DIGITS, decimal_places=ALLOWED_DECIMAL_PLACES, null=True)
    cutoff = models.DecimalField(blank=True, max_digits=ALLOWED_MAX_DIGITS, decimal_places=ALLOWED_DECIMAL_PLACES, null=True)
    result_qualitative = models.CharField(blank=True, max_length=64)
    well = models.CharField(blank=True, max_length=8)
    well_position = models.CharField(blank=True, max_length=16)
    observation_date = models.DateTimeField(null=True)


class BillingMessage(AuditLogModel):
    """
    Object representing order billing hl7 messages
    """
    order = models.ForeignKey('Order', related_name='billing_messages', on_delete=models.SET_NULL, null=True)
    # S3 Key of billing hl7 message
    hl7_message = models.FileField(storage=IntegrationMediaStorage(), null=True)
    amended = models.BooleanField(default=False)
    preliminary = models.BooleanField(default=False)
    is_sent = models.BooleanField(default=False)


class AccessionNumber(AuditLogModel):
    accession_number = models.CharField(null=True, unique=True, max_length=16)


class ReportRemarks(AuditLogModel):
    test_target = models.ForeignKey('lab.TestTarget', related_name='report_remarks', on_delete=models.SET_NULL,
                                    null=True)
    account = models.ForeignKey('accounts.Account', related_name='report_remarks', on_delete=models.SET_NULL, null=True)
    offsite_laboratory = models.ForeignKey('lab.OffsiteLaboratory', related_name='report_remarks',
                                           on_delete=models.SET_NULL, null=True)
    remarks = models.TextField(blank=True)


class BatchReports(AuditLogModel):
    """
    Provider batch reports
    """
    provider = models.ForeignKey('accounts.Provider', related_name='batch_report',
                                 on_delete=models.SET_NULL, blank=True, null=True)
    account = models.ForeignKey('accounts.Account', related_name='batch_report', on_delete=models.SET_NULL, blank=True,
                                null=True)
    collector = models.ForeignKey('accounts.Collector', related_name='batch_report', on_delete=models.SET_NULL,
                                  blank=True, null=True)
    pdf_file = models.FileField(storage=PrivateMediaStorage(), null=True)
    orders = models.ManyToManyField('orders.Order', symmetrical=False)


class OrderAdditionalDocuments(AuditLogModel):
    """
    Additional patient documents for orders
    """
    patient_document = models.ForeignKey('patients.AdditionalDocuments', related_name='order_patient_document',
                                         on_delete=models.SET_NULL, null=True)
    order = models.ForeignKey(Order, related_name='order_patient_document', on_delete=models.SET_NULL, null=True)


######################################################################################################################
# Django AuditLogs

auditlog.register(Order)
auditlog.register(OrderComment)
auditlog.register(OrderIssue)
auditlog.register(OrderPatientGuarantor)
auditlog.register(OrderPatientPayer)
auditlog.register(Sample)
auditlog.register(Report)
auditlog.register(Payment)
auditlog.register(TestPanel)
auditlog.register(TestPanelType)
auditlog.register(TestPanelTypeAlias)
auditlog.register(TestPanelCategory)
auditlog.register(TestPanelTypeComment)
auditlog.register(Test)
auditlog.register(TestType)
auditlog.register(TestTypeComment)
auditlog.register(TestTypeCategory)
auditlog.register(Result)
auditlog.register(ReportPDFFooter)
auditlog.register(Batch)
auditlog.register(DataIn)
auditlog.register(Observation)
auditlog.register(BillingMessage)
auditlog.register(AccessionNumber)
auditlog.register(InstrumentTestCode)
auditlog.register(ReportRemarks)
auditlog.register(BatchReports)
auditlog.register(OrderAdditionalDocuments)
