import graphene

from graphene import relay, AbstractType, ObjectType
from graphene_django import DjangoObjectType
from graphene_django.filter import DjangoFilterConnectionField

from . import models


class OrderType(DjangoObjectType):
    class Meta:
        model = models.Order


class ProviderType(DjangoObjectType):
    class Meta:
        model = models.Provider


class AccountType(DjangoObjectType):
    class Meta:
        model = models.Account


class Query(object):
    all_orders = graphene.List(OrderType)
    recent_orders = graphene.List(OrderType)
    all_providers = graphene.List(ProviderType)
    all_accounts = graphene.List(AccountType)
    order = graphene.Field(OrderType,
                           code=graphene.String(),
                           uuid=graphene.String())

    def resolve_all_orders(self, info, **kwargs):
        return models.Order.objects.all(active=True, submitted=True)

    def resolve_recent_orders(self, info, **kwargs):
        return models.Order.objects.filter(active=True,
                                           submitted=True,).order_by('-submitted_date')[:20]

    def resolve_all_providers(self, info, **kwargs):
        return models.Provider.objects.all()

    def resolve_all_accounts(self, info, **kwargs):
        return models.Account.objects.all()

    def resolve_order(self, info, **kwargs):
        code = kwargs.get('code')
        uuid = kwargs.get('uuid')

        if code is not None:
            return models.Order.objects.get(code=code)

        if uuid is not None:
            return models.Order.objects.get(uuid=uuid)

        return None

    """
    def resolve_all_ingredients(self, info, **kwargs):
        # We can easily optimize query count in the resolve method
        return Ingredient.objects.select_related('category').all()
    """