import random
import datetime
import string
import sendgrid
import os
import pytz

# sendgrid email API
from sendgrid.helpers.mail import Email, Content, Mail

# import environment variables from dotenv
from dotenv import load_dotenv
# environment variables saved before project root directory in local environment
dotenv_path = '.env'
load_dotenv(dotenv_path)


def generate_random_password(length):
    """
    k = length of password
    :return: random 10
    """
    return ''.join(
        random.choices(string.ascii_uppercase + string.digits, k=length))


def send_email_sendgrid(from_email,
                        to_email,
                        subject,
                        content,
                        attachment=False):
    sg = sendgrid.SendGridAPIClient(apikey=os.environ.get('SENDGRID_API_KEY'))
    from_email = Email(from_email)
    to_email = Email(to_email)
    subject = subject
    content = Content('text/html', content)
    mail = Mail(from_email, subject, to_email, content)
    if attachment:
        mail.add_attachment(attachment)
    response = sg.client.mail.send.post(request_body=mail.get())

    return response

