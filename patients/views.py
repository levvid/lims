import base64
import math
import pathlib
import uuid
import PyPDF2
from braces.views import LoginRequiredMixin
from dal import autocomplete
from django.contrib import messages
from django.core.files.base import ContentFile
from django.http import JsonResponse
from django.utils import timezone
from pdfrw import PdfWriter, PdfReader

from lab_tenant.models import User
from django.db import transaction, IntegrityError
from django.db.models import Q, Value
from django.db.models.functions import Concat
from django.shortcuts import render, redirect, get_object_or_404
from django.views.generic import View

from main.settings import FILE_UPLOAD_MAX_MEMORY_SIZE
from . import models
from . import forms
from . import functions
from lab_tenant.views import LabMemberMixin, LISLoginRequiredMixin
from accounts import models as accounts_models
from orders import models as orders_models
from lab import models as lab_models
from orders import forms as orders_forms

from main.functions import convert_utc_to_tenant_timezone
from .functions import upload_patient_documents

ALLOWED_FILE_EXTENSIONS = ['png', 'svg', 'pdf', 'jpeg', 'jpg']
DOCUMENT_TYPES = (
    '------------',
    'Primary Insurance',
    'Secondary Insurance',
    'Tertiary Insurance',
    'Workers\' Compensation',
    'Other'
)


class PatientListView(LISLoginRequiredMixin, View):

    def get(self, request):
        return render(request, 'patient_list.html', {})


class PatientDetailsView(LISLoginRequiredMixin, View):

    def get(self, request, patient_uuid):
        patient = get_object_or_404(models.Patient, uuid=patient_uuid)
        # hide provider/account related data if logged in as provider/account
        if request.user.user_type == 10 or request.user.user_type == 9:
            hide_provider_account_data = True
        else:
            hide_provider_account_data = False

        if request.user.user_type == 10:
            # check if this patient is associated with provider
            provider = accounts_models.Provider.objects.get(user__id=request.user.id)
            is_authorized_provider = provider.patient_set.filter(id=patient.id).exists()
            if not is_authorized_provider:
                return redirect('dashboard')
            prescribed_medication_history = models.PatientPrescribedMedicationHistory.objects.filter(
                provider_id=provider.id, patient_id=patient.id).all().prefetch_related('prescribed_medications'). \
                order_by('created_date').reverse()
            prescribed_medications_by_order = {}
            prescribed_medications_by_order_dates = {}
            for medication_history in prescribed_medication_history:
                for prescribed_medication in medication_history.prescribed_medications.all():
                    try:
                        prescribed_medications_by_order['{}'.format(medication_history.order.code)]. \
                            append(prescribed_medication)
                    except KeyError:
                        prescribed_medications_by_order['{}'.format(medication_history.order.code)] = []
                        prescribed_medications_by_order['{}'.format(medication_history.order.code)]. \
                            append(prescribed_medication)
                    if '{}'.format(medication_history.order.code) not in prescribed_medications_by_order_dates.keys():
                        prescribed_medications_by_order_dates['{}'.format(medication_history.order.code)] = \
                            medication_history.created_date
            prescribed_medications_list = list(zip(prescribed_medications_by_order.keys(),
                                                   prescribed_medications_by_order_dates.values(),
                                                   prescribed_medications_by_order.values()))
        else:
            prescribed_medications_list = None

        # show all reported test results (but not unreported results)
        reported_test_results = orders_models.Result.objects.filter(test__is_approved=True,
                                                                    test__order__patient=patient) \
            .order_by('test__order__code').prefetch_related('test', 'test__test_type', 'order')

        # get associated accounts - only associated with patient if an order has been created through this account
        account_list = orders_models.Order.objects.filter(patient=patient).values_list('account').distinct()
        accounts = accounts_models.Account.objects.filter(id__in=account_list)

        if patient.primary_care_provider and patient.primary_care_provider.is_active:
            # if patient has an active primary care provider, AND the primary care provider is active
            # then show on template
            primary_care_provider = patient.primary_care_provider
        else:
            primary_care_provider = None

        payers_with_subscribers = patient.patient_payers.exclude(subscriber_relationship='Self'). \
            order_by('-subscriber_last_name').prefetch_related('payer')
        payers = patient.patient_payers.filter(is_workers_comp=False). \
            order_by('-modified_date').prefetch_related('payer')
        workers_comp = patient.patient_payers.filter(is_workers_comp=True). \
            order_by('-modified_date').prefetch_related('payer')
        guarantors = patient.guarantors.all().order_by('-modified_date')
        enable_document_uploading = request.tenant.settings.get('enable_document_uploading', False)
        enable_document_scanning = request.tenant.settings.get('enable_document_scanning', False)
        documents = models.PatientsDocuments.objects.filter(patient=patient).order_by('patient_document__document_type').order_by('patient_document__document_type', '-created_date')

        return render(request, 'patient_details.html', {'patient': patient,
                                                        'accounts': accounts,
                                                        'hide_provider_account_data': hide_provider_account_data,
                                                        'payers': payers,
                                                        'payers_with_subscribers': payers_with_subscribers,
                                                        'workers_comp': workers_comp,
                                                        'guarantors': guarantors,
                                                        'primary_care_provider': primary_care_provider,
                                                        'reported_test_results': reported_test_results,
                                                        'prescribed_medications_list': prescribed_medications_list,
                                                        'enable_document_scanning': enable_document_scanning,
                                                        'enable_document_uploading': enable_document_uploading,
                                                        'documents': documents,
                                                        'additional_document_types': DOCUMENT_TYPES})

    @transaction.atomic
    def post(self, request, patient_uuid):
        if 'order_additional_documents' in request.POST:
            uploaded_document = request.FILES.get('order_document', False)
            patient = get_object_or_404(models.Patient, uuid=patient_uuid)

            if uploaded_document:
                document_type = request.POST.get('selected_document')
                # validate file before saving
                # size
                if document_type == DOCUMENT_TYPES[0]:
                    messages.error(request,
                                   'Please select a document type before uploading')

                if uploaded_document.size > FILE_UPLOAD_MAX_MEMORY_SIZE:
                    messages.error(request,
                                   'This file\'s size is {} MB, which is greater the maximum allowed size of {} MB.'
                                   .format(math.ceil(uploaded_document.size / (1024 * 1024)),
                                           math.ceil((FILE_UPLOAD_MAX_MEMORY_SIZE / (1024 * 1024)))))
                    return redirect('patient_details', patient_uuid=patient_uuid)
                document_extension = pathlib.Path(uploaded_document.name).suffix.lower()
                if document_extension == '.pdf':
                    reader = PyPDF2.PdfFileReader(uploaded_document.open('r'))
                    if reader.isEncrypted:
                        messages.error(request,
                                       'Uploaded files cannot be encrypted or password-protected.')
                        return redirect('patient_details', patient_uuid=patient_uuid)
                if any(ext in document_extension for ext in ALLOWED_FILE_EXTENSIONS):
                    upload_patient_documents(request, uploaded_document, document_type, patient)
                    messages.success(request, 'File uploaded successfully: {}.'.format(uploaded_document.name))
                    return redirect('patient_details', patient_uuid=patient_uuid)
                else:
                    messages.error(request,
                                   'Uploaded file must have a valid extension. Allowed file types are: PDF, '
                                   'JPEG, JPG, PNG, and SVG.')
                    return redirect('patient_details', patient_uuid=patient_uuid)
            else:
                messages.error(request, 'Please choose a file before uploading.')
                return redirect('patient_details', patient_uuid=patient_uuid)


class PatientDocumentsListView(LabMemberMixin, View):
    def get(self, request, patient_uuid):
        patient = get_object_or_404(models.Patient, uuid=patient_uuid)
        # get this orders documents
        documents = models.PatientsDocuments.objects.filter(patient=patient).order_by('-created_date')

        # get correct datetime/timezones
        for document in documents:
            document.created_date = convert_utc_to_tenant_timezone(request, document.created_date)

        return render(request, 'patient_document_list.html', {'patient': patient,
                                                              'documents': documents})


class PatientDocumentsDeleteView(LabMemberMixin, View):
    def get(self, request, patient_uuid, document_uuid):
        patients_document = models.PatientsDocuments.objects.get(uuid=document_uuid)
        patients_document.is_active = False
        patients_document.save(update_fields=['is_active'])

        messages.success(request, 'Document {} archived successfully.'.format(patients_document.patient_document.document_name))

        return redirect('patient_document_list', patient_uuid=patient_uuid)


class UploadAdditionalDocumentsView(LISLoginRequiredMixin, View):
    @transaction.atomic
    def post(self, request, patient_uuid):
        patient = get_object_or_404(models.Patient, uuid=patient_uuid)
        # get scanned document base64 from request
        uploaded_document = request.POST.dict().get('scanned_document', False)
        document_type = request.POST.dict().get('document_type', False)
        uploaded_document = ContentFile(base64.b64decode(uploaded_document))
        uploaded_document_name = "scanned_document_{}.pdf".format(timezone.now())

        if uploaded_document:
            upload_patient_documents(request, uploaded_document, document_type, patient)

        return JsonResponse({'status': 'ok'})


class PatientMergeView(LISLoginRequiredMixin, View):
    """
    View to allow users to select which patients to merge
    """

    def get(self, request, patient_uuid):
        patient = get_object_or_404(models.Patient, uuid=patient_uuid)
        same_name_patients = models.Patient.objects.filter(user__first_name=patient.user.first_name,
                                                           user__last_name=patient.user.last_name). \
            exclude(id=patient.id).all()
        form = forms.DuplicatePatientForm(initial={'original_patient': patient},
                                          patient=patient)

        context = {
            'form': form,
            'patient': patient,
            'same_name_patients': same_name_patients
        }
        return render(request, 'patient_merge.html', context)

    def post(self, request, patient_uuid):
        patient = get_object_or_404(models.Patient, uuid=patient_uuid)
        form = forms.DuplicatePatientForm(request.POST,
                                          patient=patient)

        context = {
            'form': form,
            'patient': patient
        }

        if form.is_valid():
            original_patient = patient
            patient_to_merge = form.cleaned_data['patient_to_merge']

            return redirect('patient_merge_confirm',
                            patient_uuid=original_patient.uuid,
                            patient_to_merge_uuid=patient_to_merge.uuid)
        else:
            return render(request, 'patient_merge.html', context)


class PatientMergeConfirmView(LISLoginRequiredMixin, View):
    """
    A confirmation screen for users to review patients to be merged
    """

    def get(self, request, patient_uuid, patient_to_merge_uuid):
        patient = get_object_or_404(models.Patient, uuid=patient_uuid)
        patient_to_merge = get_object_or_404(models.Patient, uuid=patient_to_merge_uuid)

        form = forms.PatientMergeForm()

        num_orders_patient = orders_models.Order.objects.filter(patient=patient).count()
        num_orders_patient_to_merge = orders_models.Order.objects.filter(patient=patient_to_merge).count()

        context = {
            'patient': patient,
            'num_orders_patient': num_orders_patient,
            'patient_to_merge': patient_to_merge,
            'num_orders_patient_to_merge': num_orders_patient_to_merge,
            'form': form,
        }

        return render(request, 'patient_merge_confirm.html', context)

    @transaction.atomic
    def post(self, request, patient_uuid, patient_to_merge_uuid):
        patient = get_object_or_404(models.Patient, uuid=patient_uuid)
        patient_to_merge = get_object_or_404(models.Patient, uuid=patient_to_merge_uuid)
        form = forms.PatientMergeForm(request.POST)
        num_orders_patient = orders_models.Order.objects.filter(patient=patient).count()
        num_orders_patient_to_merge = orders_models.Order.objects.filter(patient=patient_to_merge).count()

        context = {
            'patient': patient,
            'num_orders_patient': num_orders_patient,
            'patient_to_merge': patient_to_merge,
            'num_orders_patient_to_merge': num_orders_patient_to_merge,
            'form': form,
        }

        if form.is_valid():
            functions.merge_patients(patient, patient_to_merge)
            return redirect('patient_details', patient.uuid)

        else:
            return render(request, 'patient_merge_confirm.html', context)


class PatientMergeExecuteView(LISLoginRequiredMixin, View):
    def get(self, request, patient_uuid, patient_to_merge_uuid):
        pass


class PatientGuarantorDetailsView(LISLoginRequiredMixin, View):
    def get(self, request, patient_uuid, patient_guarantor_uuid):
        patient = get_object_or_404(models.Patient, uuid=patient_uuid)
        patient_guarantor = get_object_or_404(models.PatientGuarantor, uuid=patient_guarantor_uuid)

        context = {'patient_guarantor': patient_guarantor,
                   'patient': patient}
        return render(request, 'patient_guarantor_details.html', context)


class PatientUpdateView(LISLoginRequiredMixin, View):

    def get(self, request, patient_uuid):
        patient = get_object_or_404(models.Patient, uuid=patient_uuid)

        if request.user.user_type == 10:
            # check if this patient is associated with provider
            provider = accounts_models.Provider.objects.get(user__id=request.user.id)
            is_authorized_provider = provider.patient_set.filter(id=patient.id).exists()
            if not is_authorized_provider:
                return redirect('dashboard')

        form = forms.PatientForm(initial={'sex': patient.sex,
                                          'ethnicity': patient.ethnicity,
                                          'first_name': patient.user.first_name,
                                          'last_name': patient.user.last_name,
                                          'middle_initial': patient.middle_initial,
                                          'suffix': patient.suffix,
                                          'address1': patient.address1,
                                          'address2': patient.address2,
                                          'email': patient.user.email,
                                          'social_security_number': patient.social_security,
                                          'birth_date': patient.birth_date,
                                          'providers': patient.providers.all(),
                                          'primary_care_provider': patient.primary_care_provider,
                                          'phone_number': patient.phone_number,
                                          'zip_code': patient.zip_code})
        return render(request, 'patient_create.html', {'patient': patient,
                                                       'form': form,
                                                       'update': True})

    def post(self, request, patient_uuid):
        patient = get_object_or_404(models.Patient, uuid=patient_uuid)
        form = forms.PatientForm(request.POST, instance=patient)

        try:
            if form.is_valid():
                with transaction.atomic():
                    patient.middle_initial = form.cleaned_data['middle_initial']
                    patient.primary_care_provider = form.cleaned_data['primary_care_provider']
                    patient.suffix = form.cleaned_data['suffix']
                    patient.sex = form.cleaned_data['sex']
                    patient.ethnicity = form.cleaned_data['ethnicity']
                    patient.birth_date = form.cleaned_data['birth_date']
                    patient.address1 = form.cleaned_data['address1']
                    patient.address2 = form.cleaned_data['address2']
                    patient.social_security = form.cleaned_data['social_security_number']
                    patient.phone_number = form.cleaned_data['phone_number']
                    patient.zip_code = form.cleaned_data['zip_code']
                    patient.save()

                    # attributes tied to Django's User object
                    # save foreign key-related objects separately
                    patient.user.first_name = form.cleaned_data['first_name']
                    patient.user.last_name = form.cleaned_data['last_name']
                    patient.user.email = form.cleaned_data['email']
                    patient.user.save()

                    ############################################################################################
                    # You cannot assign directly to a many-to-many field (patient.providers)
                    # assign many-to-many field
                    # reset providers list, then add one at a time
                    patient.providers.clear()

                    for provider in form.cleaned_data['providers']:
                        patient.providers.add(provider)

                    return redirect('patient_details', patient_uuid=patient.uuid)
            return render(request, 'patient_create.html', {'patient': patient,
                                                           'form': form,
                                                           'update': True})
        except IntegrityError:
            return render(request, 'patient_create.html', {'patient': patient,
                                                           'form': form,
                                                           'update': True,
                                                           'form_error': 'A patient with this Social Security Number '
                                                                         'already exists.'})


class PatientCreateView(LISLoginRequiredMixin, View):

    def get(self, request):

        # provider portal
        if request.user.user_type == 10:
            provider = accounts_models.Provider.objects.get(user__id=request.user.id)
            form = forms.PatientForm(initial={'providers': provider})
        else:
            form = forms.PatientForm()

        return render(request, 'patient_create.html', {'form': form})

    @transaction.atomic
    def post(self, request):
        form = forms.PatientForm(request.POST)

        if form.is_valid():
            providers = form.cleaned_data['providers']
            primary_care_provider = form.cleaned_data['primary_care_provider']
            first_name = form.cleaned_data['first_name']
            last_name = form.cleaned_data['last_name']
            middle_initial = form.cleaned_data['middle_initial']
            suffix = form.cleaned_data['suffix']
            sex = form.cleaned_data['sex']
            ethnicity = form.cleaned_data['ethnicity']
            birth_date = form.cleaned_data['birth_date']
            address1 = form.cleaned_data['address1']
            address2 = form.cleaned_data['address2']
            zip_code = form.cleaned_data['zip_code']
            email = form.cleaned_data['email']
            phone_number = form.cleaned_data['phone_number']
            social_security = form.cleaned_data['social_security_number']

            # generate random string for username and password initially
            user_uuid = str(uuid.uuid4())
            username = first_name + '-' + last_name + '-' + user_uuid

            # create User object
            user_obj = User(first_name=first_name,
                            last_name=last_name,
                            username=username,
                            user_type=20,
                            email=email)
            user_obj.set_password(user_uuid)
            # create User object first before creating Patient
            user_obj.save()

            patient_obj = models.Patient(user=user_obj,
                                         primary_care_provider=primary_care_provider,
                                         suffix=suffix,
                                         middle_initial=middle_initial,
                                         sex=sex,
                                         ethnicity=ethnicity,
                                         birth_date=birth_date,
                                         address1=address1,
                                         address2=address2,
                                         phone_number=phone_number,
                                         social_security=social_security,
                                         zip_code=zip_code)
            patient_obj.save()

            ############################################################################################
            # You cannot assign directly to a many-to-many field (providers)
            # Do it after you create the item first!
            # assign many-to-many field

            for provider in providers:
                patient_obj.providers.add(provider)

            return redirect('patient_list')

        return render(request, 'patient_create.html', {'form': form})


class PatientPayerUpdateView(LISLoginRequiredMixin, View):
    def get(self, request, patient_uuid, patient_payer_uuid):
        patient = get_object_or_404(models.Patient, uuid=patient_uuid)
        patient_payer = get_object_or_404(models.PatientPayer, uuid=patient_payer_uuid)

        if patient_payer.is_primary:
            insurance_type = 'Primary'
        elif patient_payer.is_secondary:
            insurance_type = 'Secondary'
        elif patient_payer.is_tertiary:
            insurance_type = 'Tertiary'
        else:
            insurance_type = 'Self-Pay'

        form = forms.PatientPayerForm(initial={
            'insurance_type': insurance_type,
            'patient_payer_type': patient_payer.payer_type,
            'payer': patient_payer.payer,
            # subscriber info
            'subscriber_last_name': patient_payer.subscriber_last_name,
            'subscriber_first_name': patient_payer.subscriber_first_name,
            'subscriber_middle_initial': patient_payer.subscriber_middle_initial,
            'subscriber_suffix': patient_payer.subscriber_suffix,
            'subscriber_address1': patient_payer.subscriber_address1,
            'subscriber_address2': patient_payer.subscriber_address2,
            'subscriber_zip_code': patient_payer.subscriber_zip_code,
            'subscriber_sex': patient_payer.subscriber_sex,
            'subscriber_birth_date': patient_payer.subscriber_birth_date,
            'subscriber_social_security': patient_payer.subscriber_social_security,
            'subscriber_relationship': patient_payer.subscriber_relationship,
            # insurance details
            'member_id': patient_payer.member_id,
            'group_number': patient_payer.group_number,
        })

        context = {'form': form,
                   'update': True,
                   'patient': patient}

        return render(request, 'patient_payer_create.html', context)

    @transaction.atomic
    def post(self, request, patient_uuid, patient_payer_uuid):
        patient = get_object_or_404(models.Patient, uuid=patient_uuid)
        patient_payer = get_object_or_404(models.PatientPayer, uuid=patient_payer_uuid)

        form = forms.PatientPayerForm(request.POST)

        context = {'form': form,
                   'update': True,
                   'patient': patient}

        if form.is_valid():
            insurance_type = form.cleaned_data['insurance_type']
            patient_payer_type = form.cleaned_data['patient_payer_type']
            payer = form.cleaned_data['payer']
            # subscriber info
            subscriber_relationship = form.cleaned_data['subscriber_relationship']
            # insurance details
            member_id = form.cleaned_data['member_id']
            group_number = form.cleaned_data['group_number']

            patient_payer.payer = payer
            patient_payer.payer_type = patient_payer_type
            patient_payer.subscriber_last_name = form.cleaned_data['subscriber_last_name']
            patient_payer.subscriber_first_name = form.cleaned_data['subscriber_first_name']
            patient_payer.subscriber_middle_initial = form.cleaned_data['subscriber_middle_initial']
            patient_payer.subscriber_suffix = form.cleaned_data['subscriber_suffix']
            patient_payer.subscriber_address1 = form.cleaned_data['subscriber_address1']
            patient_payer.subscriber_address2 = form.cleaned_data['subscriber_address2']
            patient_payer.subscriber_zip_code = form.cleaned_data['subscriber_zip_code']
            patient_payer.subscriber_sex = form.cleaned_data['subscriber_sex']
            patient_payer.subscriber_birth_date = form.cleaned_data['subscriber_birth_date']
            patient_payer.subscriber_social_security = form.cleaned_data['subscriber_social_security']
            patient_payer.subscriber_relationship = subscriber_relationship
            patient_payer.member_id = member_id
            patient_payer.group_number = group_number

            # primary, secondary, tertiary boolean flags are mutually exclusive
            if insurance_type == 'Primary':
                patient_payer.is_primary = True
                patient_payer.is_secondary = False
                patient_payer.is_tertiary = False
            elif insurance_type == 'Secondary':
                patient_payer.is_primary = False
                patient_payer.is_secondary = True
                patient_payer.is_tertiary = False
            elif insurance_type == 'Tertiary':
                patient_payer.is_primary = False
                patient_payer.is_secondary = False
                patient_payer.is_tertiary = True
            # if self-pay
            else:
                patient_payer.payer = None
                patient_payer.is_primary = False
                patient_payer.is_secondary = False
                patient_payer.is_tertiary = False
                patient_payer.is_workers_comp = False
                patient_payer.subscriber_last_name = ''
                patient_payer.subscriber_first_name = ''
                patient_payer.subscriber_middle_initial = ''
                patient_payer.subscriber_suffix = ''
                patient_payer.subscriber_address1 = ''
                patient_payer.subscriber_address2 = ''
                patient_payer.subscriber_zip_code = ''
                patient_payer.subscriber_sex = ''
                patient_payer.subscriber_birth_date = None
                patient_payer.subscriber_social_security = ''
                patient_payer.member_id = ''
                patient_payer.group_number = ''

            patient_payer.save()

            return redirect('patient_details', patient_uuid)
        else:
            return render(request, 'patient_payer_create.html', context)


class PatientPayerCreateView(LISLoginRequiredMixin, View):
    def get(self, request, patient_uuid):
        patient = get_object_or_404(models.Patient, uuid=patient_uuid)

        form = forms.PatientPayerForm()
        context = {'form': form,
                   'patient': patient}

        return render(request, 'patient_payer_create.html', context)

    @transaction.atomic
    def post(self, request, patient_uuid):
        patient = get_object_or_404(models.Patient, uuid=patient_uuid)

        form = forms.PatientPayerForm(request.POST)

        context = {'form': form,
                   'patient': patient}

        if form.is_valid():
            insurance_type = form.cleaned_data['insurance_type']
            patient_payer_type = form.cleaned_data['patient_payer_type']
            payer = form.cleaned_data['payer']
            payer_name = form.cleaned_data['payer_name']

            # if payer can't be found in dropdown, create a new payer first
            if not payer and not patient_payer_type == 'Self-Pay':
                payer = models.Payer.objects.create(name=payer_name,
                                                    payer_type=patient_payer_type)

            # use update_or_create so as to not create duplicate objects
            if insurance_type == 'Primary':
                patient_payer, created = models.PatientPayer.objects.update_or_create(
                    is_primary=True,
                    patient=patient,
                    defaults={'payer': payer,
                              'payer_type': patient_payer_type,
                              # subscriber info
                              'subscriber_last_name': form.cleaned_data['subscriber_last_name'],
                              'subscriber_first_name': form.cleaned_data['subscriber_first_name'],
                              'subscriber_middle_initial': form.cleaned_data['subscriber_middle_initial'],
                              'subscriber_suffix': form.cleaned_data['subscriber_suffix'],
                              'subscriber_address1': form.cleaned_data['subscriber_address1'],
                              'subscriber_address2': form.cleaned_data['subscriber_address2'],
                              'subscriber_zip_code': form.cleaned_data['subscriber_zip_code'],
                              'subscriber_sex': form.cleaned_data['subscriber_sex'],
                              'subscriber_birth_date': form.cleaned_data['subscriber_birth_date'],
                              'subscriber_social_security': form.cleaned_data['subscriber_social_security'],
                              'subscriber_relationship': form.cleaned_data['subscriber_relationship'],
                              # insurance details
                              'member_id': form.cleaned_data['member_id'],
                              'group_number': form.cleaned_data['group_number']},
                )
            elif insurance_type == 'Secondary':
                patient_payer, created = models.PatientPayer.objects.update_or_create(
                    is_secondary=True,
                    patient=patient,
                    defaults={'payer': payer,
                              'payer_type': patient_payer_type,
                              # subscriber info
                              'subscriber_last_name': form.cleaned_data['subscriber_last_name'],
                              'subscriber_first_name': form.cleaned_data['subscriber_first_name'],
                              'subscriber_middle_initial': form.cleaned_data['subscriber_middle_initial'],
                              'subscriber_suffix': form.cleaned_data['subscriber_suffix'],
                              'subscriber_address1': form.cleaned_data['subscriber_address1'],
                              'subscriber_address2': form.cleaned_data['subscriber_address2'],
                              'subscriber_zip_code': form.cleaned_data['subscriber_zip_code'],
                              'subscriber_sex': form.cleaned_data['subscriber_sex'],
                              'subscriber_birth_date': form.cleaned_data['subscriber_birth_date'],
                              'subscriber_social_security': form.cleaned_data['subscriber_social_security'],
                              'subscriber_relationship': form.cleaned_data['subscriber_relationship'],
                              # insurance details
                              'member_id': form.cleaned_data['member_id'],
                              'group_number': form.cleaned_data['group_number']},
                )
            elif insurance_type == 'Tertiary':
                patient_payer, created = models.PatientPayer.objects.update_or_create(
                    is_tertiary=True,
                    patient=patient,
                    defaults={'payer': payer,
                              'payer_type': patient_payer_type,
                              # subscriber info
                              'subscriber_last_name': form.cleaned_data['subscriber_last_name'],
                              'subscriber_first_name': form.cleaned_data['subscriber_first_name'],
                              'subscriber_middle_initial': form.cleaned_data['subscriber_middle_initial'],
                              'subscriber_suffix': form.cleaned_data['subscriber_suffix'],
                              'subscriber_address1': form.cleaned_data['subscriber_address1'],
                              'subscriber_address2': form.cleaned_data['subscriber_address2'],
                              'subscriber_zip_code': form.cleaned_data['subscriber_zip_code'],
                              'subscriber_sex': form.cleaned_data['subscriber_sex'],
                              'subscriber_birth_date': form.cleaned_data['subscriber_birth_date'],
                              'subscriber_social_security': form.cleaned_data['subscriber_social_security'],
                              'subscriber_relationship': form.cleaned_data['subscriber_relationship'],
                              # insurance details
                              'member_id': form.cleaned_data['member_id'],
                              'group_number': form.cleaned_data['group_number']},
                )
            else:
                # if self-pay
                patient_payer, created = models.PatientPayer.objects.update_or_create(
                    patient=patient,
                    payer_type=patient_payer_type,
                )

            return redirect('patient_details', patient_uuid)
        else:
            return render(request, 'patient_payer_create.html', context)


class PatientPayerDeleteView(LISLoginRequiredMixin, View):

    @transaction.atomic
    def get(self, request, patient_uuid, patient_payer_uuid):
        patient = get_object_or_404(models.Patient, uuid=patient_uuid)
        patient_payer = get_object_or_404(models.PatientPayer, uuid=patient_payer_uuid)

        patient_payer.is_active = False
        patient_payer.save()

        return redirect('patient_details', patient_uuid)


class PatientWorkersCompCreateView(LISLoginRequiredMixin, View):
    def get(self, request, patient_uuid):
        patient = get_object_or_404(models.Patient, uuid=patient_uuid)

        form = forms.PatientWorkersCompForm()
        context = {'form': form,
                   'patient': patient}

        return render(request, 'patient_workers_comp_create.html', context)

    @transaction.atomic
    def post(self, request, patient_uuid):
        patient = get_object_or_404(models.Patient, uuid=patient_uuid)

        form = forms.PatientWorkersCompForm(request.POST)

        context = {'form': form,
                   'patient': patient}

        if form.is_valid():
            workers_comp_payer = form.cleaned_data.get('workers_comp_payer')
            workers_comp_payer_name = form.cleaned_data.get('workers_comp_payer_name')
            workers_comp_claim_id = form.cleaned_data.get('workers_comp_claim_id')
            workers_comp_adjuster = form.cleaned_data.get('workers_comp_adjuster')
            workers_comp_injury_date = form.cleaned_data.get('workers_comp_injury_date')

            # if payer can't be found in dropdown, create a new payer first
            if not workers_comp_payer:
                workers_comp_payer = models.Payer.objects.create(name=workers_comp_payer_name,
                                                                 payer_type='Workers Comp')

            # use update_or_create so as to not create duplicate objects
            workers_comp_payer, created = models.PatientPayer.objects.update_or_create(
                is_workers_comp=True,
                patient=patient,
                defaults={'payer': workers_comp_payer,
                          'payer_type': 'Workers Comp',
                          'workers_comp_claim_id': workers_comp_claim_id,
                          'workers_comp_adjuster': workers_comp_adjuster,
                          'workers_comp_injury_date': workers_comp_injury_date},
            )

            return redirect('patient_details', patient_uuid)
        else:
            return render(request, 'patient_workers_comp_create.html', context)


class PatientWorkersCompUpdateView(LISLoginRequiredMixin, View):
    def get(self, request, patient_uuid, workers_comp_uuid):
        patient = get_object_or_404(models.Patient, uuid=patient_uuid)
        workers_comp = get_object_or_404(models.PatientPayer, uuid=workers_comp_uuid)

        form = forms.PatientWorkersCompForm(initial={
            'workers_comp_payer': workers_comp.payer,
            'workers_comp_claim_id': workers_comp.workers_comp_claim_id,
            'workers_comp_adjuster': workers_comp.workers_comp_adjuster,
            'workers_comp_injury_date': workers_comp.workers_comp_injury_date
        })

        context = {'form': form,
                   'update': True,
                   'patient': patient}

        return render(request, 'patient_workers_comp_create.html', context)

    @transaction.atomic
    def post(self, request, patient_uuid, workers_comp_uuid):
        patient = get_object_or_404(models.Patient, uuid=patient_uuid)
        workers_comp = get_object_or_404(models.PatientPayer, uuid=workers_comp_uuid)

        form = forms.PatientWorkersCompForm(request.POST)

        context = {'form': form,
                   'update': True,
                   'patient': patient}

        if form.is_valid():
            workers_comp_payer = form.cleaned_data.get('workers_comp_payer')
            workers_comp_payer_name = form.cleaned_data.get('workers_comp_payer_name')
            workers_comp_claim_id = form.cleaned_data.get('workers_comp_claim_id')
            workers_comp_adjuster = form.cleaned_data.get('workers_comp_adjuster')
            workers_comp_injury_date = form.cleaned_data.get('workers_comp_injury_date')

            # if payer can't be found in dropdown, create a new payer first
            if not workers_comp_payer:
                workers_comp_payer = models.Payer.objects.create(name=workers_comp_payer_name,
                                                                 payer_type='Workers Comp')

            workers_comp.payer = workers_comp_payer
            workers_comp.workers_comp_claim_id = workers_comp_claim_id
            workers_comp.workers_comp_adjuster = workers_comp_adjuster
            workers_comp.workers_comp_injury_date = workers_comp_injury_date
            workers_comp.save()

            return redirect('patient_details', patient_uuid)
        else:
            return render(request, 'patient_workers_comp_create.html', context)


class PatientWorkersCompDeleteView(LISLoginRequiredMixin, View):

    @transaction.atomic
    def get(self, request, patient_uuid, workers_comp_uuid):
        patient = get_object_or_404(models.Patient, uuid=patient_uuid)
        workers_comp = get_object_or_404(models.PatientPayer, uuid=workers_comp_uuid)

        workers_comp.is_active = False
        workers_comp.save()

        return redirect('patient_details', patient_uuid)


class PatientGuarantorCreateView(LISLoginRequiredMixin, View):
    def get(self, request, patient_uuid):
        patient = get_object_or_404(models.Patient, uuid=patient_uuid)
        form = forms.PatientGuarantorForm()

        context = {'form': form,
                   'patient': patient}
        return render(request, 'patient_guarantor_create.html', context)

    @transaction.atomic
    def post(self, request, patient_uuid):
        """
        Not sure yet if there can be more than one guarantor per patient...
        2019-09-05 note: one guarantor per patient
        """
        patient = get_object_or_404(models.Patient, uuid=patient_uuid)
        form = forms.PatientGuarantorForm(request.POST)

        context = {'form': form,
                   'patient': patient}

        if form.is_valid():
            first_name = form.cleaned_data['first_name']
            last_name = form.cleaned_data['last_name']
            middle_initial = form.cleaned_data['middle_initial']
            suffix = form.cleaned_data['suffix']
            sex = form.cleaned_data['sex']
            birth_date = form.cleaned_data['birth_date']
            address1 = form.cleaned_data['address1']
            address2 = form.cleaned_data['address2']
            zip_code = form.cleaned_data['zip_code']
            email = form.cleaned_data['email']
            phone_number = form.cleaned_data['phone_number']
            social_security = form.cleaned_data['social_security_number']
            relationship_to_patient = form.cleaned_data['relationship_to_patient']

            # generate random string for username and password initially
            user_uuid = str(uuid.uuid4())
            username = first_name + '-' + last_name + '-' + user_uuid

            # create User object
            user_obj = User(first_name=first_name,
                            last_name=last_name,
                            username=username,
                            user_type=21,
                            email=email)
            user_obj.set_password(user_uuid)
            # create User object first before creating PatientGuarantor
            user_obj.save()

            guarantor_obj, created = models.PatientGuarantor.objects. \
                update_or_create(patient=patient,
                                 defaults={'user': user_obj,
                                           'relationship_to_patient': relationship_to_patient,
                                           'suffix': suffix,
                                           'middle_initial': middle_initial,
                                           'sex': sex,
                                           'birth_date': birth_date,
                                           'address1': address1,
                                           'address2': address2,
                                           'zip_code': zip_code,
                                           'phone_number': phone_number,
                                           'social_security': social_security})
            return redirect('patient_details', patient_uuid)
        else:
            return render(request, 'patient_guarantor_create.html', context)


class PatientGuarantorUpdateView(LISLoginRequiredMixin, View):
    def get(self, request, patient_uuid, patient_guarantor_uuid):
        patient = get_object_or_404(models.Patient, uuid=patient_uuid)
        patient_guarantor = get_object_or_404(models.PatientGuarantor, uuid=patient_guarantor_uuid)

        form = forms.PatientGuarantorForm(initial={
            'sex': patient_guarantor.sex,
            'first_name': patient_guarantor.user.first_name,
            'last_name': patient_guarantor.user.last_name,
            'middle_initial': patient_guarantor.middle_initial,
            'suffix': patient_guarantor.suffix,
            'address1': patient_guarantor.address1,
            'address2': patient_guarantor.address2,
            'zip_code': patient_guarantor.zip_code,
            'email': patient_guarantor.user.email,
            'social_security_number': patient_guarantor.social_security,
            'birth_date': patient_guarantor.birth_date,
            'phone_number': patient_guarantor.phone_number,
            'relationship_to_patient': patient_guarantor.relationship_to_patient
        })

        context = {
            'form': form,
            'patient': patient,
            'update': True
        }
        return render(request, 'patient_guarantor_create.html', context=context)

    @transaction.atomic
    def post(self, request, patient_uuid, patient_guarantor_uuid):
        patient = get_object_or_404(models.Patient, uuid=patient_uuid)
        patient_guarantor = get_object_or_404(models.PatientGuarantor, uuid=patient_guarantor_uuid)

        form = forms.PatientGuarantorForm(request.POST)
        context = {
            'form': form,
            'patient': patient,
            'update': True,
        }

        if form.is_valid():
            patient_guarantor.middle_initial = form.cleaned_data['middle_initial']
            patient_guarantor.relationship_to_patient = form.cleaned_data['relationship_to_patient']
            patient_guarantor.suffix = form.cleaned_data['suffix']
            patient_guarantor.sex = form.cleaned_data['sex']
            patient_guarantor.birth_date = form.cleaned_data['birth_date']
            patient_guarantor.address1 = form.cleaned_data['address1']
            patient_guarantor.address2 = form.cleaned_data['address2']
            patient_guarantor.zip_code = form.cleaned_data['zip_code']
            patient_guarantor.social_security = form.cleaned_data['social_security_number']
            patient_guarantor.phone_number = form.cleaned_data['phone_number']
            patient_guarantor.save()

            # attributes tied to Django's User object
            # save foreign key-related objects separately
            patient_guarantor.user.first_name = form.cleaned_data['first_name']
            patient_guarantor.user.last_name = form.cleaned_data['last_name']
            patient_guarantor.user.email = form.cleaned_data['email']
            patient_guarantor.user.save()
            return redirect('patient_details', patient_uuid=patient_uuid)
        else:
            return render(request, 'patient_guarantor_create.html', context=context)


class PatientGuarantorDeleteView(LISLoginRequiredMixin, View):

    @transaction.atomic
    def get(self, request, patient_uuid, patient_guarantor_uuid):
        patient = get_object_or_404(models.Patient, uuid=patient_uuid)
        patient_guarantor = get_object_or_404(models.PatientGuarantor, uuid=patient_guarantor_uuid)

        patient_guarantor.is_active = False
        patient_guarantor.save()
        return redirect('patient_details', patient_uuid)


class PatientOrderAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs = models.Patient.objects.annotate(
            name=Concat('user__first_name', Value(' '), 'user__last_name'),
            name_last_first=Concat('user__last_name', Value(' '), 'user__first_name'),
            name_last_first_comma=Concat('user__last_name', Value(', '), 'user__first_name'),
            birth_date_string=Concat('birth_date', Value('')),
        ).all()

        # exclude patient if forwarded to autocomplete
        patient = self.forwarded.get('original_patient', None)
        account = self.forwarded.get('account', None)
        if patient:
            qs = qs.exclude(id=patient)
        if account:
            qs = qs.filter(orders__account=account) | qs.filter(providers__accounts=account)
            qs = qs.distinct()
        else:
            qs = qs.none()

        if self.q:
            # search by 'first', 'last', 'first last',  'last, first', 'last first'
            qs = qs.filter(name__istartswith=self.q) | qs.filter(name_last_first__istartswith=self.q) | \
                 qs.filter(name_last_first_comma__istartswith=self.q) | qs.filter(birth_date_string__istartswith=self.q)
        return qs

    def get_result_label(self, item):
        return "{} {} {}".format(item.user.first_name, item.user.last_name, item.birth_date)


class PatientProviderAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs = accounts_models.Provider.objects.all()

        user = self.request.user
        if user.user_type == 9:  # account
            account = accounts_models.Account.objects.get(user_id=user.id)
            qs = account.providers.all()
        if user.user_type == 10:
            qs = accounts_models.Provider.objects.filter(user_id=user.id)

        if self.q:
            qs = qs.filter(user__first_name__istartswith=self.q) | qs.filter(user__last_name__istartswith=self.q)
        return qs


class PatientPayerAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        payer_type = self.forwarded.get('payer_type', None)
        qs = models.Payer.objects.filter().exclude(name='')

        if payer_type:
            qs = qs.filter(payer_type=payer_type)

        if self.q:
            qs = qs.filter(name__istartswith=self.q) | \
                 qs.filter(abbreviated_name__istartswith=self.q)
            qs = qs.distinct()
        return qs


class PatientGuarantorAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs = models.PatientGuarantor.objects.all()
        patient = self.forwarded.get('patient', None)

        if patient:
            qs = qs.filter(patient=patient)
        else:
            qs = qs.none()

        if self.q:
            qs = qs.filter(user__last_name__istartswith=self.q) | qs.filter(user__first_name__istartswith=self.q)
            qs = qs.distinct()
        return qs


class PatientPrescribedMedicationsAutoComplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs = lab_models.TestTarget.objects.filter(is_tox_target=True, is_medication=True).order_by('id')

        if self.q:
            qs = qs.filter(name__istartswith=self.q) | qs.filter(common_name__icontains=self.q)
            qs = qs.distinct()
        return qs

    def get_result_label(self, item):
        if not item.common_name or len(item.common_name) == 0:
            return '{}'.format(item.name)
        return '{} {}'.format(item.name, item.common_name).replace('[', '(').replace(']', ')').replace("'", "")
