import django
from django.db import transaction
import os

os.environ['DJANGO_SETTINGS_MODULE'] = 'main.settings'
django.setup()

from lab import models as lab_models


@transaction.atomic
def main():
    lab_models.CLIASampleType.objects.update_or_create(name='Amniotic fluid', defaults={"abbreviation": 'AF'})
    lab_models.CLIASampleType.objects.update_or_create(name='Eye fluid', defaults={"abbreviation": 'EF'})
    lab_models.CLIASampleType.objects.update_or_create(name='Gastric aspirates', defaults={"abbreviation": 'GA'})
    lab_models.CLIASampleType.objects.update_or_create(name='Gastric tissue', defaults={"abbreviation": 'GT'})
    lab_models.CLIASampleType.objects.update_or_create(name='Nasal Aspirate', defaults={"abbreviation": 'NA'})
    lab_models.CLIASampleType.objects.update_or_create(name='Nasopharyngeal Swab', defaults={"abbreviation": 'N'})
    lab_models.CLIASampleType.objects.update_or_create(name='Oropharyngeal Swab', defaults={"abbreviation": 'OP'})
    lab_models.CLIASampleType.objects.update_or_create(name='Plasma', defaults={"abbreviation": 'P'})
    lab_models.CLIASampleType.objects.update_or_create(name='Saliva', defaults={"abbreviation": 'SA'})
    lab_models.CLIASampleType.objects.update_or_create(name='Serum', defaults={"abbreviation": 'S'})
    lab_models.CLIASampleType.objects.update_or_create(name='Semen', defaults={"abbreviation": 'SE'})
    lab_models.CLIASampleType.objects.update_or_create(name='Stool', defaults={"abbreviation": 'ST'})
    lab_models.CLIASampleType.objects.update_or_create(name='Tissue', defaults={"abbreviation": 'T'})
    lab_models.CLIASampleType.objects.update_or_create(name='Urine', defaults={"abbreviation": 'U'})
    lab_models.CLIASampleType.objects.update_or_create(name='Whole Blood', defaults={"abbreviation": 'B'})
    lab_models.CLIASampleType.objects.update_or_create(name='Vaginal fluid', defaults={"abbreviation": 'VT'})
    lab_models.CLIASampleType.objects.update_or_create(name='Vaginal swab', defaults={"abbreviation": 'VS'})

    # other
    lab_models.CLIASampleType.objects.update_or_create(name='Culture', defaults={"abbreviation": 'C'})
    lab_models.CLIASampleType.objects.update_or_create(name='Other Fluid', defaults={"abbreviation": 'F'})
    lab_models.CLIASampleType.objects.update_or_create(name='Pericardial fluid', defaults={"abbreviation": 'PF'})
    lab_models.CLIASampleType.objects.update_or_create(name='Other', defaults={"abbreviation": 'O'})


if __name__ == '__main__':
    main()
