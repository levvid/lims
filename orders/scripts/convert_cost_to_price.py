import django
import logging
import os
import sys

from django.db import transaction
from tenant_schemas.utils import tenant_context

os.environ['DJANGO_SETTINGS_MODULE'] = 'main.settings'
django.setup()

from clients import models as client_models
from .. import models


@transaction.atomic
def main():
    """
    Convert all costs to prices for each TestPanelType and leave costs intact
    :return:
    """

    tenants = client_models.Client.objects.exclude(schema_name='public')

    for tenant in tenants:
        with tenant_context(tenant):

            test_panel_type_list = models.TestPanelType.objects.all()

            for test_panel in test_panel_type_list:
                test_panel.price = test_panel.cost
                test_panel.save()
                logging.debug("Tenant: {} - converting cost to price for {}"
                              .format(tenant, test_panel.name))


if __name__ == '__main__':
    logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)
    main()
