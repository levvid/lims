<h2>AU400</h2>

Connection Settings:
1. Under "Parameter --> Online --> Protocol" (Set)
- Bit/Sec: 9600
- Class: Class A

In Set Up, do "Real Time" for all settings.

Text Format,"Data Format" refers to how many decimal points the data has. - - "Zero Suppress" refers to if you want data to have leading zeros, like 000065.1

Good sample run to test with is 12/21/2017 13:41 (20 samples and contains creatinine tests)

<h3>Gibson's Notes<h3>
- Run "mode" command from cmd (on AU Windows computer) to get a list of ports and their settings
- Half duplex comms


<h3>Downloading the USB to RS-232 driver for OSX</h3>
For OSX, first download the PL2303_Mac OSX Drv_V1.6.2_20190723.pkg via http://www.prolific.com.tw/US/CustomerLogin.aspx (login). 

Then use the Unarchiver to open the .rar file and install the driver (follow these instructions: http://gyk.lt/using-usb-to-rs232-cable-with-mac-terminal/) 

While installing, be sure to allow the app from Security & Privacy from settings. Then restart your computer. 

The device should show up as something like: ```/dev/cu.usbserial - USB-Serial Controller D```

<h3>Dendi AU400 Parameters</h3>

Parameter/Online --> Set Up:
- Test Requisition Information Receive: Real-Time for everything
- Results Transfer: Real-Time for everything


Parameter/Online --> Protocol:
- Error Control --> Test Requisition Inf --> Receive: Continue
- Error Control --> Test Requisition Inf --> Results Transfer: Continue
- Text Format --> Rack No: Yes
- Text Format --> Data Format: 6
- Text Format --> Zero Suppress: No
- Character Format --> Character Length: 8
- Character Format --> Parity Bit: No
- Character Format --> Stop Bit: 1
- Communication Control --> Bit/Sec: 9600
- Communication Control --> Class: Class B
- Communication Control --> Retry: 2
- Data Format --> Start Code: 02h:STX, None
- Data Format --> End Code: 03h:ETX, None
- Data Format --> Text Length: 1024
- Data Format --> Unit No: Unchecked
- Data Format --> ETB Control: Unchecked
- Time Out (x100msec) --> T1: 20
- Time Out (x100msec) --> T2: 20
- Time Out (x100msec) --> T3: 20
- Time Out (x100msec) --> T4: 20
- Time Out (x100msec) --> T5: 20
- Time Out (x100msec) --> T6: 20
- Time Out (x100msec) --> T7: 20

Parameter/Online --> Test No:
- Online Test No. must be assigned for each Test name in order for data transfer to work successfully


Parameter/Format --> Requisition Format:
- Sample ID must be checked, with a Sample ID length of 10
- Sex/Age are unchecked

Parameter/System --> System:
- Test Requisition --> Routine: Barcode
- Test Requisition --> Emergency: Barcode
- Test Requisition --> STAT: Barcode
- Barcode --> Type: Code-128 (the AU has a difficult time reading Code-39)
- Barcode --> Digits: 10
- Barcode --> Check Mode: No (No Check Character) (with Code-128 this might be different)

