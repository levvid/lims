from django.http import HttpResponse, JsonResponse

from . import models
from accounts import models as accounts_models
from main.functions import convert_utc_to_tenant_timezone, date_string_or_none

from django.utils.html import format_html


def account_table_filtered(request, id, alternate_id, name, alias):
    accounts = models.Account.objects.all().order_by('name').prefetch_related('providers')
    if id != '_':
        try:
            id = int(id)
            accounts = accounts.filter(id=id)
        except ValueError:
            accounts = accounts.none()
    if alternate_id != '_':
        accounts = accounts.filter(alternate_id=alternate_id)
    if name != '_':
        accounts = accounts.filter(name__istartswith=name)
    if alias != '_':
        alias = alias.strip()
        accounts = accounts.filter(account_aliases__name__icontains=alias,
                                   account_aliases__is_active=True)
    accounts = accounts.distinct()

    results = []
    for account in accounts:
        if account.alternate_id:
            account_text = "{} ({})".format(account.id, account.alternate_id)
        else:
            account_text = "{}".format(account.id)
        if account.account_aliases.all():
            aliases = account.account_aliases.all()
            alias_list = []
            for alias in aliases:
                alias_list.append(str(alias))

            alias_str = ', '.join(alias_list)
        else:
            alias_str = ''

        account_data = {
            'id': '<!--{}--><a href="/accounts/{}">{}</a>'.format(
                    account_text, account.uuid, account_text
                ),
            'name': '<!--{}--><a href="/accounts/{}">{}</a>'.format(
                    account.name, account.uuid, account.name
                ),
            'alias': alias_str,
            'num_providers': account.providers.count(),
            'contact': account.contact_person,
            'phone': account.phone_number,
            'email': account.email,
            'fax': account.fax_number
        }
        results.append(account_data)
    return JsonResponse(results, safe=False)


def archived_account_table(request):
    # use all_objects object manager
    accounts = models.Account.all_objects.filter(is_active=False).order_by('name').prefetch_related('providers')

    results = []
    for account in accounts:
        if account.alternate_id:
            account_text = "{} ({})".format(account.id, account.alternate_id)
        else:
            account_text = "{}".format(account.id)

        undo_archive_link = format_html(
            '<a href="/orders/{}/undo_delete/"><i class="far fa-trash-restore"></i></a>',
            account.uuid
        )

        account_data = {
            'id': account_text,
            'name': account.name,
            'num_providers': account.providers.count(),
            'contact': account.contact_person,
            'phone': account.phone_number,
            'email': account.email,
            'fax': account.fax_number,
            'undo_archive_link': undo_archive_link,
        }
        results.append(account_data)
    return JsonResponse(results, safe=False)


def archived_account_table_filtered(request, id, alternate_id, name):
    # use all_objects object manager
    accounts = models.Account.all_objects.filter(is_active=False).order_by('name').prefetch_related('providers')
    if id != '_':
        try:
            id = int(id)
            accounts = accounts.filter(id=id)
        except ValueError:
            accounts = accounts.none()
    if alternate_id != '_':
        accounts = accounts.filter(alternate_id=alternate_id)
    if name != '_':
        accounts = accounts.filter(name__istartswith=name)

    results = []
    for account in accounts:
        if account.alternate_id:
            account_text = "{} ({})".format(account.id, account.alternate_id)
        else:
            account_text = "{}".format(account.id)

        undo_archive_link = format_html(
            '<a href="/accounts/{}/undo_delete/"><i class="far fa-trash-restore"></i></a>',
            account.uuid
        )

        account_data = {
            'id': account_text,
            'name': account.name,
            'num_providers': account.providers.count(),
            'contact': account.contact_person,
            'phone': account.phone_number,
            'email': account.email,
            'fax': account.fax_number,
            'undo_archive_link': undo_archive_link,
        }
        results.append(account_data)
    return JsonResponse(results, safe=False)


def provider_table(request):
    providers = models.Provider.objects.all().order_by('user__last_name').\
        prefetch_related('accounts', 'user', 'patient_set')

    if request.user.user_type == 9:
        account = accounts_models.Account.objects.get(user_id=request.user.id)
        providers = providers.filter(accounts=account)

    results = []
    for provider in providers:
        if provider.npi:
            npi_text = provider.npi
        elif provider.id_num:
            npi_text = provider.id_num
        else:
            npi_text = 'N/A'

        # convert to tenant's timezone
        date_added = date_string_or_none(convert_utc_to_tenant_timezone(request, provider.created_date))

        provider_data = {
            'npi': '<!--{}--><a href="/providers/{}">{}</a>'.format(
                    npi_text, provider.uuid, npi_text
                ),
            'name': "{}, {}".format(provider.user.last_name, provider.user.first_name),
            'num_patients': provider.patient_set.count(),
            'date_added': date_added
        }
        results.append(provider_data)
    return JsonResponse(results, safe=False)


def provider_table_filtered(request, first_name, last_name, npi):
    providers = models.Provider.objects.all().order_by('user__last_name').\
        prefetch_related('accounts', 'user', 'patient_set')
    if first_name != '_':
        providers = providers.filter(user__first_name__istartswith=first_name)
    if last_name != '_':
        providers = providers.filter(user__last_name__istartswith=last_name)
    if npi != '_':
        providers = providers.filter(npi__istartswith=npi)

    if request.user.user_type == 9:
        account = accounts_models.Account.objects.get(user_id=request.user.id)
        providers = providers.filter(accounts=account)

    results = []
    for provider in providers:
        if provider.npi:
            npi_text = provider.npi
        elif provider.id_num:
            npi_text = provider.id_num
        else:
            npi_text = 'N/A'

        # convert to tenant's timezone
        date_added = date_string_or_none(convert_utc_to_tenant_timezone(request, provider.created_date))

        provider_data = {
            'npi': '<!--{}--><a href="/providers/{}">{}</a>'.format(
                    npi_text, provider.uuid, npi_text
                ),
            'name': "{}, {}".format(provider.user.last_name, provider.user.first_name),
            'num_patients': provider.patient_set.count(),
            'date_added': date_added
        }
        results.append(provider_data)
    return JsonResponse(results, safe=False)


def archived_provider_table(request):
    # use all_objects object manager
    providers = models.Provider.all_objects.filter(is_active=False).order_by('user__last_name').\
        prefetch_related('accounts', 'user', 'patient_set')

    if request.user.user_type == 9:
        account = accounts_models.Account.objects.get(user_id=request.user.id)
        providers = providers.filter(accounts=account)

    results = []
    for provider in providers:
        if provider.npi:
            npi_text = provider.npi
        elif provider.id_num:
            npi_text = provider.id_num
        else:
            npi_text = 'N/A'

        # convert to tenant's timezone
        date_added = date_string_or_none(convert_utc_to_tenant_timezone(request, provider.created_date))

        undo_archive_link = format_html(
            '<a href="/providers/{}/undo_delete/"><i class="far fa-trash-restore"></i></a>',
            provider.uuid
        )

        provider_data = {
            'npi': '<!--{}--><a href="/providers/{}">{}</a>'.format(
                    npi_text, provider.uuid, npi_text
                ),
            'name': "{}, {}".format(provider.user.last_name, provider.user.first_name),
            'num_patients': provider.patient_set.count(),
            'date_added': date_added,
            'undo_archive_link': undo_archive_link,
        }
        results.append(provider_data)
    return JsonResponse(results, safe=False)


def archived_provider_table_filtered(request, first_name, last_name, npi):
    # use all_objects object manager
    providers = models.Provider.all_objects.filter(is_active=False).order_by('user__last_name').\
        prefetch_related('accounts', 'user', 'patient_set')

    if first_name != '_':
        providers = providers.filter(user__first_name__istartswith=first_name)
    if last_name != '_':
        providers = providers.filter(user__last_name__istartswith=last_name)
    if npi != '_':
        providers = providers.filter(npi__istartswith=npi)

    if request.user.user_type == 9:
        account = accounts_models.Account.objects.get(user_id=request.user.id)
        providers = providers.filter(accounts=account)

    results = []
    for provider in providers:
        if provider.npi:
            npi_text = provider.npi
        elif provider.id_num:
            npi_text = provider.id_num
        else:
            npi_text = 'N/A'

        # convert to tenant's timezone
        date_added = date_string_or_none(convert_utc_to_tenant_timezone(request, provider.created_date))

        undo_archive_link = format_html(
            '<a href="/providers/{}/undo_delete/"><i class="far fa-trash-restore"></i></a>',
            provider.uuid
        )

        provider_data = {
            'npi': npi_text,
            'name': "{}, {}".format(provider.user.last_name, provider.user.first_name),
            'num_patients': provider.patient_set.count(),
            'date_added': date_added,
            'undo_archive_link': undo_archive_link,
        }
        results.append(provider_data)
    return JsonResponse(results, safe=False)


def collector_table_filtered(request, first_name, last_name):
    collectors = models.Collector.objects.all().order_by('user__last_name').\
        prefetch_related('accounts', 'user')
    if first_name != '_':
        collectors = collectors.filter(user__first_name__istartswith=first_name)
    if last_name != '_':
        collectors = collectors.filter(user__last_name__istartswith=last_name)

    results = []
    for collector in collectors:
        # convert to tenant's timezone
        date_added = date_string_or_none(convert_utc_to_tenant_timezone(request, collector.created_date))

        collector_name = "{}, {}".format(collector.user.last_name, collector.user.first_name)
        accounts = ', '.join([x.name for x in collector.accounts.all()])

        provider_data = {
            'name': '<!--{}--><a href="/collectors/{}">{}</a>'.format(
              collector_name,
              collector.uuid,
              collector_name
            ),
            'date_added': date_added,
            'accounts': accounts,
        }
        results.append(provider_data)
    return JsonResponse(results, safe=False)


def archived_collector_table_filtered(request, first_name, last_name):
    collectors = models.Collector.all_objects.filter(is_active=False).order_by('user__last_name').\
        prefetch_related('accounts', 'user')
    if first_name != '_':
        collectors = collectors.filter(user__first_name__istartswith=first_name)
    if last_name != '_':
        collectors = collectors.filter(user__last_name__istartswith=last_name)

    results = []
    for collector in collectors:
        # convert to tenant's timezone
        date_added = date_string_or_none(convert_utc_to_tenant_timezone(request, collector.created_date))

        collector_name = "{}, {}".format(collector.user.last_name, collector.user.first_name)
        accounts = ', '.join([x.name for x in collector.accounts.all()])

        undo_archive_link = format_html(
            '<a href="/collectors/{}/undo_delete/"><i class="far fa-trash-restore"></i></a>',
            collector.uuid
        )

        provider_data = {
            'name': collector_name,
            'date_added': date_added,
            'accounts': accounts,
            'undo_archive_link': undo_archive_link,
        }
        results.append(provider_data)
    return JsonResponse(results, safe=False)


def critical_contact_table(request, account_uuid):
    account = models.Account.objects.get(uuid=account_uuid)
    critical_contacts = models.CriticalContact.objects.filter(account=account)

    results = []
    for contact in critical_contacts:

        update_link = format_html('<a href="/accounts/{}/critical_contacts/{}/update/">'
                                  '<i class="fas fa-pencil-alt text-warning"></i></a>',
                                  account.uuid,
                                  contact.uuid)
        delete_link = format_html('<a href="/accounts/{}/critical_contacts/{}/delete/">'
                                  '<i class="fas fa-trash-alt text-danger archive-contact"></i></a>',
                                  account.uuid,
                                  contact.uuid)

        contact_data = {
            'last_name': contact.last_name,
            'first_name': contact.first_name,
            'phone_number': contact.phone_number,
            'update_link': update_link,
            'delete_link': delete_link,
        }
        results.append(contact_data)

    return JsonResponse(results, safe=False)


