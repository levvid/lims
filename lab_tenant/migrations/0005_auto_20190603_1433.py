# Generated by Django 2.0.2 on 2019-06-03 14:33

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('lab_tenant', '0004_instrumentcalibration'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='instrumentcalibration',
            name='instrument_integration',
        ),
        migrations.DeleteModel(
            name='InstrumentCalibration',
        ),
    ]
