import time

import django
import docraptor
import os
import tempfile

from django.template.loader import render_to_string
from django.utils import timezone

os.environ['DJANGO_SETTINGS_MODULE'] = 'main.settings'
django.setup()

from . import models
from patients import models as patients_models;

BUCKET_NAME = 'dendi-lis'

docraptor.configuration.username = os.environ['DOCRAPTOR_API']
docraptor.configuration.debug = True

if os.environ.get('DEBUG') == 'False':
    DEBUG = False
else:
    DEBUG = True

"""
Use docraptor Async create document call to generate pdf reports and save them to s3
"""


def generate_requisition_PDF(request, order_uuid):
    tenant = request.tenant
    order = models.Order.objects.select_related('patient',
                                                'patient__user',
                                                'account',
                                                'provider',
                                                'provider__user').get(uuid=order_uuid)
    icd10_codes = order.icd10_codes.all()

    sample = models.Sample.objects.filter(order_id=order.id).first()

    # PatientPayer objects for the order
    primary_patient_payer = models.OrderPatientPayer.objects.filter(order=order, is_primary=True).first()
    secondary_patient_payer = models.OrderPatientPayer.objects.filter(order=order, is_secondary=True).first()
    workers_compensation = models.OrderPatientPayer.objects.filter(order=order, is_workers_comp=True).first()
    tertiary_insurance = models.OrderPatientPayer.objects.filter(order=order, is_tertiary=True).first()
    # PatientGuarantor object for the order
    patient_guarantor = models.OrderPatientGuarantor.objects.filter(order=order).first()
    # import pdb; pdb.set_trace()
    tests = models.TestPanel.objects.select_related('test_panel_type').filter(order_id=order.id).all()
    prescribed_medication_history = patients_models.PatientPrescribedMedicationHistory.objects.filter(
        order_id=order.id).prefetch_related('prescribed_medications').first()
    if prescribed_medication_history:
        prescribed_medications = prescribed_medication_history.prescribed_medications.all()
    else:
        prescribed_medications = None
    save_file_name = "requisition_form/{}/{}/{}.pdf".format(tenant.name, order.uuid, timezone.now())
    doc_api = docraptor.DocApi()
    reports = [1]

    show_poct_and_screening_panel = request.tenant.settings.get('show_prescribed_medication', False)
    show_abn_on_req_form = request.tenant.settings.get('show_abn_on_req_form', False)

    html_file = 'order_requisition_form_pdf.html'

    rendered_html = render_to_string(html_file, {'order': order,
                                                 'request': request,
                                                 'reports': reports,
                                                 'tests': tests,
                                                 'icd10_codes': icd10_codes,
                                                 'prescribed_medications': prescribed_medications,
                                                 'show_poct_and_screening_panel': show_poct_and_screening_panel,
                                                 'primary_patient_payer': primary_patient_payer,
                                                 'secondary_patient_payer': secondary_patient_payer,
                                                 'patient_guarantor': patient_guarantor,
                                                 'show_abn_on_req_form': show_abn_on_req_form,
                                                 'sample': sample,
                                                 'workers_compensation': workers_compensation,
                                                 'tertiary_insurance': tertiary_insurance})

    # synchronous call
    try:
        create_response = doc_api.create_doc({
            "test": DEBUG,  # test documents are free but watermarked
            "document_content": rendered_html,  # supply content directly
            "name": save_file_name,
            "document_type": "pdf",
            "javascript": True,    #self.fail('message')  # enable JavaScript processing
            "prince_options": {
                "media": 'screen',
            },
        })

        with tempfile.NamedTemporaryFile() as temp:
            temp.write(create_response)
            order.requisition_form.save(save_file_name, temp)
    except docraptor.rest.ApiException as error:
        print(error)

    # asyncronous call
    # response = doc_api.create_async_doc({
    #     "test": DEBUG,  # test documents are free but watermarked
    #     "document_content": rendered_html,  # supply content directly
    #     "name": save_file_name,
    #     "document_type": "pdf",
    #     # "javascript": True,    self.fail('message')  # enable JavaScript processing
    #     "prince_options": {
    #         "media": 'screen',
    #     },
    # })
    #
    # # async loop
    # while True:
    #     status_response = doc_api.get_async_doc_status(response.status_id)
    #     if status_response.status == "completed":
    #         doc_response = doc_api.get_async_doc(status_response.download_id)
    #         with tempfile.NamedTemporaryFile() as temp:
    #             temp.write(doc_response)
    #             order.requisition_form.save(save_file_name, temp)
    #         break
    #     elif status_response.status == "failed":
    #         print("FAILED")
    #         print(status_response)
    #         break
    #     else:
    #         time.sleep(1)
