from asgiref.sync import async_to_sync
from channels.generic.websocket import WebsocketConsumer
import json

from orders import tasks


class InstrumentIntegrationStatusConsumer(WebsocketConsumer):
    def connect(self):
        self.instrument_integration_uuid = self.scope['url_route']['kwargs']['instrument_integration_uuid']
        async_to_sync(self.channel_layer.group_add)(str(self.instrument_integration_uuid), self.channel_name)
        self.accept()

    def disconnect(self, close_code):
        async_to_sync(self.channel_layer.group_discard)(str(self.instrument_integration_uuid), self.channel_name)

    def receive(self, text_data):
        text_data_json = json.loads(text_data)
        message = text_data_json['message']

        # send message to room group
        async_to_sync(self.channel_layer.group_send)(
            self.instrument_integration_uuid,
            {
                'type': 'status_update',
                'message': message
            }
        )

    def status_update(self, event):
        message = event['message']
        self.send(text_data=json.dumps({
            'message': message
        }))


class OrderStatusConsumer(WebsocketConsumer):
    def connect(self):
        self.order_uuid = self.scope['url_route']['kwargs']['order_uuid']
        async_to_sync(self.channel_layer.group_add)(str(self.order_uuid), self.channel_name)
        self.accept()

    def disconnect(self, close_code):
        async_to_sync(self.channel_layer.group_discard)(str(self.order_uuid), self.channel_name)

    def receive(self, text_data):
        text_data_json = json.loads(text_data)
        message = text_data_json['message']
        report_url = text_data_json['report_url']
        preliminary = text_data_json['preliminary']
        amended_reports = text_data_json['amended_reports']
        approved_by = text_data_json['approved_by']

        # Send message to room group
        async_to_sync(self.channel_layer.group_send)(
            self.order_uuid,
            {
                'type': 'status_update',
                'message': message,
                'report_url': report_url,
                'preliminary': preliminary,
                'amended_reports': amended_reports,
                'approved_by': approved_by
            }
        )

    # Receive message from room group
    def status_update(self, event):
        message = event['message']
        report_url = event['report_url']
        preliminary = event['preliminary']
        amended_reports = event['amended_reports']
        approved_by = event['approved_by']

        # Send message to WebSocket
        self.send(text_data=json.dumps({
            'message': message,
            'report_url': report_url,
            'preliminary': preliminary,
            'amended_reports': amended_reports,
            'approved_by': approved_by
        }))


class DataInStatusConsumer(WebsocketConsumer):
    def connect(self):
        self.data_in_uuid = self.scope['url_route']['kwargs']['data_in_uuid']
        async_to_sync(self.channel_layer.group_add)(str(self.data_in_uuid), self.channel_name)
        self.accept()

    def disconnect(self, close_code):
        async_to_sync(self.channel_layer.group_discard)(str(self.data_in_uuid), self.channel_name)

    def receive(self, text_data):
        text_data_json = json.loads(text_data)
        message = text_data_json['message']

        # Send message to room group
        async_to_sync(self.channel_layer.group_send)(
            self.data_in_uuid,
            {
                'type': 'status_update',
                'message': message
            }
        )

    # Receive message from room group
    def status_update(self, event):
        message = event['message']

        # Send message to WebSocket
        self.send(text_data=json.dumps({
            'message': message
        }))

    def notification(self, event):
        message = event['message']
        self.send(text_data=json.dumps({
            'notification': message
        }))


class DataInImportConsumer(WebsocketConsumer):
    """
    Consumer that starts data import process
    """
    def connect(self):
        self.data_in_uuid = self.scope['url_route']['kwargs']['data_in_uuid']
        self.accept()

    def disconnect(self, close_code):
        pass

    def receive(self, text_data):
        text_data_json = json.loads(text_data)
        tenant_id = text_data_json['message']
        user_id = text_data_json['user_id']

        tasks.import_instrument_data.apply_async(
            args=[self.data_in_uuid, tenant_id, user_id],
            queue='default')
