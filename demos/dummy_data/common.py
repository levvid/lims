# Additional lab admin users to be created
ADDITIONAL_USERS = [
    {
        'last_name': 'Baek',
        'first_name': 'Jihoon',
        'email': 'jbaek@dendisoftware.com',
        'user_type': 1,
        'password': 'dendipower123123'
    },
    {
        'last_name': 'Mulonga',
        'first_name': 'Gibson',
        'email': 'gmulonga@dendisoftware.com',
        'user_type': 1,
        'password': 'dendipower123123'
    },
    {
        'last_name': 'George',
        'first_name': 'Zhao',
        'email': 'gzhao@dendisoftware.com',
        'user_type': 1,
        'password': 'dendipower123'
    },
    {
        'last_name': 'Benedict',
        'first_name': 'Lance',
        'email': 'lbenedict@ildp.com',
        'user_type': 1,
        'password': 'diagnostics'
    },
    {
        'last_name': 'Morgan',
        'first_name': 'Joe',
        'email': 'jmorgan@ildp.com',
        'user_type': 1,
        'password': 'diagnostics'
    },
    {
        'last_name': 'Gamel',
        'first_name': 'Lauren',
        'email': 'lgamel@ildp.com',
        'user_type': 1,
        'password': 'diagnostics'
    },
    {
        'last_name': 'Smith',
        'first_name': 'Faye',
        'email': 'fsmith@ildp.com',
        'user_type': 1,
        'password': 'diagnostics'
    }
]

# Dummy providers
PROVIDERS = [
    {
        'first_name': 'Gregory',
        'last_name': 'House',
        'email': 'GHouse@gmail.com',
        'npi': '1234567890',
    },
    {
        'first_name': 'Helen',
        'last_name': 'Taussig',
        'email': 'HTaussig@gmail.com',
        'npi': '0123456789',
    },
    {
        'first_name': 'Zora',
        'last_name': 'Janzekovic',
        'email': 'ZJanzekovic@gmail.com',
        'npi': '2345678901',
    },
    {
        'first_name': 'Virginia',
        'last_name': 'Apgar',
        'email': 'VApgar@gmail.com',
        'npi': '3456789012',
    },
    {
        'first_name': 'Victor',
        'last_name': 'McKusick',
        'email': 'VMckusic@gmail.com',
        'npi': '4567890123',
    },
]

# Dummy accounts
ACCOUNTS = [
    {
        'name': 'Durham Clinic',
        'address1': '1204 Broad St, Durham, NC',
    },
    {
        'name': 'Chapel Hill Clinic',
        'address1': '1324 Franklin St, Chapel Hill, NC',
    },
    {
        'name': 'Raleigh Center for Health',
        'address1': '342 Radurleigh Rd, Raleigh, NC',
    },
    {
        'name': 'AnyTestNow',
        'address1': '543 Hopson St, Cary, NC',
    },
    {
        'name': 'Center for Clinic',
        'address1': '2415 Foundation Cir, Durham, NC',
    },
    {
        'name': 'Sacred Heart Hospital',
        'address1': '34255 Valve, Charlotte, NC',
    },
    {
        'name': 'Mission Tests',
        'address1': '789 Dark ave, Raleigh, NC',
    },
    {
        'name': 'Holly Springs Clinic',
        'address1': '789 Light ave, Holly Springs, NC'
    },
    {
        'name': 'Cors Clinic for Health',
        'address1': '7894 Hidden Valley ave, Raleigh, NC',
    },
]

# Dummy Patients
PATIENTS = [
    {
        'first_name': 'Simon',
        'last_name': 'Jung',
        'birth_date': '1992-04-14',
        'sex': 'M'
    },
    {
        'first_name': 'Sally',
        'last_name': 'Hopson',
        'birth_date': '1994-03-13',
        'sex': 'F'
    },
    {
        'first_name': 'Missy',
        'last_name': 'Hills',
        'birth_date': '1991-04-29',
        'sex': 'F'
    },
    {
        'first_name': 'Joanna',
        'last_name': 'Smith',
        'birth_date': '1996-09-14',
        'sex': 'F'
    },
    {
        'first_name': 'Dendi',
        'last_name': 'Jung',
        'birth_date': '2012-05-19',
        'sex': 'M'
    },
    {
        'first_name': 'Jose',
        'last_name': 'Guzman',
        'birth_date': '1991-09-14',
        'sex': 'M'
    },
    {
        'first_name': 'Juan',
        'last_name': 'Peter',
        'birth_date': '1992-04-12',
        'sex': 'M'
    },
    {
        'first_name': 'Arya',
        'last_name': 'Stark',
        'birth_date': '1992-04-14',
        'sex': 'F'
    },
    {
        'first_name': 'Fernando',
        'last_name': 'Hernendez',
        'birth_date': '1982-02-10',
        'sex': 'M'
    },
    {
        'first_name': 'Visesh',
        'last_name': 'Prasad',
        'birth_date': '1974-08-12',
        'sex': 'M'
    },
    {
        'first_name': 'Katie',
        'last_name': 'Sanders',
        'birth_date': '1994-07-12',
        'sex': 'F'
    },
]