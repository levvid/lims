/**
 * Helper fn to print labels using zebra printer
 * @param label_object
 */
function zebra_labels_helper(label_object) {
    let sample_barcode = label_object.attr('sample_barcode');
    let sample_code = label_object.attr('sample_code');
    let collection_date = (label_object.attr('collection_date'));
    collection_date = collection_date.split(',');
    let test_methods = label_object.attr('test_methods');
    test_methods = test_methods.replace('[', '').replace(']', '');
    test_methods = test_methods.split(',');
    let sample_collection_date = 'Collection Date: ' + formatDate(collection_date[0] + ',' + collection_date[1]);
    let patient_name = '{{ order.patient.user.last_name }}' + ", " + '{{ order.patient.user.first_name}}';
    let patient_dob = 'DOB: ' + formatDate('{{ order.patient.birth_date }}');
    let client_name = '{{ account.id }}{% if account.alternate_id %}({{ account.alternate_id }}){% endif %}' + ' - ' + '{{ account.name }}';
    let accession_number = '{{ order.accession_number }}';

    printZebraLabel(patient_name, accession_number, patient_dob, sample_collection_date, client_name, sample_barcode, sample_code, test_methods);
}


/**
 * Helper function to print labels using dymo printer
 * @param label_object
 */
function dymo_labels_helper(label_object) {
    var sample_code = 'Sample Code: ' + label_object.attr('sample_code');
    let collection_date = (label_object.attr('collection_date'));
    collection_date = collection_date.split(',');
    var sample_collection_date = 'Collection Date: ' + formatDate(collection_date[0] + ',' + collection_date[1]);
    var patient_name = '{{ order.patient.user.last_name }}' + ", " + '{{ order.patient.user.first_name}}';
    var patient_dob = 'DOB: ' + formatDate('{{ order.patient.birth_date }}');
    var sample_barcode = label_object.attr('sample_barcode');
    var client_name = '{{ account.id }}{% if account.alternate_id %}({{ account.alternate_id }}){% endif %}' + ' - ' + '{{ account.name }}';
    var auto_print = false;

    printLabel(patient_name, sample_code, patient_dob, sample_collection_date, client_name, sample_barcode, auto_print);
}


$('.print-zebra-label').click(function (event) {
    zebra_labels_helper($('.print-zebra-label'));
});

$('.print-zebra-label-ten-digit').click(function (event) {
    zebra_labels_helper($('.print-zebra-label-ten-digit'));
});

$('.print-label').click(function (event) {
    dymo_labels_helper($('.print-label'));
});

/**
 * Automatically print sample labels if auto print is turned on;
 * Priority is given to 10 digit labels depending on the client setting
 * @param auto_print_labels
 */
function automatically_print_labels(auto_print_labels) {
    if (auto_print_labels) {
        // print 10 digit zebra label if exists
        if ($('.print-zebra-label-ten-digit').length > 0) {
            $('.print-zebra-label-ten-digit').each(function () {
                zebra_labels_helper($(this));
            });
        }  // auto print normal zebra label if exists
        else if ($('.print-zebra-label').length > 0) {
            $('.print-zebra-label').each(function () {
                zebra_labels_helper($(this));
            });
        }  // auto print dymo if exists
        else if ($('.print-zebra-label').length > 0) {
            $('.print-zebra-label').each(function () {
                zebra_labels_helper($(this));
            });
        }
    }
}