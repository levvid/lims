import logging
import sys

from ..scripts.ildp_setup_20190722 import main

if __name__ == '__main__':
    """
    Calls the ildp setup script with demo=True
    """
    logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)
    main(demo=True)
