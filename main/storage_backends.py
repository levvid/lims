import os

from storages.backends.s3boto3 import S3Boto3Storage

from . import settings
from django.core.exceptions import ValidationError


class PrivateMediaStorage(S3Boto3Storage):
    location = settings.AWS_PRIVATE_MEDIA_LOCATION
    default_acl = 'private'
    file_overwrite = True
    custom_domain = False


class PublicMediaStorage(S3Boto3Storage):
    location = settings.AWS_PUBLIC_MEDIA_LOCATION
    default_acl = 'public'
    file_overwrite = True
    custom_domain = False


class IntegrationMediaStorage(S3Boto3Storage):
    location = settings.AWS_INTEGRATION_MEDIA_LOCATION
    default_acl = 'private'
    file_overwrite = True
    custom_domain = False


def validate_file_extension(value):
    valid_extensions = ['application/jpg', 'application/jpeg', 'application/png', 'application/svg']

    if value.file.content_type not in valid_extensions:
        raise ValidationError(u'File not supported!')

