<h1>Code Guidelines</h1>

Python: 
* PEP8
* Django best practices
* Use ORM if possible instead of SQL

Project architecture:
* Heroku structure
* Everything else not in the current root directory for each app needs to go into the /scripts/ directory of the app

REST API:
* https://hackernoon.com/restful-api-designing-guidelines-the-best-practices-60e1d954e7c9

Handsontable
* Used if there are expected to be over 100 rows and CSV export functionality is desired
* Otherwise, generate a table from Bootstrap/Django