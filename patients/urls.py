from django.urls import path

from . import views


urlpatterns = [
    # patients
    path('patients/', views.PatientListView.as_view(), name='patient_list'),
    path('patients/create/', views.PatientCreateView.as_view(), name='patient_create'),
    path('patients/<str:patient_uuid>/', views.PatientDetailsView.as_view(), name='patient_details'),
    path('patients/<str:patient_uuid>/update/', views.PatientUpdateView.as_view(), name='patient_update'),
    path('patients/<str:patient_uuid>/merge/', views.PatientMergeView.as_view(), name='patient_merge'),
    path('patients/<str:patient_uuid>/merge/<str:patient_to_merge_uuid>/',
         views.PatientMergeConfirmView.as_view(), name='patient_merge_confirm'),

    path('patients/<str:patient_uuid>/payers/create/', views.PatientPayerCreateView.as_view(),
         name='patient_payer_create'),
    path('patients/<str:patient_uuid>/payers/<str:patient_payer_uuid>/update/',
         views.PatientPayerUpdateView.as_view(),
         name='patient_payer_update'),
    path('patients/<str:patient_uuid>/payers/<str:patient_payer_uuid>/delete/',
         views.PatientPayerDeleteView.as_view(),
         name='patient_payer_delete'),

    path('patients/<str:patient_uuid>/workers_comp/create/', views.PatientWorkersCompCreateView.as_view(),
         name='patient_workers_comp_create'),
    path('patients/<str:patient_uuid>/workers_comp/<str:workers_comp_uuid>/update/',
         views.PatientWorkersCompUpdateView.as_view(),
         name='patient_workers_comp_update'),
    path('patients/<str:patient_uuid>/workers_comp/<str:workers_comp_uuid>/delete/',
         views.PatientWorkersCompDeleteView.as_view(),
         name='patient_workers_comp_delete'),

    path('patients/<str:patient_uuid>/guarantors/create/', views.PatientGuarantorCreateView.as_view(),
         name='patient_guarantor_create'),
    path('patients/<str:patient_uuid>/guarantors/<str:patient_guarantor_uuid>/',
         views.PatientGuarantorDetailsView.as_view(),
         name='patient_guarantor_details'),
    path('patients/<str:patient_uuid>/guarantors/<str:patient_guarantor_uuid>/update/',
         views.PatientGuarantorUpdateView.as_view(),
         name='patient_guarantor_update'),
    path('patients/<str:patient_uuid>/guarantors/<str:patient_guarantor_uuid>/delete/',
         views.PatientGuarantorDeleteView.as_view(),
         name='patient_guarantor_delete'),

    # autocomplete views
    path('patient_order_autocomplete/', views.PatientOrderAutocomplete.as_view(), name='patient_order_autocomplete'),
    path('patient_provider_autocomplete/', views.PatientProviderAutocomplete.as_view(),
         name='patient_provider_autocomplete'),
    path('patient_payer_autocomplete/', views.PatientPayerAutocomplete.as_view(),
         name='patient_payer_autocomplete'),
    path('patient_guarantor_autocomplete/', views.PatientGuarantorAutocomplete.as_view(),
         name='patient_guarantor_autocomplete'),
    path('patient_prescribed_medications/', views.PatientPrescribedMedicationsAutoComplete.as_view(),
         name='patient_prescribed_medications'),

    # additional documents/scanning
    path('patients/<str:patient_uuid>/upload_additional_documents/', views.UploadAdditionalDocumentsView.as_view(), name='upload_additional_documents'),
    # path('orders/barcode_accession/samples/upload_additional_documents/<str:order_code>/', views.UploadAdditionalDocumentsView.as_view(), name='upload_additional_documents'),
    # path('orders/barcode_accession/orders/upload_additional_documents/<str:order_code>/', views.UploadAdditionalDocumentsView.as_view(), name='upload_additional_documents'),
    path('patients/<str:patient_uuid>/documents/', views.PatientDocumentsListView.as_view(), name='patient_document_list'),
    path('patients/<str:patient_uuid>/documents/<str:document_uuid>/delete/', views.PatientDocumentsDeleteView.as_view(), name='patient_document_delete'),

]
