from . import models
from rest_framework import serializers
from django.core.exceptions import ValidationError
from uuid import UUID, uuid4

import lab_tenant.serializers
from lab_tenant import models as lab_tenant_models
from accounts import serializers as accounts_serializers
from accounts import models as accounts_models
from patients import models as patients_models


class PatientSerializer(serializers.HyperlinkedModelSerializer):
    user = lab_tenant.serializers.UserSerializer()

    class Meta:
        model = models.Patient
        fields = ('uuid', 'user', 'middle_initial', 'birth_date',
                  'address1', 'address2', 'sex', 'phone_number',)


class PayerSerializer(serializers.HyperlinkedModelSerializer):
    name = serializers.CharField(required=True)
    payer_type = serializers.ChoiceField(required=True,
                                         choices=(
                                             ('Commercial', 'Commercial'),
                                             ('Medicare', 'Medicare'), ('Medicaid', 'Medicaid'),
                                             ('Workers Comp', 'Workers\' Compensation'),
                                             ('Other', 'Other')
                                         ))

    payer_code = serializers.CharField(required=False)
    alternate_id = serializers.CharField(required=False)
    abbreviated_name = serializers.CharField(required=False)
    address_1 = serializers.CharField(required=False)
    address_2 = serializers.CharField(required=False)
    city = serializers.CharField(required=False)
    state = serializers.CharField(required=False)
    zip_code = serializers.CharField(required=False)
    phone_number = serializers.CharField(required=False)

    class Meta:
        model = models.Payer
        fields = (
            'uuid',
            # required
            'name', 'payer_type',
            # optional
            'payer_code',
            'alternate_id',
            'abbreviated_name',
            'address_1',
            'address_2',
            'city', 'state', 'zip_code', 'phone_number'
        )

    def validate(self, attrs):

        state = attrs.get('state')
        if state and len(state) != 2:
            raise serializers.ValidationError("State must be 2 characters.")

        return attrs


class PatientPayerReadOnlySerializer(serializers.HyperlinkedModelSerializer):
    payer = PayerSerializer()

    class Meta:
        model = models.PatientPayer
        fields = ('uuid', 'payer_type', 'payer', 'is_primary', 'is_secondary', 'is_tertiary', 'is_workers_comp',
                  'member_id', 'group_number', 'subscriber_relationship', 'subscriber_last_name',
                  'subscriber_first_name',
                  'subscriber_middle_initial',
                  'subscriber_suffix',
                  'subscriber_birth_date',
                  'subscriber_address1',
                  'subscriber_address2',
                  'subscriber_zip_code',
                  'subscriber_sex',
                  'subscriber_phone_number',
                  'subscriber_social_security',
                  'created_date',
                  'workers_comp_injury_date',
                  'workers_comp_adjuster',
                  'workers_comp_claim_id',)


class PatientGuarantorSerializer(serializers.HyperlinkedModelSerializer):
    patient_uuid = serializers.CharField(required=False, allow_blank=False, write_only=True)
    relationship_to_patient = serializers.ChoiceField(
        required=False,
        choices=(('Parent', 'Parent'), ('Spouse', 'Spouse'), ('Dependent', 'Dependent'), ('Other', 'Other'))
    )
    first_name = serializers.CharField(required=False, write_only=True)
    last_name = serializers.CharField(required=False, write_only=True)
    sex = serializers.ChoiceField(required=False, choices=(('M', 'Male'), ('F', 'Female'), ('NB', 'Non-Binary')))
    birth_date = serializers.DateField(required=False)
    user = lab_tenant.serializers.PatientUserSerializer(read_only=True)
    patient = PatientSerializer(read_only=True)
    uuid = serializers.UUIDField(read_only=True)

    def validate(self, attrs):
        patient_uuid = attrs.get('patient_uuid')
        sex = attrs.get('sex')
        birth_date = attrs.get('birth_date')
        relationship_to_patient = attrs.get('relationship_to_patient')

        if self.instance:  # PUT
            if patient_uuid:
                raise serializers.ValidationError("patient_uuid not allowed for PUT requests")
        else:  # POST
            try:
                UUID(patient_uuid, version=4)
                patient = patients_models.Patient.objects.get(uuid=patient_uuid)
                attrs.pop('patient_uuid')
            except ValueError:
                raise serializers.ValidationError("Invalid UUID format for patient_uuid.")
            except accounts_models.Provider.DoesNotExist:
                raise serializers.ValidationError("Patient matching UUID patient_uuid does not exist.")
            attrs['patient'] = patient

            first_name = attrs.get('first_name')
            if not first_name:
                raise serializers.ValidationError("first_name may not be blank.")
            last_name = attrs.get('last_name')
            if not last_name:
                raise serializers.ValidationError("last_name may not be blank.")
            if not sex:
                raise serializers.ValidationError("sex may not be blank.")
            if not birth_date:
                raise serializers.ValidationError("birth_date may not be blank.")
            if not relationship_to_patient:
                raise serializers.ValidationError("relationship_to_patient may not be blank.")
        return attrs

    def validate_social_security(self, social_security):
        if social_security:
            social_security_numerical = social_security.replace('-', '').strip()
            if social_security and not len(social_security_numerical) == 9:
                raise serializers.ValidationError(
                    "social_security must be in the format '###-##-####'"
                )
        return social_security

    def validate_sex(self, sex):
        if sex and sex not in ['M', 'F', 'NB']:
            raise serializers.ValidationError("sex must be M, F, or NB")
        return sex

    def validate_relationship_to_patient(self, relationship_to_patient):
        if relationship_to_patient and relationship_to_patient not in ['Parent', 'Spouse', 'Dependent', 'Other']:
            raise serializers.ValidationError("relationship_to_patient must be Parent, Spouse, Dependent, or Other.")
        return relationship_to_patient

    def create(self, validated_data):
        user = lab_tenant_models.User.objects.create(
            user_type=21,
            first_name=validated_data.get('first_name'),
            last_name=validated_data.get('last_name'),
        )
        patient_guarantor = models.PatientGuarantor.objects.create(
            patient=validated_data.get('patient'),
            relationship_to_patient=validated_data.get('relationship_to_patient'),
            user=user,
            sex=validated_data.get('sex'),
            middle_initial=validated_data.get('middle_initial'),
            suffix=validated_data.get('suffix'),
            birth_date=validated_data.get('birth_date'),
            address1=validated_data.get('address1'),
            address2=validated_data.get('address2'),
            zip_code=validated_data.get('zip_code'),
            phone_number=validated_data.get('phone_number'),
            social_security=validated_data.get('social_security'),
        )
        return patient_guarantor

    def update(self, patient_guarantor, validated_data):
        patient_guarantor.user.first_name = validated_data.get('first_name', patient_guarantor.user.first_name)
        patient_guarantor.user.last_name = validated_data.get('last_name', patient_guarantor.user.last_name)
        patient_guarantor.user.save()

        patient_guarantor.relationship_to_patient = validated_data.get(
            'relationship_to_patient', patient_guarantor.relationship_to_patient)
        patient_guarantor.sex = validated_data.get('sex', patient_guarantor.sex)
        patient_guarantor.middle_initial = validated_data.get('middle_initial', patient_guarantor.middle_initial)
        patient_guarantor.suffix = validated_data.get('suffix', patient_guarantor.suffix)
        patient_guarantor.birth_date = validated_data.get('birth_date', patient_guarantor.birth_date)
        patient_guarantor.address1 = validated_data.get('address1', patient_guarantor.address1)
        patient_guarantor.address2 = validated_data.get('address2', patient_guarantor.address2)
        patient_guarantor.zip_code = validated_data.get('zip_code', patient_guarantor.zip_code)
        patient_guarantor.phone_number = validated_data.get('phone_number', patient_guarantor.phone_number)
        patient_guarantor.social_security = validated_data.get('social_security', patient_guarantor.social_security)
        patient_guarantor.save()
        return patient_guarantor

    class Meta:
        model = models.PatientGuarantor
        fields = (
            # required
            'patient_uuid', 'relationship_to_patient', 'first_name', 'last_name', 'sex', 'birth_date',
            # optional
            'middle_initial', 'suffix', 'address1', 'address2', 'social_security', 'phone_number',
            'zip_code',
            # read-only
            'patient', 'user', 'uuid',
        )


class PatientPayerSerializer(serializers.HyperlinkedModelSerializer):
    patient_uuid = serializers.CharField(
        required=False,
        allow_blank=False,
        write_only=True
    )
    payer_uuid = serializers.CharField(
        required=False,
        allow_blank=True,
        write_only=True
    )
    payer_name = serializers.CharField(
        required=False,
        allow_blank=True,
        write_only=True,
    )
    payer_priority = serializers.ChoiceField(
        required=False,
        choices=(('Primary', 'Primary'),
                 ('Secondary', 'Secondary'),
                 ('Tertiary', 'Tertiary')),
        write_only=True
    )
    payer_type = serializers.ChoiceField(
        required=False,
        choices=(('Self-Pay', 'Self-Pay'), ('Commercial', 'Commercial'), ('Medicare', 'Medicare'),
                 ('Medicaid', 'Medicaid'), ('Workers Comp', 'Workers\' Compensation'), ('Other', 'Other')))
    payer = PayerSerializer(read_only=True)
    is_primary = serializers.BooleanField(read_only=True)
    is_secondary = serializers.BooleanField(read_only=True)
    is_tertiary = serializers.BooleanField(read_only=True)
    is_workers_comp = serializers.BooleanField(read_only=True)
    uuid = serializers.UUIDField(read_only=True)
    created_date = serializers.DateTimeField(read_only=True)

    def validate(self, attrs):
        patient_uuid = attrs.get('patient_uuid')
        payer_type = attrs.get('payer_type')
        payer_priority = attrs.get('payer_priority')
        payer_uuid = attrs.get('payer_uuid')
        payer_name = attrs.get('payer_name')

        if self.instance:  # PUT specific validation
            patient = self.instance.patient
        else:  # POST specific validation
            if not patient_uuid:
                raise serializers.ValidationError("patient_uuid may not be blank.")
            else:
                patient = patients_models.Patient.objects.get(uuid=patient_uuid)
                attrs.pop('patient_uuid')
                attrs['patient'] = patient

            if not payer_priority:
                raise serializers.ValidationError("payer_priority may not be blank.")
            if not payer_type:
                raise serializers.ValidationError("payer_type may not be blank.")

            if payer_name and payer_uuid:
                raise serializers.ValidationError("Please provider payer_uuid or payer_name.")
            if payer_uuid:
                payer = models.Payer.objects.get(uuid=payer_uuid)
                attrs.pop('payer_uuid')
                attrs['payer'] = payer

        if payer_priority:
            if payer_type != 'Workers Comp':
                if payer_priority == 'Primary':
                    attrs['is_primary'] = True
                    attrs['is_secondary'] = False
                    attrs['is_tertiary'] = False
                elif payer_priority == 'Secondary':
                    attrs['is_primary'] = False
                    attrs['is_secondary'] = True
                    attrs['is_tertiary'] = False
                elif payer_priority == 'Tertiary':
                    attrs['is_primary'] = False
                    attrs['is_secondary'] = False
                    attrs['is_tertiary'] = True
                attrs['is_workers_comp'] = False

        if payer_type:
            if payer_type == 'Workers Comp':
                attrs['is_workers_comp'] = True
                attrs['is_primary'] = False
                attrs['is_secondary'] = False
                attrs['is_tertiary'] = False
            elif payer_type == 'Self-Pay':
                if payer_name:
                    raise serializers.ValidationError("Unable to accept payer_name if payer_type is Self-Pay")
                if payer_uuid:
                    raise serializers.ValidationError("Unable to accept payer_uuid if payer_type is Self-Pay")
            else:
                member_id = attrs.get('member_id')
                if not member_id:
                    raise serializers.ValidationError("member_id is required when payer_type is {}".format(payer_type))

        subscriber_relationship = attrs.get('subscriber_relationship')
        if not subscriber_relationship:
            attrs['subscriber_relationship'] = 'Self'
            subscriber_relationship = 'Self'
        if subscriber_relationship == 'Self':
            attrs['subscriber_last_name'] = patient.user.last_name
            attrs['subscriber_first_name'] = patient.user.first_name
            attrs['subscriber_middle_initial'] = patient.middle_initial
            attrs['subscriber_suffix'] = patient.suffix
            attrs['subscriber_birth_date'] = patient.birth_date
            attrs['subscriber_address1'] = patient.address1
            attrs['subscriber_address2'] = patient.address2
            attrs['subscriber_zip_code'] = patient.zip_code
            attrs['subscriber_sex'] = patient.sex
            attrs['subscriber_phone_number'] = patient.phone_number
            attrs['subscriber_social_security'] = patient.social_security

        return attrs

    def validate_patient_uuid(self, patient_uuid):
        try:
            UUID(patient_uuid, version=4)
            _ = patients_models.Patient.objects.get(uuid=patient_uuid)
        except ValueError:
            raise serializers.ValidationError("Invalid UUID format for patient_uuid.")
        except patients_models.Patient.DoesNotExist:
            raise serializers.ValidationError("Patient matching UUID patient_uuid does not exist.")
        return patient_uuid

    def validate_payer_uuid(self, payer_uuid):
        if payer_uuid:
            try:
                UUID(payer_uuid, version=4)
                _ = models.Payer.objects.get(uuid=payer_uuid)
            except ValueError:
                raise serializers.ValidationError("Invalid UUID format for payer_uuid.")
            except models.Payer.DoesNotExist:
                raise serializers.ValidationError("Payer matching UUID payer_uuid does not exist.")
        return payer_uuid

    def validate_payer_priority(self, payer_priority):
        if payer_priority not in ['Primary', 'Secondary', 'Tertiary']:
            raise serializers.ValidationError(
                "Invalid value for payer_priority. Must be one of the following: Primary, Secondary, Tertiary"
            )
        return payer_priority

    def validate_subscriber_social_security(self, subscriber_social_security):
        if subscriber_social_security:
            subscriber_social_security_numerical = subscriber_social_security.replace('-', '').strip()
            if not len(subscriber_social_security_numerical) == 9:
                raise serializers.ValidationError(
                    "subscriber_social_security must be in the format '###-##-####'"
                )
        return subscriber_social_security

    def validate_payer_type(self, payer_type):
        if payer_type not in ['Self-Pay', 'Commercial', 'Medicare', 'Medicaid', 'Workers Comp', 'Other']:
            raise serializers.ValidationError(
                "payer_type must be one of the following: "
                "Self-Pay, Commercial, Medicare, Medicaid, Workers Comp, or Other"
            )
        return payer_type

    def create(self, validated_data):
        if validated_data.get('payer'):
            payer = validated_data.get('payer')
        elif validated_data.get('payer_name'):
            payer = models.Payer.objects.create(
                name=validated_data.get('payer_name'),
                payer_type=validated_data.get('payer_type')
            )
        else:
            payer = None

        patient_payer = models.PatientPayer.objects.create(
            patient=validated_data.get('patient'),
            payer_type=validated_data.get('payer_type'),
            is_primary=validated_data.get('is_primary', False),
            is_secondary=validated_data.get('is_secondary', False),
            is_tertiary=validated_data.get('is_tertiary', False),
            is_workers_comp=validated_data.get('is_workers_comp', False),
            payer=payer,
            workers_comp_adjuster=validated_data.get('workers_comp_adjuster'),
            workers_comp_claim_id=validated_data.get('workers_comp_claim_id'),
            workers_comp_injury_date=validated_data.get('workers_comp_injury_date'),
            member_id=validated_data.get('member_id'),
            group_number=validated_data.get('group_number'),
            subscriber_relationship=validated_data.get('subscriber_relationship'),
            subscriber_last_name=validated_data.get('subscriber_last_name'),
            subscriber_first_name=validated_data.get('subscriber_first_name'),
            subscriber_middle_initial=validated_data.get('subscriber_middle_initial'),
            subscriber_suffix=validated_data.get('subscriber_suffix'),
            subscriber_birth_date=validated_data.get('subscriber_birth_date'),
            subscriber_address1=validated_data.get('subscriber_address1'),
            subscriber_address2=validated_data.get('subscriber_address2'),
            subscriber_zip_code=validated_data.get('subscriber_zip_code'),
            subscriber_sex=validated_data.get('subscriber_sex'),
            subscriber_phone_number=validated_data.get('subscriber_phone_number'),
            subscriber_social_security=validated_data.get('subscriber_social_security')
        )
        return patient_payer

    def update(self, patient_payer, validated_data):
        if validated_data.get('payer', patient_payer.payer):
            payer = validated_data.get('payer', patient_payer.payer)
        elif validated_data.get('payer_name'):
            payer = models.Payer.objects.create(
                name=validated_data.get('payer_name'),
                payer_type=validated_data.get('payer_type', patient_payer.payer_type)
            )
        else:
            payer = None

        patient_payer.payer_type = validated_data.get('payer_type', patient_payer.payer_type)
        patient_payer.is_primary = validated_data.get('is_primary', patient_payer.is_primary)
        patient_payer.is_secondary = validated_data.get('is_secondary', patient_payer.is_secondary)
        patient_payer.is_tertiary = validated_data.get('is_tertiary', patient_payer.is_tertiary)
        patient_payer.is_workers_comp = validated_data.get('is_workers_comp', patient_payer.is_workers_comp)
        patient_payer.payer = payer
        patient_payer.workers_comp_adjuster = validated_data.get('workers_comp_adjuster', patient_payer.workers_comp_adjuster)
        patient_payer.workers_comp_claim_id = validated_data.get('workers_comp_claim_id', patient_payer.workers_comp_claim_id)
        patient_payer.workers_comp_injury_date = validated_data.get('workers_comp_injury_date', patient_payer.workers_comp_injury_date)
        patient_payer.member_id = validated_data.get('member_id', patient_payer.member_id)
        patient_payer.group_number = validated_data.get('group_number', patient_payer.group_number)
        patient_payer.subscriber_relationship = validated_data.get('subscriber_relationship', patient_payer.subscriber_relationship)
        patient_payer.subscriber_last_name = validated_data.get('subscriber_last_name', patient_payer.subscriber_last_name)
        patient_payer.subscriber_first_name = validated_data.get('subscriber_first_name',
                                                                 patient_payer.subscriber_first_name)
        patient_payer.subscriber_middle_initial = validated_data.get('subscriber_middle_initial',
                                                                     patient_payer.subscriber_middle_initial)
        patient_payer.subscriber_suffix = validated_data.get('subscriber_suffix', patient_payer.subscriber_suffix)
        patient_payer.subscriber_birth_date = validated_data.get('subscriber_birth_date',
                                                                 patient_payer.subscriber_birth_date)
        patient_payer.subscriber_address1 = validated_data.get('subscriber_address1',
                                                               patient_payer.subscriber_address1)
        patient_payer.subscriber_address2 = validated_data.get('subscriber_address2',
                                                               patient_payer.subscriber_address2)
        patient_payer.subscriber_zip_code = validated_data.get('subscriber_zip_code', patient_payer.subscriber_zip_code)
        patient_payer.subscriber_sex = validated_data.get('subscriber_sex', patient_payer.subscriber_sex)
        patient_payer.subscriber_phone_number = validated_data.get('subscriber_phone_number',
                                                                   patient_payer.subscriber_phone_number)
        patient_payer.subscriber_social_security = validated_data.get('subscriber_social_security',
                                                                      patient_payer.subscriber_social_security)
        patient_payer.save()
        return patient_payer

    class Meta:
        model = models.PatientPayer
        fields = (
            # required
            'payer_type', 'patient_uuid',
            # optional
            'payer_priority',
            'payer_uuid',
            'payer_name',
            'member_id', 'group_number', 'subscriber_last_name', 'subscriber_first_name',
            'subscriber_middle_initial', 'subscriber_suffix', 'subscriber_relationship',
            'subscriber_birth_date', 'subscriber_address1', 'subscriber_address2',
            'subscriber_zip_code', 'subscriber_sex', 'subscriber_phone_number', 'subscriber_social_security',
            'workers_comp_injury_date', 'workers_comp_adjuster', 'workers_comp_claim_id',
            # read only
            'payer', 'is_primary', 'is_secondary', 'is_tertiary', 'is_workers_comp', 'uuid', 'created_date',
        )


class PatientGuarantorReadOnlySerializer(serializers.HyperlinkedModelSerializer):
    user = lab_tenant.serializers.UserSerializer()

    class Meta:
        model = models.PatientGuarantor
        fields = ('user', 'relationship_to_patient', 'social_security', 'middle_initial',
                  'suffix', 'birth_date', 'address1', 'address2', 'sex', 'phone_number',
                  'zip_code', 'uuid')


class UserPatientSerializer(serializers.HyperlinkedModelSerializer):
    user = lab_tenant.serializers.UserSerializer()

    class Meta:
        model = models.Patient
        fields = ('uuid', 'user', 'middle_initial', 'birth_date',
                  'address1', 'address2', 'sex', 'phone_number', 'drivers_license',
                  'alternate_id')


class PatientCreateSerializer(serializers.HyperlinkedModelSerializer):
    user = lab_tenant.serializers.PatientUserSerializer()
    birth_date = serializers.DateField(required=True)
    sex = serializers.CharField(required=True)
    provider_uuids = serializers.ListField(
        child=serializers.CharField(),
        required=False,
        min_length=1,
        write_only=True
    )
    primary_provider_uuid = serializers.CharField(
        required=False,
        allow_blank=True,
        write_only=True
    )

    def validate(self, attrs):
        # cleaning goes here
        primary_provider_uuid = attrs.get('primary_provider_uuid', None)
        provider_uuids = attrs.get('provider_uuids')
        if primary_provider_uuid and primary_provider_uuid not in provider_uuids:
            raise serializers.ValidationError("primary_provider_uuid must be in provider_uuids")

        # validate provider_uuids
        if provider_uuids:
            try:
                [UUID(x, version=4) for x in provider_uuids]
                providers = [accounts_models.Provider.objects.get(uuid=x) for x in provider_uuids]
                attrs['providers'] = providers
                attrs.pop('provider_uuids')
            except ValueError:
                raise serializers.ValidationError("Invalid UUID format in provider_uuids.")
            except accounts_models.Provider.DoesNotExist:
                raise serializers.ValidationError("Provider matching UUID in provider_uuids does not exist.")
        else:
            attrs['providers'] = None

        # validate primary_provider_uuid
        if primary_provider_uuid:
            try:
                UUID(primary_provider_uuid, version=4)
                primary_provider = accounts_models.Provider.objects.get(uuid=primary_provider_uuid)
                attrs.pop('primary_provider_uuid')
            except ValueError:
                raise serializers.ValidationError("Invalid UUID format for primary_provider_uuid")
            except accounts_models.Provider.DoesNotExist:
                raise serializers.ValidationError("Provider matching UUID primary_provider_uuid does not exist")
        elif primary_provider_uuid == '':
            attrs.pop('primary_provider_uuid')
            primary_provider = None
        else:
            primary_provider = None
        attrs['primary_provider'] = primary_provider

        # validate sex (M, F, NB)
        sex = attrs.get('sex')
        if sex and sex not in ['M', 'F', 'NB']:
            raise serializers.ValidationError("Sex must be 'M', 'F', or 'NB'.")

        # validate social security
        social_security = attrs.get('social_security')
        if social_security:
            social_security = social_security.replace('-', '').strip()
            if not len(social_security) == 9:
                raise serializers.ValidationError("social_security must be in the format '###-##-####'")

        # validate email
        user_dict = attrs.get('user')
        if user_dict:
            email = user_dict.get('email')
            if email:
                matching_email_users = lab_tenant_models.User.objects.filter(email__iexact=email)
                if matching_email_users:
                    raise serializers.ValidationError("This email address is already in use.")
            return attrs

    def create(self, validated_data):
        user_dict = validated_data.get('user', None)
        providers = validated_data.get('providers')
        primary_provider = validated_data.get('primary_provider')
        [validated_data.pop(x) for x in ['user', 'providers', 'primary_provider']]

        user = lab_tenant_models.User.objects.create(
            first_name=user_dict.get('first_name'),
            last_name=user_dict.get('last_name'),
            user_type=20,
            email=user_dict.get('email', ''),
            username=str(uuid4())
        )
        patient = models.Patient.objects.create(
            user=user,
            primary_care_provider=primary_provider,
            **validated_data
        )
        if providers:
            patient.providers.set(providers)
        return patient

    class Meta:
        model = models.Patient
        fields = (
            # required
            'user', 'middle_initial', 'sex', 'birth_date', 'provider_uuids',
            # not required
            'suffix', 'address1', 'address2', 'zip_code', 'primary_provider_uuid',
            'social_security', 'phone_number', 'drivers_license'
            )


class PayerAutocompleteSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = models.Payer
        fields = (
            'name', 'payer_type', 'payer_code', 'uuid',
        )
