# Generated by Django 2.2.2 on 2019-07-18 15:07

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('orders', '0044_auto_20190717_1030'),
    ]

    operations = [
        migrations.RenameField(
            model_name='test',
            old_name='qa_passed',
            new_name='is_approved',
        ),
    ]
