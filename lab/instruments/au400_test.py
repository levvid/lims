import time

import django
import logging
import os
import platform
import datetime
import sys
import serial
import json
import pathlib
import itertools

from serial.tools import list_ports

from django.db import transaction
from tenant_schemas.utils import tenant_context
from django.utils import timezone

os.environ['DJANGO_SETTINGS_MODULE'] = 'main.settings'
django.setup()

from orders import models as orders_models
from clients import models as clients_models
from lab_tenant import models as lab_tenant_models

########################################################################################################################

PLATFORM_NAME = platform.system()
HOME = str(pathlib.Path.home())

########################################################################################################################

TEXT_START_BLOCK = ['\x02']
TEXT_END_BLOCK = ['\x03']
BLOCK_END_BLOCK = ['E']
DUMMY_BLOCK = [' '] * 4

# Requisition inquiry is what the AU sends the LIS
# Requisition is what the the LIS sends back
REQUISITION_INQUIRY_START_BLOCK = ['R', 'B']
REQUISITION_INQUIRY_END_BLOCK = ['R', 'E']

REQUISITION_START_BLOCK = ['S', ' ']

RESULTS_START_BLOCK = ['D', 'B']
RESULTS_END_BLOCK = ['D', 'E']

# sample types
# ' ' : Serum, 'U': Urine, 'X' : Other
SERUM_BLOCK = [' ']
URINE_BLOCK = ['U']
OTHER_BLOCK = ['X']

########################################################################################################################


def simulate_au400_test_requisition_inquiry(tenant, instrument_name, platform_name):
    with tenant_context(tenant):
        # need tenant context
        instrument = lab_tenant_models.InstrumentIntegration.objects.get(internal_instrument_name=instrument_name)

        if platform_name == 'Darwin':
            port = '/dev/cu.usbserial'
        elif platform_name == 'Linux':
            port = '/dev/ttyUSB0'
        else:
            port = '/dev/ttyUSB0'

        ser = serial.Serial(
            port=port,
            baudrate=9600,
            parity=serial.PARITY_NONE,
            stopbits=serial.STOPBITS_ONE,
            bytesize=serial.EIGHTBITS,
            timeout=0.5,
            write_timeout=50)

        print("connected to: " + ser.portstr)

        char_list = []

        # ask for test codes for all samples with a barcode
        samples = orders_models.Sample.objects.exclude(barcode__isnull=True).exclude(barcode__exact='')

        requisition_inquiry_start = TEXT_START_BLOCK + REQUISITION_INQUIRY_START_BLOCK + TEXT_END_BLOCK
        requisition_inquiry_end = TEXT_START_BLOCK + REQUISITION_INQUIRY_END_BLOCK + TEXT_END_BLOCK

        for sample_number, sample in enumerate(samples, start=1):
            sample_type = sample.clia_sample_type.name

            if sample_type == 'Urine':
                sample_type_block = URINE_BLOCK
            elif sample_type == 'Serum':
                sample_type_block = SERUM_BLOCK
            else:
                sample_type_block = OTHER_BLOCK

            sample_number_block = list(str(sample_number).zfill(4))
            barcode_block = list(sample.barcode)

            requisition_inquiry = requisition_inquiry_start + sample_type_block + sample_number_block + barcode_block + \
                                  requisition_inquiry_end

            # send requisition inquiry
            for char in requisition_inquiry:
                ser.write(str.encode(char))
            # for char in end_transmission:
            #     ser.write(str.encode(char))
            print('Requisition inquiry sent.')


if __name__ == '__main__':
    logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)
    output_pathfile = '{}/au400/input/'.format(HOME)
    tenant_name = sys.argv[1]
    tenant = clients_models.Client.objects.get(schema_name=tenant_name)
    simulate_au400_test_requisition_inquiry(tenant, 'Olympus AU400', PLATFORM_NAME)


