import csv
import datetime
import django
import os
import tempfile
import xlsxwriter

os.environ['DJANGO_SETTINGS_MODULE'] = 'main.settings'
django.setup()

from . import models
from main.functions import convert_tenant_timezone_to_utc, convert_utc_to_tenant_timezone, date_string_or_none, \
    convert_timezone_to_utc, convert_utc_to_timezone
from clients import models as clients_models

DATETIME_STRING_FORMAT = "%m/%d/%Y"
HEADER_ROW = ['Order Code', 'Sample Code', 'Test', 'Account ID', 'External Account ID', 'Account',
              'Provider', 'Result', 'Due Date', 'Order Submitted Date', 'Order Received Date',
              'Test Result Dates', 'Test Approved Passed Date', 'Reported Date', 'Turnaround Time (Hours)']


def datetime_or_blank_string(input, datetime_string_format=DATETIME_STRING_FORMAT):
    if input:
        return input.strftime(datetime_string_format)
    else:
        return ''


def generate_excel_report(tenant_id, report_id, start_date, end_date, date_filter='Received'):
    """
    Create a xlsx file using NamedTemporaryFile and write data and metrics.
    Create a OperationalReport object and save file to it
    :param request:
    :param start_date: string
    :param end_date: string
    :param date_filter: 'Received' (filter by Order.received_date) or 'Reported' (filter by Test.initial_report_date)
    :return:
    """
    def write_metrics(worksheet, metrics_dict):
        # Set column widths
        worksheet.set_column(0, 0, 25)
        worksheet.set_column(1, 5, 15)

        test_type_names = sorted(metrics_dict['accessions'].keys())

        row, col = 2, 0
        if date_filter == 'Received':
            worksheet.write(0, 0, "Accessions received from {} to {}".format(start_date, end_date))
        elif date_filter == 'Reported':
            worksheet.write(0, 0, "Accessions reported from {} to {}".format(start_date, end_date))
        received_total = 0
        completed_total = 0
        positive_total = 0
        worksheet.write(1, 0, 'Test name')
        worksheet.write(1, 1, 'Received')
        worksheet.write(1, 2, 'Completed')
        worksheet.write(1, 3, 'Positive Result')
        worksheet.write(1, 4, '% Positive')
        worksheet.write(1, 5, 'Avg Turnaround Time (Hours)')

        for test_type_name in test_type_names:
            num_received = metrics_dict['accessions'][test_type_name]
            try:
                num_completed = metrics_dict['completed'][test_type_name]
            except KeyError:
                num_completed = 0
            try:
                num_positive = metrics_dict['positive_results'][test_type_name]
            except KeyError:
                num_positive = 0
            try:
                percent_positive = round(num_positive/ num_completed, 2)
            except ZeroDivisionError:
                percent_positive = ''
            try:
                turnaround_list = metrics_dict['turnaround_times'][test_type_name]
                avg_turnaround = round(sum(turnaround_list)/len(turnaround_list), 2)
            except KeyError:
                avg_turnaround = ''

            worksheet.write(row, col, test_type_name)
            worksheet.write(row, col + 1, num_received)
            worksheet.write(row, col + 2, num_completed)
            worksheet.write(row, col + 3, num_positive)
            worksheet.write(row, col + 4, percent_positive)
            worksheet.write(row, col + 5, avg_turnaround)

            row += 1
            received_total += num_received
            positive_total += num_positive
            completed_total += num_completed

        worksheet.write(row, col, 'Total:')
        worksheet.write(row, col + 1, received_total)
        worksheet.write(row, col + 2, completed_total)
        worksheet.write(row, col + 3, positive_total)
        try:
            worksheet.write(row, col + 4, round(positive_total / completed_total, 2))
        except ZeroDivisionError:
            pass

    def write_data(worksheet, timezone):
        # Set column widths
        worksheet.set_column(0, 1, 20)
        worksheet.set_column(2, 2, 30)
        worksheet.set_column(4, 4, 15)
        worksheet.set_column(5, 5, 30)
        worksheet.set_column(6, 6, 15)
        worksheet.set_column(8, 8, 15)
        worksheet.set_column(9, 13, 25)

        metrics = {'positive_results': {},
                   'completed': {},
                   'accessions': {},
                   'turnaround_times': {}}

        # convert to datetime objects first
        start_datetime = datetime.datetime.combine(start_date, datetime.time.min)
        end_datetime = datetime.datetime.combine(end_date, datetime.time.min) + datetime.timedelta(days=1)
        # convert start and end dates from tenant's timezone to UTC
        query_start_date = convert_timezone_to_utc(timezone, start_datetime)
        query_end_date = convert_timezone_to_utc(timezone, end_datetime)

        if date_filter == 'Received':
            tests = models.Test.objects.filter(order__received_date__gte=query_start_date,
                                               order__received_date__lt=query_end_date,
                                               order__is_active=True).\
                order_by('order__received_date').\
                prefetch_related('order__account', 'test_type', 'sample', 'result', 'order__provider',
                                 'order__provider__user')
        elif date_filter == 'Reported':
            tests = models.Test.objects.filter(initial_report_date__gte=start_date,
                                               initial_report_date__lt=query_end_date,
                                               order__is_active=True).\
                order_by('initial_report_date').\
                prefetch_related('order__account', 'test_type', 'sample', 'result', 'order__provider',
                                 'order__provider__user')

        row, col = 0, 0
        for header in HEADER_ROW:
            worksheet.write(row, col, header)
            col += 1
        col = 0
        row = 1

        for test in tests:
            order = test.order
            # formatting logic
            if test.test_type:
                test_type_name = str(test.test_type.name)
            else:
                test_type_name = ''

            if test.initial_report_date and order.received_date:
                difference = (test.initial_report_date - order.received_date)
                turnaround_time = round(difference.seconds / 3600, 2) + (difference.days * 24)
            else:
                turnaround_time = ''

            result = test.result.get()
            # convert UTC results back to tenant timezone
            submitted_datetime = datetime_or_blank_string(convert_utc_to_timezone(timezone, order.submitted_date))
            received_datetime = datetime_or_blank_string(convert_utc_to_timezone(timezone, order.received_date))
            test_result_datetime = datetime_or_blank_string(convert_utc_to_timezone(timezone, result.submitted_datetime))
            test_approved_datetime = datetime_or_blank_string(convert_utc_to_timezone(timezone, test.approved_date))
            reported_datetime = datetime_or_blank_string(convert_utc_to_timezone(timezone, test.initial_report_date))
            # don't convert due date
            due_date = date_string_or_none(test.due_date)

            if order.account.alternate_id:
                external_account_id = order.account.alternate_id
            else:
                external_account_id = ''
            account_id = str(order.account.id)

            data_row = [str(order.code), str(test.sample.code), test_type_name, account_id,
                        external_account_id, order.account.name,
                        str(order.provider), str(result.result), due_date, submitted_datetime,
                        received_datetime, test_result_datetime, test_approved_datetime, reported_datetime,
                        turnaround_time]
            for data_cell in data_row:
                worksheet.write(row, col, data_cell)
                col += 1
            row += 1
            col = 0
            if turnaround_time:
                try:
                    metrics['turnaround_times'][test_type_name].append(turnaround_time)
                except KeyError:
                    metrics['turnaround_times'][test_type_name] = [turnaround_time]

            if result.result and test.initial_report_date:
                try:
                    metrics['completed'][test_type_name] += 1
                except KeyError:
                    metrics['completed'][test_type_name] = 1
            if result.result == '+' and test.initial_report_date:
                try:
                    metrics['positive_results'][test_type_name] += 1
                except KeyError:
                    metrics['positive_results'][test_type_name] = 1
            try:
                metrics['accessions'][test_type_name] += 1
            except KeyError:
                metrics['accessions'][test_type_name] = 1

        return metrics

    tenant = clients_models.Client.objects.get(id=tenant_id)
    start_date = datetime.datetime.strptime(start_date[:10], '%Y-%m-%d')
    end_date = datetime.datetime.strptime(end_date[:10], '%Y-%m-%d')
    with tempfile.NamedTemporaryFile() as temp:
        workbook = xlsxwriter.Workbook(temp.name)
        # Data sheet
        data_worksheet = workbook.add_worksheet('Data')
        metrics = write_data(data_worksheet, tenant.timezone)
        # Results sheet
        results_sheet = workbook.add_worksheet('Metrics')
        write_metrics(results_sheet, metrics)
        workbook.close()

        report = models.OperationalReport.objects.get(id=report_id)
        # Save report
        save_file_name = 'operational_reports/{}/{}.xlsx'.format(tenant.name, report.uuid)
        report.report.save(save_file_name, temp)


def generate_csv_report(tenant_id, report_id, start_date, end_date, date_filter='Received'):
    """
    Create a csv file using NamedTemporaryFile and write data.
    Create a OperationalReport object and save file to it
    :param request:
    :param start_date: string
    :param end_date: string
    :param date_filter: 'Received' (filter by Order.received_date) or 'Reported' (filter by Test.initial_report_date)
    :return:
    """
    tenant = clients_models.Client.objects.get(id=tenant_id)

    # convert to datetime objects first
    try:
        start_datetime = datetime.datetime.combine(start_date, datetime.time.min)
    except:
        # convert to date objects
        start_date = datetime.datetime.strptime(start_date[:10], '%Y-%m-%d')
        start_datetime = datetime.datetime.combine(start_date, datetime.time.min)
    try:
        end_datetime = datetime.datetime.combine(end_date, datetime.time.min) + datetime.timedelta(days=1)
    except:
        end_date = datetime.datetime.strptime(end_date[:10], '%Y-%m-%d')
        end_datetime = datetime.datetime.combine(end_date, datetime.time.min) + datetime.timedelta(days=1)
    # convert start and end dates from tenant's timezone to UTC for query
    query_start_date = convert_timezone_to_utc(tenant.timezone, start_datetime)
    query_end_date = convert_timezone_to_utc(tenant.timezone, end_datetime)

    if date_filter == 'Received':
        tests = models.Test.objects.filter(order__received_date__gte=query_start_date,
                                           order__received_date__lt=query_end_date,
                                           order__is_active=True).\
            order_by('order__received_date').\
            prefetch_related('order__account', 'test_type', 'sample', 'result', 'order__provider',
                             'order__provider__user').all()
    else:
        tests = models.Test.objects.filter(initial_report_date__gte=query_start_date,
                                           initial_report_date__lt=query_end_date,
                                           order__is_active=True).\
            order_by('initial_report_date').\
            prefetch_related('order__account', 'test_type', 'sample', 'result', 'order__provider',
                             'order__provider__user').all()

    with tempfile.NamedTemporaryFile() as temp:
        with open(temp.name, 'w') as csvfile:
            filewriter = csv.writer(csvfile, delimiter=',')
            filewriter.writerow(HEADER_ROW)

            for test in tests:
                order = test.order
                # formatting logic
                if test.test_type:
                    test_type_name = str(test.test_type.name)
                else:
                    test_type_name = ''

                if test.initial_report_date and order.received_date:
                    difference = (test.initial_report_date - order.received_date)
                    turnaround_time = round(difference.seconds/3600, 2) + (difference.days * 24)
                else:
                    turnaround_time = ''

                result = test.result.get()
                # convert UTC results back to tenant timezone
                submitted_datetime = datetime_or_blank_string(
                    convert_utc_to_timezone(tenant.timezone, order.submitted_date))
                received_datetime = datetime_or_blank_string(
                    convert_utc_to_timezone(tenant.timezone, order.received_date))
                test_result_datetime = datetime_or_blank_string(
                    convert_utc_to_timezone(tenant.timezone, result.submitted_datetime))
                test_approved_datetime = datetime_or_blank_string(
                    convert_utc_to_timezone(tenant.timezone, test.approved_date))
                reported_datetime = datetime_or_blank_string(
                    convert_utc_to_timezone(tenant.timezone, test.initial_report_date))
                # don't convert due date
                due_date = date_string_or_none(test.due_date)

                if order.account.alternate_id:
                    external_account_id = order.account.alternate_id
                else:
                    external_account_id = ''
                account_id = str(order.account.id)

                filewriter.writerow([str(order.code), str(test.sample.code), test_type_name,
                                     account_id, external_account_id, order.account.name, str(order.provider),
                                     str(result.result), due_date, submitted_datetime,
                                     received_datetime, test_result_datetime, test_approved_datetime,
                                     reported_datetime, turnaround_time])

        report = models.OperationalReport.objects.get(id=report_id)
        save_file_name = 'operational_reports/{}/{}.csv'.format(tenant.name, report.uuid)
        report.report.save(save_file_name, temp)

    return save_file_name
