import csv
import django
import requests
import os
from django.db import transaction

os.environ['DJANGO_SETTINGS_MODULE'] = 'main.settings'
django.setup()

from codes import models as codes_models

CODE_MAP_SOURCE_FILE_URL = 'https://dendi-lis.s3.amazonaws.com/client_data/mako/codemap_Mako_01_01_2019.csv'
CPT_SOURCE_FILE_URL = 'https://dendi-lis.s3.amazonaws.com/client_data/public/CPT2020/CPTLong202019092548.csv'
HCPCS_II_SOURCE_FILE_URL = 'https://dendi-lis.s3.amazonaws.com/client_data/mako/HCPC2020_ANWEB_w_disclaimer.csv'

"""
Sourced CPT code index from 
https://www.cms.gov/Medicare/Coding/HCPCSReleaseCodeSets/Alpha-Numeric-HCPCS-Items/2020-Alpha-Numeric-HCPCS-File
"""


@transaction.atomic
def main():
    """
    script used to instantiate ProcedureCodeTypes, import HCPCS level I & II procedure codes, and
    import cpt/icd10 relationship provided by Mako.
    :return:
    """
    # instantiate ProcedureCodeType objects
    cpt_code_type, hcpcs_lv_2_code_type = create_procedure_code_types()

    # import CPT codes
    if codes_models.ProcedureCode.objects.filter(procedure_code_type=cpt_code_type).exists():
        print("CPT codes already exist")
    else:
        print("Importing CPT codes")
        import_cpt_codes_from_url(CPT_SOURCE_FILE_URL, cpt_code_type)
    # import HCPCS II codes
    if codes_models.ProcedureCode.objects.filter(procedure_code_type=hcpcs_lv_2_code_type).exists():
        print("HCPCS Level II codes already exist")
    else:
        print("Importing HCPCS Level II codes")
        import_HCPCS_II_codes_from_url(HCPCS_II_SOURCE_FILE_URL, hcpcs_lv_2_code_type)

    # import procedure code to ICD10 code mapping
    code_map_dicts = parse_procedure_code_to_icd10_code_csv_from_url(CODE_MAP_SOURCE_FILE_URL)
    save_code_maps(code_map_dicts)

    return True


def save_code_maps(code_map_dicts):
    """

    :param code_map_dicts:
    :return:
    """
    # create dictionary of ProcedureCode objects to minimize IO
    procedure_codes = list(set([x['cpt_code'] for x in code_map_dicts]))
    procedure_codes_dict = {}
    for procedure_code in procedure_codes:
        try:
            procedure_code_object = codes_models.ProcedureCode.objects.get(code=procedure_code)
            procedure_codes_dict[procedure_code] = procedure_code_object
        except codes_models.ProcedureCode.DoesNotExist:
            print("{} does not exist in ProcedureCode".format(procedure_code))

    # create dictionary of ICD10 codes objects to minimize IO
    icd10_codes_dict = {}
    all_icd10_codes = list(codes_models.ICD10CM.objects.all())
    for icd10_code in all_icd10_codes:
        icd10_codes_dict[icd10_code.full_code] = icd10_code


    relationship_dict = {}
    for code_map in code_map_dicts:
        procedure_code_string = code_map['cpt_code']
        icd10_code_string = code_map['icd10_code'].replace('.', '')

        if procedure_code_string not in ['81211', '81213', '81214']:  # ignore few expired cpt codes
            procedure_code_obj = procedure_codes_dict[procedure_code_string]
            icd10_code_obj = icd10_codes_dict[icd10_code_string]

            try:
                relationship_dict[procedure_code_obj].append(icd10_code_obj)
            except KeyError:
                relationship_dict[procedure_code_obj] = [icd10_code_obj]
    
    # save code map
    for procedure_code_obj in relationship_dict:
        print('setting saving {} icd 10 codes to procedure code {}'.format(
            len(relationship_dict[procedure_code_obj]),
            procedure_code_obj.code
        ))
        procedure_code_obj.icd10_codes.set(relationship_dict[procedure_code_obj])


def import_cpt_codes_from_url(file_url, procedure_type_object):
    """

    :param file_url:
    :param procedure_type_object:
    :return:
    """
    def parse_cpt_csv_from_url(file_url):
        with requests.Session() as s:
            download = s.get(file_url)
            decoded_content = download.content.decode('utf-8')
            csv_reader = csv.reader(decoded_content.splitlines())
            csv_rows = list(csv_reader)[6:]
        cleaned_rows = []
        for row in csv_rows:
            cpt_code = row[0].strip().upper()
            row_dict = {
                'code': cpt_code,
                'long_description': row[1].strip()
            }
            cleaned_rows.append(row_dict)
        return cleaned_rows

    procedure_code_dicts = parse_cpt_csv_from_url(file_url)
    procedure_codes = [codes_models.ProcedureCode(
        code=x['code'],
        description=x['long_description'],
        procedure_code_type=procedure_type_object
    ) for x in procedure_code_dicts]
    procedure_codes = codes_models.ProcedureCode.objects.bulk_create(procedure_codes, batch_size=3000)

    return procedure_codes


def create_procedure_code_types():
    """
    Add CPT and HCPCS Level II procedure code types to public schema
    :return:
    """
    cpt_obj, _ = codes_models.ProcedureCodeType.objects.update_or_create(
        name='CPT',
        defaults={'description': 'HCPCS Level I CPT or Current Procedural Terminology codes are made up of 5 digit '
                                 'numbers and managed by the American Medical Association (AMA). CPT codes are used '
                                 'to identify medical services and procedures ordered by physicians or other licensed '
                                 'professionals'})
    hcpcs_lv_2_obj, _ = codes_models.ProcedureCodeType.objects.update_or_create(
        name='HCPCS AN',
        defaults={'description': 'HCPCS Level II are alphanumeric codes consisting of one alphabetical letter '
                                 'followed by four numbers and are managed by The Centers for Medicare and Medicaid '
                                 'Services (CMS). These codes identify non-physician services such as ambulance '
                                 'services, durable medical equipment, and pharmacy. These are typically not costs '
                                 'that get passed through a physician\'s office so they must be dealt with by'
                                 ' Medicare or Medicaid differently from the way a health insurance company would '
                                 'deal with them.'})
    return cpt_obj, hcpcs_lv_2_obj


def parse_procedure_code_to_icd10_code_csv_from_url(file_url):
    """
    function used to parse cpt/icd10 relationship csv
    :param file_url:
    :return: [Dict]
    """
    with requests.Session() as s:
        download = s.get(file_url)
        decoded_content = download.content.decode('utf-8')
        csv_reader = csv.reader(decoded_content.splitlines())
        csv_rows = list(csv_reader)[1:]

    cleaned_rows = []
    for row in csv_rows:
        is_supported = row[3]
        if is_supported.strip().lower() == 'yes':
            is_supported = True
        else:
            is_supported = False

        row_dict = {
            'converted_ncd_rule': row[0],
            'cpt_code': row[1],
            'icd10_code': row[2],
            'is_supported': is_supported,
            'effective_date': row[4],
            'discontinue_date': row[5]
        }
        cleaned_rows.append(row_dict)
    return cleaned_rows


def import_HCPCS_II_codes_from_url(file_url, procedure_type_object):
    """
    create and save ProcedureCode objects
    :param file_url:
    :param procedure_type_object:
    :return:
    """

    def parse_HCPCS_II_csv_from_url(file_url):
        with requests.Session() as s:
            download = s.get(file_url)
            decoded_content = download.content.decode('utf-8')
            csv_reader = csv.reader(decoded_content.splitlines())
            csv_rows = list(csv_reader)[11:]

        cleaned_rows = []
        for row in csv_rows:
            sequence_num = row[1].strip()
            cpt_code = row[0].strip().upper()
            if sequence_num == '0010':
                row_dict = {
                    'code': cpt_code,
                    'long_description': row[3].strip(),
                    'short_description': row[4].strip()
                }
                cleaned_rows.append(row_dict)
            else:  # this csv has continuing rows for descriptions
                cleaned_rows[-1]['long_description'] += ' ' + row[3].strip()
        return cleaned_rows

    procedure_code_dicts = parse_HCPCS_II_csv_from_url(file_url)
    procedure_codes = [codes_models.ProcedureCode(
        code=x['code'],
        description=x['long_description'],
        procedure_code_type=procedure_type_object
    ) for x in procedure_code_dicts]
    procedure_codes = codes_models.ProcedureCode.objects.bulk_create(procedure_codes)
    return procedure_codes


if __name__ == '__main__':
    main()
