import base64
import copy
import datetime
import json
import os
import decimal
import pathlib

from braces.views import LoginRequiredMixin
from dal import autocomplete
from django.contrib import messages
from django.core.files.base import ContentFile
from django.db import transaction
from django.forms import formset_factory
from django.http import HttpResponseRedirect, Http404, HttpResponse, JsonResponse
from django.shortcuts import render, redirect, get_object_or_404
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import View
from django.urls import reverse
from django.utils import timezone
from django.utils.html import format_html

from orders import data_views
from orders import forms
from orders import functions
from orders import models
from orders import tasks
from patients import models as patients_models

from lab_tenant.views import LabMemberMixin, LISLoginRequiredMixin
from lab import models as lab_models
from accounts import models as accounts_models
from lab_tenant import models as lab_tenant_models
from lab.functions import galaxy_ifa_parse


from main.functions import convert_utc_to_tenant_timezone, convert_tenant_timezone_to_utc, date_string_or_none
from orders.functions import get_test_method_abbreviated_names, get_accession_details, update_test_due_dates, \
    check_for_offsite_lab_tests, accession_order_helper, accession_sample_helper, accession_container_helper

ALLOWED_FILE_EXTENSIONS = ['png', 'svg', 'pdf', 'jpeg', 'jpg']


# Create your views here.
class AccessionCheckView(LabMemberMixin, View):
    """
    A view for checking current day's accessions

    Originally requested by Galaxy for double checking purposes
    """

    def get(self, request):
        orders = models.Order.objects.filter(submitted=True)
        test_types = models.TestType.objects.all()

        context = {'orders': orders,
                   'test_types': test_types}
        return render(request, 'accession_check.html', context=context)


class WorkqueueView(LabMemberMixin, View):
    def get(self, request):
        return render(request, 'workqueue.html')


class WorkqueueIssuesView(LISLoginRequiredMixin, View):

    def get(self, request):
        if request.user.user_type == 10:
            provider = accounts_models.Provider.objects.get(user__id=request.user.id)
        else:
            provider = None

        orders = models.Order.objects.filter(submitted=True)

        ################################################################################################################
        # this code isn't necessary, but for some reason required to run. cannot figure it out
        # deleting this or the {{ forms.media }} section in workqueue_issues.html will cause an error
        if provider:
            orders = orders.filter(provider=provider)
            order_autocomplete_form = forms.OrderAutocompleteForm(initial={'provider': provider})
        else:
            order_autocomplete_form = forms.OrderAutocompleteForm()
        ################################################################################################################

        issue_types = models.OrderIssue._meta.get_field('issue_type').choices

        context = {'orders': orders,
                   'form': order_autocomplete_form,
                   'issue_types': issue_types}

        return render(request, 'workqueue_issues.html', context=context)


class WorkqueueTestView(LabMemberMixin, View):
    def get(self, request):
        # show all tests without results as well as tests with results that haven't been reported yet
        test_methods = sorted(
            list(set([(x.test_method.name, x.test_method.id) for x in
                      models.TestType.objects.filter(test_method_id__isnull=False).prefetch_related('test_method')])),
            key=lambda y: y[0].lower())
        context = {'test_methods': test_methods}
        """
        pending_tests = models.Test.objects.filter(
            result__result__isnull=True,
            order__is_active=True,
            order__submitted=True
        )
        test_methods = sorted(
            list(set(list([(x.test_method.name, x.test_method.id) for x in
                           models.TestType.objects.all().prefetch_related('test_method') if x.test_method]))),
            key=lambda y: y[0].lower())
        context = {'test_methods': [x[0] for x in test_methods]}
        test_methods_pending_count = [pending_tests.filter(test_type__test_method__id=x[1]).count() for x in
                                      test_methods]
        test_methods = list(zip([x[0] for x in test_methods], test_methods_pending_count))

        t1 = timeit.default_timer()
        print("{} seconds".format(round(t1-t0, 2)))
        """

        return render(request, 'workqueue_tests.html', context)


class TestResultListView(LabMemberMixin, View):
    def get(self, request):
        # get distinct targets
        distinct_target_ids = models.TestType.objects.values_list('test_target_id').distinct()
        distinct_targets = lab_models.TestTarget.objects.filter(id__in=distinct_target_ids).order_by('name')
        distinct_targets = [(target.uuid, str(target)) for target in distinct_targets]

        # get distinct accounts (filter by provider using Handsontable)
        accounts = [(account.uuid, str(account)) for account in accounts_models.Account.objects.all()]

        return render(request, 'test_result_list.html', {'distinct_targets': distinct_targets,
                                                         'accounts': accounts})


class WorkqueuePendingApprovalView(LISLoginRequiredMixin, View):
    def get(self, request):
        context = {}
        return render(request, 'workqueue_pending_approval.html', context)


class WorkqueueReportedView(LISLoginRequiredMixin, View):
    def get(self, request):
        context = {}
        return render(request, 'workqueue_reported.html', context)


class WorkqueueUnreceivedView(LISLoginRequiredMixin, View):
    def get(self, request):

        order_autocomplete_form = forms.UnreceivedOrderAutocompleteForm()

        context = {'form': order_autocomplete_form}
        return render(request, 'workqueue_unreceived.html', context)

    def post(self, request):
        order_autocomplete_form = forms.UnreceivedOrderAutocompleteForm(request.POST)
        context = {'form': order_autocomplete_form}

        if order_autocomplete_form.is_valid():
            order = order_autocomplete_form.cleaned_data['order']
            return redirect('order_details', order_code=order.code)
        else:
            return render(request, 'workqueue_unreceived.html', context)


class PendingTestView(LabMemberMixin, View):
    def get_result_forms(self, request, test_method):
        shown_result_count = 30
        pending_test_count = models.Test.objects.filter(result__result__isnull=True,
                                                        test_type__test_method__name=test_method,
                                                        order__is_active=True,
                                                        order__submitted=True).count()

        # show all tests without results as well as tests with results that haven't been reported yet
        results = models.Result.objects.filter(test__test_type__test_method__name=test_method,
                                               order__is_active=True,
                                               order__submitted=True,
                                               order__received_date__isnull=False,
                                               result__isnull=True). \
                      prefetch_related('test', 'order', 'user', 'test__sample',
                                       'test__test_type').order_by('test__sample__code')[:shown_result_count]
        test_method_obj = lab_models.TestMethod.objects.get(name=test_method)

        ResultFormSet = formset_factory(forms.ResultForm)
        qualitative_test_data = [{'result_obj': x,
                                  'due_date': x.test.due_date,
                                  'result_label': x.result_info_label,
                                  'result_label_prefix': x.result_info_label_prefix,
                                  'reference': x.reference,
                                  'result': x.result,
                                  'user': x.user}
                                 for x in results if
                                 x.test.test_type and x.test.test_type.result_type == 'Qualitative']
        quantitative_test_data = [{'result_obj': x,
                                   'due_date': x.test.due_date,
                                   'result_quantitative': x.result,
                                   'result_label_prefix': x.result_info_label_prefix,
                                   'result_label': x.result_info_label,
                                   'reference': x.reference,
                                   'user': x.user}
                                  for x in results if
                                  x.test.test_type and x.test.test_type.result_type == 'Quantitative']
        # merge and sort both types
        test_data = sorted(qualitative_test_data + quantitative_test_data, key=lambda y: (
            # having a test target isn't currently enforced
            y['result_obj'].test.sample.code, getattr(
                y['result_obj'].test.test_type.test_target, 'name', None)
        ))

        if request.method == 'POST':
            result_formset = ResultFormSet(request.POST, form_kwargs={
                'test_method': test_method_obj})
        else:
            result_formset = ResultFormSet(initial=test_data, form_kwargs={
                'test_method': test_method_obj})

        data_and_formset = zip(test_data, result_formset)

        context = {
            'pending_test_count': pending_test_count,
            'shown_result_count': shown_result_count,
            'test_method': test_method_obj,
            'result_formset': result_formset,
            'data_and_formset': data_and_formset}
        return context

    def get(self, request, test_method):
        context = self.get_result_forms(request, test_method)
        return render(request, 'pending_test_list.html', context)

    def post(self, request, test_method):
        context = self.get_result_forms(request, test_method)
        result_formset = context['result_formset']
        data_and_formset = context['data_and_formset']

        if result_formset.is_valid():
            result_list = []
            test_list = []
            for data, result_form in data_and_formset:
                if result_form.is_valid():
                    result_obj = data['result_obj']
                    due_date = result_form.cleaned_data.get('due_date')
                    result_quantitative = None
                    # used for Galaxy IFA and QuantStudio integration
                    result_info_label = result_form.cleaned_data.get('result_label')
                    result_info_label_prefix = result_form.cleaned_data.get('result_label_prefix')
                    reference = result_form.cleaned_data.get('reference')

                    # IFA result formset is for Galaxy only for now
                    if test_method == 'IFA' and request.tenant.name == 'Galaxy Diagnostics':
                        result = galaxy_ifa_parse(result_form.cleaned_data.get('result'), result_info_label,
                                                  result_info_label_prefix)

                    elif result_obj.test.test_type.result_type == 'Qualitative':
                        result = result_form.cleaned_data.get('result')
                    # using "below range" "within range", and "above range" instead of positive or negative
                    elif result_obj.test.test_type.test_method.is_within_range:
                        result_quantitative = result_form.cleaned_data.get('result_quantitative')
                        if result_quantitative:
                            result_quantitative = decimal.Decimal(result_quantitative).quantize(
                                decimal.Decimal('.00000001'), rounding=decimal.ROUND_DOWN)
                            if result_quantitative >= decimal.Decimal('99999999.99999999'):
                                result_quantitative = decimal.Decimal('99999999.99999999')
                        try:
                            low_value = decimal.Decimal(
                                result_obj.test.test_type.test_type_ranges.get().range_low)
                            high_value = decimal.Decimal(
                                result_obj.test.test_type.test_type_ranges.get().range_high)
                        except models.TestTypeRange.DoesNotExist:
                            low_value = None  # if cutoff is not provided
                            high_value = None
                        result = result_form.cleaned_data.get('result')
                        # 0.00 is treated as false value
                        has_quantitative_result = result_quantitative or result_quantitative == Decimal('0.00')
                        if not result and low_value and high_value and has_quantitative_result:
                            if result_quantitative >= low_value and result_quantitative <= high_value:
                                result = '~'
                            elif result_quantitative < low_value:
                                result = '<'
                            else:
                                result = '>'
                    else:  # Quantitative
                        try:
                            result_quantitative = decimal.Decimal(
                                result_form.cleaned_data.get('result_quantitative')).quantize(
                                decimal.Decimal('.00000001'), rounding=decimal.ROUND_DOWN)
                            if result_quantitative >= decimal.Decimal('99999999.99999999'):
                                result_quantitative = decimal.Decimal('99999999.99999999')
                        except TypeError:
                            result_quantitative = None
                        positive_above_cutoff = result_obj.test.test_type.test_method.positive_above_cutoff
                        try:
                            if positive_above_cutoff:  # fault tolerance for cutoff not existing
                                cutoff_value = decimal.Decimal(
                                    result_obj.test.test_type.test_type_ranges.filter(default=True).get().range_high)
                            else:
                                cutoff_value = decimal.Decimal(
                                    result_obj.test.test_type.test_type_ranges.filter(
                                        default=True).get().range_low)
                        except models.TestTypeRange.DoesNotExist:
                            cutoff_value = None  # if cutoff is not provided

                        #  if result is provided manually, then use that value
                        result = result_form.cleaned_data.get('result')
                        # 0.00 is treated as false value
                        has_quantitative_result = result_quantitative or result_quantitative == Decimal('0.00')
                        if not result and cutoff_value and has_quantitative_result:
                            if positive_above_cutoff:
                                if result_quantitative >= cutoff_value:
                                    result = '+'
                                else:
                                    result = '-'
                            else:  # negative above cutoff
                                if result_quantitative >= cutoff_value:
                                    result = '-'
                                else:
                                    result = '+'

                    if (result, result_info_label, result_info_label_prefix, reference, result_quantitative) != (
                            result_obj.result,
                            result_obj.result_info_label, result_obj.result_info_label_prefix, result_obj.reference,
                            result_obj.result_quantitative):
                        result_obj.result = result
                        result_obj.submitted_datetime = timezone.now()
                        result_obj.result_info_label = result_info_label
                        result_obj.result_info_label_prefix = result_info_label_prefix
                        result_obj.user = request.user
                        result_obj.reference = reference
                        result_obj.test.is_approved = False
                        result_obj.test.initial_report_date = None
                        result_obj.result_quantitative = result_quantitative
                        result_list.append(result_obj)
                    if due_date:
                        test = result_obj.test
                        test.due_date = due_date
                        test_list.append(test)
                else:
                    messages.error(request, 'Quantitative measurements can not contain non numeric characters')
            models.Result.objects.bulk_update(result_list,
                                              ['result', 'submitted_datetime', 'result_info_label',
                                               'result_info_label_prefix', 'user', 'reference',
                                               'test', 'result_quantitative'])
            models.Test.objects.bulk_update(test_list,
                                            ['due_date', 'is_approved', 'initial_report_date'])

        return redirect('pending_test_list', test_method=test_method)


class SampleListView(LabMemberMixin, View):
    # list of all samples
    def get(self, request):
        # get distinct targets
        distinct_target_ids = models.TestType.objects.values_list('test_target_id').distinct()
        distinct_targets = lab_models.TestTarget.objects.filter(id__in=distinct_target_ids).order_by('name')

        # get only test methods that are applicable to the lab --> for example, no tox lab does ELISA testing
        # unconventional way to get test methods by querying test targets with unique test method values
        test_method_ids = models.TestType.objects.order_by('test_method').values('test_method').distinct()
        test_method_list = lab_models.TestMethod.objects.filter(id__in=test_method_ids)

        distinct_targets = [(target.uuid, str(target)) for target in distinct_targets]
        test_methods = [(test_method.uuid, str(test_method)) for test_method in test_method_list]

        return render(request, 'sample_list.html', {'distinct_targets': distinct_targets,
                                                    'test_methods': test_methods})


class SampleUpdateView(LabMemberMixin, View):
    def get(self, request, order_code, sample_uuid):
        sample = get_object_or_404(models.Sample, uuid=sample_uuid)
        order = get_object_or_404(models.Order, code=order_code)
        # used to pass account id to collector autocomplete field
        request.session['step_1_cleaned_data'] = {'account': order.account.id}

        # convert UTC to tenant timezone
        collection_date = convert_utc_to_tenant_timezone(request, sample.collection_date)

        form = forms.SampleForm(
            initial={
                'collection_date': collection_date.strftime('%m-%d-%Y'),
                'collection_time': str(collection_date.time().strftime("%H:%M")),
                'sample_type': sample.clia_sample_type,
                'barcode': sample.barcode,
                'collection_temperature': sample.collection_temperature,
                'collection_temperature_unit': sample.collection_temperature_unit,
                'collection_user': sample.collection_user,
                'draw_type': sample.draw_type,
                'collection_temperature_is_within_range': sample.collection_temperature_is_within_range},
            sample_id=sample.id)

        return render(request, 'sample_create.html', {'form': form,
                                                      'sample': sample,
                                                      'order': order})

    def post(self, request, order_code, sample_uuid):
        sample = get_object_or_404(models.Sample, uuid=sample_uuid)
        order = get_object_or_404(models.Order, code=order_code)
        form = forms.SampleForm(request.POST, sample_id=sample.id)

        if form.is_valid():
            sample.barcode = form.cleaned_data['barcode']
            collection_date = form.cleaned_data.get('collection_date')
            collection_time = form.cleaned_data.get('collection_time')
            if collection_time:
                collection_date = datetime.datetime.combine(collection_date, collection_time)

            # convert tenant timezone to UTC for saving
            collection_date = convert_tenant_timezone_to_utc(request, collection_date)
            sample.collection_date = collection_date
            sample.clia_sample_type = form.cleaned_data['sample_type']
            sample.collection_temperature = form.cleaned_data['collection_temperature']
            sample.collection_temperature_unit = form.cleaned_data['collection_temperature_unit']
            sample.draw_type = form.cleaned_data['draw_type']
            sample.collection_user = form.cleaned_data['collection_user']
            sample.collection_temperature_is_within_range = form.cleaned_data.get(
                'collection_temperature_is_within_range')
            sample.save()

            messages.success(request, message='Sample {} updated successfully.'.format(sample.code))

            return redirect('order_details', order_code=order_code)
        else:
            return render(request, 'sample_create.html', {'form': form,
                                                          'sample': sample,
                                                          'order': order})


class SampleDeleteView(LabMemberMixin, View):
    # delete sample and associated tests with it
    def get(self, request, order_code, sample_uuid):
        sample = get_object_or_404(models.Sample, uuid=sample_uuid)
        tests = sample.tests.all()

        for test in tests:
            result = test.result.get()
            result.is_active = False
            result.save()
            test.is_active = False
            test.save()

        sample.is_active = False
        sample.save()

        return redirect('order_details', order_code=order_code)


class OperationalReportView(LabMemberMixin, View):
    """
    View for generating and viewing laboratory operations reports. Excel and CSV formats supported
    """

    def get(self, request):
        # initialize start and end date of start and end of previous month
        start_date = (datetime.date.today().replace(day=1) -
                      datetime.timedelta(days=1)).replace(day=1).strftime('%m-%d-%Y')
        end_date = (datetime.date.today().replace(day=1) - datetime.timedelta(days=1)).strftime('%m-%d-%Y')

        form = forms.OperationalReportForm(initial={'start_date': start_date,
                                                    'end_date': end_date})
        context = {'form': form}
        return render(request, 'operational_report.html', context)

    def post(self, request):
        form = forms.OperationalReportForm(request.POST)
        context = {'form': form}

        if form.is_valid():
            start_date = form.cleaned_data['start_date']
            end_date = form.cleaned_data['end_date']
            report_type = form.cleaned_data['report_type']
            date_filter = form.cleaned_data['date_filter']

            if report_type == 'CSV':
                report = models.OperationalReport.objects.create(
                    start_date=start_date, end_date=end_date, user=request.user,
                    report_format='CSV', date_filter=date_filter, is_queued=True,
                )
                tasks.generate_operational_report_csv.apply_async(
                    args=[request.tenant.id, report.id, start_date, end_date, date_filter],
                    queue='default'
                )
                messages.success(
                    request, "{} - {} CSV report is now creating.".format(start_date, end_date))
                return render(request, 'operational_report.html', context)
            elif report_type == 'Excel':
                report = models.OperationalReport.objects.create(
                    start_date=start_date, end_date=end_date, user=request.user,
                    report_format='Excel', date_filter=date_filter, is_queued=True,
                )
                tasks.generate_operational_report_excel.apply_async(
                    args=[request.tenant.id, report.id, start_date, end_date, date_filter],
                    queue='default'
                )
                messages.success(
                    request, "{} - {} Excel report is now creating.".format(start_date, end_date))
                return render(request, 'operational_report.html', context)


class ContainerAccessioningView(LabMemberMixin, View):
    """
    A view for checking in samples that have been received using a barcode reader
    """

    def get(self, request):
        form = forms.AccessionForm()
        context = {'form': form}
        return render(request, 'accession_containers.html', context)

    def post(self, request):
        editable_order_status_list = ['Unreceived', 'Pending Results']
        form = forms.AccessionForm(request.POST)

        if form.is_valid():
            barcode = form.cleaned_data['barcode']
            # if there is no hyphen in barcode, then return -1
            # if there is a hyphen in barcode, then remove the hyphen
            if barcode.find('-') != -1:
                barcode = barcode[:barcode.find('-')]
            try:
                receive_container = True
                message, context = accession_container_helper(request, barcode, receive_container)
                messages.info(request, message=message)
                context['editable_order_status_list'] = editable_order_status_list
                context['container_acc_barcode'] = barcode
                order = context.get('order')
                return redirect('accession_details', order_code=order.code, accession_type='containers', barcode=barcode)
            except Http404:
                message = format_html('Container Barcode not found. Please '
                                      '<a target="_blank" href="{}">{}</a>.', reverse('order_create'), 'create a new order')
                messages.error(request, message=message)
                return render(request, 'accession_containers.html', {'form': form})
        else:
            return render(request, 'accession_containers.html', {'form': form})


class SampleAccessioningView(LabMemberMixin, View):
    """
    A view for checking in samples that have been received using a barcode reader
    """

    def get(self, request):
        form = forms.AccessionForm()
        context = {'form': form}
        return render(request, 'accession_samples.html', context)

    def post(self, request):
        editable_order_status_list = ['Unreceived', 'Pending Results']
        form = forms.AccessionForm(request.POST)

        if form.is_valid():
            barcode = form.cleaned_data['barcode']
            # if there is no hyphen in barcode, then return -1
            # if there is a hyphen in barcode, then remove the hyphen
            if barcode.find('-') != -1:
                barcode = barcode[:barcode.find('-')]
            try:
                receive_sample = True
                message, context = accession_sample_helper(request, barcode, receive_sample)
                messages.info(request, message=message)
                context['editable_order_status_list'] = editable_order_status_list
                context['sample_acc_barcode'] = barcode
                order = context.get('order')
                return redirect('accession_details', order_code=order.code, accession_type='samples', barcode=barcode)
            except Http404:
                message = format_html('Sample Barcode not found. Please '
                                      '<a target="_blank" href="{}">{}</a>.', reverse('order_create'), 'create a new order')
                messages.error(request, message=message)
                return render(request, 'accession_samples.html', {'form': form})
        else:
            return render(request, 'accession_samples.html', {'form': form})


class OrderAccessioningView(LabMemberMixin, View):
    """
    A view for checking in orders that have been received using a barcode reader
    """

    def get(self, request):
        form = forms.AccessionForm()
        context = {'form': form}
        return render(request, 'accession_orders.html', context)

    def post(self, request):
        editable_order_status_list = ['Unreceived', 'Pending Results']
        form = forms.AccessionForm(request.POST)

        if form.is_valid():
            barcode = form.cleaned_data['barcode']
            # if there is no hyphen in barcode, then return -1
            # if there is a hyphen in barcode, then remove the hyphen
            if barcode.rfind('-') != -1 and barcode.rfind('-') > 6:
                barcode = barcode[:barcode.rfind('-')]
            try:
                accession_order = True
                message, context = accession_order_helper(request, barcode, accession_order)
                messages.info(request, message=message)
                context['editable_order_status_list'] = editable_order_status_list
                context['order_acc_barcode'] = barcode
                order = context.get('order')
                return redirect('accession_details', order_code=order.code, accession_type='orders', barcode=barcode)
            except Http404:
                message = format_html('Order Barcode not found. Please '
                                      '<a target="_blank" href="{}">{}</a>.', reverse('order_create'), 'create a new order')
                messages.error(request, message=message)
                return render(request, 'accession_orders.html', {'form': form})
        else:
            return render(request, 'accession_orders.html', {'form': form})


class AccessioningDetailsView(LabMemberMixin, View):

    def get(self, request, order_code, accession_type, barcode):
        order = get_object_or_404(models.Order, code=order_code)
        # only show the samples that have been accessioned
        samples = models.Sample.objects.filter(order_id=order.id, received_date__isnull=False)
        containers = models.Container.objects.filter(order_id=order.id, received_date__isnull=False)
        tests = order.tests.filter(test_type__offsite_laboratory__isnull=False)
        is_order = True
        check_for_offsite_lab_tests(request, tests, is_order)

        order_test_methods = {}
        # update samples received_date for this order
        for sample in samples:
            test_methods = get_test_method_abbreviated_names(sample)
            order_test_methods.update({sample.code: test_methods})

        message = ''
        context = get_accession_details(request, order, False, message)
        context['samples'] = samples
        context['containers'] = containers
        context['order_test_methods'] = order_test_methods
        context['barcode'] = barcode

        if accession_type not in ['orders', 'samples', 'containers']:
            return redirect('/')
        template_str = 'accession_' + accession_type + '.html'
        return render(request, template_str, context)

    def post(self, request, order_code, accession_type, barcode):
        comment_form = forms.AccessioningViewCommentForm(request.POST)
        if comment_form.is_valid():
            order = get_object_or_404(models.Order, code=order_code)
            comment = models.OrderComment(
                order=order,
                user=request.user,
                comment=comment_form.cleaned_data['comment']
            )
            comment.save()
            return redirect('accession_details', accession_type=accession_type, order_code=order.code, barcode=barcode)
        return redirect('/')


class ContainerAccessioningConfirmReceiptView(LabMemberMixin, View):
    """
    A view for manually receiving a container using a barcode reader
    """

    def get(self, request):
        form = forms.AccessionForm()
        context = {'form': form}
        return render(request, 'accession_containers_confirm_receipt.html', context)

    def post(self, request):
        form = forms.AccessionForm(request.POST)
        editable_order_status_list = ['Unreceived', 'Pending Results']
        if 'receive_container' in request.POST:
            barcode = request.POST.get('receive_container', False)
            if barcode:
                receive_container = True
                message, context = accession_container_helper(request, barcode, receive_container)
                messages.success(request, message=message)
                if context['already_received']:  # if container was already received, do not auto print labels
                    receive_container = False
                context['receive_container'] = receive_container
                context['editable_order_status_list'] = editable_order_status_list
                context['container_acc_barcode'] = barcode
                order = context.get('order')
                return redirect('accession_confirm_receipt_details', order_code=order.code, accession_type='containers', barcode=barcode)
            else:
                return render(request, 'accession_containers_confirm_receipt.html', {'form': form})

        if form.is_valid():
            barcode = form.cleaned_data['barcode']
            # if there is no hyphen in barcode, then return -1
            # if there is a hyphen in barcode, then remove the hyphen
            if barcode.find('-') != -1:
                barcode = barcode[:barcode.find('-')]
            try:
                receive_container = False
                message, context = accession_container_helper(request, barcode, receive_container)
                messages.info(request, message=message)
                context['receive_container'] = receive_container
                context['editable_order_status_list'] = editable_order_status_list
                order = context.get('order')
                return redirect('accession_confirm_receipt_details', order_code=order.code, accession_type='containers', barcode=barcode)
            except Http404:
                message = format_html('Container Barcode not found. Please '
                                      '<a target="_blank" href="{}">{}</a>.', reverse('order_create'), 'create a new order')
                messages.error(request, message=message)
                return render(request, 'accession_containers_confirm_receipt.html', {'form': form})
        else:
            return render(request, 'accession_containers_confirm_receipt.html', {'form': form})


class SampleAccessioningConfirmReceiptView(LabMemberMixin, View):
    """
    A view for manually receiving a sample using a barcode reader
    """

    def get(self, request):
        form = forms.AccessionForm()
        context = {'form': form}
        return render(request, 'accession_samples_confirm_receipt.html', context)

    def post(self, request):
        form = forms.AccessionForm(request.POST)
        editable_order_status_list = ['Unreceived', 'Pending Results']

        if 'receive_sample' in request.POST:
            barcode = request.POST.get('receive_sample', False)
            if barcode:
                receive_sample = True
                message, context = accession_sample_helper(request, barcode, receive_sample)
                if context['already_received']:  # if sample was already received, do not auto print labels
                    receive_sample = False
                context['receive_sample'] = receive_sample
                context['editable_order_status_list'] = editable_order_status_list
                context['sample_acc_barcode'] = barcode
                order = context.get('order')
                return redirect('accession_confirm_receipt_details', order_code=order.code, accession_type='samples', barcode=barcode)
            else:
                return render(request, 'accession_samples_confirm_receipt.html', {'form': form})

        if form.is_valid():
            barcode = form.cleaned_data['barcode']
            # if there is no hyphen in barcode, then return -1
            # if there is a hyphen in barcode, then remove the hyphen
            if barcode.find('-') != -1:
                barcode = barcode[:barcode.find('-')]
            try:
                receive_sample = False
                message, context = accession_sample_helper(request, barcode, receive_sample)
                messages.info(request, message=message)
                context['receive_sample'] = receive_sample
                context['editable_order_status_list'] = editable_order_status_list
                order = context.get('order')
                return redirect('accession_confirm_receipt_details', order_code=order.code, accession_type='samples', barcode=barcode)
            except Http404:
                message = format_html('Sample Barcode not found. Please '
                                      '<a target="_blank" href="{}">{}</a>.', reverse('order_create'), 'create a new order')
                messages.error(request, message=message)
                return render(request, 'accession_samples_confirm_receipt.html', {'form': form})
        else:
            return render(request, 'accession_samples_confirm_receipt.html', {'form': form})


class OrderAccessioningConfirmReceiptView(LabMemberMixin, View):
    """
    A view for checking in orders that have been received using a barcode reader. Orders are marked as received
    when the user clicks the receive order button
    """

    def get(self, request):
        form = forms.AccessionForm()
        context = {'form': form}
        return render(request, 'accession_orders_confirm_receipt.html', context)

    def post(self, request):
        form = forms.AccessionForm(request.POST)
        editable_order_status_list = ['Unreceived', 'Pending Results']

        if 'receive_order' in request.POST:  # confirm receipt of order - receive order
            barcode = request.POST.get('receive_order', False)
            if barcode:
                accession_order = True
                message, context = accession_order_helper(request, barcode, accession_order)
                messages.success(request, message=message)

                if context['already_received']:  # fail safe, can be removed after testing unless proves necessary
                    accession_order = False
                context['accession_order'] = accession_order
                context['editable_order_status_list'] = editable_order_status_list
                context['order_acc_barcode'] = barcode
                order = context.get('order')
                return redirect('accession_confirm_receipt_details', order_code=order.code, accession_type='orders', barcode=barcode)
            else:
                return render(request, 'accession_orders_confirm_receipt.html', {'form': form})

        if form.is_valid():
            barcode = form.cleaned_data['barcode']
            # if there is no hyphen in barcode, then return -1
            # if there is a hyphen in barcode, then remove the hyphen
            if barcode.rfind('-') != -1 and barcode.rfind('-') > 6:
                barcode = barcode[:barcode.rfind('-')]
            try:
                accession_order = False
                message, context = accession_order_helper(request, barcode, accession_order)
                messages.info(request, message=message)
                context['accession_order'] = accession_order
                context['editable_order_status_list'] = editable_order_status_list
                context['order_acc_barcode'] = barcode
                order = context.get('order')
                return redirect('accession_confirm_receipt_details', order_code=order.code, accession_type='orders', barcode=barcode)
            except Http404:
                message = format_html('Order Barcode not found. Please '
                                      '<a target="_blank" href="{}">{}</a>.', reverse('order_create'), 'create a new order')
                messages.error(request, message=message)
                return render(request, 'accession_orders_confirm_receipt.html', {'form': form})
        else:
            return render(request, 'accession_orders_confirm_receipt.html', {'form': form})


class AccessioningConfirmReceiptDetailsView(LabMemberMixin, View):

    def get(self, request, order_code, accession_type, barcode):
        order = get_object_or_404(models.Order, code=order_code)
        # only show the samples that have been accessioned
        samples = models.Sample.objects.filter(order_id=order.id, received_date__isnull=False)
        containers = models.Container.objects.filter(order_id=order.id, received_date__isnull=False)
        tests = order.tests.filter(test_type__offsite_laboratory__isnull=False)
        is_order = True
        check_for_offsite_lab_tests(request, tests, is_order)

        order_test_methods = {}
        # update samples received_date for this order
        for sample in samples:
            test_methods = get_test_method_abbreviated_names(sample)
            order_test_methods.update({sample.code: test_methods})

        message = ''
        context = get_accession_details(request, order, False, message)
        context['samples'] = samples
        context['containers'] = containers
        context['order_test_methods'] = order_test_methods
        context['barcode'] = barcode

        if accession_type not in ['orders', 'samples', 'containers']:
            return redirect('/')
        template_str = 'accession_' + accession_type + '_confirm_receipt.html'
        return render(request, template_str, context)

    def post(self, request, order_code, accession_type, barcode):
        comment_form = forms.AccessioningViewCommentForm(request.POST)
        if comment_form.is_valid():
            order = get_object_or_404(models.Order, code=order_code)
            comment = models.OrderComment(
                order=order,
                user=request.user,
                comment=comment_form.cleaned_data['comment']
            )
            comment.save()
            return redirect('accession_confirm_receipt_details', accession_type=accession_type, order_code=order.code, barcode=barcode)
        return redirect('/')


class DataInListView(LabMemberMixin, View):
    """

    """

    def get(self, request):
        context = {}
        return render(request, 'data_in_list.html', context)


class ReportRemarksDetailsView(LISLoginRequiredMixin, View):
    def get(self, request):
        report_remarks = models.ReportRemarks.objects.all()

        context = {'report_remarks': report_remarks}
        return render(request, 'report_remarks.html', context=context)


class ReportRemarksCreateView(LISLoginRequiredMixin, View):
    def get(self, request):
        form = forms.ReportRemarksForm()
        context = {'form': form}
        return render(request, 'report_remarks_create.html', context=context)

    @transaction.atomic
    def post(self, request):
        form = forms.ReportRemarksForm(request.POST)
        context = {'form': form}
        if form.is_valid():
            test_target = form.cleaned_data['test_target']
            account = form.cleaned_data['account']
            offsite_laboratory = form.cleaned_data['offsite_laboratory']
            remarks = form.cleaned_data['remarks']

            models.ReportRemarks.objects.create(
                test_target=test_target,
                account=account,
                offsite_laboratory=offsite_laboratory,
                remarks=remarks
            )

            return redirect('report_remarks_list')
        else:
            return render(request, 'report_remarks_create.html', context=context)


class ReportRemarksUpdateView(LISLoginRequiredMixin, View):
    def get(self, request, report_remark_uuid):
        report_remark = get_object_or_404(models.ReportRemarks, uuid=report_remark_uuid)
        form = forms.ReportRemarksForm(initial={'test_target': report_remark.test_target,
                                                'account': report_remark.account,
                                                'offsite_laboratory': report_remark.offsite_laboratory,
                                                'remarks': report_remark.remarks})
        context = {'form': form,
                   'update': True}
        return render(request, 'report_remarks_create.html', context=context)

    @transaction.atomic
    def post(self, request, report_remark_uuid):
        report_remark = get_object_or_404(models.ReportRemarks, uuid=report_remark_uuid)
        form = forms.ReportRemarksForm(request.POST)

        context = {'form': form,
                   'update': True}
        if form.is_valid():
            report_remark.test_target = form.cleaned_data['test_target']
            report_remark.account = form.cleaned_data['account']
            report_remark.offsite_laboratory = form.cleaned_data['offsite_laboratory']
            report_remark.remarks = form.cleaned_data['remarks']
            report_remark.save()

            return redirect('report_remarks_list')
        else:
            return render(request, 'report_remarks_create.html', context=context)


class AccountsAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs = accounts_models.Account.objects.all()

        if self.q:
            qs = qs.filter(name__icontains=self.q)
        return qs


class OrderEditPatientPrescribedMedications(LISLoginRequiredMixin, View):
    def get(self, request, order_code, provider_uuid, patient_uuid):
        patient = get_object_or_404(patients_models.Patient, uuid=patient_uuid)
        provider = get_object_or_404(accounts_models.Provider, uuid=provider_uuid)
        order = get_object_or_404(models.Order, code=order_code)
        try:
            prescribed_medications = patients_models.PatientPrescribedMedicationHistory.objects.filter(
                patient=patient, provider=provider, order=order). \
                prefetch_related('prescribed_medications').latest('created_date').prescribed_medications.all()
        except patients_models.PatientPrescribedMedicationHistory.DoesNotExist:
            prescribed_medications = None

        form = forms.PatientStatusForm(initial={'prescribed_medications': prescribed_medications})

        context = {'form': form}
        return render(request, 'order_edit_patient_prescribed_medications.html', context)

    def post(self, request, order_code, provider_uuid, patient_uuid):
        form = forms.PatientStatusForm(request.POST)
        patient = get_object_or_404(patients_models.Patient, uuid=patient_uuid)
        provider = get_object_or_404(accounts_models.Provider, uuid=provider_uuid)
        order = get_object_or_404(models.Order, code=order_code)
        context = {'form': form}

        if form.is_valid():
            chosen_prescribed_medications = form.cleaned_data['prescribed_medications']
            # for future use in case we bring this back with ILDP
            # temperature = str(form.cleaned_data['temperature'])
            # blood_pressure_systolic = str(form.cleaned_data['blood_pressure_systolic'])
            # blood_pressure_diastolic = str(form.cleaned_data['blood_pressure_diastolic'])
            # fasting = form.cleaned_data['fasting']
            # heart_rate = form.cleaned_data['heart_rate']
            # temperature_unit = form.cleaned_data['temperature_unit']
            #
            # if temperature or blood_pressure_diastolic or blood_pressure_systolic:
            #     patients_models.PatientStatus(patient=patient,
            #                                   order=order,
            #                                   fasting=fasting,
            #                                   heart_rate=heart_rate,
            #                                   temperature=temperature,
            #                                   temperature_unit=temperature_unit,
            #                                   blood_pressure_systolic=blood_pressure_systolic,
            #                                   blood_pressure_diastolic=blood_pressure_diastolic).save()
            patient_prescribed_medication_history = patients_models.PatientPrescribedMedicationHistory.objects.filter(
                patient=patient,
                order=order,
                provider=provider).prefetch_related('prescribed_medications').order_by('-created_date').first()
            if chosen_prescribed_medications:
                if patient_prescribed_medication_history:
                    # clear the list and set with new chosen meds
                    patient_prescribed_medication_history.prescribed_medications.clear()
                    patient_prescribed_medication_history.prescribed_medications.set(chosen_prescribed_medications)
                else:
                    patient_prescribed_medication_history = patients_models.PatientPrescribedMedicationHistory(
                        patient=patient,
                        order=order,
                        provider=provider)
                    patient_prescribed_medication_history.save()
                    patient_prescribed_medication_history.prescribed_medications.set(chosen_prescribed_medications)
            else:  # prescribed meds cleared
                if patient_prescribed_medication_history:
                    patient_prescribed_medication_history.prescribed_medications.clear()
            messages.success(request, 'Prescribed medications successfully updated.')
            return redirect('order_details', order_code=order.code)
        else:
            return render(request, 'order_edit_patient_prescribed_medications.html', context)


class OrderDocumentsListView(LabMemberMixin, View):
    def get(self, request, order_uuid):
        order = get_object_or_404(models.Order, uuid=order_uuid)
        # get this orders documents
        documents = models.OrderAdditionalDocuments.objects.filter(order=order).order_by('-created_date')
        # get correct datetime/timezones
        for document in documents:
            document.created_date = convert_utc_to_tenant_timezone(request, document.created_date)
        return render(request, 'order_document_list.html', {'order': order,
                                                            'documents': documents})


class OrderDocumentsDeleteView(LISLoginRequiredMixin, View):
    def get(self, request, order_uuid, document_uuid):
        order_documents = models.OrderAdditionalDocuments.objects.get(uuid=document_uuid)
        order_documents.is_active = False
        order_documents.save(update_fields=['is_active'])

        messages.success(request,
                         'Document {} archived successfully.'.format(order_documents.patient_document.document_name))

        return redirect('order_document_list', order_uuid=order_uuid)


class BillingMessagesListView(LabMemberMixin, View):
    def get(self, request):
        billing_messages = models.BillingMessage.objects.all()
        accounts = accounts_models.Account.objects.all()
        context = {
            'billing_messages': billing_messages,
            'accounts': accounts
        }

        if os.environ.get('DEBUG') == 'False' and not os.environ.get('STAGING') == 'True':  # production environment
            for billing_message in billing_messages:
                url = billing_message.hl7_message
                url = str(url).replace('hl7_messages', 'hl7_messages_processed')
                billing_message.hl7_message = url

        return render(request, 'billing_messages_list.html', context)
