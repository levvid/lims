# Generated by Django 2.2.2 on 2019-07-21 02:49

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('patients', '0019_auto_20190715_1602'),
    ]

    operations = [
        migrations.AddField(
            model_name='patient',
            name='alternate_id',
            field=models.CharField(blank=True, max_length=32),
        ),
    ]
