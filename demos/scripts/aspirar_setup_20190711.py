import datetime
import decimal
import django

import logging
import os
import random
import sys

from django.db import transaction
from django.utils import timezone
from tenant_schemas.utils import tenant_context

os.environ['DJANGO_SETTINGS_MODULE'] = 'main.settings'
django.setup()

from clients import models as client_models
from orders import models as orders_models
from accounts import models as accounts_models
from lab import models as lab_models
from lab_tenant import models as lab_tenant_models
from patients import models as patients_models
from orders.functions import generate_order_code, generate_sample_code, get_sample_barcode

# import dummy data
from demos.shared_data.shared_data import INSTRUMENTS, TEST_TARGETS
from demos.dummy_data.common import ADDITIONAL_USERS, PROVIDERS, ACCOUNTS, PATIENTS
from demos.dummy_data.molecular import CONTROLS, DATA_IN_LIST, INSTRUMENT_INTEGRATIONS, TEST_TYPES

from demos.initialization_functions import create_providers, create_accounts, create_users, create_instruments, \
    create_instrument_integrations, create_patients, create_controls, create_data_in_demo, create_test_types, \
    create_test_targets

from tasks import initialize_client

"""
Script to set up a new schema for demo purposes
"""

DEMO = True
NUM_ORDERS = 60

# schema variables
SCHEMA_NAME = 'aspirardemo1'
LABORATORY_NAME = 'Aspirar'
LABORATORY_TIMEZONE = 'US/Eastern'
EMAIL = 'sjung@dendisoftware.com'
FIRST_NAME = 'Simon'
LAST_NAME = 'Jung'
PASSWORD = 'dendipower123123'
SCHEMA_CONTEXT = {'client_name': LABORATORY_NAME,
                  'schema_name': SCHEMA_NAME,
                  'email': EMAIL,
                  'first_name': FIRST_NAME,
                  'last_name': LAST_NAME,
                  'password': PASSWORD,
                  'lab_type': '',
                  }

SCHEMA_SETTINGS = {"require_icd10": True,
                   "skip_patient_history": True}


@transaction.atomic
def main():
    try:
        tenant = client_models.Client.objects.get(schema_name=SCHEMA_NAME)
        logging.debug('Using schema: {}'.format(SCHEMA_NAME))
    except client_models.Client.DoesNotExist:
        logging.debug('Creating schema: {}'.format(SCHEMA_NAME))
        initialize_client(SCHEMA_CONTEXT)
        tenant = client_models.Client.objects.get(schema_name=SCHEMA_NAME)
        tenant.timezone = LABORATORY_TIMEZONE
        tenant.settings = SCHEMA_SETTINGS
        tenant.save(update_fields=['timezone', 'settings'])

        logging.debug('Creating users')
        with tenant_context(tenant):
            create_users(ADDITIONAL_USERS)

    logging.debug('Initializing test targets')
    create_test_targets(TEST_TARGETS)

    logging.debug('Initializing required instruments')
    create_instruments(INSTRUMENTS)

    with tenant_context(tenant):
        logging.debug('Initializing instrument integrations')
        create_instrument_integrations(INSTRUMENT_INTEGRATIONS)

        # make test types
        logging.debug('Initializing test types')
        test_types = create_test_types(TEST_TYPES)

        # make test panel
        logging.debug('Initializing test panel types')
        initialize_test_panel_types(test_types)

        logging.debug('Initializing instrument calibrations')
        initialize_instrument_calibrations()

        if DEMO:
            # make account
            logging.debug('Initializing dummy accounts')
            create_accounts(ACCOUNTS)

            # make patients
            logging.debug('Initializing dummy patients')
            create_patients(PATIENTS)

            # make providers
            logging.debug("Initializing dummy providers")
            create_providers(PROVIDERS)

            # make orders
            logging.debug('Initializing {} dummy orders'.format(NUM_ORDERS))
            initialize_dummy_orders(NUM_ORDERS)

            # make some in transit orders
            logging.debug("Initializing in transit orders")
            initialize_in_transit_orders()

            # make controls
            logging.debug("Initializing controls")
            create_controls(CONTROLS)

            # add data-in
            logging.debug("Initializing data in demo")
            create_data_in_demo(DATA_IN_LIST)

            # make dummy control observations
            logging.debug("Initializing control observations")
            initialize_dummy_observations()


def initialize_test_panel_types(utm_test_types):
    """
    Create the Urinary Tract Microbiota TaqMan™ Assay Panel and assign TestTypes to it
    :param utm_test_types:
    :return:
    """
    test_panel_type, created = orders_models.TestPanelType.objects.update_or_create(
        name='Urinary Tract Microbiota TaqMan™ Assay Panel',
        menu_category_id=1)

    for test_type in utm_test_types:
        test_panel_type.test_types.add(test_type)


def initialize_instrument_calibrations():
    instrument_integration = lab_tenant_models.InstrumentIntegration.objects.get(
        internal_instrument_name=INSTRUMENT_INTEGRATIONS[0]['internal_instrument_name']
    )
    test_types = orders_models.TestType.objects.all()

    cutoff = decimal.Decimal('25.50')
    for test_type in test_types:
        orders_models.InstrumentCalibration.objects.update_or_create(instrument_integration=instrument_integration,
                                                                     test_type=test_type,
                                                                     target_name=test_type.test_target.name,
                                                                     defaults={'cutoff_value': cutoff}
                                                                     )
        cutoff = cutoff + decimal.Decimal('0.23')


def initialize_dummy_orders(num_orders):
    urine_sample_type = lab_models.CLIASampleType.objects.get(name='Urine')
    test_panel_type = orders_models.TestPanelType.objects.get(name='Urinary Tract Microbiota TaqMan™ Assay Panel')

    days_back = 60
    received_date_start = timezone.now() - datetime.timedelta(days=days_back)
    average_order_count = int(num_orders / days_back)

    for order_count in range(num_orders):
        latest_order_code = generate_order_code()
        rand_account = accounts_models.Account.objects.get(id=random.randint(1, 9))
        rand_patient = patients_models.Patient.objects.get(id=random.randint(1, 11))
        logging.debug('Order {}: {} {} '.format(order_count, rand_account.name, rand_patient.user.first_name))
        order = orders_models.Order.objects.create(
            provider_id=random.randint(1, 5),
            account=rand_account,
            patient=rand_patient,
            code=latest_order_code,
            research_consent=False,
            patient_result_request=False,

        )
        order.received_date = received_date_start
        order.save(update_fields=['received_date'])

        latest_sample_code = generate_sample_code(order.code, urine_sample_type)
        sample = orders_models.Sample.objects.create(
            order=order,
            code=latest_sample_code,
            clia_sample_type=urine_sample_type,
            collection_date=timezone.now()
        )
        sample_barcode = get_sample_barcode(sample.id)
        sample.barcode = sample_barcode
        sample.save(update_fields=['barcode'])

        test_panel = orders_models.TestPanel.objects.create(
            order=order,
            test_panel_type=test_panel_type,
            expected_revenue=test_panel_type.cost,
        )

        for test_type in test_panel_type.test_types.all():
            test = orders_models.Test.objects.create(
                order=order,
                sample=sample,
                test_panel=test_panel,
                test_type=test_type,
                due_date=datetime.date.today() + datetime.timedelta(days=test_type.due_date_days)
            )
            result_obj = orders_models.Result.objects.create(
                order=order,
                test=test
            )
        order.submitted = True
        order.save(update_fields=['submitted'])

        if random.randint(1, average_order_count + 1) == average_order_count + 1:
            if received_date_start + datetime.timedelta(days=1, hours=2) < timezone.now():
                received_date_start = received_date_start + datetime.timedelta(days=1, hours=2)


def initialize_in_transit_orders():
    urine_sample_type = lab_models.CLIASampleType.objects.get(name='Urine')
    test_panel_type = orders_models.TestPanelType.objects.get(name='Urinary Tract Microbiota TaqMan™ Assay Panel')

    latest_order_code = generate_order_code()
    rand_account = accounts_models.Account.objects.get(id=random.randint(1, 9))
    rand_patient = patients_models.Patient.objects.get(id=random.randint(1, 11))
    logging.debug('In transit order Order: {} {} '.format(rand_account.name, rand_patient.user.first_name))
    order = orders_models.Order.objects.create(
        provider_id=1,
        account=rand_account,
        patient=rand_patient,
        code=latest_order_code,
        research_consent=False,
        patient_result_request=False,

    )

    latest_sample_code = generate_sample_code(order.code, urine_sample_type)
    sample = orders_models.Sample.objects.create(
        order=order,
        code=latest_sample_code,
        clia_sample_type=urine_sample_type,
        collection_date=timezone.now()
    )
    sample_barcode = get_sample_barcode(sample.id)
    sample.barcode = sample_barcode
    sample.save(update_fields=['barcode'])

    test_panel = orders_models.TestPanel.objects.create(
        order=order,
        test_panel_type=test_panel_type,
        expected_revenue=test_panel_type.cost,
    )

    for test_type in test_panel_type.test_types.all():
        test = orders_models.Test.objects.create(
            order=order,
            sample=sample,
            test_panel=test_panel,
            test_type=test_type,
            due_date=datetime.date.today() + datetime.timedelta(days=test_type.due_date_days)
        )
        result_obj = orders_models.Result.objects.create(
            order=order,
            test=test
        )
    order.submitted = True
    order.save(update_fields=['submitted'])


def initialize_dummy_observations():
    """
    create dummy observations for QC charting purposes
    :return:
    """
    days_back = 30
    date_counter = timezone.now() - datetime.timedelta(days=days_back)
    data_in = orders_models.DataIn.objects.first()
    controls = orders_models.Control.objects.all()
    targets = ['Acinetobacter baumannii', 'Candida albicans']

    while date_counter < timezone.now():
        for control in controls:
            for target in targets:
                orders_models.Observation.objects.create(
                    control=control,
                    target_name=target,
                    data_in=data_in,
                    result_value=random.uniform(20.0, 25.0),
                    observation_date=date_counter,
                )
        date_counter += datetime.timedelta(days=1)


if __name__ == '__main__':
    logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)
    main()
