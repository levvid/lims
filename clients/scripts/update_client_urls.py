import django
import logging
import os
import sys

os.environ['DJANGO_SETTINGS_MODULE'] = 'main.settings'
django.setup()

from .. import models

DEVELOPMENT_DOMAIN = 'localhost'
# PRODUCTION_DOMAIN = 'dendisoftware.com'
STAGING_DOMAIN = 'dendi-staging.com'

DOMAIN_DICT = {
               'staging': STAGING_DOMAIN,
               'development': DEVELOPMENT_DOMAIN,
               # 'production': PRODUCTION_DOMAIN
               }


def main(domain):

    # import pdb; pdb.set_trace()
    for client in models.Client.objects.all():
        print(client)
        client_url_split = client.domain_url.split('.')
        # replace the URL with local
        if len(client_url_split) > 2:
            client_url = client_url_split[0] + '.' + domain
        else:
            client_url = domain

        client.domain_url = client_url
        client.save()
        print(client.domain_url)


if __name__ == '__main__':
    if len(sys.argv) == 1:
        print("Missing argument. Please specify which environment you are converting into: development or staging")
    elif len(sys.argv) == 2 and sys.argv[1].lower() in ['development', 'staging']:
        target_env = sys.argv[1].lower()
        logging.info("Initializing data for {}".format(target_env))
        main(domain=DOMAIN_DICT[target_env])
    else:
        print("Unknown arguments")

