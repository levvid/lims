from django.http import JsonResponse
from django.utils.html import format_html
from django.db.models.functions import Lower

from . import models
from orders import models as orders_models


def offsite_integration_table(request):
    """
    data view for offsite table
    :param request:
    :return:
    """
    offsite_integrations = models.OffsiteLaboratoryIntegration.objects.all()

    results = []
    for offsite_integration in offsite_integrations:

        offsite_integration_data = {
            'name': {
                'text': offsite_integration.offsite_laboratory.name,
                'url': '<a href="/offsite_integrations/{}">{}</a>'.format(
                    offsite_integration.uuid,
                    offsite_integration.offsite_laboratory.name
                )
            },
            'sending_facility_id': offsite_integration.sending_facility_id
        }
        results.append(offsite_integration_data)
    return JsonResponse(results, safe=False)


def instrument_integration_table(request):
    """
    data view for instrument integration table
    :param request:
    :return:
    """
    instrument_integrations = models.InstrumentIntegration.objects.all()

    results = []
    for instrument_integration in instrument_integrations:
        instrument_name = instrument_integration.internal_instrument_name
        data = {
            'name': {
                'text': instrument_name,
                'url': '<a href="/instrument_integrations/{}">{}</a>'.format(
                    instrument_integration.uuid,
                    instrument_name)
            },
            'model': instrument_integration.instrument.name,
            'manufacturer': instrument_integration.instrument.manufacturer,
            'method': instrument_integration.instrument.method.name,
            'after_data_import': instrument_integration.after_data_import
        }
        results.append(data)
    return JsonResponse(results, safe=False)


def lab_departments_table(request):
    """
    data view for lab departments table
    :param request:
    :return:
    """
    lab_departments = models.LabDepartment.objects.all()

    results = []
    for lab_department in lab_departments:
        lab_department_name = lab_department.name
        data = {
            'name': {
                'text': lab_department_name,
                'url': '<a href="/lab_departments/{}">{}</a>'.format(
                    lab_department.uuid,
                    lab_department.name
                )
            }
        }
        results.append(data)

    return JsonResponse(results, safe=False)


def instrument_calibration_table(request, instrument_integration_uuid):
    """
    data view for instrument calibration table
    :param request:
    :param instrument_integration_uuid:
    :return:
    """
    instrument_integration = models.InstrumentIntegration.objects.get(uuid=instrument_integration_uuid)
    instrument_calibrations = instrument_integration.instrument_calibrations.all().prefetch_related('test_type').order_by(Lower('test_type__name'))

    results = []

    for instrument_calibration in instrument_calibrations:

        update_link = format_html('<a href="/instrument_integrations/{}/calibrations/{}/update/">'
                                  '<i class="fas fa-pencil-alt text-warning"></i></a>',
                                  instrument_integration.uuid,
                                  instrument_calibration.uuid,)
        delete_link = format_html('<a href="/instrument_integrations/{}/calibrations/{}/delete/">'
                                  '<i class="fas fa-trash-alt text-danger"></i></a>',
                                  instrument_integration.uuid,
                                  instrument_calibration.uuid, )

        data = {
            'test_type': instrument_calibration.test_type.name,
            'target_name': instrument_calibration.target_name,
            'cutoff_value': instrument_calibration.cutoff_value,
            'update_link': update_link,
            'delete_link': delete_link,
        }
        results.append(data)
    return JsonResponse(results, safe=False)


def instrument_test_code_table(request, instrument_integration_uuid):
    """
    data view for AU test codes
    :param request:
    :param instrument_integration_uuid:
    :return:
    """
    instrument_integration = models.InstrumentIntegration.objects.get(uuid=instrument_integration_uuid)
    instrument_test_codes = orders_models.InstrumentTestCode.objects.filter(instrument_integration=instrument_integration)\
        .order_by('test_code')

    results = []

    for test_code in instrument_test_codes:

        update_link = format_html('<a href="/instrument_integrations/{}/instrument_test_codes/{}/update/">'
                                  '<i class="fas fa-pencil-alt text-warning"></i></a>',
                                  instrument_integration.uuid,
                                  test_code.uuid)

        delete_link = format_html('<a href="/instrument_integrations/{}/instrument_test_codes/{}/delete/">'
                                  '<i class="fas fa-trash-alt test-code-delete text-danger"></i></a>',
                                  instrument_integration.uuid,
                                  test_code.uuid)

        data = {
            'test_code': test_code.test_code,
            'test_type': test_code.test_type.name,
            'test_target': test_code.test_type.test_target.name,
            'update_link': update_link,
            'delete_link': delete_link,
        }
        results.append(data)
    return JsonResponse(results, safe=False)


def in_house_lab_location_table(request):
    """
    data view for In-House Lab Location table
    :param request:
    :return:
    """

    in_house_lab_locations = models.InHouseLabLocation.objects.all()

    results = []
    for in_house_lab_location in in_house_lab_locations:
        address1 = in_house_lab_location.address1
        address2 = in_house_lab_location.address2
        zip_code = in_house_lab_location.zip_code
        test_profiles = orders_models.TestPanelType.objects.filter(in_house_lab_locations=in_house_lab_location)

        address = '{} {}{}'.format(address1,
                                   address2 if address2 else '',
                                   ', ' + zip_code if zip_code else '')
        in_house_lab_location_data = {
            'name': {
                'text': in_house_lab_location.name,
                'url': '<a href="/in_house_lab_locations/{}">{}</a>'.format(
                    in_house_lab_location.uuid,
                    in_house_lab_location.name
                )
            },
            'alias': in_house_lab_location.alias,
            'lab_director': in_house_lab_location.lab_director,
            'address': address,
            'phone_number': in_house_lab_location.phone_number,
            'fax_number': in_house_lab_location.fax_number,
            'test_profile_count': test_profiles.count()
        }
        results.append(in_house_lab_location_data)
    return JsonResponse(results, safe=False)
