import os
import sys

import django
import logging

os.environ['DJANGO_SETTINGS_MODULE'] = 'main.settings'
django.setup()

from tenant_schemas.utils import tenant_context
import clients.models as clients_models
import orders.models as orders_models
import accounts.models as accounts_models
from orders import generate_billing_hl7_labrcm, tasks

TENANT_NAME = 'ildp'
PROD_DB_ORDER_CODES = ['20012900004']
# add accession_numbers here to test on local DB
TEST_DB_ORDER_CODES = ['2020-0000082']


def report_orders():
    """
    Report orders that were affected by the lack of a provider
    :return:
    """

    tenant = clients_models.Client.objects.get(schema_name=TENANT_NAME)

    with tenant_context(tenant):
        # get all reported orders with requisition forms
        orders_to_report = orders_models.Order.objects.filter(
            code__in=PROD_DB_ORDER_CODES)  # get all reported orders
        preliminary = False  # real report
        report_approved_by_id = 5  # Joe Morgan
        provider_id = 121  # Stephanie Harris
        provider = accounts_models.Provider.objects.get(id=provider_id)
        print('Re-reporting {} orders'.format(len(orders_to_report)))
        for order in orders_to_report:
            # Update provider to Stephanie Harris
            order.provider = provider
            order.save(update_fields=['provider'])
            tasks.generate_order_report.apply_async(
                args=[tenant.id, order.uuid, report_approved_by_id, preliminary],
                queue='default'
            )


if __name__ == '__main__':
    logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)
    report_orders()
