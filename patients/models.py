import uuid as uuid_lib

from django.db import models
from django.utils import timezone
from django.db.models.functions import Lower

from lab_tenant.models import User
from accounts import models as accounts_models
from main import settings
from main.storage_backends import PrivateMediaStorage
from orders import models as orders_models

from lab_tenant.models import AuditLogModel
from auditlog.registry import auditlog

DOCUMENT_TYPES = (('', '-------------'), ('Commercial', 'Commercial'), ('Medicare', 'Medicare'), ('Medicaid', 'Medicaid'), ('Other', 'Other'))


# Create your models here.
class Patient(AuditLogModel):
    """
    Patient is always tied to a User object.

    Won't always have email - internally speaking, a Patient's default username/password is their UUID.
    If a Patient is "activated", meaning they need to actually log in, a provider or lab user will have to
    activate that account by setting the username to their email and setting a default password.

    """
    # IDs from different LISs
    alternate_id = models.CharField(max_length=32, blank=True)
    # a User object is created for each Patient object
    user = models.OneToOneField(User, on_delete=models.CASCADE, null=True)
    providers = models.ManyToManyField(accounts_models.Provider)
    primary_care_provider = models.ForeignKey(accounts_models.Provider, related_name='patients',
                                              on_delete=models.CASCADE, null=True, blank=True)
    middle_initial = models.CharField(max_length=10, blank=True)
    # Jr, II, etc.
    suffix = models.CharField(max_length=255, blank=True)
    birth_date = models.DateField(null=True, blank=True)
    address1 = models.CharField(max_length=255, blank=True)
    address2 = models.CharField(max_length=255, blank=True)
    sex = models.CharField(max_length=10, blank=True,
                           choices=(('M', 'Male'), ('F', 'Female'), ('NB', 'Non-Binary')))
    phone_number = models.CharField(max_length=255, blank=True)
    # social security numbers are in the format: AAA-GG-SSSS
    social_security = models.CharField(max_length=11, blank=True, null=True, default=None)
    drivers_license = models.CharField(max_length=15, blank=True, default='')
    patient_id = models.CharField(max_length=128, null=True, unique=True, blank=True)
    zip_code = models.CharField(max_length=128, blank=True)
    state = models.CharField(max_length=16, blank=True, default='')
    city = models.CharField(max_length=32, blank=True, default='')
    ethnicity = models.CharField(max_length=3, blank=True,
                                 choices=(('W', 'White'), ('B', 'Black or African-American'),
                                          ('A', 'American Indian or Alaska Native'), ('S', 'Asian'),
                                          ('P', 'Native Hawaiian or Other Pacific Islander'), ('H', 'Hispanic or Latino'),
                                          ('O', 'Other'), ('U', 'Unknown')))


    class Meta:
        db_table = 'patient'
        ordering = [Lower('user__last_name')]

    def __str__(self):
        return '{} {}'.format(self.user.first_name, self.user.last_name)


class Payer(AuditLogModel):
    """
    This is a tenant-specific payer object. There is another "global" payer in lab.models
    """
    alternate_id = models.CharField(max_length=32, blank=True)
    # this payer code (or ID) is an industry standard
    payer_code = models.CharField(max_length=30, blank=True)
    name = models.CharField(max_length=128, blank=True)
    abbreviated_name = models.CharField(max_length=128, blank=True)
    address_1 = models.CharField(max_length=128, blank=True)
    address_2 = models.CharField(max_length=128, blank=True)
    city = models.CharField(max_length=128, blank=True)
    state = models.CharField(max_length=2, blank=True)
    zip_code = models.CharField(max_length=128, blank=True)
    phone_number = models.CharField(max_length=128, blank=True)
    payer_type = models.CharField(max_length=64, choices=(('Commercial', 'Commercial'),
                                                          ('Medicare', 'Medicare'), ('Medicaid', 'Medicaid'),
                                                          ('Workers Comp', 'Workers\' Compensation'),
                                                          ('Other', 'Other')), default='Commercial')

    class Meta:
        ordering = [Lower('name')]

    def __str__(self):
        return self.name


class PatientGuarantor(AuditLogModel):
    alternate_id = models.CharField(max_length=32, blank=True)
    user = models.OneToOneField(User, on_delete=models.CASCADE, null=True)
    patient = models.ForeignKey(Patient, related_name='guarantors',
                                on_delete=models.CASCADE, null=True, blank=True)
    middle_initial = models.CharField(max_length=10, blank=True)
    # Jr, II, etc.
    suffix = models.CharField(max_length=255, blank=True)
    birth_date = models.DateField(null=True, blank=True)
    address1 = models.CharField(max_length=255, blank=True)
    address2 = models.CharField(max_length=255, blank=True)
    sex = models.CharField(max_length=10, blank=True,
                           choices=(('M', 'Male'), ('F', 'Female'), ('NB', 'Non-Binary')))
    phone_number = models.CharField(max_length=255, blank=True)
    # social security numbers are in the format: AAA-GG-SSSS
    social_security = models.CharField(max_length=11, blank=True, default=None)
    relationship_to_patient = models.CharField(max_length=64, blank=True,
                                               choices=(('Parent', 'Parent'), ('Spouse', 'Spouse'),
                                                        ('Dependent', 'Dependent'), ('Other', 'Other')))
    zip_code = models.CharField(max_length=128, blank=True)

    def __str__(self):
        return '{} {}'.format(self.user.first_name, self.user.last_name)


class PatientStatus(AuditLogModel):
    """

    """
    patient = models.ForeignKey(Patient, related_name='status', on_delete=models.CASCADE)
    order = models.ForeignKey(orders_models.Order, related_name='patient_status', on_delete=models.CASCADE, null=True)
    created_datetime = models.DateTimeField(default=timezone.now)

    status_datetime = models.DateTimeField(null=True)
    heart_rate = models.IntegerField(null=True)
    temperature = models.DecimalField(decimal_places=2, max_digits=5, null=True)
    temperature_unit = models.CharField(max_length=2, blank=True)
    blood_pressure_systolic = models.DecimalField(decimal_places=2, max_digits=5, null=True)
    blood_pressure_diastolic = models.DecimalField(decimal_places=2, max_digits=5, null=True)
    fasting = models.BooleanField(default=False)


class PatientPayer(AuditLogModel):
    """
    Patients can have multiple payers.
    All payer information is optional
    """
    patient = models.ForeignKey('Patient', related_name='patient_payers', on_delete=models.CASCADE)

    # required field
    payer_type = models.CharField(max_length=64,
                                  choices=(('Self-Pay', 'Self-Pay'), ('Commercial', 'Commercial'),
                                           ('Medicare', 'Medicare'), ('Medicaid', 'Medicaid'),
                                           ('Workers Comp', 'Workers\' Compensation'),
                                           ('Other', 'Other')), default='Commercial')

    # status fields
    is_primary = models.BooleanField(default=False)
    is_secondary = models.BooleanField(default=False)
    is_tertiary = models.BooleanField(default=False)
    is_workers_comp = models.BooleanField(default=False)

    payer = models.ForeignKey(Payer, related_name='patient_payers', on_delete=models.CASCADE, null=True)

    member_id = models.CharField(max_length=256, blank=True)
    group_number = models.CharField(max_length=256, blank=True)

    # subscriber name provided if different from patient
    subscriber_last_name = models.CharField(max_length=255, blank=True)
    subscriber_first_name = models.CharField(max_length=255, blank=True)
    subscriber_middle_initial = models.CharField(max_length=10, blank=True)
    # Jr, II, etc.
    subscriber_suffix = models.CharField(max_length=255, blank=True)
    subscriber_birth_date = models.DateField(null=True, blank=True)
    subscriber_address1 = models.CharField(max_length=255, blank=True)
    subscriber_address2 = models.CharField(max_length=255, blank=True)
    subscriber_zip_code = models.CharField(max_length=128, blank=True)
    subscriber_sex = models.CharField(max_length=10, blank=True,
                                      choices=(('M', 'Male'), ('F', 'Female'), ('NB', 'Non-Binary')))
    subscriber_phone_number = models.CharField(max_length=255, blank=True)
    # social security numbers are in the format: AAA-GG-SSSS
    subscriber_social_security = models.CharField(max_length=11, blank=True, null=True, default=None)
    # subscriber relationship to patient
    subscriber_relationship = models.CharField(max_length=64, blank=True,
                                               choices=(('Self', 'Self'), ('Spouse', 'Spouse'),
                                                        ('Parent', 'Parent'), ('Other', 'Other')), default='Self')

    # for workers' comp use only
    workers_comp_injury_date = models.DateTimeField(null=True)
    workers_comp_adjuster = models.CharField(max_length=255, blank=True)
    workers_comp_claim_id = models.CharField(max_length=64, blank=True)

    class Meta:
        db_table = 'patient_payer'

    @property
    def subscriber_name(self):
        # only applies if subscriber relationship isn't "Self"
        if self.subscriber_relationship != 'Self':
            if self.subscriber_middle_initial:
                subscriber_name = '{} {}. {} {}'.format(self.subscriber_first_name, self.subscriber_middle_initial,
                                                        self.subscriber_last_name, self.subscriber_suffix)
            else:
                subscriber_name = '{} {} {}'.format(self.subscriber_first_name,
                                                    self.subscriber_last_name, self.subscriber_suffix)
            return subscriber_name

    def __str__(self):
        if self.payer:
            return "{}: {}".format(self.payer.name, self.member_id)
        else:
            return "{}: {}".format(self.payer_type, self.member_id)


class PatientPrescribedMedicationHistory(AuditLogModel):
    """
    Patients can have multiple medications
    """
    patient = models.ForeignKey(Patient, related_name="patient_prescribed_medication_history",
                                on_delete=models.SET_NULL, null=True)
    order = models.ForeignKey(orders_models.Order, related_name='patient_prescribed_medication_history',
                              on_delete=models.SET_NULL, null=True)
    prescribed_medications = models.ManyToManyField('lab.TestTarget')
    provider = models.ForeignKey(accounts_models.Provider, related_name="patient_prescribed_medication_history",
                                 on_delete=models.SET_NULL, blank=True, null=True)

    class Meta:
        db_table = 'patient_prescribed_medication_history'


class AdditionalDocuments(AuditLogModel):
    """
    All the additional documents for all patients
    """
    patient = models.ForeignKey(Patient, related_name="additional_documents", on_delete=models.SET_NULL, null=True)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL, null=True)
    document = models.FileField(storage=PrivateMediaStorage(), null=True)
    document_name = models.CharField(max_length=255, blank=True)
    document_type = models.CharField(max_length=255, choices=DOCUMENT_TYPES, default='', blank=True)


class PatientsDocuments(AuditLogModel):
    """
    Current additional documents for each patient
    """
    patient_document = models.ForeignKey(AdditionalDocuments, related_name='patient_documents', on_delete=models.SET_NULL,
                                         null=True)
    patient = models.ForeignKey(Patient, related_name='patient_document', on_delete=models.SET_NULL, null=True)


######################################################################################################################
# Django AuditLogs

auditlog.register(Patient)
auditlog.register(Payer)
auditlog.register(PatientPayer)
auditlog.register(PatientPrescribedMedicationHistory)
auditlog.register(AdditionalDocuments)
auditlog.register(PatientsDocuments)
