// These renderers are used for rendering handsontable data
function linkRenderer(instance, td, row, col, prop, value, cellProperties) {
  if (value) {
    td.innerHTML = value.url;
  }
}
