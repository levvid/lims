from django.urls import path

from . import views

urlpatterns = [
    # accounts
    path('accounts/', views.AccountListView.as_view(), name='account_list'),
    path('accounts/archived/', views.AccountArchivedListView.as_view(), name='account_archived_list'),
    path('accounts/create/', views.AccountCreateView.as_view(), name='account_create'),
    path('accounts/<str:account_uuid>/', views.AccountDetailsView.as_view(), name='account_details'),
    path('accounts/<str:account_uuid>/update/', views.AccountUpdateView.as_view(), name='account_update'),
    path('accounts/<str:account_uuid>/delete/', views.AccountDeleteView.as_view(), name='account_delete'),
    path('accounts/<str:account_uuid>/undo_delete/', views.AccountUndoDeleteView.as_view(), name='account_undo_delete'),


    path('accounts/<str:account_uuid>/critical_contacts/create/',
         views.CriticalContactCreateView.as_view(), name='critical_contact_create'),
    path('accounts/<str:account_uuid>/critical_contacts/<str:contact_uuid>/update/',
         views.CriticalContactUpdateView.as_view(), name='critical_contact_update'),
    path('accounts/<str:account_uuid>/critical_contacts/<str:contact_uuid>/delete/',
         views.CriticalContactDeleteView.as_view(), name='critical_contact_delete'),

    # providers
    path('providers/', views.ProviderListView.as_view(), name='provider_list'),
    path('providers/archived/', views.ProviderArchivedListView.as_view(), name='provider_archived_list'),
    path('providers/create/', views.ProviderCreateView.as_view(), name='provider_create'),
    path('providers/<str:provider_uuid>/', views.ProviderDetailsView.as_view(), name='provider_details'),
    path('providers/<str:provider_uuid>/update/', views.ProviderUpdateView.as_view(), name='provider_update'),
    path('providers/<str:provider_uuid>/delete/', views.ProviderDeleteView.as_view(), name='provider_delete'),
    path('providers/<str:provider_uuid>/undo_delete/', views.ProviderUndoDeleteView.as_view(), name='provider_undo_delete'),

    # collectors
    path('collectors/', views.CollectorListView.as_view(), name='collector_list'),
    path('collectors/archived/', views.CollectorArchivedListView.as_view(), name='collector_archived_list'),
    path('collectors/create/', views.CollectorCreateView.as_view(), name='collector_create'),
    path('collectors/<str:collector_uuid>/', views.CollectorDetailsView.as_view(), name='collector_details'),
    path('collectors/<str:collector_uuid>/update/', views.CollectorUpdateView.as_view(), name='collector_update'),
    path('collectors/<str:collector_uuid>/delete/', views.CollectorDeleteView.as_view(), name='collector_delete'),
    path('collectors/<str:collector_uuid>/undo_delete/', views.CollectorUndoDeleteView.as_view(),
         name='collector_undo_delete'),

    # autocomplete views
    path('primary_care_provider_autocomplete/', views.PrimaryCareProviderAutocomplete.as_view(), name='primary_care_provider_autocomplete'),
    path('provider_autocomplete/', views.ProviderAutocomplete.as_view(), name='provider_autocomplete'),
    path('all_provider_autocomplete/', views.AllProviderAutocomplete.as_view(), name='all_provider_autocomplete'),
    path('account_autocomplete/', views.AccountAutocomplete.as_view(), name='account_autocomplete'),
    path('collector_account_autocomplete/', views.CollectorAccountAutocomplete.as_view(),
         name='collector_account_autocomplete'),

    path('fix_passwords/', views.FixPasswords.as_view(), name='fix_password')
]