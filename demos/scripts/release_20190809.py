import django
import os

os.environ['DJANGO_SETTINGS_MODULE'] = 'main.settings'
django.setup()

from orders.scripts.ildp_test_target_setup import main as update_targets

from clients import models as client_models
from lab import models as lab_models

TARGETS = ['Ethyl Alcohol',
           'Clonazepam',
           'Barbiturates',
           'Benzodiazepines',
           'Opiates']
METHODS = [
    {
        'name': 'Specimen Validity',
        'order': 1,
        'is_within_range': True,
        'cutoff_field': None
    },
    {
        'name': 'General Chemistry',
        'order': 5,
        'is_within_range': False,
        'cutoff_field': 'Concentration'
    }
]


def main():
    """
    run after deploy to production
    :return:
    """
    # add specimen validity test method
    add_test_method(METHODS)
    # set galaxy specimen validity setting to hide
    # set galaxy research consent to true
    # set galaxy patient result request to true
    galaxy = client_models.Client.objects.get(schema_name='galaxy')
    galaxy_settings = galaxy.settings
    galaxy_settings['show_research_consent'] = True
    galaxy_settings['show_patient_result_request'] = True
    galaxy_settings['hide_specimen_validity'] = True
    galaxy.save()

    # add missing test targets
    add_test_targets(TARGETS)


def add_test_targets(target_names):
    for target_name in target_names:
        lab_models.TestTarget.objects.update_or_create(name=target_name, is_tox_target=True)


def add_test_method(methods):
    for method_dict in methods:
        lab_models.TestMethod.objects.update_or_create(name=method_dict['name'],
                                                       defaults={'display_order': method_dict['order'],
                                                                 'is_within_range': method_dict['is_within_range'],
                                                                 'cutoff_field': method_dict['cutoff_field']})


if __name__ == '__main__':
    main()
