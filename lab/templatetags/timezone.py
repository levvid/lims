from django import template
import copy
import pytz

register = template.Library()


@register.filter(name='as_timezone')
def as_timezone(value, arg):
    try:
        new_value = copy.deepcopy(value)
        return new_value.astimezone(arg)
    except AttributeError:
        return ''