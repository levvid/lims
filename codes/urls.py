from django.urls import include, path

from . import views


urlpatterns = [

    # autocomplete views
    path('icd10_autocomplete/', views.ICD10Autocomplete.as_view(), name='icd10_autocomplete'),
    path('procedure_code_autocomplete/', views.ProcedureCodeAutocomplete.as_view(), name='procedure_code_autocomplete')
]