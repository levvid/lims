# Generated by Django 2.0.2 on 2019-04-06 22:34

from django.db import migrations, models
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('clients', '0003_client_settings'),
    ]

    operations = [
        migrations.CreateModel(
            name='CLIALabsUpdateLog',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_date', models.DateTimeField(auto_now_add=True)),
                ('modified_date', models.DateTimeField(auto_now=True)),
                ('is_active', models.BooleanField(default=True)),
                ('uuid', models.UUIDField(db_index=True, default=uuid.uuid4, editable=False)),
                ('new_lab_count', models.IntegerField(null=True)),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
