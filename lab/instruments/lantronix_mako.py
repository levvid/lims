import socket
import decimal
import time

import channels.layers
import django
import logging
import os
import platform
import datetime
import sys
import serial
import json
import pathlib
import itertools

from asgiref.sync import async_to_sync
from serial.tools import list_ports

from django.db import transaction
from tenant_schemas.utils import tenant_context
from django.utils import timezone
from django.core.exceptions import ObjectDoesNotExist

os.environ['DJANGO_SETTINGS_MODULE'] = 'main.settings'
django.setup()

from orders import models as orders_models
from clients import models as clients_models
from lab_tenant import models as lab_tenant_models

from main.functions import convert_utc_to_timezone


INSTRUMENT_NAME = 'Olympus AU400'

TEXT_START_BLOCK = b'\x02'
TEXT_END_BLOCK = b'\x03'
BLOCK_END_BLOCK = b'E'
DUMMY_BLOCK = b' ' * 4

# Requisition inquiry is what the AU sends the LIS
# Requisition is what the the LIS sends back
REQUISITION_INQUIRY_START_BLOCK = b'RB'
REQUISITION_INQUIRY_END_BLOCK = b'RE'

REQUISITION_START_BLOCK = b'S '
REQUISITION_END_BLOCK = b'\x02SE\x03'

RESULTS_START_BLOCK = b'DB'
RESULTS_END_BLOCK = b'DE'


def chunks(l, n):
    """Yield successive n-sized chunks from l."""
    for i in range(0, len(l), n):
        yield l[i:i + n]


def save_data_to_lis(instrument, data_dict):
    """
    Save JSON data to LIS - create a Result object
    """
    barcode = data_dict['sample_barcode']
    tests = data_dict['tests']

    # sample must be accessioned first in order to have a barcode...is that correct?
    # tenant.timezone
    sample = orders_models.Sample.objects.filter(barcode=barcode).first()

    if sample:
        # get all tests for the sample
        for test_data in tests:
            test_code = test_data['test_code']
            test_code_obj = orders_models.InstrumentTestCode.objects.filter(test_code=test_code).first()
            test_type = test_code_obj.test_type

            try:
                # get related test object - each test is only done on one sample
                test = orders_models.Test.objects.get(sample=sample, test_type=test_type)
            except Exception as e:
                error_text = 'Test: {} was not ordered for this sample.'.format(test_type.name)
                print(error_text)
                orders_models.InstrumentError.objects.create(instrument_integration=instrument,
                                                             error_text=error_text)
                test = None

            if test:
                # using timezone.now() defaults to UTC, since our settings is using UTC
                # be careful --> Test/Result is a one-to-one relationship (although it's not reflected in db yet)
                # when a Test object is created, a Result object is also created
                # Result.result is a "qualitative" field, like "+", "-"
                # compare result_quantitative to reference ranges and save result.result
                result_quantitative = test_data['test_result']
                result_obj = orders_models.Result.objects.get(test=test)

                if test.test_type.test_method.is_within_range:

                    if result_quantitative:
                        result_quantitative = decimal.Decimal(result_quantitative).quantize(decimal.Decimal('.0001'),
                                                                                            rounding=decimal.ROUND_DOWN)
                        if result_quantitative >= decimal.Decimal('999999.9999'):
                            result_quantitative = decimal.Decimal('999999.9999')
                    try:
                        low_value = decimal.Decimal(
                            result_obj.test.test_type.test_type_ranges.get().range_low)
                        high_value = decimal.Decimal(
                            result_obj.test.test_type.test_type_ranges.get().range_high)
                    except orders_models.TestTypeRange.DoesNotExist:
                        low_value = None  # if cutoff is not provided
                        high_value = None
                    if low_value and high_value and result_quantitative:
                        if result_quantitative >= low_value and result_quantitative <= high_value:
                            result = '~'
                        elif result_quantitative < low_value:
                            result = '<'
                        else:
                            result = '>'
                    else:
                        result = ''

                else:
                    try:
                        result_quantitative = decimal.Decimal(result_quantitative).quantize(
                            decimal.Decimal('.0001'), rounding=decimal.ROUND_DOWN)
                        if result_quantitative >= decimal.Decimal('999999.9999'):
                            result_quantitative = decimal.Decimal('999999.9999')
                    except TypeError:
                        result_quantitative = None
                    positive_above_cutoff = result_obj.test.test_type.test_method.positive_above_cutoff
                    try:
                        if positive_above_cutoff:  # fault tolerance for cutoff not existing
                            cutoff_value = decimal.Decimal(
                                result_obj.test.test_type.test_type_ranges.filter(default=True).get().range_low)
                        else:
                            cutoff_value = decimal.Decimal(
                                result_obj.test.test_type.test_type_ranges.filter(default=True).get().range_high)
                    except orders_models.TestTypeRange.DoesNotExist:
                        cutoff_value = None  # if cutoff is not provided

                    if cutoff_value and result_quantitative:
                        if positive_above_cutoff:
                            if result_quantitative >= cutoff_value:
                                result = '+'
                            else:
                                result = '-'
                        else:  # negative above cutoff
                            if result_quantitative >= cutoff_value:
                                result = '-'
                            else:
                                result = '+'
                    else:
                        result = ''

                result_obj.result_quantitative = result_quantitative
                result_obj.result = result
                result_obj.save()
                print(result_obj)
                print('Saving data...')


def data_str_to_json(instrument, data_str_list, barcode_length):
    """
    Rack number --> data_str_list[2:6]
    Cup position --> data_str_list[6:8]
    Sample type --> data_str_list[8]
    Sample number (barcode) --> data_str_list[9:13]
    """
    # dummy block used for some reason
    dummy_block_length = 4
    end_of_sample_id_index = 14

    sample_number = data_str_list[10:end_of_sample_id_index].decode('utf-8')
    end_of_barcode_index = end_of_sample_id_index + dummy_block_length + barcode_length

    # parse into barcode even if empty
    barcode_str = data_str_list[end_of_sample_id_index:end_of_sample_id_index + barcode_length].decode('utf-8')

    ####################################################################################################################
    # use for testing
    # use a series of barcodes for testing
    # barcode_str = '3333333347'
    ####################################################################################################################

    data_dict = {}
    data_dict['sample_number'] = sample_number
    data_dict['sample_barcode'] = barcode_str
    # multiple tests can be done on a sample
    data_dict['tests'] = []

    # chunk barcodes by their length
    test_code_and_results_chunked = list(chunks(data_str_list[end_of_barcode_index+1:], barcode_length))

    for test in test_code_and_results_chunked:
        test_code = test[:2]
        if test_code == b'\x03':
            continue

        test_code = test_code.decode('utf-8')

        try:
            # map test codes using orders_models.InstrumentTestCode
            test_code = orders_models.InstrumentTestCode.objects.get(test_code=test_code).test_code
        except Exception as e:
            error_text = 'Test code ({}) is not mapped to a test in Dendi LIS.'.format(test_code)
            print(error_text)
            orders_models.InstrumentError.objects.create(instrument_integration=instrument,
                                                         error_text=error_text)
            continue
        try:
            test_result = float(test[2:8])
        except ValueError as e:
            test_result = 0
            error_text = 'Invalid result {} for test code: {}'.format(test[2:8], test_code)
            print(error_text)
            orders_models.InstrumentError.objects.create(instrument_integration=instrument,
                                                         error_text=error_text)

        data_dict['tests'].append({'test_code': test_code,
                                   'test_result': test_result})
    return data_dict


def requisition_mapper(instrument, req_bytes, barcode_length):
    """
    Parse requisition data and return associated test codes in string blocks
    """
    rack_number = req_bytes[3:7].decode('utf-8')

    cup_position = req_bytes[7:9].decode('utf-8')
    sample_type = bytes([req_bytes[9]]).decode('utf-8')
    sample_number = req_bytes[10:14].decode('utf-8')

    # print(sample_type)
    # parse into barcode even if empty
    barcode = req_bytes[14:14 + barcode_length].decode('utf-8')


    ####################################################################################################################
    # For testing - you don't want to actually change the barcode value
    # if no barcode is provided, use a barcode with 0s
    # for index, char in enumerate(barcode):
    #     if char == ' ':
    #         barcode[index] = '0'
    ####################################################################################################################
    # get sample
    requisition_dict = {}
    requisition_dict['rack_number'] = rack_number
    requisition_dict['cup_position'] = cup_position
    requisition_dict['sample_number_block'] = sample_number
    requisition_dict['sample_type_block'] = sample_type
    requisition_dict['barcode_block'] = barcode

    barcode_str = ''.join(barcode)
    # get sample and related tests and test types
    ####################################################################################################################
    # use barcode_str
    # sample = orders_models.Sample.objects.filter(barcode='3333333334').first()
    sample = orders_models.Sample.objects.filter(barcode=barcode_str).first()
    ####################################################################################################################

    test_code_list = []
    if sample:
        # get all tests for the sample
        tests = sample.tests.all()
        for test in tests:
            test_codes = orders_models.InstrumentTestCode.objects.filter(instrument_integration=instrument,
                                                                         test_type=test.test_type)\
                .values_list('test_code', flat=True)
            # if a test code isn't mapped, then what happens???
            if test_codes:
                for test_code in test_codes:
                    test_code = list(test_code)
                    test_code_list.append(test_code)

        # flatten the list so that it's not a list of lists
        test_code_list = [item for sublist in test_code_list for item in sublist]
        requisition_dict['test_codes_block'] = ''.join(test_code_list)
    else:
        # if a barcode can't be found, what happens?
        print('No sample matching barcode.')
    return requisition_dict


def make_connection(instrument, barcode_length):
    # Create a TCP/IP socket
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    # Bind the socket to the port
    server_address = ('', 10002)
    print('Starting up on {} port {}'.format(*server_address))
    sock.bind(server_address)

    # Listen for incoming connections
    sock.listen(1)
    req_begin_status = False
    result_begin_status = False
    previous_data = None
    while True:
        # Wait for a connection
        print('waiting for a connection')
        connection, client_address = sock.accept()
        try:
            print('connection from', client_address)

            # Receive the data in small chunks and retransmit it
            while True:
                data = connection.recv(4096)
                if data == b'':  # do not send ack for empty data from AU
                    # print('Skipping ACK sending')  # skip and do not print
                    continue
                print('received data is {}'.format(data))

                if data == b'\x06':
                    print('Requisition data successfully received by AU (ACK).')
                    continue



                try:
                    print('sending ACK')
                    connection.send(str.encode('\x06'))
                    print('ACK sent!')
                except BrokenPipeError:
                    print('Ran into broken pipe error during ack send')

                # if data == b'\x02RB\x03':
                #     print('req beginning')
                #     req_begin_status = True
                #     continue
                #
                # if data == b'\x02DB\x03':
                #     print('req beginning')
                #     result_begin_status = True
                #     continue

                # if data == b'\x02RE\x03':
                #     req_begin_status = False
                #     continue

                if data == b'RB\x03' and previous_data == b'\x02':
                    print('req beginning RB')
                    req_begin_status = True
                    continue

                if data == b'DB\x03' and previous_data == b'\x02':
                    print('req beginning DB')
                    result_begin_status = True
                    continue

                if data == b'RE\x03' and previous_data == b'\x02':
                    req_begin_status = False
                    continue

                if data == b'\x15':
                    print('Requisition data transfer error (NAK).')
                    continue
                if len(data) < 2:
                    print('Short data {}'.format(data))
                    previous_data = data
                    continue

                if req_begin_status and bytes([data[1]]) == b'R' and len(data) > 4:
                    requisition_dict = requisition_mapper(instrument, data, barcode_length)
                    rack_number_block = requisition_dict['rack_number'].encode('utf-8')
                    cup_position_block = requisition_dict['cup_position'].encode('utf-8')
                    sample_type_block = requisition_dict['sample_type_block'].encode('utf-8')
                    sample_number_block = requisition_dict['sample_number_block'].encode('utf-8')
                    barcode_block = requisition_dict['barcode_block'].encode('utf-8')
                    test_codes_block = requisition_dict['test_codes_block'].encode('utf-8')
                    # requisition block
                    requisition = TEXT_START_BLOCK + REQUISITION_START_BLOCK + rack_number_block + \
                                  cup_position_block + sample_type_block + \
                                  sample_number_block + barcode_block + DUMMY_BLOCK + BLOCK_END_BLOCK + \
                                  test_codes_block + TEXT_END_BLOCK + REQUISITION_END_BLOCK
                    try:
                        print('Sending requisition data...')
                        print(requisition)
                        connection.send(requisition)
                        print("Done sending req...")
                    except BrokenPipeError:
                        print('Broken pipe error during req send')

                print('Data received: {}'.format(data))
                # results

                if result_begin_status and bytes([data[1]]) == b'D' and len(data) > 4:
                    # print('Receiving data...{}'.format(data))

                    data_dict = data_str_to_json(instrument, data, barcode_length)
                    # save data_dict to LIS
                    save_data_to_lis(instrument, data_dict)
                    print("data dict...")
                    print(data_dict)
                # previous_data = data
        finally:
            # Clean up the connection
            print("Closing current connection")
            connection.close()


if __name__ == '__main__':
    tenant_name = 'ildp'
    tenant = clients_models.Client.objects.get(schema_name=tenant_name)
    with tenant_context(tenant):
        instrument = lab_tenant_models.InstrumentIntegration.objects.get(internal_instrument_name=INSTRUMENT_NAME)
        make_connection(instrument, 10)
