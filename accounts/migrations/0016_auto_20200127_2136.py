# Generated by Django 2.2.2 on 2020-01-27 21:36

from django.db import migrations
import django.db.models.functions.text


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0015_auto_20191126_1851'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='account',
            options={'ordering': [django.db.models.functions.text.Lower('name')]},
        ),
        migrations.AlterModelOptions(
            name='collector',
            options={'ordering': [django.db.models.functions.text.Lower('user__last_name')]},
        ),
        migrations.AlterModelOptions(
            name='provider',
            options={'ordering': [django.db.models.functions.text.Lower('user__last_name')]},
        ),
    ]
