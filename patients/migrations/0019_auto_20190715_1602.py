# Generated by Django 2.2.2 on 2019-07-15 16:02

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('patients', '0018_patient_patient_id'),
    ]

    operations = [
        migrations.AlterField(
            model_name='patientprescribedmedicationhistory',
            name='provider',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='patient_prescribed_medication_history', to='accounts.Provider'),
        ),
    ]

