import pytz
import uuid as uuid_lib

from django.db import models
from django.contrib.auth.models import AbstractUser
from django.utils import timezone
from django.contrib.postgres.fields import CICharField

from main.managers import ActiveManager, AllManager, ActiveUserManager, AllUserManager

from auditlog.registry import auditlog
from auditlog.models import AuditlogHistoryField


########################################################################################################################
class User(AbstractUser):
    # Users can assume only one role
    # Source: https://simpleisbetterthancomplex.com/tutorial/2018/01/18/how-to-implement-multiple-user-types-with-django.html

    # Note: since we're inheriting from Django's default AbstractUser, we'll use is_active instead of active

    USER_TYPE_CHOICES = (
        # we use these digits so that we can add in user types in between without disrupting workflow
        # for example, we can use request.user_type < 10 for any lab employee,
        # or request.user_type >= 10 for any non lab user

        # employee types
        (1, 'lab_admin'),
        (2, 'lab_collaborator'),

        # account user type
        (9, 'Account'),
        # provider types
        (10, 'provider'),
        (11, 'collector'),

        # patient types
        (20, 'patient'),
        (21, 'patient_guarantor'),
    )
    # User timezone options
    TIMEZONES = tuple(zip(pytz.all_timezones, pytz.all_timezones))

    # NO DEFAULTS for user_type
    uuid = models.UUIDField(
        db_index=True,
        default=uuid_lib.uuid4,
        editable=False,
        unique=True
    )
    user_type = models.PositiveSmallIntegerField(choices=USER_TYPE_CHOICES)
    timezone = models.CharField(max_length=32, choices=TIMEZONES, default='UTC')

    two_factor_auth_secret = models.CharField(max_length=16, null=True, default=None)

    # use as such: Model.objects.filter() for only active objects
    # Model.all_objects.filter() for all objects
    objects = ActiveUserManager()
    all_objects = AllUserManager()

    # required fields for when user is created using python manage.py createsuperuser
    REQUIRED_FIELDS = ['user_type', 'email']

    def save(self, *args, **kwargs):
        # Override save method so that if User is deleted (not active), then update the username to the UUID of the User
        # Note: you cannot override the save method on objects using custom managers
        if not self.is_active:
            self.username = self.uuid
        # if email isn't provided, then username will be auto-generated to prevent bugs
        if not self.email and not self.username:
            self.username = str(self.uuid) + '@dendisoftware.com'

        super().save(*args, **kwargs)  # Call the "real" save() method.

    def __str__(self):
        if self.last_name and self.first_name:
            return '{}, {}'.format(self.last_name, self.first_name)
        else:
            return '{}'.format(self.email)


class AuditLogModel(models.Model):
    # this model is used to add created and modified timestamps for all descendant models
    # as well as add an active flag to all models
    # https://stackoverflow.com/questions/4754485/dry-way-to-add-created-modified-by-and-time
    created_date = models.DateTimeField(auto_now_add=True)
    modified_date = models.DateTimeField(auto_now=True)
    is_active = models.BooleanField(default=True)
    history = AuditlogHistoryField()

    uuid = models.UUIDField(
        db_index=True,
        default=uuid_lib.uuid4,
        editable=False
    )

    # use as such: Model.objects.filter() for only active objects
    # Model.all_objects.filter() for all objects
    objects = ActiveManager()
    all_objects = AllManager()

    class Meta:
        # it's an abstract model, so a table isn't created
        abstract = True


########################################################################################################################

# Note: these lab user types are mutually exclusive, meaning a user cannot be both a LabAdmin and a LabCollaborator
class LabAdmin(AuditLogModel):
    uuid = models.UUIDField(
        db_index=True,
        default=uuid_lib.uuid4,
        editable=False,
        unique=True,
    )
    user = models.OneToOneField(User, on_delete=models.CASCADE, null=True)

    class Meta:
        # don't specify db_table next time
        db_table = 'lab_admin'
        ordering = ['user__last_name']

    def __str__(self):
        return 'Admin: {} {}'.format(self.user.first_name, self.user.last_name)


class LabCollaborator(AuditLogModel):
    uuid = models.UUIDField(
        db_index=True,
        default=uuid_lib.uuid4,
        editable=False,
        unique=True,
    )
    user = models.OneToOneField(User, on_delete=models.CASCADE, null=True)

    class Meta:
        # don't specify db_table next time
        db_table = 'lab_collaborator'
        ordering = ['user__last_name']

    def __str__(self):
        return 'Collaborator: {} {}'.format(self.user.first_name, self.user.last_name)


########################################################################################################################

class InstrumentIntegration(AuditLogModel):
    instrument = models.ForeignKey('lab.Instrument', related_name='integrations', on_delete=models.CASCADE)
    internal_instrument_name = models.CharField(max_length=255)
    serial_number = models.CharField(max_length=255, blank=True)
    # auto_complete_order field to be deprecated by after_data_import LIS-322
    auto_complete_order = models.BooleanField(default=False)
    after_data_import = models.CharField(
        choices=(('Do nothing', 'Do nothing'),
                 ('Automatically complete orders', 'Automatically complete orders'),
                 ('Automatically report orders', 'Automatically report orders')),
        default='Do nothing',
        max_length=64,
    )

    def __str__(self):
        return self.internal_instrument_name


########################################################################################################################

class OffsiteLaboratoryIntegration(AuditLogModel):
    uuid = models.UUIDField(
        db_index=True,
        default=uuid_lib.uuid4,
        editable=False,
        unique=True,
    )
    offsite_laboratory = models.ForeignKey('lab.OffsiteLaboratory', related_name='integrations',
                                           on_delete=models.CASCADE)
    sending_facility_id = models.CharField(max_length=64, blank=True)


class InHouseLabLocation(AuditLogModel):
    """
    In house lab locations for labs with multiple locations
    """
    name = models.CharField(max_length=255)
    address1 = models.CharField(max_length=255, blank=True)
    address2 = models.CharField(max_length=255, blank=True)
    zip_code = models.CharField(max_length=128, blank=True)
    lab_director = models.CharField(max_length=255, blank=True)
    alias = models.CharField(max_length=255)
    phone_number = models.CharField(max_length=255, blank=True)
    fax_number = models.CharField(max_length=255, blank=True)

    def __str__(self):
        return self.name


########################################################################################################################

class LabDepartment(AuditLogModel):
    """
    Lab departments for labs that want to organize/restrict test profiles by department.
    """
    name = CICharField(max_length=64)

    def __str__(self):
        return self.name


########################################################################################################################

# Django AuditLogs
auditlog.register(LabAdmin)
auditlog.register(LabCollaborator)
auditlog.register(InstrumentIntegration)
auditlog.register(OffsiteLaboratoryIntegration)
auditlog.register(LabDepartment)
auditlog.register(InHouseLabLocation)
