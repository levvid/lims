import datetime
import timeit

from django.http import HttpResponse, JsonResponse

from . import models
from accounts import models as accounts_models
from main.functions import date_string_or_none


def patient_table(request):
    """deprecated for patient_table_filtered"""
    # limit results requested by providers
    if request.user.user_type == 10:
        provider = accounts_models.Provider.objects.get(user__id=request.user.id)
        patients = provider.patient_set.order_by('user__last_name').prefetch_related('user').distinct().\
            values('user__first_name', 'user__last_name', 'middle_initial', 'sex', 'birth_date', 'uuid')
    elif request.user.user_type == 9:
        account = accounts_models.Account.objects.get(user__id=request.user.id)
        #patients = models.Patient.objects.filter(orders__account=account).order_by('user__last_name').prefetch_related('user').distinct()
        patients = models.Patient.objects.filter(providers=account.providers).order_by('user__last_name').prefetch_related('user').distinct()
    else:
        patients = models.Patient.objects.all().order_by('user__last_name').prefetch_related('user').\
            values('user__first_name', 'user__last_name', 'middle_initial', 'sex', 'birth_date', 'uuid')

    results = []
    for patient in patients:
        # format name
        uuid = patient.get('uuid')
        first_name = patient.get('user__first_name')
        last_name = patient.get('user__last_name')
        middle_initial = patient.get('middle_initial')
        birth_date = patient.get('birth_date')
        sex = patient.get('sex')

        last_name = {
            'text': patient.get('user__last_name'),
            'url': '<a href="/patients/{}">{}</a>'.format(
                uuid, last_name
            )
        }
        first_name = {
            'text': patient.get('user__first_name'),
            'url': '<a href="/patients/{}">{}</a>'.format(
                uuid, first_name
            )
        }
        middle_initial = {
            'text': patient.get('middle_initial'),
            'url': '<a href="/patients/{}">{}</a>'.format(
                uuid, middle_initial
            )
        }
        patient_data = {
            'first_name': first_name,
            'last_name': last_name,
            'middle_initial': middle_initial,
            'sex': sex,
            'birth_date': date_string_or_none(birth_date),
        }

        results.append(patient_data)
    return JsonResponse(results, safe=False)


def patient_table_filtered(request, first_name, last_name, middle, sex, birth_date_string):
    # limit results requested by providers
    if request.user.user_type == 10:
        provider = accounts_models.Provider.objects.get(user__id=request.user.id)
        patients = provider.patient_set.order_by('user__last_name').prefetch_related('user').distinct()
    elif request.user.user_type == 9:
        account = accounts_models.Account.objects.get(user__id=request.user.id)
        patients = models.Patient.objects.filter(providers__accounts=account) | models.Patient.objects.filter(orders__account=account)
        patients = patients.order_by('user__last_name').prefetch_related('user').distinct()
    elif request.user.user_type == 11:  # collector user
        collector = accounts_models.Collector.objects.get(user__id=request.user.id)
        patients = models.Patient.objects.filter(providers__accounts__collectors=collector)
        patients = patients.order_by('user__last_name').prefetch_related('user').distinct()
    else:
        patients = models.Patient.objects.all().order_by('user__last_name').prefetch_related('user').distinct()
    if first_name != '_':
        patients = patients.filter(user__first_name__istartswith=first_name)
    if last_name != '_':
        patients = patients.filter(user__last_name__istartswith=last_name)
    if middle != '_':
        patients = patients.filter(middle_initial=middle)
    if sex != '_':
        patients = patients.filter(sex=sex)
    if birth_date_string != '_':
        birth_date = datetime.datetime.strptime(birth_date_string, '%m-%d-%Y')
        patients = patients.filter(birth_date=birth_date)

    patient_values = patients.values(
        'user__first_name',
        'user__last_name',
        'middle_initial',
        'sex',
        'birth_date',
        'uuid')

    results = []
    for patient in patient_values:
        # format name
        uuid = patient.get('uuid')
        first_name = patient.get('user__first_name')
        last_name = patient.get('user__last_name')
        middle_initial = patient.get('middle_initial')
        birth_date = patient.get('birth_date')
        sex = patient.get('sex')

        last_name = {
            'text': patient.get('user__last_name'),
            'url': '<a href="/patients/{}">{}</a>'.format(
                uuid, last_name
            )
        }
        first_name = {
            'text': patient.get('user__first_name'),
            'url': '<a href="/patients/{}">{}</a>'.format(
                uuid, first_name
            )
        }
        middle_initial = {
            'text': patient.get('middle_initial'),
            'url': '<a href="/patients/{}">{}</a>'.format(
                uuid, middle_initial
            )
        }
        patient_data = {
            'first_name': first_name,
            'last_name': last_name,
            'middle_initial': middle_initial,
            'sex': sex,
            'birth_date': date_string_or_none(birth_date),
        }

        results.append(patient_data)
    return JsonResponse(results, safe=False)

