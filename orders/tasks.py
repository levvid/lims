import traceback
from datetime import datetime

import django
import os
import time

import channels.layers
from asgiref.sync import async_to_sync
from celery import shared_task
from django.db import transaction
from django.db import models as django_models
from django.utils import timezone
from django.core import serializers
from tenant_schemas.utils import tenant_context

from main.functions import convert_utc_to_tenant_timezone_with_workers, convert_utc_to_timezone
from orders import functions
from orders.generate_operational_report import generate_csv_report, generate_excel_report
from orders import parse_orders_in
from public.email import send_email_sendgrid

os.environ['DJANGO_SETTINGS_MODULE'] = 'main.settings'
django.setup()

from orders import models, functions, generate_report_docraptor, generate_report, generate_results_out_hl7, generate_billing_hl7, generate_billing_hl7_labrcm
import clients.models as clients_models

DEVELOPER_EMAIL = os.environ.get('DEVELOPER_EMAIL')
PRODUCTION_EMAIL = 'support@dendisoftware.com'

@shared_task
def get_hl7_orders_in():
    """
    Get all orders_in hl7 messages and create orders
    """
    parse_orders_in.get_orders_in_hl7()


# if preliminary = False that's a real report,
# preliminary report = true for draft reports
@shared_task
def generate_order_report(tenant_id, order_uuid, approved_by_id, preliminary=False):
    """
    Generates draft or real report pdf, send HL7 message to billing and EMRs, set Order status flags
    :param tenant_id:
    :param order_uuid:
    :param preliminary: denotes draft report
    :return:
    """
    JSONSerializer = serializers.get_serializer("json")

    class JSONWithURLSerializer(JSONSerializer):
        def handle_field(self, obj, field):
            value = field.value_from_object(obj)
            if isinstance(field, django_models.FileField):
                self._current[field.name] = value.url
            elif isinstance(value, datetime):
                self._current[field.name] = convert_utc_to_tenant_timezone_with_workers(tenant_id, value)
            else:
                return super(JSONWithURLSerializer, self).handle_field(obj, field)

    tenant = clients_models.Client.objects.get(id=tenant_id)
    with tenant_context(tenant):
        order = models.Order.objects.get(uuid=order_uuid)
        approved_by_user = models.User.objects.get(id=approved_by_id)
        approved_by = {'id': approved_by_id,
                       'user': str(approved_by_user)}

        use_docraptor = tenant.settings.get('use_docraptor', False)

        order_report_date = timezone.now()
        if not preliminary:  # real report
            order.report_date = order_report_date  # only update for real reports
            order.save(update_fields=['report_date'])

        num_reported_tests = 0
        report_generation_error = False
        try:
            if use_docraptor:
                # toxicology reports use approved by argument
                num_reported_tests = generate_report_docraptor.generate_pdf_report(tenant.id, order_uuid,
                                                                                   approved_by_id,
                                                                                   order_report_date, preliminary)
            else:
                # galaxy reports don't use approved by, so don't use that argument
                generate_report.generate_pdf_report(tenant.id, order_uuid, preliminary)

            reports = models.Report.objects.filter(order=order, preliminary=False).order_by('-created_date')
            preliminary_reports = models.Report.objects.filter(order=order, preliminary=True).order_by('-created_date')

            # send hl7 messages to EMRs and Billing
            if not preliminary:
                # format results outbound hl7 data and drop to S3
                if order.account.results_out and (not order.partial or order.account.results_out_allow_partial):
                    generate_results_out_hl7.send_results_out_hl7(tenant.id, order.uuid)
                # send billing hl7 message if order has results in at least one test
                # waiting on feedback on the order's specimens are valid
                # if num_reported_tests > 0 and order.validity_string == 'Within normal range':
                if num_reported_tests > 0 and not order.is_billed:  # do not bill for amended reports
                    # do not generate for dev and staging environments
                    environment = 'Development'
                    if os.environ.get('DEBUG') == 'False' and not os.environ.get('STAGING') == 'True':  # production
                        environment = 'Production'
                        functions.bill_order(tenant, order, preliminary, environment)
                    elif os.environ.get('STAGING') == 'True':  # Staging environment
                        environment = 'Staging'
                        functions.bill_order(tenant, order, preliminary, environment)
                    else:  # development
                        functions.bill_order(tenant, order, preliminary, environment)
                report_url = reports[0].pdf_file.url
            else:
                report_url = preliminary_reports[0].pdf_file.url
        except Exception as err:
            reports = models.Report.objects.filter(order=order, preliminary=False).order_by('-created_date')
            report_generation_error = True
            report_url = '#'  # url to remain on the same page
            print("Error occurred during report generation!")
            subject = 'Report Generation Error'
            error = '<br><br><u><b>Error:</b></u><br><br>{}'.format(traceback.format_exc().replace('\n', '<br>'))
            print('{}'.format(error))

            if os.environ.get('DEBUG') == 'False':  # prod and staging should send emails to support@dendisoftware.com
                content = 'Error occurred in Environment Production.{}'.format(error)
                if os.environ.get('STAGING') == 'True':  # staging
                    content = 'Error occurred in Environment Staging.{}'.format(error)
                send_email_sendgrid(PRODUCTION_EMAIL,
                                    PRODUCTION_EMAIL,
                                    subject,
                                    content)
            else:
                content = 'Error occurred in Environment Development.{}'.format(error)
                send_email_sendgrid(DEVELOPER_EMAIL,
                                    DEVELOPER_EMAIL,
                                    subject,
                                    content)

        # send message to change order status label
        serializer = JSONWithURLSerializer()
        data = serializer.serialize(reports, fields=('created_date', 'amended', 'pdf_file'))

        channel_layer = channels.layers.get_channel_layer()
        async_to_sync(channel_layer.group_send)(str(order_uuid), {
            'type': 'status_update',
            'message': 'Reported' if not report_generation_error else 'Error',
            'report_url': report_url,
            'preliminary': preliminary,
            'amended_reports': data,
            'approved_by': approved_by,
        })

        if preliminary:
            order.creating_preliminary_report = False
            order.save(update_fields=['creating_preliminary_report'])
        else:
            order.creating_report = False
            if not report_generation_error:  # only change order to reported if there are no errors
                order.reported = True
                order.completed = True
            else:
                order.reporting_error = True
                order.save(update_fields=['reporting_error'])
            order.save(update_fields=['reported', 'completed', 'creating_report', 'is_billed'])

        time.sleep(30)
        print("Resending message....")

        #  Redundant message to handle cases when page loads slowly
        async_to_sync(channel_layer.group_send)(str(order_uuid), {
            'type': 'status_update',
            'message': 'Reported' if not report_generation_error else 'Error',
            'report_url': report_url,
            'preliminary': preliminary,
            'amended_reports': data,
            'approved_by': approved_by,
        })


@shared_task
def assign_accession_number(order_id, tenant_id):
    """
    Assign a sequential accession number to an Order
    :param order_id:
    :param tenant_id:
    :return:
    """
    tenant = clients_models.Client.objects.get(id=tenant_id)
    date_encoded_accession_number = tenant.settings.get('enable_date_encoded_accession_number', False)
    date_prefix = convert_utc_to_timezone(tenant.timezone, timezone.now()).date().strftime("%y%m%d")

    with tenant_context(tenant):
        with transaction.atomic():
            order = models.Order.objects.get(id=order_id)

            try:
                accession_number_counter = models.Counter.objects.select_for_update().get(name='Accession Number')
            except models.Counter.DoesNotExist:
                accession_number_counter = models.Counter.objects.create(name='Accession Number',
                                                                         int_value=0)

            last_accession_number = accession_number_counter.int_value
            if date_encoded_accession_number:
                if last_accession_number:
                    last_date_prefix = str(last_accession_number)[:6]
                    if last_date_prefix == date_prefix:
                        last_accession_counter = int(str(last_accession_number)[-5:])
                        accession_counter = str(last_accession_counter + 1).zfill(5)
                        accession_number = int(last_date_prefix + accession_counter)
                    else:
                        accession_number = int(date_prefix + '00001')
                else:
                    accession_number = int(date_prefix + '00001')
            else:
                if last_accession_number:
                    accession_number = last_accession_number + 1
                else:
                    accession_number = 1

            # check if accession number exists
            while models.Order.objects.filter(accession_number=str(accession_number)):
                accession_number += 1

            accession_number_counter.int_value = accession_number
            accession_number_counter.save(update_fields=['int_value'])

            order.accession_number = str(accession_number)
            order.save(update_fields=['accession_number'])

    return accession_number


@shared_task
def create_order_with_code(tenant_id):
    """
    Create an Order object and assign an order code.
    Format: YYYY-9999999, e.g. 2019-0000001
    :param tenant_id:
    :return: Order.id
    """
    tenant = clients_models.Client.objects.get(id=tenant_id)
    date_prefix = convert_utc_to_timezone(tenant.timezone, timezone.now()).date().strftime('%Y')

    with tenant_context(tenant):
        with transaction.atomic():
            try:
                order_code_counter = models.Counter.objects.select_for_update().get(name='Order Code')
            except models.Counter.DoesNotExist:
                order_code_counter = models.Counter.objects.create(
                    name='Order Code',
                    value=date_prefix + '-0000000')

            last_order_code = order_code_counter.value
            last_order_code_prefix = last_order_code[:4]
            last_code_suffix = int(last_order_code[-7:])
            if date_prefix == last_order_code_prefix:
                code_suffix = str(last_code_suffix + 1).zfill(7)
            else:
                code_suffix = '0000001'
            code = date_prefix + '-' + code_suffix

            order = models.Order.objects.create(code=code)
            order_code_counter.value = code
            order_code_counter.save(update_fields=['value'])
    return order.id


@shared_task
def import_instrument_data(data_in_uuid, tenant_id, user_id):
    """
    Async function for auto instrument data import
    :param data_in_uuid:
    :param tenant_id:
    :param user_id: User that will be saved as approval user
    :return:
    """
    tenant = clients_models.Client.objects.get(id=tenant_id)

    with tenant_context(tenant):
        data_in_object = models.DataIn.objects.get(uuid=data_in_uuid)
        data_in_object.is_completed = False
        data_in_object.is_queued = True
        data_in_object.save()
        try:
            functions.import_instrument_data(data_in_uuid, tenant_id, user_id)
            data_in_object.is_completed = True
            data_in_object.is_queued = False
            data_in_object.processed_datetime = timezone.now()
            data_in_object.save(update_fields=['is_completed', 'is_queued', 'processed_datetime'])
            channel_layer = channels.layers.get_channel_layer()
            async_to_sync(channel_layer.group_send)(str(data_in_uuid), {
                'type': 'status_update',
                'message': '<i class="fas fa-check"></i> Processed'
            })
        except Exception as e:
            data_in_object.is_queued = False
            data_in_object.has_error = True
            data_in_object.error_message = str(e)[:127]
            data_in_object.save(update_fields=['is_queued', 'has_error', 'error_message'])
            channel_layer = channels.layers.get_channel_layer()
            async_to_sync(channel_layer.group_send)(str(data_in_uuid), {
                'type': 'status_update',
                'message': 'Error',
            })


@shared_task
def generate_operational_report_csv(tenant_id, report_id, start_date, end_date, date_filter):
    """
    Async function for generation of csv operational reports
    :param tenant_id:
    :param report_id:
    :param start_date:
    :param end_date:
    :param date_filter:
    :return:
    """
    tenant = clients_models.Client.objects.get(id=tenant_id)

    with tenant_context(tenant):
        operational_report = models.OperationalReport.objects.get(id=report_id)
        try:
            generate_csv_report(tenant_id, report_id, start_date, end_date, date_filter)
            operational_report.is_queued = False
            operational_report.is_completed = True
            operational_report.save(update_fields=['is_queued', 'is_completed'])
        except Exception as e:
            operational_report.is_queued = False
            operational_report.has_error = True
            operational_report.error_message = str(e)[:127]
            operational_report.save(update_fields=['is_queued', 'has_error', 'error_message'])


@shared_task
def generate_operational_report_excel(tenant_id, report_id, start_date, end_date, date_filter):
    """
    Async function for generation of excel operational reports
    :param tenant_id:
    :param report_id:
    :param start_date:
    :param end_date:
    :param date_filter:
    :return:
    """
    tenant = clients_models.Client.objects.get(id=tenant_id)

    with tenant_context(tenant):
        operational_report = models.OperationalReport.objects.get(id=report_id)
        try:
            generate_excel_report(tenant_id, report_id, start_date, end_date, date_filter)
            operational_report.is_queued = False
            operational_report.is_completed = True
            operational_report.save(update_fields=['is_queued', 'is_completed'])
        except Exception as e:
            operational_report.is_queued = False
            operational_report.has_error = True
            operational_report.error_message = str(e)[:127]
            operational_report.save(update_fields=['is_queued', 'has_error', 'error_message'])
