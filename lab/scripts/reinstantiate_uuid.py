import django
import logging
import os
import sys
import uuid as uuid_lib

from django.db import transaction

os.environ['DJANGO_SETTINGS_MODULE'] = 'main.settings'
django.setup()

from .. import models

"""
Adding uuid field for a model will add the same uuid for all preexisting objects.
Use this script to give every object a new, probabilistically distinct uuid
"""

MODELS = [models.Account, models.Provider, models.Order, models.Sample,
          models.Test, models.TestType, models.Procedure, models.ProcedureType,
          models.Patient, models.ICD10CM]


@transaction.atomic
def main():
    for model in MODELS:
        logging.info("reinstantiating uuid values for {}".format(model))
        model_objects = model.objects.all()
        for model_object in model_objects:
            uuid = uuid_lib.uuid4()
            logging.info("saving {} uuid as {}".format(model_object, uuid))
            model_object.uuid = uuid
            model_object.save()


if __name__ == '__main__':
    logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)
    main()
