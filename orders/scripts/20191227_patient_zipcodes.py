import os
import sys

import django
import logging

from geopy import Nominatim

os.environ['DJANGO_SETTINGS_MODULE'] = 'main.settings'
django.setup()

from tenant_schemas.utils import tenant_context
from clients import models as clients_models
from patients import models as patients_models
from orders import functions

TENANT_NAME = 'ildp'


def generate_patient_zipcodes():
    """
    :return:
    """
    tenant = clients_models.Client.objects.get(schema_name=TENANT_NAME)

    with tenant_context(tenant):
        patients = patients_models.Patient.objects.filter(zip_code__exact='')
        for patient in patients:
            address = patient.address1.split(',') if patient.address1 else []
            if len(address) >= 2:
                address_1 = address[0]
            else:
                address_1 = patient.address1

            if not address_1 or address_1 == '':
                print('address for patient {} is blank'.format(patient.user.first_name))
                continue

            zip_code = patient.zip_code
            if not zip_code or zip_code == '':
                zip_code = functions.get_zipcode(address_1)
                if zip_code:
                    patient.zip_code = zip_code
                    patient.save(update_fields=['zip_code'])
                    print('Added zip code {} to DB for address {}'.format(zip_code, address_1))

        print('{} patient records were updated'.format(len(patients)))


if __name__ == '__main__':
    logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)
    # geolocator = Nominatim()
    generate_patient_zipcodes()
