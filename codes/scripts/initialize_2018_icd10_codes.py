import boto3
import csv
import django
import os
import tempfile
import timeit

os.environ['DJANGO_SETTINGS_MODULE'] = 'main.settings'
django.setup()

from .. import models

CSV_KEY = 'codes/2018icd10codes.csv'
BUCKET_NAME = 'dendi-lis'


def main():
    fill_2018_icd10_codes()


def fill_2018_icd10_codes():
    # initialize boto s3 connection
    s3 = boto3.client(
        's3',
        aws_access_key_id=os.environ.get('S3_ACCESS_KEY'),
        aws_secret_access_key=os.environ.get('S3_SECRET_KEY'))

    with tempfile.NamedTemporaryFile() as temp:
        # download source data from s3
        s3.download_file(BUCKET_NAME, CSV_KEY, temp.name)

        csv_reader = csv.DictReader(open(temp.name), delimiter=',', quotechar='"')
        code_list = []

        for row in csv_reader:
            # strip whitespace and quotations
            for column in row:
                try:
                    row[column] = row[column].strip().replace('"', '')
                except AttributeError:
                    try:
                        row[column] = row[column][0].strip().replace('"', '')
                    except TypeError:
                        pass
                except TypeError:
                    pass
            # create ICD10CM objects

            # deal with full_description sometimes being cutoff
            try:
                extra_field = row[None]
            except KeyError:
                extra_field = None

            if extra_field:
                code_list.append(models.ICD10CM(
                    category=row['Category Code'],
                    diagnosis_code=row['Diagnosis Code'],
                    full_code=row['Full Code'],
                    abbreviated_description=row['Abbreviated Description'],
                    full_description=row['Full Description'] + ', ' + row['Category Title'],
                    category_title=extra_field
                ))
            else:
                code_list.append(models.ICD10CM(
                    category=row['Category Code'],
                    diagnosis_code=row['Diagnosis Code'],
                    full_code=row['Full Code'],
                    abbreviated_description=row['Abbreviated Description'],
                    full_description=row['Full Description'],
                    category_title=row['Category Title']
                ))

        # bulk insert
        models.ICD10CM.objects.bulk_create(code_list)



if __name__ == '__main__':
    main()

