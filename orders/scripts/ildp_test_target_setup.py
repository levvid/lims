import django

import logging
import os
import sys

from django.db import transaction

os.environ['DJANGO_SETTINGS_MODULE'] = 'main.settings'
django.setup()

from lab import models as lab_models

@transaction.atomic
def main():
    """
    Add TestTargets from ILDP requisition_form with common name and category
    :return:
    """
    # Opiates/Opioids
    lab_models.TestTarget.objects.update_or_create(name='Codeine', defaults={'common_name': ['Tylenol III'], 'category': 'Opiates/Opioids', 'is_tox_target': True, 'is_medication': True})
    lab_models.TestTarget.objects.update_or_create(name='Morphine', defaults={'common_name': ['MOP'], 'category': 'Opiates/Opioids', 'is_tox_target': True, 'is_medication': True})
    lab_models.TestTarget.objects.update_or_create(name='Hydrocodone', defaults={'common_name': ['Vicodin, Norco'], 'category': 'Opiates/Opioids',
                                                                      'is_tox_target': True, 'is_medication': True})
    lab_models.TestTarget.objects.update_or_create(name='Norhydrocodone',
                                                   defaults={'common_name': [], 'category': 'Opiates/Opioids',
                                                             'is_tox_target': True})
    lab_models.TestTarget.objects.update_or_create(name='Hydromorphone', defaults={'common_name': ['Dilaudid'], 'category': 'Opiates/Opioids',
                                                                      'is_tox_target': True, 'is_medication': True})
    lab_models.TestTarget.objects.update_or_create(name='Oxycodone', defaults={'common_name': ['OXY', 'Percocet'], 'category': 'Opiates/Opioids',
                                                                      'is_tox_target': True, 'is_medication': True})
    lab_models.TestTarget.objects.update_or_create(name='Noroxycodone',
                                                   defaults={'common_name': [], 'category': 'Opiates/Opioids',
                                                             'is_tox_target': True})
    lab_models.TestTarget.objects.update_or_create(name='Oxymorphone', defaults={'common_name': ['Opana'], 'category': 'Opiates/Opioids',
                                                                      'is_tox_target': True, 'is_medication': True})

    lab_models.TestTarget.objects.update_or_create(name='Methadone', defaults={'common_name': ['MTD', 'Dolophine'], 'category': 'Opiates/Opioids',
                                                                      'is_tox_target': True, 'is_medication': True})
    lab_models.TestTarget.objects.update_or_create(name='EDDP', defaults={'common_name': [], 'category': 'Opiates/Opioids',
                                                                      'is_tox_target': True})
    lab_models.TestTarget.objects.update_or_create(name='Meperidine', defaults={'common_name': ['Demerol'], 'category': 'Opiates/Opioids',
                                                                      'is_tox_target': True, 'is_medication': True})
    lab_models.TestTarget.objects.update_or_create(name='Normeperidine', defaults={'common_name': [''], 'category': 'Opiates/Opioids',
                                                                      'is_tox_target': True})
    lab_models.TestTarget.objects.update_or_create(name='Fentanyl', defaults={'common_name': ['Actiq', 'Duragesic'],
                                                                              'category': 'Opiates/Opioids',
                                                                              'is_tox_target': True,
                                                                              'is_medication': True})
    lab_models.TestTarget.objects.update_or_create(name='Norfentanyl', defaults={'common_name': [], 'category': 'Opiates/Opioids',
                                                                      'is_tox_target': True})
    lab_models.TestTarget.objects.update_or_create(name='Tramadol', defaults={'common_name': ['Ultram'], 'category': 'Opiates/Opioids',
                                                                      'is_tox_target': True, 'is_medication': True})
    lab_models.TestTarget.objects.update_or_create(name='O-Desmethyl-cis-Tramadol', defaults={'common_name': [''], 'category': 'Opiates/Opioids',
                                                                      'is_tox_target': True})
    lab_models.TestTarget.objects.update_or_create(name='O-desmethyltramadol',
                                                   defaults={'common_name': [''], 'category': 'Opiates/Opioids',
                                                             'is_tox_target': True})
    lab_models.TestTarget.objects.update_or_create(name='Propoxyphene', defaults={'common_name': ['Darvocet'], 'category': 'Opiates/Opioids',
                                                                      'is_tox_target': True, 'is_medication': True})
    lab_models.TestTarget.objects.update_or_create(name='Norpropoxyphene', defaults={'common_name': [], 'category': 'Opiates/Opioids',
                                                                      'is_tox_target': True})
    lab_models.TestTarget.objects.update_or_create(name='Tapentadol', defaults={'common_name': ['Nucynta'], 'category': 'Opiates/Opioids',
                                                                      'is_tox_target': True, 'is_medication': True})

    # Opiate Agonist Antagonists
    lab_models.TestTarget.objects.update_or_create(name='Suboxone',
                                                   defaults={'common_name': ['Naloxone and Buprenorphine'],
                                                             'category': 'Opiate Agonist Antagonists',
                                                             'is_tox_target': True, 'is_medication': True})
    lab_models.TestTarget.objects.update_or_create(name='Naltrexone', defaults={'common_name': ['Vivitrol'],
                                                                                'category': 'Opiate Agonist Antagonists',
                                                                                'is_tox_target': True,
                                                                                'is_medication': True})
    lab_models.TestTarget.objects.update_or_create(name='Naloxone', defaults={'common_name': [],
                                                                              'category': 'Opiate Agonist Antagonists',
                                                                              'is_tox_target': True,
                                                                              'is_medication': True})
    lab_models.TestTarget.objects.update_or_create(name='Buprenorphine', defaults={'common_name': ['BUP', 'Buprenex'],
                                                                                   'category': 'Opiate Agonist Antagonists',
                                                                                   'is_tox_target': True,
                                                                                   'is_medication': True})
    lab_models.TestTarget.objects.update_or_create(name='Norbuprenorphine',
                                                   defaults={'common_name': [], 'category': 'Opiate Agonist Antagonists',
                                                             'is_tox_target': True})



    # Illicits
    lab_models.TestTarget.objects.update_or_create(name='6-MAM', defaults={'common_name': ['Heroin'], 'category': 'Illicits',
                                                                      'is_tox_target': True})
    lab_models.TestTarget.objects.update_or_create(name='Benzoylecgonine', defaults={'common_name': [], 'category': 'Illicits',
                                                                      'is_tox_target': True})
    lab_models.TestTarget.objects.update_or_create(name='Methamphetamine', defaults={'common_name': ['MET', 'Meth'], 'category': 'Illicits',
                                                                      'is_tox_target': True, 'is_medication': True})
    lab_models.TestTarget.objects.update_or_create(name='MDMA', defaults={'common_name': ['Ecstasy'], 'category': 'Illicits',
                                                                      'is_tox_target': True})
    lab_models.TestTarget.objects.update_or_create(name='Phencyclidine', defaults={'common_name': ['PCP', 'Angel Dust'], 'category': 'Illicits',
                                                                      'is_tox_target': True})
    # x2
    lab_models.TestTarget.objects.update_or_create(name='THC', defaults={'common_name': ['Marijuana', 'Medical Marijuana'], 'category': 'Illicits',
                                                                      'is_tox_target': True, 'is_medication': True})
    lab_models.TestTarget.objects.update_or_create(name='THC-COOH',
                                                   defaults={'common_name': [],
                                                             'category': 'Illicits',
                                                             'is_tox_target': True})
    lab_models.TestTarget.objects.update_or_create(name='JWH-018 5-pentanoic acid', defaults={'common_name': ['K2', 'Spice'], 'category': 'Illicits',
                                                                      'is_tox_target': True})
    lab_models.TestTarget.objects.update_or_create(name='JWH-073 4-butanoic acid',
                                                   defaults={'common_name': ['K2', 'Spice'], 'category': 'Illicits',
                                                             'is_tox_target': True})
    lab_models.TestTarget.objects.update_or_create(name='MDPV', defaults={'common_name': ['Bath Salt'], 'category': 'Illicits',
                                                                      'is_tox_target': True, 'is_medication': True})

    # Amphetamines
    # x2
    lab_models.TestTarget.objects.update_or_create(name='Amphetamine', defaults={'common_name': ['Adderall'],
                                                                                 'category': 'Amphetamines',
                                                                                 'is_tox_target': True,
                                                                                 'is_medication': True})
    lab_models.TestTarget.objects.update_or_create(name='Ritalinic Acid', defaults={'common_name': ['Ritalin'],
                                                                      'category': 'Amphetamines',
                                                                      'is_tox_target': True})
    #  added
    lab_models.TestTarget.objects.update_or_create(name='Methylphenidate', defaults={'common_name': ['Ritalin'],
                                                                                    'category': 'Amphetamines',
                                                                                    'is_tox_target': True, 'is_medication': True})
    lab_models.TestTarget.objects.update_or_create(name='Phentermine', defaults={'common_name': ['Adipex-P'],
                                                                                 'category': 'Amphetamines',
                                                                                 'is_tox_target': True,
                                                                                 'is_medication': True})


    #  Relaxants/Sleep Aids
    lab_models.TestTarget.objects.update_or_create(name='Carisoprodol', defaults={'common_name': ['Soma'],
                                                                      'category': 'Relaxants/Sleep Aids',
                                                                      'is_tox_target': True, 'is_medication': True})
    lab_models.TestTarget.objects.update_or_create(name='Meprobamate', defaults={'common_name': ['Equanil'],
                                                                      'category': 'Relaxants/Sleep Aids',
                                                                      'is_tox_target': True, 'is_medication': True})
    lab_models.TestTarget.objects.update_or_create(name='Zolpidem', defaults={'common_name': ['Ambien'],
                                                                      'category': 'Relaxants/Sleep Aids',
                                                                      'is_tox_target': True, 'is_medication': True})
    lab_models.TestTarget.objects.update_or_create(name='Cyclobenzaprine', defaults={'common_name': ['Flexeril'],
                                                                      'category': 'Relaxants/Sleep Aids',
                                                                      'is_tox_target': True, 'is_medication': True})



    # Benzodiazepines
    lab_models.TestTarget.objects.update_or_create(name='Alprazolam', defaults={'common_name': ['Xanax'],
                                                                      'category': 'Benzodiazepines',
                                                                      'is_tox_target': True, 'is_medication': True})
    lab_models.TestTarget.objects.update_or_create(name='alpha-Hydroxyalprazolam', defaults={'common_name': [''],
                                                                      'category': 'Benzodiazepines',
                                                                      'is_tox_target': True})
    lab_models.TestTarget.objects.update_or_create(name='7-Aminoclonazepam', defaults={'common_name': ['Klonopin'],
                                                                      'category': 'Benzodiazepines',
                                                                      'is_tox_target': True})
    lab_models.TestTarget.objects.update_or_create(name='Diazepam', defaults={'common_name': ['Valium'],
                                                                      'category': 'Benzodiazepines',
                                                                      'is_tox_target': True, 'is_medication': True})
    lab_models.TestTarget.objects.update_or_create(name='Nordiazepam', defaults={'common_name': [],
                                                                      'category': 'Benzodiazepines',
                                                                      'is_tox_target': True})
    lab_models.TestTarget.objects.update_or_create(name='Oxazepam', defaults={'common_name': ['Serax'],
                                                                      'category': 'Benzodiazepines',
                                                                      'is_tox_target': True, 'is_medication': True})
    lab_models.TestTarget.objects.update_or_create(name='Temazepam', defaults={'common_name': ['Restoril'],
                                                                      'category': 'Benzodiazepines',
                                                                      'is_tox_target': True, 'is_medication': True})
    lab_models.TestTarget.objects.update_or_create(name='Lorazepam', defaults={'common_name': ['Ativan'],
                                                                      'category': 'Benzodiazepines',
                                                                      'is_tox_target': True, 'is_medication': True})

    # checked till here
    # Tricyclic Antidepressants
    lab_models.TestTarget.objects.update_or_create(name='Amitriptyline', defaults={'common_name': ['Elavil'],
                                                                      'category': 'Tricyclic Antidepressants',
                                                                      'is_tox_target': True, 'is_medication': True})
    lab_models.TestTarget.objects.update_or_create(name='Nortriptyline', defaults={'common_name': [],
                                                                      'category': 'Tricyclic Antidepressants',
                                                                      'is_tox_target': True, 'is_medication': True})
    lab_models.TestTarget.objects.update_or_create(name='Doxepin', defaults={'common_name': ['Aponal'],
                                                                      'category': 'Tricyclic Antidepressants',
                                                                      'is_tox_target': True, 'is_medication': True})
    lab_models.TestTarget.objects.update_or_create(name='Desmethyldoxepin', defaults={'common_name': [''],
                                                                      'category': 'Tricyclic Antidepressants',
                                                                      'is_tox_target': True})
    lab_models.TestTarget.objects.update_or_create(name='N-Desmethylclomipramine', defaults={'common_name': [''],
                                                                                      'category': 'Tricyclic Antidepressants',
                                                                                      'is_tox_target': True})
    lab_models.TestTarget.objects.update_or_create(name='Imipramine', defaults={'common_name': ['Tofranil'],
                                                                      'category': 'Tricyclic Antidepressants',
                                                                      'is_tox_target': True, 'is_medication': True})
    lab_models.TestTarget.objects.update_or_create(name='Desipramine', defaults={'common_name': [],
                                                                      'category': 'Tricyclic Antidepressants',
                                                                      'is_tox_target': True})
    lab_models.TestTarget.objects.update_or_create(name='Clomipramine', defaults={'common_name': ['Anafranil'],
                                                                      'category': 'Tricyclic Antidepressants',
                                                                      'is_tox_target': True, 'is_medication': True})


    # SSRIs
    lab_models.TestTarget.objects.update_or_create(name='Sertraline', defaults={'common_name': ['Zoloft'],
                                                                      'category': 'SSRIs',
                                                                      'is_tox_target': True, 'is_medication': True})
    lab_models.TestTarget.objects.update_or_create(name='Paroxetine', defaults={'common_name': ['Paxil'],
                                                                      'category': 'SSRIs',
                                                                      'is_tox_target': True, 'is_medication': True})
    lab_models.TestTarget.objects.update_or_create(name='Fluoxetine', defaults={'common_name': ['Prozac'],
                                                                      'category': 'SSRIs',
                                                                      'is_tox_target': True, 'is_medication': True})
    lab_models.TestTarget.objects.update_or_create(name='Duloxetine', defaults={'common_name': ['Cymbalta'],
                                                                      'category': 'SSRIs',
                                                                      'is_tox_target': True, 'is_medication': True})
    lab_models.TestTarget.objects.update_or_create(name='Trazodone', defaults={'common_name': [],
                                                                      'category': 'SSRIs',
                                                                      'is_tox_target': True, 'is_medication': True})


    # GABA Analogues
    lab_models.TestTarget.objects.update_or_create(name='Gabapentin', defaults={'common_name': ['Neurontin'],
                                                                      'category': 'GABA Analogues',
                                                                      'is_tox_target': True, 'is_medication': True})
    lab_models.TestTarget.objects.update_or_create(name='Pregabalin', defaults={'common_name': ['Lyrica'],
                                                                      'category': 'GABA Analogues',
                                                                      'is_tox_target': True, 'is_medication': True})

    # Barbiturates
    lab_models.TestTarget.objects.update_or_create(name='Butalbital', defaults={'common_name': ['Fioricet'],
                                                                      'category': 'Barbiturates',
                                                                      'is_tox_target': True, 'is_medication': True})
    lab_models.TestTarget.objects.update_or_create(name='Phenobarbital', defaults={'common_name': ['Luminal'],
                                                                      'category': 'Barbiturates',
                                                                      'is_tox_target': True, 'is_medication': True})

    # Other Tests
    lab_models.TestTarget.objects.update_or_create(name='Cotinine', defaults={'common_name': ['Tobacco', 'Nicotine'],
                                                                                           'category': 'Other Tests',
                                                                                           'is_tox_target': True})
    lab_models.TestTarget.objects.update_or_create(name='Ethyl Glucoronide', defaults={'common_name': [''],
                                                                      'category': 'Other Tests',
                                                                      'is_tox_target': True})

    # Antipsychotics
    lab_models.TestTarget.objects.update_or_create(name='Quetiapine', defaults={'common_name': ['Seroquel'],
                                                                      'category': 'Antipsychotics',
                                                                      'is_tox_target': True, 'is_medication': True})
    lab_models.TestTarget.objects.update_or_create(name='Haloperidol', defaults={'common_name': ['Haldol'],
                                                                      'category': 'Antipsychotics',
                                                                      'is_tox_target': True, 'is_medication': True})
    lab_models.TestTarget.objects.update_or_create(name='Aripiprazole', defaults={'common_name': ['Abilify'],
                                                                      'category': 'Antipsychotics',
                                                                      'is_tox_target': True, 'is_medication': True})

    # Antihistamine (Benadryl)
    lab_models.TestTarget.objects.update_or_create(name='Diphenhydramine', defaults={'common_name': ['Benadryl'],
                                                                                  'category': 'Antihistamines',
                                                                                  'is_tox_target': True,
                                                                                  'is_medication': True})


def delete_duplicate_drugs():
    lab_models.TestTarget.objects.filter(name='Methamphetamines').delete()
    lab_models.TestTarget.objects.filter(name='Phencyclindine (PCP)').delete()
    lab_models.TestTarget.objects.filter(name='Amphetamines').delete()
    lab_models.TestTarget.objects.filter(name='Ethyl Glucoronide Screen').delete()
    lab_models.TestTarget.objects.filter(name='Cocaine Metabolites').delete()
    lab_models.TestTarget.objects.filter(name='Methylenedioxymethamphetamine (MDMA)').delete()
    lab_models.TestTarget.objects.filter(name='aOH-Alprazolam').delete()
    lab_models.TestTarget.objects.filter(name='Methadone metabolite (EDDP)').delete()



if __name__ == '__main__':
    logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)
    delete_duplicate_drugs()
    main()
