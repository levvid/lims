import datetime
import decimal
import re

from dal import autocomplete, forward
from decimal import Decimal
from django import forms
from django.core import validators
from django.db.utils import ProgrammingError
from django.forms.formsets import BaseFormSet
from django.forms import Select
from django.contrib.postgres.forms import SimpleArrayField

from . import models, functions

from main.settings import ALLOWED_MAX_DIGITS, ALLOWED_DECIMAL_PLACES
from accounts import models as accounts_models
from codes import models as codes_models
from patients import models as patients_models
from lab import models as lab_models
from lab_tenant import models as lab_tenant_models

ACCOUNT_TYPES = (('Existing Account', 'Existing Account'), ('New Account', 'New Account'),)
PROVIDER_TYPES = (('Existing Provider', 'Existing Provider'), ('New Provider', 'New Provider'),
                  ('No Provider', 'No Provider'))
PATIENT_TYPES = (('Existing Patient', 'Existing Patient'), ('New Patient', 'New Patient'),)

PAYER_TYPES = (('Saved Insurance', 'Saved Insurance'),
               ('None', 'None (Self-Pay)'),
               ('New Insurance', 'New Insurance'),)
OTHER_PAYER_TYPES = (('Saved Insurance', 'Saved Insurance'),
                     ('None', 'None'),
                     ('New Insurance', 'New Insurance'),)
WORKERS_COMP_TYPES = (('Saved Workers Comp', 'Saved Workers\' Compensation'),
                      ('None', 'None'),
                      ('New Workers Comp', 'New Workers\' Compensation'))
PATIENT_GUARANTOR_TYPES = (('Saved Guarantor', 'Saved Guarantor'),
                           ('None', 'None'),
                           ('New Guarantor', 'New Guarantor'))

PAYER_CHOICES = (('Commercial', 'Commercial'),
                 ('Medicare', 'Medicare'), ('Medicaid', 'Medicaid'), ('Other', 'Other'))
RELATIONSHIP_CHOICES = (('Self', 'Self'), ('Spouse', 'Spouse'),
                        ('Dependent', 'Dependent'), ('Other', 'Other'))

DRAW_TYPES = (('', '---------'), ('Fingerstick', 'Fingerstick'), ('Venipuncture', 'Venipuncture'))
TEMPERATURE_UNITS = (('', ''), ('°F', '°F'), ('°C', '°C'))
RANGE_TYPES = (('Flag', 'Flag'), ('Panic', 'Panic'), ('Reflex', 'Reflex'))

REPORT_TYPES = (('CSV', 'CSV: Data'), ('Excel', 'Excel: Data + Metrics'))
DATE_FILTER_CHOICES = (('Received', 'Order received datetime'),
                       ('Reported', 'Test reported datetime'))
SEX_CHOICES = (('NB', 'Non-Binary'), ('M', 'Male'), ('F', 'Female'))
GUARANTOR_RELATIONSHIP_CHOICES = (('Parent', 'Parent'), ('Spouse', 'Spouse'),
                                  ('Dependent', 'Dependent'), ('Other', 'Other'))
TEMPERATURE_IS_WITHIN_RANGE_CHOICES = (
    (True, 'Within Range'),
    (False, 'Outside of Range')
)

UNIT_CHOICES = (
    ('', 'None'), ('%', '%'), ('% of Hb', '% of Hb'), ('% of RBC', '% of RBC'), ('arb U/ml', 'arb U/ml'),
    ('cP', 'cP'), ('EU', 'EU'), ('fL', 'fL'), ('fmol/cell', 'fmol/cell'), ('g/dL', 'g/dL'),
    ('g/L', 'g/L'), ('GPLU/ml', 'GPLU/ml'), ('IU/L',
                                             'IU/L'), ('kPa', 'kPa'), ('kU/L', 'kU/L'),
    ('mcU/mL', 'mcU/mL'), ('mEq/L', 'mEq/L'), ('mg/dL', 'mg/dL'), ('mg/L', 'mg/L'),
    ('mg/mL', 'mg/mL'), ('min/mm\u00B3', 'min/mm\u00B3'), ('mIU/L', 'mIU/L'), ('mm/h', 'mm/h'),
    ('mmHg', 'mmHg'), ('mmol/L', 'mmol/L'), ('mOsm/kg', 'mOsm/kg'), ('mOsm/L', 'mOsm/L'),
    ('MPL/ml', 'MPL/ml'), ('mU/mL', 'mU/mL'), ('ng/(mL·h)', 'ng/(mL·h)'), ('ng/dL', 'ng/dL'),
    ('ng/dl per ng/(mL·h)', 'ng/dl per ng/(mL·h)'), ('ng/L', 'ng/L'), ('ng/mL', 'ng/mL'),
    ('nmol/L', 'nmol/L'), ('pg/cell', 'pg/cell'), ('pg/mL', 'pg/mL'), ('pmol/L', 'pmol/L'),
    ('pmol/liter per µg/(L·h)', 'pmol/liter per µg/(L·h)'), ('s', 's'), ('torr', 'torr'),
    ('u/dL', 'u/dL'), ('U/L', 'U/L'), ('U/mL',
                                       'U/mL'), ('x10\u00b9\u00b2/L', 'x10\u00b9\u00b2/L'),
    ('x10\u00b3/mm\u00b3', 'x10\u00b3/mm\u00b3'), ('x10\u2076/L', 'x10\u2076/L'),
    ('x10\u2079/L', 'x10\u2079/L'), ('x1000/µL', 'x1000/µL'), ('µg/dL', 'µg/dL'),
    ('µg/L', 'µg/L'), ('µIU/mL', 'µIU/mL'), ('µkat/L', 'µkat/L'), ('μmol/L', 'μmol/L'))


class Select(Select):
    """
    Custom Select widget that disables options that do not have a value
    """

    def create_option(self, *args, **kwargs):
        option = super().create_option(*args, **kwargs)
        if not option.get('value'):
            option['attrs']['disabled'] = 'disabled'
        return option


class OrderForm(forms.Form):
    account_type = forms.ChoiceField(choices=ACCOUNT_TYPES,
                                     widget=forms.RadioSelect(),
                                     initial=ACCOUNT_TYPES[0])
    account = forms.ModelChoiceField(
        queryset=accounts_models.Account.objects.all(),
        widget=autocomplete.ModelSelect2(
            url='account_autocomplete',
            forward=(forward.Field('provider', 'provider'),)
        ),
        required=False,
        label='Account'
    )
    provider_type = forms.ChoiceField(choices=PROVIDER_TYPES,
                                      widget=forms.RadioSelect(),
                                      initial=PROVIDER_TYPES[0])
    provider_country = forms.ChoiceField(choices=(('US', 'US'),
                                                  ('Other', 'Other')),
                                         widget=forms.RadioSelect(),
                                         initial=('US', 'US'),
                                         label="Provider Country")
    provider = forms.ModelChoiceField(
        queryset=accounts_models.Provider.objects.all(),
        widget=autocomplete.ModelSelect2(
            url='provider_autocomplete',
            forward=(forward.Field('account', 'account'),
                     forward.Field('account_type', 'account_type')),
        ),
        required=False,
        label='Provider'
    )
    patient_type = forms.ChoiceField(choices=PATIENT_TYPES,
                                     widget=forms.RadioSelect(),
                                     initial=PATIENT_TYPES[0])
    patient = forms.ModelChoiceField(
        queryset=patients_models.Patient.objects.all(),
        widget=autocomplete.ModelSelect2(
            url='patient_order_autocomplete',
            forward=(forward.Field('provider', 'provider'),
                     forward.Field('account', 'account'),),
        ),
        required=False,
        label='Patient'
    )

    icd10_codes = forms.ModelMultipleChoiceField(
        label='ICD Codes',
        queryset=codes_models.ICD10CM.objects.all(),
        widget=autocomplete.ModelSelect2Multiple(url='icd10_autocomplete'),
        required=False,
    )

    # new account fields
    account_name = forms.CharField(max_length=255, label='Account Name', required=False)
    account_address1 = forms.CharField(max_length=255, label='Mailing Address', required=False)
    account_address2 = forms.CharField(
        max_length=255, label='Apartment, suite, unit (optional)', required=False)
    account_zip_code = forms.CharField(max_length=128, label='Zip Code', required=False)
    account_phone = forms.CharField(max_length=255, label='Phone', required=False)
    account_fax = forms.CharField(max_length=255, label='Fax', required=False)
    account_email = forms.EmailField(max_length=255, label='Email', required=False,
                                     widget=forms.EmailInput(
                                         attrs={'class': 'form-control'})
                                     )
    account_alternate_id = forms.CharField(max_length=32, label='Alternate ID', required=False)

    # new provider fields
    provider_first_name = forms.CharField(max_length=255, label='First Name', required=False)
    provider_last_name = forms.CharField(max_length=255, label='Last Name', required=False)
    provider_npi = forms.CharField(max_length=255, label='NPI', required=False)
    provider_id_num = forms.CharField(max_length=255, label='Identification Number', required=False)
    provider_suffix = forms.CharField(
        max_length=255, label='Suffix (Jr, II)', required=False)
    provider_title = forms.CharField(
        max_length=255, label='Title (MD, RN)', required=False)
    provider_email = forms.EmailField(max_length=255, label='Email', required=False,
                                      widget=forms.EmailInput(
                                          attrs={'class': 'form-control'})
                                      )

    # new patient fields
    patient_first_name = forms.CharField(max_length=255, label='First Name', required=False)
    patient_last_name = forms.CharField(max_length=255, label='Last Name', required=False)
    patient_middle_initial = forms.CharField(max_length=10, label='Middle Initial', required=False)
    patient_suffix = forms.CharField(max_length=255, label='Suffix (Jr, II)', required=False)
    patient_address1 = forms.CharField(max_length=255, label='Mailing Address', required=False)
    patient_address2 = forms.CharField(
        max_length=255, label='Apartment, suite, unit (optional)', required=False)
    patient_zip_code = forms.CharField(max_length=128, label='Zip Code', required=False)
    patient_phone = forms.CharField(max_length=255, label='Phone', required=False)
    patient_email = forms.EmailField(max_length=255, label='Email', required=False,
                                     widget=forms.EmailInput(
                                         attrs={'class': 'form-control'})
                                     )
    patient_birth_date = forms.DateField(label='Date of Birth', required=False, input_formats=['%m-%d-%Y'])
    patient_sex = forms.CharField(max_length=10, required=False, label='Sex',
                                  widget=Select(
                                      attrs={'class': 'form-control'},
                                      choices=(
                                          ('', 'Select...'),
                                          ('NB', 'Non-binary'),
                                          ('F', 'Female'),
                                          ('M', 'Male')
                                      )
                                  ))
    patient_ethnicity = forms.CharField(max_length=3, required=False, label='Ethnicity',
                                        widget=Select(
                                            attrs={'class': 'form-control'},
                                            choices=(
                                                ('U', 'Unknown'), ('W', 'White'),
                                                ('B', 'Black or African-American'),
                                                ('A', 'American Indian or Alaska Native'),
                                                ('S', 'Asian'), ('P', 'Native Hawaiian or Other Pacific Islander'),
                                                ('H', 'Hispanic or Latino'), ('O', 'Other')
                                            )
                                        ))
    patient_social_security_number = forms.CharField(
        max_length=11, label='Social Security Number', required=False)

    # order fields
    research_consent = forms.BooleanField(label='Consent to research', required=False)
    patient_result_request = forms.BooleanField(label='Patient result request', required=False)

    def __init__(self, *args, **kwargs):
        try:
            self.require_icd10 = kwargs.pop('require_icd10')
        except KeyError:  # when view is not an update view
            self.require_icd10 = False
        super(OrderForm, self).__init__(*args, **kwargs)
        # Use custom label instead of object's __str__ method
        self.fields['icd10_codes'] = forms.ModelMultipleChoiceField(
            label='ICD Codes',
            queryset=codes_models.ICD10CM.objects.all(),
            widget=autocomplete.ModelSelect2Multiple(url='icd10_autocomplete'),
            required=self.require_icd10,
        )

    def clean_account_phone(self):
        # scrub non numeric characters
        account_phone = re.sub("[^0-9]", "", self.cleaned_data['account_phone'])
        return account_phone

    def clean_account_fax(self):
        # scrub non numeric characters
        account_fax = re.sub("[^0-9]", "", self.cleaned_data['account_fax'])
        return account_fax

    def clean_provider_first_name(self):
        provider_first_name = self.cleaned_data['provider_first_name'].title()
        return provider_first_name

    def clean_provider_last_name(self):
        provider_last_name = self.cleaned_data['provider_last_name'].title()
        return provider_last_name

    def clean_provider_npi(self):
        provider_npi = self.cleaned_data['provider_npi']
        if provider_npi:
            matching_npi = accounts_models.Provider.objects.filter(npi=provider_npi).exists()
            if matching_npi:
                self.add_error('provider_npi', forms.ValidationError(
                    'A provider with this npi already exists in the system.'))
            if len(provider_npi) != 10:
                self.add_error('provider_npi', forms.ValidationError(
                    'Provider NPI must be 10 digits'))
            if not provider_npi.isdigit():
                self.add_error('provider_npi', forms.ValidationError(
                    'Provider NPI must be all digits'))
        return provider_npi

    def clean_account_email(self):
        account_email = self.cleaned_data.get('account_email')
        if account_email:
            matching_account_email = lab_tenant_models.User.objects.filter(email=account_email) | \
                                     lab_tenant_models.User.objects.filter(username=account_email)
            matching_account_email = matching_account_email.distinct()

            if matching_account_email:
                self.add_error('account_email', forms.ValidationError(
                    'The email address provided for Account Email already exists in our system.'
                ))

            provider_email = self.cleaned_data.get('provider_email')
            if account_email == provider_email:
                self.add_error('account_email', forms.ValidationError(
                    'Account email must be different from provider email.'
                ))
        return account_email

    def clean_provider_email(self):
        provider_email = self.cleaned_data['provider_email']
        if provider_email:
            matching_username = lab_tenant_models.User.objects.filter(
                username=provider_email).exists()
            if matching_username:
                self.add_error('provider_email', forms.ValidationError(
                    'A user matching the provider\'s email already exists.'))

            account_email = self.cleaned_data.get('account_email')
            if provider_email == account_email:
                self.add_error('provider_email', forms.ValidationError(
                    'Provider email must be different from account email.'
                ))
        return provider_email

    def clean_patient_phone(self):
        # scrub non numeric characters
        patient_phone = re.sub("[^0-9]", "", self.cleaned_data['patient_phone'])
        return patient_phone

    def clean_patient_birth_date(self):
        patient_birth_date = self.cleaned_data.get('patient_birth_date')
        if patient_birth_date:
            if patient_birth_date > datetime.date.today():
                raise forms.ValidationError('A birth date can not be in the future')
        return patient_birth_date

    def clean_patient_email(self):
        patient_email = self.cleaned_data['patient_email']
        if patient_email:
            matching_username = lab_tenant_models.User.objects.filter(
                username=patient_email).exists()
            if matching_username:
                self.add_error('patient_email', forms.ValidationError(
                    'A user matching the patient\'s email already exists.'))
        return patient_email

    def clean_patient_social_security_number(self):
        social_security_number = self.cleaned_data.get('patient_social_security_number')

        if social_security_number:
            social_security_number = social_security_number.replace('_', '').strip()
            if not len(social_security_number) == 11:
                raise forms.ValidationError('Patient Social Security Number must be 9 digits.')
            # check for duplicates on create
            if patients_models.Patient.objects.filter(social_security=social_security_number):
                self.add_error('patient_social_security_number',
                               forms.ValidationError('A patient with this Social Security Number already exists.'))
        # if not given, then return None
        else:
            social_security_number = None

        return social_security_number

    def clean(self):

        if not self.cleaned_data.get('account_type') == 'Existing Account':
            self.cleaned_data['account'] = None

        if not self.cleaned_data.get('provider_type') == 'Existing Provider':
            self.cleaned_data['provider'] = None

        if not self.cleaned_data.get('patient_type') == 'Existing Patient':
            self.cleaned_data['patient'] = None

        # require new account fields if no account selected from drop down
        if not self.cleaned_data.get('account'):
            # required new account fields
            if not self.cleaned_data.get('account_name'):
                self.add_error('account_name', forms.ValidationError('Account name is required.'))
            if not self.cleaned_data.get('account_address1'):
                self.add_error('account_address1', forms.ValidationError(
                    'Account address is required.'))

        # require new provider fields if no provider is selected from drop down
        if self.cleaned_data['provider_type'] == 'New Provider':
            # required new provider fields
            if not self.cleaned_data.get('provider_first_name'):
                self.add_error('provider_first_name', forms.ValidationError(
                    'Provider first name is required.'))
            if not self.cleaned_data.get('provider_last_name'):
                self.add_error('provider_last_name', forms.ValidationError(
                    'Provider last name is required.'))
            if self.cleaned_data.get('provider_country') == 'US':
                if not self.cleaned_data.get('provider_npi'):
                    self.add_error('provider_npi', forms.ValidationError(
                        'Provider NPI is required.'))

        # require a provider if "Existing Provider" is selected from radio button
        if self.cleaned_data.get('provider_type') == 'Existing Provider':
            if not self.cleaned_data.get('provider'):
                self.add_error('provider', forms.ValidationError('Provider is required.'))

        # require new patient fields if no patient is selected from drop down
        if not self.cleaned_data.get('patient'):
            # required new patient fields
            if not self.cleaned_data.get('patient_first_name'):
                self.add_error('patient_first_name', forms.ValidationError(
                    'Patient first name is required.'))
            if not self.cleaned_data.get('patient_last_name'):
                self.add_error('patient_last_name', forms.ValidationError(
                    'Patient last name is required.'))
            if not self.cleaned_data.get('patient_birth_date'):
                self.add_error('patient_birth_date', forms.ValidationError(
                    'Patient birth date is required.'))
            if not self.cleaned_data.get('patient_sex'):
                self.add_error('patient_sex', forms.ValidationError('Patient sex is required.'))


class TestSelectForm(forms.Form):
    test_type = forms.ModelChoiceField(queryset=models.TestType.objects.all(),
                                       required=False)
    checked = forms.BooleanField(required=False)


class TestPanelSelectForm(forms.Form):
    """
    Form to let users select test panels on create order form
    """
    test_panel_type = forms.ModelChoiceField(queryset=models.TestPanelType.objects.all(),
                                             required=False)
    checked = forms.BooleanField(required=False)


class BaseTestPanelSelectFormSet(BaseFormSet):
    def __init__(self, *args, **kwargs):
        super(BaseTestPanelSelectFormSet, self).__init__(*args, **kwargs)
        for form in self.forms:
            form.empty_permitted = True


class BaseSampleFormSet(BaseFormSet):
    def __init__(self, *args, **kwargs):
        super(BaseSampleFormSet, self).__init__(*args, **kwargs)
        for form in self.forms:
            form.empty_permitted = False

    def clean(self):
        """
        Any Formset validation goes here
        :return:
        """
        if any(self.errors):
            return

        collection_dates = []
        sample_types = []
        barcodes = []

        # check to see at least one sample form is
        if not any([x.has_changed() for x in self.forms]):
            raise forms.ValidationError('Please add at least one sample.')

        for form in self.forms:
            if form.cleaned_data:
                sample_type = form.cleaned_data['sample_type']
                collection_date = form.cleaned_data['collection_date']
                barcode = form.cleaned_data['barcode']

                collection_dates.append(collection_date)
                sample_types.append(sample_type)
                barcodes.append(barcode)


class SampleForm(forms.Form):
    sample_type = forms.ModelChoiceField(queryset=lab_models.CLIASampleType.objects.all(),
                                         widget=forms.Select(attrs={'class': 'form-control'}),
                                         label='Sample Type',
                                         required=False)
    draw_type = forms.ChoiceField(required=False,
                                  label='Draw Type',
                                  choices=DRAW_TYPES,
                                  widget=forms.Select(attrs={'class': 'form-control'}))
    collection_time = forms.CharField(required=False, label='Collection Time')
    collection_date = forms.DateField(required=False, label='Collection Date', input_formats=['%m-%d-%Y'],
                                      widget=forms.DateInput(attrs={'placeholder': 'MM-DD-YYYY',
                                                                    'class': 'dateMask form-control'}), )
    barcode = forms.CharField(required=False,
                              label='Barcode',
                              widget=forms.TextInput(
                                  attrs={'class': 'form-control barcode'}))
    collection_temperature = forms.DecimalField(label='Temperature',
                                                required=False,
                                                widget=forms.NumberInput(attrs={'class': 'form-control'}))
    collection_temperature_unit = forms.CharField(max_length=2, required=False, label='°F/°C',
                                                  widget=Select(
                                                      attrs={'class': 'form-control'},
                                                      choices=TEMPERATURE_UNITS
                                                  ))
    collection_temperature_is_within_range = forms.ChoiceField(
        label='Specimen Temperature Within Range? (90°-100°F)',
        choices=TEMPERATURE_IS_WITHIN_RANGE_CHOICES,
        widget=forms.RadioSelect(),
        required=False,
    )
    collection_user = forms.ModelChoiceField(
        queryset=lab_tenant_models.User.objects.filter().all(),
        widget=autocomplete.ModelSelect2(url='collector_autocomplete'),
        required=False,
        label='Collected By',
    )

    def clean_sample_type(self):
        return self.cleaned_data['sample_type']

    def clean_collection_date(self):
        return self.cleaned_data['collection_date']

    def clean_collection_time(self):
        collection_time = self.cleaned_data.get('collection_time')
        # if no collection time is provided, default to 12:00 am string before parsing
        if not collection_time:
            collection_time = '12:00 am'
        try:
            collection_time = datetime.datetime.strptime(collection_time, '%I:%M %p').time()
        except ValueError:
            self.add_error('collection_time', forms.ValidationError(
                'Collection time is invalid. Please try again.'))
        return collection_time

    def clean_collection_temperature(self):
        collection_temperature = self.cleaned_data['collection_temperature']
        if collection_temperature:
            if collection_temperature >= Decimal(1000):
                self.add_error('collection_temperature',
                               forms.ValidationError('Please enter a valid temperature.'))
            else:
                return collection_temperature
        else:
            return collection_temperature

    def clean_barcode(self):
        barcode = self.cleaned_data['barcode']
        if barcode == '':
            return None
        else:
            return self.cleaned_data['barcode']

    def clean(self):
        sample_type = self.cleaned_data['sample_type']
        collection_user = self.cleaned_data.get('collection_user')
        collection_date = self.cleaned_data.get('collection_date')

        barcode = self.cleaned_data['barcode']
        if self.sample_id:
            matching_barcode = models.Sample.objects.exclude(pk=self.sample_id).filter(barcode=barcode).exists()
        else:
            matching_barcode = models.Sample.objects.filter(barcode=barcode).exists()
        if barcode and matching_barcode and barcode != '':
            self.add_error('barcode', forms.ValidationError('Unique barcode is required.'))

        # only check forms in formsets that are not empty
        if self.has_changed():
            if 'collection_date' not in self.cleaned_data:
                self.add_error('collection_date', forms.ValidationError('Enter a valid date.'))
            if not sample_type:
                self.add_error('sample_type', forms.ValidationError('Sample type is required.'))
            if not collection_date:
                self.add_error('collection_date', forms.ValidationError(
                    'Collection date is required.'))
        # if form is not part of a formset, still verify
        else:
            if 'collection_date' not in self.cleaned_data:
                self.add_error('collection_date', forms.ValidationError('Enter a valid date.'))
            if not sample_type:
                self.add_error('sample_type', forms.ValidationError('Sample type is required.'))
            if not collection_date:
                self.add_error('collection_date', forms.ValidationError(
                    'Collection date is required.'))

    def __init__(self, *args, **kwargs):
        try:
            self.sample_id = kwargs.pop('sample_id')
        except KeyError:  # when view is not an update view
            self.sample_id = None
        try:
            self.require_collected_by = kwargs.pop('require_collected_by')
        except KeyError:  # when view is not an update view
            self.require_collected_by = False
        try:
            self.account = kwargs.pop('account')
        except KeyError:
            self.account = None
        super(SampleForm, self).__init__(*args, **kwargs)
        # Use custom label instead of object's __str__ method
        self.fields['sample_type'].label_from_instance = self.label_from_instance
        self.fields['collection_user'] = forms.ModelChoiceField(
            queryset=lab_tenant_models.User.objects.filter(user_type=11).all(),
            widget=autocomplete.ModelSelect2(url='collector_autocomplete',
                                             forward=(forward.Self(),)),
            required=self.require_collected_by,
            label='Collected By',
        )

    @staticmethod
    def label_from_instance(obj):
        return obj.name


class AccessionForm(forms.Form):
    """
    Form for the accession page
    """
    barcode = forms.CharField(required=False,
                              label='Barcode',
                              widget=forms.TextInput(
                                  attrs={'class': 'form-control barcode', 'autofocus': 'autofocus'}))


class PatientStatusForm(forms.Form):
    fasting = forms.BooleanField(initial=False,
                                 label='Fasting',
                                 required=False)
    prescribed_medications = forms.ModelMultipleChoiceField(
        label='Prescribed Medications',
        queryset=lab_models.TestTarget.objects.filter(is_tox_target=True),
        widget=autocomplete.ModelSelect2Multiple(url='patient_prescribed_medications'),
        required=False,
    )
    heart_rate = forms.IntegerField(label='Heart rate (bpm)',
                                    required=False)
    temperature = forms.DecimalField(label='Body Temperature',
                                     required=False,
                                     widget=forms.NumberInput(attrs={'class': 'form-control'}))
    temperature_unit = forms.CharField(max_length=2, required=False, label='°F/°C',
                                       widget=Select(
                                           attrs={'class': 'form-control'},
                                           choices=TEMPERATURE_UNITS,
                                       ),
                                       initial=TEMPERATURE_UNITS[1])
    blood_pressure_systolic = forms.DecimalField(label='Blood Pressure - Systolic (mmHg)',
                                                 required=False,
                                                 widget=forms.NumberInput(
                                                     attrs={'class': 'form-control'})
                                                 )
    blood_pressure_diastolic = forms.DecimalField(label='Blood Pressure - Diastolic (mmHg)',
                                                  required=False,
                                                  widget=forms.NumberInput(
                                                      attrs={'class': 'form-control'})
                                                  )


class PatientPayerForm(forms.Form):
    maintain_patient_payer = forms.BooleanField(
        label='Maintain Patient Information',
        initial=False,
        widget=forms.CheckboxInput(attrs={'data-toggle': 'toggle'}),
        required=False)

    insurance_type = forms.ChoiceField(choices=PAYER_TYPES,
                                       widget=forms.RadioSelect(), )

    # new primary payer fields
    patient_payer_type = forms.ChoiceField(choices=PAYER_CHOICES, required=False, label='Payer Type')
    payer = forms.ModelChoiceField(
        label='Payer',
        queryset=patients_models.Payer.objects.all(),
        widget=autocomplete.ModelSelect2(url='patient_payer_autocomplete'),
        required=False,
    )
    payer_name = forms.CharField(max_length=128, label='Payer Name (If Not Found in Search)', required=False)
    member_id = forms.CharField(max_length=64, label='Member ID/Policy Number', required=False)
    group_number = forms.CharField(max_length=64, label='Group Number', required=False)

    subscriber_last_name = forms.CharField(max_length=64, label='Subscriber Last Name', required=False)
    subscriber_first_name = forms.CharField(max_length=64, label='Subscriber First Name', required=False)
    subscriber_middle_initial = forms.CharField(max_length=1, label='Middle Initial', required=False,
                                                error_messages={'max_length': 'Middle initial must be 1 letter.'})
    subscriber_suffix = forms.CharField(max_length=255, label='Suffix (Jr, III, etc.)', required=False)
    subscriber_sex = forms.ChoiceField(label='Subscriber Sex', choices=SEX_CHOICES, required=False)
    subscriber_birth_date = forms.DateField(label='Subscriber Date of Birth', required=False,
                                            input_formats=['%m-%d-%Y'])
    subscriber_address1 = forms.CharField(max_length=255, label='Subscriber Mailing Address', required=False)
    subscriber_address2 = forms.CharField(max_length=255, label='Apartment, Suite, Unit - Optional', required=False)
    subscriber_zip_code = forms.CharField(max_length=128, label='Zip Code', required=False)
    subscriber_social_security = forms.CharField(max_length=11, label='Subscriber Social Security Number',
                                                 required=False)
    subscriber_relationship = forms.ChoiceField(choices=RELATIONSHIP_CHOICES, required=False,
                                                label='Relationship to Subscriber')

    secondary_insurance_type = forms.ChoiceField(choices=OTHER_PAYER_TYPES,
                                                 widget=forms.RadioSelect())
    # new secondary payer fields
    secondary_patient_payer_type = forms.ChoiceField(choices=PAYER_CHOICES, required=False, label='Payer Type')
    secondary_payer = forms.ModelChoiceField(
        label='Secondary Payer',
        queryset=patients_models.Payer.objects.all(),
        widget=autocomplete.ModelSelect2(url='patient_payer_autocomplete'),
        required=False,
    )
    secondary_payer_name = forms.CharField(max_length=128, label='Payer Name (If Not Found in Search)', required=False)
    secondary_member_id = forms.CharField(max_length=64, label='Member ID/Policy Number', required=False)
    secondary_group_number = forms.CharField(max_length=64, label='Group Number', required=False)

    secondary_subscriber_last_name = forms.CharField(max_length=64, label='Subscriber Last Name', required=False)
    secondary_subscriber_first_name = forms.CharField(max_length=64, label='Subscriber First Name', required=False)
    secondary_subscriber_middle_initial = forms.CharField(max_length=1, label='Middle Initial', required=False,
                                                          error_messages={
                                                              'max_length': 'Middle initial must be 1 letter.'})
    secondary_subscriber_suffix = forms.CharField(max_length=255, label='Suffix (Jr, III, etc.)', required=False)
    secondary_subscriber_sex = forms.ChoiceField(label='Subscriber Sex', choices=SEX_CHOICES, required=False)
    secondary_subscriber_birth_date = forms.DateField(label='Subscriber Date of Birth', required=False,
                                                      input_formats=['%m-%d-%Y'])
    secondary_subscriber_address1 = forms.CharField(max_length=255, label='Subscriber Mailing Address', required=False)
    secondary_subscriber_address2 = forms.CharField(max_length=255, label='Apartment, Suite, Unit - Optional',
                                                    required=False)
    secondary_subscriber_zip_code = forms.CharField(max_length=128, label='Zip Code', required=False)
    secondary_subscriber_social_security = forms.CharField(max_length=11, label='Subscriber Social Security Number',
                                                           required=False)

    secondary_subscriber_relationship = forms.ChoiceField(choices=RELATIONSHIP_CHOICES, required=False,
                                                          label='Relationship to Subscriber')

    tertiary_insurance_type = forms.ChoiceField(choices=OTHER_PAYER_TYPES,
                                                widget=forms.RadioSelect())

    # new tertiary payer fields
    tertiary_patient_payer_type = forms.ChoiceField(choices=PAYER_CHOICES, required=False, label='Payer Type')
    tertiary_payer = forms.ModelChoiceField(
        label='Tertiary Payer',
        queryset=patients_models.Payer.objects.all(),
        widget=autocomplete.ModelSelect2(url='patient_payer_autocomplete'),
        required=False,
    )
    tertiary_payer_name = forms.CharField(max_length=128, label='Payer Name (If Not Found in Search)', required=False)
    tertiary_member_id = forms.CharField(max_length=64, label='Member ID/Policy Number', required=False)
    tertiary_group_number = forms.CharField(max_length=64, label='Group Number', required=False)

    tertiary_subscriber_last_name = forms.CharField(max_length=64, label='Subscriber Last Name', required=False)
    tertiary_subscriber_first_name = forms.CharField(max_length=64, label='Subscriber First Name', required=False)
    tertiary_subscriber_middle_initial = forms.CharField(max_length=1, label='Middle Initial', required=False,
                                                         error_messages={
                                                             'max_length': 'Middle initial must be 1 letter.'})
    tertiary_subscriber_suffix = forms.CharField(max_length=255, label='Suffix (Jr, III, etc.)', required=False)
    tertiary_subscriber_sex = forms.ChoiceField(label='Subscriber Sex', choices=SEX_CHOICES, required=False)
    tertiary_subscriber_birth_date = forms.DateField(label='Subscriber Date of Birth', required=False,
                                                     input_formats=['%m-%d-%Y'])
    tertiary_subscriber_address1 = forms.CharField(max_length=255, label='Subscriber Mailing Address', required=False)
    tertiary_subscriber_address2 = forms.CharField(max_length=255, label='Apartment, Suite, Unit - Optional',
                                                   required=False)
    tertiary_subscriber_zip_code = forms.CharField(max_length=128, label='Zip Code', required=False)
    tertiary_subscriber_social_security = forms.CharField(max_length=11, label='Subscriber Social Security Number',
                                                          required=False)

    tertiary_subscriber_relationship = forms.ChoiceField(choices=RELATIONSHIP_CHOICES, required=False,
                                                         label='Relationship to Subscriber')

    # new workers comp fields
    workers_comp_type = forms.ChoiceField(choices=WORKERS_COMP_TYPES,
                                          widget=forms.RadioSelect())
    workers_comp_payer = forms.ModelChoiceField(
        label='Workers\' Compensation ',
        queryset=patients_models.Payer.objects.all(),
        widget=autocomplete.ModelSelect2(url='patient_payer_autocomplete'),
        required=False,
    )
    workers_comp_payer_name = forms.CharField(max_length=128,
                                              label='Workers\'s Compensation Name (If Not Found in Search)',
                                              required=False)
    workers_comp_claim_id = forms.CharField(max_length=64, label='Claim ID', required=False)
    workers_comp_adjuster = forms.CharField(max_length=128, label='Claim Manager/Adjuster', required=False)
    workers_comp_injury_date = forms.DateField(label='Injury Date', required=False, input_formats=['%m-%d-%Y'])

    patient_guarantor_type = forms.ChoiceField(choices=PATIENT_GUARANTOR_TYPES,
                                               widget=forms.RadioSelect(),
                                               initial=PATIENT_GUARANTOR_TYPES[0])

    # new guarantor fields (guarantor can be the patient, or "self")
    first_name = forms.CharField(max_length=255, label='First Name', required=False,
                                 error_messages={'required': 'Patient first name is required.'})
    middle_initial = forms.CharField(max_length=1, label='Middle Initial', required=False,
                                     error_messages={'max_length': 'Middle initial must be 1 letter.'})
    last_name = forms.CharField(max_length=255, label='Last Name', required=False,
                                error_messages={'required': 'Patient last name is required.'})
    suffix = forms.CharField(max_length=255, label='Suffix', required=False)

    sex = forms.ChoiceField(choices=SEX_CHOICES, required=False)

    birth_date = forms.DateField(label='Date of Birth', required=False, input_formats=['%m-%d-%Y'])

    address1 = forms.CharField(max_length=255, label='Mailing Address', required=False)
    address2 = forms.CharField(max_length=255, label='Apartment, Suite, Unit - Optional', required=False)
    zip_code = forms.CharField(max_length=128, label='Zip Code', required=False)

    email = forms.EmailField(max_length=255, label='Email', required=False, widget=forms.EmailInput(
        attrs={'class': 'form-control'}))

    phone_number = forms.CharField(max_length=255, label='Phone', required=False)

    social_security_number = forms.CharField(max_length=11, label='Social Security Number', required=False)

    relationship_to_patient = forms.ChoiceField(choices=GUARANTOR_RELATIONSHIP_CHOICES, required=False,
                                                label='Relationship to patient')

    def clean_subscriber_social_security(self):
        # not a unique identifier (unlike other SSN checks)
        social_security = self.cleaned_data.get('subscriber_social_security')

        if social_security:
            social_security = social_security.replace('_', '').strip()
            if not len(social_security) == 11:
                raise forms.ValidationError('Social Security Number must be 9 digits.')

    def clean_subscriber_birth_date(self):
        birth_date = self.cleaned_data.get('subscriber_birth_date')
        if birth_date:
            if birth_date > datetime.date.today():
                raise forms.ValidationError('Birth date cannot be in the future.')
        return birth_date

    def clean_secondary_subscriber_social_security(self):
        # not a unique identifier (unlike other SSN checks)
        social_security = self.cleaned_data.get('secondary_subscriber_social_security')

        if social_security:
            social_security_number = social_security.replace('_', '').strip()
            if not len(social_security_number) == 11:
                raise forms.ValidationError('Social Security Number must be 9 digits.')

    def clean_secondary_subscriber_birth_date(self):
        birth_date = self.cleaned_data.get('secondary_subscriber_birth_date')
        if birth_date:
            if birth_date > datetime.date.today():
                raise forms.ValidationError('Birth date cannot be in the future.')
        return birth_date

    def clean_tertiary_subscriber_social_security(self):
        # not a unique identifier (unlike other SSN checks)
        social_security = self.cleaned_data.get('tertiary_subscriber_social_security')

        if social_security:
            social_security = social_security.replace('_', '').strip()
            if not len(social_security) == 11:
                raise forms.ValidationError('Social Security Number must be 9 digits.')

    def clean_tertiary_subscriber_birth_date(self):
        birth_date = self.cleaned_data.get('tertiary_subscriber_birth_date')
        if birth_date:
            if birth_date > datetime.date.today():
                raise forms.ValidationError('Birth date cannot be in the future.')
        return birth_date

    def clean_insurance_type(self):
        payer_type = self.cleaned_data.get('insurance_type')
        if payer_type == 'None':
            return None
        else:
            return payer_type

    def clean_secondary_insurance_type(self):
        payer_type = self.cleaned_data.get('secondary_insurance_type')
        if payer_type == 'None':
            return None
        else:
            return payer_type

    def clean_tertiary_insurance_type(self):
        payer_type = self.cleaned_data.get('tertiary_insurance_type')
        if payer_type == 'None':
            return None
        else:
            return payer_type

    def clean_workers_comp_type(self):
        payer_type = self.cleaned_data.get('workers_comp_type')
        if payer_type == 'None':
            return None
        else:
            return payer_type

    def clean_patient_guarantor_type(self):
        patient_guarantor_type = self.cleaned_data.get('patient_guarantor_type')
        if patient_guarantor_type == 'None':
            return None
        else:
            return patient_guarantor_type

    def clean_member_id(self):
        # no dashes or spaces, only alphanumeric characters
        member_id = self.cleaned_data.get('member_id')
        if member_id:
            member_id = re.compile('[\W_]+').sub('', member_id)
        return member_id

    def clean_secondary_member_id(self):
        # no dashes or spaces, only alphanumeric characters
        member_id = self.cleaned_data.get('secondary_member_id')
        if member_id:
            member_id = re.compile('[\W_]+').sub('', member_id)
        return member_id

    def clean_tertiary_member_id(self):
        # no dashes or spaces, only alphanumeric characters
        member_id = self.cleaned_data.get('tertiary_member_id')
        if member_id:
            member_id = re.compile('[\W_]+').sub('', member_id)
        return member_id

    def clean(self):
        if self.cleaned_data.get('insurance_type') == 'New Insurance':
            if not self.cleaned_data.get('payer') and not self.cleaned_data.get('payer_name'):
                self.add_error('payer', forms.ValidationError(
                    'Choose a payer from the dropdown, or fill in the Payer Name field.'))
            if not self.cleaned_data.get('member_id'):
                self.add_error('member_id', forms.ValidationError(
                    'Member ID is missing.'))

            if self.cleaned_data.get('subscriber_relationship') != 'Self':
                if not self.cleaned_data.get('subscriber_first_name'):
                    self.add_error('subscriber_first_name', forms.ValidationError(
                        'Subscriber first name is missing.'))
                if not self.cleaned_data.get('subscriber_last_name'):
                    self.add_error('subscriber_last_name', forms.ValidationError(
                        'Subscriber last name is missing.'))

        ################################################################################################################
        if self.cleaned_data.get('secondary_insurance_type') == 'New Insurance':
            # if self-pay, then no other fields are required.
            if self.cleaned_data.get('secondary_patient_payer_type') != 'Self-Pay':
                if not (self.cleaned_data.get('secondary_payer') or self.cleaned_data.get('secondary_payer_name')):
                    self.add_error('secondary_payer', forms.ValidationError(
                        'Choose a secondary payer from the dropdown, or fill in the Payer Name field.'))
                if not self.cleaned_data.get('secondary_member_id'):
                    self.add_error('secondary_member_id', forms.ValidationError(
                        'Secondary member ID is missing.'))

                if self.cleaned_data.get('secondary_subscriber_relationship') != 'Self':
                    if not self.cleaned_data.get('secondary_subscriber_first_name'):
                        self.add_error('secondary_subscriber_first_name', forms.ValidationError(
                            'Secondary subscriber first name is missing.'))
                    if not self.cleaned_data.get('secondary_subscriber_last_name'):
                        self.add_error('secondary_subscriber_last_name', forms.ValidationError(
                            'Secondary subscriber last name is missing.'))
        ################################################################################################################
        if self.cleaned_data.get('tertiary_insurance_type') == 'New Insurance':
            # if self-pay, then no other fields are required.
            if self.cleaned_data.get('tertiary_patient_payer_type') != 'Self-Pay':
                if not (self.cleaned_data.get('tertiary_payer') or self.cleaned_data.get('tertiary_payer_name')):
                    self.add_error('tertiary_payer', forms.ValidationError(
                        'Choose a tertiary payer from the dropdown, or fill in the Payer Name field.'))
                if not self.cleaned_data.get('tertiary_member_id'):
                    self.add_error('tertiary_member_id', forms.ValidationError(
                        'Tertiary member ID is missing.'))

                if self.cleaned_data.get('tertiary_subscriber_relationship') != 'Self':
                    if not self.cleaned_data.get('tertiary_subscriber_first_name'):
                        self.add_error('tertiary_subscriber_first_name', forms.ValidationError(
                            'Tertiary subscriber first name is missing.'))
                    if not self.cleaned_data.get('tertiary_subscriber_last_name'):
                        self.add_error('tertiary_subscriber_last_name', forms.ValidationError(
                            'Tertiary subscriber last name is missing.'))
        ################################################################################################################
        # 'None' is string, not boolean
        if self.cleaned_data.get('workers_comp_type') == 'New WC':
            if not (self.cleaned_data.get('workers_comp_payer') or self.cleaned_data.get('workers_comp_payer_name')):
                self.add_error('workers_comp_payer', forms.ValidationError(
                    'Choose a Workers\' Compensation from the dropdown, or fill in the Name field.'))
        ################################################################################################################
        # 'None' is string, not boolean
        if self.cleaned_data.get('patient_guarantor_type') == 'New Guarantor':
            if not self.cleaned_data.get('first_name'):
                self.add_error('first_name', forms.ValidationError(
                    'Patient guarantor first name is required.'))
            if not self.cleaned_data.get('last_name'):
                self.add_error('last_name', forms.ValidationError(
                    'Patient guarantor last name is required.'))
            if not self.cleaned_data.get('birth_date'):
                self.add_error('last_name', forms.ValidationError(
                    'Patient guarantor birth date is required.'))

    def __init__(self, *args, **kwargs):
        # this allows you to customize the required error message for all fields
        super(PatientPayerForm, self).__init__(*args, **kwargs)
        for field in self.fields.values():
            field.error_messages = {'required': '{fieldname} is required.'.format(fieldname=field.label),
                                    'invalid': '{fieldname} is invalid.'.format(fieldname=field.label)}


class ControlForm(forms.Form):
    name = forms.CharField(label='Name', required=True, max_length=128,
                           widget=forms.TextInput(attrs={'class': 'form-control'}))
    code = forms.CharField(label='Code', required=True, max_length=128,
                           widget=forms.TextInput(attrs={'class': 'form-control'}))
    instrument_integrations = forms.ModelMultipleChoiceField(
        label='Associated Instruments',
        queryset=lab_tenant_models.InstrumentIntegration.objects.all(),
        widget=autocomplete.ModelSelect2Multiple(url='instrument_integration_autocomplete'),
        required=False
    )
    lot_number = forms.CharField(
        label='Lot Number', required=True, max_length=128,
        widget=forms.TextInput(attrs={'class': 'form-control'})
    )
    expiration_date = forms.DateField(
        label='Expiration Date',
        required=True,
        input_formats=['%m-%d-%Y'],
        widget=forms.DateInput(attrs={'class': 'form-control dateMask'})
    )


class ControlKnownValueForm(forms.Form):
    concentration = forms.DecimalField(
        label='Value (Vendor Specified)',
        required=False,
        widget=forms.NumberInput(
            attrs={'class': 'form-control'})
    )
    unit = forms.ChoiceField(
        choices=UNIT_CHOICES,
        required=False,
        widget=forms.Select(attrs={'class': 'form-control'}),
        label='Unit of Measurement'
    )
    test_target = forms.ModelChoiceField(
        label='Analyte or Target',
        queryset=lab_models.TestTarget.objects.all(),
        widget=autocomplete.ModelSelect2(url='test_target_autocomplete'),
        required=True,
    )

    def __init__(self, *args, control, **kwargs):
        self.control = control
        super().__init__(*args, **kwargs)

    def clean_concentration(self):
        concentration = self.cleaned_data.get('concentration')
        # truncate last decimals
        concentration = decimal.Decimal(concentration).quantize(
            decimal.Decimal('.00000001'), rounding=decimal.ROUND_DOWN)
        if concentration >= decimal.Decimal('99999999.99999999'):
            self.add_error('concentration', 'Control value is too large.')
        return concentration

    def clean_test_target(self):
        test_target = self.cleaned_data['test_target']
        matching_known_value = self.control.known_values.filter(test_target=test_target).exists()

        if matching_known_value:
            self.add_error(
                'test_target',
                forms.ValidationError('This control already has a known value for {}.'.format(test_target.name))
            )
        return test_target


class ControlObservationForm(forms.Form):
    control = forms.ModelChoiceField(
        label='hidden',
        queryset=models.Control.objects.all(),
        widget=autocomplete.ModelSelect2(url='control_autocomplete'),
        required=True
    )
    test_target = forms.ModelChoiceField(
        label='Analyte or Target',
        queryset=lab_models.TestTarget.objects.all(),
        widget=autocomplete.ModelSelect2(
            url='test_target_autocomplete',
            forward=(forward.Field('control', 'control'),),
        ),
        required=True
    )
    instrument_integration = forms.ModelChoiceField(
        queryset=lab_tenant_models.InstrumentIntegration.objects.all(),
        widget=autocomplete.ModelSelect2(
            url='instrument_integration_autocomplete',
            forward=(forward.Field('control', 'control'),),
        ),
        required=True,
        label='Instrument'
    )
    result_quantitative = forms.CharField(
        max_length=255,
        label='Measurement',
        required=False,
        widget=forms.NumberInput(
            attrs={'class': 'form-control'})
    )


class TestTypeRangeForm(forms.Form):
    range_low = forms.DecimalField(label='Low', required=False,
                                   widget=forms.NumberInput(attrs={'class': 'form-control'}))
    range_high = forms.DecimalField(label='High (Cutoff)', required=False,
                                    widget=forms.NumberInput(attrs={'class': 'form-control'}))
    critical_low = forms.DecimalField(label='Critical Low', required=False,
                                      widget=forms.NumberInput(attrs={'class': 'form-control'}))
    critical_high = forms.DecimalField(label='Critical High', required=False,
                                       widget=forms.NumberInput(attrs={'class': 'form-control'}))
    report_comment = forms.CharField(label='Comment', required=False, max_length=500,
                                     widget=forms.TextInput(attrs={'class': 'form-control'}))
    critical_report_comment = forms.CharField(label='Critical Result Comment', required=False, max_length=500,
                                              widget=forms.TextInput(attrs={'class': 'form-control'}))
    # if sex isn't chosen, then assumed to be non-binary
    sex = forms.CharField(max_length=10, required=False, label='Sex',
                          widget=Select(
                              attrs={'class': 'form-control'},
                              choices=(
                                  ('NB', 'Select...'),
                                  ('F', 'Female'),
                                  ('M', 'Male')
                              )
                          ))

    accounts = forms.ModelMultipleChoiceField(
        label='Use rule for specific account(s) only.',
        queryset=accounts_models.Account.objects.all(),
        widget=autocomplete.ModelSelect2Multiple(url='account_autocomplete'),
        required=False,
    )

    # new date ranges
    # min age
    min_age_days = forms.IntegerField(label='Days', required=False, min_value=0, initial=0,
                                      widget=forms.NumberInput(attrs={'class': 'form-control'}))
    min_age_weeks = forms.IntegerField(label='Weeks', required=False, min_value=0, initial=0,
                                       widget=forms.NumberInput(attrs={'class': 'form-control'}))
    min_age_months = forms.IntegerField(label='Months', required=False, min_value=0, initial=0,
                                        widget=forms.NumberInput(attrs={'class': 'form-control'}))
    min_age_years = forms.IntegerField(label='Years', required=False, min_value=0, initial=0,
                                       widget=forms.NumberInput(attrs={'class': 'form-control'}))

    # max ages
    max_age_days = forms.IntegerField(label='Days', required=False, min_value=0, initial=0,
                                      widget=forms.NumberInput(attrs={'class': 'form-control'}))
    max_age_weeks = forms.IntegerField(label='Weeks', required=False, min_value=0, initial=0,
                                       widget=forms.NumberInput(attrs={'class': 'form-control'}))
    max_age_months = forms.IntegerField(label='Months', required=False, min_value=0, initial=0,
                                        widget=forms.NumberInput(attrs={'class': 'form-control'}))
    max_age_years = forms.IntegerField(label='Years', required=False, min_value=0, initial=0,
                                       widget=forms.NumberInput(attrs={'class': 'form-control'}))

    def clean_sex(self):
        sex = self.cleaned_data.get('sex')
        if not sex:
            sex = 'NB'
        return sex

    def clean_min_age_days(self):
        min_age_days = self.cleaned_data.get('min_age_days')
        if not min_age_days:
            min_age_days = 0
        else:
            if min_age_days < 0:
                self.add_error('min_age_days', forms.ValidationError(
                    'Please ensure that the minimum age number of days is at least 0.'))
        return min_age_days

    def clean_min_age_weeks(self):
        min_age_weeks = self.cleaned_data.get('min_age_weeks')
        if not min_age_weeks:
            min_age_weeks = 0
        else:
            if min_age_weeks < 0:
                self.add_error('min_age_weeks', forms.ValidationError(
                    'Please ensure that the minimum age number of weeks is at least 0.'))
        return min_age_weeks

    def clean_min_age_months(self):
        min_age_months = self.cleaned_data.get('min_age_months')
        if not min_age_months:
            min_age_months = 0
        else:
            if min_age_months < 0:
                self.add_error('min_age_months', forms.ValidationError(
                    'Please ensure that the minimum age number of months is at least 0.'))
        return min_age_months

    def clean_min_age_years(self):
        min_age_years = self.cleaned_data.get('min_age_years')
        if not min_age_years:
            min_age_years = 0
        else:
            if min_age_years < 0:
                self.add_error('min_age_years', forms.ValidationError(
                    'Please ensure that the minimum age number of years is at least 0.'))
        return min_age_years

    def clean_max_age_days(self):
        max_age_days = self.cleaned_data.get('max_age_days')
        if not max_age_days:
            max_age_days = 0
        else:
            if max_age_days < 0:
                self.add_error('max_age_days', forms.ValidationError(
                    'Please ensure that the maximum age number of days is at least 0.'))
        return max_age_days

    def clean_max_age_weeks(self):
        max_age_weeks = self.cleaned_data.get('max_age_weeks')
        if not max_age_weeks:
            max_age_weeks = 0
        else:
            if max_age_weeks < 0:
                self.add_error('max_age_weeks', forms.ValidationError(
                    'Please ensure that the maximum age number of weeks is at least 0.'))
        return max_age_weeks

    def clean_max_age_months(self):
        max_age_months = self.cleaned_data.get('max_age_months')
        if not max_age_months:
            max_age_months = 0
        else:
            if max_age_months < 0:
                self.add_error('max_age_months', forms.ValidationError(
                    'Please ensure that the maximum age number of months is at least 0.'))
        return max_age_months

    def clean_max_age_years(self):
        max_age_years = self.cleaned_data.get('max_age_years')
        if not max_age_years:
            max_age_years = 0
        else:
            if max_age_years < 0:
                self.add_error('max_age_years', forms.ValidationError(
                    'Please ensure that the maximum age number of years is at least 0.'))
        return max_age_years

    def clean_report_comment(self):
        report_comment = self.cleaned_data.get('report_comment')
        if len(report_comment) >= 500:
            self.add_error('report_comment', forms.ValidationError(
                'The comment must be under 500 characters.'))
        return report_comment

    def clean_critical_report_comment(self):
        critical_report_comment = self.cleaned_data.get('critical_report_comment')
        if len(critical_report_comment) >= 500:
            self.add_error('critical_report_comment', forms.ValidationError('The critical result comment must be'
                                                                            'under 500 characters.'))
        return critical_report_comment

    def clean_range_low(self):
        range_low = self.cleaned_data.get('range_low')
        # truncate last decimals
        if range_low:
            range_low = decimal.Decimal(range_low).quantize(
                decimal.Decimal('.00000001'), rounding=decimal.ROUND_DOWN)
            if range_low >= decimal.Decimal('99999999.99999999'):
                self.add_error('range_low', 'Reference range value (low) is too large.')
        return range_low

    def clean_range_high(self):
        range_high = self.cleaned_data.get('range_high')
        # truncate last decimals
        if range_high:
            range_high = decimal.Decimal(range_high).quantize(
                decimal.Decimal('.00000001'), rounding=decimal.ROUND_DOWN)
            if range_high >= decimal.Decimal('99999999.99999999'):
                self.add_error('range_low', 'Reference range value (high) is too large.')
        return range_high

    def clean_critical_low(self):
        critical_low = self.cleaned_data.get('critical_low')
        # truncate last decimals
        if critical_low:
            critical_low = decimal.Decimal(critical_low).quantize(
                decimal.Decimal('.00000001'), rounding=decimal.ROUND_DOWN)
            if critical_low >= decimal.Decimal('99999999.99999999'):
                self.add_error('critical_low', 'Critical reference range value (low) is too large.')
        return critical_low

    def clean_critical_high(self):
        critical_high = self.cleaned_data.get('critical_high')
        # truncate last decimals
        if critical_high:
            critical_high = decimal.Decimal(critical_high).quantize(
                decimal.Decimal('.00000001'), rounding=decimal.ROUND_DOWN)
            if critical_high >= decimal.Decimal('99999999.99999999'):
                self.add_error('range_low', 'Critical reference range value (high) is too high.')
        return critical_high

    def clean_accounts(self):
        accounts = self.cleaned_data.get('accounts')
        if not accounts:
            accounts = [None]
        return accounts

    def clean(self):
        range_low = self.cleaned_data.get('range_low')
        critical_low = self.cleaned_data.get('critical_low')
        range_high = self.cleaned_data.get('range_high')
        critical_high = self.cleaned_data.get('critical_high')

        # WARNING:
        # be careful here because 0 'falsey' but is a legitimate value! that's why we use 'is not None' instead of 'not'
        # critical ranges must be greater in magnitude than normal ranges
        if range_low is not None and critical_low is not None:
            if range_low < critical_low:
                self.add_error('range_low', forms.ValidationError('Critical low value must be lower than the '
                                                                  'normal low value.'))
        if range_high is not None and critical_high is not None:
            if range_high > critical_high:
                self.add_error('range_high', forms.ValidationError('Critical high value must be higher than the '
                                                                   'normal high value.'))

        if range_high is None:
            self.add_error('range_high', forms.ValidationError('Please enter the high range.'))
        if range_low is not None and range_high is not None:
            if range_low >= range_high:
                self.add_error('range_low', forms.ValidationError('The low value must be lower than the '
                                                                  'high value.'))

        # Only one rule can be default and have sex & min/max age not specified
        sex = self.cleaned_data['sex']
        accounts = self.cleaned_data.get('accounts')
        data_with_total_age = functions.test_type_range_form_data(self)
        min_age_total = data_with_total_age['min_age_total']
        max_age_total = data_with_total_age['max_age_total']
        if (sex, min_age_total, max_age_total) == ('NB', 0, 0):
            has_default = models.TestTypeRange.objects.filter(test_type=self.test_type,
                                                              default=True)
            if self.instance:
                has_default = has_default.exclude(id=self.instance.id)
            has_default = has_default.exists()
            if has_default:
                self.add_error('sex', forms.ValidationError('There can only be one rule where sex and min/max age are'
                                                            'not specified.'))
        # min total age must be lower than max total age
        if min_age_total != 0:
            if min_age_total >= max_age_total:
                self.add_error('min_age_years', forms.ValidationError('Minimum age must be less than maximum age.'))

        # sex, min/max age must be unique
        overlap_exists = False
        if max_age_total:
            for account in accounts:
                same_sex_ranges = models.TestTypeRange.objects.filter(test_type=self.test_type,
                                                                      sex=sex,
                                                                      accounts=account).prefetch_related('accounts')
                if self.instance:
                    same_sex_ranges = same_sex_ranges.exclude(id=self.instance.id)

                for reference_range in same_sex_ranges:
                    if reference_range.max_age_total != 0 and \
                            (reference_range.min_age_total <= min_age_total < reference_range.max_age_total or
                             min_age_total <= reference_range.min_age_total < max_age_total):
                        overlap_exists = True
            if overlap_exists:
                self.add_error(
                    'min_age_years',
                    "Min/max age conflicts with another reference range with the same sex or account."
                )

        if accounts == [None]:
            self.cleaned_data['accounts'] = None

    def __init__(self, *args, instance, test_type, **kwargs):
        self.instance = instance
        self.test_type = test_type
        super().__init__(*args, **kwargs)


class TestTypeReflexForm(forms.Form):
    reflex_test_type = forms.ModelChoiceField(
        label='Reflex Test',
        queryset=models.TestType.objects.all(),
        widget=autocomplete.ModelSelect2(url='test_type_autocomplete',
                                         forward=(forward.Self(),)),
        required=True)

    # what happens if a test type is added where the sample cannot be reflexed?
    # for example, a blood or semen sample cannot reflex to a TSH test

    # conditionals
    reflex_criteria_quantitative = forms.DecimalField(widget=forms.NumberInput(attrs={'class': 'form-control'}),
                                                      required=False)
    reflex_criteria_qualitative = forms.ChoiceField(choices=(('', '-------'),
                                                             ('-', 'Negative'),
                                                             ('+', 'Positive'),
                                                             ('=', 'Equivocal'),
                                                             ('?', 'Indeterminate'),
                                                             ('.', 'Pending')), required=False)

    quantitative_comparison_operator = forms.ChoiceField(choices=(('=', 'equal to'),
                                                                  ('>=', 'greater than or equal to'),
                                                                  ('>', 'greater than'),
                                                                  ('<=', 'less than or equal to'),
                                                                  ('<', 'less than')), required=False)

    notes = forms.CharField(widget=forms.Textarea(attrs={'class': 'form-control'}),
                            required=False)

    def __init__(self, *args, test_method, result_type, **kwargs):
        self.test_method = test_method
        self.result_type = result_type
        super().__init__(*args, **kwargs)
        positive_label, negative_label, equivocal_label = get_test_method_labels(
            self.test_method.name)
        if positive_label and negative_label and equivocal_label:
            self.fields['reflex_criteria_qualitative'] = forms.ChoiceField(choices=(('', '-------'),
                                                                                    ('-', negative_label),
                                                                                    ('+', positive_label),
                                                                                    ('=', equivocal_label),
                                                                                    ('?', 'Indeterminate'),
                                                                                    ('.', 'Pending')), required=False)

    def clean_reflex_criteria_quantitative(self):
        reflex_criteria_quantitative = self.cleaned_data.get('reflex_criteria_quantitative')
        # truncate last decimals
        reflex_criteria_quantitative = decimal.Decimal(reflex_criteria_quantitative).quantize(
            decimal.Decimal('.00000001'), rounding=decimal.ROUND_DOWN)
        if reflex_criteria_quantitative >= decimal.Decimal('99999999.99999999'):
            self.add_error('reflex_criteria_quantitative', 'Reflex criteria value is too large.')
        return reflex_criteria_quantitative


class TestTypeForm(forms.Form):
    """
    Form for creating/updating Proprietary Test Types
    """
    name = forms.CharField(max_length=255, label='Test Name', required=True,
                           error_messages={'required': 'Name is required.'})
    aliases = forms.CharField(label='Aliases (separate multiple with a new line)',
                              required=False,
                              widget=forms.Textarea(attrs={'class': 'form-control'}))
    manufacturer = forms.CharField(max_length=255, label='Manufacturer', required=False,
                                   widget=forms.TextInput(
                                       attrs={'class': 'form-control'}
                                   ))
    result_type = forms.ChoiceField(choices=(('Quantitative', 'Quantitative'),
                                             ('Qualitative', 'Qualitative')),
                                    label='Result type',
                                    widget=forms.RadioSelect(),
                                    initial=('Quantitative', 'Quantitative'))

    test_method = forms.ModelChoiceField(
        queryset=lab_models.TestMethod.objects.all(),
        widget=autocomplete.ModelSelect2(url='test_method_autocomplete'),
        required=True,
        label='Method'
    )
    specialty = forms.ModelChoiceField(
        queryset=lab_models.CLIATestTypeSpecialty.objects.all(),
        widget=autocomplete.ModelSelect2(url='test_type_specialty_autocomplete'),
        required=True,
        label='Specialty'
    )
    test_type_categories = forms.ModelMultipleChoiceField(
        label='Test Categories',
        queryset=models.TestTypeCategory.objects.all(),
        widget=autocomplete.ModelSelect2Multiple(url='test_type_category_autocomplete'),
        required=False,
    )
    test_target = forms.ModelChoiceField(
        label='Analyte or Target',
        queryset=lab_models.TestTarget.objects.all(),
        widget=autocomplete.ModelSelect2(url='test_target_autocomplete'),
        required=True,
    )
    isotype = forms.CharField(
        required=False,
        widget=forms.TextInput(
            attrs={'class': 'form-control'},
        ),
        label='Isotype'
    )
    band = forms.CharField(
        required=False,
        widget=forms.TextInput(
            attrs={'class': 'form-control'},
        ),
        label='Band'
    )
    icd10_codes = forms.ModelMultipleChoiceField(
        label='Medical Necessity Codes',
        queryset=codes_models.ICD10CM.objects.all(),
        widget=autocomplete.ModelSelect2Multiple(url='icd10_autocomplete'),
        required=False,
    )

    loinc_code = forms.CharField(max_length=16, label='LOINC Code', required=False,
                                 widget=forms.TextInput(
                                     attrs={'class': 'form-control'},
                                 ))

    turnaround_time = forms.IntegerField(required=True, min_value=0,
                                         widget=forms.NumberInput(attrs={'class': 'form-control'}),
                                         label="Required Turnaround Time",
                                         initial=14)
    turnaround_time_unit = forms.ChoiceField(
        choices=(
            ('Days', 'Days'),
            ('Hours', 'Hours'),
            ('Minutes', 'Minutes')
        ),
        required=True,
        widget=forms.Select(attrs={'class': 'form-control'}),
        label='')
    is_research_only = forms.BooleanField(required=False,
                                          label="Research Use Only",
                                          initial=False)
    offsite_laboratory = forms.ModelChoiceField(
        queryset=lab_models.OffsiteLaboratory.objects.all(),
        widget=autocomplete.ModelSelect2(
            url='offsite_laboratory_autocomplete',
        ),
        required=False,
        label='Offsite Laboratory'
    )
    offsite_test_id = forms.CharField(required=False,
                                      widget=forms.TextInput(
                                          attrs={'class': 'form-control'},
                                      ),
                                      label='Offsite Test ID')
    is_approval_required = forms.BooleanField(required=False,
                                              label='Require Approval',
                                              initial=False)
    unit = forms.ChoiceField(choices=UNIT_CHOICES,
                             required=False,
                             widget=forms.Select(attrs={'class': 'form-control'}),
                             label='Unit of Measurement')

    use_round_figures = forms.BooleanField(required=False,
                                           label="Use Round Figures",
                                           initial=False)
    round_figures = forms.IntegerField(required=False,
                                       widget=forms.TextInput(attrs={'class': 'form-control'}),
                                       label='# of Decimal Places in Results',
                                       min_value=0,
                                       max_value=8,
                                       initial=2)
    min_volume = forms.DecimalField(
        required=False,
        max_digits=18,
        decimal_places=9,
        label='Minimum Required Volume (mL)',
        widget=forms.NumberInput(attrs={'class': 'form-control'})
    )
    container_type = forms.ModelChoiceField(
        queryset=models.ContainerType.objects.all(),
        widget=autocomplete.ModelSelect2(
            url='container_type_autocomplete',
        ),
        required=False,
        label='Container Type'
    )
    is_internal = forms.BooleanField(
        label='Internal Test',
        initial=False,
        widget=forms.CheckboxInput(attrs={'data-toggle': 'toggle'}),
        required=False)

    def clean_round_figures(self):
        round_figures = self.cleaned_data.get('round_figures')
        # if number isn't provided (or is falsey), default to 0
        if not round_figures:
            round_figures = 0
        if round_figures > 8:
            self.add_error('round_figures', forms.ValidationError('Decimal places must be less than 8.'))
        return round_figures

    def clean_aliases(self):
        aliases = self.cleaned_data.get('aliases').splitlines()
        aliases = [alias.strip() for alias in aliases]

        lower_case_aliases = set([alias.lower() for alias in aliases])
        if len(aliases) != len(lower_case_aliases):
            self.add_error('aliases', forms.ValidationError('Please make sure there are no duplicates in the list.'))

        matching_aliases = models.TestTypeAlias.objects.filter(name__in=aliases)

        if self.instance_id and aliases:
            matching_aliases = matching_aliases.exclude(test_type=self.instance_id)
            if matching_aliases:
                for alias in matching_aliases:
                    self.add_error('aliases', forms.ValidationError('This alias already exists: {}'.format(str(alias))))
        return aliases

    def __init__(self, *args, instance_id, **kwargs):
        self.instance_id = instance_id
        super().__init__(*args, **kwargs)


class TestCommentForm(forms.Form):
    comment = forms.CharField(widget=forms.Textarea(
        attrs={'cols': '80',
               'rows': '5'}
    ), required=True)
    is_public = forms.BooleanField(label='Public (Reported)', required=False)


class TestPanelCommentForm(forms.Form):
    comment = forms.CharField(widget=forms.Textarea(
        attrs={'cols': '80',
               'rows': '5'}
    ), required=True)
    is_public = forms.BooleanField(label='Public (Reported)', required=False)


class TestTypeCommentForm(forms.Form):
    comment = forms.CharField(widget=forms.Textarea(
        attrs={'cols': '80',
               'rows': '5'}
    ), required=True)
    is_public = forms.BooleanField(label='Public (Reported)', required=False)


class TestPanelTypeCommentForm(forms.Form):
    comment = forms.CharField(widget=forms.Textarea(
        attrs={'cols': '80',
               'rows': '5'}
    ), required=True)
    is_public = forms.BooleanField(label='Public (Reported)', required=False)


class TestTypeCategoryForm(forms.Form):
    name = forms.CharField(max_length=255,
                           label='Name',
                           required=True,
                           error_messages={'required': 'Name is required.'})


class TestTypeCLIASampleForm(forms.Form):
    sample_type = forms.ModelChoiceField(queryset=lab_models.CLIASampleType.objects.all(),
                                         required=False)
    num_samples = forms.IntegerField(widget=forms.NumberInput(attrs={'class': 'form-control'}),
                                     required=False,
                                     min_value=0,
                                     max_value=10)


class ContainerTypeForm(forms.Form):
    name = forms.CharField(max_length=255,
                           label='Name',
                           required=True,
                           error_messages={'required': 'Name of container is required'})
    volume = forms.DecimalField(
        required=False,
        max_digits=18,
        min_value=0,
        decimal_places=9,
        label='Volume (mL)',
        widget=forms.NumberInput(attrs={'class': 'form-control'})
    )

    def clean_name(self):
        name = self.cleaned_data.get('name')
        matching_container_type_name = models.ContainerType.objects.filter(name__iexact=name)
        if matching_container_type_name:
            matching_container_type_name = matching_container_type_name.exclude(id=self.instance_id)

            if matching_container_type_name:
                self.add_error('name', forms.ValidationError('Container type name already exists.'))

        return name

    def __init__(self, *args, instance_id, **kwargs):
        self.instance_id = instance_id
        super().__init__(*args, **kwargs)


class TestPanelTypeForm(forms.Form):
    # https://stackoverflow.com/questions/16593143/django-how-to-override-default-form-error-messages
    # reminder: you have to include the keys in the error_messages dict that you want to override
    # the other keys aren't overridden
    name = forms.CharField(max_length=255,
                           label='Name',
                           required=True,
                           error_messages={'required': 'Name is required.'})
    aliases = forms.CharField(label='Aliases (separate multiple with a new line)',
                              required=False,
                              widget=forms.Textarea(attrs={'class': 'form-control'}))
    alternate_id = forms.CharField(max_length=32,
                                   label='Alternate ID',
                                   required=False,
                                   widget=forms.TextInput(attrs={'class': 'form-control'}))
    price = forms.DecimalField(decimal_places=2,
                               max_digits=7,
                               error_messages={'required': 'Price is required.'},
                               required=False,
                               label='Price')
    # use ModelMultipleChoiceField for many-to-many relationships
    test_types = forms.ModelMultipleChoiceField(
        label='Tests',
        queryset=models.TestType.objects.all(),
        widget=autocomplete.ModelSelect2Multiple(url='test_type_autocomplete',
                                                 forward=(forward.Self(),)),
        required=False, )
    procedure_codes = forms.ModelMultipleChoiceField(
        label='Procedure Codes',
        queryset=codes_models.ProcedureCode.objects.all(),
        widget=autocomplete.ModelSelect2Multiple(url='procedure_code_autocomplete', ),
        required=False,
    )

    test_panel_category = forms.ModelChoiceField(
        label='Test Profile Category',
        queryset=models.TestPanelCategory.objects.all(),
        widget=autocomplete.ModelSelect2(url='test_panel_type_category_autocomplete'),
        required=True
    )

    lab_departments = forms.ModelMultipleChoiceField(
        label='Lab Departments',
        queryset=lab_tenant_models.LabDepartment.objects.all(),
        widget=autocomplete.ModelSelect2Multiple(url='lab_department_autocomplete', ),
        required=False
    )
    offsite_laboratory = forms.ModelChoiceField(
        queryset=lab_models.OffsiteLaboratory.objects.all(),
        widget=autocomplete.ModelSelect2(
            url='offsite_laboratory_autocomplete',
        ),
        required=False,
        label='Offsite Laboratory'
    )
    category_order = forms.IntegerField(required=False,
                                        min_value=0,
                                        label='Test Profile Category Rank')
    notes = forms.CharField(widget=forms.Textarea(attrs={'class': 'form-control'}),
                            required=False)
    research_consent_required = forms.BooleanField(required=False,
                                                   label='Research consent required')
    is_exclusive = forms.BooleanField(required=False,
                                      label='Only show this test profile for selected accounts and providers')
    exclusive_providers = forms.ModelMultipleChoiceField(
        label='Providers:',
        queryset=accounts_models.Provider.objects.all(),
        widget=autocomplete.ModelSelect2Multiple(url='all_provider_autocomplete',
                                                 forward=(forward.Self(),)),
        required=False, )
    exclusive_accounts = forms.ModelMultipleChoiceField(
        label='Accounts:',
        queryset=accounts_models.Account.objects.all(),
        widget=autocomplete.ModelSelect2Multiple(url='account_autocomplete',
                                                 forward=(forward.Self(),)),
        required=False, )
    in_house_lab_locations = forms.ModelMultipleChoiceField(
        label='In-House Lab Locations',
        queryset=lab_tenant_models.InHouseLabLocation.objects.all(),
        widget=autocomplete.ModelSelect2Multiple(url='in_house_lab_location_autocomplete'),
        required=False
    )

    def clean_price(self):
        price = self.cleaned_data.get('price')



        return price

    def clean_test_types(self):
        offsite_laboratory = self.data.get('offsite_laboratory')
        test_types = self.data.getlist('tests_selected')
        if not offsite_laboratory:
            return test_types
        else:
            # if test_panel is associated with an off-site laboratory then
            # only tests that are associated with that off-site laboratory can
            # be added to it.
            # convert test_types to int values before running comparisons.
            test_types_ints = [int(test_type_id) for test_type_id in test_types]
            valid_tests = [test_type.id for test_type in models.TestType.objects.filter(offsite_laboratory=offsite_laboratory)]
            invalid_tests = [test_type_id for test_type_id in test_types_ints if test_type_id not in valid_tests]

            if len(invalid_tests) > 0:
                self.add_error('test_types', forms.ValidationError('Cannot add tests to the test panel that are \
                                                                    not performed at the selected off-site laboratory.'))
            else:
                return test_types

    def clean_alternate_id(self):
        alternate_id = self.cleaned_data.get('alternate_id')
        matching_alternate_id = models.TestPanelType.objects.filter(alternate_id=alternate_id)

        if self.instance_id and alternate_id:
            matching_alternate_id = matching_alternate_id.exclude(id=self.instance_id)
            if matching_alternate_id:
                self.add_error('alternate_id', forms.ValidationError('This alternate ID already exists.'))
        return alternate_id

    def clean_aliases(self):
        aliases = self.cleaned_data.get('aliases').splitlines()
        aliases = [alias.strip() for alias in aliases]

        lower_case_aliases = set([alias.lower() for alias in aliases])
        if len(aliases) != len(lower_case_aliases):
            self.add_error('aliases', forms.ValidationError('Please make sure there are no duplicates in the list.'))

        matching_aliases = models.TestPanelTypeAlias.objects.filter(name__in=aliases)

        if self.instance_id and aliases:
            matching_aliases = matching_aliases.exclude(test_panel_type=self.instance_id)
            if matching_aliases:
                for alias in matching_aliases:
                    self.add_error('aliases', forms.ValidationError('This alias already exists: {}'.format(str(alias))))
        return aliases

    def __init__(self, *args, instance_id, **kwargs):
        self.instance_id = instance_id
        super().__init__(*args, **kwargs)


class SuperTestPanelForm(forms.Form):
    name = forms.CharField(max_length=255,
                           label='Name',
                           required=True,
                           error_messages={'required': 'Name is required.'})

    test_panel_types = forms.ModelMultipleChoiceField(label='Test Profiles',
                                                      queryset=models.TestPanelType.objects.all(),
                                                      widget=autocomplete.ModelSelect2Multiple(
                                                          url='test_panel_type_autocomplete'),
                                                      required=True)

    is_exclusive = forms.BooleanField(required=False,
                                      label='Only show this super test profile for selected accounts and providers')
    exclusive_providers = forms.ModelMultipleChoiceField(
        label='Providers:',
        queryset=accounts_models.Provider.objects.all(),
        widget=autocomplete.ModelSelect2Multiple(url='all_provider_autocomplete',
                                                 forward=(forward.Self(),)),
        required=False, )
    exclusive_accounts = forms.ModelMultipleChoiceField(
        label='Accounts:',
        queryset=accounts_models.Account.objects.all(),
        widget=autocomplete.ModelSelect2Multiple(url='account_autocomplete',
                                                 forward=(forward.Self(),)),
        required=False, )

    def clean_name(self):
        name = self.cleaned_data.get('name')
        matching_name = models.SuperTestPanelType.objects.filter(name=name)

        if name and matching_name:
            matching_name = matching_name.exclude(id=self.instance_id)
            if matching_name:
                self.add_error('name', forms.ValidationError(
                    'This super test profile name already exists.'))
        return name

    def __init__(self, *args, instance_id, **kwargs):
        self.instance_id = instance_id
        super().__init__(*args, **kwargs)


class TestPanelCategoryForm(forms.Form):
    name = forms.CharField(max_length=32,
                           label='Category Name',
                           required=True,
                           widget=forms.TextInput(attrs={'class': 'form-control'}))


class OrderAutocompleteForm(forms.Form):
    provider = forms.ModelChoiceField(
        queryset=accounts_models.Provider.objects.all(),
        widget=autocomplete.ModelSelect2(
            url='provider_autocomplete',
        ),
        required=False,
        label='Provider'
    )
    account = forms.ModelChoiceField(
        queryset=accounts_models.Account.objects.all(),
        widget=autocomplete.ModelSelect2(
            url='account_autocomplete',
        ),
        required=False,
        label='Account'
    )
    order = forms.ModelChoiceField(
        queryset=models.Order.objects.filter(submitted=True),
        widget=autocomplete.ModelSelect2(
            url='order_autocomplete',
            forward=(forward.Field('provider', 'provider'), forward.Field('account', 'account'),),
        ),
        required=True,
    )


class UnreceivedOrderAutocompleteForm(forms.Form):
    order = forms.ModelChoiceField(
        queryset=models.Order.objects.filter(submitted=True),
        widget=autocomplete.ModelSelect2(
            url='unreceived_order_autocomplete',
        ),
        required=True,
    )


def get_test_method_labels(test_method_name):
    """
    Helper function to get a test method's result labels
    :param test_method_name:
    :return: (String, String, String)
    """
    try:
        test_method = lab_models.TestMethod.objects.get(name=test_method_name)
        positive_label = test_method.positive_result_label
        negative_label = test_method.negative_result_label
        equivocal_label = test_method.equivocal_result_label
    except ProgrammingError:
        positive_label = 'Positive'
        negative_label = 'Negative'
        equivocal_label = 'Equivocal'
    except lab_models.TestMethod.DoesNotExist:
        positive_label = 'Positive'
        negative_label = 'Negative'
        equivocal_label = 'Equivocal'

    return positive_label, negative_label, equivocal_label


class ResultForm(forms.Form):
    result_obj = forms.ModelChoiceField(queryset=models.Result.objects.all(),
                                        required=False)
    result = forms.ChoiceField(choices=(('', '-------'),
                                        ('+', 'Positive'),
                                        ('-', 'Negative'),
                                        ('=', 'Equivocal'),
                                        ('?', 'Indeterminate'),
                                        ('.', 'Pending')), required=False)
    result_quantitative = forms.CharField(max_length=255,
                                          label='Result',
                                          required=False,
                                          widget=forms.TextInput(
                                              attrs={'class': 'form-control numberMask'}
                                          ))
    result_label = forms.CharField(required=False)
    result_label_prefix = forms.ChoiceField(required=False,
                                            choices=(('', ''),
                                                     ('≥', '≥'),
                                                     ('>', '>'),
                                                     ('≤', '≤'),
                                                     ('<', '<')))
    reference = forms.ChoiceField(choices=(('', '-------'),
                                           ('+', 'Positive'),
                                           ('-', 'Negative'),
                                           ('RUO', 'RUO')), required=False)
    due_date = forms.DateTimeField(label='Due date', required=False,
                                   input_formats=['%m-%d-%Y %H:%M'],
                                   widget=forms.DateTimeInput(
                                       attrs={'class': 'form-control dateMask'}))

    def clean_result_quantitative(self):
        result_quantitative = self.cleaned_data['result_quantitative']
        if result_quantitative.strip() == '':
            return None
        try:
            Decimal(result_quantitative)
        except decimal.InvalidOperation:
            raise forms.ValidationError('Quantitative result must be a number.')
        return result_quantitative

    def clean_due_date(self):
        due_date = self.cleaned_data['due_date']
        return due_date

    def clean_result(self):
        result = self.cleaned_data['result']
        if result.strip() == '':
            return None
        return result

    def clean_reference(self):
        reference = self.cleaned_data['reference']
        if reference.strip() == '':
            return None
        return reference

    def clean(self):
        due_date = self.cleaned_data.get('due_date')
        if not due_date:
            self.add_error('due_date', forms.ValidationError('Invalid DateTime format.'))

    def __init__(self, *args, test_method, **kwargs):
        self.test_method = test_method
        super().__init__(*args, **kwargs)
        positive_label, negative_label, equivocal_label = get_test_method_labels(
            self.test_method.name)
        if positive_label and negative_label and equivocal_label:
            self.fields['result'] = forms.ChoiceField(choices=(('', '-------'),
                                                               ('-', negative_label),
                                                               ('+', positive_label),
                                                               ('=', equivocal_label),
                                                               ('?', 'Indeterminate'),
                                                               ('.', 'Pending')), required=False)
            self.fields['reference'] = forms.ChoiceField(choices=(('-', negative_label),
                                                                  ('+', positive_label),
                                                                  ('RUO', 'RUO')), required=False)
        if self.test_method.is_within_range:
            self.fields['result'] = forms.ChoiceField(choices=(('', '-------'),
                                                               ('<', 'Below Range'),
                                                               ('>', 'Above Range'),
                                                               ('~', 'Within Range')), required=False)


class BaseResultFormSet(BaseFormSet):
    def __init__(self, test_method, *args, **kwargs):
        super(BaseResultFormSet, self).__init__(*args, **kwargs)
        for form in self.forms:
            form.empty_permitted = True


class OrderCommentForm(forms.ModelForm):
    comment = forms.CharField(widget=forms.Textarea(
        attrs={'cols': '80',
               'rows': '5'},
    ))

    class Meta:
        model = models.OrderComment
        fields = ('comment',)


class AccessioningViewCommentForm(forms.ModelForm):
    comment = forms.CharField(required=False, widget=forms.Textarea(
        attrs={'cols': '80',
               'rows': '5'},
    ))

    barcode = forms.CharField(required=False)
    order_code = forms.CharField(required=False)

    class Meta:
        model = models.OrderComment
        fields = ('comment',)


class OrderNotesForm(forms.Form):
    notes = forms.CharField(widget=forms.Textarea(
        attrs={'cols': '80',
               'rows': '5'}
    ), required=False)

    def clean_notes(self):
        notes = self.cleaned_data['notes']
        return notes


class OrderUpdateForm(forms.Form):
    received_date = forms.DateField(required=True,
                                    label='Received Date',
                                    input_formats=['%m-%d-%Y'],
                                    widget=forms.DateInput(attrs={
                                        'class': 'form-control date_input datetimeMask',
                                        'placeholder': 'MM-DD-YYYY'
                                    }))
    received_time = forms.CharField(required=False, label='Received Time')

    research_consent = forms.BooleanField(label='Consent to research', required=False)
    patient_result_request = forms.BooleanField(label='Patient result request', required=False)

    swap_accession_number_with = forms.ModelChoiceField(
        queryset=models.Order.objects.all(),
        widget=autocomplete.ModelSelect2(
            url='order_accession_number_swap_autocomplete',
        ),
        required=False,
        label='Swap Accession Number with:',
    )

    account = forms.ModelChoiceField(
        queryset=accounts_models.Account.objects.all(),
        widget=autocomplete.ModelSelect2(
            url='account_autocomplete',
            forward=(forward.Field('provider', 'provider'),)
        ),
        required=True,
        label='Account'
    )

    provider = forms.ModelChoiceField(
        queryset=accounts_models.Provider.objects.all(),
        widget=autocomplete.ModelSelect2(
            url='provider_autocomplete',
            forward=(forward.Field('account', 'account'),
                     forward.Field('account_type', 'account_type')),
        ),
        required=False,
        label='Provider'
    )

    patient = forms.ModelChoiceField(
        queryset=patients_models.Patient.objects.all(),
        widget=autocomplete.ModelSelect2(
            url='patient_order_autocomplete',
            forward=(forward.Field('provider', 'provider'),),
        ),
        required=True,
        label='Patient'
    )

    icd10_codes = forms.ModelMultipleChoiceField(
        label='ICD-10 Codes',
        queryset=codes_models.ICD10CM.objects.all(),
        widget=autocomplete.ModelSelect2Multiple(url='icd10_autocomplete'),
        required=False,
    )
    alternate_id = forms.CharField(
        label='Order Alternate ID (External ID)',
        required=False,
        widget=forms.TextInput(attrs={'class': 'form-control'})
    )

    def clean_received_time(self):
        received_time = self.cleaned_data.get('received_time')
        try:
            received_time = datetime.datetime.strptime(received_time, '%H:%M').time()
        except ValueError:
            # if no received time is provided, or bad format, default to 00:00
            received_time = datetime.datetime.strptime('00:00', '%H:%M').time()
        return received_time

    def clean_alternate_id(self):
        alternate_id = self.cleaned_data.get('alternate_id')
        if alternate_id and len(alternate_id) > 32:
            self.add_error('alternate_id', forms.ValidationError('Alternate ID may not be longer than 32 characters.'))
        return alternate_id

    def __init__(self, *args, **kwargs):
        self.order_id = kwargs.pop('order_id')
        super(OrderUpdateForm, self).__init__(*args, **kwargs)
        self.fields['swap_accession_number_with'] = forms.ModelChoiceField(
            queryset=models.Order.objects.all(),
            widget=autocomplete.ModelSelect2(
                url='order_accession_number_swap_autocomplete',
                forward=(forward.Self(),)
            ),
            required=False,
            label='Swap Accession Number with:',
        )


class OrderIssueForm(forms.Form):
    issue_type = forms.ChoiceField(choices=(('', '-------'),
                                            ('test_not_performed', 'Test Not Performed'),
                                            ('demographics', 'Patient Demographics'),
                                            ('billing', 'Billing/Coding'),
                                            ('medication', 'Medication'),
                                            ('other', 'Other')), required=False, label='Type')
    is_public = forms.BooleanField(label='Public', required=False)

    order = forms.ModelChoiceField(queryset=models.Order.objects.none(), )
    samples = forms.ModelMultipleChoiceField(
        queryset=models.Sample.objects.filter(),
        widget=autocomplete.ModelSelect2Multiple(url='order_sample_autocomplete',
                                                 forward=(forward.Field('order', 'order'),)),
        required=False,
        label='Samples'
    )

    description = forms.CharField(widget=forms.Textarea(
        attrs={'cols': '80',
               'rows': '5'}
    ), required=True)

    def clean(self):
        issue_type = self.cleaned_data.get('issue_type')
        if not issue_type:
            self.add_error('issue_type', forms.ValidationError('Please choose an issue type.'))

    def __init__(self, *args, order_uuid, **kwargs):
        self.order_uuid = order_uuid
        super().__init__(*args, **kwargs)
        self.fields['order'] = forms.ModelChoiceField(
            queryset=models.Order.objects.filter(uuid=self.order_uuid), )


class OrderIssueResolvedForm(forms.Form):
    comment = forms.CharField(label='Resolver Comment', required=False, max_length=500,
                              widget=forms.TextInput(attrs={'class': 'form-control'}))

    def clean_comment(self):
        comment = self.cleaned_data.get('comment')
        return comment.strip()

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)


class OrderResultForm(forms.Form):
    order = forms.ModelChoiceField(queryset=models.Order.objects.none(), )
    sample = forms.ModelChoiceField(
        queryset=models.Sample.objects.all(),
        widget=autocomplete.ModelSelect2(
            url='order_sample_autocomplete', forward=(forward.Field('order', 'order'),)),
        required=True,
        label='Sample'
    )
    test_type = forms.ModelChoiceField(
        queryset=models.TestType.objects.all(),
        widget=autocomplete.ModelSelect2(
            url='order_test_create_test_type_autocomplete', forward=(forward.Field('order', 'order'),)),
        required=True,
        label='Test'
    )
    days_incubated = forms.IntegerField(required=False,
                                        min_value=0,
                                        widget=forms.NumberInput(
                                            attrs={'class': 'form-control'}
                                        ),
                                        label="Number of days incubated (culture samples)")
    due_date = forms.DateField(label='Due date', required=False,
                               input_formats=['%m-%d-%Y'],
                               widget=forms.DateInput(
                                   attrs={'class': 'form-control dateMask'}
                               ))

    def __init__(self, *args, order_uuid, **kwargs):
        self.order_uuid = order_uuid
        super().__init__(*args, **kwargs)
        self.fields['order'] = forms.ModelChoiceField(
            queryset=models.Order.objects.filter(uuid=self.order_uuid), )

    def clean_test_type(self):
        test_type = self.cleaned_data['test_type']
        order = self.cleaned_data['order']

        # make sure the selected test type is not already present on the order
        if test_type:
            order_tests = order.tests.prefetch_related('test_type').all()
            order_test_types = [test.test_type.id for test in order_tests]

            if test_type.id in order_test_types:
                self.add_error('test_type', forms.ValidationError(
                    'Selected test is already present on this order. Please select a different test.'))
            else:
                return test_type
        else:
            self.add_error('test_type', forms.ValidationError(
                'Please select a test to add to this order.'))


class TestPerformedByForm(forms.Form):
    user = forms.ModelChoiceField(
        queryset=lab_tenant_models.User.objects.filter(user_type__lt=10).all(),
        widget=autocomplete.ModelSelect2(url='lab_member_autocomplete'),
        required=False,
        label='Test performed by',
    )

    def clean_user(self):
        return self.cleaned_data['user']


class ReportApprovedByForm(forms.Form):
    user = forms.ModelChoiceField(
        queryset=lab_tenant_models.User.objects.filter(user_type__lt=10).all(),
        widget=autocomplete.ModelSelect2(url='lab_member_autocomplete'),
        required=False,
        label='Report approved by',
    )

    def clean_user(self):
        return self.cleaned_data['user']


class SampleTypeForm(forms.Form):
    name = forms.CharField(required=True,
                           widget=forms.TextInput(attrs={'class': 'form-control'}))
    notes = forms.CharField(widget=forms.Textarea(
        attrs={'cols': '80',
               'rows': '5'}
    ), required=False)


class SampleSelectForm(forms.Form):
    """
    used in BatchCreate
    """
    test_panel_type = forms.ModelChoiceField(queryset=models.TestPanelType.objects.all())
    sample = forms.ModelChoiceField(
        queryset=models.Sample.objects.all(),
        widget=autocomplete.ModelSelect2(url='sample_autocomplete',
                                         forward=(forward.Field('test_panel_type', 'test_panel_type'),)),
        required=False,
        label=''
    )

    def __init__(self, *args, test_panel_type_uuid, **kwargs):
        self.test_panel_type_uuid = test_panel_type_uuid
        super().__init__(*args, **kwargs)
        self.fields['test_panel_type'] = forms.ModelChoiceField(
            queryset=models.TestPanelType.objects.filter(uuid=self.test_panel_type_uuid))


class OperationalReportForm(forms.Form):
    date_filter = forms.ChoiceField(choices=DATE_FILTER_CHOICES, required=True,
                                    widget=forms.Select(attrs={'class': 'form-control'}), label='Filter Type')
    start_date = forms.DateField(label='Start Date', required=False,
                                 input_formats=['%m-%d-%Y'],
                                 widget=forms.DateInput(attrs={'class': 'form-control dateMask start-date'}))
    end_date = forms.DateField(label='End Date (Inclusive)', required=True,
                               input_formats=['%m-%d-%Y'],
                               widget=forms.DateInput(attrs={'class': 'form-control dateMask end-date'}))
    report_type = forms.ChoiceField(choices=REPORT_TYPES, required=True, widget=forms.Select(
        attrs={'class': 'form-control'}), label='Format')


class BatchForm(forms.Form):
    name = forms.CharField(required=True,
                           widget=forms.TextInput(attrs={'class': 'form-control'}))
    test_panel_type = forms.ModelChoiceField(
        queryset=models.TestPanelType.objects.all(),
        widget=autocomplete.ModelSelect2(url='test_panel_type_autocomplete'),
        required=True,
        label='Test Profile',
    )
    instrument_integration = forms.ModelChoiceField(
        queryset=lab_tenant_models.InstrumentIntegration.objects.all(),
        widget=autocomplete.ModelSelect2(url='instrument_integration_autocomplete'),
        required=True,
        label='Instrument'
    )
    controls = forms.ModelMultipleChoiceField(
        queryset=models.Control.objects.all(),
        widget=autocomplete.ModelSelect2Multiple(url='control_autocomplete'),
        required=False,
        label='Included controls'
    )


class ReportRemarksForm(forms.Form):
    test_target = forms.ModelChoiceField(
        label='Analyte or Target',
        queryset=lab_models.TestTarget.objects.all(),
        widget=autocomplete.ModelSelect2(url='test_target_autocomplete'),
        required=False,
    )
    account = forms.ModelChoiceField(
        queryset=accounts_models.Account.objects.all(),
        widget=autocomplete.ModelSelect2(url='accounts_autocomplete'),
        required=False,
        label='Account'
    )
    offsite_laboratory = forms.ModelChoiceField(
        queryset=lab_models.OffsiteLaboratory.objects.all(),
        widget=autocomplete.ModelSelect2(
            url='offsite_laboratory_autocomplete',
        ),
        required=False,
        label='Offsite Laboratory'
    )
    remarks = forms.CharField(widget=forms.Textarea(
        attrs={'cols': '100',
               'rows': '10'}
    ), required=True)


class OrderAccessionNumberForm(forms.Form):
    accession_number = forms.CharField(
        required=True,
        label='Accession Number',
        widget=forms.TextInput(attrs={'class': 'form-control accessionNumberMask'})
    )

    def __init__(self, *args, order_id, **kwargs):
        self.order_id = order_id
        super().__init__(*args, **kwargs)

    def clean_accession_number(self):
        accession_number = self.cleaned_data['accession_number']
        orders_matching_accession_number = models.Order.objects.filter(accession_number=accession_number). \
            exclude(id=self.order_id)
        if orders_matching_accession_number:
            self.add_error('accession_number', forms.ValidationError(
                'Accession Number {} already exists. Please enter another number.'.format(accession_number)))
        if len(accession_number) != 11:
            self.add_error('accession_number', forms.ValidationError('Accession Number must be 11 digits.'))
        try:
            int(accession_number)
        except ValueError:
            self.add_error('accession_number', forms.ValidationError('Accession Number must be all numerical digits.'))
        return accession_number
