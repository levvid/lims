var labelXml = `<?xml version="1.0" encoding="utf-8"?>
  <DieCutLabel Version="8.0" Units="twips">
    <PaperOrientation>Landscape</PaperOrientation>
    <Id>Address</Id>
    <PaperName>30252 Address</PaperName>
    <DrawCommands>
      <RoundRectangle X="0" Y="0" Width="1581" Height="5040" Rx="270" Ry="270" />
    </DrawCommands>
    <ObjectInfo>
      <TextObject>
        <Name>PatientName</Name>
        <ForeColor Alpha="255" Red="0" Green="0" Blue="0" />
        <BackColor Alpha="0" Red="255" Green="255" Blue="255" />
        <LinkedObjectName></LinkedObjectName>
        <Rotation>Rotation0</Rotation>
        <IsMirrored>False</IsMirrored>
        <IsVariable>True</IsVariable>
        <HorizontalAlignment>Left</HorizontalAlignment>
        <VerticalAlignment>Bottom</VerticalAlignment>
        <TextFitMode>ShrinkToFit</TextFitMode>
        <UseFullFontHeight>True</UseFullFontHeight>
        <Verticalized>False</Verticalized>
        <StyledText/>
      </TextObject>
      <Bounds X="320" Y="200" Width="1900" Height="220" />
    </ObjectInfo>
    <ObjectInfo>
      <TextObject>
        <Name>DOB</Name>
        <ForeColor Alpha="255" Red="0" Green="0" Blue="0" />
        <BackColor Alpha="0" Red="255" Green="255" Blue="255" />
        <LinkedObjectName></LinkedObjectName>
        <Rotation>Rotation0</Rotation>
        <IsMirrored>False</IsMirrored>
        <IsVariable>True</IsVariable>
        <HorizontalAlignment>Left</HorizontalAlignment>
        <VerticalAlignment>Bottom</VerticalAlignment>
        <TextFitMode>ShrinkToFit</TextFitMode>
        <UseFullFontHeight>True</UseFullFontHeight>
        <Verticalized>False</Verticalized>
        <StyledText/>
      </TextObject>
      <Bounds X="320" Y="380" Width="1250" Height="200" />
    </ObjectInfo>
    <ObjectInfo>
      <TextObject>
        <Name>CollectionDate</Name>
        <ForeColor Alpha="255" Red="0" Green="0" Blue="0" />
        <BackColor Alpha="0" Red="255" Green="255" Blue="255" />
        <LinkedObjectName></LinkedObjectName>
        <Rotation>Rotation0</Rotation>
        <IsMirrored>False</IsMirrored>
        <IsVariable>True</IsVariable>
        <HorizontalAlignment>Left</HorizontalAlignment>
        <VerticalAlignment>Bottom</VerticalAlignment>
        <TextFitMode>ShrinkToFit</TextFitMode>
        <UseFullFontHeight>True</UseFullFontHeight>
        <Verticalized>False</Verticalized>
        <StyledText/>
      </TextObject>
      <Bounds X="320" Y="550" Width="2100" Height="200" />
    </ObjectInfo>
    <ObjectInfo>
      <BarcodeObject>
        <Name>Barcode</Name>
        <ForeColor Alpha="255" Red="0" Green="0" Blue="0" />
        <BackColor Alpha="0" Red="255" Green="255" Blue="255" />
        <LinkedObjectName></LinkedObjectName>
        <Rotation>Rotation0</Rotation>
        <IsMirrored>False</IsMirrored>
        <IsVariable>False</IsVariable>
        <Text></Text>
        <Type>Code128Auto</Type>
        <Size>Medium</Size>
        <TextPosition>None</TextPosition>
        <TextFont Family="Arial" Size="8" Bold="False" Italic="False" Underline="False" Strikeout="False" />
        <CheckSumFont Family="Arial" Size="6" Bold="False" Italic="False" Underline="False" Strikeout="False" />
        <TextEmbedding>None</TextEmbedding>
        <ECLevel>0</ECLevel>
        <HorizontalAlignment>Left</HorizontalAlignment>
        <QuietZonesPadding Left="0" Top="0" Right="0" Bottom="0" />
      </BarcodeObject>
      <Bounds X="320" Y="750" Width="3500" Height="350" />
    </ObjectInfo>
    <ObjectInfo>
      <TextObject>
        <Name>ClientName</Name>
        <ForeColor Alpha="255" Red="0" Green="0" Blue="0" />
        <BackColor Alpha="0" Red="255" Green="255" Blue="255" />
        <LinkedObjectName></LinkedObjectName>
        <Rotation>Rotation0</Rotation>
        <IsMirrored>False</IsMirrored>
        <IsVariable>True</IsVariable>
        <HorizontalAlignment>Left</HorizontalAlignment>
        <VerticalAlignment>Bottom</VerticalAlignment>
        <TextFitMode>ShrinkToFit</TextFitMode>
        <UseFullFontHeight>True</UseFullFontHeight>
        <Verticalized>False</Verticalized>
        <StyledText/>
      </TextObject>
      <Bounds X="320" Y="1120" Width="2300" Height="200" />
    </ObjectInfo>
    <ObjectInfo>
      <TextObject>
        <Name>SampleCode</Name>
        <ForeColor Alpha="255" Red="0" Green="0" Blue="0" />
        <BackColor Alpha="0" Red="255" Green="255" Blue="255" />
        <LinkedObjectName></LinkedObjectName>
        <Rotation>Rotation0</Rotation>
        <IsMirrored>False</IsMirrored>
        <IsVariable>True</IsVariable>
        <HorizontalAlignment>Left</HorizontalAlignment>
        <VerticalAlignment>Bottom</VerticalAlignment>
        <TextFitMode>ShrinkToFit</TextFitMode>
        <UseFullFontHeight>True</UseFullFontHeight>
        <Verticalized>False</Verticalized>
        <StyledText/>
      </TextObject>
      <Bounds X="320" Y="1300" Width="2600" Height="200" />
    </ObjectInfo>
  </DieCutLabel>`


var containerLabelXml = `<?xml version="1.0" encoding="utf-8"?>
  <DieCutLabel Version="8.0" Units="twips">
    <PaperOrientation>Landscape</PaperOrientation>
    <Id>Address</Id>
    <PaperName>30252 Address</PaperName>
    <DrawCommands>
      <RoundRectangle X="0" Y="0" Width="1581" Height="5040" Rx="270" Ry="270" />
    </DrawCommands>
    <ObjectInfo>
      <TextObject>
        <Name>PatientName</Name>
        <ForeColor Alpha="255" Red="0" Green="0" Blue="0" />
        <BackColor Alpha="0" Red="255" Green="255" Blue="255" />
        <LinkedObjectName></LinkedObjectName>
        <Rotation>Rotation0</Rotation>
        <IsMirrored>False</IsMirrored>
        <IsVariable>True</IsVariable>
        <HorizontalAlignment>Left</HorizontalAlignment>
        <VerticalAlignment>Bottom</VerticalAlignment>
        <TextFitMode>ShrinkToFit</TextFitMode>
        <UseFullFontHeight>True</UseFullFontHeight>
        <Verticalized>False</Verticalized>
        <StyledText/>
      </TextObject>
      <Bounds X="320" Y="150" Width="1900" Height="220" />
    </ObjectInfo>
    <ObjectInfo>
      <TextObject>
        <Name>DOB</Name>
        <ForeColor Alpha="255" Red="0" Green="0" Blue="0" />
        <BackColor Alpha="0" Red="255" Green="255" Blue="255" />
        <LinkedObjectName></LinkedObjectName>
        <Rotation>Rotation0</Rotation>
        <IsMirrored>False</IsMirrored>
        <IsVariable>True</IsVariable>
        <HorizontalAlignment>Left</HorizontalAlignment>
        <VerticalAlignment>Bottom</VerticalAlignment>
        <TextFitMode>ShrinkToFit</TextFitMode>
        <UseFullFontHeight>True</UseFullFontHeight>
        <Verticalized>False</Verticalized>
        <StyledText/>
      </TextObject>
      <Bounds X="320" Y="380" Width="1250" Height="200" />
    </ObjectInfo>
    <ObjectInfo>
      <TextObject>
        <Name>CollectionDate</Name>
        <ForeColor Alpha="255" Red="0" Green="0" Blue="0" />
        <BackColor Alpha="0" Red="255" Green="255" Blue="255" />
        <LinkedObjectName></LinkedObjectName>
        <Rotation>Rotation0</Rotation>
        <IsMirrored>False</IsMirrored>
        <IsVariable>True</IsVariable>
        <HorizontalAlignment>Left</HorizontalAlignment>
        <VerticalAlignment>Bottom</VerticalAlignment>
        <TextFitMode>ShrinkToFit</TextFitMode>
        <UseFullFontHeight>True</UseFullFontHeight>
        <Verticalized>False</Verticalized>
        <StyledText/>
      </TextObject>
      <Bounds X="320" Y="550" Width="2100" Height="200" />
    </ObjectInfo>
    <ObjectInfo>
      <BarcodeObject>
        <Name>Barcode</Name>
        <ForeColor Alpha="255" Red="0" Green="0" Blue="0" />
        <BackColor Alpha="0" Red="255" Green="255" Blue="255" />
        <LinkedObjectName></LinkedObjectName>
        <Rotation>Rotation0</Rotation>
        <IsMirrored>False</IsMirrored>
        <IsVariable>False</IsVariable>
        <Text></Text>
        <Type>Code128Auto</Type>
        <Size>Medium</Size>
        <TextPosition>None</TextPosition>
        <TextFont Family="Arial" Size="8" Bold="False" Italic="False" Underline="False" Strikeout="False" />
        <CheckSumFont Family="Arial" Size="6" Bold="False" Italic="False" Underline="False" Strikeout="False" />
        <TextEmbedding>None</TextEmbedding>
        <ECLevel>0</ECLevel>
        <HorizontalAlignment>Left</HorizontalAlignment>
        <QuietZonesPadding Left="0" Top="0" Right="0" Bottom="0" />
      </BarcodeObject>
      <Bounds X="320" Y="750" Width="3500" Height="300" />
    </ObjectInfo>
    <ObjectInfo>
      <TextObject>
        <Name>ClientName</Name>
        <ForeColor Alpha="255" Red="0" Green="0" Blue="0" />
        <BackColor Alpha="0" Red="255" Green="255" Blue="255" />
        <LinkedObjectName></LinkedObjectName>
        <Rotation>Rotation0</Rotation>
        <IsMirrored>False</IsMirrored>
        <IsVariable>True</IsVariable>
        <HorizontalAlignment>Left</HorizontalAlignment>
        <VerticalAlignment>Bottom</VerticalAlignment>
        <TextFitMode>ShrinkToFit</TextFitMode>
        <UseFullFontHeight>True</UseFullFontHeight>
        <Verticalized>False</Verticalized>
        <StyledText/>
      </TextObject>
      <Bounds X="320" Y="1000" Width="2300" Height="200" />
    </ObjectInfo>
    <ObjectInfo>
      <TextObject>
        <Name>SampleCode</Name>
        <ForeColor Alpha="255" Red="0" Green="0" Blue="0" />
        <BackColor Alpha="0" Red="255" Green="255" Blue="255" />
        <LinkedObjectName></LinkedObjectName>
        <Rotation>Rotation0</Rotation>
        <IsMirrored>False</IsMirrored>
        <IsVariable>True</IsVariable>
        <HorizontalAlignment>Left</HorizontalAlignment>
        <VerticalAlignment>Bottom</VerticalAlignment>
        <TextFitMode>ShrinkToFit</TextFitMode>
        <UseFullFontHeight>True</UseFullFontHeight>
        <Verticalized>False</Verticalized>
        <StyledText/>
      </TextObject>
      <Bounds X="320" Y="1200" Width="2600" Height="200" />
    </ObjectInfo>
  </DieCutLabel>`


function generateXml(originalXml, label, labelPlaceholder) {
    `
    Generates the width of a label given the number of characters in
    the label
  `
    var charWidth = 80;
    var labelWidth = charWidth * label.length;
    return originalXml.replace(labelPlaceholder, labelWidth.toString());
}


function zero_pad(d) {
  return (d < 10 ? '0' : '') + d;
}


function formatDate(date) {
    date = date.replace('.', '');
    var tmpDate = new Date(date);
    var date = zero_pad(tmpDate.getDate());
    var month = zero_pad(tmpDate.getMonth() + 1);
    var year = tmpDate.getFullYear();

    return month + '/' + date + '/' + year;
}


function decodeHtml(html) {
    // remove escape characters from text
    var txt = document.createElement("textarea");
    txt.innerHTML = html;
    return txt.value;
}


function printLabel(patient_name, sample_code, patient_dob, sample_collection_date, client_name, sample_barcode, auto_print) {
    client_name = decodeHtml(client_name.substring(0, 35));
    patient_name = decodeHtml(patient_name.substring(0, 25));
    var label = dymo.label.framework.openLabelXml(labelXml);
    label.setObjectText("Barcode", sample_barcode);
    label.setObjectText("PatientName", patient_name);
    label.setObjectText("DOB", patient_dob);
    label.setObjectText("CollectionDate", sample_collection_date);
    label.setObjectText("ClientName", client_name);
    label.setObjectText("SampleCode", sample_code);

    var printers = dymo.label.framework.getPrinters();

    if (auto_print){ // auto print labels - do not alert when printer is missing
        if (printers.length == 0) {
            throw "No DYMO printers are installed. Install DYMO printers.";
            return;
        }
    } else {
        if (printers.length == 0) {
            alert("No Dymo Printers installed!");
            throw "No DYMO printers are installed. Install DYMO printers.";
        }
    }


    var printerName = "";
    for (var i = 0; i < printers.length; ++i) {
        var printer = printers[i];
        if (printer.printerType == "LabelWriterPrinter") {
            printerName = printer.name;
            break;
        }
    }
    label.print(printerName);
    // only alert when not autoprinting
    if (!auto_print){
        alert("Printing label for sample " + sample_code);
    }
    else{
      alert("Auto-printing sample labels for this order");
    }

}


function printContainerLabel(patient_name, sample_code, patient_dob, sample_collection_date, client_name, sample_barcode, auto_print) {
    client_name = decodeHtml(client_name.substring(0, 35));
    patient_name = decodeHtml(patient_name.substring(0, 25));
    var label = dymo.label.framework.openLabelXml(containerLabelXml);
    label.setObjectText("Barcode", sample_barcode);
    label.setObjectText("PatientName", patient_name);
    label.setObjectText("DOB", patient_dob);
    label.setObjectText("CollectionDate", sample_collection_date);
    label.setObjectText("ClientName", client_name);
    label.setObjectText("SampleCode", sample_code);

    var printers = dymo.label.framework.getPrinters();

    if (auto_print){ // auto print labels - do not alert when printer is missing
        if (printers.length == 0) {
            throw "No DYMO printers are installed. Install DYMO printers.";
            return;
        }
    } else {
        if (printers.length == 0) {
            alert("No Dymo Printers installed!");
            throw "No DYMO printers are installed. Install DYMO printers.";
        }
    }


    var printerName = "";
    for (var i = 0; i < printers.length; ++i) {
        var printer = printers[i];
        if (printer.printerType == "LabelWriterPrinter") {
            printerName = printer.name;
            break;
        }
    }
    label.print(printerName);
    // only alert when not autoprinting
    if (!auto_print){
        alert("Printing label for sample " + sample_code);
    }
    else{
      alert("Auto-printing sample labels for this order");
    }

}
