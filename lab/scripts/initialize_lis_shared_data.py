import django
import os

os.environ['DJANGO_SETTINGS_MODULE'] = 'main.settings'
django.setup()

from .. import models

def main():
    fill_test_method()
    fill_test_targets()
    fill_tox_test_targets()

def fill_test_method():
    pcr_method = models.TestMethod.objects.create(name='PCR',
                                                  cutoff_field='DNA Sequence ID',
                                                  positive_result_label='Detected',
                                                  negative_result_label='Not-Detected')
    epcr_method = models.TestMethod.objects.create(name='ePCR',
                                                   cutoff_field='DNA Sequence ID',
                                                   positive_result_label='Detected',
                                                   negative_result_label='Not-Detected'
                                                       )
    ifa_method = models.TestMethod.objects.create(name='IFA',
                                                  cutoff_field='Titer',
                                                  positive_result_label='Reactive',
                                                  negative_result_label='Non-Reactive')
    wb_method = models.TestMethod.objects.create(name='WB',
                                                 positive_result_label='Present',
                                                 negative_result_label='Absent')
    elisa_method = models.TestMethod.objects.create(name='ELISA')
    lcms_method = models.TestMethod.objects.create(name='LC-MS',
                                                   cutoff_field='Concentration (ng/mL)')

def fill_test_targets():
    models.TestTarget.objects.create(name='Borrelia burgdorferi',
                                     is_tox_target=False)
    models.TestTarget.objects.create(name='Bartonella quintana',
                                     is_tox_target=False)
    models.TestTarget.objects.create(name='Bartonella henselae',
                                     is_tox_target=False)
    models.TestTarget.objects.create(name='Rickettsia spp.',
                                     is_tox_target=False)
    models.TestTarget.objects.create(name='Ehrlichia spp.',
                                     is_tox_target=False)
    models.TestTarget.objects.create(name='Bartonella spp.',
                                     is_tox_target=False)
    models.TestTarget.objects.create(name='Babesia/Theileria spp.',
                                     is_tox_target=False)
    models.TestTarget.objects.create(name='Anaplasma spp.',
                                     is_tox_target=False)

def fill_tox_test_targets():
    models.TestTarget.objects.create(name='6-Monoacetyl Morphine',
                                     is_tox_target=True)
    models.TestTarget.objects.create(name='Cocaine',
                                     is_tox_target=True)
    models.TestTarget.objects.create(name='Benzoylecgonine',
                                     is_tox_target=True)
    models.TestTarget.objects.create(name='Cocaethylene',
                                     is_tox_target=True)
    models.TestTarget.objects.create(name='Methamphetamine',
                                     is_tox_target=True)
    models.TestTarget.objects.create(name='MDMA',
                                     is_tox_target=True)
    models.TestTarget.objects.create(name='MDA',
                                     is_tox_target=True)
    models.TestTarget.objects.create(name='MDEA',
                                     is_tox_target=True)
    models.TestTarget.objects.create(name='Phencyclidine',
                                     is_tox_target=True)
    models.TestTarget.objects.create(name='THCA',
                                     is_tox_target=True)
    models.TestTarget.objects.create(name='Oxycodone',
                                     is_tox_target=True)
    models.TestTarget.objects.create(name='Noroxycodone',
                                     is_tox_target=True)
    models.TestTarget.objects.create(name='Hydrocodone',
                                     is_tox_target=True)
    models.TestTarget.objects.create(name='Norhydrocodone',
                                     is_tox_target=True)
    models.TestTarget.objects.create(name='Dihydrocodeine',
                                     is_tox_target=True)
    models.TestTarget.objects.create(name='Hydromorphone',
                                     is_tox_target=True)
    models.TestTarget.objects.create(name='Dihydromorphine',
                                     is_tox_target=True)
    models.TestTarget.objects.create(name='Morphine',
                                     is_tox_target=True)
    models.TestTarget.objects.create(name='Normorphine',
                                     is_tox_target=True)
    models.TestTarget.objects.create(name='Codeine',
                                     is_tox_target=True)
    models.TestTarget.objects.create(name='Norcodeine',
                                     is_tox_target=True)
    models.TestTarget.objects.create(name='Amphetamine',
                                     is_tox_target=True)
    models.TestTarget.objects.create(name='Amobarbital',
                                     is_tox_target=True)
    models.TestTarget.objects.create(name='Butalbital',
                                     is_tox_target=True)
    models.TestTarget.objects.create(name='Pentobarbital',
                                     is_tox_target=True)
    models.TestTarget.objects.create(name='Phenobarbital',
                                     is_tox_target=True)
    models.TestTarget.objects.create(name='Secobarbital',
                                     is_tox_target=True)
    models.TestTarget.objects.create(name='7-Aminoclonazepam',
                                     is_tox_target=True)
    models.TestTarget.objects.create(name='7-Aminoflunitrazepam',
                                     is_tox_target=True)
    models.TestTarget.objects.create(name='Alprazolam',
                                     is_tox_target=True)
    models.TestTarget.objects.create(name='Hydroxyalprazolam',
                                     is_tox_target=True)
    models.TestTarget.objects.create(name='Lorazepam',
                                     is_tox_target=True)
    models.TestTarget.objects.create(name='Oxazepam',
                                     is_tox_target=True)
    models.TestTarget.objects.create(name='Temazepam',
                                     is_tox_target=True)
    models.TestTarget.objects.create(name='Diazepam',
                                     is_tox_target=True)
    models.TestTarget.objects.create(name='Nordiazepam',
                                     is_tox_target=True)
    models.TestTarget.objects.create(name='Buprenorphine',
                                     is_tox_target=True)
    models.TestTarget.objects.create(name='Norbuprenorphine',
                                     is_tox_target=True)
    models.TestTarget.objects.create(name='Ethyl Glucuronide Screen',
                                     is_tox_target=True)
    models.TestTarget.objects.create(name='Methadone',
                                     is_tox_target=True)
    models.TestTarget.objects.create(name='EDDP',
                                     is_tox_target=True)




if __name__ == '__main__':
    main()
    #fill_tox_test_targets()
