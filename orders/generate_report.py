import django
import os
import math
import tempfile
import pytz

from django.db.models import F, Func
from django.utils import timezone
from reportlab.pdfgen import canvas
from reportlab.platypus import Paragraph
from reportlab.lib.units import inch
from reportlab.lib.styles import ParagraphStyle, getSampleStyleSheet

from reportlab.pdfbase import pdfmetrics
from reportlab.pdfbase.ttfonts import TTFont
from textwrap import wrap

os.environ['DJANGO_SETTINGS_MODULE'] = 'main.settings'
django.setup()

from . import models
from clients import models as clients_models
from lab import models as lab_models
from lab_tenant import models as lab_tenant_models


BUCKET_NAME = 'dendi-lis'

styles = getSampleStyleSheet()
styleN = styles['Normal']
styleH = styles['Heading1']


class IsNull(Func):
    """
    to run SQL-level functions.
    Used to sort list of items by a column that has NULLs
    """
    template = '%(expressions)s IS NULL'


def generate_pdf_report(tenant_id, order_uuid, preliminary=False):
    tenant = clients_models.Client.objects.get(id=tenant_id)
    order = models.Order.objects.get(uuid=order_uuid)
    order_code = order.code
    account_name = order.account.name
    tests = order.tests.prefetch_related('result', 'test_type__test_method').all()
    test_methods = list(set([x.test_type.test_method for x in tests
                             if x.is_approved and x.test_type and not x.test_type.is_internal]))

    with tempfile.NamedTemporaryFile() as temp:
        # initialize pdf canvas
        c = canvas.Canvas(temp.name)
        pdfmetrics.registerFont(TTFont('SourceSansPro', 'lab/static/fonts/source-sans-pro/SourceSansPro-Regular.ttf'))
        pdfmetrics.registerFont(TTFont('SourceSansProItalic', 'lab/static/fonts/source-sans-pro/SourceSansPro-Italic.ttf'))
        pdfmetrics.registerFont(TTFont('SourceSansProBold', 'lab/static/fonts/source-sans-pro/SourceSansPro-Bold.ttf'))
        pdfmetrics.registerFont(
            TTFont('SourceSerifPro', 'lab/static/fonts/source-serif-pro/SourceSerifPro-Regular.ttf'))
        pdfmetrics.registerFont(
            TTFont('SourceSerifProBold', 'lab/static/fonts/source-serif-pro/SourceSerifPro-Bold.ttf'))
        c.setTitle("{} - {} - {}".format(tenant.name, account_name, order_code))

        for test_method in test_methods:
            c.translate(inch, inch)
            draw_report_template(tenant.id, order_uuid, c, test_method, preliminary=preliminary)
            draw_report_results(tenant.id, order_uuid, c, test_method)
            c.showPage()
            if tenant.name == 'Galaxy Diagnostics' and test_method.name != 'WB RUO':
                c.translate(inch, inch)
                draw_report_interpretation(tenant.id, c, test_method)
                c.showPage()

        c.save()
        # upload file after creation
        save_file_name = "reports/{}/{}/{}.pdf".format(tenant.name, order.uuid, timezone.now())
        # save method here is not Django ORM's default save
        # using main/storage_backends.py

        # if there's already a Report for the order, then the next report must be "amended"
        if models.Report.objects.filter(order=order, preliminary=False):
            report = models.Report(order=order, amended=True, preliminary=preliminary)
        else:
            report = models.Report(order=order, preliminary=preliminary)
        report.pdf_file.save(save_file_name, temp)

    return save_file_name


def draw_report_interpretation(tenant_id, canvas, test_method):
    """
    Create report interpretation instructions page
    :param request:
    :param canvas:
    :param test_method:
    :return:
    """
    tenant = clients_models.Client.objects.get(id=tenant_id)
    tenant_name = tenant.name

    # Top left logo
    if tenant.logo_file:
        canvas.drawImage(tenant.logo_file.url, -.5 * inch, 8.7 * inch, width=1.5 * inch, height=1.5 * inch,
                         mask=None)
    else:
        public_tenant = clients_models.Client.objects.get(schema_name='public')
        if public_tenant.logo_file:
            canvas.drawImage(public_tenant.logo_file.url, -.7 * inch, 8.7 * inch, width=1.5 * inch, height=1.5 * inch,
                             mask=None)

    # Tenant Name
    canvas.setFont("SourceSerifProBold", 24)
    tenant_name_split = tenant_name.split(' ')
    if len(tenant_name_split) > 1:
        canvas.drawString(1.1 * inch, 9.55 * inch, tenant_name_split[0])
        canvas.drawString(1.1 * inch, 9.15 * inch, ' '.join(tenant_name_split[1:]))
    else:
        canvas.drawString(1.1 * inch, 9.55 * inch, tenant_name)

    # Address
    canvas.setFont("SourceSansPro", 8)
    start_y = 9.7 * inch
    spacing = .15 * inch
    if tenant.address_1:
        canvas.drawRightString(6.7 * inch, start_y, tenant.address_1.upper(), charSpace=1.5)
        start_y -= spacing
    if tenant.address_2:
        canvas.drawRightString(6.7 * inch, start_y, tenant.address_2.upper(), charSpace=1.5)
        start_y -= spacing
    if tenant.phone_number:
        canvas.drawRightString(6.7 * inch, start_y, tenant.phone_number, charSpace=1.5)
        start_y -= spacing
    if tenant.fax_number:
        canvas.drawRightString(6.7 * inch, start_y, tenant.fax_number, charSpace=1.5)
        start_y -= spacing
    if tenant.website:
        canvas.drawRightString(6.7 * inch, start_y, tenant.website.upper(), charSpace=1.5)

    # Interpretation instructions
    start_y = 8.35 * inch
    text_start_y = 8.05 * inch
    start_x = -.39 * inch
    text_start_x = -.2 * inch
    spacing = .2 * inch
    line_width = 90
    canvas.setFont("SourceSansProBold", 12)
    if test_method.name == 'PCR':
        canvas.drawString(start_x, start_y, "PCR Test Interpretation", charSpace=.5)
        interpretation_texts = ["1. Not Detected (PCR) - Target pathogen DNA was not amplified by PCR from the "
                                "patient's blood. Failure to detect the target pathogen DNA does not rule out an "
                                "infection.",
                                "2. Detected (PCR)* - Identification of target pathogen DNA was established by DNA "
                                "sequence of the PCR product and by alignment using the most current GenBank database "
                                "in conjunction with the internal sequence database of Galaxy Diagnostics, Inc. ",
                                "3. Detected (PCR)* - Indeterminate (Sequence) - Target pathogen DNA was amplified by "
                                "PCR from the patient's blood. A positive PCR was confirmed by repeat PCR testing. "
                                "However, the PCR result could not be verified by DNA sequencing. Repeat testing prior "
                                "to or after initiation of antibiotic therapy can at times result identification of "
                                "the infecting pathogen.",
                                "4. In the event of a positive PCR result, DNA sequence information is provided by "
                                "GENEWIZ, a CLIA-certified laboratory located at 115 Corporate Boulevard, South "
                                "Plainfield, NJ 07080. Interpretation of the DNA sequence information is performed "
                                "by Galaxy Diagnostics, Inc. staff.",
                                "5. Indeterminate PCR*, Indeterminate Sequence - A positive PCR was obtained. "
                                "However, DNA sequencing was not successful and repeat PCR was negative.",
                                "6. Lack of detection by qPCR of target pathogen genus DNA may be due to variable "
                                "species-specific limits of detection.",
                                '7. The expected ("Reference") value in the normal population is target organism '
                                'genus/species "Not Detected."',
                                '8. RUO (Research Use Only) is noted in the "Reference" column when tests are '
                                'performed which have not been validated according to CLIA regulatory requirements.'
                               ]
        canvas.setFont("SourceSansPro", 12)
        for interpretation_text in interpretation_texts:
            wrapped_text = "\n".join(wrap(interpretation_text, line_width))
            text_object = canvas.beginText(text_start_x, text_start_y)
            text_object.textLines(wrapped_text)
            canvas.drawText(text_object)
            text_start_y -= spacing * math.ceil((len(interpretation_text) / line_width) + 2)
    elif test_method.name == 'ePCR':
        canvas.drawString(start_x, start_y, "Bartonella ePCR Test Interpretation", charSpace=.5)
        interpretation_texts = ["1. Not Detected (PCR) - Bartonella DNA was not amplified by PCR form the patient's "
                                "blood, serum, or Bartonella spp. BAPGM enrichment culture. Failure to detect "
                                "infection with a Bartonella spp. using Bartonella ePCR does not rule out a diagnosis "
                                "of bartonellosis.",
                                "2. Detected (PCR)* - Identification of target pathogen DNA was established by DNA "
                                "sequence of the PCR product and by alignment using the most current GenBank database "
                                "in conjunction with the internal sequence database of Galaxy Diagnostics, Inc. ",
                                "3. Detected (PCR)* - Indeterminate (Sequence) - Bartonella DNA was amplified by "
                                "PCR from the patient's blood, serum, or Bartonella spp. BAPGM enrichment culture. A "
                                "positive PCR was confirmed by repeat PCR testing. "
                                "However, the PCR result could not be verified by DNA sequencing. Repeat testing prior "
                                "to or after initiation of antibiotic therapy can at times result identification of "
                                "the infecting pathogen.",
                                "4. In the event of a positive PCR result, DNA sequence information is provided by "
                                "GENEWIZ, a CLIA-certified laboratory located at 115 Corporate Boulevard, South "
                                "Plainfield, NJ 07080. Interpretation of the DNA sequence information is performed "
                                "by Galaxy Diagnostics, Inc. staff.",
                                "5. Indeterminate PCR*, Indeterminate Sequence - A positive PCR was obtained. "
                                "However, DNA sequencing was not successful and repeat PCR was negative.",
                                "6. Lack of detection by qPCR of target pathogen genus DNA may be due to variable "
                                "species-specific limits of detection.",
                                '7. The expected ("Reference") value in the normal population is target organism '
                                'genus/species "Not Detected."',
                                '8. RUO (Research Use Only) is noted in the "Reference" column when tests are '
                                'performed which hav not been validated according to CLIA regulatory requirements.'
                               ]
        canvas.setFont("SourceSansPro", 12)
        for interpretation_text in interpretation_texts:
            wrapped_text = "\n".join(wrap(interpretation_text, line_width))
            text_object = canvas.beginText(text_start_x, text_start_y)
            text_object.textLines(wrapped_text)
            canvas.drawText(text_object)
            text_start_y -= spacing * math.ceil((len(interpretation_text) / line_width) + 2)
    elif test_method.name == 'IFA':
        canvas.drawString(start_x, start_y, "Bartonella IFA (IgG) Serology Test Interpretation", charSpace=.5)
        interpretation_texts = ["1. A titer to either Bartonella species greater than or equal to 1:64 indicates the "
                               "sera tested was seroreactive to the Bartonella species antigen, and the patient has "
                               "been exposed to or may presently be infected with a Bartonella species. IFA results "
                               "with a titer to Bartonella henselae or Bartonella quintana less than 1:64 indicates "
                               "the sera tested was non-reactive to the Bartonella species antigen.",
                               "2. Cross-reactivity between Bartonella henselae and Bartonella quintana has been "
                               "reported.",
                               "3. Indeterminate IFA - Unable to determine definitive reactivity status. Submission "
                               "of an additional sample for repeat testing is recommended.",
                               '4. The expected ("Reference") value in the normal population is "Non-reactive" to '
                               'Bartonella henselae or Bartonella quintana. Serological results should be '
                               'interpreted in the context of patient history, possible past exposure, other clinical '
                               'findings and laboratory results.',
                               "5. Research studies have shown that some bactermic patients do not develop antibodies.",
                               '6. RUO (Research Use Only) is noted in the "Reference" column when tests are '
                               'performed which have not been validated according to CLIA regulatory requirements.',
                               ]
        canvas.setFont("SourceSansPro", 12)
        for interpretation_text in interpretation_texts:
            wrapped_text = "\n".join(wrap(interpretation_text, line_width))
            text_object = canvas.beginText(text_start_x, text_start_y)
            text_object.textLines(wrapped_text)
            canvas.drawText(text_object)
            text_start_y -= spacing * math.ceil((len(interpretation_text) / line_width) + 2)
    elif test_method.name == 'ELISA':
        canvas.drawString(start_x, start_y, "Borrelia burgdorferi (Lyme) ELISA Test Interpretation", charSpace=.5)
        interpretation_texts = ["Negative: Patient's result is less than the internally established reference value. "
                                "This result indicates that the sample tested was non-reactive with Borrelia "
                                "burgdorferi whole cell sonicate and does not meet the threshold criteria for reflex "
                                "testing by Western blot.",
                                "Equivocal: Patient's result is equal to the internally established reference range. "
                                "This result indicates that the sample tested exhibits borderline reactivity with "
                                "Borrelia burgdorferi whole cell sonicate and does not meet the threshold criteria "
                                "for reflex testing by Western blot.",
                                "Positive: Patient's result is greater than the internally established reference "
                                "value. This result indicates that the sample tested exhibits reactivity with "
                                "Borrelia burgdorferi whole cell sonicate and does meet the threshold criteria for "
                                "reflex testing by Western blot.",
                                "Reference Values: derived from a panel of B. burgdorferi-negative, healthy donor"
                                " sera.",
                                "RUO (Research Use Only) is noted in the \"Reference\" column when tests are performed "
                                "which have not been validated according to CLIA regulatory requirements."]
        canvas.setFont("SourceSansPro", 12)
        for interpretation_text in interpretation_texts:
            wrapped_text = "\n".join(wrap(interpretation_text, line_width))
            text_object = canvas.beginText(text_start_x, text_start_y)
            text_object.textLines(wrapped_text)
            canvas.drawText(text_object)
            text_start_y -= spacing * math.ceil((len(interpretation_text) / line_width) + 2)
    elif test_method.name == 'WB':
        interpretation_texts = ["Positive: At least 2 out of the 3 following diagnostic bands (23, 39, 41 kDa) are "
                                "present at an intensity greater than or equal to band intensities present in "
                                "validated control samples. Sample does meet or exceeds the threshold for reactivity "
                                "with the Borrelia burgdorferi whole-cell lysate.",
                                "Negative: Less than 2 out of the 3 diagnostic bands are present at an intensity "
                                "greater than or equal to band intensities present in validated control samples. "
                                "Sample does not meet the threshold for reactivity with the Borrelia burgdorferi "
                                "whole-cell lysate.",
                                "Indeterminate: Reactivity status cannot be definitively determined. It is "
                                "recommended that a new sample be sent for re-testing.",
                                "RUO (Research Use Only) is noted in the \"Reference\" column when tests are "
                                "performed which have not been validated according to CLIA regulatory requirements.",]
        interpretation_texts2 = [
                                "Positive: At least 5 out of the 10 following diagnostic bands (18, 23, 28, 30, 39, "
                                "31, 35, 58, 66, 93 kDa) are present at an intensity greater than or equal to band "
                                "intensities present in validated control samples. Sample does meet or exceeds the "
                                "threshold for reactivity with the Borrelia burgdorferi whole-cell lysate.",
                                "Negative: Less than 5 out of the 10 diagnostic bands are present at an intensity "
                                "greater than or equal to band intensities present in validated control samples. "
                                "Sample does not meet threshold for reactivity with the Borrelia burgdorferi "
                                "whole-cell lysate.",
                                "Indeterminate: Reactivity status cannot be definitively determined. It is "
                                "recommended that a new sample be sent for re-testing.",
                                "RUO (Research Use Only) is noted in the \"Reference\" column when tests are "
                                "performed which have not been validated according to CLIA regulatory requirements.",]
        interpretation_texts3 = [
                                "Results for bands 31 and 34 kDa are available on a Research Use Only (RUO) basis. "
                                "Please contact Galaxy Diagnostics.",]
        interpretation_texts4 = [
                                "Positive: BOTH the IgM ELISA and Western blot, or BOTH the IgG ELISA and Western "
                                "blot are Positive (Positive/Positive), or the ELISA is Equivocal and the Western "
                                "blot is Positive (Equivocal/Positive). The patient has been exposed to Borrelia "
                                "burgdorferi and may have Lyme disease. The patient should consult with their "
                                "physician to review test results and discuss treatment options.",
                                "Negative: Two-tier tests results for IgM ELISA/Western blot or IgG ELISA/Western "
                                "blot are not Positive/Positive or Equivocal/Positive. The patient does not meet CDC "
                                "guidelines for serodiagnosis of Lyme disease. However, laboratory tests are only "
                                "one component utilized by physicians to diagnose and treat potential infections. "
                                "The patient should consult with their physician to discuss test results and other "
                                "options.",
                                "Serum samples from patients with Bartonella infections, Epstein-Barr viral "
                                "infections (mononucleosis), rheumatoid arthritis, or that have had other spirochetal "
                                "infections (e.g. periodontitis, syphillis) may exhibit cross-reactivity with B. "
                                "burgdorferi ELISA and Western blot tests and produce false positive results."]
        line_width = 110
        canvas.drawString(start_x, start_y, "Borrelia Burgdoferi IgM Western Blot Serology Test Interpretation",
                          charSpace=.5)
        canvas.setFont("SourceSansPro", 10)
        spacing = .18 * inch
        for interpretation_text in interpretation_texts:
            wrapped_text = "\n".join(wrap(interpretation_text, line_width))
            text_object = canvas.beginText(text_start_x, text_start_y)
            text_object.textLines(wrapped_text)
            canvas.drawText(text_object)
            text_start_y -= spacing * math.ceil((len(interpretation_text) / line_width) + 1)
        canvas.setFont("SourceSansProBold", 12)
        canvas.drawString(start_x, text_start_y, "Borrelia Burgdorferi IgG Western Blot Serology Test Interpretation",
                          charSpace=.5)
        text_start_y -= spacing * 1.5
        canvas.setFont("SourceSansPro", 10)
        for interpretation_text in interpretation_texts2:
            wrapped_text = "\n".join(wrap(interpretation_text, line_width))
            text_object = canvas.beginText(text_start_x, text_start_y)
            text_object.textLines(wrapped_text)
            canvas.drawText(text_object)
            text_start_y -= spacing * math.ceil((len(interpretation_text) / line_width) + 1)
        canvas.setFont("SourceSansProBold", 12)
        canvas.drawString(start_x, text_start_y, "Borrelia Burgdorferi Two-Tier Test Interpretation",
                          charSpace=.5)
        text_start_y -= spacing * 1.5
        canvas.setFont("SourceSansPro", 10)
        for interpretation_text in interpretation_texts3:
            wrapped_text = "\n".join(wrap(interpretation_text, line_width))
            text_object = canvas.beginText(text_start_x, text_start_y)
            text_object.textLines(wrapped_text)
            canvas.drawText(text_object)
            text_start_y -= spacing * math.ceil((len(interpretation_text) / line_width) + 1)
        canvas.setFont("SourceSansProBold", 12)
        canvas.drawString(start_x, text_start_y, "Borrelia Burgdorferi Two-Tier Test Interpretation",
                          charSpace=.5)
        text_start_y -= spacing * 1.5
        canvas.setFont("SourceSansPro", 10)
        for interpretation_text in interpretation_texts4:
            wrapped_text = "\n".join(wrap(interpretation_text, line_width))
            text_object = canvas.beginText(text_start_x, text_start_y)
            text_object.textLines(wrapped_text)
            canvas.drawText(text_object)
            text_start_y -= spacing * math.ceil((len(interpretation_text) / line_width) + 1)
    else:
        interpretation_texts = []


def draw_report_template(tenant_id, order_uuid, canvas, test_method, preliminary=False):
    # Set pdf title
    order = models.Order.objects.prefetch_related('patient__user', 'provider__user',
                                                  'account', 'patient', 'provider',
                                                  'results', 'results__user').get(uuid=order_uuid)
    order_code = order.code
    tenant = clients_models.Client.objects.get(id=tenant_id)
    tenant_name = tenant.name
    account_name = order.account.name

    # default logic for test_performed by
    tests_performed_by = set([x.user for x in order.results.filter(result__isnull=False)])
    tests_performed_by_names = []
    for test_user in tests_performed_by:
        if test_user.first_name and test_user.last_name:
            tests_performed_by_names.append(str(test_user))
        else:
            tests_performed_by_names.append(test_user.email)
    tests_performed_by = ', '.join(tests_performed_by_names)

    # If test user is selected in order details
    test_user_id = order.test_performed_by.get(test_method.name, None)
    if test_user_id:
        test_user = lab_tenant_models.User.all_objects.get(id=test_user_id)
        tests_performed_by = str(test_user)

    # Top left logo
    if tenant.logo_file:
        canvas.drawImage(tenant.logo_file.url, -.5 * inch, 8.7*inch, width=1.5 * inch, height=1.5 * inch, mask=None)
    else:
        public_tenant = clients_models.Client.objects.get(schema_name='public')
        if public_tenant.logo_file:
            canvas.drawImage(public_tenant.logo_file.url, -.7 * inch, 8.7*inch, width=1.5 * inch, height=1.5 * inch, mask=None)

    if preliminary:
        canvas.drawImage('https://s3.amazonaws.com/dendi-lis/media/public/draft.png',
                         1 * inch, 1.5 * inch, width=4 * inch, height= 2*inch, mask=None)

    # Tenant Name
    canvas.setFont("SourceSerifProBold", 24)
    tenant_name_split = tenant_name.split(' ')
    if len(tenant_name_split) > 1:
        canvas.drawString(1.1 * inch, 9.55*inch, tenant_name_split[0])
        canvas.drawString(1.1 * inch, 9.15 * inch, ' '.join(tenant_name_split[1:]))
    else:
        canvas.drawString(1.1 * inch, 9.55 * inch, tenant_name)

    # Address
    canvas.setFont("SourceSansPro", 8)
    start_y = 9.7*inch
    spacing = .15 * inch
    if tenant.address_1:
        canvas.drawRightString(6.7 * inch, start_y, tenant.address_1.upper(), charSpace=1.5)
        start_y -= spacing
    if tenant.address_2:
        canvas.drawRightString(6.7 * inch, start_y, tenant.address_2.upper(), charSpace=1.5)
        start_y -= spacing
    if tenant.phone_number:
        canvas.drawRightString(6.7 * inch, start_y, 'P: {}'.format(tenant.phone_number), charSpace=1.5)
        start_y -= spacing
    if tenant.fax_number:
        canvas.drawRightString(6.7 * inch, start_y, 'F: {}'.format(tenant.fax_number), charSpace=1.5)
        start_y -= spacing
    if tenant.website:
        canvas.drawRightString(6.7 * inch, start_y, tenant.website.upper(), charSpace=1.5)

    # Order Info Labels
    canvas.setFont("SourceSansProBold", 10)
    account_name_work_list = account_name.split(' ')
    # Left column Label
    start_y = 8.35 * inch
    start_x = -.39 * inch
    spacing = .27 * inch
    canvas.drawString(start_x, start_y, "PATIENT (LAST, FIRST)", charSpace=.5)
    start_y -= spacing
    canvas.drawString(start_x, start_y, "DATE OF BIRTH", charSpace=.5)
    start_y -= spacing
    canvas.drawString(start_x, start_y, "CLINIC", charSpace=.5)
    start_y -= spacing
    if len(account_name_work_list) > 4:
        start_y -= spacing * 2
    elif len(account_name_work_list) > 2:
        start_y -= spacing
    canvas.drawString(start_x, start_y, "PHYSICIAN", charSpace=.5)
    start_y -= spacing

    # Left column Answer
    canvas.setFont("SourceSansPro", 10)
    start_y = 8.35 * inch
    start_x = 1.4 * inch
    spacing = .27 * inch
    canvas.drawString(start_x, start_y, "{}, {}".format(order.patient.user.last_name,
                                                        order.patient.user.first_name), charSpace=.5)
    start_y -= spacing
    canvas.drawString(start_x, start_y, "{}".format(order.patient.birth_date), charSpace=.5)
    start_y -= spacing
    if len(account_name_work_list) > 4:
        canvas.drawString(start_x, start_y, ' '.join(account_name_work_list[:2]), charSpace=.5)
        start_y -= spacing
        canvas.drawString(start_x, start_y, ' '.join(account_name_work_list[2:4]), charSpace=.5)
        start_y -= spacing
        canvas.drawString(start_x, start_y, ' '.join(account_name_work_list[4:]))
    elif len(account_name_work_list) > 2:
        canvas.drawString(start_x, start_y, ' '.join(account_name_work_list[:2]), charSpace=.5)
        start_y -= spacing
        canvas.drawString(start_x, start_y, ' '.join(account_name_work_list[2:]), charSpace=.5)
    else:
        canvas.drawString(start_x, start_y, account_name, charSpace=.5)
    start_y -= spacing
    if order.provider:
        canvas.drawString(start_x, start_y, "{}, {}".format(order.provider.user.last_name,
                                                            order.provider.user.first_name), charSpace=.5)
    start_y -= spacing
    left_y = start_y

    # right column labels
    canvas.setFont("SourceSansProBold", 10)
    start_y = 8.35 * inch
    start_x = 3.25 * inch
    spacing = .27 * inch
    canvas.drawString(start_x, start_y, "ORDER RECEIVED", charSpace=.5)
    start_y -= spacing
    canvas.drawString(start_x, start_y, "ORDER REPORTED", charSpace=.5)
    start_y -= spacing
    canvas.drawString(start_x, start_y, "ORDER NUMBER", charSpace=.5)
    start_y -= spacing
    if tenant.clia_number:
        canvas.drawString(start_x, start_y, "CLIA #", charSpace=.5)
        start_y -= spacing
    if tenant.clia_director:
        canvas.drawString(start_x, start_y, "CLIA DIRECTOR", charSpace=.5)
        start_y -= spacing
    canvas.drawString(start_x, start_y, "TESTS PERFORMED BY", charSpace=.5)
    start_y -= spacing
    # right column answer
    canvas.setFont("SourceSansPro", 10)
    start_y = 8.35 * inch
    start_x = 4.8 * inch
    spacing = .27 * inch
    canvas.drawString(start_x, start_y, order.received_date.strftime('%Y-%m-%d'), charSpace=.5)
    start_y -= spacing
    start_y -= spacing
    canvas.drawString(start_x, start_y, order_code, charSpace=.5)
    start_y -= spacing
    if tenant.clia_number:
        canvas.drawString(start_x, start_y, tenant.clia_number, charSpace=.5)
        start_y -= spacing
    if tenant.clia_director:
        canvas.drawString(start_x, start_y, tenant.clia_director, charSpace=.5)
        start_y -= spacing
    canvas.drawString(start_x, start_y, tests_performed_by, charSpace=.5)
    start_y -= spacing
    right_y = start_y

    second_bar_y = min(left_y, right_y)

    # Header and footer lines
    canvas.setStrokeColorRGB(0, 0, 0)
    canvas.setLineWidth(4)
    canvas.line(-.43*inch, 8.66*inch, 6.7*inch, 8.66*inch)
    canvas.line(-.43 * inch, .64 * inch, 6.7 * inch, .64 * inch)
    canvas.line(-.43 * inch, second_bar_y, 6.7 * inch, second_bar_y)


def draw_report_results(tenant_id, order_uuid, canvas, test_method):
    order = models.Order.objects.get(uuid=order_uuid)
    tenant = clients_models.Client.objects.get(id=tenant_id)

    start_y = 6.35 * inch
    start_x = -.39 * inch
    spacing = .27 * inch

    tests = order.tests.filter(test_type__test_method=test_method,
                               result__result__isnull=False,
                               is_approved=True,
                               test_type__is_internal=False). \
        annotate(days_incubated_isnull=IsNull('result__days_incubated')). \
        order_by('sample__collection_date', 'sample__code', '-days_incubated_isnull', 'result__days_incubated'). \
        prefetch_related('sample', 'test_type', 'result', 'test_type__test_target')


    # Report date: show the initial report date per test type
    canvas.setFont("SourceSansPro", 10)
    report_date_x, report_date_y = 4.8 * inch, 8.08 * inch
    report_dates = [x.initial_report_date for x in tests if x.initial_report_date]

    est = pytz.timezone('US/Eastern')

    if report_dates:
        report_date = max(report_dates).astimezone(est)
        canvas.drawString(report_date_x, report_date_y, report_date.strftime('%Y-%m-%d'), charSpace=.5)

    canvas.setFont("SourceSansProBold", 14)
    if test_method.name == 'Waived':
        canvas.drawString(start_x, start_y, "Results", charSpace=1)
        start_y -= .75 * inch
        # Result labels
        canvas.setFont("SourceSansProBold", 10)
        headers = ['TARGET', 'COLLECTION', 'SAMPLE', 'TYPE', 'RESULT']
        column_xs = [-.39 * inch, 1.4 * inch, 2.7 * inch, 3.4 * inch, 4.2 * inch]
        for header, column_x in zip(headers, column_xs):
            canvas.drawString(column_x, start_y, header.format(test_method.name).upper(), charSpace=1)
        # results
        canvas.setFont("SourceSansPro", 10)
        start_y -= spacing
        for test in tests:
            if test.clia_test_type.analyte_name:
                canvas.drawString(column_xs[0], start_y, test.clia_test_type.analyte_name[:25])
            canvas.drawString(column_xs[1], start_y, test.sample.collection_date.strftime("%Y-%m-%d"))
            sample_code_abbr = '-'.join(test.sample.code.split('-')[-2:])
            if test.result.get().days_incubated:
                canvas.drawString(column_xs[2], start_y, sample_code_abbr + 'x' +
                                  str(test.result.get().days_incubated))
            else:
                canvas.drawString(column_xs[2], start_y, sample_code_abbr)
            try:
                canvas.drawString(column_xs[3], start_y, test.sample.clia_sample_type.name[:7])
            except AttributeError:
                pass
            canvas.drawString(column_xs[4], start_y, test.result.get().result_label)
            start_y -= spacing
    elif test_method.name == 'General Chemistry':
        canvas.drawString(start_x, start_y, "Results", charSpace=1)
        start_y -= .75 * inch
        # Result labels
        canvas.setFont("SourceSansProBold", 10)
        headers = ['TARGET', 'COLLECTION', 'SAMPLE', 'TYPE', 'RESULT']
        column_xs = [-.39 * inch, 1.4 * inch, 2.7 * inch, 3.4 * inch, 4.2 * inch]
        for header, column_x in zip(headers, column_xs):
            canvas.drawString(column_x, start_y, header.format(test_method.name).upper(), charSpace=1)
        # results
        canvas.setFont("SourceSansPro", 10)
        start_y -= spacing
        for test in tests:
            if test.clia_test_type.analyte_name:
                canvas.drawString(column_xs[0], start_y, test.clia_test_type.analyte_name[:25])
            canvas.drawString(column_xs[1], start_y, test.sample.collection_date.strftime("%Y-%m-%d"))
            sample_code_abbr = '-'.join(test.sample.code.split('-')[-2:])
            if test.result.get().days_incubated:
                canvas.drawString(column_xs[2], start_y, sample_code_abbr + 'x' +
                                  str(test.result.get().days_incubated))
            else:
                canvas.drawString(column_xs[2], start_y, sample_code_abbr)
            try:
                canvas.drawString(column_xs[3], start_y, test.sample.clia_sample_type.name[:7])
            except AttributeError:
                pass
            canvas.drawString(column_xs[4], start_y, test.result.get().result_label)
            start_y -= spacing
    elif test_method.name == 'PCR':
        # Title
        if tenant.pcr_report_title:
            canvas.drawString(start_x, start_y, tenant.pcr_report_title, charSpace=1)
        else:
            canvas.drawString(start_x, start_y, "{} Results".format(test_method.name).upper(), charSpace=1)
        start_y -= .75 * inch
        # Result labels
        canvas.setFont("SourceSansProBold", 10)
        headers = ['TARGET', 'COLLECTION', 'SAMPLE', 'TYPE', 'RESULT', 'DNA SEQUENCE', 'REFERENCE']
        column_xs = [-.39 * inch, 1.1 * inch, 2.2 * inch, 3 * inch, 3.6 * inch, 4.6 * inch, 5.9 * inch]
        for header, column_x in zip(headers, column_xs):
            canvas.drawString(column_x, start_y, header.format(test_method.name).upper(), charSpace=1)
        # results
        canvas.setFont("SourceSansPro", 10)
        start_y -= spacing
        for test in tests:
            if test.test_type.test_target:
                canvas.drawString(column_xs[0], start_y, test.test_type.test_target.name)
            canvas.drawString(column_xs[1], start_y, test.sample.collection_date.strftime("%Y-%m-%d"))
            sample_code_abbr = '-'.join(test.sample.code.split('-')[-2:])
            if test.result.get().days_incubated:
                canvas.drawString(column_xs[2], start_y, sample_code_abbr + 'x' +
                                  str(test.result.get().days_incubated))
            else:
                canvas.drawString(column_xs[2], start_y, sample_code_abbr)
            try:
                canvas.drawString(column_xs[3], start_y, test.sample.sample_type.name)
            except AttributeError:
                canvas.drawString(column_xs[3], start_y, test.sample.clia_sample_type.name[:7])
            canvas.drawString(column_xs[4], start_y, test.result.get().result_label)
            if test.result.get().result_info_label:
                canvas.drawString(column_xs[5], start_y, test.result.get().result_info_label)
            canvas.drawString(column_xs[6], start_y, test.result.get().reference_label)
            start_y -= spacing
    elif test_method.name == 'ePCR':
        # Title
        if tenant.epcr_report_title:
            canvas.drawString(start_x, start_y, tenant.epcr_report_title, charSpace=1)
        else:
            canvas.drawString(start_x, start_y, "{} Results".format(test_method.name).upper(), charSpace=1)

        if len(tests) > 13:
            start_y -= .25 * inch
        else:
            start_y -= .75 * inch

        # Result labels
        canvas.setFont("SourceSansProBold", 10)
        headers = ['TARGET', 'COLLECTION', 'SAMPLE', 'TYPE', 'RESULT', 'DNA SEQUENCE', 'REFERENCE']
        column_xs = [-.39 * inch, 1.1 * inch, 2.15 * inch, 2.8 * inch, 3.7 * inch, 4.6 * inch, 5.85 * inch]
        for header, column_x in zip(headers, column_xs):
            canvas.drawString(column_x, start_y, header.format(test_method.name).upper(), charSpace=1)
        # results
        canvas.setFont("SourceSansPro", 10)
        start_y -= spacing
        for test in tests:
            if test.test_type.test_target:
                canvas.drawString(column_xs[0], start_y, test.test_type.test_target.name)
            canvas.drawString(column_xs[1], start_y, test.sample.collection_date.strftime("%Y-%m-%d"))
            sample_code_abbr = '-'.join(test.sample.code.split('-')[-2:])
            if test.result.get().days_incubated:
                canvas.drawString(column_xs[2], start_y, sample_code_abbr + 'x' +
                                  str(test.result.get().days_incubated))
            else:
                canvas.drawString(column_xs[2], start_y, sample_code_abbr)
            try:
                canvas.drawString(column_xs[3], start_y, test.sample.sample_type.name)
            except AttributeError:
                canvas.drawString(column_xs[3], start_y, test.sample.clia_sample_type.name[:7])
            canvas.drawString(column_xs[4], start_y, test.result.get().result_label)
            if test.result.get().result_info_label:
                canvas.drawString(column_xs[5], start_y, test.result.get().result_info_label)
            canvas.drawString(column_xs[6], start_y, test.result.get().reference_label)
            start_y -= spacing
    elif test_method.name == 'IFA':
        # Title
        if tenant.ifa_report_title:
            canvas.drawString(start_x, start_y, tenant.ifa_report_title, charSpace=1)
        else:
            canvas.drawString(start_x, start_y, "{} Results".format(test_method.name).upper(), charSpace=1)
        start_y -= .75 * inch
        # Result labels
        canvas.setFont("SourceSansProBold", 10)
        headers = ['TARGET', 'COLLECTION', 'SAMPLE', 'TYPE', 'RESULT', 'TITER', 'REFERENCE']
        column_xs = [-.39 * inch, 1 * inch, 2.05 * inch, 2.7 * inch, 3.75 * inch, 4.75 * inch, 5.9 * inch]
        for header, column_x in zip(headers, column_xs):
            canvas.drawString(column_x, start_y, header.format(test_method.name).upper(), charSpace=1)
        # results
        canvas.setFont("SourceSansPro", 10)
        start_y -= spacing
        for test in tests:
            if test.test_type.test_target:
                canvas.drawString(column_xs[0], start_y, test.test_type.test_target.name)
            canvas.drawString(column_xs[1], start_y, test.sample.collection_date.strftime("%Y-%m-%d"))
            sample_code_abbr = '-'.join(test.sample.code.split('-')[-2:])
            if test.result.get().days_incubated:
                canvas.drawString(column_xs[2], start_y, sample_code_abbr + 'x' +
                                  str(test.result.get().days_incubated))
            else:
                canvas.drawString(column_xs[2], start_y, sample_code_abbr)
            try:
                canvas.drawString(column_xs[3], start_y, test.sample.sample_type.name)
            except AttributeError:
                canvas.drawString(column_xs[3], start_y, test.sample.clia_sample_type.name[:7])
            canvas.drawString(column_xs[4], start_y, test.result.get().result_label)
            if test.result.get().result_info_label:
                if test.result.get().result_info_label_prefix:
                    canvas.drawString(column_xs[5], start_y, test.result.get().result_info_label_prefix +
                                      test.result.get().result_info_label)
                else:
                    canvas.drawString(column_xs[5], start_y, test.result.get().result_info_label)
            canvas.drawString(column_xs[6], start_y, test.result.get().reference_label)
            start_y -= spacing
    elif test_method.name == 'ELISA':
        # Title
        if tenant.elisa_report_title:
            canvas.drawString(start_x, start_y, tenant.elisa_report_title, charSpace=1)
        else:
            canvas.drawString(start_x, start_y, "{} Results".format(test_method.name).upper(), charSpace=1)
        start_y -= .75 * inch
        # Result labels
        canvas.setFont("SourceSansProBold", 10)
        headers = ['ISOTYPE', 'TARGET', 'COLLECTION', 'SAMPLE', 'TYPE', 'RESULT', 'REFERENCE']
        column_xs = [-.39 * inch, .5 * inch, 1.95 * inch, 3.2 * inch, 4.1 * inch, 4.9 * inch, 5.75 * inch]
        for header, column_x in zip(headers, column_xs):
            canvas.drawString(column_x, start_y, header.format(test_method.name).upper(), charSpace=1)
        # results
        canvas.setFont("SourceSansPro", 10)
        start_y -= spacing
        for test in tests:
            if test.test_type.isotype:
                canvas.drawString(column_xs[0], start_y, test.test_type.isotype)
            if test.test_type.test_target:
                canvas.drawString(column_xs[1], start_y, test.test_type.test_target.name)
            canvas.drawString(column_xs[2], start_y, test.sample.collection_date.strftime("%Y-%m-%d"))
            sample_code_abbr = '-'.join(test.sample.code.split('-')[-2:])
            if test.result.get().days_incubated:
                canvas.drawString(column_xs[3], start_y, sample_code_abbr + 'x' +
                                  str(test.result.get().days_incubated))
            else:
                canvas.drawString(column_xs[3], start_y, sample_code_abbr)
            try:
                canvas.drawString(column_xs[4], start_y, test.sample.sample_type.name)
            except AttributeError:
                canvas.drawString(column_xs[4], start_y, test.sample.clia_sample_type.name[:7])
            canvas.drawString(column_xs[5], start_y, test.result.get().result_label)
            canvas.drawString(column_xs[6], start_y, test.result.get().reference_label)
            start_y -= spacing
    elif test_method.name == 'WB':
        # Title
        if tenant.wb_report_title:
            canvas.drawString(start_x, start_y, tenant.wb_report_title, charSpace=1)
        else:
            canvas.drawString(start_x, start_y, "{} Results".format(test_method.name).upper(), charSpace=1)
        start_y -= .75 * inch
        # Result labels
        canvas.setFont("SourceSansProBold", 10)
        headers = ['ISOTYPE', 'TARGET', 'COLLECTION', 'SAMPLE', 'TYPE', 'RESULT', 'REFERENCE']
        column_xs = [-.39 * inch, .6 * inch, 2 * inch, 3.15 * inch, 4.1 * inch, 4.8 * inch, 5.9 * inch]
        for header, column_x in zip(headers, column_xs):
            canvas.drawString(column_x, start_y, header.format(test_method.name).upper(), charSpace=1)
        # results
        canvas.setFont("SourceSansPro", 10)
        start_y -= spacing
        for test in tests:
            if test.test_type.isotype:
                canvas.drawString(column_xs[0], start_y, test.test_type.isotype)
            if test.test_type.test_target:
                canvas.drawString(column_xs[1], start_y, test.test_type.test_target.name)
            canvas.drawString(column_xs[2], start_y, test.sample.collection_date.strftime("%Y-%m-%d"))
            sample_code_abbr = '-'.join(test.sample.code.split('-')[-2:])
            if test.result.get().days_incubated:
                canvas.drawString(column_xs[3], start_y, sample_code_abbr + 'x' +
                                  str(test.result.get().days_incubated))
            else:
                canvas.drawString(column_xs[3], start_y, sample_code_abbr)
            try:
                canvas.drawString(column_xs[4], start_y, test.sample.sample_type.name)
            except AttributeError:
                canvas.drawString(column_xs[4], start_y, test.sample.clia_sample_type.name[:7])
            canvas.drawString(column_xs[5], start_y, test.result.get().result_label)
            canvas.drawString(column_xs[6], start_y, test.result.get().reference_label)
            start_y -= spacing

            if test.result.get().result_info_label:
                canvas.setFont("SourceSansProBold", 10)
                canvas.drawString(3.0 * inch, start_y, 'DETECTED BANDS:', charSpace=1)
                canvas.setFont("SourceSansPro", 10)
                canvas.drawString(4.5 * inch, start_y, '{}'.format(test.result.get().result_info_label))
                start_y -= spacing * 1.3
    elif test_method.name == 'WB RUO':
        # Title
        if tenant.wb_report_title:
            canvas.drawString(start_x, start_y, tenant.wb_report_title, charSpace=1)
        else:
            canvas.drawString(start_x, start_y, "{} Results".format(test_method.name).upper(), charSpace=1)
        start_y -= .3 * inch
        canvas.drawString(start_x, start_y, "RESEARCH USE ONLY".format(test_method.name).upper(), charSpace=1)
        start_y -= .6 * inch
        # Result labels
        canvas.setFont("SourceSansProBold", 10)
        headers = ['ISOTYPE', 'TARGET', 'SAMPLE', 'COLLECTION', 'TYPE', 'BAND', 'RESULT', 'REFERENCE']
        column_xs = [-.39 * inch, .35 * inch, 1.75 * inch, 2.55 * inch, 3.6 * inch, 4.25 * inch, 5. * inch, 5.85 * inch]
        for header, column_x in zip(headers, column_xs):
            canvas.drawString(column_x, start_y, header.format(test_method.name).upper(), charSpace=1)
        # results
        canvas.setFont("SourceSansPro", 10)
        start_y -= spacing
        isotypes = sorted(list(set([x.test_type.isotype for x in tests])))
        for isotype in isotypes:
            isotype_tests = tests.filter(test_type__isotype=isotype)
            initial = True
            for test in isotype_tests:
                if initial:
                    if test.test_type.isotype:
                        canvas.setFont("SourceSansProBold", 10)
                        canvas.drawString(column_xs[0], start_y, test.test_type.isotype)
                        canvas.setFont("SourceSansPro", 10)
                    if test.test_type.test_target:
                        canvas.drawString(column_xs[1], start_y, test.test_type.test_target.name)
                    sample_code_abbr = '-'.join(test.sample.code.split('-')[-2:])
                    if test.result.get().days_incubated:
                        canvas.drawString(column_xs[2], start_y, sample_code_abbr + 'x' +
                                          str(test.result.get().days_incubated))
                    else:
                        canvas.drawString(column_xs[2], start_y, sample_code_abbr)
                    canvas.drawString(column_xs[3], start_y, test.sample.collection_date.strftime("%Y-%m-%d"))
                    try:
                        canvas.drawString(column_xs[4], start_y, test.sample.sample_type.name)
                    except AttributeError:
                        canvas.drawString(column_xs[4], start_y, test.sample.clia_sample_type.name[:7])
                if test.test_type.band:
                    canvas.drawString(column_xs[5], start_y, test.test_type.band)
                canvas.drawString(column_xs[6], start_y, test.result.get().result_label)
                canvas.drawString(column_xs[7], start_y, test.result.get().reference_label)
                start_y -= spacing
                initial = False
            start_y -= 2 * spacing

        # galaxy wb ruo disclaimer
        if tenant.schema_name == 'galaxy':
            canvas.setFont("SourceSansProBold", 10)
            disclaimer = "These results are for research use only. The clinical utility of the 31 kDa and 34 kDa bands has not been determined by Galaxy Diagnostics Inc."
            disclaimer_2 = "Serum samples from patients with Bartonella infections, Epstein-Barr viral infections (mononucleosis), rheumatoid arthritis, or that have had other spirochetal infections (e.g. periodontitis, syphilis) may exhibit cross-reactivity with B. burgdorferi ELISA or Western blot tests and produce false positive results."
            start_y = 2.25 * inch
            start_x = -.25 * inch
            spacing = .27 * inch
            canvas.drawString(start_x, start_y, 'Research Use Only Disclaimer: ')
            start_y -= spacing
            canvas.setFont("SourceSansPro", 10)
            line_width = 115
            wrapped_text = "\n".join(wrap(disclaimer, line_width))
            text_object = canvas.beginText(start_x, start_y)
            text_object.textLines(wrapped_text)
            canvas.drawText(text_object)
            start_y -= spacing * 2
            wrapped_text_2 = "\n".join(wrap(disclaimer_2, line_width))
            text_object_2 = canvas.beginText(start_x, start_y)
            text_object_2.textLines(wrapped_text_2)
            canvas.drawText(text_object_2)

    elif test_method.name == 'LC-MS':
        # Title
        if tenant.lcms_report_title:
            canvas.drawString(start_x, start_y, tenant.lcms_report_title, charSpace=1)
        else:
            canvas.drawString(start_x, start_y, "{} Results".format(test_method.name).upper(), charSpace=1)
        start_y -= .75 * inch
        # Result labels
        canvas.setFont("SourceSansProBold", 10)
        headers = ['TARGET', 'COLLECTION', 'SAMPLE', 'TYPE', 'RESULT', 'CONCENTRATION', 'REFERENCE']
        column_xs = [-.39 * inch, .8 * inch, 1.85 * inch, 3.1 * inch, 3.7 * inch, 4.4 * inch, 5.75 * inch]
        for header, column_x in zip(headers, column_xs):
            canvas.drawString(column_x, start_y, header.format(test_method.name).upper(), charSpace=1)
        # Second row
        start_y -= spacing
        canvas.drawString(4.7 * inch, start_y, 'ng/mL', charSpace=1)
        # results
        canvas.setFont("SourceSansPro", 10)
        start_y -= spacing
        # center concentration value
        column_xs[5] = 4.8 * inch
        for test in tests:
            if test.test_type.test_target:
                canvas.drawString(column_xs[0], start_y, test.test_type.test_target.name)
            canvas.drawString(column_xs[1], start_y, test.sample.collection_date.strftime("%Y-%m-%d"))
            sample_code_abbr = '-'.join(test.sample.code.split('-')[-2:])
            if test.result.get().days_incubated:
                canvas.drawString(column_xs[2], start_y, sample_code_abbr + 'x' +
                                  str(test.result.get().days_incubated))
            else:
                canvas.drawString(column_xs[2], start_y, sample_code_abbr)
            try:
                canvas.drawString(column_xs[3], start_y, test.sample.sample_type.name)
            except AttributeError:
                canvas.drawString(column_xs[3], start_y, test.sample.clia_sample_type.name[:7])
            canvas.drawString(column_xs[4], start_y, test.result.get().result_label)
            if test.result.get().result_info_label:
                canvas.drawString(column_xs[5], start_y, test.result.get().result_info_label)
            canvas.drawString(column_xs[6], start_y, test.result.get().reference_label)
            start_y -= spacing
    else:
        # Title
        canvas.drawString(start_x, start_y, "{} Results".format(test_method.name).upper(), charSpace=1)
        start_y -= .75 * inch
        # Result labels
        canvas.setFont("SourceSansProBold", 10)
        if test_method.cutoff_field:
            headers = ['TARGET', 'COLLECTION', 'SAMPLE', 'TYPE', 'RESULT', test_method.cutoff_field.upper()]
            column_xs = [-.39 * inch, 1.7 * inch, 2.75 * inch, 3.45 * inch, 4.45 * inch, 5.15 * inch]
        else:
            headers = ['TARGET', 'COLLECTION', 'SAMPLE', 'TYPE', 'RESULT']
            column_xs = [-.39 * inch, 1.7 * inch, 2.75 * inch, 3.45 * inch, 4.45 * inch]
        for header, column_x in zip(headers, column_xs):
            canvas.drawString(column_x, start_y, header.format(test_method.name).upper(), charSpace=1)
        # results
        canvas.setFont("SourceSansPro", 10)
        start_y -= spacing
        for test in tests:
            if test.test_type.test_target:
                canvas.drawString(column_xs[0], start_y, test.test_type.test_target.name)
            canvas.drawString(column_xs[1], start_y, test.sample.collection_date.strftime("%Y-%m-%d"))
            sample_code_abbr = '-'.join(test.sample.code.split('-')[-2:])
            if test.result.get().days_incubated:
                canvas.drawString(column_xs[2], start_y, sample_code_abbr + 'x' +
                                  str(test.result.get().days_incubated))
            else:
                canvas.drawString(column_xs[2], start_y, sample_code_abbr)
            try:
                canvas.drawString(column_xs[3], start_y, test.sample.sample_type.name)
            except AttributeError:
                canvas.drawString(column_xs[3], start_y, test.sample.clia_sample_type.name[:7])
            canvas.drawString(column_xs[4], start_y, test.result.get().result_label)
            if test.result.get().result_info_label:
                if test.result.get().result_info_label_prefix:
                    canvas.drawString(column_xs[5], start_y, test.result.get().result_info_label_prefix +
                                      test.result.get().result_info_label)
                else:
                    canvas.drawString(column_xs[5], start_y, test.result.get().result_info_label)
            start_y -= spacing

    # notes
    if order.notes:
        canvas.setFont("SourceSansProBold", 10)
        start_y = 1.75 * inch
        start_x = -.25 * inch
        spacing = .27 * inch
        canvas.drawString(start_x, start_y, 'Notes:')
        start_y -= spacing
        canvas.setFont("SourceSansPro", 10)
        line_width = 115
        wrapped_text = "\n".join(wrap(order.notes, line_width))
        text_object = canvas.beginText(start_x, start_y)
        text_object.textLines(wrapped_text)
        canvas.drawText(text_object)

    # footer
    if models.ReportPDFFooter.objects.filter(test_method=test_method).exists():
        footer = models.ReportPDFFooter.objects.get(test_method=test_method)
        canvas.setFont("SourceSansPro", 9)
        line_width = 120
        text_start_x = -.43 * inch
        text_start_y = .4 * inch
        wrapped_text = "\n".join(wrap(footer.footer_text, line_width))
        text_object = canvas.beginText(text_start_x, text_start_y)
        text_object.textLines(wrapped_text)
        canvas.drawText(text_object)
