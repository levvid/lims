function createSampleTypeTable(sample_types, table) {
  sample_types.forEach(element => {
    element.name.toString = () => element.name.text;
  });

  if (!table) {
    const hotElement = document.querySelector('#sample-type-table');
    const hotSettings = {
      data: sample_types,
      columns: [
        {
          data: 'name',
          editor: false,
          renderer: linkRenderer,
        },
      ],
      stretchH: 'all',
      //width: 806,
      height: 700,
      autoColumnSize: true,
      autoWrapRow: true,
      manualRowResize: false,
      manualColumnResize: false,
      columnSorting: true,
      sortIndicator: true,
      rowHeaders: false,
      colHeaders: true,
      nestedHeaders: [
        [
          'Name',
        ],
      ],
      manualRowMove: false,
      manualColumnMove: false,
      contextMenu: false,
      filters: true,
      dropdownMenu: ['filter_by_condition', 'filter_action_bar'],
      afterGetColHeader: function(col, TH) {
        TH.className = 'center';
      },
    };

    return new Handsontable(hotElement, hotSettings);
  } else {
    // load new data without replacing old table
    table.getPlugin('filters').clearConditions();
    table.loadData(accounts);
    return table;
  }
}
