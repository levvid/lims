# Generated by Django 2.2.2 on 2019-10-18 18:39

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('patients', '0036_auto_20191014_1909'),
    ]

    operations = [
        migrations.AlterField(
            model_name='payer',
            name='payer_code',
            field=models.CharField(blank=True, max_length=30),
        ),
    ]
