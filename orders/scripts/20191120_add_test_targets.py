import django

import logging
import os
import sys

from django.db import transaction

os.environ['DJANGO_SETTINGS_MODULE'] = 'main.settings'
django.setup()

from lab import models as lab_models


@transaction.atomic
def main():
    """
    Add TestTargets from Lance email from Nov 15th
    :return:
    """
    lab_models.TestTarget.objects.update_or_create(name='Amitryptyline',
                                                   defaults={'is_tox_target': True, 'is_medication': True})
    lab_models.TestTarget.objects.update_or_create(name='Cyclobenzaprine',
                                                   defaults={'common_name': ['Flexeril', 'Amrix', 'Fexmid'],
                                                             'category': 'Relaxants/Sleep Aids',
                                                             'is_tox_target': True,
                                                             'is_medication': True})
    lab_models.TestTarget.objects.update_or_create(name='Amobarbital',
                                                   defaults={'common_name': ['Amytal', 'Amytal Sodium'],
                                                             'is_tox_target': True,
                                                             'is_medication': True})
    lab_models.TestTarget.objects.update_or_create(name='Morphine',
                                                   defaults={'common_name': ['MOP', 'Avinza', 'Duramorph', 'Embeda',
                                                                             'Kadian', 'MS Contin', 'MSIR', 'MSSR',
                                                                             'Oramorph', 'Roxanol'],
                                                             'category': 'Opiates/Opioids',
                                                             'is_tox_target': True, 'is_medication': True})
    lab_models.TestTarget.objects.update_or_create(name='Buprenorphine',
                                                   defaults={'common_name': ['BUP', 'Buprenex', 'Bunavail', 'Buprenex',
                                                                             'Butrans', 'Sublocade', 'Subutex'],
                                                             'category': 'Opiate Agonist Antagonists',
                                                             'is_tox_target': True,
                                                             'is_medication': True})
    lab_models.TestTarget.objects.update_or_create(name='Citalopram',
                                                   defaults={'common_name': ['Celexa', 'Lexapro'],
                                                             'is_tox_target': True,
                                                             'is_medication': True})
    lab_models.TestTarget.objects.update_or_create(name='Ritalinic Acid',
                                                   defaults={'common_name': ['Concerta', 'Ritalin',
                                                                             'Dexmethylphenidate', 'Focalin',
                                                                             'Methylphenidate'],
                                                             'is_tox_target': True,
                                                             'is_medication': True})
    lab_models.TestTarget.objects.update_or_create(name='Propoxyphene',
                                                   defaults={'common_name': ['Darvocet', 'Darvon'],
                                                             'category': 'Opiates/Opioids',
                                                             'is_tox_target': True, 'is_medication': True})
    lab_models.TestTarget.objects.update_or_create(name='Amphetamine',
                                                   defaults={'common_name': ['Adderall', 'Dexedrine', 'ProCentra',
                                                                             'Vyvanse'],
                                                             'category': 'Amphetamines',
                                                             'is_tox_target': True,
                                                             'is_medication': True})
    lab_models.TestTarget.objects.update_or_create(name='Oxycodone',
                                                   defaults={'common_name': ['OXY', 'Percocet', 'Endocet', 'Magnacet',
                                                                             'OXY IR', 'Percodan', 'Percolone',
                                                                             'Roxicet', 'Roxicodone', 'Tylox',],
                                                             'category': 'Opiates/Opioids',
                                                             'is_tox_target': True,
                                                             'is_medication': True})
    lab_models.TestTarget.objects.update_or_create(name='Hydromorphone',
                                                   defaults={'common_name': ['Dilaudid', 'Exalgo', 'Hydrostat IR'],
                                                             'category': 'Opiates/Opioids',
                                                             'is_tox_target': True, 'is_medication': True})
    lab_models.TestTarget.objects.update_or_create(name='Fentanyl',
                                                   defaults={'common_name': ['Actiq', 'Duragesic', 'Fentora',
                                                                             'Sublimaze'],
                                                             'category': 'Opiates/Opioids',
                                                             'is_tox_target': True,
                                                             'is_medication': True})
    lab_models.TestTarget.objects.update_or_create(name='Butalbital',
                                                   defaults={'common_name': ['Fioricet', 'Fiorinal'],
                                                             'category': 'Barbiturates',
                                                             'is_tox_target': True,
                                                             'is_medication': True})
    lab_models.TestTarget.objects.update_or_create(name='Hydrocodone',
                                                   defaults={'common_name': ['Vicodin', 'Hydrocet', 'Lorcet', 'Lortab',
                                                                             'Mazidone', 'Norco', 'Reprexain',
                                                                             'Tussionex', 'Hydrocodone', 'Vicoprofen',
                                                                             'Xodol'],
                                                             'category': 'Opiates/Opioids',
                                                             'is_tox_target': True, 'is_medication': True})
    lab_models.TestTarget.objects.update_or_create(name='Chlordiazepoxide',
                                                   defaults={'common_name': ['Librium'],
                                                             'category': 'Benzodiazepines',
                                                             'is_tox_target': True,
                                                             'is_medication': True})
    lab_models.TestTarget.objects.update_or_create(name='Zopiclone',
                                                   defaults={'common_name': ['Lunesta'],
                                                             'category': 'Relaxants/Sleep Aids',
                                                             'is_tox_target': True,
                                                             'is_medication': True})
    lab_models.TestTarget.objects.update_or_create(name='THC-COOH',
                                                   defaults={'common_name': ['Marinol'],
                                                             'category': 'Illicits',
                                                             'is_tox_target': True})
    lab_models.TestTarget.objects.update_or_create(name='Methadone',
                                                   defaults={'common_name': ['MTD', 'Dolophine', 'Methadose'],
                                                             'category': 'Opiates/Opioids',
                                                             'is_tox_target': True,
                                                             'is_medication': True})
    lab_models.TestTarget.objects.update_or_create(name='Meprobamate',
                                                   defaults={'common_name': ['Equanil', 'Miltown'],
                                                             'category': 'Relaxants/Sleep Aids',
                                                             'is_tox_target': True,
                                                             'is_medication': True})
    lab_models.TestTarget.objects.update_or_create(name='Oxymorphone',
                                                   defaults={'common_name': ['Opana', 'Numorphan', 'OxyContin'],
                                                             'category': 'Opiates/Opioids',
                                                             'is_tox_target': True, 'is_medication': True})
    lab_models.TestTarget.objects.update_or_create(name='Nortriptyline',
                                                   defaults={'common_name': ['Pamelor'],
                                                             'category': 'Tricyclic Antidepressants',
                                                             'is_tox_target': True,
                                                             'is_medication': True})
    lab_models.TestTarget.objects.update_or_create(name='Dextromethorphan',
                                                   defaults={'common_name': ['Robitussin'],
                                                             'is_tox_target': True,
                                                             'is_medication': True})
    lab_models.TestTarget.objects.update_or_create(name='Secobarbital',
                                                   defaults={'common_name': ['Seconal Sodium'],
                                                             'category': 'Barbiturates',
                                                             'is_tox_target': True,
                                                             'is_medication': True})
    lab_models.TestTarget.objects.update_or_create(name='Phenobarbital',
                                                   defaults={'common_name': ['Luminal', 'Solfoton'],
                                                             'category': 'Barbiturates',
                                                             'is_tox_target': True,
                                                             'is_medication': True})
    lab_models.TestTarget.objects.update_or_create(name='Zaleplon',
                                                   defaults={'common_name': ['Sonata'],
                                                             'category': 'Relaxants/Sleep Aids',
                                                             'is_tox_target': True,
                                                             'is_medication': True})
    lab_models.TestTarget.objects.update_or_create(name='Pentazocine',
                                                   defaults={'common_name': ['Talwin'],
                                                             'category': 'Opiates/Opioids',
                                                             'is_tox_target': True,
                                                             'is_medication': True,})
    lab_models.TestTarget.objects.update_or_create(name='Tramadol',
                                                   defaults={'common_name': ['Ultram', 'Ultracet'],
                                                             'category': 'Opiates/Opioids',
                                                             'is_tox_target': True, 'is_medication': True})



if __name__ == '__main__':
    logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)
    main()