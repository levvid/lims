from . import models
from . import serializers
from rest_framework import viewsets


class ProcedureCodeViewSet(viewsets.ModelViewSet):
    """
    External API endpoint that allows Procedure codes to be viewed
    """
    http_method_names = ['get']
    lookup_field = 'uuid'
    serializer_class = serializers.ProcedureCodeICD10Serializer

    def get_queryset(self):
        code = self.request.query_params.get('code')
        queryset = models.ProcedureCode.objects.all()
        if code:
            queryset = queryset.filter(code__istartswith=code)
        return queryset
