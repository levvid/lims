import os
import datetime

import django
import logging

from django.db import transaction

os.environ['DJANGO_SETTINGS_MODULE'] = 'main.settings'
django.setup()

from tenant_schemas.utils import tenant_context
import clients.models as clients_models
import orders.models as orders_models

RUN_FOR_ALL_TENANTS = True  # change to false for testing, otherwise takes too long
SPECIFIC_TENANTS_SCHEMA_NAMES = ['ildp']
TENANT_SCHEMAS_TO_EXCLUDE = ['public']


def migrate_tests():
    """
    :return:
    """
    if RUN_FOR_ALL_TENANTS:
        all_tenants = clients_models.Client.objects.all()
        SPECIFIC_TENANTS_SCHEMA_NAMES.clear()
        for tenant in all_tenants:
            SPECIFIC_TENANTS_SCHEMA_NAMES.append(tenant.schema_name)

    tenants_to_migrate = clients_models.Client.objects.filter(schema_name__in=SPECIFIC_TENANTS_SCHEMA_NAMES)\
        .exclude(schema_name__in=TENANT_SCHEMAS_TO_EXCLUDE)

    for tenant in tenants_to_migrate:
        logging.debug('Migrating test panel to test panels for schema {} - {}'.format(tenant.schema_name, tenant.name))
        print('Migrating test panel to test panels for schema {} - {}'.format(tenant.schema_name, tenant.name))
        with tenant_context(tenant):
            # get all tests for schema
            test_ids = orders_models.Test.objects.all().values_list('id', flat=True).exclude(test_panel=None)
            print('retrieved {} tests'.format(len(test_ids)))
            num_tests = len(test_ids)
            counter = 0
            null_counter = 0
            for test_id in test_ids:
                test = orders_models.Test.objects.get(id=test_id)
                # update test panels with test_panel
                try:
                    test.test_panels.set([test.test_panel])
                    counter += 1
                    print('Migrated test # {} of {}'.format(counter, num_tests))
                except django.db.utils.IntegrityError:
                    null_counter += 1
                    print('Skipping null test panel')
                    continue
            print('Updated {} tests for schema {} - {} with {} tests with null test panel IDs'
                  .format(counter, tenant.schema_name, tenant.name, null_counter))


if __name__ == '__main__':
    migrate_tests()

