# Generated by Django 2.0.2 on 2019-04-01 12:36

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('orders', '0006_auto_20190401_1214'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='testtypereflex',
            name='reflex_test_type',
        ),
        migrations.AddField(
            model_name='testtypereflex',
            name='reflex_test_type',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='test_type_reflexed', to='orders.TestType'),
        ),
    ]
