import os
import sys

import django
import logging

os.environ['DJANGO_SETTINGS_MODULE'] = 'main.settings'
django.setup()

from tenant_schemas.utils import tenant_context
import clients.models as clients_models
import orders.models as orders_models
from orders import generate_billing_hl7_labrcm

TENANT_NAME = 'ildp'
REQUESTED_ORDER_ACCESSIONS = ['20011700009', '20020400009', '20020400008', '20013000003', '20021000008', '20021000010',
                              '20013000004', '20020500006', '20020500009', '20020500008', '20020400005', '20020500007',
                              '20013100002', '20020400004', '20021000006', '20020700005', '20020700008', '20020700003',
                              '20020700007', '20020700006', '20020700004', '20020500003', '20020400003', '20020600001',
                              '20013100001', '20020600005', '20021000007', '20020600004', '20020700002', '20013000002',
                              '20013100003', '20020700001', '20021000001', '20020400001', '20020500002', '20020500004',
                              '20020400010', '20021000002', '20020600003', '20012900004', '20020400002', '20021000005',
                              '20012700002', '20021000004', '20013100004', '20012800002', '20021000011', '20021000003',
                              '20020600002', '20020500005', '20020400006', '20020400007', '20012700001', '20013000001']
# add accession_numbers here to test on local DB
TEST_DB_ACCESSIONS = ['20012900004']


def send_billing_messages():
    """
    :return:
    """
    tenant = clients_models.Client.objects.get(schema_name=TENANT_NAME)

    with tenant_context(tenant):
        # get all reported orders with requisition forms
        reported_orders = orders_models.Order.objects.filter(
            accession_number__in=REQUESTED_ORDER_ACCESSIONS)  # get all reported orders

        print('orders {}'.format(len(reported_orders)))
        for order in reported_orders:
            order.is_billed = False

            # Archive any old billing messages first
            billing_messages = orders_models.BillingMessage.objects.filter(order=order)
            for billing_message in billing_messages:
                billing_message.is_active = False
                billing_message.save(update_fields=['is_active'])

            print('Generating billing messages for order {} and is billed {}'.format(order.code, order.is_billed))

            if os.environ.get('DEBUG') == 'False' and not os.environ.get('STAGING') == 'True':  # production environment
                generate_billing_hl7_labrcm.send_billing_message_message(tenant.id, order.uuid,
                                                                         environment='Production')
            elif os.environ.get('STAGING') == 'True':  # staging for testing
                generate_billing_hl7_labrcm.send_billing_message_message(tenant.id, order.uuid,
                                                                         environment='Staging')
            else:  # dev
                generate_billing_hl7_labrcm.send_billing_message_message(tenant.id, order.uuid,
                                                                         environment='Development')

            # update billing messages to say "Sent" for the order
            billing_messages = orders_models.BillingMessage.objects.filter(order=order)
            for billing_message in billing_messages:
                billing_message.is_sent = True
                billing_message.save(update_fields=['is_sent'])


if __name__ == '__main__':
    logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)
    send_billing_messages()
