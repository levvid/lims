import copy
import datetime
import uuid

from braces.views import LoginRequiredMixin
from django.contrib import messages
from django.db import transaction
from django.forms import formset_factory
from django.http import HttpResponseRedirect
from django.shortcuts import render, redirect
from django.urls import reverse
from django.utils import timezone
from django.views.generic import View

from orders import forms, generate_hl7, generate_requisition_form, models, tasks

from accounts import models as accounts_models
from lab import models as lab_models
from lab_tenant.models import User
from lab_tenant.views import LabMemberMixin, LISLoginRequiredMixin
from patients import models as patients_models

from orders.functions import get_test_panel_forms, get_test_forms, \
    generate_sample_code, add_bulk_results, get_sample_barcode, add_containers
from main.functions import convert_tenant_timezone_to_utc, convert_utc_to_tenant_timezone

# order_create_add_sample must be the last step until object creation logic is decoupled from view
ORDER_CREATE_STEPS = ['order_create',
                      'order_create_patient_payer',
                      'order_create_patient_status',
                      'order_create_super_test_panel_select',
                      'order_create_test_panel_select',
                      'order_create_test_select',
                      'order_create_add_samples']

# url name to button label
URL_TO_BUTTON_NAME = {'order_create_patient_payer': 'Patient Insurance',
                      'order_create_patient_status': 'Patient History',
                      'order_create_super_test_panel_select': 'Select Super Test Profiles',
                      'order_create_test_panel_select': 'Select Test Profiles',
                      'order_create_test_select': 'Select Tests',
                      'order_create_add_samples': 'Add Samples'}


def get_next_step_url(request, current_step):
    """
    Returns the url of the next page in Order creation
    :param request:
    :param current_step:
    :return:
    """
    order_create_steps = copy.deepcopy(ORDER_CREATE_STEPS)
    skip_insurance = request.tenant.settings.get('skip_insurance', False)
    skip_patient_history = request.tenant.settings.get('skip_patient_history', False)
    enable_individual_analytes = request.tenant.settings.get('enable_individual_analytes', False)

    if skip_insurance:
        order_create_steps.remove('order_create_patient_payer')
    if skip_patient_history:
        order_create_steps.remove('order_create_patient_status')
    if not enable_individual_analytes:
        order_create_steps.remove('order_create_test_select')
        order_create_steps.remove('order_create_super_test_panel_select')

    return order_create_steps[order_create_steps.index(current_step) + 1]


class OrderCreateStep1View(LISLoginRequiredMixin, View):
    def get(self, request):
        next_step_url_name = get_next_step_url(request, 'order_create')
        next_step_button_string = URL_TO_BUTTON_NAME[next_step_url_name]
        require_icd10 = request.tenant.settings.get('require_icd10', False)

        if request.user.user_type == 10:
            provider = accounts_models.Provider.objects.get(user__id=request.user.id)
            form = forms.OrderForm(initial={'provider': provider}, require_icd10=require_icd10)
        elif request.user.user_type == 9:
            account = accounts_models.Account.objects.get(user__id=request.user.id)
            form = forms.OrderForm(initial={'account': account}, require_icd10=require_icd10)
        else:
            form = forms.OrderForm(require_icd10=require_icd10)

        context = dict()
        context['form'] = form
        context['next_step_button_string'] = next_step_button_string
        context['require_icd10'] = require_icd10
        # reset session data
        request.session['requested_super_test_panels'] = None

        return render(request, 'order_create.html', context)

    @transaction.atomic
    def post(self, request):
        next_step_url_name = get_next_step_url(request, 'order_create')
        next_step_button_string = URL_TO_BUTTON_NAME[next_step_url_name]
        require_icd10 = request.tenant.settings.get('require_icd10', False)
        form = forms.OrderForm(request.POST, require_icd10=require_icd10)

        context = dict()
        context['form'] = form
        context['next_step_button_string'] = next_step_button_string
        context['require_icd10'] = require_icd10

        if form.is_valid():
            jsonable_cleaned_data = copy.deepcopy(form.cleaned_data)
            if jsonable_cleaned_data['account']:
                jsonable_cleaned_data['account'] = jsonable_cleaned_data['account'].id
            if jsonable_cleaned_data['provider']:
                jsonable_cleaned_data['provider'] = jsonable_cleaned_data['provider'].id
            if jsonable_cleaned_data['patient']:
                jsonable_cleaned_data['patient'] = jsonable_cleaned_data['patient'].id
            if jsonable_cleaned_data['patient_birth_date']:
                jsonable_cleaned_data['patient_birth_date'] = str(jsonable_cleaned_data['patient_birth_date'])
            if jsonable_cleaned_data['icd10_codes']:
                jsonable_cleaned_data['icd10_codes'] = [x.id for x in jsonable_cleaned_data['icd10_codes']]
            else:
                jsonable_cleaned_data['icd10_codes'] = None
            request.session['step_1_cleaned_data'] = jsonable_cleaned_data

            return redirect(next_step_url_name)
        else:
            return render(request, 'order_create.html', context)


class OrderCreatePatientPayerView(LISLoginRequiredMixin, View):
    def get(self, request):
        next_step_url_name = get_next_step_url(request, 'order_create_patient_payer')
        next_step_button_string = URL_TO_BUTTON_NAME[next_step_url_name]

        step_1_cleaned_data = request.session['step_1_cleaned_data']
        if step_1_cleaned_data.get('patient'):
            patient = patients_models.Patient.objects.get(id=step_1_cleaned_data['patient'])
            # get "latest" objects (although there should only be one of each)
            # should be using get method instead of filter
            try:
                primary_patient_payer = patient.patient_payers.filter(is_primary=True).latest('modified_date')
            except patients_models.PatientPayer.DoesNotExist:
                primary_patient_payer = None
            try:
                secondary_patient_payer = patient.patient_payers.filter(is_secondary=True).latest('modified_date')
            except patients_models.PatientPayer.DoesNotExist:
                secondary_patient_payer = None
            try:
                tertiary_patient_payer = patient.patient_payers.filter(is_tertiary=True).latest('modified_date')
            except patients_models.PatientPayer.DoesNotExist:
                tertiary_patient_payer = None
            try:
                workers_comp_payer = patient.patient_payers.filter(is_workers_comp=True).latest('modified_date')
            except patients_models.PatientPayer.DoesNotExist:
                workers_comp_payer = None
            try:
                patient_guarantor = patients_models.PatientGuarantor.objects.filter(patient=patient).latest('modified_date')
            except patients_models.PatientGuarantor.DoesNotExist:
                patient_guarantor = None
        else:
            patient = None
            primary_patient_payer = None
            secondary_patient_payer = None
            tertiary_patient_payer = None
            workers_comp_payer = None
            patient_guarantor = None

        # this refers to the form field choices
        if primary_patient_payer:
            insurance_type = 'Saved Insurance'
        else:
            insurance_type = 'None'
        if secondary_patient_payer:
            secondary_insurance_type = 'Saved Insurance'
        else:
            secondary_insurance_type = 'None'
        if tertiary_patient_payer:
            tertiary_insurance_type = 'Saved Insurance'
        else:
            tertiary_insurance_type = 'None'
        if workers_comp_payer:
            workers_comp_type = 'Saved Workers Comp'
        else:
            workers_comp_type = 'None'
        if patient_guarantor:
            patient_guarantor_type = 'Saved Guarantor'
        else:
            patient_guarantor_type = 'None'

        form = forms.PatientPayerForm(initial={'patient': patient,
                                               'insurance_type': insurance_type,
                                               'secondary_insurance_type': secondary_insurance_type,
                                               'tertiary_insurance_type': tertiary_insurance_type,
                                               'workers_comp_type': workers_comp_type,
                                               'patient_guarantor_type': patient_guarantor_type})

        context = {'form': form,
                   'patient': patient,
                   'primary_patient_payer': primary_patient_payer,
                   'secondary_patient_payer': secondary_patient_payer,
                   'tertiary_patient_payer': tertiary_patient_payer,
                   'workers_comp_payer': workers_comp_payer,
                   'patient_guarantor': patient_guarantor,
                   'next_step_button_string': next_step_button_string}
        return render(request, 'order_create_patient_payer.html', context)

    @transaction.atomic
    def post(self, request):
        next_step_url_name = get_next_step_url(request, 'order_create_patient_payer')
        next_step_button_string = URL_TO_BUTTON_NAME[next_step_url_name]

        step_1_cleaned_data = request.session['step_1_cleaned_data']
        if step_1_cleaned_data['patient']:
            patient = patients_models.Patient.objects.get(id=step_1_cleaned_data['patient'])
            # get "latest" objects (although there should only be one of each)
            # should be using get method instead of filter
            try:
                primary_patient_payer = patient.patient_payers.filter(is_primary=True).latest('modified_date')
            except patients_models.PatientPayer.DoesNotExist:
                primary_patient_payer = None
            try:
                secondary_patient_payer = patient.patient_payers.filter(is_secondary=True).latest('modified_date')
            except patients_models.PatientPayer.DoesNotExist:
                secondary_patient_payer = None
            try:
                tertiary_patient_payer = patient.patient_payers.filter(is_tertiary=True).latest('modified_date')
            except patients_models.PatientPayer.DoesNotExist:
                tertiary_patient_payer = None
            try:
                workers_comp_payer = patient.patient_payers.filter(is_workers_comp=True).latest('modified_date')
            except patients_models.PatientPayer.DoesNotExist:
                workers_comp_payer = None
            try:
                patient_guarantor = patients_models.PatientGuarantor.objects.filter(patient=patient).latest(
                    'modified_date')
            except patients_models.PatientGuarantor.DoesNotExist:
                patient_guarantor = None

        else:
            patient = None
            primary_patient_payer = None
            secondary_patient_payer = None
            tertiary_patient_payer = None
            workers_comp_payer = None
            patient_guarantor = None

        # this refers to the form field choices
        if primary_patient_payer:
            insurance_type = 'Saved Insurance'
        else:
            insurance_type = 'None'
        if secondary_patient_payer:
            secondary_insurance_type = 'Saved Insurance'
        else:
            secondary_insurance_type = 'None'
        if tertiary_patient_payer:
            tertiary_insurance_type = 'Saved Insurance'
        else:
            tertiary_insurance_type = 'None'
        if workers_comp_payer:
            workers_comp_type = 'Saved Workers Comp'
        else:
            workers_comp_type = 'None'
        if patient_guarantor:
            patient_guarantor_type = 'Saved Guarantor'
        else:
            patient_guarantor_type = 'None'

        form = forms.PatientPayerForm(request.POST, initial={'patient': patient,
                                                             'insurance_type': insurance_type,
                                                             'secondary_insurance_type': secondary_insurance_type,
                                                             'tertiary_insurance_type': tertiary_insurance_type,
                                                             'workers_comp_type': workers_comp_type,
                                                             'patient_guarantor_type': patient_guarantor_type})

        context = {'form': form,
                   'patient': patient,
                   'primary_patient_payer': primary_patient_payer,
                   'secondary_patient_payer': secondary_patient_payer,
                   'tertiary_patient_payer': tertiary_patient_payer,
                   'workers_comp_payer': workers_comp_payer,
                   'patient_guarantor': patient_guarantor,
                   'next_step_button_string': next_step_button_string}

        if form.is_valid():
            jsonable_cleaned_data = copy.deepcopy(form.cleaned_data)

            if jsonable_cleaned_data.get('payer'):
                jsonable_cleaned_data['payer'] = jsonable_cleaned_data['payer'].id
            if jsonable_cleaned_data.get('secondary_payer'):
                jsonable_cleaned_data['secondary_payer'] = jsonable_cleaned_data['secondary_payer'].id
            if jsonable_cleaned_data.get('tertiary_payer'):
                jsonable_cleaned_data['tertiary_payer'] = jsonable_cleaned_data['tertiary_payer'].id
            if jsonable_cleaned_data.get('workers_comp_payer'):
                jsonable_cleaned_data['workers_comp_payer'] = jsonable_cleaned_data['workers_comp_payer'].id
            # date object must be JSON serializable
            # guarantor or patient birth date
            if jsonable_cleaned_data.get('birth_date'):
                jsonable_cleaned_data['birth_date'] = str(jsonable_cleaned_data['birth_date'])
            # subscriber birth dates (if applicable)
            if jsonable_cleaned_data.get('subscriber_birth_date'):
                jsonable_cleaned_data['subscriber_birth_date'] = str(jsonable_cleaned_data['subscriber_birth_date'])
            if jsonable_cleaned_data.get('secondary_subscriber_birth_date'):
                jsonable_cleaned_data['secondary_subscriber_birth_date'] = str(jsonable_cleaned_data['secondary_subscriber_birth_date'])
            if jsonable_cleaned_data.get('tertiary_subscriber_birth_date'):
                jsonable_cleaned_data['tertiary_subscriber_birth_date'] = str(jsonable_cleaned_data['tertiary_subscriber_birth_date'])

            if jsonable_cleaned_data.get('workers_comp_injury_date'):
                jsonable_cleaned_data['workers_comp_injury_date'] = str(jsonable_cleaned_data['workers_comp_injury_date'])

            request.session['patient_payer_cleaned_data'] = jsonable_cleaned_data
            return HttpResponseRedirect(reverse(next_step_url_name))
        else:
            return render(request, 'order_create_patient_payer.html', context)


class OrderCreatePatientStatusView(LISLoginRequiredMixin, View):

    def get_context(self, request):
        step_1_cleaned_data = request.session['step_1_cleaned_data']
        if step_1_cleaned_data.get('account'):
            account = accounts_models.Account.objects.get(id=step_1_cleaned_data['account'])
        else:
            account = None
        if step_1_cleaned_data.get('provider'):
            provider = accounts_models.Provider.objects.get(id=step_1_cleaned_data['provider'])
        else:
            provider = None

        super_test_panel_types = models.SuperTestPanelType.objects.filter(is_exclusive=True,
                                                                          exclusive_accounts=account) | \
                                 models.SuperTestPanelType.objects.filter(is_exclusive=True,
                                                                          exclusive_providers=provider)

        if super_test_panel_types:
            next_step_url_name = get_next_step_url(request, 'order_create_patient_status')
        else:
            next_step_url_name = get_next_step_url(request, 'order_create_super_test_panel_select')

        next_step_button_string = URL_TO_BUTTON_NAME[next_step_url_name]

        if step_1_cleaned_data['patient'] and step_1_cleaned_data['provider']:
            try:
                prescribed_medications = patients_models.PatientPrescribedMedicationHistory.objects.filter(
                    patient_id=step_1_cleaned_data['patient'], provider_id=step_1_cleaned_data['provider']). \
                    prefetch_related('prescribed_medications').latest('created_date').prescribed_medications.all()
            except patients_models.PatientPrescribedMedicationHistory.DoesNotExist:
                prescribed_medications = None
        else:
            prescribed_medications = None

        context = {'prescribed_medications': prescribed_medications,
                   'next_step_button_string': next_step_button_string,
                   'next_step_url_name': next_step_url_name}
        return context

    def get(self, request):
        context = self.get_context(request)
        form = forms.PatientStatusForm(initial={'prescribed_medications': context['prescribed_medications']})
        context['form'] = form
        return render(request, 'order_create_patient_status.html', context)

    def post(self, request):

        context = self.get_context(request)
        form = forms.PatientStatusForm(request.POST)

        if form.is_valid():
            jsonable_cleaned_data = copy.deepcopy(form.cleaned_data)
            if jsonable_cleaned_data['temperature']:
                jsonable_cleaned_data['temperature'] = str(jsonable_cleaned_data['temperature'])
            if jsonable_cleaned_data['blood_pressure_systolic']:
                jsonable_cleaned_data['blood_pressure_systolic'] = str(jsonable_cleaned_data['blood_pressure_systolic'])
            if jsonable_cleaned_data['blood_pressure_diastolic']:
                jsonable_cleaned_data['blood_pressure_diastolic'] = str(jsonable_cleaned_data['blood_pressure_diastolic'])
            if jsonable_cleaned_data['prescribed_medications']:
                jsonable_cleaned_data['prescribed_medications'] = \
                    [x.id for x in jsonable_cleaned_data['prescribed_medications']]
            else:
                jsonable_cleaned_data['prescribed_medications'] = None
            request.session['patient_status_cleaned_data'] = jsonable_cleaned_data

            return redirect(context['next_step_url_name'])
        else:
            return render(request, 'order_create_patient_status.html', context)


class OrderCreateSuperTestPanelSelectView(LISLoginRequiredMixin, View):

    def get_context(self, request):
        next_step_url_name = get_next_step_url(request, 'order_create_super_test_panel_select')
        next_step_button_string = URL_TO_BUTTON_NAME[next_step_url_name]

        step_1_cleaned_data = request.session['step_1_cleaned_data']
        if step_1_cleaned_data.get('account'):
            account = accounts_models.Account.objects.get(id=step_1_cleaned_data['account'])
        else:
            account = None
        if step_1_cleaned_data.get('provider'):
            provider = accounts_models.Provider.objects.get(id=step_1_cleaned_data['provider'])
        else:
            provider = None

        # OR query because a super test panel can be exclusive to both accounts and providers
        super_test_panel_types = models.SuperTestPanelType.objects.filter(is_exclusive=True,
                                                                          exclusive_accounts=account) | \
                                 models.SuperTestPanelType.objects.filter(is_exclusive=True,
                                                                          exclusive_providers=provider)
        # in case exclusive accounts/providers OR query creates duplicates
        super_test_panel_types = super_test_panel_types.distinct()
        super_test_panel_type_list = []

        for super_test_panel in super_test_panel_types:
            test_panel_type_list = []

            test_panel_types = models.TestPanelType.objects.filter(super_test_panel_types=super_test_panel)

            for test_panel_type in test_panel_types:
                test_panel_type_list.append(test_panel_type)

            super_test_panel_type_list.append((super_test_panel, test_panel_type_list))

        context = {}
        context['next_step_url_name'] = next_step_url_name
        context['next_step_button_string'] = next_step_button_string
        context['super_test_panel_type_list'] = super_test_panel_type_list
        return context

    def get(self, request):
        context = self.get_context(request)
        return render(request, 'order_create_super_test_panel_select.html', context=context)

    @transaction.atomic
    def post(self, request):
        context = self.get_context(request)

        super_test_panel_types_selected = request.POST.getlist('super_test_panel_types_selected')
        super_test_panel_types_selected = [int(x) for x in super_test_panel_types_selected]

        request.session['requested_super_test_panels'] = super_test_panel_types_selected
        return redirect(context['next_step_url_name'])


class OrderCreateTestPanelSelectView(LISLoginRequiredMixin, View):

    def get_context(self, request):
        step_1_cleaned_data = request.session['step_1_cleaned_data']
        if step_1_cleaned_data.get('account'):
            account = accounts_models.Account.objects.get(id=step_1_cleaned_data['account'])
        else:
            account = None
        if step_1_cleaned_data.get('provider'):
            provider = accounts_models.Provider.objects.get(id=step_1_cleaned_data['provider'])
        else:
            provider = None

        next_step_url_name = get_next_step_url(request, 'order_create_test_panel_select')
        next_step_button_string = URL_TO_BUTTON_NAME[next_step_url_name]

        requested_super_test_panels = request.session.get('requested_super_test_panels')
        if requested_super_test_panels:
            requested_test_panels_from_super = models.TestPanelType.objects.filter(super_test_panel_types__in=requested_super_test_panels)
        else:
            requested_test_panels_from_super = None

        test_panel_categories = models.TestPanelCategory.objects.all()
        test_panel_type_list = []

        for test_panel_category in test_panel_categories:
            test_panel_types = models.TestPanelType.objects. \
                filter(test_panel_category=test_panel_category, is_exclusive=False). \
                order_by('category_order'). \
                prefetch_related('test_panel_category')
            if provider:
                # include TestPanels exclusive to this provider
                test_panel_types = test_panel_types | \
                                   models.TestPanelType.objects.filter(test_panel_category=test_panel_category,
                                                                       is_exclusive=True,
                                                                       exclusive_providers=provider)

            if account:
                # include TestPanels exclusive to this account
                test_panel_types = test_panel_types.exclude(excluded_accounts=account) | \
                                   models.TestPanelType.objects.filter(test_panel_category=test_panel_category,
                                                                       is_exclusive=True,
                                                                       exclusive_accounts=account)
            test_panel_types = test_panel_types.distinct()

            for test_panel_type in test_panel_types:
                if requested_test_panels_from_super:
                    if test_panel_type in requested_test_panels_from_super:
                        test_panel_type.selected = True

            if test_panel_types:
                test_panel_type_list.append((test_panel_category, test_panel_types))

        context = {}
        context['next_step_url_name'] = next_step_url_name
        context['next_step_button_string'] = next_step_button_string
        context['test_panel_type_list'] = test_panel_type_list

        return context

    def get(self, request):
        context = self.get_context(request)
        return render(request, 'order_create_test_panel_select.html', context)

    @transaction.atomic
    def post(self, request):
        context = self.get_context(request)

        test_panel_types_selected = request.POST.getlist('test_panel_types_selected')
        test_panel_types_selected = [int(x) for x in test_panel_types_selected]

        if not test_panel_types_selected:
            messages.error(request, 'Please select at least one test profile.')
            return render(request, 'order_create_test_panel_select.html', context)

        request.session['requested_test_panels'] = test_panel_types_selected
        return redirect(context['next_step_url_name'])


class OrderCreateTestSelectView(LISLoginRequiredMixin, View):
    def get(self, request):
        step_1_cleaned_data = request.session['step_1_cleaned_data']

        next_step_url_name = get_next_step_url(request, 'order_create_test_select')
        next_step_button_string = URL_TO_BUTTON_NAME[next_step_url_name]

        requested_test_panels = request.session['requested_test_panels']
        test_panel_types = models.TestPanelType.objects.filter(id__in=requested_test_panels)

        # query for all tests first, then query for test panel categories associated with each test
        test_types = models.TestType.objects.filter(testpaneltype__in=test_panel_types)
        # all test types are selected in the UI, since the test panels were chosen
        test_type_categories = models.TestTypeCategory.objects.filter(test_types__in=test_types).distinct().order_by('name')

        test_type_category_list = []
        for test_type_category in test_type_categories:
            category_test_type_list = []
            # only include test types from the category that are included the test panels
            category_test_types = test_type_category.test_types.all()
            for category_test_type in category_test_types:
                if category_test_type in test_types:
                    category_test_type_list.append(category_test_type)

            test_type_category_list.append((test_type_category.name, category_test_type_list))

        # not all tests are in a test category. group uncategorized tests together
        # add test types that are not categorized
        uncategorized_test_types = test_types.filter(test_type_categories__isnull=True).distinct()
        if uncategorized_test_types:
            test_type_category_list.append(('Uncategorized Tests', uncategorized_test_types))

        context = {}
        context['next_step_button_string'] = next_step_button_string

        context['test_panel_types'] = test_panel_types
        context['test_types'] = test_types
        context['test_type_categories'] = test_type_categories
        context['test_type_category_list'] = test_type_category_list
        return render(request, 'order_create_test_select.html', context)

    @transaction.atomic
    def post(self, request):
        next_step_url_name = get_next_step_url(request, 'order_create_test_select')
        next_step_button_string = URL_TO_BUTTON_NAME[next_step_url_name]

        requested_test_panels = request.session['requested_test_panels']
        test_panel_types = models.TestPanelType.objects.filter(id__in=requested_test_panels)\
            .prefetch_related('test_types')

        # query for all tests first, then query for test panel categories associated with each test
        test_types = models.TestType.objects.filter(testpaneltype__in=test_panel_types)
        # all test types are selected in the UI, since the test panels were chosen
        test_type_categories = models.TestTypeCategory.objects.filter(test_types__in=test_types).distinct().order_by(
            'name')

        test_type_category_list = []
        for test_type_category in test_type_categories:
            category_test_type_list = []
            # only include test types from the category that are included the test panels
            category_test_types = test_type_category.test_types.all()
            for category_test_type in category_test_types:
                if category_test_type in test_types:
                    category_test_type_list.append(category_test_type)

            test_type_category_list.append((test_type_category.name, category_test_type_list))

        # not all tests are in a test category. group uncategorized tests together
        # add test types that are not categorized
        uncategorized_test_types = test_types.filter(test_type_categories__isnull=True).distinct()
        if uncategorized_test_types:
            test_type_category_list.append(('Uncategorized Tests', uncategorized_test_types))

        # context = get_test_forms(request, requested_test_panels)
        context = {}
        context['next_step_button_string'] = next_step_button_string

        context['test_panel_types'] = test_panel_types
        context['test_types'] = test_types
        context['test_type_categories'] = test_type_categories
        context['test_type_category_list'] = test_type_category_list

        context['next_step_button_string'] = next_step_button_string

        tests_selected = request.POST.getlist('tests_selected')

        if not tests_selected:
            messages.error(request, 'Please select at least one test.')
            return render(request, 'order_create_test_select.html', context)

        # the requested_tests_dict is a dict of {'test_panel_name': [test_list]}
        # it doesn't matter which tests are in the test_list for each test panel
        # (only that the selected tests are listed in all panels), since the
        # constituent tests aren't going to be saved in the test panel object
        # EDIT: 3/18/2020 - Changing it so it now DOES matter which tests are
        # in each test_list, otherwise it interferes with the migration of Off-site Labs
        # to Test Panels from Tests
        requested_tests_dict = {}

        for test_panel in test_panel_types:
            test_panel_specific_tests = \
                [x.id for x in models.TestType.objects.filter(testpaneltype=test_panel,
                                                              id__in=tests_selected)]
            requested_tests_dict[test_panel.name] = test_panel_specific_tests

        request.session['requested_tests'] = tests_selected
        request.session['requested_tests_dict'] = requested_tests_dict
        return HttpResponseRedirect(reverse(next_step_url_name))


class OrderCreateAddSamples(LISLoginRequiredMixin, View):
    def get(self, request):
        requested_tests = request.session.get('requested_tests', None)
        if not requested_tests:
            requested_tests = []
            requested_test_panels = request.session.get('requested_test_panels')
            for test_panel_type_id in requested_test_panels:
                test_panel_type = models.TestPanelType.objects.get(id=test_panel_type_id)
                test_types = test_panel_type.test_types.all()
                for test_type in test_types:
                    requested_tests.append(str(test_type.id))
            requested_tests = list(set(requested_tests))

        clia_sample_types = lab_models.CLIASampleType.objects.all()
        require_collected_by = request.tenant.settings.get('require_collected_by', False)
        default_datetime = convert_utc_to_tenant_timezone(request, timezone.now())

        step_1_cleaned_data = request.session['step_1_cleaned_data']
        if step_1_cleaned_data['account']:
            account = accounts_models.Account.objects.get(id=step_1_cleaned_data['account'])
        else:
            account = None

        # calculated required samples types
        samples_required = {}
        for test_type_id in requested_tests:
            test_type = models.TestType.objects.get(id=int(test_type_id))
            if test_type.required_samples:
                for clia_sample_type in clia_sample_types:
                    samples_required[str(clia_sample_type.id)] = max(samples_required.get(str(clia_sample_type.id), 0),
                                                                     test_type.required_samples.get(
                                                                         str(clia_sample_type.id), 0))

        initial_data = []
        for clia_sample_type in clia_sample_types:
            initial_data += [{'sample_type': clia_sample_type,
                              'collection_time': default_datetime.strftime('%I:%M %p'),
                              'collection_date': default_datetime.strftime('%m-%d-%Y'),
                              'collection_temperature_unit': '°F'} for x in
                             range(samples_required.get(str(clia_sample_type.id), 0))]
        SampleFormSet = formset_factory(forms.SampleForm, formset=forms.BaseSampleFormSet, extra=0)
        sample_formset = SampleFormSet(prefix='sample', initial=initial_data,
                                       form_kwargs={'require_collected_by': require_collected_by,
                                                    'account': account})
        # set num required sample logic here
        context = {'sample_formset': sample_formset,
                   'require_collected_by': require_collected_by}

        return render(request, 'order_create_add_samples.html', context)

    def post(self, request):
        require_collected_by = request.tenant.settings.get('require_collected_by', False)
        context = {'require_collected_by': require_collected_by}
        SampleFormSet = formset_factory(forms.SampleForm, formset=forms.BaseSampleFormSet)
        sample_formset = SampleFormSet(request.POST, prefix='sample',
                                       form_kwargs={'require_collected_by': require_collected_by})

        if sample_formset.is_valid():
            # create order
            order_result = tasks.create_order_with_code.apply_async(args=[request.tenant.id], queue='identifiers')
            order_id = order_result.get()

            # assign accession number if order is created 'in-house'
            if request.user.user_type < 9 and request.tenant.settings.get('enable_accession_number', False):
                accession_number_result = tasks.assign_accession_number.apply_async(
                    args=[order_id, request.tenant.id],
                    queue='identifiers')
                accession_number = accession_number_result.get()

            order = models.Order.objects.get(id=order_id)
            # set received date to now if order is created 'in-house'
            if request.user.user_type < 9:
                order.received_date = timezone.now()
            step_1_cleaned_data = request.session['step_1_cleaned_data']
            patient_payer_cleaned_data = request.session.get('patient_payer_cleaned_data', None)
            patient_status_cleaned_data = request.session.get('patient_status_cleaned_data', None)

            if step_1_cleaned_data['account']:
                account = accounts_models.Account.objects.get(id=step_1_cleaned_data['account'])
            else:
                account = None
            if not account:
                account_uuid = uuid.uuid4()
                email = step_1_cleaned_data['account_email']
                name = step_1_cleaned_data['account_name']
                if email:
                    account_email = email
                else:
                    lab_schema = request.tenant.schema_name
                    account_email = str(account_uuid) + '@' + lab_schema + '.com'

                user_obj = User(user_type=9,
                                username=account_email,
                                email=account_email)
                user_obj.set_password(str(account_uuid))
                user_obj.save()

                account = accounts_models.Account.objects.create(
                    name=name,
                    address1=step_1_cleaned_data['account_address1'],
                    address2=step_1_cleaned_data['account_address2'],
                    zip_code=step_1_cleaned_data['account_zip_code'],
                    phone_number=step_1_cleaned_data['account_phone'],
                    fax_number=step_1_cleaned_data['account_fax'],
                    email=email,
                    user=user_obj,
                    uuid=account_uuid,
                    alternate_id=step_1_cleaned_data['account_alternate_id']
                )

            if step_1_cleaned_data['provider']:
                provider = accounts_models.Provider.objects.get(id=step_1_cleaned_data['provider'])
                if step_1_cleaned_data['account_type'] == 'New Account':
                    provider.accounts.add(account)
            else:
                provider = None
            if step_1_cleaned_data['provider_type'] == 'New Provider':
                # create User object before creating Provider, since provider has a username
                provider_uuid = uuid.uuid4()
                if step_1_cleaned_data['provider_email']:
                    provider_email = step_1_cleaned_data['provider_email']
                else:
                    lab_schema = request.tenant.schema_name
                    provider_email = str(provider_uuid) + '@' + lab_schema + '.com'

                user_obj = User(first_name=step_1_cleaned_data['provider_first_name'],
                                last_name=step_1_cleaned_data['provider_last_name'],
                                username=provider_email,
                                email=provider_email,
                                user_type=10)
                user_obj.set_password(str(provider_uuid))
                user_obj.save()

                provider = accounts_models.Provider(
                    user=user_obj,
                    npi=step_1_cleaned_data['provider_npi'],
                    id_num=step_1_cleaned_data['provider_id_num'],
                    suffix=step_1_cleaned_data['provider_suffix'],
                    title=step_1_cleaned_data['provider_title'],
                    uuid=provider_uuid
                )
                provider.save()
                provider.accounts.add(account)

            if step_1_cleaned_data['patient']:
                patient = patients_models.Patient.objects.get(id=step_1_cleaned_data['patient'])
            else:
                patient = None
            if not patient:
                # generate random string for username and password initially
                user_uuid = str(uuid.uuid4())
                username = step_1_cleaned_data['patient_first_name'] + '-' + step_1_cleaned_data[
                    'patient_last_name'] + '-' + user_uuid

                # create User object
                user_obj = User(first_name=step_1_cleaned_data['patient_first_name'],
                                last_name=step_1_cleaned_data['patient_last_name'],
                                username=username,
                                user_type=20)
                if step_1_cleaned_data['patient_email']:
                    user_obj.email = step_1_cleaned_data['patient_email']
                user_obj.set_password(user_uuid)
                # create User object first before creating Patient
                user_obj.save()

                patient = patients_models.Patient(user=user_obj,
                                                  suffix=step_1_cleaned_data['patient_suffix'],
                                                  middle_initial=step_1_cleaned_data['patient_middle_initial'],
                                                  sex=step_1_cleaned_data['patient_sex'],
                                                  ethnicity=step_1_cleaned_data['patient_ethnicity'],
                                                  birth_date=step_1_cleaned_data['patient_birth_date'],
                                                  address1=step_1_cleaned_data['patient_address1'],
                                                  address2=step_1_cleaned_data['patient_address2'],
                                                  zip_code=step_1_cleaned_data['patient_zip_code'],
                                                  social_security=step_1_cleaned_data['patient_social_security_number'],
                                                  phone_number=step_1_cleaned_data['patient_phone'])
                patient.save()

            ############################################################################################################
            # patient_payer_cleaned_data must not be None (otherwise, this section should be skipped)
            if patient_payer_cleaned_data:
                # primary payer
                if not patient_payer_cleaned_data.get('insurance_type'):
                    primary_patient_payer = None
                    # create self-pay PatientPayer object
                    patients_models.PatientPayer.objects.update_or_create(patient=patient,
                                                                          payer_type='Self-Pay')

                # update or create new primary payer
                elif patient_payer_cleaned_data.get('insurance_type') == 'New Insurance':
                    # if payer already exists in dropdown and is selected
                    if patient_payer_cleaned_data.get('payer'):
                        primary_patient_payer, created = patients_models.PatientPayer.objects.update_or_create(
                            is_primary=True,
                            patient=patient,
                            defaults={'payer_id': patient_payer_cleaned_data['payer'],
                                      'payer_type': patient_payer_cleaned_data['patient_payer_type'],
                                      # subscriber info
                                      'subscriber_last_name': patient_payer_cleaned_data['subscriber_last_name'],
                                      'subscriber_first_name': patient_payer_cleaned_data['subscriber_first_name'],
                                      'subscriber_middle_initial': patient_payer_cleaned_data[
                                          'subscriber_middle_initial'],
                                      'subscriber_suffix': patient_payer_cleaned_data['subscriber_suffix'],
                                      'subscriber_address1': patient_payer_cleaned_data['subscriber_address1'],
                                      'subscriber_address2': patient_payer_cleaned_data['subscriber_address2'],
                                      'subscriber_zip_code': patient_payer_cleaned_data['subscriber_zip_code'],
                                      'subscriber_sex': patient_payer_cleaned_data['subscriber_sex'],
                                      'subscriber_birth_date': patient_payer_cleaned_data['subscriber_birth_date'],
                                      'subscriber_social_security': patient_payer_cleaned_data[
                                          'subscriber_social_security'],
                                      'subscriber_relationship': patient_payer_cleaned_data['subscriber_relationship'],
                                      # insurance details
                                      'member_id': patient_payer_cleaned_data['member_id'],
                                      'group_number': patient_payer_cleaned_data['group_number']},
                        )
                    # if payer can't be found in dropdown, create a new payer first
                    else:
                        payer = patients_models.Payer.objects.create(name=patient_payer_cleaned_data.get('payer_name'),
                                                                     payer_type=patient_payer_cleaned_data.get(
                                                                         'patient_payer_type'))

                        primary_patient_payer, created = patients_models.PatientPayer.objects.update_or_create(
                            is_primary=True,
                            patient=patient,
                            defaults={'payer_id': payer.id,
                                      'payer_type': patient_payer_cleaned_data['patient_payer_type'],
                                      # subscriber info
                                      'subscriber_last_name': patient_payer_cleaned_data['subscriber_last_name'],
                                      'subscriber_first_name': patient_payer_cleaned_data['subscriber_first_name'],
                                      'subscriber_middle_initial': patient_payer_cleaned_data[
                                          'subscriber_middle_initial'],
                                      'subscriber_suffix': patient_payer_cleaned_data['subscriber_suffix'],
                                      'subscriber_address1': patient_payer_cleaned_data['subscriber_address1'],
                                      'subscriber_address2': patient_payer_cleaned_data['subscriber_address2'],
                                      'subscriber_zip_code': patient_payer_cleaned_data['subscriber_zip_code'],
                                      'subscriber_sex': patient_payer_cleaned_data['subscriber_sex'],
                                      'subscriber_birth_date': patient_payer_cleaned_data['subscriber_birth_date'],
                                      'subscriber_social_security': patient_payer_cleaned_data[
                                          'subscriber_social_security'],
                                      'subscriber_relationship': patient_payer_cleaned_data['subscriber_relationship'],
                                      # insurance details
                                      'member_id': patient_payer_cleaned_data['member_id'],
                                      'group_number': patient_payer_cleaned_data['group_number']},
                        )
                else:
                    # if saved insurance, no change to PatientPayer
                    primary_patient_payer = patients_models.PatientPayer.objects.filter(is_primary=True,
                                                                                        patient=patient)[0]

                ########################################################################################################
                # secondary payer
                if not patient_payer_cleaned_data.get('secondary_insurance_type'):
                    secondary_patient_payer = None
                # update or create new primary payer
                elif patient_payer_cleaned_data.get('secondary_insurance_type') == 'New Insurance':
                    # if payer already exists in dropdown and is selected
                    if patient_payer_cleaned_data.get('secondary_payer'):
                        secondary_patient_payer, created = patients_models.PatientPayer.objects.update_or_create(
                            is_secondary=True,
                            patient=patient,
                            defaults={'payer_id': patient_payer_cleaned_data['secondary_payer'],
                                      'payer_type': patient_payer_cleaned_data['secondary_patient_payer_type'],
                                      # subscriber info
                                      'subscriber_last_name': patient_payer_cleaned_data[
                                          'secondary_subscriber_last_name'],
                                      'subscriber_first_name': patient_payer_cleaned_data[
                                          'secondary_subscriber_first_name'],
                                      'subscriber_middle_initial': patient_payer_cleaned_data[
                                          'secondary_subscriber_middle_initial'],
                                      'subscriber_suffix': patient_payer_cleaned_data['secondary_subscriber_suffix'],
                                      'subscriber_address1': patient_payer_cleaned_data[
                                          'secondary_subscriber_address1'],
                                      'subscriber_address2': patient_payer_cleaned_data[
                                          'secondary_subscriber_address2'],
                                      'subscriber_zip_code': patient_payer_cleaned_data['secondary_subscriber_zip_code'],
                                      'subscriber_sex': patient_payer_cleaned_data['secondary_subscriber_sex'],
                                      'subscriber_birth_date': patient_payer_cleaned_data[
                                          'secondary_subscriber_birth_date'],
                                      'subscriber_social_security': patient_payer_cleaned_data[
                                          'secondary_subscriber_social_security'],
                                      'subscriber_relationship': patient_payer_cleaned_data[
                                          'secondary_subscriber_relationship'],
                                      # insurance details
                                      'member_id': patient_payer_cleaned_data['secondary_member_id'],
                                      'group_number': patient_payer_cleaned_data['secondary_group_number']},
                        )
                    # if payer can't be found in dropdown, create a new payer first
                    else:
                        payer = patients_models.Payer.objects.create(
                            name=patient_payer_cleaned_data.get('secondary_payer_name'),
                            payer_type=patient_payer_cleaned_data.get('secondary_patient_payer_type'))

                        secondary_patient_payer, created = patients_models.PatientPayer.objects.update_or_create(
                            is_secondary=True,
                            patient=patient,
                            defaults={'payer_id': payer.id,
                                      'payer_type': patient_payer_cleaned_data['secondary_patient_payer_type'],
                                      # subscriber info
                                      'subscriber_last_name': patient_payer_cleaned_data[
                                          'secondary_subscriber_last_name'],
                                      'subscriber_first_name': patient_payer_cleaned_data[
                                          'secondary_subscriber_first_name'],
                                      'subscriber_middle_initial': patient_payer_cleaned_data[
                                          'secondary_subscriber_middle_initial'],
                                      'subscriber_suffix': patient_payer_cleaned_data['secondary_subscriber_suffix'],
                                      'subscriber_address1': patient_payer_cleaned_data[
                                          'secondary_subscriber_address1'],
                                      'subscriber_address2': patient_payer_cleaned_data[
                                          'secondary_subscriber_address2'],
                                      'subscriber_zip_code': patient_payer_cleaned_data[
                                          'secondary_subscriber_zip_code'],
                                      'subscriber_sex': patient_payer_cleaned_data['secondary_subscriber_sex'],
                                      'subscriber_birth_date': patient_payer_cleaned_data[
                                          'secondary_subscriber_birth_date'],
                                      'subscriber_social_security': patient_payer_cleaned_data[
                                          'secondary_subscriber_social_security'],
                                      'subscriber_relationship': patient_payer_cleaned_data[
                                          'secondary_subscriber_relationship'],
                                      # insurance details
                                      'member_id': patient_payer_cleaned_data['secondary_member_id'],
                                      'group_number': patient_payer_cleaned_data['secondary_group_number']},
                        )
                else:
                    # if saved insurance, no change to PatientPayer
                    secondary_patient_payer = patients_models.PatientPayer.objects.filter(is_secondary=True,
                                                                                          patient=patient)[0]

                ########################################################################################################
                # tertiary payer
                if not patient_payer_cleaned_data.get('tertiary_insurance_type'):
                    tertiary_patient_payer = None
                # update or create new primary payer
                elif patient_payer_cleaned_data.get('tertiary_insurance_type') == 'New Insurance':
                    # if payer already exists in dropdown and is selected
                    if patient_payer_cleaned_data.get('tertiary_payer'):
                        tertiary_patient_payer, created = patients_models.PatientPayer.objects.update_or_create(
                            is_tertiary=True,
                            patient=patient,
                            defaults={'payer_id': patient_payer_cleaned_data['tertiary_payer'],
                                      'payer_type': patient_payer_cleaned_data['tertiary_patient_payer_type'],
                                      # subscriber info
                                      'subscriber_last_name': patient_payer_cleaned_data[
                                          'tertiary_subscriber_last_name'],
                                      'subscriber_first_name': patient_payer_cleaned_data[
                                          'tertiary_subscriber_first_name'],
                                      'subscriber_middle_initial': patient_payer_cleaned_data[
                                          'tertiary_subscriber_middle_initial'],
                                      'subscriber_suffix': patient_payer_cleaned_data['tertiary_subscriber_suffix'],
                                      'subscriber_address1': patient_payer_cleaned_data['tertiary_subscriber_address1'],
                                      'subscriber_address2': patient_payer_cleaned_data['tertiary_subscriber_address2'],
                                      'subscriber_zip_code': patient_payer_cleaned_data['tertiary_subscriber_zip_code'],
                                      'subscriber_sex': patient_payer_cleaned_data['tertiary_subscriber_sex'],
                                      'subscriber_birth_date': patient_payer_cleaned_data[
                                          'tertiary_subscriber_birth_date'],
                                      'subscriber_social_security': patient_payer_cleaned_data[
                                          'tertiary_subscriber_social_security'],
                                      'subscriber_relationship': patient_payer_cleaned_data[
                                          'tertiary_subscriber_relationship'],
                                      # insurance details
                                      'member_id': patient_payer_cleaned_data['tertiary_member_id'],
                                      'group_number': patient_payer_cleaned_data['tertiary_group_number']},
                        )
                    # if payer can't be found in dropdown, create a new payer first
                    else:
                        payer = patients_models.Payer.objects.create(
                            name=patient_payer_cleaned_data.get('tertiary_payer_name'),
                            payer_type=patient_payer_cleaned_data.get('tertiary_patient_payer_type'))

                        tertiary_patient_payer, created = patients_models.PatientPayer.objects.update_or_create(
                            is_tertiary=True,
                            patient=patient,
                            defaults={'payer_id': payer.id,
                                      'payer_type': patient_payer_cleaned_data['tertiary_patient_payer_type'],
                                      # subscriber info
                                      'subscriber_last_name': patient_payer_cleaned_data[
                                          'tertiary_subscriber_last_name'],
                                      'subscriber_first_name': patient_payer_cleaned_data[
                                          'tertiary_subscriber_first_name'],
                                      'subscriber_middle_initial': patient_payer_cleaned_data[
                                          'tertiary_subscriber_middle_initial'],
                                      'subscriber_suffix': patient_payer_cleaned_data['tertiary_subscriber_suffix'],
                                      'subscriber_address1': patient_payer_cleaned_data['tertiary_subscriber_address1'],
                                      'subscriber_address2': patient_payer_cleaned_data['tertiary_subscriber_address2'],
                                      'subscriber_zip_code': patient_payer_cleaned_data['tertiary_subscriber_zip_code'],
                                      'subscriber_sex': patient_payer_cleaned_data['tertiary_subscriber_sex'],
                                      'subscriber_birth_date': patient_payer_cleaned_data[
                                          'tertiary_subscriber_birth_date'],
                                      'subscriber_social_security': patient_payer_cleaned_data[
                                          'tertiary_subscriber_social_security'],
                                      'subscriber_relationship': patient_payer_cleaned_data[
                                          'tertiary_subscriber_relationship'],
                                      # insurance details
                                      'member_id': patient_payer_cleaned_data['tertiary_member_id'],
                                      'group_number': patient_payer_cleaned_data['tertiary_group_number']},
                        )
                else:
                    # if saved insurance, no change to PatientPayer
                    tertiary_patient_payer = patients_models.PatientPayer.objects.filter(is_tertiary=True,
                                                                                         patient=patient)[0]
                ########################################################################################################
                # Workers' Comp
                if not patient_payer_cleaned_data.get('workers_comp_type'):
                    workers_comp_payer = None
                # update or create new WC payer
                elif patient_payer_cleaned_data.get('workers_comp_type') == 'New Workers Comp':
                    # if payer already exists in dropdown
                    if patient_payer_cleaned_data.get('workers_comp_payer'):
                        workers_comp_payer, created = patients_models.PatientPayer.objects.update_or_create(
                            is_workers_comp=True,
                            patient=patient,
                            defaults={'payer_id': patient_payer_cleaned_data['workers_comp_payer'],
                                      # see PatientPayer model for choices
                                      'payer_type': 'Workers Comp',
                                      'workers_comp_claim_id': patient_payer_cleaned_data['workers_comp_claim_id'],
                                      'workers_comp_injury_date': patient_payer_cleaned_data[
                                          'workers_comp_injury_date'],
                                      'workers_comp_adjuster': patient_payer_cleaned_data['workers_comp_adjuster']},
                        )
                    # if payer can't be found in dropdown, create a new payer first
                    else:
                        payer = patients_models.Payer.objects.create(
                            name=patient_payer_cleaned_data.get('workers_comp_payer_name'),
                            payer_type=patient_payer_cleaned_data.get('workers_comp_type'))

                        workers_comp_payer, created = patients_models.PatientPayer.objects.update_or_create(
                            is_workers_comp=True,
                            patient=patient,
                            defaults={'payer_id': payer.id,
                                      # see PatientPayer model for choices
                                      'payer_type': 'Workers Comp',
                                      'workers_comp_claim_id': patient_payer_cleaned_data['workers_comp_claim_id'],
                                      'workers_comp_injury_date': patient_payer_cleaned_data[
                                          'workers_comp_injury_date'],
                                      'workers_comp_adjuster': patient_payer_cleaned_data['workers_comp_adjuster']},
                        )
                else:
                    # if saved insurance, no change to PatientPayer
                    workers_comp_payer = patients_models.PatientPayer.objects.filter(is_workers_comp=True,
                                                                                     patient=patient)[0]

                ########################################################################################################
                # guarantor
                # if patient guarantor type is falsey (None)
                if not patient_payer_cleaned_data.get('patient_guarantor_type'):
                    patient_guarantor = None
                elif patient_payer_cleaned_data.get('patient_guarantor_type') == 'Saved Guarantor':
                    try:
                        # Note (7/29)
                        # technically, there should just be one guarantor...not sure if there can be multiple?
                        patient_guarantor = patients_models.PatientGuarantor.objects.filter(patient=patient).latest(
                            'modified_date')
                    except patients_models.PatientGuarantor.DoesNotExist:
                        patient_guarantor = None
                else:
                    # generate random string for username and password initially
                    user_uuid = str(uuid.uuid4())
                    username = user_uuid

                    # create User object
                    user_obj = User(first_name=patient_payer_cleaned_data['first_name'],
                                    last_name=patient_payer_cleaned_data['last_name'],
                                    username=username,
                                    user_type=19,
                                    email=patient_payer_cleaned_data['email'])
                    user_obj.set_password(user_uuid)
                    # create User object first before creating Patient
                    user_obj.save()

                    patient_guarantor, created = patients_models.PatientGuarantor.objects.update_or_create(
                        patient=patient,
                        defaults={'user': user_obj,
                                  'relationship_to_patient': patient_payer_cleaned_data['relationship_to_patient'],
                                  'suffix': patient_payer_cleaned_data['suffix'],
                                  'middle_initial': patient_payer_cleaned_data['middle_initial'],
                                  'sex': patient_payer_cleaned_data['sex'],
                                  'birth_date': patient_payer_cleaned_data['birth_date'],
                                  'address1': patient_payer_cleaned_data['address1'],
                                  'address2': patient_payer_cleaned_data['address2'],
                                  'zip_code': patient_payer_cleaned_data['zip_code'],
                                  'phone_number': patient_payer_cleaned_data['phone_number'],
                                  'social_security': patient_payer_cleaned_data['social_security_number']}
                    )
            ########################################################################################################
            else:
                primary_patient_payer = None
                secondary_patient_payer = None
                tertiary_patient_payer = None
                workers_comp_payer = None
                patient_guarantor = None

            # add provider to patient.providers manually (many-to-many)
            if provider and provider not in patient.providers.all():
                try:
                    patient.providers.add(provider)
                except:
                    pass

            # assign objects to order
            # must do this in order to preserve order status
            order.provider = provider
            order.account = account
            order.patient = patient
            order.research_consent = step_1_cleaned_data['research_consent']
            order.patient_result_request = step_1_cleaned_data['patient_result_request']
            order.save()

            # create foreign key objects relating to Order
            if primary_patient_payer:
                models.OrderPatientPayer.objects.create(order=order,
                                                        patient=patient,
                                                        is_primary=True,
                                                        payer=primary_patient_payer.payer,
                                                        payer_type=primary_patient_payer.payer_type,
                                                        group_number=primary_patient_payer.group_number,
                                                        member_id=primary_patient_payer.member_id,
                                                        subscriber_last_name=primary_patient_payer.subscriber_last_name,
                                                        subscriber_first_name=primary_patient_payer.subscriber_first_name,
                                                        subscriber_middle_initial=primary_patient_payer.subscriber_middle_initial,
                                                        subscriber_suffix=primary_patient_payer.subscriber_suffix,
                                                        subscriber_birth_date=primary_patient_payer.subscriber_birth_date,
                                                        subscriber_address1=primary_patient_payer.subscriber_address1,
                                                        subscriber_address2=primary_patient_payer.subscriber_address2,
                                                        subscriber_zip_code=primary_patient_payer.subscriber_zip_code,
                                                        subscriber_sex=primary_patient_payer.subscriber_sex,
                                                        subscriber_phone_number=primary_patient_payer.subscriber_phone_number,
                                                        subscriber_social_security=primary_patient_payer.subscriber_social_security,
                                                        subscriber_relationship=primary_patient_payer.subscriber_relationship)
            if secondary_patient_payer:
                models.OrderPatientPayer.objects.create(order=order,
                                                        patient=patient,
                                                        is_secondary=True,
                                                        payer=secondary_patient_payer.payer,
                                                        payer_type=secondary_patient_payer.payer_type,
                                                        group_number=secondary_patient_payer.group_number,
                                                        member_id=secondary_patient_payer.member_id,
                                                        subscriber_last_name=secondary_patient_payer.subscriber_last_name,
                                                        subscriber_first_name=secondary_patient_payer.subscriber_first_name,
                                                        subscriber_middle_initial=secondary_patient_payer.subscriber_middle_initial,
                                                        subscriber_suffix=secondary_patient_payer.subscriber_suffix,
                                                        subscriber_birth_date=secondary_patient_payer.subscriber_birth_date,
                                                        subscriber_address1=secondary_patient_payer.subscriber_address1,
                                                        subscriber_address2=secondary_patient_payer.subscriber_address2,
                                                        subscriber_zip_code=secondary_patient_payer.subscriber_zip_code,
                                                        subscriber_sex=secondary_patient_payer.subscriber_sex,
                                                        subscriber_phone_number=secondary_patient_payer.subscriber_phone_number,
                                                        subscriber_social_security=secondary_patient_payer.subscriber_social_security,
                                                        subscriber_relationship=secondary_patient_payer.subscriber_relationship)
            if tertiary_patient_payer:
                models.OrderPatientPayer.objects.create(order=order,
                                                        patient=patient,
                                                        is_tertiary=True,
                                                        payer=tertiary_patient_payer.payer,
                                                        payer_type=tertiary_patient_payer.payer_type,
                                                        group_number=tertiary_patient_payer.group_number,
                                                        member_id=tertiary_patient_payer.member_id,
                                                        subscriber_last_name=tertiary_patient_payer.subscriber_last_name,
                                                        subscriber_first_name=tertiary_patient_payer.subscriber_first_name,
                                                        subscriber_middle_initial=tertiary_patient_payer.subscriber_middle_initial,
                                                        subscriber_suffix=tertiary_patient_payer.subscriber_suffix,
                                                        subscriber_birth_date=tertiary_patient_payer.subscriber_birth_date,
                                                        subscriber_address1=tertiary_patient_payer.subscriber_address1,
                                                        subscriber_address2=tertiary_patient_payer.subscriber_address2,
                                                        subscriber_zip_code=tertiary_patient_payer.subscriber_zip_code,
                                                        subscriber_sex=tertiary_patient_payer.subscriber_sex,
                                                        subscriber_phone_number=tertiary_patient_payer.subscriber_phone_number,
                                                        subscriber_social_security=tertiary_patient_payer.subscriber_social_security,
                                                        subscriber_relationship=tertiary_patient_payer.subscriber_relationship)
            if workers_comp_payer:
                models.OrderPatientPayer.objects.create(order=order,
                                                        patient=patient,
                                                        is_workers_comp=True,
                                                        payer=workers_comp_payer.payer,
                                                        payer_type=workers_comp_payer.payer_type,
                                                        workers_comp_claim_id=workers_comp_payer.workers_comp_claim_id,
                                                        workers_comp_injury_date=workers_comp_payer.workers_comp_injury_date,
                                                        workers_comp_adjuster=workers_comp_payer.workers_comp_adjuster)
            if patient_guarantor:
                models.OrderPatientGuarantor.objects.create(order=order,
                                                            patient=patient,
                                                            user=patient_guarantor.user,
                                                            relationship_to_patient=patient_guarantor.relationship_to_patient,
                                                            suffix=patient_guarantor.suffix,
                                                            middle_initial=patient_guarantor.middle_initial,
                                                            sex=patient_guarantor.sex,
                                                            birth_date=patient_guarantor.birth_date,
                                                            address1=patient_guarantor.address1,
                                                            address2=patient_guarantor.address2,
                                                            zip_code=patient_guarantor.zip_code,
                                                            phone_number=patient_guarantor.phone_number,
                                                            social_security=patient_guarantor.social_security)

            # add samples
            order_samples = []
            for sample_form in sample_formset:
                if sample_form.is_valid():
                    sample_type = sample_form.cleaned_data.get('sample_type')
                    collection_date = sample_form.cleaned_data.get('collection_date')
                    collection_time = sample_form.cleaned_data.get('collection_time')
                    if collection_time:
                        collection_date = datetime.datetime.combine(collection_date, collection_time)
                    else:
                        collection_date = datetime.datetime.combine(collection_date, datetime.datetime.min.time())
                    # convert tenant timezone to UTC before saving
                    collection_date = convert_tenant_timezone_to_utc(request, collection_date)

                    if sample_type and collection_date:
                        latest_sample_code = generate_sample_code(order.code, sample_type)
                        sample = models.Sample.objects.create(
                            order=order,
                            code=latest_sample_code,
                            clia_sample_type=sample_type,
                            collection_date=collection_date,
                            collection_temperature=sample_form.cleaned_data.get('collection_temperature'),
                            collection_temperature_unit=sample_form.cleaned_data.get('collection_temperature_unit'),
                            collection_temperature_is_within_range=sample_form.cleaned_data.get(
                                'collection_temperature_is_within_range'
                            ),
                            collection_user=sample_form.cleaned_data.get('collection_user'),
                            barcode=sample_form.cleaned_data.get('barcode'),
                            draw_type=sample_form.cleaned_data.get('draw_type'),
                        )
                        if sample.barcode is None or len(
                                sample.barcode.strip()) == 0:  # simple encryption algo to obfuscate sample.id
                            sample.barcode = get_sample_barcode(sample.id)
                            sample.save(update_fields=['barcode'])
                        order_samples.append(sample)

            icd10_codes = step_1_cleaned_data.get('icd10_codes')
            if icd10_codes:
                order.icd10_codes.set(icd10_codes)

            # add requested services to order
            order_samples = order.samples.all().prefetch_related('clia_sample_type')
            requested_test_panels = request.session['requested_test_panels']
            result_list = []
            enable_individual_analytes = request.tenant.settings.get('enable_individual_analytes', False)
            sample_volume_dict = {}
            for test_panel_type_id in requested_test_panels:
                test_panel_type = models.TestPanelType.objects.prefetch_related('test_types').get(id=test_panel_type_id)
                if enable_individual_analytes:
                    try:
                        test_types = models.TestType.objects.filter(
                            id__in=request.session['requested_tests_dict'][test_panel_type.name]
                        ).all()
                        if not test_types:
                            continue
                    except KeyError:
                        continue
                else:
                    # Note: 2020-02-08
                    # If individual tests cannot be selected (see advanced settings), then it doesn't matter
                    # what tests are picked anyways, so that's why it's test_types.all()
                    # However, this is a bug, since by regulation all labs must
                    # allow providers to choose individual tests
                    test_types = test_panel_type.test_types.all()

                test_panel = models.TestPanel.objects.create(
                    order=order,
                    test_panel_type=test_panel_type,
                    expected_revenue=test_panel_type.cost
                )
                for test_type in test_types:
                    required_samples = test_type.required_samples
                    if required_samples:
                        for required_sample_type_id in required_samples:
                            samples = order_samples.filter(clia_sample_type__id=required_sample_type_id).order_by(
                                'code')[
                                      :required_samples[required_sample_type_id]]
                            for sample in samples:
                                result_list.append((order, sample, test_panel, test_type))

                                # container logic
                                if test_type.container_type:
                                    if test_type.min_volume:
                                        min_volume = test_type.min_volume
                                    else:
                                        min_volume = 0
                                    try:
                                        sample_volume_dict[sample][test_type.container_type] += min_volume
                                    except KeyError:
                                        try:
                                            sample_volume_dict[sample][test_type.container_type] = min_volume
                                        except KeyError:
                                            sample_volume_dict[sample] = {}
                                            sample_volume_dict[sample][test_type.container_type] = min_volume
                    else:
                        for sample in order_samples:
                            result_list.append((order, sample, test_panel, test_type))

            add_containers(sample_volume_dict, order)
            add_bulk_results(request, result_list)

            if request.user.user_type < 9:
                # set samples and container received_date for in-house orders
                for sample in order.samples.all():
                    sample.received_date = timezone.now()
                    sample.save()
                    for container in sample.containers.all():
                        container.received_date = timezone.now()
                        container.save()

            # finalize order by setting submitted=True
            order.submitted = True
            order.submitted_date = timezone.now()
            order.save(update_fields=['submitted'])

            # if patient info exists, then save
            if patient_status_cleaned_data:
                patients_models.PatientStatus(patient=patient,
                                              order=order,
                                              fasting=patient_status_cleaned_data.get('fasting'),
                                              heart_rate=patient_status_cleaned_data.get('heart_rate'),
                                              temperature=patient_status_cleaned_data.get('temperature'),
                                              temperature_unit=patient_status_cleaned_data.get('temperature_unit'),
                                              blood_pressure_systolic=patient_status_cleaned_data.get(
                                                  'blood_pressure_systolic'),
                                              blood_pressure_diastolic=patient_status_cleaned_data.get(
                                                  'blood_pressure_diastolic')).save()
                # if prescribed medications exist, then save
                if patient_status_cleaned_data.get('prescribed_medications'):
                    patient_prescribed_medication_history = patients_models.PatientPrescribedMedicationHistory(
                        patient=patient,
                        order=order,
                        provider=provider if provider else None)  # Add provider if provider != None else None
                    patient_prescribed_medication_history.save()
                    patient_prescribed_medication_history.prescribed_medications.set(
                        patient_status_cleaned_data.get('prescribed_medications'))

            # create hl7 message
            orders_out = generate_hl7.get_order_out_message(order.id, request.tenant.id)
            if orders_out:
                for offsite_laboratory_id in orders_out.keys():
                    lab_models.OrdersOut.objects.create(tenant_id=request.tenant.id,
                                                        uuid=orders_out[offsite_laboratory_id]['uuid'],
                                                        offsite_laboratory_id=int(offsite_laboratory_id),
                                                        hl7_message=orders_out[offsite_laboratory_id]['message'])
            # generate the order requisition form
            generate_requisition_form.generate_requisition_PDF(request, order.uuid)

            request.session['print_labels'] = True
            patient_documents = patients_models.PatientsDocuments.objects.filter(patient=patient)
            if patient_documents:
                order_documents_list = []
                for patient_document in patient_documents:
                    order_document = models.OrderAdditionalDocuments(
                        order=order,
                        patient_document=patient_document.patient_document
                    )
                    order_documents_list.append(order_document)
                if order_documents_list:
                    models.OrderAdditionalDocuments.objects.bulk_create(order_documents_list)

            return redirect('order_details', order.code)

        else:
            # sample formset errors
            if sample_formset.non_form_errors():
                for formset_error in sample_formset.non_form_errors():
                    messages.error(request, formset_error)

            context['sample_formset'] = sample_formset

            return render(request, 'order_create_add_samples.html', context)
