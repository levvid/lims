import datetime

from decimal import Decimal
from django.forms import formset_factory
from django.utils import timezone

from . import forms
from . import models


def merge_patients(patient, patient_to_merge):
    """
    Assign all data from patient_to_merge to patient then delete patient_to_merge
    :param patient: Patient to be preserved
    :param patient_to_merge: Patient to be merged and deleted
    :return:
    """
    orders = patient_to_merge.orders.update(patient=patient)
    # patient status
    patient_status = patient_to_merge.status.update(patient=patient)

    # providers
    for provider in patient_to_merge.providers.all():
        patient.providers.add(provider)
    patient_to_merge.is_active = False
    patient_to_merge.save()


def upload_patient_documents(request, uploaded_document, document_type, patient):
    """

    :param request:
    :param uploaded_document:
    :param document_type:
    :param patient:
    :return:
    """
    patient_document = models.AdditionalDocuments(
        user=request.user,
        patient=patient,
        document_name=uploaded_document.name,
        document_type=document_type
    )

    patient_document.document.save(uploaded_document.name, uploaded_document)
    patients_document = models.PatientsDocuments(
        patient_document=patient_document,
        patient=patient
    )
    patients_document.save()
