import os
import sys

import django
import logging

os.environ['DJANGO_SETTINGS_MODULE'] = 'main.settings'
django.setup()

from tenant_schemas.utils import tenant_context
import clients.models as clients_models
import orders.models as orders_models

TENANT_NAME = 'ildp'


def set_billing_message_status():
    """
    Update status of old billing messages to sent
    """
    tenant = clients_models.Client.objects.get(schema_name=TENANT_NAME)

    with tenant_context(tenant):
        # get all reported orders with requisition forms
        billing_messages = orders_models.BillingMessage.objects.all()

        print('Updating {} billing messages'.format(len(billing_messages)))
        updated_billing_messages = []
        for billing_message in billing_messages:
            billing_message.is_sent = True
            updated_billing_messages.append(billing_message)

        orders_models.BillingMessage.objects.bulk_update(updated_billing_messages, ['is_sent'])

        print('Updated {} billing messages'.format(len(updated_billing_messages)))


if __name__ == '__main__':
    logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)
    set_billing_message_status()
