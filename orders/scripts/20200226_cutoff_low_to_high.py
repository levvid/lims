import os
import django
import logging
import sys

os.environ['DJANGO_SETTINGS_MODULE'] = 'main.settings'
django.setup()

from django.db import transaction
from tenant_schemas.utils import tenant_context

from clients import models as clients_models
from orders import models as orders_models


@transaction.atomic
def main():
    """
    We have been using TestTypeRange.range_low to store cutoff values. Use TestTypeRange.range_high instead

    :return:
    """
    tenants = clients_models.Client.objects.exclude(schema_name='public').all()

    for tenant in tenants:
        logging.debug("Editing TestTypeRange objects for schema - {}".format(tenant.schema_name))
        with tenant_context(tenant):
            test_type_ranges_to_update = []
            test_types = orders_models.TestType.objects.filter(
                test_method__is_within_range=False).all().\
                prefetch_related('test_type_ranges', 'test_method')
            logging.debug("Found {} TestTypes with test_method.is_within_range=False".format(len(test_types)))

            for test_type in test_types:
                if test_type.test_type_ranges:
                    for test_type_range in test_type.test_type_ranges.all():
                        if test_type.test_method.positive_above_cutoff:
                            if test_type_range.range_low and not test_type_range.range_high:
                                # swap range_low and range_high values
                                range_low = test_type_range.range_low
                                test_type_range.range_low = test_type_range.range_high
                                test_type_range.range_high = range_low

                                test_type_ranges_to_update.append(test_type_range)
                        else:
                            if test_type_range.range_high and not test_type_range.range_low:
                                # swap range_low and range_high values
                                range_low = test_type_range.range_low
                                test_type_range.range_low = test_type_range.range_high
                                test_type_range.range_high = range_low

                                test_type_ranges_to_update.append(test_type_range)

            logging.debug("Found {} TestTypeRange objects to migrate".format(
                len(test_type_ranges_to_update))
            )

            logging.debug("Saving updates")
            orders_models.TestTypeRange.objects.bulk_update(test_type_ranges_to_update,
                                                            ['range_low', 'range_high'])


if __name__ == '__main__':
    logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)
    main()
