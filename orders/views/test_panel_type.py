from braces.views import LoginRequiredMixin
from django.db import transaction
from django.shortcuts import render, redirect, get_object_or_404
from django.contrib import messages
from django.views.generic import View

from orders import forms
from orders import models

from lab_tenant.views import LabMemberMixin, LISLoginRequiredMixin


class TestPanelTypeListView(LISLoginRequiredMixin, View):
    def get(self, request):
        return render(request, 'test_panel_type_list.html', {})


class TestPanelTypeDetailsView(LISLoginRequiredMixin, View):
    def get(self, request, test_panel_uuid):
        test_panel_type = get_object_or_404(models.TestPanelType, uuid=test_panel_uuid)
        return render(request, 'test_panel_type_details.html', {'test_panel_type': test_panel_type})


class TestPanelTypeUpdateView(LabMemberMixin, View):
    def populate_test_type_category_list(self, test_panel_type, tests_selected=None):
        test_type_category_list = []
        test_type_categories = models.TestTypeCategory.objects.filter().order_by('name')

        # if no tests_selected input is provided then get all test_types for the test_panel_type
        # in the db. If tests_selected input is provided, then clean the input by converting
        # all test_type_ids to integers.
        if not tests_selected:
            tests_selected = [test_type.id for test_type in test_panel_type.test_types.all()]
        else:
            tests_selected = [int(test_type_id) for test_type_id in tests_selected]

        for test_type_category in test_type_categories:
            name = test_type_category.name
            test_types = test_type_category.test_types.all()

            for test in test_types:
                if test.id in tests_selected:
                    test.selected = True
            test_type_category_list.append((name, test_types))

        # add test types that are not categorized
        uncategorized_test_types = models.TestType.objects.filter(test_type_categories__isnull=True)
        for test in uncategorized_test_types:
            if test.id in tests_selected:
                test.selected = True
        test_type_category_list.append(('Uncategorized Tests', uncategorized_test_types))

        return test_type_category_list

    def get(self, request, test_panel_uuid):
        test_panel_type = get_object_or_404(models.TestPanelType, uuid=test_panel_uuid)

        test_type_category_list = self.populate_test_type_category_list(test_panel_type)

        test_panel_type_alias = test_panel_type.test_panel_type_aliases.all()
        test_panel_type_alias = [str(alias) for alias in test_panel_type_alias]
        alias_str = '\r\n'.join(test_panel_type_alias)

        # for many-to-many relationships (test types), use .all() method to get the queryset
        form = forms.TestPanelTypeForm(initial={'research_consent_required': test_panel_type.research_consent_required,
                                                'test_types': test_panel_type.test_types.all(),
                                                'name': test_panel_type.name,
                                                'aliases': alias_str,
                                                'alternate_id': test_panel_type.alternate_id,
                                                'price': test_panel_type.price,
                                                'category_order': test_panel_type.category_order,
                                                'notes': test_panel_type.notes,
                                                'test_panel_category': test_panel_type.test_panel_category,
                                                'offsite_laboratory': test_panel_type.offsite_laboratory,
                                                'is_exclusive': test_panel_type.is_exclusive,
                                                'exclusive_providers': test_panel_type.exclusive_providers.all(),
                                                'exclusive_accounts': test_panel_type.exclusive_accounts.all(),
                                                'procedure_codes': test_panel_type.procedure_codes.all(),
                                                'lab_departments': test_panel_type.lab_departments.all(),
                                                'in_house_lab_locations': test_panel_type.in_house_lab_locations.all()},
                                       instance_id=test_panel_type.id)
        return render(request, 'test_panel_type_create.html', {'test_panel_type': test_panel_type,
                                                               'form': form,
                                                               'update': True,
                                                               'test_type_category_list': test_type_category_list})

    @transaction.atomic
    def post(self, request, test_panel_uuid):
        test_panel_type = get_object_or_404(models.TestPanelType, uuid=test_panel_uuid)
        tests_selected = request.POST.getlist('tests_selected')
        test_type_category_list = self.populate_test_type_category_list(test_panel_type, tests_selected=tests_selected)

        form = forms.TestPanelTypeForm(request.POST, instance_id=test_panel_type.id)

        if form.is_valid():
            test_panel_type.name = form.cleaned_data['name']
            aliases = form.cleaned_data.get('aliases')
            test_panel_type.alternate_id = form.cleaned_data['alternate_id']
            test_panel_type.price = form.cleaned_data['price']
            test_panel_type.test_panel_category = form.cleaned_data['test_panel_category']
            test_panel_type.category_order = form.cleaned_data['category_order']
            test_panel_type.notes = form.cleaned_data['notes']
            test_panel_type.research_consent_required = form.cleaned_data['research_consent_required']
            test_panel_type.offsite_laboratory = form.cleaned_data['offsite_laboratory']
            test_panel_type.is_exclusive = form.cleaned_data['is_exclusive']
            # save object after update
            test_panel_type.save()

            # make sure alias list gets updated every time
            test_panel_aliases = models.TestPanelTypeAlias.objects.filter(test_panel_type=test_panel_type)
            for test_panel_alias in test_panel_aliases:
                test_panel_alias.is_active = False
                test_panel_alias.save()
            for alias in aliases:
                alias = alias.strip()
                if alias:
                    alias = models.TestPanelTypeAlias(name=alias, test_panel_type=test_panel_type)
                    alias.save()

            test_types = models.TestType.objects.filter(pk__in=tests_selected)
            test_panel_type.test_types.set(test_types)
            exclusive_providers = form.cleaned_data['exclusive_providers']
            exclusive_accounts = form.cleaned_data['exclusive_accounts']
            in_house_lab_locations = form.cleaned_data['in_house_lab_locations']
            test_panel_type.in_house_lab_locations.set(in_house_lab_locations)
            test_panel_type.exclusive_providers.set(exclusive_providers)
            test_panel_type.exclusive_accounts.set(exclusive_accounts)
            test_panel_type.procedure_codes.set(form.cleaned_data['procedure_codes'])
            test_panel_type.lab_departments.set(form.cleaned_data['lab_departments'])

            return redirect('test_panel_type_details', test_panel_uuid=test_panel_type.uuid)
        return render(request, 'test_panel_type_create.html', {'form': form,
                                                               'test_panel_type': test_panel_type,
                                                               'update': True,
                                                               'test_type_category_list': test_type_category_list})


class TestPanelTypeDeleteView(LabMemberMixin, View):
    def get(self, request, test_panel_uuid):
        test_panel_type = get_object_or_404(models.TestPanelType, uuid=test_panel_uuid)
        test_panel_type.is_active = False
        test_panel_type.save()
        return redirect('test_panel_type_list')


class TestPanelTypeCloneView(LabMemberMixin, View):
    def get(self, request, test_panel_uuid):
        test_panel_type = get_object_or_404(models.TestPanelType, uuid=test_panel_uuid)

        cloned_test_panel_type = models.TestPanelType.objects.create(
            name=test_panel_type.name + ' (Copy)',
            price=test_panel_type.price,
            test_panel_category=test_panel_type.test_panel_category,
            category_order=test_panel_type.category_order,
            notes=test_panel_type.notes,
            research_consent_required=test_panel_type.research_consent_required,
            offsite_laboratory=test_panel_type.offsite_laboratory,
            is_exclusive=test_panel_type.is_exclusive,
        )
        cloned_test_panel_type.test_types.set(test_panel_type.test_types.all())
        cloned_test_panel_type.exclusive_providers.set(test_panel_type.exclusive_providers.all())
        cloned_test_panel_type.exclusive_accounts.set(test_panel_type.exclusive_accounts.all())
        cloned_test_panel_type.procedure_codes.set(test_panel_type.procedure_codes.all())
        cloned_test_panel_type.lab_departments.set(test_panel_type.lab_departments.all())
        cloned_test_panel_type.in_house_lab_locations.set(test_panel_type.in_house_lab_locations.all())

        return redirect('test_panel_type_details', test_panel_uuid=cloned_test_panel_type.uuid)


class TestPanelTypeCreateView(LabMemberMixin, View):
    def get(self, request):
        form = forms.TestPanelTypeForm(instance_id=None)
        tests = models.TestType.objects.filter()

        test_type_category_list = []
        test_type_categories = models.TestTypeCategory.objects.filter().order_by('name')

        for test_type_category in test_type_categories:
            name = test_type_category.name
            test_types = test_type_category.test_types.all()
            test_type_category_list.append((name, test_types))

        # add test types that are not categorized
        uncategorized_test_types = models.TestType.objects.filter(test_type_categories__isnull=True)
        test_type_category_list.append(('Uncategorized Tests', uncategorized_test_types))

        return render(request, 'test_panel_type_create.html', {'form': form,
                                                               'tests': tests,
                                                               'test_type_category_list': test_type_category_list})

    @transaction.atomic
    def post(self, request):
        # If necessary to reset id sequence:
        # SELECT pg_catalog.setval(pg_get_serial_sequence('galaxy.test_panel_type_test_types', 'id'), MAX(id))
        # FROM galaxy.test_panel_type_test_types;
        form = forms.TestPanelTypeForm(request.POST, instance_id=None)
        if form.is_valid():
            tests_selected = request.POST.getlist('tests_selected')

            name = form.cleaned_data['name']
            aliases = form.cleaned_data.get('aliases')
            alternate_id = form.cleaned_data['alternate_id']
            price = form.cleaned_data['price']
            test_types = models.TestType.objects.filter(pk__in=tests_selected)
            test_panel_category = form.cleaned_data['test_panel_category']
            category_order = form.cleaned_data['category_order']
            notes = form.cleaned_data['notes']
            research_consent_required = form.cleaned_data['research_consent_required']
            offsite_laboratory = form.cleaned_data['offsite_laboratory']
            is_exclusive = form.cleaned_data['is_exclusive']
            exclusive_providers = form.cleaned_data['exclusive_providers']
            exclusive_accounts = form.cleaned_data['exclusive_accounts']
            in_house_lab_locations = form.cleaned_data['in_house_lab_locations']
            lab_departments = form.cleaned_data['lab_departments']

            test_panel_type_obj = models.TestPanelType(name=name,
                                                       alternate_id=alternate_id,
                                                       price=price,
                                                       test_panel_category=test_panel_category,
                                                       category_order=category_order,
                                                       notes=notes,
                                                       research_consent_required=research_consent_required,
                                                       offsite_laboratory=offsite_laboratory,
                                                       is_exclusive=is_exclusive
                                                       )
            test_panel_type_obj.save()

            alias_list = []
            for alias in aliases:
                alias = alias.strip()
                if alias:
                    alias_list.append(models.TestPanelTypeAlias(name=alias, test_panel_type=test_panel_type_obj))
            models.TestPanelTypeAlias.objects.bulk_create(alias_list)

            ############################################################################################
            # You cannot assign directly to a many-to-many field (test_types)
            # Do it after you create the item first!
            # assign many-to-many field
            test_panel_type_obj.test_types.set(test_types)
            test_panel_type_obj.exclusive_providers.set(exclusive_providers)
            test_panel_type_obj.exclusive_accounts.set(exclusive_accounts)
            test_panel_type_obj.procedure_codes.set(form.cleaned_data['procedure_codes'])
            test_panel_type_obj.in_house_lab_locations.set(in_house_lab_locations)
            test_panel_type_obj.lab_departments.set(lab_departments)

            return redirect('test_panel_type_list')

        return render(request, 'test_panel_type_create.html', {'form': form})


class TestPanelCategoryCreateView(LabMemberMixin, View):
    def get(self, request):
        form = forms.TestPanelCategoryForm()
        context = {'form': form}
        return render(request, 'test_panel_category_create.html', context)

    @transaction.atomic
    def post(self, request):
        form = forms.TestPanelCategoryForm(request.POST)

        if form.is_valid():
            models.TestPanelCategory.objects.create(
                name=form.cleaned_data['name']
            )
            return redirect('test_panel_type_list')
        else:
            return render(request, 'test_panel_category_create.html', {'form': form})


class SuperTestPanelTypeListView(LabMemberMixin, View):
    def get(self, request):
        return render(request, 'super_test_panel_type_list.html')


class SuperTestPanelTypeDetailsView(LabMemberMixin, View):
    def get(self, request, super_test_panel_uuid):
        super_test_panel_type = get_object_or_404(models.SuperTestPanelType, uuid=super_test_panel_uuid)
        test_panel_types = super_test_panel_type.test_panel_types.all()
        # distinct test types, since test panel types can contain the same test types
        test_types = models.TestType.objects.filter(testpaneltype__super_test_panel_types=super_test_panel_type).distinct()

        return render(request, 'super_test_panel_type_details.html', {'super_test_panel_type': super_test_panel_type,
                                                                      'test_panel_types': test_panel_types,
                                                                      'test_types': test_types})


class SuperTestPanelTypeCreateView(LabMemberMixin, View):
    def get(self, request):
        form = forms.SuperTestPanelForm(instance_id=None)
        return render(request, 'super_test_panel_type_create.html', {'form': form})

    @transaction.atomic
    def post(self, request):
        form = forms.SuperTestPanelForm(request.POST, instance_id=None)
        if form.is_valid():
            name = form.cleaned_data.get('name')
            test_panel_types = form.cleaned_data.get('test_panel_types')
            is_exclusive = form.cleaned_data.get('is_exclusive')

            exclusive_providers = form.cleaned_data.get('exclusive_providers')
            exclusive_accounts = form.cleaned_data.get('exclusive_accounts')

            super_test_panel = models.SuperTestPanelType(name=name,
                                                         is_exclusive=is_exclusive)
            super_test_panel.save()
            super_test_panel.test_panel_types.set(test_panel_types)
            super_test_panel.exclusive_providers.set(exclusive_providers)
            super_test_panel.exclusive_accounts.set(exclusive_accounts)

            return redirect('super_test_panel_type_list')

        return render(request, 'super_test_panel_type_create.html', {'form': form})


class SuperTestPanelTypeUpdateView(LabMemberMixin, View):
    def get(self, request, super_test_panel_uuid):
        super_test_panel_type = get_object_or_404(models.SuperTestPanelType, uuid=super_test_panel_uuid)
        form = forms.SuperTestPanelForm(initial={'name': super_test_panel_type.name,
                                                 'test_panel_types': super_test_panel_type.test_panel_types.all(),
                                                 'is_exclusive': super_test_panel_type.is_exclusive,
                                                 'exclusive_providers': super_test_panel_type.exclusive_providers.all(),
                                                 'exclusive_accounts': super_test_panel_type.exclusive_accounts.all(),
                                                 },
                                        instance_id=super_test_panel_type.id)
        return render(request, 'super_test_panel_type_create.html', {'form': form,
                                                                     'super_test_panel_type': super_test_panel_type,
                                                                     'update': True})

    @transaction.atomic
    def post(self, request, super_test_panel_uuid):
        super_test_panel_type = get_object_or_404(models.SuperTestPanelType, uuid=super_test_panel_uuid)

        form = forms.SuperTestPanelForm(request.POST, instance_id=super_test_panel_type.id)

        if form.is_valid():
            name = form.cleaned_data.get('name')
            test_panel_types = form.cleaned_data.get('test_panel_types')

            super_test_panel_type.name = name
            super_test_panel_type.is_exclusive = form.cleaned_data.get('is_exclusive')
            super_test_panel_type.save()

            exclusive_providers = form.cleaned_data['exclusive_providers']
            exclusive_accounts = form.cleaned_data['exclusive_accounts']
            super_test_panel_type.exclusive_providers.set(exclusive_providers)
            super_test_panel_type.exclusive_accounts.set(exclusive_accounts)

            super_test_panel_type.test_panel_types.set(test_panel_types)

            return redirect('super_test_panel_type_list')
        return render(request, 'super_test_panel_type_create.html', {'form': form,
                                                                     'super_test_panel_type': super_test_panel_type,
                                                                     'update': True})


class SuperTestPanelTypeDeleteView(LabMemberMixin, View):
    def get(self, request, super_test_panel_uuid):
        super_test_panel_type = get_object_or_404(models.SuperTestPanelType, uuid=super_test_panel_uuid)
        super_test_panel_type.is_active = False
        super_test_panel_type.save()
        return redirect('super_test_panel_type_list')


class TestPanelTypeCommentCreateView(LabMemberMixin, View):
    def get(self, request, test_panel_type_uuid):
        test_panel_type = get_object_or_404(models.TestPanelType, uuid=test_panel_type_uuid)

        form = forms.TestTypeCommentForm()
        return render(request, 'test_panel_type_comment_create.html', {'form': form,
                                                                       'test_panel_type': test_panel_type})

    def post(self, request, test_panel_type_uuid):
        test_panel_type = get_object_or_404(models.TestPanelType, uuid=test_panel_type_uuid)
        form = forms.TestPanelTypeCommentForm(request.POST)

        if form.is_valid():
            comment = form.cleaned_data.get('comment')
            is_public = form.cleaned_data.get('is_public')

            test_panel_type_comment = models.TestPanelTypeComment(test_panel_type=test_panel_type,
                                                                  comment=comment,
                                                                  is_public=is_public)
            test_panel_type_comment.save()

            redirect_to = request.GET.get('redirect_to')
            if redirect_to:
                return redirect(redirect_to)
            return redirect('test_panel_type_details', test_panel_uuid=test_panel_type_uuid)

        return render(request, 'test_panel_type_comment_create.html', {'form': form,
                                                                       'test_panel_type': test_panel_type})


class TestPanelTypeCommentUpdateView(LabMemberMixin, View):
    def get(self, request, test_panel_type_uuid, test_panel_type_comment_uuid):
        test_panel_type = get_object_or_404(models.TestPanelType, uuid=test_panel_type_uuid)
        test_panel_type_comment = get_object_or_404(models.TestPanelTypeComment, uuid=test_panel_type_comment_uuid)

        form = forms.TestPanelTypeCommentForm(initial={'comment': test_panel_type_comment.comment,
                                                       'is_public': test_panel_type_comment.is_public})

        return render(request, 'test_panel_type_comment_create.html', {'form': form,
                                                                       'test_panel_type': test_panel_type,
                                                                       'update': True})

    def post(self, request, test_panel_type_uuid, test_panel_type_comment_uuid):
        test_panel_type = get_object_or_404(models.TestPanelType, uuid=test_panel_type_uuid)
        test_panel_type_comment = get_object_or_404(models.TestPanelTypeComment, uuid=test_panel_type_comment_uuid)

        form = forms.TestPanelTypeCommentForm(request.POST)

        if form.is_valid():
            comment = form.cleaned_data.get('comment')
            is_public = form.cleaned_data.get('is_public')

            test_panel_type_comment.comment = comment
            test_panel_type_comment.is_public = is_public
            test_panel_type_comment.save()

            redirect_to = request.GET.get('redirect_to')
            if redirect_to:
                return redirect(redirect_to)
            return redirect('test_panel_type_details', test_panel_uuid=test_panel_type_uuid)

        return render(request, 'test_panel_type_comment_create.html', {'form': form,
                                                                       'test_panel_type': test_panel_type})


class TestPanelTypeCommentDeleteView(LabMemberMixin, View):
    def get(self, request, test_panel_type_uuid, test_panel_type_comment_uuid):
        test_panel_type_comment = get_object_or_404(models.TestPanelTypeComment, uuid=test_panel_type_comment_uuid)
        test_panel_type_comment.is_active = False
        test_panel_type_comment.save()

        messages.success(request, "Comment successfully archived.")

        redirect_to = request.GET.get('redirect_to')
        if redirect_to:
            return redirect(redirect_to)
        return redirect('test_panel_type_details', test_panel_uuid=test_panel_type_uuid)
