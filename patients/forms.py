import datetime
import re
import string
from django import forms
from dal import autocomplete, forward

from accounts import models as accounts_models
from lab import models as lab_models
from . import models

from orders import forms as orders_forms


SEX_CHOICES = (('NB', 'Non-Binary'), ('M', 'Male'), ('F', 'Female'))
ETHNICITY_CHOICES = (('U', 'Unknown'), ('B', 'Black or African-American'),
                     ('A', 'American Indian or Alaska Native'), ('S', 'Asian'),
                     ('P', 'Native Hawaiian or Other Pacific Islander'), ('H', 'Hispanic or Latino'),
                     ('O', 'Other'), ('W', 'White'))
OTHER_PAYER_TYPES = (('Saved Insurance', 'Saved Insurance'),
                     ('None', 'None'),
                     ('New Insurance', 'New Insurance'),)
PAYER_CHOICES = (('Commercial', 'Commercial'), ('Medicare', 'Medicare'), ('Medicaid', 'Medicaid'), ('Other', 'Other'))
WORKERS_COMP_TYPES = (('Saved WC', 'Saved Workers\' Compensation'),
                      ('None', 'None'),
                      ('New WC', 'New Workers\' Compensation'))
RELATIONSHIP_CHOICES = (('Self', 'Self'), ('Spouse', 'Spouse'),
                        ('Dependent', 'Dependent'), ('Other', 'Other'))
GUARANTOR_RELATIONSHIP_CHOICES = (('Parent', 'Parent'), ('Spouse', 'Spouse'),
                                  ('Dependent', 'Dependent'), ('Other', 'Other'))
DOCUMENT_TYPES = (('', '-------------'), ('Commercial', 'Commercial'), ('Medicare', 'Medicare'), ('Medicaid', 'Medicaid'), ('Other', 'Other'))


class PatientPayerForm(forms.Form):
    # see orders forms as well
    insurance_type = forms.ChoiceField(choices=(('Self-Pay', 'Self-Pay'), ('Primary', 'Primary'),
                                                ('Secondary', 'Secondary'), ('Tertiary', 'Tertiary')),
                                       widget=forms.RadioSelect(),
                                       label='Insurance Type',
                                       initial=('Self-Pay', 'Self-Pay'))

    # new primary payer fields
    patient_payer_type = forms.ChoiceField(choices=PAYER_CHOICES, required=False, label='Payer Type')
    payer = forms.ModelChoiceField(
        label='Payer',
        queryset=models.Payer.objects.all(),
        widget=autocomplete.ModelSelect2(url='patient_payer_autocomplete'),
        required=False,
    )
    payer_name = forms.CharField(max_length=128, label='Payer Name (If Not Found in Search)', required=False)
    member_id = forms.CharField(max_length=64, label='Member ID/Policy Number', required=False)
    group_number = forms.CharField(max_length=64, label='Group Number', required=False)

    subscriber_last_name = forms.CharField(max_length=64, label='Subscriber Last Name', required=False)
    subscriber_first_name = forms.CharField(max_length=64, label='Subscriber First Name', required=False)
    subscriber_middle_initial = forms.CharField(max_length=1, label='Middle Initial', required=False,
                                                error_messages={'max_length': 'Middle initial must be 1 letter.'})
    subscriber_suffix = forms.CharField(max_length=255, label='Suffix (Jr, III, etc.)', required=False)

    subscriber_sex = forms.ChoiceField(label='Subscriber Sex', choices=SEX_CHOICES, required=False)

    subscriber_birth_date = forms.DateField(label='Subscriber Date of Birth', required=False, input_formats=['%m-%d-%Y'])

    subscriber_address1 = forms.CharField(max_length=255, label='Subscriber Mailing Address', required=False)
    subscriber_address2 = forms.CharField(max_length=255, label='Apartment, Suite, Unit - Optional', required=False)
    subscriber_zip_code = forms.CharField(max_length=128, label='Zip Code', required=False)

    subscriber_social_security = forms.CharField(max_length=11, label='Subscriber Social Security Number', required=False)

    subscriber_relationship = forms.ChoiceField(choices=RELATIONSHIP_CHOICES, required=False,
                                                label='Relationship to Subscriber')

    def clean_patient_payer_type(self):
        patient_payer_type = self.cleaned_data.get('patient_payer_type')
        if self.cleaned_data.get('insurance_type') == 'Self-Pay':
            patient_payer_type = 'Self-Pay'
        return patient_payer_type

    def clean_subscriber_social_security_number(self):
        # not a unique identifier (unlike other SSN checks)
        social_security_number = self.cleaned_data.get('subscriber_social_security_number')

        if social_security_number:
            social_security_number = social_security_number.replace('_', '').strip()
            if not len(social_security_number) == 11:
                raise forms.ValidationError('Social Security Number must be 9 digits.')

    def clean_subscriber_birth_date(self):
        birth_date = self.cleaned_data.get('subscriber_birth_date')
        if birth_date:
            if birth_date > datetime.date.today():
                raise forms.ValidationError('Birth date cannot be in the future.')
        return birth_date

    def clean(self):
        # don't do any form validation if patient is self-pay
        if not self.cleaned_data.get('insurance_type') == 'Self-Pay':
            if not self.cleaned_data.get('payer') and not self.cleaned_data.get('payer_name'):
                self.add_error('payer', forms.ValidationError(
                    'Choose a payer from the dropdown, or fill in the Payer Name field.'))
            if not self.cleaned_data.get('subscriber_last_name') and \
                    self.cleaned_data.get('subscriber_relationship') != 'Self':
                self.add_error('subscriber_last_name', forms.ValidationError(
                    'Subscriber last name is missing.'))
            if not self.cleaned_data.get('subscriber_first_name') and \
                    self.cleaned_data.get('subscriber_relationship') != 'Self':
                self.add_error('subscriber_first_name', forms.ValidationError(
                    'Subscriber first name is missing.'))
            if not self.cleaned_data.get('member_id'):
                self.add_error('member_id', forms.ValidationError(
                    'Member ID/Policy Number is missing.'))

        # if updating insurance info
        # if subscriber is Self, then save blank values (or None for dates) for these fields
        if self.cleaned_data.get('subscriber_relationship') == 'Self':
            self.cleaned_data['subscriber_last_name'] = ''
            self.cleaned_data['subscriber_first_name'] = ''
            self.cleaned_data['subscriber_middle_initial'] = ''
            self.cleaned_data['subscriber_suffix'] = ''
            self.cleaned_data['subscriber_sex'] = 'NB'
            self.cleaned_data['subscriber_birth_date'] = None
            self.cleaned_data['subscriber_address1'] = ''
            self.cleaned_data['subscriber_address2'] = ''
            self.cleaned_data['subscriber_zip_code'] = ''
            self.cleaned_data['subscriber_social_security'] = ''

        return self.cleaned_data

    def __init__(self, *args, **kwargs):
        # this allows you to customize the required error message for all fields
        super(PatientPayerForm, self).__init__(*args, **kwargs)
        for field in self.fields.values():
            field.error_messages = {'required': '{fieldname} is required.'.format(fieldname=field.label),
                                    'invalid': '{fieldname} is invalid.'.format(fieldname=field.label)}


class PatientWorkersCompForm(forms.Form):
    workers_comp_payer = forms.ModelChoiceField(
        label='Workers\' Compensation ',
        queryset=models.Payer.objects.all(),
        widget=autocomplete.ModelSelect2(url='patient_payer_autocomplete'),
        required=False,
    )
    workers_comp_payer_name = forms.CharField(max_length=128, label='Name (If Not Found in Search)', required=False)
    workers_comp_claim_id = forms.CharField(max_length=64, label='Claim ID', required=False)
    workers_comp_adjuster = forms.CharField(max_length=128, label='Claim Manager/Adjuster', required=False)
    workers_comp_injury_date = forms.DateField(label='Injury Date', required=False, input_formats=['%m-%d-%Y'])

    def clean_workers_comp_type(self):
        payer_type = self.cleaned_data.get('workers_comp_type')
        if payer_type == 'None':
            return None
        else:
            return payer_type

    def clean(self):
        # 'None' is string, not boolean
        if self.cleaned_data.get('workers_comp_type') == 'New WC':
            if not (self.cleaned_data.get('workers_comp_payer') or self.cleaned_data.get('workers_comp_payer_name')):
                self.add_error('workers_comp_payer', forms.ValidationError(
                    'Choose a Workers\' Compensation from the dropdown, or fill in the Name field.'))


class PatientGuarantorForm(forms.Form):
    first_name = forms.CharField(max_length=255, label='First Name', required=True,
                                 error_messages={'required': 'Patient first name is required.'})
    middle_initial = forms.CharField(max_length=1, label='Middle Initial', required=False,
                                     error_messages={'max_length': 'Middle initial must be 1 letter.'})
    last_name = forms.CharField(max_length=255, label='Last Name', required=True,
                                error_messages={'required': 'Patient last name is required.'})
    suffix = forms.CharField(max_length=255, label='Suffix', required=False)

    sex = forms.ChoiceField(choices=SEX_CHOICES, required=True)

    birth_date = forms.DateField(label='Date of Birth', required=True, input_formats=['%m-%d-%Y'])

    address1 = forms.CharField(max_length=255, label='Mailing Address', required=False)
    address2 = forms.CharField(max_length=255, label='Apartment, Suite, Unit - Optional', required=False)

    email = forms.EmailField(max_length=255, label='Email', required=False, widget=forms.EmailInput(
        attrs={'class': 'form-control'}))

    phone_number = forms.CharField(max_length=255, label='Phone', required=False)

    social_security_number = forms.CharField(max_length=11, label='Social Security Number', required=False)

    relationship_to_patient = forms.ChoiceField(choices=GUARANTOR_RELATIONSHIP_CHOICES, required=False,
                                                label='Relationship to patient')
    zip_code = forms.CharField(max_length=128, label='Zip Code', required=False)


class PatientForm(forms.ModelForm):
    class Meta:
        model = models.Patient
        fields = []

    providers = forms.ModelMultipleChoiceField(
        label='Provider(s)',
        queryset=accounts_models.Provider.objects.all(),
        widget=autocomplete.ModelSelect2Multiple(url='patient_provider_autocomplete',
                                                 forward=(forward.Self(),)),
        required=True,
    )

    primary_care_provider = forms.ModelChoiceField(
        label='Primary Care Provider',
        queryset=accounts_models.Provider.objects.filter(),
        widget=autocomplete.ModelSelect2(url='primary_care_provider_autocomplete',
                                         forward=['providers']),
        required=False,
    )

    first_name = forms.CharField(max_length=255, label='First Name', required=True,
                                 error_messages={'required': 'Patient first name is required.'})
    middle_initial = forms.CharField(max_length=1, label='Middle Initial', required=False,
                                     error_messages={'max_length': 'Middle initial must be 1 letter.'})
    last_name = forms.CharField(max_length=255, label='Last Name', required=True,
                                error_messages={'required': 'Patient last name is required.'})
    suffix = forms.CharField(max_length=255, label='Suffix', required=False)

    sex = forms.ChoiceField(choices=SEX_CHOICES, required=True)

    ethnicity = forms.ChoiceField(choices=ETHNICITY_CHOICES, initial=ETHNICITY_CHOICES[0], label='Ethnicity', required=False)

    birth_date = forms.DateField(label='Date of Birth', required=True, input_formats=['%m-%d-%Y'])

    address1 = forms.CharField(max_length=255, label='Mailing Address', required=False)
    address2 = forms.CharField(max_length=255, label='Apartment, Suite, Unit - Optional', required=False)
    zip_code = forms.CharField(max_length=128, label='Zip Code', required=False)

    email = forms.EmailField(max_length=255, label='Email', required=False, widget=forms.EmailInput(
        attrs={'class': 'form-control'}))

    phone_number = forms.CharField(max_length=255, label='Phone', required=False)

    social_security_number = forms.CharField(max_length=11, label='Social Security Number', required=False)

    def clean_social_security_number(self):
        # unique identifier - check for already existing one

        social_security_number = self.cleaned_data.get('social_security_number')

        if social_security_number:
            social_security_number = social_security_number.replace('_', '').strip()
            if not len(social_security_number) == 11:
                raise forms.ValidationError('Patient Social Security Number must be 9 digits.')

            matching_social_security_number = models.Patient.objects.filter(social_security=social_security_number)

            # if this is an update instead of a create, then exclude own object from search
            if self.instance:
                matching_social_security_number = matching_social_security_number\
                    .exclude(social_security=self.instance.social_security)
                if matching_social_security_number:
                    raise forms.ValidationError('A patient with this Social Security Number already exists.')

        return social_security_number

    def clean_email(self):
        email = self.cleaned_data.get('email')
        # make sure that email is checked against username rather than other emails, since emails aren't unique
        return email

    def clean_birth_date(self):
        birth_date = self.cleaned_data.get('birth_date')
        if birth_date > datetime.date.today():
            raise forms.ValidationError('Birth date can not be in the future.')
        return birth_date

    def clean(self):
        providers = self.cleaned_data['providers']
        primary_care_provider = self.cleaned_data['primary_care_provider']

        # if primary care provider is chosen (not required), then it must be in the provider list
        if primary_care_provider:
            if primary_care_provider not in providers:
                raise forms.ValidationError('A Primary Care Provider must be selected from the Provider(s) list.')


class DuplicatePatientForm(forms.Form):
    original_patient = forms.ModelChoiceField(
        queryset=models.Patient.objects.all(),
        required=False
    )
    patient_to_merge = forms.ModelChoiceField(
        label='Patient to merge',
        queryset=models.Patient.objects.all(),
        widget=autocomplete.ModelSelect2(url='patient_order_autocomplete',
                                         forward=['original_patient']),
        required=True,
    )

    def __init__(self, *args, patient, **kwargs):
        self.patient_obj = patient
        super().__init__(*args, **kwargs)
        if self.patient_obj:
            self.fields['original_patient'] = forms.ModelChoiceField(
                queryset=models.Patient.objects.filter(uuid=self.patient_obj.uuid),
                required=False
            )


class PatientMergeForm(forms.Form):
    confirm_merge = forms.BooleanField(
        required=True,
    )


class PayerForm(forms.Form):
    # new primary payer fields
    payer_type = forms.ChoiceField(choices=PAYER_CHOICES, required=True, label='Payer Type')
    payer_name = forms.CharField(max_length=128, label='Payer Name', required=True)
    payer_address1 = forms.CharField(max_length=255, label='Mailing Address', required=False)
    payer_address2 = forms.CharField(max_length=255, label='Apartment, Suite, Unit - Optional', required=False)
    payer_zip_code = forms.CharField(max_length=128, label='Zip Code', required=False)
    payer_code = forms.CharField(max_length=128, label='Payer Code', required=False)
    phone_number = forms.CharField(max_length=128, label='Phone Number', required=False)
    abbreviated_name = forms.CharField(max_length=128, label='Abbreviated Name', required=False)

    def clean(self):
        if not self.cleaned_data.get('payer_type') and not self.cleaned_data.get('payer_name'):
            self.add_error('payer_name', forms.ValidationError(
                'Please fill in the Payer Name field.'))
        return self.cleaned_data

    def __init__(self, *args, **kwargs):
        # this allows you to customize the required error message for all fields
        super(PayerForm, self).__init__(*args, **kwargs)
        for field in self.fields.values():
            field.error_messages = {'required': '{fieldname} is required.'.format(fieldname=field.label),
                                    'invalid': '{fieldname} is invalid.'.format(fieldname=field.label)}
