function createWorkqueueQAPassedTable(orders, table) {
  orders.forEach(element => {
    element.code.toString = () => element.code.text;
  });

  if (!table) {
    const hotElement = document.querySelector('#workqueue-qa-passed-table');
    const hotSettings = {
      data: orders,
      columns: [
        {
          data: 'code',
          editor: false,
          renderer: linkRenderer,
        },
        {
          data: 'status',
          editor: false,
          type: 'text',
          className: 'htRight',
        },
        {
          data: 'patient_name',
          editor: false,
          renderer: linkRenderer,
        },
        {
          data: 'submitted_date',
          editor: false,
          type: 'date',
          dateFormat: 'MM/DD/YYYY',
          correctFormat: true,
        },
        {
          data: 'report_pdf',
          editor: false,
          renderer: linkRenderer,
        },
        {
          data: 'received_date',
          editor: false,
          type: 'date',
          dateFormat: 'MM/DD/YYYY',
          correctFormat: true,
          className: 'htRight',
        },
        {
          data: 'approved_user',
          editor: false,
          type: 'text',
          className: 'htRight',
        },
      ],
      stretchH: 'all',
      //width: 806,
      height: 700,
      autoColumnSize: true,
      autoWrapRow: true,
      manualRowResize: false,
      manualColumnResize: false,
      columnSorting: true,
      sortIndicator: true,
      rowHeaders: false,
      colHeaders: true,
      nestedHeaders: [
        [
          'Order Code',
          'Status',
          'Patient Name',
          'Submitted Date',
          'Report PDF',
          'QA Passed Date',
          'QA User',
        ],
      ],
      manualRowMove: false,
      manualColumnMove: false,
      contextMenu: false,
      filters: true,
      dropdownMenu: ['filter_by_condition', 'filter_action_bar'],
      afterGetColHeader: function(col, TH) {
        TH.className = 'center';
      },
    };

    return new Handsontable(hotElement, hotSettings);
  } else {
    // load new data without replacing old table
    table.getPlugin('filters').clearConditions();
    table.loadData(orders);
    return table;
  }
}
