# /opt/python/bundle/deployment_number/app/myappname if using SSH to access to EC2 instance
import os
import dj_database_url
import sentry_sdk
import requests

# import environment variables from dotenv
from dotenv import load_dotenv
from sentry_sdk.integrations.django import DjangoIntegration

# environment variables saved before project root directory in local environment
dotenv_path = '.env'
load_dotenv(dotenv_path)

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
PROJECT_ROOT = os.path.dirname(os.path.abspath(__file__))

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = os.environ.get('SECRET_KEY')

# SECURITY WARNING: don't run with debug turned on in production!
if os.environ.get('DEBUG') == 'False':
    DEBUG = False
else:
    DEBUG = True
if os.environ.get('STAGING') == 'True':
    STAGING = True
else:
    STAGING = False

# see middleware.py
REMOVE_WWW = True

# if production and not staging, then auto SSL!
if not DEBUG and not STAGING:
    SECURE_SSL_REDIRECT = True
    SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')

# only allow sentry on production
if not DEBUG:
    # sentry logging
    sentry_sdk.init(
        dsn="https://b03dbed436504ee2ae84ce6e2ec24a3d@sentry.io/1316585",
        integrations=[DjangoIntegration()],
        environment="production"
    )


if not DEBUG or STAGING:
    ALLOWED_HOSTS = ['.dendi-lis-staging.herokuapp.com', '.apex-lis.herokuapp.com', '.dendisoftware.com',
                     '.dendi-staging.com', 'herokudns.com',
                     '.us-east-1.elasticbeanstalk.com', '.elasticbeanstalk.com', '.amazonaws.com', '127.0.0.1',
                     '.dendi-staging-1.com']
else:
    # allow all hosts on local environments
    ALLOWED_HOSTS = ['*']

#######################################################################################################################
# Elastic Beanstalk settings
try:
    internal_ip = requests.get('http://instance-data/latest/meta-data/local-ipv4').text
except requests.exceptions.ConnectionError:
    pass
else:
    ALLOWED_HOSTS.append(internal_ip)
del requests
#######################################################################################################################

# this is the User model that is subclassed from the AbstractUser
AUTH_USER_MODEL = 'lab_tenant.User'

TENANT_MODEL = "clients.Client"

SHARED_APPS = [
    'tenant_schemas', # mandatory, should always be before any django app
    'lab_tenant',
    'clients', # must list the app where tenant model resides
    'codes',
    'lab',
    'public',

    # Django autocomplete light
    'channels',
    'dal',
    'dal_select2',
    # Django apps
    'django.contrib.admin',
    # https://stackoverflow.com/questions/25468676/django-sites-model-what-is-and-why-is-site-id-1
    # 'django.contrib.sites',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.humanize',
    'django.contrib.sites',
    # Disable Django's own staticfiles handling in favour of WhiteNoise, for
    # greater consistency between gunicorn and `./manage.py runserver`. See:
    # http://whitenoise.evans.io/en/stable/django.html#using-whitenoise-in-development
    'whitenoise.runserver_nostatic',
    'django.contrib.staticfiles',
    # restframework
    'rest_framework',
    'rest_framework.authtoken',
    'auditlog',
    'timezone_field',
]

TENANT_APPS = [
    'django.contrib.contenttypes',
    'django.contrib.auth',
    'django.contrib.admin',

    # tenant-specific apps
    'channels',
    'lab_tenant',
    'accounts',
    'patients',
    'orders',
    'auditlog',
    # restframework
    'rest_framework',
    'rest_framework.authtoken',
]


# Application definition
INSTALLED_APPS = [
    'tenant_schemas', # mandatory, should always be before any django app
    # Shared apps
    'clients',
    'codes',
    'lab',
    'public',
    # Tenant apps
    'lab_tenant',
    'accounts',
    'patients',
    'orders',

    'channels',
    # Django autocomplete light
    'dal',
    'dal_select2',
    # Django apps
    'django.contrib.admin',
    # https://stackoverflow.com/questions/25468676/django-sites-model-what-is-and-why-is-site-id-1
    # 'django.contrib.sites',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.humanize',
    'django.contrib.sites',
    # Disable Django's own staticfiles handling in favour of WhiteNoise, for
    # greater consistency between gunicorn and `./manage.py runserver`. See:
    # http://whitenoise.evans.io/en/stable/django.html#using-whitenoise-in-development
    'whitenoise.runserver_nostatic',
    'django.contrib.staticfiles',
    # restframework
    'rest_framework',
    'rest_framework.authtoken',
    'storages',
    'django_tables2',
    'django_filters',
    'bootstrap4',
    'auditlog',
    'timezone_field',
]

MIDDLEWARE = [
    'tenant_schemas.middleware.TenantMiddleware',
    'django.middleware.security.SecurityMiddleware',
    'whitenoise.middleware.WhiteNoiseMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    # custom middleware written to remove 'www'
    'main.middleware.RemoveWWWMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'auditlog.middleware.AuditlogMiddleware',
]

ROOT_URLCONF = 'main.urls'
PUBLIC_SCHEMA_URLCONF = 'public.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            'public/templates/',
            'lab_tenant/templates/',
        ],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'django.template.context_processors.request'

            ],
            'debug': DEBUG,
        },
    },
]


WSGI_APPLICATION = 'main.wsgi.application'

DATABASES = {}
DATABASES['default'] = dj_database_url.config()
DATABASES['default']['CONN_MAX_AGE'] = 500
DATABASES['default']['ENGINE'] = 'tenant_schemas.postgresql_backend'

if len(DATABASES['default']) == 0:
    DATABASES = {
        'default': {
            'ENGINE': 'tenant_schemas.postgresql_backend',
            'NAME': os.environ.get('DEV_DB_NAME'),
            'USER': os.environ.get('DEV_DB_USER'),
            'PASSWORD': os.environ.get('DEV_DB_PASSWORD'),
            'HOST': os.environ.get('DEV_DB_HOST'),
            'PORT': os.environ.get('DEV_DB_PORT'),
        },
    }

DATABASE_ROUTERS = (
    'tenant_schemas.routers.TenantSyncRouter',
)

DEFAULT_FILE_STORAGE = 'tenant_schemas.storage.TenantFileSystemStorage'


AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]


# Internationalization

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = False


# Static files (CSS, JavaScript, Images)
STATIC_ROOT = os.path.join(PROJECT_ROOT, 'staticfiles')
STATIC_URL = '/static/'

# Extra places for collectstatic to find static files.
STATICFILES_DIRS = [
    os.path.join(PROJECT_ROOT, 'static'),
]

# Simplified static file serving.
# https://warehouse.python.org/project/whitenoise/
# STATICFILES_STORAGE = 'whitenoise.storage.CompressedManifestStaticFilesStorage'

REST_FRAMEWORK = {
    'DEFAULT_PERMISSION_CLASSES': (
        'rest_framework.permissions.IsAdminUser',
    ),
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'rest_framework.authentication.TokenAuthentication',
        'rest_framework.authentication.SessionAuthentication',
    ),
    'DEFAULT_PAGINATION_CLASS': 'rest_framework.pagination.LimitOffsetPagination',
    'PAGE_SIZE': 10,
}

GRAPHENE = {
    'SCHEMA': 'schema.schema'
}

# Recaptcha site-key and secret-key
RECAPTCHA_SITE_KEY = os.environ.get('RECAPTCHA_SITE_KEY')
RECAPTCHA_SECRET_KEY = os.environ.get('RECAPTCHA_SECRET_KEY')

# Time (in seconds) before the user should be logged out if inactive. Default is 30 min
SESSION_EXPIRE_AT_BROWSER_CLOSE = True
SESSION_COOKIE_AGE = 60 * 30
SESSION_SAVE_EVERY_REQUEST = True

AWS_ACCESS_KEY_ID = os.environ.get('S3_ACCESS_KEY')
AWS_SECRET_ACCESS_KEY = os.environ.get('S3_SECRET_KEY')
AWS_STORAGE_BUCKET_NAME = 'dendi-lis'

S3_URL = 'http://%s.s3.amazonaws.com' % AWS_STORAGE_BUCKET_NAME
MEDIA_DIRECTORY = '/media/'
MEDIA_URL = S3_URL + MEDIA_DIRECTORY

AWS_PRIVATE_MEDIA_LOCATION = 'media/private/'
AWS_PUBLIC_MEDIA_LOCATION = 'media/public/'
AWS_INTEGRATION_MEDIA_LOCATION = 'Mirth/Integrations/'
PRIVATE_FILE_STORAGE = 'mysite.storage_backends.PrivateMediaStorage'
AWS_DEFAULT_ACL = None

SITE_ID = 1

CELERY_BROKER_URL = os.environ.get('CELERY_BROKER_URL', 'amqp://localhost')
CELERY_RESULT_BACKEND = os.environ.get('CELERY_RESULT_BACKEND', 'amqp://localhost')
CELERY_ROUTES = {
    'lis.orders.tasks.receive_order': {'queue': 'identifiers'},
    'lis.orders.tasks.assign_order_code': {'queue': 'identifiers'},
}

# Channel Settings

ASGI_APPLICATION = "main.routing.application"
CHANNEL_LAYERS = {
    'default': {
        'BACKEND': 'channels_redis.core.RedisChannelLayer',
        'CONFIG': {
            "hosts": [os.environ.get("CHANNEL_HOST", 'redis://127.0.0.1:6379')],
        },
    },
}

# The maximum size in bytes that a request body may be before a SuspiciousOperation (RequestDataTooBig) is raised
DATA_UPLOAD_MAX_MEMORY_SIZE = 500*1024*1024  # N*1021*1024 = N Megabytes

# The maximum size (in bytes) that an upload will be before it gets streamed to the file system
FILE_UPLOAD_MAX_MEMORY_SIZE = 100*1024*1024


LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'handlers': {
        'null': {
            'class': 'logging.NullHandler',
        },
    },
    'loggers': {
        'django.security.DisallowedHost': {
            'handlers': ['null'],
            'propagate': False,
        },
    },
}

# Dendi's max allowable digits and decimal places
ALLOWED_MAX_DIGITS = 16
ALLOWED_DECIMAL_PLACES = 8

