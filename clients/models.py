import pytz

from django.db import models
from jsonfield import JSONField
from tenant_schemas.models import TenantMixin
from timezone_field import TimeZoneField

from main.storage_backends import PrivateMediaStorage

from lab_tenant.models import AuditLogModel
from auditlog.registry import auditlog


class Client(TenantMixin):
    name = models.CharField(max_length=255)
    paid_until = models.DateField()
    on_trial = models.BooleanField()
    created_on = models.DateField(auto_now_add=True)
    logo_file = models.FileField(storage=PrivateMediaStorage(), null=True)

    address_1 = models.CharField(max_length=128, blank=True)
    address_2 = models.CharField(max_length=128, blank=True)
    phone_number = models.CharField(max_length=128, blank=True)
    fax_number = models.CharField(max_length=128, blank=True)
    website = models.CharField(max_length=128, blank=True)
    clia_number = models.CharField(max_length=128, blank=True)
    clia_director = models.CharField(max_length=128, blank=True)

    # report preferences
    pcr_report_title = models.CharField(max_length=128, blank=True)
    epcr_report_title = models.CharField(max_length=128, blank=True)
    wb_report_title = models.CharField(max_length=128, blank=True)
    ifa_report_title = models.CharField(max_length=128, blank=True)
    lcms_report_title = models.CharField(max_length=128, blank=True)
    elisa_report_title = models.CharField(max_length=128, blank=True)

    # timezone is UTC by default
    timezone = TimeZoneField(default='UTC')

    lab_type = models.CharField(max_length=128, blank=True, default='POL')
    settings = JSONField(default={})


    # default true, schema will be automatically created and synced when it is saved
    auto_create_schema = True

    # id for EMRs
    results_outbound_id = models.CharField(max_length=128, blank=True, default='')

    # Receiving application for billing
    receiving_application = models.CharField(max_length=128, blank=True)

    def __str__(self):
        return "{} - {}".format(self.schema_name, self.name)


class ClientLeads(AuditLogModel):
    """
    Clients that have inputted information into schedule demo page
    """
    name = models.CharField(max_length=255, null=True)
    company_name = models.CharField(max_length=255, blank=True)
    email = models.CharField(max_length=255, null=True)
    phone_number = models.CharField(max_length=64, blank=True)

    def __str__(self):
        return self.name


class CLIALabs(AuditLogModel):
    """
    Labs in the CLIA database
    """
    name = models.CharField(max_length=500, blank=True)
    lab_type = models.CharField(max_length=255, blank=True)
    # name and lab type are permanent
    # the below fields can be updated
    phone_number = models.CharField(max_length=128, blank=True)
    certificate_expiration_date = models.DateField(null=True)
    certificate_type = models.CharField(max_length=255, blank=True)
    clia_number = models.CharField(max_length=255, blank=True)
    address = models.CharField(max_length=500, blank=True)
    city = models.CharField(max_length=255, blank=True)
    state = models.CharField(max_length=2, blank=True)
    zip_code = models.CharField(max_length=10, blank=True)

    def __str__(self):
        return self.name


class CLIALabsUpdateLog(AuditLogModel):
    """
    Updates every time the update CLIA lab script runs
    """
    # use a counter in script to count number of new labs in CLIA db
    new_lab_count = models.IntegerField(null=True)
    error_count = models.IntegerField(null=True)
    errors = models.TextField(blank=True)


######################################################################################################################
# Django AuditLogs

auditlog.register(ClientLeads)
