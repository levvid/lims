from django.db import connection
from django.utils import timezone
from rest_framework import serializers
from uuid import UUID, uuid4

from . import models, tasks, generate_hl7, generate_requisition_form
from orders.functions import generate_sample_code, add_bulk_results, add_containers
from accounts import models as accounts_models
from accounts import serializers as accounts_serializers
from clients import models as clients_models
from codes import serializers as codes_serializers
from patients import serializers as patients_serializers
from patients import models as patients_models
from lab import models as lab_models


# External API Serializers
class SampleSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = models.Sample
        fields = ('uuid', 'code', 'collection_date')


class TestTypeSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = models.TestType
        fields = ('uuid', 'name')


class TestSerializer(serializers.HyperlinkedModelSerializer):
    test_type = TestTypeSerializer()

    class Meta:
        model = models.Test
        fields = ('uuid', 'test_type')


class ResultSerializer(serializers.HyperlinkedModelSerializer):
    test = TestSerializer()

    class Meta:
        model = models.Result
        fields = ('uuid', 'result', 'result_quantitative', 'cutoff',
                  'test')


class ReportSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = models.Report
        fields = ('pdf_file', 'amended', 'preliminary', 'created_date')


class UserOrderSerializer(serializers.HyperlinkedModelSerializer):
    account = accounts_serializers.AccountSerializer()
    samples = SampleSerializer(many=True)
    results = ResultSerializer(many=True)
    reports = ReportSerializer(many=True)

    class Meta:
        model = models.Order
        fields = ('code',
                  'accession_number',
                  'samples',
                  'account',
                  'results',
                  'submitted_date',
                  'received_date',
                  'completed_date',
                  'report_date',
                  'accession_number',
                  'reports',
                  'report_date')


class IssueOrderSerializer(serializers.HyperlinkedModelSerializer):
    """
    Serializer used for Order model in Issues api
    """
    account = accounts_serializers.AccountSerializer()

    class Meta:
        model = models.Order
        fields = ('code',
                  'accession_number',
                  'account',
                  'submitted_date',
                  'received_date',
                  'completed_date',
                  'report_date',
                  'accession_number',
                  'report_date')


class UserTestPanelTypeSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = models.TestPanelType
        fields = ('uuid', 'name', 'price')


class UserTestTypeSerializer(serializers.HyperlinkedModelSerializer):
    required_samples = serializers.JSONField()

    class Meta:
        model = models.TestType
        fields = ('uuid', 'name', 'required_samples', 'loinc_code')


class TestPanelTypeSerializer(serializers.HyperlinkedModelSerializer):
    procedure_codes = codes_serializers.ProcedureCodeSerializer(many=True)

    class Meta:
        model = models.TestPanelType
        fields = ('uuid', 'name', 'procedure_codes', 'price')


class UserIssueSerializer(serializers.HyperlinkedModelSerializer):
    order = IssueOrderSerializer()

    class Meta:
        model = models.OrderIssue
        fields = ('uuid', 'order', 'is_resolved', 'is_public', 'description', 'issue_type',
                  'created_date', 'resolved_datetime')


# legacy serializers
class TestProfileTypeSerializer(serializers.HyperlinkedModelSerializer):
    procedure_codes = codes_serializers.ProcedureCodeSerializer(many=True)

    class Meta:
        model = models.TestPanelType
        fields = ('uuid', 'name', 'procedure_codes')


class TestPanelSerializer(serializers.HyperlinkedModelSerializer):
    test_panel_type = TestPanelTypeSerializer()

    class Meta:
        model = models.TestPanel
        fields = ('uuid', 'test_panel_type', 'expected_revenue', 'adjustment', 'received_revenue',
                  'uuid')


class OrderSerializer(serializers.HyperlinkedModelSerializer):
    # required
    account_uuid = serializers.CharField(required=True, write_only=True)
    provider_uuid = serializers.CharField(required=True, write_only=True)
    patient_uuid = serializers.CharField(required=True, write_only=True)
    collection_date = serializers.DateTimeField(required=True, write_only=True)
    test_panel_uuids = serializers.JSONField(required=True, write_only=True)
    # optional
    received = serializers.BooleanField(required=False, default=False, write_only=True)

    # read only
    code = serializers.CharField(read_only=True)
    accession_number = serializers.CharField(read_only=True)
    submitted_date = serializers.DateTimeField(read_only=True)
    received_date = serializers.DateTimeField(read_only=True)

    def validate_account_uuid(self, account_uuid):
        try:
            UUID(account_uuid, version=4)
            _ = accounts_models.Account.objects.get(uuid=account_uuid)
        except ValueError:
            raise serializers.ValidationError("Invalid UUID format for account_uuid.")
        except accounts_models.Account.DoesNotExist:
            raise serializers.ValidationError("Account matching UUID account_uuid does not exist.")
        return account_uuid

    def validate_provider_uuid(self, provider_uuid):
        try:
            UUID(provider_uuid, version=4)
            _ = accounts_models.Provider.objects.get(uuid=provider_uuid)
        except ValueError:
            raise serializers.ValidationError("Invalid UUID format for provider_uuid.")
        except accounts_models.Provider.DoesNotExist:
            raise serializers.ValidationError("Provider matching UUID provider_uuid does not exist.")
        return provider_uuid

    def validate_patient_uuid(self, patient_uuid):
        try:
            UUID(patient_uuid, version=4)
            _ = patients_models.Patient.objects.get(uuid=patient_uuid)
        except ValueError:
            raise serializers.ValidationError("Invalid UUID format for patient_uuid.")
        except patients_models.Patient.DoesNotExist:
            raise serializers.ValidationError("Provider matching UUID patient_uuid does not exist.")
        return patient_uuid

    def validate_test_panel_uuids(self, test_panel_uuids):
        try:
            [UUID(x, version=4) for x in test_panel_uuids]
            _ = [models.TestPanelType.objects.get(uuid=x) for x in test_panel_uuids]
        except ValueError:
            raise serializers.ValidationError("Invalid UUID format for uuid in test_panel_uuids")
        except models.TestPanelType.DoesNotExist:
            raise serializers.ValidationError("TestPanelType matching UUID in test_panel_uuids does not exist.")
        except TypeError:
            raise serializers.ValidationError("test_panel_uuids is required")
        return test_panel_uuids

    def validate(self, attrs):
        # convert provider_uuid to Provider object
        provider_uuid = attrs.get('provider_uuid')
        provider = accounts_models.Provider.objects.get(uuid=provider_uuid)
        attrs.pop('provider_uuid')
        attrs['provider'] = provider

        # convert account_uuid to Account object
        account_uuid = attrs.get('account_uuid')
        account = accounts_models.Account.objects.get(uuid=account_uuid)
        attrs.pop('account_uuid')
        attrs['account'] = account

        # convert patient_uuid to Account object
        patient_uuid = attrs.get('patient_uuid')
        patient = patients_models.Patient.objects.get(uuid=patient_uuid)
        attrs.pop('patient_uuid')
        attrs['patient'] = patient

        # convert test_panel_uuids to TestPanelType objects
        test_panel_uuids = attrs.get('test_panel_uuids')
        test_panel_types = [models.TestPanelType.objects.get(uuid=x) for x in test_panel_uuids]
        attrs.pop('test_panel_uuids')
        attrs['test_panel_types'] = test_panel_types

        # put patient payer information into attrs
        try:
            primary_patient_payer = patients_models.PatientPayer.objects.filter(is_primary=True, patient=patient)[0]
            attrs['primary_patient_payer'] = primary_patient_payer
        except IndexError:
            attrs['primary_patient_payer'] = None
        try:
            secondary_patient_payer = patients_models.PatientPayer.objects.filter(is_secondary=True, patient=patient)[0]
            attrs['secondary_patient_payer'] = secondary_patient_payer
        except IndexError:
            attrs['secondary_patient_payer'] = None
        try:
            tertiary_patient_payer = patients_models.PatientPayer.objects.filter(is_tertiary=True, patient=patient)[0]
            attrs['tertiary_patient_payer'] = tertiary_patient_payer
        except IndexError:
            attrs['tertiary_patient_payer'] = None
        try:
            workers_comp_payer = patients_models.PatientPayer.objects.filter(is_workers_comp=True, patient=patient)[0]
            attrs['workers_comp_payer'] = workers_comp_payer
        except IndexError:
            attrs['workers_comp_payer'] = None
        return attrs

    def create(self, validated_data):
        schema_name = connection.schema_name
        request = self.context.get('request')
        tenant = clients_models.Client.objects.get(schema_name=schema_name)
        order_result = tasks.create_order_with_code.apply_async(args=[tenant.id], queue='identifiers')
        order_id = order_result.get()
        order = models.Order.objects.get(id=order_id)

        received = validated_data.get('received')
        if received:
            accession_number_result = tasks.assign_accession_number.apply_async(
                args=[order_id, tenant.id],
                queue='identifiers')
            _ = accession_number_result.get()
            order = models.Order.objects.get(id=order_id)
            order.received_date = timezone.now()

        provider = validated_data.get('provider')
        account = validated_data.get('account')
        patient = validated_data.get('patient')

        primary_patient_payer = validated_data.get('primary_patient_payer')
        secondary_patient_payer = validated_data.get('secondary_patient_payer')
        tertiary_patient_payer = validated_data.get('tertiary_patient_payer')
        workers_comp_payer = validated_data.get('workers_comp_payer')
        patient_guarantor = validated_data.get('patient_guarantor')

        order.provider = provider
        order.account = account
        order.patient = patient
        order.submitted_date = timezone.now()
        order.submitted = True
        order.save()

        test_panel_types = validated_data.get('test_panel_types')
        requested_tests = []
        for test_panel_type in test_panel_types:
            test_types = test_panel_type.test_types.all()
            for test_type in test_types:
                requested_tests.append(str(test_type.id))
        requested_tests = list(set(requested_tests))

        # calculated required samples types
        samples_required = {}
        clia_sample_types = lab_models.CLIASampleType.objects.all()
        for test_type_id in requested_tests:
            test_type = models.TestType.objects.get(id=int(test_type_id))
            if test_type.required_samples:
                for clia_sample_type in clia_sample_types:
                    samples_required[str(clia_sample_type.id)] = max(samples_required.get(str(clia_sample_type.id), 0),
                                                                     test_type.required_samples.get(
                                                                         str(clia_sample_type.id), 0))
        for clia_sample_type in clia_sample_types:
            if samples_required.get(str(clia_sample_type.id), 0) > 0:
                latest_sample_code = generate_sample_code(order.code, clia_sample_type)
                sample = models.Sample.objects.create(
                    order=order,
                    code=latest_sample_code,
                    clia_sample_type=clia_sample_type,
                    collection_date=validated_data.get('collection_date')
                )

        # add requested tests to order
        enable_individual_analytes = request.tenant.settings.get('enable_individual_analytes', False)
        order_test_panels = []
        sample_volume_dict = {}
        result_list = []
        order_samples = order.samples.all().prefetch_related('clia_sample_type')
        for test_panel_type in test_panel_types:
            if enable_individual_analytes:
                test_panel_specific_tests = \
                    [x.id for x in models.TestType.objects.filter(testpaneltype=test_panel_type,
                                                                  id__in=requested_tests)]
                test_types = models.TestType.objects.filter(id__in=test_panel_specific_tests).all()
                if not test_types:
                    continue
            else:
                test_types = test_panel_type.test_types.all()

            test_panel = models.TestPanel.objects.create(order=order, test_panel_type=test_panel_type,
                                                         expected_revenue=test_panel_type.cost)
            order_test_panels.append(test_panel)

            for test_type in test_types:
                required_samples = test_type.required_samples
                if required_samples:
                    for required_sample_type_id in required_samples:
                        samples = order_samples.filter(clia_sample_type__id=required_sample_type_id).order_by(
                            'code')[
                                  :required_samples[required_sample_type_id]]
                        for sample in samples:
                            result_list.append((order, sample, test_panel, test_type))

                            # container logic
                            if test_type.container_type:
                                if test_type.min_volume:
                                    min_volume = test_type.min_volume
                                else:
                                    min_volume = 0
                                try:
                                    sample_volume_dict[sample][test_type.container_type] += min_volume
                                except KeyError:
                                    try:
                                        sample_volume_dict[sample][test_type.container_type] = min_volume
                                    except KeyError:
                                        sample_volume_dict[sample] = {}
                                        sample_volume_dict[sample][test_type.container_type] = min_volume
                else:
                    for sample in order_samples:
                        result_list.append((order, sample, test_panel, test_type))
        add_containers(sample_volume_dict, order)
        add_bulk_results(request, result_list)

        if received:
            # set samples and container received_date for in-house orders
            for sample in order.samples.all():
                sample.received_date = timezone.now()
                sample.save()
                for container in sample.containers.all():
                    container.received_date = timezone.now()
                    container.save()

        if primary_patient_payer:
            models.OrderPatientPayer.objects.create(order=order,
                                                    patient=patient,
                                                    is_primary=True,
                                                    payer=primary_patient_payer.payer,
                                                    payer_type=primary_patient_payer.payer_type,
                                                    group_number=primary_patient_payer.group_number,
                                                    member_id=primary_patient_payer.member_id,
                                                    subscriber_last_name=primary_patient_payer.subscriber_last_name,
                                                    subscriber_first_name=primary_patient_payer.subscriber_first_name,
                                                    subscriber_middle_initial=primary_patient_payer.subscriber_middle_initial,
                                                    subscriber_suffix=primary_patient_payer.subscriber_suffix,
                                                    subscriber_birth_date=primary_patient_payer.subscriber_birth_date,
                                                    subscriber_address1=primary_patient_payer.subscriber_address1,
                                                    subscriber_address2=primary_patient_payer.subscriber_address2,
                                                    subscriber_zip_code=primary_patient_payer.subscriber_zip_code,
                                                    subscriber_sex=primary_patient_payer.subscriber_sex,
                                                    subscriber_phone_number=primary_patient_payer.subscriber_phone_number,
                                                    subscriber_social_security=primary_patient_payer.subscriber_social_security,
                                                    subscriber_relationship=primary_patient_payer.subscriber_relationship)
        if secondary_patient_payer:
            models.OrderPatientPayer.objects.create(order=order,
                                                    patient=patient,
                                                    is_secondary=True,
                                                    payer=secondary_patient_payer.payer,
                                                    payer_type=secondary_patient_payer.payer_type,
                                                    group_number=secondary_patient_payer.group_number,
                                                    member_id=secondary_patient_payer.member_id,
                                                    subscriber_last_name=secondary_patient_payer.subscriber_last_name,
                                                    subscriber_first_name=secondary_patient_payer.subscriber_first_name,
                                                    subscriber_middle_initial=secondary_patient_payer.subscriber_middle_initial,
                                                    subscriber_suffix=secondary_patient_payer.subscriber_suffix,
                                                    subscriber_birth_date=secondary_patient_payer.subscriber_birth_date,
                                                    subscriber_address1=secondary_patient_payer.subscriber_address1,
                                                    subscriber_address2=secondary_patient_payer.subscriber_address2,
                                                    subscriber_zip_code=secondary_patient_payer.subscriber_zip_code,
                                                    subscriber_sex=secondary_patient_payer.subscriber_sex,
                                                    subscriber_phone_number=secondary_patient_payer.subscriber_phone_number,
                                                    subscriber_social_security=secondary_patient_payer.subscriber_social_security,
                                                    subscriber_relationship=secondary_patient_payer.subscriber_relationship)
        if tertiary_patient_payer:
            models.OrderPatientPayer.objects.create(order=order,
                                                    patient=patient,
                                                    is_tertiary=True,
                                                    payer=tertiary_patient_payer.payer,
                                                    payer_type=tertiary_patient_payer.payer_type,
                                                    group_number=tertiary_patient_payer.group_number,
                                                    member_id=tertiary_patient_payer.member_id,
                                                    subscriber_last_name=tertiary_patient_payer.subscriber_last_name,
                                                    subscriber_first_name=tertiary_patient_payer.subscriber_first_name,
                                                    subscriber_middle_initial=tertiary_patient_payer.subscriber_middle_initial,
                                                    subscriber_suffix=tertiary_patient_payer.subscriber_suffix,
                                                    subscriber_birth_date=tertiary_patient_payer.subscriber_birth_date,
                                                    subscriber_address1=tertiary_patient_payer.subscriber_address1,
                                                    subscriber_address2=tertiary_patient_payer.subscriber_address2,
                                                    subscriber_zip_code=tertiary_patient_payer.subscriber_zip_code,
                                                    subscriber_sex=tertiary_patient_payer.subscriber_sex,
                                                    subscriber_phone_number=tertiary_patient_payer.subscriber_phone_number,
                                                    subscriber_social_security=tertiary_patient_payer.subscriber_social_security,
                                                    subscriber_relationship=tertiary_patient_payer.subscriber_relationship)
        if workers_comp_payer:
            models.OrderPatientPayer.objects.create(order=order,
                                                    patient=patient,
                                                    is_workers_comp=True,
                                                    payer=workers_comp_payer.payer,
                                                    payer_type=workers_comp_payer.payer_type,
                                                    workers_comp_claim_id=workers_comp_payer.workers_comp_claim_id,
                                                    workers_comp_injury_date=workers_comp_payer.workers_comp_injury_date,
                                                    workers_comp_adjuster=workers_comp_payer.workers_comp_adjuster)
        if patient_guarantor:
            models.OrderPatientGuarantor.objects.create(order=order,
                                                        patient=patient,
                                                        user=patient_guarantor.user,
                                                        relationship_to_patient=patient_guarantor.relationship_to_patient,
                                                        suffix=patient_guarantor.suffix,
                                                        middle_initial=patient_guarantor.middle_initial,
                                                        sex=patient_guarantor.sex,
                                                        birth_date=patient_guarantor.birth_date,
                                                        address1=patient_guarantor.address1,
                                                        address2=patient_guarantor.address2,
                                                        zip_code=patient_guarantor.zip_code,
                                                        phone_number=patient_guarantor.phone_number,
                                                        social_security=patient_guarantor.social_security)

        # create hl7 message
        orders_out = generate_hl7.get_order_out_message(order.id, tenant.id)
        if orders_out:
            for offsite_laboratory_id in orders_out.keys():
                lab_models.OrdersOut.objects.create(tenant_id=tenant.id,
                                                    uuid=orders_out[offsite_laboratory_id]['uuid'],
                                                    offsite_laboratory_id=int(offsite_laboratory_id),
                                                    hl7_message=orders_out[offsite_laboratory_id]['message'])

        # generate the order requisition form
        generate_requisition_form.generate_requisition_PDF(request, order.uuid)

        return order

    class Meta:
        model = models.Order
        fields = (
            # read only
            'code',
            'accession_number',
            'submitted_date',
            'received_date',
            # required
            'account_uuid',
            'provider_uuid',
            'patient_uuid',
            'collection_date',
            'test_panel_uuids',
            # optional
            'received',
            )
