import os
import datetime
import sys

import django
import logging

from django.db.models import F, Q

os.environ['DJANGO_SETTINGS_MODULE'] = 'main.settings'
django.setup()

from tenant_schemas.utils import tenant_context
import clients.models as clients_models
import orders.models as orders_models
from orders import generate_billing_hl7_labrcm


TENANT_NAME = 'ildp'


def send_billing_messages():
    """
    :return:
    """
    tenant = clients_models.Client.objects.get(schema_name=TENANT_NAME)

    with tenant_context(tenant):
        # get all reported orders with requisition forms
        reported_orders = orders_models.Order.objects.filter(reported=True).exclude(
            Q(requisition_form='') | Q(requisition_form=None))  # get all reported orders

        print('orders {}'.format(len(reported_orders)))
        for order in reported_orders:
            print('Generating billing messages for order {}'.format(order.code))
            if os.environ.get('DEBUG') == 'False' and not os.environ.get('STAGING') == 'True':  # production environment
                generate_billing_hl7_labrcm.send_billing_message_message(tenant.id, order.uuid, preliminary=False,
                                                                  production_environment=True)
            else:  # staging and dev
                generate_billing_hl7_labrcm.send_billing_message_message(tenant.id, order.uuid, preliminary=False,
                                                                  production_environment=False)


if __name__ == '__main__':
    logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)
    send_billing_messages()
