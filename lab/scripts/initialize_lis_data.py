import django
import logging
import os
import sys

from django.db import transaction

os.environ['DJANGO_SETTINGS_MODULE'] = 'main.settings'
django.setup()

from clients.scripts.create_example_client import main as create_example_client
from clients.scripts.create_public_client import main as create_public_client
from codes.scripts.initialize_2018_icd10_codes import fill_2018_icd10_codes
from lab.scripts.initialize_lis_shared_data import fill_test_targets, fill_test_method

DEVELOPMENT_DOMAIN = 'localhost'
PRODUCTION_DOMAIN = 'dendisoftware.com'
DOMAIN_DICT = {'production': PRODUCTION_DOMAIN,
               'development': DEVELOPMENT_DOMAIN}

@transaction.atomic
def main(target_env='development'):
    """
    This script should be run after initial migrations have been made to initialize prerequisite data

    1. Create Public Tenant - Clients
    2. Create Several Example Tenants - Clients
    3. Create 2018 ICD10 code objects - Codes
    4. Create supported Test Method objects - Lab
    5. Create supported Test Target objects - Lab

    :return:
    """
    create_public_client(domain=DOMAIN_DICT[target_env])
    create_example_client(domain=DOMAIN_DICT[target_env])
    fill_2018_icd10_codes()
    fill_test_method()
    fill_test_targets()


if __name__ == '__main__':
    logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)
    if len(sys.argv) == 1:
        print("Please specify production or development")
    elif len(sys.argv) == 2 and sys.argv[1].lower() in ['production', 'development']:
        target_env = sys.argv[1].lower()
        logging.info("Initializing data for {}".format(target_env))
        main(target_env=target_env)
    else:
        print("Unknown arguments")