import django
import docraptor
import os
import tempfile
import time
import decimal

from django.template.loader import render_to_string
from django.utils import timezone
from celery.contrib import rdb

from main.functions import convert_utc_to_tenant_timezone, convert_utc_to_tenant_timezone_with_workers
from . import models
from patients import models as patients_models
from clients import models as clients_models
from lab_tenant.models import User

os.environ['DJANGO_SETTINGS_MODULE'] = 'main.settings'
django.setup()

BUCKET_NAME = 'dendi-lis'

docraptor.configuration.username = os.environ['DOCRAPTOR_API']
docraptor.configuration.debug = True

if os.environ.get('DEBUG') == 'False':
    DEBUG = False
else:
    DEBUG = True

MAX_HISTORICAL_RESULTS = 6

"""
Use docraptor Async create document call to generate pdf reports and save them to s3
"""


def get_patient_prescribed_medications(order):
    """

    :param order:
    :return:
    """
    patient = order.patient
    provider = order.provider
    try:
        prescribed_medication_history = patients_models.PatientPrescribedMedicationHistory.objects.filter(
            patient_id=patient.id, provider_id=provider if provider else None, order_id=order.id). \
            prefetch_related('prescribed_medications').order_by('-created_date').first()
        if prescribed_medication_history:
            prescribed_medications = prescribed_medication_history.prescribed_medications.all()
            return prescribed_medications
    except patients_models.PatientPrescribedMedicationHistory.DoesNotExist:
        prescribed_medications = None
    return None


def get_patient_prescribed_medications_list(prescribed_medications):
    """
    Get all the prescribed medications and their common names that the patient is taking.
    Also get the prescribed medications' maetabolites
    :param prescribed_medications:
    :return:
    """

    medications_list = []
    if not prescribed_medications:
        return medications_list

    for prescribed_medication in prescribed_medications:
        medications_list.append(prescribed_medication.name)
        common_name = prescribed_medication.common_name

        # add metabolites of the prescribed medications
        metabolites = prescribed_medication.metabolites.all().values_list('name', flat=True)
        if metabolites:
            medications_list.extend(metabolites)

        # add the common names for check
        if common_name:
            medications_list.extend(common_name)

    return medications_list


def remove_exponent(num):
    """
    Remove trailing zeros in decimals
    :param num:
    :return:
    """
    return num.to_integral() if num == num.to_integral() else num.normalize()


def get_expected(interpretation, analyte, medications_list, test_method_name, test):
    """
    Return Expected - 1) if patient is taking the meds and result == Positive then expected == YES,
                      2) if patient is taking meds and result == Negative then expected == NO - patient skipping meds
                      3) if patient is not taking meds and result == Positive, then expected == NO,
                      4) if patient is not taking meds and result == Negative, then expected == YES

    :param test:
    :param test_method_name:
    :param interpretation: str
    :param analyte: str
    :param medications_list [str]
    :return:
    """
    if test_method_name == 'Specimen Validity':
        range_low = test.test_type.test_type_ranges.get().range_low
        if range_low:
            range_low = remove_exponent(decimal.Decimal(range_low))

        range_high = test.test_type.test_type_ranges.get().range_high
        if range_high:
            range_high = remove_exponent(decimal.Decimal(range_high))

        expected = '{} - {}'.format(range_low, range_high)
    else:
        if any(analyte in medication for medication in medications_list):
            if interpretation == "Positive":  # 1)
                expected = 'Yes'
            else:  # 2) - interpretation is Negative
                expected = 'No'
        else:  # analyte not in prescribed meds
            if interpretation == "Positive":  # 3)
                expected = 'No'
            else:  # 4)
                expected = 'Yes'

    return expected


def get_inconsistent(interpretation, expected):
    """
    Inconsistent - if result == Positive and Expected == NO, then Inconsistent == YES,
                   if result == Negative and Expected == YES, then Inconsistent == YES
                   if result == Positive and Expected == YES, then inconsistent == NO,
                   if result == Negative and Expcected == NO
    :param interpretation: str
    :param expected: str
    :return:
    """
    if interpretation == 'Positive' and expected == 'No':
        inconsistent = 'Yes'
    else:
        inconsistent = 'No'

    return inconsistent


def get_color_code(inconsistent):
    """
    Returns interpretation, expected and inconsistent color codes.

    If inconsistent == Yes, Color == Red, else black
    :param inconsistent:
    :return:
    """

    color_code = 'black'

    if inconsistent == 'Yes':
        color_code = 'red'

    return color_code


def get_historical_results(recent_order, analyte_dict, sample_type, max_historical_results=MAX_HISTORICAL_RESULTS):
    """

    :param analyte_dict:
    :param max_historical_results: int
    :param recent_order:
    :return:
    """
    patient = recent_order.patient
    provider = recent_order.provider
    if provider is None:
        return None

    historical_orders = models.Order.objects.filter(patient=patient, provider=provider, report_date__isnull=False,
                                                    submitted_date__lte=recent_order.submitted_date,
                                                    tests__sample__clia_sample_type=sample_type) \
                            .exclude(id=recent_order.id).order_by('-report_date') \
                            .prefetch_related('tests__result', 'tests__test_type__test_method',
                                              'tests__sample', 'tests__sample__clia_sample_type',
                                              'tests__test_type', 'tests__result', 'tests__test_type__test_target',
                                              # 'tests_test_panel',
                                              'tests__test_type__instrument_calibrations', 'samples')[
                        :max_historical_results]

    historical_results = {}
    date_list = {}
    date_list_with_timezone = {}
    master_analyte_list = list(analyte_dict.keys())
    for order in historical_orders:
        # get analytes with inconsistent and +ves
        sample = order.samples.filter(clia_sample_type=sample_type).first()
        if sample:
            date_list[order.report_date.strftime('%Y-%m-%d %H:%M:%S')] = sample.code # if sample.code else ''  # in case no sample code is available
        else:
            date_list[order.report_date.strftime('%Y-%m-%d %H:%M:%S')] = ''
        date_list_with_timezone[order.report_date.strftime('%Y-%m-%d %H:%M:%S')] = order.report_date
        tests = order.tests.filter(sample__clia_sample_type=sample_type)
        test_methods = list(set([x.test_type.test_method for x in tests if x.is_approved and x.test_type]))
        prescribed_medications = get_patient_prescribed_medications(order)
        medications_list = get_patient_prescribed_medications_list(prescribed_medications)
        for test_method in test_methods:
            tests = order.tests.filter(is_approved=True, test_type__test_method=test_method,
                                       sample__clia_sample_type=sample_type). \
                order_by('sample__collection_date', 'sample__code')
            test_method_name = test_method.name
            for test in tests:
                result_type = test.test_type.result_type
                if result_type == 'Quantitative':
                    analyte = test.test_type.test_target.name  # drug name being tested for
                    result_value = get_result_value(test.result.get().result_quantitative,
                                                    test.test_type)  # test result value of sample
                    interpretation = test.result.get().result_label
                    expected = get_expected(interpretation, analyte, medications_list, test_method_name, test)
                    inconsistent = get_inconsistent(interpretation, expected)
                    upper_limit_of_quantification = test.result.get().upper_limit_of_quantification
                    if interpretation == 'Positive' or inconsistent == 'Yes':
                        if analyte not in master_analyte_list:  # add to master if not present
                            master_analyte_list.append(analyte)

                        interpretation = interpretation.upper() if 'Positive' else interpretation  # change to
                        # uppercase for Positive
                        result = {'inconsistent': inconsistent,
                                  'interpretation': interpretation if interpretation else 'Not Detected',
                                  'result_value': remove_exponent(
                                      decimal.Decimal(result_value)) if result_value else '',
                                  # remove trailing zeros if not null
                                  'upper_limit_of_quantification': upper_limit_of_quantification}
                        try:
                            historical_results[analyte][order.report_date.strftime('%Y-%m-%d %H:%M:%S')] = result
                        except KeyError:
                            historical_results[analyte] = {order.report_date.strftime('%Y-%m-%d %H:%M:%S'): result}

    historical_result_dict = {}
    for analyte in master_analyte_list:
        results_dict = {}
        for report_date in date_list.keys():
            try:
                result = historical_results[analyte][report_date]
                if result['inconsistent'] == 'Yes':  # if inconsistent add *
                    if result['interpretation'] == 'POSITIVE':  # if +ve add result_value, make text upper
                        if result['result_value'] and str(result['result_value']).strip() != '' and result[
                            'upper_limit_of_quantification'] and result['result_value'] >= result[
                             'upper_limit_of_quantification']:
                            value = '{}* (>{})'.format(result['interpretation'], remove_exponent(
                                decimal.Decimal(result['upper_limit_of_quantification'])))
                        else:
                            try:
                                value = '{}* ({})'.format(result['interpretation'],
                                                          remove_exponent(decimal.Decimal(result['result_value'])))
                            except (ValueError, decimal.DecimalException):
                                value = '{}*'.format(result['interpretation'])
                    else:
                        value = '{}*'.format(result['interpretation'])
                else:  # inconsistent == No
                    if result['result_value'] and result['upper_limit_of_quantification'] and result_value >= result[
                         'upper_limit_of_quantification']:
                        value = '{} (>{})'.format(result['interpretation'], result['upper_limit_of_quantification'])
                    else:
                        value = '{} ({})'.format(result['interpretation'], result['result_value'])
            except KeyError:
                value = 'Not Detected'

            results_dict['{}'.format(report_date)] = value

        historical_result_dict[analyte] = results_dict

    return historical_result_dict, date_list, date_list_with_timezone


def get_result_value(result_value, test_type):
    """
    Return rounded result value
    :param test_type:
    :param result_value:
    :return:
    """
    # only use round figures if the test type is set to use them
    if test_type.use_round_figures:
        if test_type.round_figures or test_type.round_figures == 0:
            try:
                result_value = round(result_value, test_type.round_figures)
            except Exception as e:
                pass

    return result_value


def get_remarks(order, offsite_laboratory=None):
    """

    :param order:
    :return:
    """
    remarks = []
    report_remarks = models.ReportRemarks.objects.all()
    tests = order.tests.filter(is_approved=True)

    if report_remarks:
        for report_remark in report_remarks:
            test_target = report_remark.test_target

            account = report_remark.account
            if test_target:
                tests = tests.filter(test_type__test_target=test_target)
            if offsite_laboratory:
                tests = tests.filter(test_type__offsite_laboratory=offsite_laboratory)
            if account:
                if order.account != account:
                    continue

            if tests.count() > 0:
                remarks.append(report_remark)

    return remarks


def generate_pdf_report(tenant_id, order_uuid, approved_by_id, order_report_date, preliminary=False):
    tenant = clients_models.Client.objects.get(id=tenant_id)
    order = models.Order.objects.prefetch_related('tests__result', 'tests__sample', 'test_panels__test_panel_type',
                                                  'tests__test_type__test_type_comments',
                                                  'test_panels__test_panel_type__test_panel_type_comments',
                                                  'tests__sample__clia_sample_type', 'tests__test_type__test_method',
                                                  'tests__test_type',
                                                  'tests__test_type__test_target',
                                                  'tests__test_type__instrument_calibrations',
                                                  'tests__test_type__test_type_ranges',
                                                  'order_test_comments', 'order_test_panel_comments').get(uuid=order_uuid)
    approved_by = User.objects.get(id=approved_by_id)
    sample = models.Sample.objects.filter(order_id=order.id).first()  # change to accommodate more than one sample
    samples = models.Sample.objects.filter(order_id=order.id)
    # sort order for sample types when generating report, saliva comes last ...etc
    sample_type_sort_order = ['Urine', 'Saliva', 'Other']
    sample_types_unordered = list(set([x.clia_sample_type for x in samples if x.clia_sample_type]))
    sample_types = [x for y in sample_type_sort_order for x in sample_types_unordered if x.name == y]

    test_panel_ids = list(order.test_panels.all().values_list('id', flat=True))
    test_panel_types = models.TestPanelType.objects.filter(test_panels__in=test_panel_ids)
    test_panel_type_comments = models.TestPanelTypeComment.objects.filter(test_panel_type__in=test_panel_types,
                                                                          is_public=True)
    order_test_panel_comments = models.OrderTestPanelComment.objects.filter(order=order,
                                                                            is_public=True)
    tests = order.tests.all()
    test_methods = list(set([x.test_type.test_method for x in tests
                             if x.is_approved and x.test_type and not x.test_type.is_internal]))
    prescribed_medications = get_patient_prescribed_medications(order)
    medications_list = get_patient_prescribed_medications_list(prescribed_medications)
    certification_date = order_report_date if order_report_date else timezone.now()
    specimen_validity_summary = order.validity_string

    specimen_validity_result_list = []
    drug_screen_results_list = []
      # inconsistent and positive analyte list
    num_tests_with_results = 0
    # order issues
    issues = models.OrderIssue.objects.filter(order=order, is_public=True, is_resolved=False)
    # for issue in issues:  # use to generate
    #     issue.resolved_datetime = convert_utc_to_tenant_timezone(request, issue.resolved_datetime)
    sample_type_results_dict = {}
    for sample_type in sample_types:
        remarks = []
        detailed_results_dict = {}
        summary_results_dict = {}
        analyte_dict = {}
        for test_method in test_methods:
            tests = order.tests.filter(is_approved=True, test_type__is_internal=False, test_type__test_method=test_method,
                                       sample__clia_sample_type=sample_type). \
                order_by('sample__collection_date', 'sample__code')
            test_method_name = test_method.name
            positive_above_cutoff = test_method.positive_above_cutoff
            for test in tests:
                result_type = test.test_type.result_type
                # test type comments
                test_type_comments = test.test_type.test_type_comments.all()
                test_type_comment_list = [test_type_comment.comment for test_type_comment in test_type_comments if
                                          test_type_comment.is_public]
                order_test_comments = models.OrderTestComment.objects.filter(order=order,
                                                                             test=test,
                                                                             is_public=True)
                order_test_comment_list = [comment.comment for comment in order_test_comments]

                if test.test_type.offsite_laboratory:  # add offsite lab results later after EMR implementation
                    remarks = get_remarks(order, test.test_type.offsite_laboratory)
                if result_type == 'Quantitative':
                    try:
                        cutoff_value = decimal.Decimal(
                            test.test_type.test_type_ranges.filter(default=True).get().range_high)
                    except models.TestTypeRange.DoesNotExist:
                        cutoff_value = None  # if cutoff is not provided

                    # Use instrument calibrations unit if test_type is unavailable
                    # 'None' string must be validated
                    # 2020-03-02 note: 'None' string shouldn't be coming through anymore going forward
                    if not test.test_type.unit or test.test_type.unit == 'None':
                        unit_of_measurement = ''
                    else:
                        unit_of_measurement = test.test_type.unit

                    analyte = test.test_type.test_target.name  # drug name being tested for
                    drug_category = test.test_type.test_target.category
                    result_value = get_result_value(test.result.get().result_quantitative,
                                                    test.test_type)  # test result value of sample
                    if result_value:
                        num_tests_with_results += 1
                    else:
                        print('Result value: {}'.format(result_value))
                    interpretation = test.result.get().result_label
                    expected = get_expected(interpretation, analyte, medications_list, test_method_name, test)
                    inconsistent = get_inconsistent(interpretation, expected)
                    color_code = get_color_code(inconsistent)
                    upper_limit_of_quantification = test.result.get().upper_limit_of_quantification

                    if interpretation is None:  # if no interpretation value, have ba
                        interpretation = ''
                    elif interpretation == 'Negative':
                        interpretation = 'Not Detected'

                    # historical results
                    if interpretation == 'Positive' or inconsistent == 'Yes':
                        result = {'inconsistent': inconsistent,
                                  'interpretation': interpretation,
                                  'result_value': result_value}

                        try:
                            analyte_dict[analyte].append(result)
                        except KeyError:
                            analyte_dict[analyte] = []
                            analyte_dict[analyte].append(result)
                    #  end of historical results

                    if test_method_name == 'Specimen Validity':
                        result = {'test': analyte,
                                  'result_value': remove_exponent(decimal.Decimal(result_value)) if result_value else '',
                                  'expected': expected,
                                  'test_type_comments': test_type_comment_list,
                                  'unit_of_measurement': unit_of_measurement if unit_of_measurement and result_value else ''}

                        specimen_validity_result_list.append(result)

                    elif test_method_name == 'Chemistry-Immunoassay':
                        result = {'test': analyte,
                                  'cutoff': '{} {}'.format(remove_exponent(decimal.Decimal(cutoff_value)),
                                                           unit_of_measurement) if cutoff_value else '',
                                  'test_type_comments': test_type_comment_list,
                                  'interpretation': interpretation}

                        drug_screen_results_list.append(result)
                    else:
                        # show results above and below linearity for Allied Healthcare
                        if order.account.name == 'Allied Healthcare' and tenant.name == 'Industry Lab Diagnostic Partners':
                            if result_value:
                                result_for_report = '{} {}'.format(remove_exponent(decimal.Decimal(result_value)),
                                                                   unit_of_measurement)
                        else:
                            if result_value and interpretation == 'Positive':
                                if upper_limit_of_quantification:
                                    if result_value >= upper_limit_of_quantification:  # if result is above ULOC, show >ULOC
                                        result_for_report = '>{} {}'.format(
                                            remove_exponent(decimal.Decimal(upper_limit_of_quantification)),
                                            unit_of_measurement)
                                    else:
                                        result_for_report = '{} {}'.format(remove_exponent(decimal.Decimal(result_value)),
                                                                           unit_of_measurement)
                                else:  # when no ULOC is provided
                                    result_for_report = '{} {}'.format(remove_exponent(decimal.Decimal(result_value)),
                                                                       unit_of_measurement)
                            else:  # negative results
                                result_for_report = ''

                        result = {'analyte': analyte,
                                  'result_value': result_for_report,
                                  'cutoff': '{} {}'.format(remove_exponent(decimal.Decimal(cutoff_value)),
                                                           unit_of_measurement) if cutoff_value else '',
                                  # Empty if cutoff is not provided
                                  'interpretation': interpretation,
                                  # if cutoff was not provided then interpretation will not exist
                                  'expected': expected,
                                  'inconsistent': inconsistent,
                                  'test_type_comments': test_type_comment_list,
                                  # this is on Test object and not TestType
                                  'order_test_comments': order_test_comment_list,
                                  'color_code': color_code}

                        # get results summary
                        # Do not show Tox Screen in summary
                        if interpretation and (interpretation == 'Positive' or inconsistent == 'Yes') and test.is_approved \
                            and result_value and result_value != 'Not Detected' and test_method_name != 'Toxicology Screening':  # add to results summary
                            try:
                                summary_results_dict[result_type][test_method_name].append(result)
                            except KeyError:
                                try:
                                    summary_results_dict[result_type][test_method_name] = []
                                    summary_results_dict[result_type][test_method_name].append(result)
                                except KeyError:
                                    summary_results_dict[result_type] = {test_method_name: [result]}

                        # detailed results
                        try:
                            detailed_results_dict[result_type][test_method_name][drug_category].append(result)
                        except KeyError:
                            try:
                                detailed_results_dict[result_type][test_method_name][drug_category] = []
                                detailed_results_dict[result_type][test_method_name][drug_category].append(result)
                            except KeyError:
                                try:
                                    detailed_results_dict[result_type][test_method_name] = {drug_category: [result]}
                                except KeyError:
                                    detailed_results_dict[result_type] = {test_method_name: {drug_category: [result]}}

                else:
                    analyte = test.test_type.test_target.name  # drug name being tested for
                    collection_date = test.sample.collection_date.strftime('%Y-%m-%d')
                    sample_code = '-'.join(test.sample.code.split('-')[-2:])
                    sample_type_string = test.sample.clia_sample_type.name
                    result_label = test.result.get().result_label

                    if len(result_label.strip()) > 0:
                        num_tests_with_results += 1
                    result = {'analyte': analyte,
                              'collection_date': collection_date,
                              'sample_code': sample_code,
                              'sample_type': sample_type_string,
                              'result_label': result_label}
                    if test_method_name != 'Toxicology Screening':
                        try:
                            summary_results_dict[result_type][test_method_name].append(result)
                        except KeyError:
                            try:
                                summary_results_dict[result_type][test_method_name] = []
                                summary_results_dict[result_type][test_method_name].append(result)
                            except KeyError:
                                summary_results_dict[result_type] = {test_method_name: [result]}

        try:
            historical_results, historical_dates, historical_datetimes = get_historical_results(order, analyte_dict, sample_type)
        except TypeError:  # historical results do not exist for patient and provider
            historical_results = None
            historical_dates = None
            historical_datetimes = None

        summary_results = list(zip(summary_results_dict.keys(), summary_results_dict.values()))
        print('Results {}'.format(summary_results))
        detailed_results = list(zip(detailed_results_dict.keys(), detailed_results_dict.values()))
        sample = models.Sample.objects.filter(order_id=order.id, clia_sample_type=sample_type).first()  # change to accommodate more than one sample
        sample_type_results_dict[sample_type.name] = {'order': order,
                                                      'tenant': tenant,
                                                      'summary_results': summary_results,
                                                      'detailed_results': detailed_results,
                                                      'historical_results': historical_results,
                                                      'historical_dates': historical_dates,
                                                      'historical_datetimes': historical_datetimes,
                                                      'sample_code': sample.code,
                                                      'certification_date': certification_date,
                                                      'approved_by': approved_by,
                                                      'preliminary': preliminary,
                                                      'remarks': list(set(remarks))}
    save_file_name = "reports/{}/{}/{}.pdf".format(tenant.name, order.uuid, timezone.now())

    doc_api = docraptor.DocApi()
    reports = [1]
    amended = False
    if models.Report.objects.filter(order=order, preliminary=False):
        amended = True

    report_generation_date = str(convert_utc_to_tenant_timezone_with_workers(tenant.id, timezone.now()))
    rendered_html = render_to_string('order_pdf_report.html', {'sample_type_results_dict': sample_type_results_dict,
                                                                'order': order,
                                                                'sample_types': sample_types,
                                                                'tenant': tenant,
                                                                'reports': reports,
                                                                'specimen_validity_summary': specimen_validity_summary,
                                                                'specimen_validity_results': specimen_validity_result_list,
                                                                'drug_screen_results': drug_screen_results_list,
                                                                'prescribed_medications': prescribed_medications,
                                                                'notes': order.notes,
                                                                'test_panel_type_comments': test_panel_type_comments,
                                                                'order_test_panel_comments': order_test_panel_comments,
                                                                'amended': amended,
                                                                'issues': issues,
                                                                'preliminary': preliminary,
                                                                'certification_date': certification_date,
                                                                'report_generation_date': report_generation_date,
                                                                'approved_by': approved_by,
                                                                })

    response = doc_api.create_async_doc({
        "test": DEBUG,  # test documents are free but watermarked
        "document_content": rendered_html,  # supply content directly
        "name": save_file_name,
        "document_type": "pdf",
        # "javascript": True,  # enable JavaScript processing
        "prince_options": {
            "media": 'screen',
            "javascript": True,  # use Prince's js option to access custom Prince addScriptFunc function for the date
        },
    })

    # async loop
    while True:
        status_response = doc_api.get_async_doc_status(response.status_id)
        if status_response.status == "completed":
            doc_response = doc_api.get_async_doc(status_response.download_id)
            with tempfile.NamedTemporaryFile() as temp:
                temp.write(doc_response)

                # if there's already a Report for the order, then the next report must be "amended"
                if models.Report.objects.filter(order=order, preliminary=False):
                    report = models.Report(order=order, amended=True, preliminary=preliminary, approved_by=approved_by)
                else:
                    report = models.Report(order=order, preliminary=preliminary, approved_by=approved_by)
                report.pdf_file.save(save_file_name, temp)
            break
        elif status_response.status == "failed":
            print("FAILED")
            print(status_response)
            break
        else:
            time.sleep(1)

    return num_tests_with_results
