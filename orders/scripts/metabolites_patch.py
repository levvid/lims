import django

import logging
import os
import sys

from django.db import transaction

os.environ['DJANGO_SETTINGS_MODULE'] = 'main.settings'
django.setup()

from lab import models as lab_models


@transaction.atomic
def add_metabolites_as_common_names():
    """
    Temp fix to update test targets to have metabolites as common names
    :return:
    """
    lab_models.TestTarget.objects.update_or_create(name='Alprazolam',
                                                   defaults={'common_name': ['Xanax', 'alpha-Hydroxyalprazolam']})
    lab_models.TestTarget.objects.update_or_create(name='Buprenorphine',
                                                   defaults={'common_name': ['BUP', 'Buprenex', 'Norbuprenorphine']})
    lab_models.TestTarget.objects.update_or_create(name='Suboxone',
                                                   defaults={'common_name': ['Naloxone and Buprenorphine',
                                                                             'Norbuprenorphine']})
    lab_models.TestTarget.objects.update_or_create(name='Fentanyl',
                                                   defaults={'common_name': ['Actiq', 'Duragesic', 'Norfentanyl']})
    lab_models.TestTarget.objects.update_or_create(name='Meperidine',
                                                   defaults={'common_name': ['Demerol', 'Normeperidine']})
    lab_models.TestTarget.objects.update_or_create(name='Methadone',
                                                   defaults={'common_name': ['MTD', 'Dolophine', 'EDDP']})
    lab_models.TestTarget.objects.update_or_create(name='Oxycodone',
                                                   defaults={'common_name': ['OXY', 'Percocet', 'Noroxycodone']})
    lab_models.TestTarget.objects.update_or_create(name='Propoxyphene',
                                                   defaults={'common_name': ['Darvocet', 'Norpropoxyphene']})

    lab_models.TestTarget.objects.update_or_create(name='Tramadol',
                                                   defaults={'common_name': ['Ultram', 'O-Desmethyltramadol']})

    lab_models.TestTarget.objects.update_or_create(name='Amitriptyline',
                                                   defaults={'common_name': ['Elavil', 'Nortriptyline']})
    lab_models.TestTarget.objects.update_or_create(name='Clomipramine',
                                                   defaults={'common_name': ['Anafranil', 'N-Desmethylclomipramine']})

    lab_models.TestTarget.objects.update_or_create(name='Doxepin',
                                                   defaults={'common_name': ['Aponal', 'Desmethyldoxepin']})
    lab_models.TestTarget.objects.update_or_create(name='Imipramine',
                                                   defaults={'common_name': ['Tofranil', 'Desipramine']})

    lab_models.TestTarget.objects.update_or_create(name='Diazepam', defaults={
        'common_name': ['Valium', 'Oxazepam', 'Temazepam', 'Nordiazepam']})
    lab_models.TestTarget.objects.update_or_create(name='Clonazepam',
                                                   defaults={'common_name': ['Klonopin', '7-Aminoclonazepam'],
                                                             'category': 'Benzodiazepines',
                                                             'is_medication': True})

    lab_models.TestTarget.objects.update_or_create(name='THC', defaults={
        'common_name': ['Marijuana', 'Medical Marijuana', 'THC-COOH']})

    lab_models.TestTarget.objects.update_or_create(name='Hydrocodone',
                                                   defaults={'common_name': ['Vicodin, Norco', 'Norhydrocodone']})

    lab_models.TestTarget.objects.update_or_create(name='Methylphenidate',
                                                   defaults={'common_name': ['Ritalin', 'Ritalinic Acid']})

    lab_models.TestTarget.objects.update_or_create(name='Ritalinic Acid',
                                                   is_medication=False,
                                                   defaults={'common_name': ['Ritalin'],
                                                             'is_medication': False})


@transaction.atomic
def remove_metabolites_as_common_names():
    """
    Update test targets to have metabolites as common names
    :return:
    """
    lab_models.TestTarget.objects.update_or_create(name='Alprazolam',
                                                   defaults={'common_name': ['Xanax']})
    lab_models.TestTarget.objects.update_or_create(name='Buprenorphine',
                                                   defaults={'common_name': ['BUP', 'Buprenex', 'Butrans', 'Sublocade',
                                                                             'Subutex']})
    lab_models.TestTarget.objects.update_or_create(name='Suboxone',
                                                   defaults={'common_name': ['Naloxone and Buprenorphine']})
    lab_models.TestTarget.objects.update_or_create(name='Fentanyl',
                                                   defaults={'common_name': ['Actiq', 'Duragesic']})
    lab_models.TestTarget.objects.update_or_create(name='Meperidine',
                                                   defaults={'common_name': ['Demerol']})
    lab_models.TestTarget.objects.update_or_create(name='Methadone',
                                                   defaults={'common_name': ['MTD', 'Dolophine']})
    lab_models.TestTarget.objects.update_or_create(name='Oxycodone',
                                                   defaults={'common_name': ['OXY', 'Percocet']})
    lab_models.TestTarget.objects.update_or_create(name='Propoxyphene',
                                                   defaults={'common_name': ['Darvocet']})

    lab_models.TestTarget.objects.update_or_create(name='Tramadol',
                                                   defaults={'common_name': ['Ultram']})

    lab_models.TestTarget.objects.update_or_create(name='Amitriptyline',
                                                   defaults={'common_name': ['Elavil']})
    lab_models.TestTarget.objects.update_or_create(name='Clomipramine',
                                                   defaults={'common_name': ['Anafranil']})

    lab_models.TestTarget.objects.update_or_create(name='Doxepin',
                                                   defaults={'common_name': ['Aponal']})
    lab_models.TestTarget.objects.update_or_create(name='Imipramine',
                                                   defaults={'common_name': ['Tofranil']})

    lab_models.TestTarget.objects.update_or_create(name='Diazepam', defaults={'common_name': ['Valium']})
    lab_models.TestTarget.objects.update_or_create(name='Clonazepam',
                                                   defaults={'common_name': ['Klonopin']})

    lab_models.TestTarget.objects.update_or_create(name='THC', defaults={
        'common_name': ['Marijuana', 'Medical Marijuana', 'THC-COOH']})

    lab_models.TestTarget.objects.update_or_create(name='Hydrocodone',
                                                   defaults={'common_name': ['Vicodin, Norco']})

    lab_models.TestTarget.objects.update_or_create(name='Methylphenidate',
                                                   defaults={'common_name': ['Ritalin']})

    lab_models.TestTarget.objects.update_or_create(name='Ritalinic Acid',
                                                   defaults={'common_name': ['Ritalin'],
                                                             'is_medication': False})


if __name__ == '__main__':
    logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)
    # add_metabolites_as_common_names()
    remove_metabolites_as_common_names()
