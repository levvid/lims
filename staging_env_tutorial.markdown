<h3>Staging Environment</h3>

Some points to consider:
- The LIS is under the <i>dendisoftware</i> team.
- Both staging and production environments are on Heroku.
- Staging and production environments are on separate apps altogether.
- Demos for prospective clients should be created in the <i>staging</i> environment, not production. We want to do this 
in order to not "pollute" production. Production is sacred!


<h3>Heroku Setup</h3>
 
- Join the dendisoftware team on Heroku.
 
- Typically, Heroku assumes that you are using your personal account, so we'll have to make your local environment
default to our dendisoftware team account, so that when you ```git push heroku master``` you are deploying to the 
correct app. <br>

- Set your local default Heroku environment to point to the team by going into your bash profile: 
```sudo nano ~/.bashrc``` (Unix) or ```sudo nano ~/.bash_profile``` (Mac). 
<br><br>Then add the following to the bash profile:<br>
```
# Configuration for Heroku
export HEROKU_ORGANIZATION=dendisoftware
```
- Restart your bash profile or open up a new terminal session for the previous changes to go into effect. Now you should be 
automatically connected to the team Heroku environment.

- Upgrade your git config so that your Heroku remotes point towards the correct repo. Open up
your git config file using ```sudo nano .git/config```, and make sure your git config looks 
something like this:
```
[remote "staging"]
        url = https://git.heroku.com/dendi-lis-staging.git
        fetch = +refs/heads/*:refs/remotes/staging/*
[remote "heroku"]
        url = https://git.heroku.com/apex-lis.git
        fetch = +refs/heads/*:refs/remotes/heroku/*

```

This means you can also run also run ```heroku run``` commands on staging (use -r flag):
```
heroku run -r staging python manage.py [action_here]
```

<h3>Staging Workflow</h3>
- Create a snapshot of the AWS RDS instance and modify it to be appropriate
- AWS DMS (Database Migration Service) doesn't seem to work. There were errors when performing
migrations (NULL values for IDs) 
- When the production db is copied to staging, run the following script to complete conversion:
```
heroku run -r staging python -m staging_environment.convert_production_to_staging_client staging
```

<h3>Deploy to Staging</h3>
Prior to pushing to staging (git push staging staging:master), make sure the staging db is ready
and up-to-date. Then run: ```git push staging staging:master``` to push to staging.

<h3>Deploy to Production</h3>
```git push heroku master```


<h3>Creating Demos on Staging</h3>
- Run the following on the staging Heroku dyno to create a new demo:
```
heroku run -r staging python -m demos.scripts.your_demo_name
```

<h3>Note About Heroku Post-Deploy Scripts</h3>
I've utilized a pre-existing Heroku buildpack to help this process: 
https://elements.heroku.com/buildpacks/heroku/heroku-buildpack-multi-procfile#buildpack-instructions
<br><br>
What this buildpack allows us to do is to use 2 different Procfiles for each "type" of deployment.
This also means that we can utilize a custom Python post-deploy script if we do 
```git push staging master``` and fix the client URL issue (which copies the production db, so it's 
always being linked as dendisoftware.com until we change it to point to the staging URL). 
<br><br>











