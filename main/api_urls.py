from django.urls import include, path

from rest_framework_nested import routers
from rest_framework.authtoken.views import obtain_auth_token

from accounts import data_views as accounts_data_views
from accounts import viewsets as accounts_viewsets
from codes import viewsets as codes_viewsets
from orders import data_views as orders_data_views
from orders import viewsets as orders_viewsets
from patients import data_views as patients_data_views
from patients import viewsets as patients_viewsets
from lab import data_views as lab_data_views
from lab import viewsets as lab_viewsets
from lab_tenant import data_views as lab_tenant_data_views

# api urls
router = routers.DefaultRouter()
# apis filtered by user (Mako exchange apis)
router.register(r'user/orders', orders_viewsets.UserOrderViewSet, basename='user-orders')
router.register(r'user/patients', patients_viewsets.UserPatientViewSet, basename='user-patients')
router.register(r'user/accounts', accounts_viewsets.UserAccountViewSet, basename='user-accounts')
router.register(r'user/issues', orders_viewsets.UserIssueViewSet, basename='user-issues')
router.register(r'user/samples', orders_viewsets.UserSampleViewSet, basename='user-samples')
router.register(r'patients', patients_viewsets.PatientCreateViewSet, basename='patients')
router.register(r'patient_payers', patients_viewsets.PatientPayerViewSet,
                basename='patient_payers')
router.register(r'patient_guarantors', patients_viewsets.PatientGuarantorViewSet,
                basename='patient_guarantors')
router.register(r'test_panel_types', orders_viewsets.TestPanelTypeViewSet, basename='test_panel_types')
router.register(r'collectors', accounts_viewsets.CollectorViewSet, basename='collectors')
router.register(r'procedure_codes', codes_viewsets.ProcedureCodeViewSet, basename='procedure_codes')
router.register(r'payers', patients_viewsets.PayerViewSet, basename='payers')
router.register(r'orders', orders_viewsets.OrderViewSet, basename='orders')
router.register(r'autocomplete/accounts', accounts_viewsets.UserAccountAutocompleteViewSet,
                basename='user-accounts-autocomplete')
router.register(r'autocomplete/medications', lab_viewsets.UserMedicationAutocompleteViewSet,
                basename='user-medications-autocomplete')
router.register(r'autocomplete/payers', patients_viewsets.PayerAutocompleteViewSet,
                basename='payers-autocomplete')
router.register(r'autocomplete/test_types', orders_viewsets.UserTestTypeAutocompleteViewSet,
                basename='user-test_types_autocomplete')
router.register(r'autocomplete/test_panel_types', orders_viewsets.UserTestPanelTypeAutocompleteViewSet,
                basename='user-test-panel-types-autocomplete')
router.register(r'autocomplete/providers', accounts_viewsets.ProviderAutocompleteViewSet,
                basename='providers-autocomplete')
router.register(r'user/providers', accounts_viewsets.UserProviderViewSet, basename='user-providers')

#router.register(r'orders', orders_viewsets.OrderViewSet)
#router.register(r'test_panel', orders_viewsets.TestPanelViewSet)
#router.register(r'samples', orders_viewsets.SampleViewSet)
#router.register(r'accounts', accounts_viewsets.AccountViewSet)
#router.register(r'providers', accounts_viewsets.ProviderViewSet)
#router.register(r'test_panel_types', orders_viewsets.TestPanelTypeViewSet)
#router.register(r'test_types', orders_viewsets.TestTypeViewSet)

# orders_router = routers.NestedSimpleRouter(router, r'orders', lookup='order')
# orders_router.register(r'test_panels', orders_viewsets.OrderTestPanelViewSet, base_name='order-test_panels')
# orders_router.register(r'samples', orders_viewsets.OrderSampleViewSet, base_name='order-samples')
#
# account_router = routers.NestedSimpleRouter(router, r'accounts', lookup='account')
# account_router.register(r'providers', accounts_viewsets.AccountProviderViewSet, base_name='account-providers')

urlpatterns =[
    # REST API
    path('v1/', include(router.urls)),
    # path('v1/', include(orders_router.urls)),
    # path('v1/', include(account_router.urls)),
    path('api-token-auth', obtain_auth_token),

    # Internal api
    path('v1/internal/archived_account_table/', accounts_data_views.archived_account_table, name='archived_account_table'),
    path('v1/internal/account_table/<str:id>/<str:alternate_id>/<str:name>/<str:alias>/',
         accounts_data_views.account_table_filtered, name='account_table_filtered'),
    path('v1/internal/archived_account_table/<str:id>/<str:alternate_id>/<str:name>/',
         accounts_data_views.archived_account_table_filtered, name='archived_account_table_filtered'),
    path('v1/internal/critical_contact_table/<str:account_uuid>/',
         accounts_data_views.critical_contact_table, name='critical_contact_table'),

    path('v1/internal/test_panel_type_table/<str:name>/<str:category>/<str:alias>/<str:lab_department>/',
         orders_data_views.test_panel_type_table, name='test_panel_table'),
    path('v1/internal/super_test_panel_type_table/', orders_data_views.super_test_panel_type_table, name='super_test_panel_table'),
    path('v1/internal/patient_table/', patients_data_views.patient_table, name='patient_table'),
    path('v1/internal/patient_table/<str:first_name>/<str:last_name>/<str:middle>/<str:sex>/<str:birth_date_string>/',
         patients_data_views.patient_table_filtered, name='patient_table_filtered'),
    path('v1/internal/provider_table/', accounts_data_views.provider_table, name='provider_table'),
    path('v1/internal/archived_provider_table/', accounts_data_views.archived_provider_table, name='archived_provider_table'),
    path('v1/internal/provider_table/<str:first_name>/<str:last_name>/<str:npi>/',
         accounts_data_views.provider_table_filtered, name='provider_table_filtered'),
    path('v1/internal/archived_provider_table/<str:first_name>/<str:last_name>/<str:npi>/',
         accounts_data_views.archived_provider_table_filtered, name='archived_provider_table_filtered'),
    path('v1/internal/collector_table/<str:first_name>/<str:last_name>/',
         accounts_data_views.collector_table_filtered, name='collector_table_filtered'),
    path('v1/internal/archived_collector_table/<str:first_name>/<str:last_name>/',
         accounts_data_views.archived_collector_table_filtered, name='archived_collector_table_filtered'),
    path('v1/internal/medication_table/<str:name>/<str:common_name>/',
         lab_data_views.medication_table_filtered, name='medication_table_filtered'),
    path('v1/internal/provider_inbox_table/<int:start_year>/<int:start_month>/<int:start_day>/<int:end_year>/'
         '<int:end_month>/<int:end_day>/', orders_data_views.provider_inbox_table,
         name='provider_inbox_table'),
    path('v1/internal/offsite_integration_table/', lab_tenant_data_views.offsite_integration_table,
         name='offsite_integration_table'),
    path('v1/internal/lab_departments_table/', lab_tenant_data_views.lab_departments_table,
         name='lab_departments_table'),
    path('v1/internal/in_house_lab_location_table/', lab_tenant_data_views.in_house_lab_location_table,
         name='in_house_lab_location_table'),

    # instrument-related
    path('v1/internal/instrument_integration_table/', lab_tenant_data_views.instrument_integration_table,
         name='instrument_integration_table'),
    path('v1/internal/instrument_calibration_table/<str:instrument_integration_uuid>/',
         lab_tenant_data_views.instrument_calibration_table,
         name='instrument_calibration_table'),
    path('v1/internal/instrument_test_code_table/<str:instrument_integration_uuid>/',
         lab_tenant_data_views.instrument_test_code_table,
         name='instrument_test_code_table'),

    path('v1/internal/batch_table/', orders_data_views.batch_table, name='batch_table'),
    path('v1/internal/control_table/', orders_data_views.control_table, name='control_table'),
    path('v1/internal/observation_table/<str:control_uuid>/<str:target_name_filter>/<int:start_year>/<int:start_month>/<int:start_day>/<int:end_year>/'
         '<int:end_month>/<int:end_day>/', orders_data_views.observation_table, name='observation_table'),
    path('v1/internal/archived_orders_table/<str:test_filter>/<str:date_type>/<int:start_year>/<int:start_month>/<int:start_day>/<int:end_year>/'
         '<int:end_month>/<int:end_day>/', orders_data_views.archived_orders, name='archived_orders'),
    path('v1/internal/recent_orders_table/<str:test_panel_filter>/<str:test_filter>/<str:date_type>/'
         '<int:start_year>/<int:start_month>/<int:start_day>/<int:end_year>/'
         '<int:end_month>/<int:end_day>/<str:first_name>/<str:last_name>/<str:middle>/<str:sex>/'
         '<str:birth_date_string>/<str:accession_number>/<str:account_filter>/',
         orders_data_views.recent_orders, name='recent_orders'),
    path('v1/internal/accession_check_table/<str:test_filter>/<int:start_year>/<int:start_month>/<int:start_day>/<int:end_year>/'
         '<int:end_month>/<int:end_day>/', orders_data_views.accession_check, name='accession_check'),
    path('v1/internal/test_type_table/<str:name>/<str:alias>/<str:category>/<str:specialty>/<str:method>/<str:target>/',
         orders_data_views.test_type_table, name='test_type_table'),
    path('v1/internal/test_type_category_table/', orders_data_views.test_type_category_table, name='test_type_category_table'),
    path('v1/internal/container_type_table/', orders_data_views.container_type_table, name='container_type_table'),
    path('v1/internal/reference_range_table/<str:test_type_uuid>/<str:sex_filter>/<str:account_name_filter>/',
         orders_data_views.reference_range_table, name='reference_range_table'),

    path('v1/internal/clia_test_type_table/<str:complexity>/<str:specialty>/<str:text_filter>/<int:start_year>/<int:start_month>/<int:start_day>/<int:end_year>/'
         '<int:end_month>/<int:end_day>/', lab_data_views.clia_test_type_table, name='clia_test_type_table'),

    path('v1/internal/sample_type_table/', orders_data_views.sample_type_table, name='sample_type_table'),
    path('v1/internal/workqueue_table/<int:start_year>/<int:start_month>/<int:start_day>/<int:end_year>/'
         '<int:end_month>/<int:end_day>/', orders_data_views.workqueue_table, name='workqueue_table'),

    # the workqueue issues table is actually the same as recent_orders table
    path('v1/internal/workqueue_issues_table/<str:test_filter>/<str:date_type>/<int:start_year>/<int:start_month>/<int:start_day>/<int:end_year>/'
         '<int:end_month>/<int:end_day>', orders_data_views.workqueue_issues_table, name='workqueue_issues_table'),

    path('v1/internal/workqueue_test_table/<str:test_method>/<int:start_year>/<int:start_month>/<int:start_day>/<int:end_year>/'
         '<int:end_month>/<int:end_day>/', orders_data_views.workqueue_test_table, name='workqueue_test_table'),
    path('v1/internal/workqueue_test_count/<int:test_method_id>/', orders_data_views.workqueue_test_count,
         name='workqueue_test_count'),
    path('v1/internal/workqueue_unreceived_table/<int:start_year>/<int:start_month>/<int:start_day>/<int:end_year>/'
         '<int:end_month>/<int:end_day>/', orders_data_views.workqueue_unreceived_table,
         name='workqueue_unreceived_table'),
    path('v1/internal/workqueue_pending_approval_table/<int:start_year>/<int:start_month>/<int:start_day>/'
         '<int:end_year>/<int:end_month>/<int:end_day>/',
         orders_data_views.workqueue_pending_approval_table, name='workqueue_pending_approval_table'),
    path('v1/internal/workqueue_qa_passed_table/<int:start_year>/<int:start_month>/<int:start_day>/<int:end_year>/'
         '<int:end_month>/<int:end_day>/<str:first_name>/<str:last_name>/<str:middle>/<str:sex>/'
         '<str:birth_date_string>/<str:accession_number>/',
         orders_data_views.workqueue_reported_table,
         name='workqueue_table_qa_passed'),

    # test results
    path('v1/internal/test_results_table/<str:result>/<str:target_uuid>/<str:account_uuid>/'
         '<int:start_year>/<int:start_month>/<int:start_day>/<int:end_year>/'
         '<int:end_month>/<int:end_day>/', orders_data_views.test_results_table, name='test_results_table'),

    # samples
    path('v1/internal/sample_table/<str:target_uuid>/<str:method_uuid>/'
         '<int:start_year>/<int:start_month>/<int:start_day>/<int:end_year>/'
         '<int:end_month>/<int:end_day>/', orders_data_views.sample_table, name='sample_table'),

    path('v1/internal/account_reported_orders_table/<str:account_uuid>/',
         orders_data_views.account_reported_orders_table, name='account_reported_orders_table'),
    path('v1/internal/provider_reported_orders_table/<str:provider_uuid>/',
         orders_data_views.provider_reported_orders_table, name='provider_reported_orders_table'),
    path('v1/internal/patient_reported_orders_table/<str:patient_uuid>/',
         orders_data_views.patient_reported_orders_table, name='patient_reported_orders_table'),

    path('v1/internal/operational_report_table/', orders_data_views.operational_report_table,
         name='operational_report_table'),
    path('v1/internal/data_in_table/<str:instrument_integration_filter>/<str:data_type>/<int:start_year>/'
         '<int:start_month>/<int:start_day>/<int:end_year>/<int:end_month>/<int:end_day>/',
         orders_data_views.data_in_table,
         name='data_in_table'),
    path('v1/internal/control/leveyjenningschart/<str:control_uuid>/<str:instrument_integration>/<str:test_target>/'
         '<int:start_year>/<int:start_month>/<int:start_day>/<int:end_year>/'
         '<int:end_month>/<int:end_day>/',
         orders_data_views.control_levey_jennings_chart,
         name='control_levey_jennings_chart'),
    path('v1/internal/billing_messages_table/<str:account_filter>/<int:start_year>/<int:start_month>/<int:start_day>/<int:end_year>/'
         '<int:end_month>/<int:end_day>/', orders_data_views.billing_messages_table, name='billing_messages_table'),
    path('v1/internal/payer_table/<str:payer_name>/<str:payer_type>/',
         lab_data_views.payer_table, name='payer_table'),
    path('v1/internal/payer_archived_table/<str:payer_name>/<str:payer_type>/',
         lab_data_views.archived_payers, name='payer_archived_table'),
]
