# Generated by Django 2.2.2 on 2019-10-18 00:58

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('orders', '0099_orderissue_is_visible_to_provider'),
    ]

    operations = [
        migrations.AlterField(
            model_name='orderissue',
            name='issue_type',
            field=models.CharField(choices=[('test_not_performed', 'Test Not Performed'), ('demographics', 'Patient Demographics'), ('billing', 'Billing/Coding'), ('medication', 'Medication'), ('other', 'Other')], max_length=32),
        ),
    ]
