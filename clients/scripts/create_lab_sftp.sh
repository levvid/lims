#!/usr/bin/env bash

# create billing company directories for the lab
function add_billing_companies() {
    #args
    #    : @required string labname

    labname=$1
    mkdir -p /home/"$labname"/mirth/billing

    echo -e "\n \n \n"
    echo "Enter the number of billing companies used by $labname"
    read num_billing_companies

    for ((i=1; i<=num_billing_companies; i++))
    do
        echo "Enter the name of $1's billing company #${i}:"
        read billing_company
        # notify user if billing company already exists
        if [ -d "/home/$labname/mirth/billing/$billing_company" ]
        then
            echo -e "\n\n$billing_company already exists! Skipping...\n"
        else
            mkdir -p /home/"$labname"/mirth/billing/"$billing_company"
        fi
    done
}
# end of billing company addition


# create emr directories
function add_emrs() {
    #args
    #    : @required string labname

    labname=$1

    printf "\n \n \n"
    echo "Enter the number of EMR's used by $labname"
    read num_emrs

    for ((i=1; i<=num_emrs; i++))
    do
        echo "Enter the name of $labname's emr #${i}:"
        read -r emr

        # notify user if emr already exists
        if [ -d "/home/$labname/mirth/billing/$emr" ]
        then
            echo -e "\n\n$emr already exists! Skipping...\n"
        else
            # create orders directory
            mkdir -p /home/"$labname"/mirth/emrs/"$emr"/orders
            # create results directory
            mkdir -p /home/"$labname"/mirth/emrs/"$emr"/results
        fi
    done
}
# end of emr addition


# assign appropriate directory permissions
function directory_permissions() {
    #args
    #    : @required labname

    labname=$1
    # make root owner of the lab's home folder
    chown root:root /home/"$labname"
    chmod 755 /home/"$labname"

    # make $labname owner of /mirth folder and all subdirectories
    chown -R "$labname":"$labname" /home/"$labname"/mirth

    echo -e "\n\n\nGiven the right permissions to $labname directories"

}

function main() {
    # Ask for the lab's username to create the account
    echo "Please enter the new lab's username:"
    read labname
    adduser "$labname"
    echo "Added user $labname to sftp server"

    usermod -aG sftp_users "$labname"
    echo "Added user $labname to sftp user group"

    # create mirth folder where files are going to be saved
    mkdir -p /home/"$labname"/mirth
    echo "Created $labname mirth folder"

    # add billing companies
    add_billing_companies "$labname"


    # add emrs
    add_emrs "$labname"


    # assign appropriate directory permissions
    directory_permissions "$labname"

    # restart the OpenSSH server
    systemctl restart sshd
    printf "\n\n\nFinished and restarted ssh server\n\n"

}


# call main function to create lab user, add emrs and billing companies with relevant directory permissions
main
