import django
import logging
import os
import sys

from django.db import transaction
from tenant_schemas.utils import tenant_context

os.environ['DJANGO_SETTINGS_MODULE'] = 'main.settings'
django.setup()

from clients import models as client_models
from .. import models
from lab import models as lab_models
"""
Script to convert required_samples to required_clia_samples
"""
conversion_dict = {'Fluid': lab_models.CLIASampleType.objects.get(name='Other Fluid'),
                   'Blood': lab_models.CLIASampleType.objects.get(name='Whole Blood'),
                   'Tissue': lab_models.CLIASampleType.objects.get(name='Tissue'),
                   'Culture': lab_models.CLIASampleType.objects.get(name='Culture'),
                   'Other': lab_models.CLIASampleType.objects.get(name='Other'),
                   'Serum': lab_models.CLIASampleType.objects.get(name='Serum'),
                   'Pericardial fluid': lab_models.CLIASampleType.objects.get(name='Pericardial fluid'),
                   'num_blood_samples': lab_models.CLIASampleType.objects.get(name='Whole Blood'),
                   'num_serum_samples': lab_models.CLIASampleType.objects.get(name='Serum'),
                   'num_culture_samples': lab_models.CLIASampleType.objects.get(name='Culture'),
                   'num_fluid_samples': lab_models.CLIASampleType.objects.get(name='Other Fluid')}


@transaction.atomic
def main():
    tenants = client_models.Client.objects.exclude(schema_name='public')

    for tenant in tenants:
        if tenant.lab_type == 'OTHER':
            with tenant_context(tenant):
                test_types = models.TestType.objects.all()
                for test_type in test_types:
                    if test_type.required_samples:
                        logging.debug("Converting {} {} {}".format(tenant.name, test_type.name, str(test_type.required_samples)))
                        for sample_type_id in test_type.required_samples:
                            try:
                                sample_type = models.SampleType.objects.get(id=int(sample_type_id))
                                clia_sample_type = conversion_dict[sample_type.name]
                            except ValueError:
                                clia_sample_type = conversion_dict[sample_type_id]
                            except models.SampleType.DoesNotExist:
                                import pdb; pdb.set_trace()
                                print('hi')
                            if test_type.required_clia_samples:
                                test_type.required_clia_samples[str(clia_sample_type.id)] = test_type.required_samples[sample_type_id]
                            else:
                                test_type.required_clia_samples = {str(clia_sample_type.id): test_type.required_samples[sample_type_id]}
                            logging.debug("converted to: {}".format(str(test_type.required_clia_samples)))
                            test_type.save()
                        test_type.required_samples = None

                        test_type.save()


if __name__ == '__main__':
    logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)
    main()
