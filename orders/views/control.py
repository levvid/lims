from braces.views import LoginRequiredMixin
from django_tables2 import RequestConfig
from django.contrib import messages
from django.db import transaction, IntegrityError
from django.forms import formset_factory
from django.http import HttpResponseRedirect
from django.shortcuts import render, redirect, get_object_or_404
from django.utils import timezone
from django.views.generic import View

from lab_tenant.views import LISLoginRequiredMixin
from orders import forms
from orders import models


class ControlListView(LISLoginRequiredMixin, View):
    def get(self, request):
        context = dict()
        return render(request, 'control_list.html', context)


class ControlCreateView(LISLoginRequiredMixin, View):
    def get(self, request):
        form = forms.ControlForm()

        context = {
            'form': form,
        }
        return render(request, 'control_create.html', context)

    def post(self, request):
        form = forms.ControlForm(request.POST)
        context = {
            'form': form
        }

        if form.is_valid():
            name = form.cleaned_data['name']
            code = form.cleaned_data['code']
            instrument_integrations = form.cleaned_data['instrument_integrations']
            lot_number = form.cleaned_data['lot_number']
            expiration_date = form.cleaned_data['expiration_date']

            try:
                control = models.Control.objects.create(
                    name=name,
                    code=code,
                    lot_number=lot_number,
                    expiration_date=expiration_date,
                )
                control.instrument_integrations.set(instrument_integrations)
                return redirect('control_list')
            except IntegrityError:
                messages.error(request, "Control name and code must be unique")
                return render(request, 'control_create.html', context)

        else:
            return render(request, 'control_create.html', context)


class ControlDetailsView(LISLoginRequiredMixin, View):
    def get(self, request, control_uuid):
        control = get_object_or_404(models.Control, uuid=control_uuid)
        #observations = control.observations.all().prefetch_related('data_in__instrument_integration')
        instrument_integrations = control.instrument_integrations.all()
        known_values = control.known_values.all().prefetch_related('test_target')
        test_targets = list(set([x.test_target for x in known_values]))

        # error messages to explain why chart is not populated
        if len(known_values) == 0:
            messages.error(request,
                           "No known values associated with this control. "
                           "Please add known values to populate charts.")
        if len(instrument_integrations) == 0:
            messages.error(request,
                           "No instruments associated with this control. "
                           "Please add associated instruments to populate charts.")

        context = {
            'control': control,
            'instrument_integrations': instrument_integrations,
            'test_targets': test_targets,
            'known_values': known_values
        }
        return render(request, 'control_details.html', context)


class ControlUpdateView(LISLoginRequiredMixin, View):
    def get(self, request, control_uuid):
        control = get_object_or_404(models.Control, uuid=control_uuid)
        if control.expiration_date:
            expiration_date = control.expiration_date.strftime('%m%d%y')
        else:
            expiration_date = ''
        form = forms.ControlForm(initial={
            'name': control.name,
            'code': control.code,
            'instrument_integrations': control.instrument_integrations.all(),
            'lot_number': control.lot_number,
            'expiration_date': expiration_date,
        })

        context = {
            'form': form,
            'update': True,
        }
        return render(request, 'control_create.html', context)

    def post(self, request, control_uuid):
        control = get_object_or_404(models.Control, uuid=control_uuid)
        form = forms.ControlForm(request.POST)

        context = {
            'form': form,
            'update': True,
        }
        if form.is_valid():
            name = form.cleaned_data['name']
            code = form.cleaned_data['code']
            instrument_integrations = form.cleaned_data['instrument_integrations']
            lot_number = form.cleaned_data['lot_number']
            expiration_date = form.cleaned_data['expiration_date']

            control.name = name
            control.code = code
            control.instrument_integrations.set(instrument_integrations)
            control.lot_number = lot_number
            control.expiration_date = expiration_date
            try:
                control.save()
            except IntegrityError:
                messages.error(request, "Control name and code must be unique")
                return render(request, 'control_Create.html', context)
            messages.success(request, '{} ({}) successfully updated!'.format(control.name, control.code))
            return redirect('control_details', control_uuid=control_uuid)
        else:
            return render(request, 'control_create.html', context)


class ControlKnownValueCreateView(LISLoginRequiredMixin, View):
    def get(self, request, control_uuid):
        control = get_object_or_404(models.Control, uuid=control_uuid)
        form = forms.ControlKnownValueForm(
            initial={'unit': 'ng/mL'},
            control=control
        )

        context = {
            'form': form,
            'control': control,
        }
        return render(request, 'control_known_value_create.html', context)

    def post(self, request, control_uuid):
        control = get_object_or_404(models.Control, uuid=control_uuid)
        form = forms.ControlKnownValueForm(
            request.POST,
            control=control
        )

        context = {
            'form': form,
            'control': control,
        }
        if form.is_valid():
            test_target = form.cleaned_data['test_target']
            concentration = form.cleaned_data['concentration']
            unit = form.cleaned_data['unit']

            models.ControlKnownValue.objects.create(
                control=control,
                test_target=test_target,
                concentration=concentration,
                unit=unit,
            )
            return redirect('control_details', control_uuid=control_uuid)
        else:
            return render(request, 'control_known_value_create.html', context)


class ControlKnownValueDeleteView(LISLoginRequiredMixin, View):
    def get(self, request, control_uuid, known_value_uuid):
        control = get_object_or_404(models.Control, uuid=control_uuid)
        known_value = get_object_or_404(models.ControlKnownValue, uuid=known_value_uuid)

        known_value.is_active = False
        known_value.save(update_fields=['is_active'])

        return redirect('control_details', control_uuid=control.uuid)


class ControlObservationCreateView(LISLoginRequiredMixin, View):
    def get(self, request, control_uuid):
        control = get_object_or_404(models.Control, uuid=control_uuid)

        initial = {'control': control}
        if control.known_values.count() == 1:  # single analyte control
            initial['test_target'] = control.known_values.get().test_target
        if control.instrument_integrations.count() == 1:
            initial['instrument_integration'] = control.instrument_integrations.get()

        form = forms.ControlObservationForm(initial=initial)

        context = {
            'form': form,
            'control': control,
        }
        return render(request, 'control_observation_create.html', context)

    def post(self, request, control_uuid):
        control = get_object_or_404(models.Control, uuid=control_uuid)
        form = forms.ControlObservationForm(request.POST)

        context = {
            'form': form,
            'control': control,
        }
        if form.is_valid():
            test_target = form.cleaned_data['test_target']
            instrument_integration = form.cleaned_data['instrument_integration']
            result_quantitative = form.cleaned_data['result_quantitative']

            models.Observation.objects.create(
                control=control,
                target_name=test_target.name,
                test_target=test_target,
                instrument_integration=instrument_integration,
                result_value=result_quantitative,
                observation_date=timezone.now()
            )
            return redirect('control_details', control_uuid=control_uuid)
        else:
            return render(request, 'control_observation_create.html', context)


class ControlObservationDeleteView(LISLoginRequiredMixin, View):
    def get(self, request, control_uuid, observation_uuid):
        control = get_object_or_404(models.Control, uuid=control_uuid)
        observation = get_object_or_404(models.Observation, uuid=observation_uuid)

        observation.is_active = False
        observation.save(update_fields=['is_active'])

        return redirect('control_details', control_uuid=control.uuid)
