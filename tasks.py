import datetime
import django
from django.db import transaction
import os

from public.email import send_email_sendgrid
from tenant_schemas.utils import tenant_context

os.environ['DJANGO_SETTINGS_MODULE'] = 'main.settings'
django.setup()

from clients import models as clients_models
from lab_tenant import models as lab_tenant_models
from orders import models as orders_models


# ----------------------------------------------------------------------------------------------------------------------

SUPPORT_EMAIL = 'support@dendisoftware.com'
ERROR_SUBJECT = 'Signup Error'

# ----------------------------------------------------------------------------------------------------------------------


@transaction.atomic
def initialize_client(context):
    domain = os.environ.get('DOMAIN', '.localhost')
    client_name = context['client_name']
    schema_name = context['schema_name']
    email = context['email']
    first_name = context['first_name']
    last_name = context['last_name']
    password = context['password']
    lab_type = context['lab_type']
    address_1 = context.get('address_1')
    address_2 = context.get('address_2')
    phone_number = context.get('phone_number')
    fax_number = context.get('fax_number')
    clia_number = context.get('clia_number')
    clia_director = context.get('clia_director', '')
    logo_file = context.get('logo_file', '')
    website = context.get('website', '')
    results_outbound_id = context.get('results_outbound_id', '')

    # The correct way to catch database errors is around an atomic block
    # https://docs.djangoproject.com/en/2.1/topics/db/transactions/#controlling-transactions-explicitly
    with transaction.atomic():
        try:
            client = clients_models.Client(name=client_name,
                                           on_trial=True,
                                           paid_until=datetime.date.today() + datetime.timedelta(days=30),
                                           schema_name=schema_name,
                                           domain_url=schema_name + domain,
                                           lab_type=lab_type,
                                           address_1=address_1,
                                           address_2=address_2,
                                           phone_number=phone_number,
                                           fax_number=fax_number,
                                           clia_number=clia_number,
                                           clia_director=clia_director,
                                           logo_file=logo_file,
                                           website=website,
                                           results_outbound_id=results_outbound_id)
            client.save()

            # create initial user
            with tenant_context(client):
                # create LabEmployee User type
                user_obj = lab_tenant_models.User(last_name=last_name,
                                                  first_name=first_name,
                                                  username=email,
                                                  email=email,
                                                  user_type=1)
                # save User object first
                user_obj.set_password(password)
                user_obj.save()
                lab_admin_obj = lab_tenant_models.LabAdmin(user=user_obj)
                lab_admin_obj.save()

                # default test panel cateogry
                orders_models.TestPanelCategory.objects.create(name='Test Profiles')

            # sent confirmation email
            subject = 'Your LIS is ready'
            content = 'Thank you for your patience. <br>Please bookmark or save the following link for your future ' \
                      'reference.<br>' \
                      '<a>{}{}</a> <br><br>'.format(schema_name, domain)
            send_email_sendgrid(SUPPORT_EMAIL,
                                email,
                                subject,
                                content)
        except Exception as e:
            send_email_sendgrid(SUPPORT_EMAIL,
                                'sjung@dendisoftware.com',
                                ERROR_SUBJECT,
                                str(e))
