import csv
import django
import logging
import os
import requests
import sys
from django.db import transaction
from tenant_schemas.utils import tenant_context

os.environ['DJANGO_SETTINGS_MODULE'] = 'main.settings'
django.setup()

from clients import models as client_models
from accounts import models as accounts_models
from orders import models as orders_models
"""
Script used to work on task:
Import test panel/account associations
"""

PANEL_ACCOUNT_CSV_URL = 'https://dendi-lis.s3.amazonaws.com/client_data/ildp/Profile+Associations.csv'


def import_panel_account_associations(csv_url):
    panel_associations = []
    with requests.Session() as session:
        download = session.get(csv_url)
        decoded_content = download.content.decode('utf-8')
        csv_reader = csv.reader(decoded_content.splitlines(), delimiter=',')
        csv_rows = list(csv_reader)[1:]
        for row in csv_rows:
            row_dict = {
                    'name': row[0],
                    'id': row[1],
                    'profiles': [x for x in row[2:] if x != '']
                }
            panel_associations.append(row_dict)

    panel_dict = {}
    for panel_association in panel_associations:
        panel_ids = panel_association['profiles']
        if panel_ids:
            try:
                account = accounts_models.Account.objects.get(alternate_id=panel_association['id'])
            except accounts_models.Account.DoesNotExist:
                continue
            print('Setting panels for account: {}'.format(account.name))
            for panel_id in panel_ids:
                try:
                    panel = panel_dict[panel_id]
                except KeyError:
                    try:
                        panel = orders_models.TestPanelType.objects.get(alternate_id=panel_id)
                        panel_dict[panel_id] = panel
                    except orders_models.TestPanelType.DoesNotExist:
                        continue
                panel.exclusive_accounts.add(account)


def main():
    import timeit
    t0 = timeit.default_timer()
    tenant = client_models.Client.objects.get(schema_name='ildp2')
    with tenant_context(tenant):
        import_panel_account_associations(PANEL_ACCOUNT_CSV_URL)
    t1 = timeit.default_timer()
    print("{} seconds".format(t1-t0))


if __name__ == '__main__':
    main()
