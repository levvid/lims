import uuid as uuid_lib

from django.contrib.auth.models import User
from django.db import connection, models
from django.utils import timezone
from django.contrib.postgres.fields import ArrayField

from jsonfield import JSONField

from lab_tenant.models import AuditLogModel
from auditlog.registry import auditlog
from auditlog.models import AuditlogHistoryField

from main.storage_backends import PrivateMediaStorage


class TestMethod(AuditLogModel):
    """
    Supported Test methods for Dendi LIS
    PCR, IFA, WB, ELISA etc.
    """
    uuid = models.UUIDField(
        db_index=True,
        default=uuid_lib.uuid4,
        editable=False
    )
    name = models.CharField(max_length=255, null=True, blank=True)
    cutoff_field = models.CharField(max_length=255, null=True, blank=True)
    positive_result_label = models.CharField(max_length=32, blank=True, default='Positive')
    negative_result_label = models.CharField(max_length=32, blank=True, default='Negative')
    equivocal_result_label = models.CharField(max_length=32, blank=True, default='Equivocal')
    # When true, the test has a positive result if quantitative measurement is above the cutoff
    positive_above_cutoff = models.BooleanField(default=True)
    # show different choices for results
    is_within_range = models.BooleanField(default=False)
    display_order = models.IntegerField(default=5)

    @property
    def full_name(self):
        return self.name

    def __str__(self):
        return self.name


class TestTarget(AuditLogModel):
    """
    Supported Test Targets (or Analyte)

    Bartonella spp., THC, etc.
    """
    uuid = models.UUIDField(
        db_index=True,
        default=uuid_lib.uuid4,
        editable=False
    )
    name = models.CharField(max_length=255, null=True, blank=True, unique=True)
    # diagnostic or toxicology
    is_tox_target = models.BooleanField(default=True)
    category = models.CharField(max_length=255, null=True, blank=True)
    common_name = ArrayField(models.CharField(max_length=255), null=True, blank=True)
    # medication or other
    is_medication = models.BooleanField(default=False)
    metabolites = models.ManyToManyField('self', symmetrical=False, blank=True)

    class Meta:
        db_table = 'test_target'

    @property
    def full_name(self):
        return self.name

    def __str__(self):
        return self.name


class Payer(AuditLogModel):
    """
    Insurance company or similiar
    """
    # this payer code (or ID) is an industry standard
    payer_code = models.CharField(max_length=24, blank=True)
    name = models.CharField(max_length=128, blank=True)
    abbreviated_name = models.CharField(max_length=128, blank=True)
    address_1 = models.CharField(max_length=128, blank=True)
    address_2 = models.CharField(max_length=128, blank=True)
    city = models.CharField(max_length=128, blank=True)
    state = models.CharField(max_length=2, blank=True)
    zip_code = models.CharField(max_length=128, blank=True)
    phone_number = models.CharField(max_length=128, blank=True)
    payer_type = models.CharField(max_length=16, choices=(('Self-Pay', 'Self-Pay'), ('Private', 'Private'),
                                                          ('Medicare', 'Medicare'), ('Medicaid', 'Medicaid'),
                                                          ('Other', 'Other')), default='Self-Pay')

    def __str__(self):
        return self.name


class UnregisteredUser(models.Model):
    uuid = models.UUIDField(
        db_index=True,
        default=uuid_lib.uuid4,
        editable=False
    )
    email = models.CharField(max_length=256)
    email_confirmed = models.BooleanField(default=False)
    datetime_created = models.DateTimeField(default=timezone.now, null=True)


class CLIATestType(AuditLogModel):
    document_number = models.CharField(max_length=128)
    test_system_id = models.CharField(max_length=128)
    test_system_name = models.CharField(max_length=254)
    qualifier_1 = models.TextField(blank=True)
    qualifier_2 = models.TextField(blank=True)
    analyte_id = models.CharField(max_length=128)
    analyte_name = models.CharField(max_length=128)
    specialty_id = models.CharField(max_length=128, blank=True)
    complexity = models.CharField(max_length=128)
    date_effective = models.DateTimeField(null=True)
    required_samples = JSONField(null=True, blank=True)
    # test methodology
    test_method = models.ForeignKey(TestMethod, related_name='clia_test_types', on_delete=models.CASCADE,
                                    null=True)
    result_type = models.CharField(max_length=32, blank=True)

    def __str__(self):
        return "{} - {} - {}".format(self.analyte_name, self.test_system_name,
                                     self.complexity)


class CLIATestTypeUpdateLog(AuditLogModel):
    uuid = models.UUIDField(
        db_index=True,
        default=uuid_lib.uuid4,
        editable=False
    )
    update_datetime = models.DateTimeField(default=timezone.now)
    num_new_rows = models.IntegerField(null=True)


class CLIATestTypeSpecialty(AuditLogModel):
    uuid = models.UUIDField(
        db_index=True,
        default=uuid_lib.uuid4,
        editable=False
    )
    specialty_id = models.IntegerField(null=True)
    specialty_name = models.CharField(max_length=32, blank=True)

    def __str__(self):
        return self.specialty_name


class CLIASampleType(AuditLogModel):
    """
    Sample types defined by CLIA
    """
    uuid = models.UUIDField(
        db_index=True,
        default=uuid_lib.uuid4,
        editable=False,
    )
    name = models.CharField(max_length=255)
    notes = models.TextField(blank=True)
    abbreviation = models.CharField(max_length=3, blank=True)

    def __str__(self):
        return "<SampleType {}>".format(self.name)


class OffsiteLaboratory(AuditLogModel):
    """
    Laboratories available for offsite testing
    """
    uuid = models.UUIDField(
        db_index=True,
        default=uuid_lib.uuid4,
        editable=False,
    )
    name = models.CharField(max_length=255)
    receiving_application = models.CharField(max_length=255, blank=True)
    receiving_facility = models.CharField(max_length=255, blank=True)
    address1 = models.CharField(max_length=255, blank=True)
    address2 = models.CharField(max_length=255, blank=True)
    zip_code = models.CharField(max_length=128, blank=True)
    lab_director = models.CharField(max_length=255, blank=True)

    def __str__(self):
        return self.name


class Instrument(AuditLogModel):
    """
    Instruments supported for integration
    """
    name = models.CharField(max_length=255)
    manufacturer = models.CharField(max_length=255)
    method = models.ForeignKey('lab.TestMethod', related_name='instrument', on_delete=models.CASCADE)
    unit_of_measurement = models.CharField(max_length=16, blank=True)

    def __str__(self):
        return self.name


class OrdersOut(AuditLogModel):
    """
    Order HL7 messages out
    """
    uuid = models.UUIDField(
        db_index=True,
        default=uuid_lib.uuid4,
        editable=False,
    )
    hl7_message = models.TextField()
    tenant = models.ForeignKey('clients.Client', related_name='orders_out', on_delete=models.SET_NULL, null=True)
    offsite_laboratory = models.ForeignKey('OffsiteLaboratory', related_name='orders_out', on_delete=models.CASCADE)
    created_datetime = models.DateTimeField(default=timezone.now)
    received_datetime = models.DateTimeField(null=True)


class ResultsIn(models.Model):
    """
    Result HL7 messages in

    This object is not an AuditLogModel because it will be written to by MirthConnect, not django.
    """
    uuid = models.UUIDField(
        db_index=True,
        default=uuid_lib.uuid4,
        editable=False,
    )
    hl7_message = models.TextField()
    filename = models.CharField(max_length=255, blank=True)
    received_datetime = models.DateTimeField(null=True)
    processed_datetime = models.DateTimeField(null=True)
    errors = models.TextField(null=True)


class USZipCode(models.Model):
    """
    US zipcodes sourced from
    http://federalgovernmentzipcodes.us/
    updated 1/22/2012
    """
    # 5 digit Zipcode or military postal code(FPO/APO)
    zipcode = models.CharField(max_length=12, blank=False)
    # Standard, PO BOX Only, Unique, Military(implies APO or FPO)
    zipcode_type = models.CharField(max_length=32, blank=True)
    # USPS official city name(s)
    city = models.CharField(max_length=64, blank=True)
    # USPS offical state, territory, or quasi-state (AA, AE, AP) abbreviation code
    state = models.CharField(max_length=2, blank=True)
    # Decimal Latitude, if available
    latitude = models.CharField(max_length=8, blank=True)
    # Decimal Longitude, if available
    longitude = models.CharField(max_length=8, blank=True)
    # Standard Display  (eg Phoenix, AZ ; Pago Pago, AS ; Melbourne, AU )
    location = models.CharField(max_length=256, blank=True)
    # Yes implies historical Zipcode, No Implies current Zipcode
    decommissioned = models.BooleanField(default=False)
    # Number of Individual Tax Returns Filed in 2008
    tax_returns = models.IntegerField(null=True)
    # Tax returns filed + Married filing jointly + Dependents
    estimated_population = models.IntegerField(null=True)
    # Total of Wages Salaries and Tips
    total_wages = models.IntegerField(null=True)


class MirthDataIn(models.Model):
    """
    data in from mirth connect
    """
    uuid = models.UUIDField(
        db_index=True,
        default=uuid_lib.uuid4,
        editable=False,
    )
    raw = models.TextField(blank=True)
    created_datetime = models.DateTimeField(null=True)
    processed_datetime = models.DateTimeField(null=True)
    client = models.ForeignKey('clients.Client', related_name='mirth_data_in', on_delete=models.CASCADE)
    instrument = models.ForeignKey('lab.Instrument', related_name='mirth_data_in', on_delete=models.CASCADE)
    processed = models.BooleanField(default=False)
    original_file_name = models.CharField(max_length=256, blank=True)


######################################################################################################################
# Django AuditLogs

auditlog.register(TestMethod)
auditlog.register(TestTarget)
auditlog.register(Instrument)
auditlog.register(OffsiteLaboratory)
auditlog.register(OrdersOut)
auditlog.register(Payer)
