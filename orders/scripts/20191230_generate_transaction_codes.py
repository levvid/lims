import os
import datetime
import sys

import django
import logging

from django.db.models import F, Q

os.environ['DJANGO_SETTINGS_MODULE'] = 'main.settings'
django.setup()

from tenant_schemas.utils import tenant_context
import clients.models as clients_models
import orders.models as orders_models


TENANT_NAME = 'ildp'

TEST_TYPES = [
    {
        'name': '7-Aminoclonazepam',
        'transaction_code': '7ACLONALOC'
    },
    {
        'name': 'Naloxone',
        'transaction_code': 'NALOXNC'
    },
    {
        'name': 'EtG',
        'transaction_code': 'ETGC'
    },
    {
        'name': 'Nordiazepam',
        'transaction_code': 'NORDIAZPMC'
    },
    {
        'name': 'Methadone (EDDP)',
        'transaction_code': 'METHADC'
    },
    {
        'name': 'Lorazepam',
        'transaction_code': 'LORAZMC'
    },
    {
        'name': 'Lorazepam - Oral',
        'transaction_code': 'LORAZLOC'
    },
    {
        'name': 'Diazepam - Oral',
        'transaction_code': 'DIAZLOC'
    },
    {
        'name': 'Desmethylclomipramine - Confirmation',
        'transaction_code': 'DESMCLOC'
    },
    {
        'name': 'Cocaine (Benzoylecgonine)',
        'transaction_code': 'BENYLCC'
    },
    {
        'name': 'Benzoylecogonine - Oral',
        'transaction_code': 'BENYLOC'
    }
]


def generate_transaction_codes():
    """
    :return:
    """
    tenant = clients_models.Client.objects.get(schema_name=TENANT_NAME)

    with tenant_context(tenant):
        # update all the new test types with their transaction codes
        for test_type_update in TEST_TYPES:
            test_type = orders_models.TestType.objects.get(name=test_type_update['name'])
            test_type.transaction_code = test_type_update['transaction_code']
            test_type.save()

            print('Updated test type {} with transaction code {}'.format(test_type.name, test_type.transaction_code))


if __name__ == '__main__':
    logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)
    generate_transaction_codes()
