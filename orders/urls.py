from django.urls import path

from .views import autocomplete
from .views import batch
from .views import control
from .views import data_in
from .views import order
from .views import order_create
from .views import test_panel_type
from .views import test_type
from .views import views
from .views import container

urlpatterns = [
    # orders
    path('orders/', order.OrderListView.as_view(), name='order_list'),
    path('orders/archived/', order.OrderArchivedListView.as_view(), name='order_archived_list'),

    path('orders/create/', order_create.OrderCreateStep1View.as_view(), name='order_create'),
    path('orders/create/patient_payer/', order_create.OrderCreatePatientPayerView.as_view(),
         name='order_create_patient_payer'),
    path('orders/create/patient_status/', order_create.OrderCreatePatientStatusView.as_view(),
         name='order_create_patient_status'),
    path('orders/create/super_test_profile_select/', order_create.OrderCreateSuperTestPanelSelectView.as_view(),
         name='order_create_super_test_panel_select'),
    path('orders/create/test_profile_select/', order_create.OrderCreateTestPanelSelectView.as_view(),
         name='order_create_test_panel_select'),
    path('orders/create/test_select/', order_create.OrderCreateTestSelectView.as_view(),
         name='order_create_test_select'),
    path('orders/create/add_samples/', order_create.OrderCreateAddSamples.as_view(),
         name='order_create_add_samples'),

    #### Accessioning
    path('containers/barcode_accession/', views.ContainerAccessioningView.as_view(), name='accession_containers'),
    path('samples/barcode_accession/', views.SampleAccessioningView.as_view(), name='accession_samples'),
    path('orders/barcode_accession/', views.OrderAccessioningView.as_view(), name='accession_orders'),
    path('<str:accession_type>/barcode_accession/<str:order_code>/<str:barcode>/', views.AccessioningDetailsView.as_view(),
         name='accession_details'),

    # confirm receipt views
    path('containers/barcode_accession/confirm_receipt/', views.ContainerAccessioningConfirmReceiptView.as_view(),
         name='accession_containers_confirm_receipt'),
    path('samples/barcode_accession/confirm_receipt/', views.SampleAccessioningConfirmReceiptView.as_view(),
         name='accession_samples_confirm_receipt'),
    path('orders/barcode_accession/confirm_receipt/', views.OrderAccessioningConfirmReceiptView.as_view(),
         name='accession_orders_confirm_receipt'),
    path('<str:accession_type>/barcode_accession/confirm_receipt/<str:order_code>/<str:barcode>/', views.AccessioningConfirmReceiptDetailsView.as_view(),
         name='accession_confirm_receipt_details'),

    path('orders/<str:order_code>/', order.OrderDetailsView.as_view(), name='order_details'),
    path('orders/<str:order_code>/notes/', order.OrderNotesView.as_view(), name='order_notes'),
    path('orders/<str:order_code>/received/', order.OrderReceivedView.as_view(), name='order_received'),
    path('orders/<str:order_code>/complete/', order.OrderCompleteView.as_view(), name='order_complete'),
    path('orders/<str:order_code>/edit/', order.OrderEditView.as_view(), name='order_edit'),
    path('orders/<str:order_code>/update/', order.OrderUpdateView.as_view(),
         name='order_details_update'),
    path('orders/<str:order_code>/delete/', order.OrderDeleteView.as_view(), name='order_delete'),
    path('orders/<str:order_code>/undo_delete/', order.OrderUndoDeleteView.as_view(), name='order_undo_delete'),
    path('orders/<str:order_code>/approve/', order.OrderApproveView.as_view(), name='order_approve'),
    path('orders/<str:order_code>/tests/approve/', order.OrderTestsApproveView.as_view(),
         name='order_tests_approve'),
    path('orders/<str:order_code>/accession_number/update/', order.OrderAccessionNumberUpdateView.as_view(),
         name='order_accession_number_update'),

    path('orders/<str:order_code>/requisition_form/',
         order.OrderRequisitionFormView.as_view(), name='order_requisition_form'),
    path('orders/<str:order_code>/billing_message/',
         order.OrderBillingMessageView.as_view(), name='order_billing_message'),
    path('billing_messages/',
         views.BillingMessagesListView.as_view(), name='billing_messages'),

    # patient   info
    path('orders/<str:order_code>/patient_payer/update/', order.OrderPatientPayerUpdateView.as_view(),
         name='order_patient_payer_update'),

    path('orders/<str:order_code>/issues/',
         order.OrderIssuesListView.as_view(), name='order_issue_list'),
    path('orders/<str:order_code>/issues/create/',
         order.OrderIssuesCreateView.as_view(), name='order_issue_create'),
    path('orders/<str:order_code>/issues/<str:order_issue_uuid>/update/',
         order.OrderIssuesUpdateView.as_view(), name='order_issue_update'),
    path('orders/<str:order_code>/issues/<str:order_issue_uuid>/resolve/',
         order.OrderIssuesResolveView.as_view(), name='order_issue_resolve'),

    path('orders/<str:order_code>/amend_report/',
         order.OrderAmendReportView.as_view(), name='order_amend_report'),
    path('orders/<str:order_code>/refresh_pdf/',
         order.OrderRefreshPDFView.as_view(), name='order_refresh_pdf'),
    path('orders/<str:order_code>/report/', order.OrderReportView.as_view(), name='order_report'),
    path('orders/<str:order_code>/samples/', order.OrderSamplesView.as_view(), name='order_samples'),
    path('orders/<str:order_code>/report_preview',
         order.OrderReportPreview.as_view(), name='order_report_preview'),
    path('orders/<str:order_code>/approve_and_report/', order.OrderApproveAndReportView.as_view(),
         name='order_approve_and_report'),
    path('orders/<str:order_code>/next/', order.OrderNextView.as_view(), name='order_next'),
    path('orders/<str:order_code>/previous/', order.OrderPreviousView.as_view(), name='order_previous'),

    # next and previous order by accession
    path('orders/<str:order_code>/next_accession/', order.OrderNextAccessionView.as_view(), name='order_next_accession'),
    path('orders/<str:order_code>/previous_accession/', order.OrderPreviousAccessionView.as_view(), name='order_previous_accession'),

    # order tests
    path('orders/<str:order_code>/tests/', order.OrderTestCreateView.as_view(), name='order_test_create'),
    path('orders/<str:order_code>/tests/<str:test_uuid>/delete/', order.OrderTestDeleteView.as_view(),
         name='order_test_delete'),
    # order test panels
    path('orders/<str:order_code>/test_profiles/',
         order.OrderTestPanelUpdateView.as_view(), name='order_test_panel_update'),
    # order samples
    path('orders/<str:order_code>/samples/<str:sample_uuid>/update/',
         views.SampleUpdateView.as_view(), name='sample_update'),
    path('orders/<str:order_code>/samples/<str:sample_uuid>/delete/',
         views.SampleDeleteView.as_view(), name='sample_delete'),
    path('orders/<str:order_code>/tests/<str:test_uuid>/approved/', order.OrderTestApprovedView.as_view(),
         name='order_test_approved'),
    path('orders/<str:order_code>/tests/method/<str:test_method>/approved/',
         order.OrderTestMethodApprovedView.as_view(), name='order_test_method_approved'),
    path('orders/<str:order_code>/tests/method/<str:test_method>/approval_revert/',
         order.OrderTestMethodApprovalRevertView.as_view(), name='order_test_method_approval_revert'),
    # order reflex list
    path('orders/<str:order_code>/reflex_tests/',
         order.OrderReflexListView.as_view(), name='order_reflex_list'),
    path('orders/<str:order_code>/reflex_tests/<str:test_type_uuid>/', order.OrderReflexTestCreateView.as_view(),
         name='order_reflex_test_create'),

    # order comments
    path('orders/<str:order_code>/comments/<str:order_comment_uuid>/update/',
         order.OrderCommentUpdateView.as_view(), name='order_comment_update'),
    path('orders/<str:order_code>/comments/<str:order_comment_uuid>/delete/',
         order.OrderCommentDeleteView.as_view(), name='order_comment_delete'),

    path('orders/<str:order_code>/test_profiles/<str:test_panel_uuid>/comments/create/',
         order.OrderTestPanelCommentCreateView.as_view(), name='order_test_panel_comment_create'),
    path('orders/<str:order_code>/test_profiles/<str:test_panel_uuid>/comments/<str:comment_uuid>/update/',
         order.OrderTestPanelCommentUpdateView.as_view(), name='order_test_panel_comment_update'),
    path('orders/<str:order_code>/test_profiles/<str:test_panel_uuid>/comments/<str:comment_uuid>/delete/',
         order.OrderTestPanelCommentDeleteView.as_view(), name='order_test_panel_comment_delete'),

    path('orders/<str:order_code>/tests/<str:test_uuid>/comments/create/',
         order.OrderTestCommentCreateView.as_view(), name='order_test_comment_create'),
    path('orders/<str:order_code>/tests/<str:test_uuid>/comments/<str:comment_uuid>/update/',
         order.OrderTestCommentUpdateView.as_view(), name='order_test_comment_update'),
    path('orders/<str:order_code>/tests/<str:test_uuid>/comments/<str:comment_uuid>/delete/',
         order.OrderTestCommentDeleteView.as_view(), name='order_test_comment_delete'),

    # tests
    path('tests/pending/<str:test_method>/', views.PendingTestView.as_view(), name='pending_test_list'),

    # test results
    path('test_results/', views.TestResultListView.as_view(), name='test_result_list'),

    # test panel types
    path('test_profile_types/', test_panel_type.TestPanelTypeListView.as_view(), name='test_panel_type_list'),
    path('test_profile_types/create/', test_panel_type.TestPanelTypeCreateView.as_view(),
         name='test_panel_type_create'),
    path('test_profile_types/<str:test_panel_uuid>/', test_panel_type.TestPanelTypeDetailsView.as_view(),
         name='test_panel_type_details'),
    path('test_profile_types/<str:test_panel_uuid>/clone/', test_panel_type.TestPanelTypeCloneView.as_view(),
         name='test_panel_type_clone'),
    path('test_profile_types/<str:test_panel_uuid>/update/', test_panel_type.TestPanelTypeUpdateView.as_view(),
         name='test_panel_type_update'),
    path('test_profile_types/<str:test_panel_uuid>/delete/', test_panel_type.TestPanelTypeDeleteView.as_view(),
         name='test_panel_type_delete'),
    path('test_profile_types/categories/create/', test_panel_type.TestPanelCategoryCreateView.as_view(),
         name='test_panel_category_create'),

    # test panel type comments
    path('test_profile_types/<str:test_panel_type_uuid>/comments/create/',
         test_panel_type.TestPanelTypeCommentCreateView.as_view(), name='test_panel_type_comment_create'),
    path('test_profile_types/<str:test_panel_type_uuid>/<str:test_panel_type_comment_uuid>/update/',
         test_panel_type.TestPanelTypeCommentUpdateView.as_view(), name='test_panel_type_comment_update'),
    path('test_profile_types/<str:test_panel_type_uuid>/<str:test_panel_type_comment_uuid>/delete/',
         test_panel_type.TestPanelTypeCommentDeleteView.as_view(), name='test_panel_type_comment_delete'),


    # super test panel types
    path('super_test_profile_types/', test_panel_type.SuperTestPanelTypeListView.as_view(), name='super_test_panel_type_list'),
    path('super_test_profile_types/create/', test_panel_type.SuperTestPanelTypeCreateView.as_view(),
         name='super_test_panel_type_create'),
    path('super_test_profile_types/<str:super_test_panel_uuid>/', test_panel_type.SuperTestPanelTypeDetailsView.as_view(),
         name='super_test_panel_type_details'),
    path('super_test_profile_types/<str:super_test_panel_uuid>/update/', test_panel_type.SuperTestPanelTypeUpdateView.as_view(),
         name='super_test_panel_type_update'),
    path('super_test_profile_types/<str:super_test_panel_uuid>/delete/', test_panel_type.SuperTestPanelTypeDeleteView.as_view(),
         name='super_test_panel_type_delete'),


    # test types
    path('test_types/', test_type.TestTypeListView.as_view(), name='test_type_list'),
    path('test_types/create/', test_type.TestTypeCreateView.as_view(), name='test_type_create'),
    path('test_types/<str:test_type_uuid>/',
         test_type.TestTypeDetailsView.as_view(), name='test_type_details'),
    path('test_types/<str:test_type_uuid>/update/',
         test_type.TestTypeUpdateView.as_view(), name='test_type_update'),
    path('test_types/<str:test_type_uuid>/delete/',
         test_type.TestTypeDeleteView.as_view(), name='test_type_delete'),

    # test type categories
    path('test_type_categories/', test_type.TestTypeCategoryListView.as_view(), name='test_type_category_list'),
    path('test_type_categories/create/', test_type.TestTypeCategoryCreateView.as_view(), name='test_type_category_create'),
    path('test_type_categories/<str:test_type_category_uuid>/', test_type.TestTypeCategoryDetailsView.as_view(), name='test_type_category_details'),
    path('test_type_categories/<str:test_type_category_uuid>/update/', test_type.TestTypeCategoryUpdateView.as_view(), name='test_type_category_update'),


    # test type reference ranges
    path('test_types/<str:test_type_uuid>/ranges/create/',
         test_type.TestTypeRangeCreateView.as_view(), name='test_type_range_create'),
    path('test_types/<str:test_type_uuid>/ranges/<str:range_uuid>/update/',
         test_type.TestTypeRangeUpdateView.as_view(), name='test_type_range_update'),
    path('test_types/<str:test_type_uuid>/ranges/<str:range_uuid>/delete/',
         test_type.TestTypeRangeDeleteView.as_view(), name='test_type_range_delete'),

    # test type reflex logic
    path('test_types/<str:test_type_uuid>/reflexes/create/',
         test_type.TestTypeReflexCreateView.as_view(), name='test_type_reflex_create'),
    path('test_types/<str:test_type_uuid>/reflexes/<str:test_type_reflex_uuid>/update/',
         test_type.TestTypeReflexUpdateView.as_view(), name='test_type_reflex_update'),
    path('test_types/<str:test_type_uuid>/reflexes/<str:test_type_reflex_uuid>/delete/',
         test_type.TestTypeReflexDeleteView.as_view(), name='test_type_reflex_delete'),

    # test type comments
    path('test_types/<str:test_type_uuid>/comments/create/',
         test_type.TestTypeCommentCreateView.as_view(), name='test_type_comment_create'),
    path('test_types/<str:test_type_uuid>/comments/<str:test_type_comment_uuid>/update/',
             test_type.TestTypeCommentUpdateView.as_view(), name='test_type_comment_update'),
    path('test_types/<str:test_type_uuid>/comments/<str:test_type_comment_uuid>/delete/',
             test_type.TestTypeCommentDeleteView.as_view(), name='test_type_comment_delete'),

    # sample list (all samples)
    path('samples/', views.SampleListView.as_view(), name='sample_list'),

    # Containers
    path('container_types/', container.ContainerTypeListView.as_view(), name='container_type_list'),
    path('container_types/create/', container.ContainerTypeCreateView.as_view(), name='container_type_create'),
    path('container_types/<str:container_type_uuid>/', container.ContainerTypeDetailsView.as_view(),
         name='container_type_details'),
    path('container_types/<str:container_type_uuid>/update/', container.ContainerTypeUpdateView.as_view(),
         name='container_type_update'),

    # workqueue
    path('workqueue/', views.WorkqueueView.as_view(), name='workqueue_pending_results'),
    path('workqueue/issues/', views.WorkqueueIssuesView.as_view(), name='workqueue_issues'),
    path('workqueue/tests/', views.WorkqueueTestView.as_view(), name='workqueue_tests'),
    path('workqueue/unreceived/', views.WorkqueueUnreceivedView.as_view(), name='workqueue_unreceived'),
    path('workqueue/pending_approval/', views.WorkqueuePendingApprovalView.as_view(), name='workqueue_pending_approval'),
    path('workqueue/reported/', views.WorkqueueReportedView.as_view(), name='workqueue_reported'),
    path('accession_check/', views.AccessionCheckView.as_view(), name='accession_check'),

    # Batch Test
    path('batches/', batch.BatchListView.as_view(), name='batch_list'),
    path('batches/create/', batch.BatchCreateView.as_view(), name='batch_create'),
    path('batches/<str:batch_uuid>/', batch.BatchDetailsView.as_view(), name='batch_details'),
    path('batches/<str:batch_uuid>/add_samples/',
         batch.BatchAddSamplesView.as_view(), name='batch_add_samples'),
    path('batches/<str:batch_uuid>/update_samples/', batch.BatchUpdateSamplesView.as_view(),
         name='batch_update_samples'),

    # data in
    path('data_in/', data_in.DataInListView.as_view(), name='data_in_list'),
    path('data_in/<str:data_in_uuid>/', data_in.DataInDetailsView.as_view(), name='data_in_details'),
    path('data_in/<str:data_in_uuid>/auto_import/', data_in.DataInAutoImportView.as_view(), name='data_in_auto_import'),

    # controls
    path('controls/', control.ControlListView.as_view(), name='control_list'),
    path('controls/create/', control.ControlCreateView.as_view(), name='control_create'),
    path('controls/<str:control_uuid>/', control.ControlDetailsView.as_view(), name='control_details'),
    path('controls/<str:control_uuid>/known_value/create/', control.ControlKnownValueCreateView.as_view(), name='control_known_value_create'),
    path('controls/<str:control_uuid>/known_value/<str:known_value_uuid>/delete',
         control.ControlKnownValueDeleteView.as_view(), name='control_known_value_delete'),
    path('controls/<str:control_uuid>/update/', control.ControlUpdateView.as_view(), name='control_update'),
    path('controls/<str:control_uuid>/observation/create/', control.ControlObservationCreateView.as_view(),
         name='control_observation_create'),
    path('controls/<str:control_uuid>/observation/<str:observation_uuid>/delete/',
         control.ControlObservationDeleteView.as_view(), name='control_observation_delete'),

    # autocomplete views
    path('order_autocomplete/', autocomplete.OrderAutocomplete.as_view(), name='order_autocomplete'),
    path('order_accession_number_swap_autocomplete/', autocomplete.OrderAccessionNumberSwapAutocomplete.as_view(),
         name='order_accession_number_swap_autocomplete'),
    path('unreceived_order_autocomplete/', autocomplete.UnreceivedOrderAutocomplete.as_view(),
         name='unreceived_order_autocomplete'),
    path('order_sample_autocomplete', autocomplete.OrderSampleAutocomplete.as_view(),
         name='order_sample_autocomplete'),
    path('order_test_type_autocomplete', autocomplete.OrderTestTypeAutocomplete.as_view(),
         name='order_test_type_autocomplete'),
    path('provider_autocomplete/', autocomplete.OrderAutocomplete.as_view(), name='provider_autocomplete'),
    path('sample_autocomplete/', autocomplete.SampleAutocomplete.as_view(), name='sample_autocomplete'),
    path('test_type_autocomplete/', autocomplete.TestTypeAutocomplete.as_view(),
         name='test_type_autocomplete'),
    path('order_test_create_test_type_autocomplete/', autocomplete.OrderTestCreateTestTypeAutocomplete.as_view(),
         name='order_test_create_test_type_autocomplete'),
    path('test_type_category_autocomplete/', autocomplete.TestTypeCategoryAutocomplete.as_view(),
         name='test_type_category_autocomplete'),
    path('test_profile_type_autocomplete/', autocomplete.TestPanelTypeAutocomplete.as_view(),
         name='test_panel_type_autocomplete'),
    path('test_profile_type_category_autocomplete/', autocomplete.TestPanelTypeCategoryAutocomplete.as_view(),
         name='test_panel_type_category_autocomplete'),
    path('test_profile_type_to_exclude_autocomplete/', autocomplete.TestPanelTypeToExcludeAutocomplete.as_view(),
         name='test_panel_type_to_exclude_autocomplete'),
    path('control_autocomplete/', autocomplete.ControlAutocomplete.as_view(), name='control_autocomplete'),
    path('accounts_automplete/', views.AccountsAutocomplete.as_view(),
         name='accounts_autocomplete'),
    path('container_type_autocomplete/', autocomplete.ContainerTypeAutocomplete.as_view(),
         name='container_type_autocomplete'),

    # operational report view
    path('operational_report/', views.OperationalReportView.as_view(), name='operational_report'),

    # report remarks
    path('report_remarks/', views.ReportRemarksDetailsView.as_view(), name='report_remarks_list'),
    path('report_remarks/create', views.ReportRemarksCreateView.as_view(), name='report_remarks_create'),
    path('report_remarks/<str:report_remark_uuid>/update', views.ReportRemarksUpdateView.as_view(), name='report_remarks_update'),

    # document scanning
    path('orders/<str:order_uuid>/documents/', views.OrderDocumentsListView.as_view(), name='order_document_list'),
    path('orders/<str:order_uuid>/documents/<str:document_uuid>/delete/', views.OrderDocumentsDeleteView.as_view(), name='order_document_delete'),

    # edit prescribed medications
    path('orders/<str:order_code>/<str:provider_uuid>/<str:patient_uuid>/prescribed_medications',
         views.OrderEditPatientPrescribedMedications.as_view(), name='order_edit_prescribed_medications'),
]
