$(document).ready(function() {
  // Patient Birth Date Selector
  let patientBirthDate = new Pikaday({
    field: $('#patient-birth-date')[0],
    format: 'YYYY-MM-DD',
    maxDate: moment().toDate(),
    minDate: moment("19200101", "YYYYMMDD"),
    yearRange: 120,
  });

  // Add datepicker and form-control class
  let collectionDate = $("input[id*='collection_date']");
  let sampleType = $("select[id*='sample_type']");
  collectionDate.each(function() {
    new Pikaday({
      field: $(this)[0],
      format: 'YYYY-MM-DD',
      maxDate: moment().toDate()
    });
    $(this).addClass("form-control")
  });

  sampleType.each(function() {
    $(this).addClass("form-control")
  });

  // When add sample is clicked, add date picker for last collection date input
  $('div.sample-form > .add-row').first().click(function() {
    let collectionDate = $("input[id*='collection_date']");
    let sampleType = $("select[id*='sample_type']");
    new Pikaday({
      field: collectionDate.last()[0],
      format: 'YYYY-MM-DD',
      maxDate: moment().toDate()
    });
    collectionDate.last().addClass("form-control");
    sampleType.last().addClass("form-control");
  });


  // Google Maps Autocomplete
  const options = {
    // only return results with a precise address
    types: ['address']
  };
  let accountAddress = document.getElementById('account-address');
  let autocomplete1 = new google.maps.places.Autocomplete(accountAddress, options);
  let patientAddress = document.getElementById('patient-address');
  let autocomplete2 = new google.maps.places.Autocomplete(patientAddress, options);


  //////////////////////////////////////////////////////////////////////////////////////
  $('ul').css({'list-style': 'none'});

  // check for existing or new accounts
  if($("input[name='account_type']:checked").val() === 'Existing Account') {
      $('.new-account').hide();
      $('.existing-account').show();
    } else {
      $('.new-account').show();
      $('.existing-account').hide();
    }

  // if new account is selected, then show the new form section
  $('input[name="account_type"]').click(function(){
    if ($(this).is(':checked')) {
      if($(this).val() === 'Existing Account') {
        // if "existing account" is chosen, hide the div and empty out all of the text fields
        $('.new-account').hide().find('input:text').val('');
        $('.existing-account').show();
      } else {
        $('.new-account').show();
        $('.existing-account').hide();
      }
    }
  });

  // check for existing or new provider
  if($("input[name='provider_type']:checked").val() === 'Existing Provider') {
      $('.new-provider').hide();
      $('.existing-provider').show();
    } else {
      $('.new-provider').show();
      $('.existing-provider').hide();
    }

  // if new account is selected, then show the new form section
  $('input[name="provider_type"]').click(function(){
    if ($(this).is(':checked')) {
      if($(this).val() === 'Existing Provider') {
         // if "existing provider" is chosen, hide the div and empty out all of the text fields
        $('.new-provider').hide().find('input:text').val('');
        $('.existing-provider').show();
      } else {
        $('.new-provider').show();
        $('.existing-provider').hide();
      }
    }
  });

  // check for existing or new patient
  if($("input[name='patient_type']:checked").val() === 'Existing Patient') {
      $('.new-patient').hide();
      $('.existing-patient').show();
    } else {
      $('.new-patient').show();
      $('.existing-patient').hide();
    }

  // if new account is selected, then show the new form section
  $('input[name="patient_type"]').click(function(){
    if ($(this).is(':checked')) {
      if($(this).val() === 'Existing Patient') {
        // if "existing patient" is chosen, hide the div and empty out all of the text fields
        $('.new-patient').hide().find('input:text').val('');
        $('.existing-patient').show();
      } else {
        $('.new-patient').show();
        $('.existing-patient').hide();
      }
    }
  });








});
