from braces.views import LoginRequiredMixin
from django.contrib import messages
from django.db import transaction
from django.shortcuts import render, redirect, get_object_or_404
from django.views.generic import View

from orders import models, data_views, tasks
from lab_tenant.views import LabMemberMixin, LISLoginRequiredMixin
from lab_tenant import models as lab_tenant_models


class DataInListView(LabMemberMixin, View):
    def get(self, request):
        instrument_integrations = lab_tenant_models.InstrumentIntegration.objects.all()
        context = {'instrument_integrations': instrument_integrations}
        return render(request, 'data_in_list.html', context)


class DataInDetailsView(LISLoginRequiredMixin, View):
    def get(self, request, data_in_uuid):
        data_in_object = get_object_or_404(models.DataIn, uuid=data_in_uuid)
        results = data_views.parse_result_file(data_in_uuid)
        context = {
            'data_in': data_in_object,
            'results': results,
                   }
        return render(request, 'data_in_details.html', context)


class DataInAutoImportView(LISLoginRequiredMixin, View):
    """
    Deprecated by async function call
    see orders.consumers.DataInImportConsumer
    """
    @transaction.atomic
    def get(self, request, data_in_uuid):
        tenant = request.tenant
        # async function
        tasks.import_instrument_data.apply_async(
            args=[data_in_uuid, tenant.id, request.user.id],
            queue='default')
        messages.success(request, 'Instrument result import in progress')
        return redirect('data_in_details', data_in_uuid)
