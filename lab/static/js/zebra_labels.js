let selected_device;
let devices = [];

let accessionLabelTemplate = `CT~~CD,~CC^~CT~
	^XA
	~TA000
	~JSN
	^LT0
	^MNW
	^MTT
	^PON
	^PMN
	^LH0,0
	^JMA
	^PR6,6
	~SD15
	^JUS
	^LRN
	^CI0
	^PA0,1,1,0
	^XZ
	^XA
	^MMT
	^PW448
	^LL254
	^LS0
	^FPH,1^FT16,75^A0N,28,28^FH\\^CI28^FDPatientName^FS^CI0
	^FPH,1^FT14,109^A0N,28,28^FH\\^CI28^FDDateOfBirth^FS^CI0
	^FPH,1^FT14,42^A0N,28,28^FH\\^CI28^FDORDER ACCESSION LABEL^FS^CI0
	^FPH,1^FT16,215^A0N,28,28^FH\\^CI28^FDClientName^FS^CI0
	^FPH,1^FT16,242^A0N,28,28^FH\\^CI28^FDAccession Number: AccessionNumber^FS^CI0
	^BY4,3,75^FT14,190^BCN,,N,N
	^FH\\^FD>;AccessionBarcode^FS
	^PQ1,0,1,Y
	^XZ`;

function setup(counter=1) {
	//Get the default device from the application as a first step. Discovery takes longer to complete.
	BrowserPrint.getDefaultDevice("printer", function(device) {
				//Add device to list of devices and to html select element
				selected_device = device;
				devices.push(device);
				// var html_select = document.getElementById("selected_device");
				// var option = document.createElement("option");
				// option.text = device.name;
				// html_select.add(option);

				//Discover any other devices available to the application
				// BrowserPrint.getLocalDevices(function(device_list) {
				// 	for(var i = 0; i < device_list.length; i++)
				// 	{
				// 		//Add device to list of devices and to html select element
				// 		var device = device_list[i];
				// 		if(!selected_device || device.uid != selected_device.uid)
				// 		{
				// 			devices.push(device);
				// 			var option = document.createElement("option");
				// 			option.text = device.name;
				// 			option.value = device.uid;
				// 			html_select.add(option);
				// 		}
				// 	}
				//
				// }, function(){console.log("Error getting local devices")},"printer");

			}, function(error){
				// if (counter < 5){  // Try finding zebra printer 15 times if not found abort
				// 	setup(counter=counter+1);
				// }

				console.log(error);
			})
}

function writeToSelectedPrinter(dataToWrite, counter=1) {
	try{
		if( selected_device != undefined){
			selected_device.send(dataToWrite, undefined, errorCallback);
		}else{
			setup(4);
			console.log("Retrying...selected device undefined");
			if(counter<3){
				writeToSelectedPrinter(dataToWrite, counter=counter+1);
			}
		}
	}
	catch(err){
		console.log(err);
	}
}


var errorCallback = function(errorMessage){
	//writeToSelectedPrinter(dataToWrite,1);
	console.log("Error");
	console.log(errorMessage);
}


function formatCode128CBarcode(sample_id) {
	// Code 128-C can only have even number of digits.
	// If odd number of digits, last digit of barcode must be transcribed to 128-B with '>6'
	let barcode = sample_id;
	if(sample_id.length%2 === 1){  // is odd
		barcode = sample_id.slice(0,sample_id.length - 1) + '>6' + sample_id.slice(sample_id.length - 1, sample_id.length);
	}
  return barcode;
}


function printZebraLabel(patient_name, accession_number, patient_dob, sample_collection_date, client_name, sample_barcode, sample_code, test_methods) {
	if (client_name.length >= 33){
		client_name = decodeHtml(client_name.substring(0, 29)) + '...';
	}

	if (patient_name.length > 28){
		patient_name = decodeHtml(patient_name.substring(0, 25)) + '...';
	}

	for (var i = 0; i < test_methods.length; i++) {
		test_methods[i] = test_methods[i].replace("'", '').replace("'", '').trim();
	}

	let generatedLabel = '';
	let sampleLabelTemplate = `CT~~CD,~CC^~CT~
		^XA
		~TA000
		~JSN
		^LT0
		^MNW
		^MTT
		^PON
		^PMN
		^LH0,0
		^JMA
		^PR4,4
		~SD15
		^JUS
		^LRN
		^CI0
		^PA0,1,1,0
		^XZ
		^XA
		^MMT
		^PW465
		^LL210
		^LS0
		^FPH,1^FT15,31^A0N,25,25^FH\\^CI28^FDPatientName^FS^CI0
		^FPH,1^FT15,57^A0N,25,25^FH\\^CI28^FDDateOfBirth^FS^CI0
		^FPH,1^FT15,83^A0N,25,25^FH\\^CI28^FDCollectionDate^FS^CI0
		^FPH,1^FT359,34^A0N,25,25^FH\\^CI28^FDTestMethod1TestMethod2^FS^CI0
		^FPH,1^FT359,65^A0N,25,25^FH\\^CI28^FDTestMethod3^FS^CI0
		^FPH,1^FT15,168^A0N,25,25^FH\\^CI28^FDClientName^FS^CI0
		^FPH,1^FT15,198^A0N,25,25^FH\\^CI28^FDAccession Number: AccessionNumber^FS^CI0
		^BY2,3,55^FT15,142^BCN,,N,N
		^FH\\^FD>;Barcode^FS
		^PQ1,0,1,Y
		^XZ`;
	sampleLabelTemplate = sampleLabelTemplate.replace("PatientName", patient_name)
    .replace("AccessionNumber", accession_number)
    .replace("DateOfBirth", patient_dob)
    .replace("CollectionDate", sample_collection_date)
    .replace("ClientName", client_name)
    .replace("Barcode", formatCode128CBarcode(sample_barcode));

	if (test_methods.length > 2){
		sampleLabelTemplate = sampleLabelTemplate.replace("TestMethod1", test_methods[0]);
		sampleLabelTemplate = sampleLabelTemplate.replace("TestMethod2", ',' + test_methods[1]) + ',';
		generatedLabel = sampleLabelTemplate.replace("TestMethod3", test_methods[2]);
	}
	else if (test_methods.length == 2){
		sampleLabelTemplate = sampleLabelTemplate.replace("TestMethod1", test_methods[0]);
		if(test_methods[1].length <= 3){
			sampleLabelTemplate = sampleLabelTemplate.replace("TestMethod2", ',' + test_methods[1]);
			generatedLabel = sampleLabelTemplate.replace("TestMethod3", '');
		}
		else {
			sampleLabelTemplate = sampleLabelTemplate.replace("TestMethod3", test_methods[1]);
			generatedLabel = sampleLabelTemplate.replace("TestMethod2", '');
		}


	}
	else if (test_methods.length == 1){
		sampleLabelTemplate = sampleLabelTemplate.replace("TestMethod1", test_methods[0]);
		sampleLabelTemplate = sampleLabelTemplate.replace("TestMethod2", '');
		generatedLabel = sampleLabelTemplate.replace("TestMethod3", '');
	}
	else {
		sampleLabelTemplate = sampleLabelTemplate.replace("TestMethod1", '');
		sampleLabelTemplate = sampleLabelTemplate.replace("TestMethod2", '');
		generatedLabel = sampleLabelTemplate.replace("TestMethod3", '');
	}
  	writeToSelectedPrinter(generatedLabel);
	writeToSelectedPrinter(generatedLabel);
}


/**
 * Deprecated fn that was used to print accession labels for req forms before we implemented document uploading
 * @param patient_name
 * @param accession_number
 * @param patient_dob
 * @param client_name
 */
function printZebraAccessionLabel(patient_name, accession_number, patient_dob, client_name){
	if (client_name.length >= 33){
		client_name = decodeHtml(client_name.substring(0, 29)) + '...';
	}

	if (patient_name.length > 32){
		patient_name = decodeHtml(patient_name.substring(0, 29)) + '...';
	}

	var generatedLabel = '';
	accessionLabelTemplate = accessionLabelTemplate.replace("PatientName", patient_name)
    .replace("AccessionNumber", accession_number)
    .replace("DateOfBirth", patient_dob)
    .replace("ClientName", client_name)
	.replace("AccessionBarcode", accession_number);

	generatedLabel = accessionLabelTemplate;
  	writeToSelectedPrinter(generatedLabel);
	alert("Printing labels for accession " + accession_number);
}

window.onload = setup(1);
