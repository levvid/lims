import os
import sys

import django
import logging

os.environ['DJANGO_SETTINGS_MODULE'] = 'main.settings'
django.setup()

from tenant_schemas.utils import tenant_context
import clients.models as clients_models
import orders.models as orders_models
from orders import generate_billing_hl7_labrcm

TENANT_NAME = 'ildp'
REQUESTED_ORDER_ACCESSIONS = ['20010600001', '20010600002', '20010600003', '20010600004', '20010600005', '20010700001']
# add accession_numbers here to test on local DB
TEST_DB_ACCESSIONS = ['20010700005', '20010700006', '20010700007', '20010700012', '20010800001', '20010900005',
                      '20010900003', '20010900007']


def send_billing_messages():
    """
    :return:
    """
    tenant = clients_models.Client.objects.get(schema_name=TENANT_NAME)

    with tenant_context(tenant):
        # get all reported orders with requisition forms
        reported_orders = orders_models.Order.objects.filter(
            accession_number__in=REQUESTED_ORDER_ACCESSIONS)  # get all reported orders

        print('orders {}'.format(len(reported_orders)))
        for order in reported_orders:
            order.is_billed = False
            print('Generating billing messages for order {} and is billed {}'.format(order.code, order.is_billed))

            if os.environ.get('DEBUG') == 'False' and not os.environ.get('STAGING') == 'True':  # production environment
                generate_billing_hl7_labrcm.send_billing_message_message(tenant.id, order.uuid, preliminary=False,
                                                                         production_environment=True)
            else:  # staging and dev
                generate_billing_hl7_labrcm.send_billing_message_message(tenant.id, order.uuid, preliminary=False,
                                                                         production_environment=False)


if __name__ == '__main__':
    logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)
    send_billing_messages()
