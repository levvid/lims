import django

import logging
import os
import sys

from django.db import transaction

os.environ['DJANGO_SETTINGS_MODULE'] = 'main.settings'
django.setup()

from lab import models as lab_models


@transaction.atomic
def mako_test_codes_setup():
    """
    Adding Mako Test targets to Dendi DB
    :return:
    """

    lab_models.TestTarget.objects.update_or_create(name='Alcohol')
    lab_models.TestTarget.objects.update_or_create(name='Mitragynine')
    lab_models.TestTarget.objects.update_or_create(name='Bupropion', defaults={'is_medication': True})
    lab_models.TestTarget.objects.update_or_create(name='Hydroxybupropion')
    lab_models.TestTarget.objects.update_or_create(name='Norketamine', defaults={'common_name': ['N-Desmethylketamine']})
    lab_models.TestTarget.objects.update_or_create(name='Seproxetine')
    lab_models.TestTarget.objects.update_or_create(name='N-Desmethyltapentadol')
    lab_models.TestTarget.objects.update_or_create(name='Hydroxy Tapentadol')
    lab_models.TestTarget.objects.update_or_create(name='Norfluoxetine')
    lab_models.TestTarget.objects.update_or_create(name='Venlafaxine', defaults={'is_medication': True})
    lab_models.TestTarget.objects.update_or_create(name='O-Desmethylvenlafaxine')
    lab_models.TestTarget.objects.update_or_create(name='Mirtazapine', defaults={'is_medication': True})
    lab_models.TestTarget.objects.update_or_create(name='Topiramate', defaults={'is_medication': True})
    lab_models.TestTarget.objects.update_or_create(name='Lamotrigine', defaults={'is_medication': True})
    lab_models.TestTarget.objects.update_or_create(name='Ketamine', defaults={'is_medication': True})
    lab_models.TestTarget.objects.update_or_create(name='Noroxymorphone')
    lab_models.TestTarget.objects.update_or_create(name='Dextrorphan', defaults={'common_name': ['Levorphanol']})
    lab_models.TestTarget.objects.update_or_create(name= '6-B-Naltrexol')
    lab_models.TestTarget.objects.update_or_create(name='Lysergic Acid Diethylamide', defaults={'common_name': ['LSD']})
    lab_models.TestTarget.objects.update_or_create(name='Ethyl Alcohol', defaults={'common_name': ['ETOH']})
    lab_models.TestTarget.objects.update_or_create(name='Ethyl Sulfate', defaults={'common_name': ['ETS', 'Ethyl Sulphate']})
    lab_models.TestTarget.objects.update_or_create(name='Ethyl Glucuronide', defaults={'common_name': ['ETG']})  # remove same name of test target as common name
    lab_models.TestTarget.objects.update_or_create(name='Venlafaxine', defaults={'common_name': ['Effexor XR']})
    lab_models.TestTarget.objects.update_or_create(name='Mirtazapine', defaults={'common_name': ['Remeron', 'Remeronsoltab']})
    lab_models.TestTarget.objects.update_or_create(name='Topiramate', defaults={'common_name': ['Trokendi XR', 'Qudexy XR', 'Topamax']})
    lab_models.TestTarget.objects.update_or_create(name='Lamotrigine', defaults={'common_name': ['Lamictal XR', 'Lamictal ODT', 'Lamictal']})
    lab_models.TestTarget.objects.update_or_create(name='Risperidone', defaults={'common_name': ['Risperdal M-TAB', 'Risperdal', 'Risperdal Consta']})
    lab_models.TestTarget.objects.update_or_create(name='Venlafaxine')
    lab_models.TestTarget.objects.update_or_create(name='Risperidone', defaults={'is_medication': True})
    lab_models.TestTarget.objects.update_or_create(name='9-Hydroxyrisperidone')
    lab_models.TestTarget.objects.update_or_create(name='Benzoylecgonine', defaults={'common_name': ['Cocaine Metabolite']})

    # meth isomers
    lab_models.TestTarget.objects.update_or_create(name='Desoxyn', defaults={'is_medication': True})
    lab_models.TestTarget.objects.update_or_create(name='Methamphetamine D/L Isomer', defaults={'common_name': ['Dextro', 'Levo']})
    lab_models.TestTarget.objects.update_or_create(name='Vicks Inhaler', defaults={'common_name': ['Vicks VapoInhaler']})
    lab_models.TestTarget.objects.update_or_create(name='Tricyclic Antidepressants', defaults={'category': 'Tricyclic Antidepressants'})
    lab_models.TestTarget.objects.update_or_create(name='Oxidants',
                                                   defaults={'category': 'Oxidants'})


if __name__ == '__main__':
    logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)
    mako_test_codes_setup()
