from django.shortcuts import render

from dal import autocomplete

from . import models


# Create your views here.
class ICD10Autocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs = models.ICD10CM.objects.all()

        if self.q:
            qs = qs.filter(full_code__istartswith=self.q.replace('.', '')) | \
                 qs.filter(abbreviated_description__icontains=self.q)
        return qs


class ProcedureCodeAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs = models.ProcedureCode.objects.all()

        if self.q:
            qs = qs.filter(code__istartswith=self.q)
        return qs

    def get_result_label(self, result):
        return "{} - {}".format(result.code, result.description)
