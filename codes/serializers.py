from . import models
from rest_framework import serializers


# External API Serializers
class ICD10Serializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = models.ICD10CM
        fields = ('full_code', 'uuid', 'full_description')


class ProcedureCodeSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = models.ProcedureCode
        fields = ('uuid', 'code', 'description')


class ProcedureCodeICD10Serializer(serializers.HyperlinkedModelSerializer):
    """
    Procedure code serializer that also shows related icd10 codes
    """
    icd10_codes = ICD10Serializer(many=True)

    class Meta:
        model = models.ProcedureCode
        fields = ('uuid', 'code', 'description', 'icd10_codes')
