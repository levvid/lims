import os
import uuid
import tempfile

import boto3
import django

from django.utils import timezone

os.environ['DJANGO_SETTINGS_MODULE'] = 'main.settings'
django.setup()

from . import models
from clients import models as clients_models
from pdfrw import PdfReader, PdfWriter
from orders import functions

DATETIME_FORMAT = '%Y%m%d%H%M'
BIRTHDATE_FORMAT = '%Y%m%d'
MESSAGE_TYPE = 'DFT^P03'  # post detail financial transactions
PROCESSING_ID_DICT = {'False': 'P',
                      'True': 'D'}
SENDING_APPLICATION = 'DENDI'
TRANSACTION_TYPE = 'CG'  # Charge
VERSION_ID = '2.5.1'
SOURCE_BUCKET = 'media/private/'
DESTINATION_BUCKET = 'Mirth/Integrations/'
AWS_ACCESS_KEY_ID = os.environ.get('S3_ACCESS_KEY')
AWS_SECRET_ACCESS_KEY = os.environ.get('S3_SECRET_KEY')
TESTING_ENVIRONMENT_TENANT = 'Dendi Test'
SOURCE_BUCKET = 'dendi-lis'


def scale_drawing(drawing, scaling_factor):
    """
    Scale a reportlab.graphics.shapes.Drawing()
    object while maintaining the aspect ratio

    """
    scaling_x = scaling_factor
    scaling_y = scaling_factor
    drawing.width = drawing.minWidth() * scaling_x
    drawing.height = drawing.height * scaling_y
    drawing.scale(scaling_x, scaling_y)
    return drawing


def get_department_code(tenant_name):
    """

    :param tenant_name:
    :return: dept code - first letter of each word in tenant name
    """
    tenant_name_list = tenant_name.split()
    letters = [name[0] for name in tenant_name_list]
    return "".join(letters)


def copy_requisition_to_mirth(tenant_id, order, production_environment):
    """

    :param tenant_id:
    :param order:
    :return:
    """
    tenant = clients_models.Client.objects.get(id=tenant_id)
    s3 = boto3.client('s3',
                      aws_access_key_id=AWS_ACCESS_KEY_ID,
                      aws_secret_access_key=AWS_SECRET_ACCESS_KEY,
                      )

    if production_environment:
        requisition_form_destination = '{}{}/{}/{}/{}.pdf'.format(DESTINATION_BUCKET, 'Billing Outbound',
                                                                  tenant.name, 'requisition_forms',
                                                                  order.accession_number)
    else:
        requisition_form_destination = '{}{}/{}/{}/{}.pdf'.format(DESTINATION_BUCKET, 'Billing Outbound',
                                                                  TESTING_ENVIRONMENT_TENANT, 'requisition_forms',
                                                                  order.accession_number)
    # https://github.com/pmaupin/pdfrw/blob/master/examples/cat.py
    writer = PdfWriter()

    order_requisition_form = order.requisition_form.open('rb')
    writer.addpages(PdfReader(order_requisition_form).pages)

    with tempfile.NamedTemporaryFile() as temp:
        writer.write(temp.name)
        s3.upload_file(temp.name, SOURCE_BUCKET, requisition_form_destination)


def send_billing_message_message(tenant_id, order_uuid, preliminary=False, production_environment=False):
    """
    :param production_environment:
    :param tenant_id:
    :param preliminary:
    :param order_uuid:
    :return:
    """
    order = models.Order.objects.select_related('patient',
                                                'patient__user',
                                                'account',
                                                'provider',
                                                'provider__user', ) \
        .prefetch_related('icd10_codes', 'billing_messages', 'results', 'tests__test_type__test_method').get(
        uuid=order_uuid)

    tests = order.tests.filter(is_approved=True).exclude(test_type__test_method__name='Specimen Validity')
    if len(tests) == 0:  # if no approved tests, do not bill
        return

    tenant = clients_models.Client.objects.get(id=tenant_id)
    copy_requisition_to_mirth(tenant.id, order, production_environment)

    if production_environment:
        save_file_name = "Billing Outbound/{}/hl7_messages/{}_{}.hl7".format(tenant.name, order.accession_number,
                                                                             timezone.now())  # Production variable
    else:
        save_file_name = "Billing Outbound/{}/hl7_messages/{}_{}.hl7".format(TESTING_ENVIRONMENT_TENANT,
                                                                             order.accession_number,
                                                                             timezone.now())  # Dev variable
    hl7_message = get_billing_hl7_message(order, tenant.id)

    with tempfile.NamedTemporaryFile() as temp:
        temp.write(hl7_message.encode())

        # if there's already a billing_message message for the order, then the next message must be "amended"
        if models.BillingMessage.objects.filter(order=order, preliminary=False):
            billing_message = models.BillingMessage(order=order, amended=True, preliminary=preliminary)
        else:
            billing_message = models.BillingMessage(order=order, preliminary=preliminary)
        billing_message.hl7_message.save(save_file_name, temp)


def get_billing_hl7_message(order, tenant_id):
    """
    Generate an order out hl7 message for each offsite laboratory
    :param tenant_id:
    :param order:
    :return: A dictionary of hl7 messages or None
    """

    message_control_id = uuid.uuid4()
    # message header
    message = get_msh(tenant_id, message_control_id)
    # event type
    # message += get_evn()

    message += get_in1(order)
    # PID
    message += get_pid(order)

    # PV1
    message += get_pv1(order)

    # financial transaction, for ildp the transaction type is empty
    message += get_ft1(tenant_id, order)

    # procedures, not used for ildp
    # message += get_pr1(order)
    # insurance

    # message = {'message': message,
    #                             'uuid': message_control_id}

    return message


def get_msh(tenant_id, message_control_id):
    """
    Utilized elements: 1 Field Separator, 2 Encoding Characters, 3 Sending Application, 4 Sending Facility,
        5 Receiving Application, 6 Receiving Facility, 7 Date/Time Of Message, 9 Message Type, 10 Message Control ID,
        11 Processing ID, 12 Version ID
    :param tenant_id:
    :param offsite_laboratory:
    :param message_control_id:
    :return:
    """
    tenant = clients_models.Client.objects.get(id=tenant_id)

    sending_facility = "{}^{}".format(tenant.name, tenant.id)
    message_datetime = timezone.now().strftime(DATETIME_FORMAT)
    text = "MSH|^~\&|{}|{}|{}||{}||{}|{}|{}|{}\r".format(SENDING_APPLICATION,
                                                         sending_facility,
                                                         tenant.receiving_application,
                                                         message_datetime,
                                                         MESSAGE_TYPE,
                                                         message_control_id,
                                                         PROCESSING_ID_DICT[os.environ.get('DEBUG')],
                                                         VERSION_ID)
    return text


def get_evn():
    """
    Event type
    Not needed for ILDP
    Utilized elements: 1 Event Type Code, 2 Recorded Date/Time

    :return:
    """
    event_type_code = 'T31'
    recorded_datetime = timezone.now().strftime(DATETIME_FORMAT)

    text = "EVN|{}|{}|\r".format(
        event_type_code,
        recorded_datetime
    )
    return text


def get_pid(order):
    """
    Utilized elements: 2. Patient MRN, 3 Patient Identifier List (Internal ID), 5 Patient Name, 7 Date/Time of Birth,
    8 Administrative Sex, 11 Patient Address, 19 SSN Number
    :return: PID segment
    """
    patient = order.patient
    patient_identifier_list = order.accession_number  # for ILDP is accession #

    middle_initial = get_value_or_blank(order.patient.middle_initial)
    suffix = get_value_or_blank(order.patient.suffix)
    patient_name = "{}^{}^{}^^^".format(patient.user.last_name, patient.user.first_name, middle_initial, suffix)

    if patient.birth_date:
        birth_datetime = patient.birth_date.strftime(BIRTHDATE_FORMAT)
    else:
        birth_datetime = ''
    administrative_sex = patient.sex

    address = patient.address1.split(',') if patient.address1 else []
    if len(address) == 3:
        address_1 = address[0]
        city = address[1]
        state = address[2]
    elif len == 2:
        address_1 = address[0]
        city = ''
        state = ''
    else:
        address_1 = patient.address1
        city = ''
        state = ''

    zip_code = get_value_or_blank(patient.zip_code)
    if not zip_code or zip_code == '':
        zip_code = functions.get_zipcode(address_1)
        if not zip_code: # for imported addresses which have a weird format - example 1757 EAST OHIO PIKE APT 3 AMELIA, OH 45102
            address_components = address_1.split(',')
            if len(address_components) == 2:
                zip_code = address_components[1].strip().split(' ')[1]

    patient_address = '{}^{}^{}^{}^{}'.format(address_1, patient.address2, city, state,
                                              zip_code) if patient.address1 else ''

    patient_ssn = "{}".format(get_value_or_blank(patient.social_security)).replace('(', '').replace(')', '').replace(
        '-', '').replace('-', '')  # remove hyphens and special characters

    text = "PID||{}|{}||{}||{}|{}|||{}||||||||{}|||||||||||||||||||||||||||||||||||\r".format(
        functions.get_patient_mrn(patient.id),
        patient_identifier_list,
        patient_name,
        birth_datetime,
        administrative_sex,
        patient_address,
        patient_ssn
    )
    return text


def get_ft1(tenant_id, order, transaction_type=TRANSACTION_TYPE):
    """
    Financial transaction:
    Utilized elements: 1 Set ID, 4 Transaction Date, 6 Transaction Type, 7 Transaction Code,
    9 Transaction Description (Target Name), 10 Transaction Quantity, 13 Department Code, 14 Insurance Plan ID,
    16.4 - Ordering Location/Sendout Lab
    19 Diagnosis Code - FT1,
    FT1|1|||20190610||D|7ACLONAC|||1|||||||||Z79.899~F11.20~F11.10|||||||
    :return:
    """
    set_id = 1
    tenant = clients_models.Client.objects.get(id=tenant_id)
    tenant_name = tenant.name
    transaction_date = timezone.now().strftime(DATETIME_FORMAT)
    primary_payer = models.OrderPatientPayer.objects.filter(order=order, is_primary=True).first()
    if primary_payer:
        insurance_plan_id = primary_payer.member_id  # made this the primary_payer memberID
    else:
        insurance_plan_id = ''

    # ILDP specific
    transaction_quantity = 1  # counts the # of similar targets?

    department_code = get_department_code(tenant_name)
    filler_order_number = transaction_date

    diagnosis_codes = ''
    for icd10_code in order.icd10_codes.all():
        if len(icd10_code.full_code) > 3:  # add period to codes
            formatted_icd10_code = '{}.{}'.format(icd10_code.full_code[:3], icd10_code.full_code[3:])
        else:
            formatted_icd10_code = icd10_code.full_code

        diagnosis_codes += '{}~'.format(formatted_icd10_code)

    diagnosis_codes = diagnosis_codes[:-1]  # remove last tilde

    text = ""
    for test in order.tests.filter(is_approved=True).exclude(test_type__test_method__name='Specimen Validity'):
        transaction_code = get_value_or_blank(test.test_type.transaction_code)
        transaction_description = test.test_type.test_target.name
        # currently not doing send outs
        offsite_laboratory = ''  # sendout lab/facility
        # if test.test_type.offsite_laboratory:
        #     offsite_laboratory = '^^^{}'.format(test.test_type.offsite_laboratory.)
        text += "FT1|{}|||{}||{}|{}||{}|{}|||{}|{}||{}|||{}||||{}|\r".format(
            set_id,
            transaction_date,
            transaction_type,
            transaction_code,
            transaction_description,
            transaction_quantity,
            department_code,
            insurance_plan_id,
            offsite_laboratory,
            diagnosis_codes,
            filler_order_number
        )
        set_id += 1

    return text


def get_pv1(order):
    """
    Patient Visit Information
    Utilized Elements: 3 Assigned Patient Location, 8 Referring Doctor
    :return:
    """
    assigned_patient_location = ""  # assigned location - where order where was generated from
    provider_npi = order.provider.npi
    text = "PV1|||{}|||||{}^^^^^^^^^^^^NPI|||||||||||||||||||||||||||||||||||||||||||||\r".format(
        assigned_patient_location,
        provider_npi
    )
    return text


def get_pr1(order):
    """
    Procedures
    Utilized elements: 1 Set ID, 2 Procedure Coding Method, 3 Procedure Code, 5 Procedure Date/Time
    :return:
    """
    set_id = 1  # auto increment - first occurrence is 1, second 2...
    procedure_coding_method = "CCC"
    procedure_code = "TEST"
    procedure_datetime = order.completed_date.strftime(DATETIME_FORMAT)

    text = "PR1|{}|{}|{}||{}|\r".format(
        set_id,
        procedure_coding_method,
        procedure_code,
        procedure_datetime
    )
    return text


def get_value_or_blank(value):
    """
    Return empty string if value is None
    :return:
    """
    if value:
        return value
    else:
        return ''


def get_insurance_information(set_id, order_patient_payer, patient):
    """

    """
    insurance_plan_id = get_value_or_blank(order_patient_payer.member_id)  # blank for ildp, not commonly used
    payer_code = order_patient_payer.payer.payer_code
    if payer_code.strip() == '':  # send payer id when host code is blank (new payers)
        insurance_company_id = order_patient_payer.id
    else:
        insurance_company_id = payer_code
    insurance_company_name = order_patient_payer.payer.name

    insurance_address_1 = get_value_or_blank(order_patient_payer.payer.address_1)
    insurance_address_2 = get_value_or_blank(order_patient_payer.payer.address_2)
    insurance_city = get_value_or_blank(order_patient_payer.payer.city)
    insurance_state = get_value_or_blank(order_patient_payer.payer.state)
    insurance_zip = get_value_or_blank(order_patient_payer.payer.zip_code)
    insurance_phone_number = get_value_or_blank(order_patient_payer.payer.phone_number)

    # blank for ildp
    # address1^address2^City^State^Zip
    insurance_company_address = '{}^{}^{}^{}^{}' \
        .format(insurance_address_1, insurance_address_2, insurance_city, insurance_state, insurance_zip)

    group_number = order_patient_payer.group_number
    subscriber_relationship_to_patient = order_patient_payer.subscriber_relationship

    if subscriber_relationship_to_patient != 'Self':
        subscriber_name = '{}^{}^{}'.format(order_patient_payer.subscriber_last_name,
                                            order_patient_payer.subscriber_first_name,
                                            order_patient_payer.subscriber_suffix if order_patient_payer.subscriber_suffix else '')
        subscriber_dob = order_patient_payer.subscriber_birth_date.strftime(BIRTHDATE_FORMAT) if order_patient_payer.subscriber_birth_date else ''
    else:
        subscriber_name = '{}^{}^{}'.format(patient.user.last_name, patient.user.first_name,
                                            patient.suffix if patient.suffix else '')
        subscriber_dob = patient.birth_date.strftime(BIRTHDATE_FORMAT) if patient.birth_date else ''

    subscriber_address = '{}^{}'.format(order_patient_payer.subscriber_address1,
                                        order_patient_payer.subscriber_address2)
    policy_number = order_patient_payer.member_id
    subscriber_sex = order_patient_payer.subscriber_sex  # not needed for ildp

    # abbreviate the subscriber_relationship_to_patient
    if subscriber_relationship_to_patient == 'Spouse':
        subscriber_relationship_to_patient = 'SP'
    else:
        subscriber_relationship_to_patient = subscriber_relationship_to_patient[0]

    text = "IN1|{}|{}|{}|{}|{}||{}|{}||||||||{}|{}|{}|{}|||||||||||||||||{}|||||||{}|\r".format(
        set_id,
        insurance_plan_id,
        insurance_company_id,
        insurance_company_name,
        insurance_company_address,
        insurance_phone_number,
        group_number,
        subscriber_name,
        subscriber_relationship_to_patient,
        subscriber_dob,
        subscriber_address,
        policy_number,
        subscriber_sex
    )

    return text


def get_in1(order):
    """
    Insurance
    Utilized elements: 1 Set ID, 2 Insurance Plan ID, 3 Insurance Company ID, 4 Insurance Company Name, 5 Insurance Company Address,
    , 7 Insurance Company Phone Number, 8 Group Number, 16 Name of Insured (Subscriber), 17 Insured's Relationship to Patient, 18 Insured's Date of Birth, 19 insured's Address,
    36 Policy Number, 43 Insured's Administrative Sex,
    :return:
    """
    set_id = 1

    primary_payer = models.OrderPatientPayer.objects.filter(order=order, is_primary=True).order_by(
        '-created_date').first()
    secondary_payer = models.OrderPatientPayer.objects.filter(order=order, is_secondary=True).order_by(
        '-created_date').first()
    tertiary_payer = models.OrderPatientPayer.objects.filter(order=order, is_tertiary=True).order_by(
        '-created_date').first()

    text = ''
    patient = order.patient
    if primary_payer:
        text += get_insurance_information(set_id, primary_payer, patient)

    if secondary_payer:
        set_id += 1
        text += get_insurance_information(set_id, secondary_payer, patient)

    if tertiary_payer:
        set_id += 1
        text += get_insurance_information(set_id, tertiary_payer, patient)

    return text
