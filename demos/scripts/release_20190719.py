import django
import os

os.environ['DJANGO_SETTINGS_MODULE'] = 'main.settings'
django.setup()

from orders.scripts.ildp_test_target_setup import main as update_targets
from orders.scripts.ildp_test_target_setup import delete_duplicate_drugs

from lab import models as lab_models


def main():
    """
    run after deploy to production
    :return:
    """
    delete_duplicate_drugs()
    update_targets()
    rename_lcms()


def rename_lcms():
    try:
        target = lab_models.TestMethod.objects.get(name='LCMS')
        target.name = 'LC-MS'
        target.save()
    except lab_models.TestMethod.DoesNotExist:
        pass


if __name__ == '__main__':
    main()
