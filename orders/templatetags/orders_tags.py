from django import template

register = template.Library()


@register.filter(name='lookup')
def lookup(value, arg):
    return value[arg]


@register.filter(name='safe_lookup')
def safe_lookup(value, arg):
    lookup = value.get(arg)
    if lookup:
        return lookup
    else:
        return ''


@register.filter(name='get_short_accession_number')
def get_short_accession_number(value):
    """
    used to create 10 digit version of 11 digit accession number
    :param value:
    :param arg:
    :return:
    """
    return value[:6] + value[7:]


@register.filter(name='lookup_management_form')
def lookup(value, arg):
    return value[arg].management_form


@register.filter
def list_item(lst, i):
    try:
        return lst[i]
    except:
        return None
