import datetime
import decimal
import django

import logging
import os
import random
import sys
import timeit
import csv
import requests
import re

from django.db import transaction
from django.utils import timezone
from tenant_schemas.utils import tenant_context

os.environ['DJANGO_SETTINGS_MODULE'] = 'main.settings'
django.setup()

from clients import models as client_models
from orders import models as orders_models
from accounts import models as accounts_models
from lab import models as lab_models
from lab_tenant import models as lab_tenant_models
from patients import models as patients_models
from orders.functions import generate_order_code, generate_sample_code, get_sample_barcode

# import dummy data
from demos.shared_data.shared_data import INSTRUMENTS, TEST_TARGETS
from demos.dummy_data.common import ADDITIONAL_USERS, PROVIDERS, ACCOUNTS, PATIENTS
from demos.dummy_data.toxicology import CONTROLS, DATA_IN_LIST, INSTRUMENT_INTEGRATIONS, TEST_TYPES, \
    TEST_PANEL_GROUPS

from demos.initialization_functions import create_providers, create_accounts, create_users, create_instruments, \
    create_instrument_integrations, create_patients, create_controls, create_data_in_demo, create_test_types, \
    create_test_targets, increment_schema_name

from tasks import initialize_client

"""
Script to set up a new schema for demo purposes
"""

DEMO = True
NUM_ORDERS = 20

# schema variables
SCHEMA_NAME = 'phr'
LABORATORY_NAME = 'Precision Health Resources'
LABORATORY_TIMEZONE = 'US/Eastern'
EMAIL = 'sjung@dendisoftware.com'
FIRST_NAME = 'Simon'
LAST_NAME = 'Jung'
PASSWORD = 'dendipower123123'
SCHEMA_CONTEXT = {'client_name': LABORATORY_NAME,
                  'schema_name': SCHEMA_NAME,
                  'email': EMAIL,
                  'first_name': FIRST_NAME,
                  'last_name': LAST_NAME,
                  'password': PASSWORD,
                  'lab_type': '',
                  'address_1': '2 Harvard Circle #500',
                  'address_2': 'West Palm Beach, Florida 33409',
                  'phone_number': '(561) 684-9700',
                  'fax_number': '(561) 684-9222',
                  'clia_number': '44D208045',
                  'clia_director': 'Michael Jordan',
                  }

SCHEMA_SETTINGS = {"require_icd10": True,
                   "skip_patient_history": False,
                   'show_prescribed_medication': True}

ACCOUNTS_CSV_URL = 'https://dendi-lis.s3.amazonaws.com/client_data/ildp/account_list.csv'
PROVIDERS_CSV_URL = 'https://dendi-lis.s3.amazonaws.com/client_data/ildp/provider_list.csv'
PAYERS_CSV_URL = 'https://dendi-lis.s3.amazonaws.com/client_data/ildp/labgen_payer_list.csv'
PATIENTS_CSV_URL = 'https://dendi-lis.s3.amazonaws.com/client_data/ildp/patient_list.csv'


@transaction.atomic
def main():
    t0 = timeit.default_timer()
    try:
        tenant = client_models.Client.objects.get(schema_name=SCHEMA_CONTEXT['schema_name'])
        logging.debug('{} schema exists'.format(SCHEMA_CONTEXT['schema_name']))
        schema_name = increment_schema_name(SCHEMA_CONTEXT['schema_name'])
        logging.debug('using {} as schema_name'.format(schema_name))
        SCHEMA_CONTEXT['schema_name'] = schema_name
    except client_models.Client.DoesNotExist:
        logging.debug('Creating schema: {}'.format(SCHEMA_CONTEXT['schema_name']))
    initialize_client(SCHEMA_CONTEXT)
    tenant = client_models.Client.objects.get(schema_name=SCHEMA_CONTEXT['schema_name'])
    tenant.timezone = LABORATORY_TIMEZONE
    tenant.settings = SCHEMA_SETTINGS
    tenant.save(update_fields=['timezone', 'settings'])

    logging.debug('Creating users')
    with tenant_context(tenant):
        create_users(ADDITIONAL_USERS)
    t1 = timeit.default_timer()
    logging.debug('Setting up schema - {} seconds'.format(round(t1-t0, 3)))

    logging.debug('Initializing required instruments')
    create_instruments(INSTRUMENTS)
    t3 = timeit.default_timer()
    logging.debug('done - {} seconds'.format(round(t3 - t1, 3)))

    with tenant_context(tenant):
        logging.debug('Initializing instrument integrations')
        create_instrument_integrations(INSTRUMENT_INTEGRATIONS)
        t4 = timeit.default_timer()
        logging.debug('done - {} seconds'.format(round(t4 - t3, 3)))

        # make test panel
        logging.debug('Initializing test panel types')
        initialize_test_panel_types(TEST_PANEL_GROUPS)
        t5 = timeit.default_timer()
        logging.debug('done - {} seconds'.format(round(t5 - t4, 3)))

        # make test types
        logging.debug('Initializing test types')
        test_types = create_test_types(TEST_TYPES, create_panel=True, tox=True)
        initialize_all_inclusive_test_panel_type()
        t6 = timeit.default_timer()
        logging.debug('done - {} seconds'.format(round(t6 - t5, 3)))

        logging.debug('Initializing instrument calibrations')
        initialize_instrument_calibrations()
        t7 = timeit.default_timer()
        logging.debug('done - {} seconds'.format(round(t7 - t6, 3)))

        ################################################################################################################
        # make accounts first because everything else depends on accounts
        """
        logging.debug('Initializing real accounts')
        initialize_accounts()
        t8 = timeit.default_timer()
        logging.debug('done - {} seconds'.format(round(t8 - t7, 3)))

        # make providers second because patients depend on providers and accounts
        logging.debug("Initializing real providers")
        initialize_providers()
        t9 = timeit.default_timer()
        logging.debug('done - {} seconds'.format(round(t9 - t8, 3)))

        # make patients
        logging.debug('Initializing real patients')
        # create_patients(PATIENTS)
        t10 = timeit.default_timer()
        logging.debug('done - {} seconds'.format(round(t10 - t9, 3)))
        """

        if DEMO:
            # make account
            logging.debug('Initializing accounts')
            create_accounts(ACCOUNTS)
            t8 = timeit.default_timer()
            logging.debug('done - {} seconds'.format(round(t8 - t7, 3)))

            # make patients
            logging.debug('Initializing dummy patients')
            create_patients(PATIENTS)
            t9 = timeit.default_timer()
            logging.debug('done - {} seconds'.format(round(t9 - t8, 3)))

            # make providers
            logging.debug("Initializing dummy providers")
            create_providers(PROVIDERS)
            t10 = timeit.default_timer()
            logging.debug('done - {} seconds'.format(round(t10 - t9, 3)))

            # make orders
            logging.debug('Initializing {} dummy orders'.format(NUM_ORDERS))
            initialize_dummy_orders(NUM_ORDERS)
            t11 = timeit.default_timer()
            logging.debug('done - {} seconds'.format(round(t11 - t10, 3)))

            # make some in transit orders
            #logging.debug("Initializing in transit orders")
            #initialize_in_transit_orders()

            # make controls
            logging.debug("Initializing controls")
            create_controls(CONTROLS)
            t12 = timeit.default_timer()
            logging.debug('done - {} seconds'.format(round(t12 - t11, 3)))

            # add data-in
            logging.debug("Initializing data in demo")
            create_data_in_demo(DATA_IN_LIST)
            t13 = timeit.default_timer()
            logging.debug('done - {} seconds'.format(round(t13 - t12, 3)))

            # make dummy control observations
            logging.debug("Initializing control observations")
            initialize_dummy_observations()
            t14 = timeit.default_timer()
            logging.debug('done - {} seconds'.format(round(t14 - t13, 3)))
            logging.debug('total: {} seconds'.format(round(t14-t0, 3)))


def initialize_test_panel_types(test_panel_groups):
    """
    Create group panel types
    :param test_types:
    :return:
    """
    menu_categories = orders_models.TestPanelCategory.objects.bulk_create(
        [orders_models.TestPanelCategory(name=x) for x in TEST_PANEL_GROUPS]
    )
    test_panel_types = orders_models.TestPanelType.objects.bulk_create(
        [orders_models.TestPanelType(
            name=x[1],
            menu_category=x[0],
            category_order=1
        ) for x in zip(menu_categories, test_panel_groups)]
    )
    return menu_categories, test_panel_types


def initialize_all_inclusive_test_panel_type():
    # All inclusive panel
    test_panel_type, created = orders_models.TestPanelType.objects.update_or_create(
        name='All analyte test',
        menu_category_id=1
    )
    test_types = orders_models.TestType.objects.all()
    for test_type in test_types:
        test_panel_type.test_types.add(test_type)


def initialize_instrument_calibrations():
    ms_instruments = lab_tenant_models.InstrumentIntegration.objects.filter(
        internal_instrument_name__icontains='Shimadzu'
    )
    calibrations = []
    for ms_instrument in ms_instruments:
        for test_type_dict in TEST_TYPES:
            test_type = orders_models.TestType.objects.get(name=test_type_dict['name'])
            calibration = orders_models.InstrumentCalibration(
                instrument_integration=ms_instrument,
                test_type=test_type,
                target_name=test_type.test_target.name,
                cutoff_value=test_type_dict.get('cutoff')
            )
            calibrations.append(calibration)
    orders_models.InstrumentCalibration.objects.bulk_create(calibrations)


def initialize_dummy_orders(num_orders):
    urine_sample_type = lab_models.CLIASampleType.objects.get(name='Urine')
    test_panel_type = orders_models.TestPanelType.objects.get(name='All analyte test')

    days_back = max(60, num_orders/1.5)
    received_date_start = timezone.now() - datetime.timedelta(days=days_back)
    average_order_count = int(num_orders / days_back)

    for order_count in range(num_orders):
        latest_order_code = generate_order_code()
        rand_account = accounts_models.Account.objects.get(id=random.randint(1, 9))
        rand_patient = patients_models.Patient.objects.get(id=random.randint(1, 11))
        logging.debug('Order {}: {} {} '.format(order_count, rand_account.name, rand_patient.user.first_name))
        order = orders_models.Order.objects.create(
            provider_id=random.randint(1, 5),
            account=rand_account,
            patient=rand_patient,
            code=latest_order_code,
            research_consent=False,
            patient_result_request=False,
            received_date=received_date_start,
            submitted=True

        )

        latest_sample_code = generate_sample_code(order.code, urine_sample_type)
        sample = orders_models.Sample.objects.create(
            order=order,
            code=latest_sample_code,
            clia_sample_type=urine_sample_type,
            collection_date=timezone.now()
        )
        sample_barcode = get_sample_barcode(sample.id)
        sample.barcode = sample_barcode
        sample.save(update_fields=['barcode'])

        test_panel = orders_models.TestPanel.objects.create(
            order=order,
            test_panel_type=test_panel_type,
            expected_revenue=test_panel_type.cost,
        )

        for test_type in test_panel_type.test_types.all():
            test = orders_models.Test.objects.create(
                order=order,
                sample=sample,
                test_panel=test_panel,
                test_type=test_type,
                due_date=datetime.date.today() + datetime.timedelta(days=test_type.due_date_days)
            )
            result_obj = orders_models.Result.objects.create(
                order=order,
                test=test
            )

        if random.randint(1, average_order_count + 1) == average_order_count + 1:
            if received_date_start + datetime.timedelta(days=1, hours=2) < timezone.now():
                received_date_start = received_date_start + datetime.timedelta(days=1, hours=2)


def initialize_in_transit_orders():
    urine_sample_type = lab_models.CLIASampleType.objects.get(name='Urine')
    test_panel_type = orders_models.TestPanelType.objects.get(name='All analyte test')

    latest_order_code = generate_order_code()
    rand_account = accounts_models.Account.objects.get(id=random.randint(1, 9))
    rand_patient = patients_models.Patient.objects.get(id=random.randint(1, 11))
    logging.debug('In transit order Order: {} {} '.format(rand_account.name, rand_patient.user.first_name))
    order = orders_models.Order.objects.create(
        provider_id=1,
        account=rand_account,
        patient=rand_patient,
        code=latest_order_code,
        research_consent=False,
        patient_result_request=False,

    )

    latest_sample_code = generate_sample_code(order.code, urine_sample_type)
    sample = orders_models.Sample.objects.create(
        order=order,
        code=latest_sample_code,
        clia_sample_type=urine_sample_type,
        collection_date=timezone.now()
    )
    sample_barcode = get_sample_barcode(sample.id)
    sample.barcode = sample_barcode
    sample.save(update_fields=['barcode'])

    test_panel = orders_models.TestPanel.objects.create(
        order=order,
        test_panel_type=test_panel_type,
        expected_revenue=test_panel_type.cost,
    )

    for test_type in test_panel_type.test_types.all():
        test = orders_models.Test.objects.create(
            order=order,
            sample=sample,
            test_panel=test_panel,
            test_type=test_type,
            due_date=datetime.date.today() + datetime.timedelta(days=test_type.due_date_days)
        )
        result_obj = orders_models.Result.objects.create(
            order=order,
            test=test
        )
    order.submitted = True
    order.save(update_fields=['submitted'])


def initialize_dummy_observations():
    """
    create dummy observations for QC charting purposes
    :return:
    """
    days_back = 30
    date_counter = timezone.now() - datetime.timedelta(days=days_back)
    data_in = orders_models.DataIn.objects.first()
    controls = orders_models.Control.objects.all()
    targets = ['Cocaine', 'Lorazepam', 'Carisoprodol', 'Meprobamate']

    control_concentrations = [50.0, 100.0, 250.0, 500.0, 1000.0, 2000.0, 5000.0]

    observation_list = []
    while date_counter < timezone.now():
        control_index = 0
        for control in controls:
            for target in targets:
                observation = orders_models.Observation(
                    control=control,
                    target_name=target,
                    data_in=data_in,
                    result_value=random.uniform(control_concentrations[control_index] - 5.0,
                                                control_concentrations[control_index] + 5.0),
                    observation_date=date_counter,
                )
                observation_list.append(observation)
            control_index += 1
        date_counter += datetime.timedelta(days=1)
    orders_models.Observation.objects.bulk_create(observation_list)


def initialize_accounts():
    # get accounts list CSV for ILDP from S3 bucket
    # convert to list of dictionaries

    with requests.Session() as s:
        download = s.get(ACCOUNTS_CSV_URL)

        decoded_content = download.content.decode('utf-8')

        csv_reader = csv.reader(decoded_content.splitlines(), delimiter=',')
        # skip first line (columns)
        account_list = list(csv_reader)[1:]
        account_dict_list = []

        for row in account_list:
            # format phone/fax
            row[3] = re.sub("\D", "", row[3])
            row[4] = re.sub("\D", "", row[4])
            account_dict_list.append({'alternate_id': row[0],
                                      'name': row[1],
                                      'address1': row[2],
                                      'phone_number': row[3],
                                      'fax_number': row[4]})
        # create & save account objects
        create_accounts(account_dict_list)
        return account_dict_list


def initialize_providers():
    # get CSV from S3 bucket
    # convert to list of dictionaries
    tenant = client_models.Client.objects.get(schema_name=SCHEMA_CONTEXT['schema_name'])
    with tenant_context(tenant):

        with requests.Session() as s:
            download = s.get(PROVIDERS_CSV_URL)

            decoded_content = download.content.decode('utf-8')

            csv_reader = csv.reader(decoded_content.splitlines(), delimiter=',')
            # skip first line (columns)
            provider_list = list(csv_reader)[1:]
            provider_dict_list = []

            for row in provider_list:
                provider_id = row[0]
                provider_last_name = row[5]
                provider_first_name = row[6]
                provider_npi = row[7]
                provider_email = str(provider_id) + '@' + SCHEMA_NAME + '.com'

                provider_dict_list.append({'alternate_id': provider_id,
                                           'first_name': provider_first_name,
                                           'last_name': provider_last_name,
                                           'email': provider_email,
                                           'npi': provider_npi
                                           })

            # create providers first, then add accounts
            create_providers(provider_dict_list)

            for row in provider_list:
                provider_npi = row[7]
                # clean associated account data
                account_id_list = [row[1], row[2], row[3], row[4]]
                # clean empty items from list
                account_id_list = list(filter(None, account_id_list))
                accounts = accounts_models.Account.objects.filter(alternate_id__in=account_id_list)

                # link providers to accounts
                provider = accounts_models.Provider.objects.get(npi=provider_npi)
                provider.accounts.set(accounts)
                logging.debug('Accounts ' + str(accounts) + ' added to provider ' + str(provider))


def initialize_payers():
    # get CSV from S3 bucket
    # convert to list of dictionaries
    tenant = client_models.Client.objects.get(schema_name=SCHEMA_CONTEXT['schema_name'])
    with tenant_context(tenant):
        with requests.Session() as s:
            download = s.get(PAYERS_CSV_URL)

            decoded_content = download.content.decode('utf-8')

            csv_reader = csv.reader(decoded_content.splitlines(), delimiter=',')
            # skip first line (columns)
            payer_list = list(csv_reader)[1:]
            payer_dict_list = []

            for row in payer_list:
                row[1] = str(row[1]).strip().upper()
                # simple parse
                if 'MEDICARE' in row[1]:
                    payer_type = 'Medicare'
                elif 'MEDICAID' in row[1]:
                    payer_type = 'Medicaid'
                else:
                    payer_type = 'Private'

                payer_dict_list.append({'alternate_id': row[0],
                                        'name': row[1],
                                        'payer_type': payer_type})
            payers = []
            for payer_dict in payer_dict_list:
                payer = patients_models.Payer(alternate_id=payer_dict.get('alternate_id'),
                                              name=payer_dict.get('name'),
                                              payer_type=payer_dict.get('payer_type'))
                payers.append(payer)
            patients_models.Payer.objects.bulk_create(payers)

            """
            # if we are to update instead of creating from scratch
            payer, created = patients_models.Payer.objects.update_or_create(alternate_id=payer_dict.get('alternate_id'),
                                                                            defaults={
                                                                                'name': payer_dict.get('name'),
                                                                                'payer_type': payer_dict.get(
                                                                                    'payer_type')
                                                                            })
            if created:
                logging.debug('Payer ' + str(payer) + ' added.')
            """
            logging.debug('Payers added.')


def initialize_patients():
    # get CSV from S3 bucket
    # convert to list of dictionaries
    tenant = client_models.Client.objects.get(schema_name=SCHEMA_CONTEXT['schema_name'])
    with tenant_context(tenant):
        with requests.Session() as s:
            download = s.get(PATIENTS_CSV_URL)

            decoded_content = download.content.decode('utf-8')

            csv_reader = csv.reader(decoded_content.splitlines(), delimiter=',')
            # skip first line (columns)
            patient_list = list(csv_reader)[1:]

            patient_dict_list = []

            for row in patient_list:
                # strip out whitespace
                row = list(map(str.strip, row))

                last_name = row[1]
                first_name = row[2]
                middle_initial = row[3]
                sex = row[4]
                birth_date = datetime.datetime.strptime(row[5], '%m/%d/%y')
                # string parsing means that if the birthday is mm/dd/2025, it'll automatically convert to 1925
                if birth_date.year > datetime.datetime.now().year:
                    birth_date = birth_date.replace(year=birth_date.year - 100)
                address = row[6] + row[7] + ' ' + row[8] + ', ' + row[9] + ' ' + row[10]

                provider_alternate_id = row[12]

                try:
                    payer1_alternate_id = int(row[13])
                    # payer_id = 0 is invalid
                    if payer1_alternate_id > 0:
                        payer1_policy = row[15]
                        payer1_group = row[16]
                except:
                    payer1_alternate_id = None
                    payer1_policy = None
                    payer1_group = None


                try:
                    payer2_alternate_id = int(row[17])
                    # payer_id = 0 is invalid
                    if payer2_alternate_id > 0:
                        payer2_alternate_id = None
                        payer2_policy = row[15]
                        payer2_group = row[16]
                except:
                    payer2_policy = None
                    payer2_group = None

                # SE (self) SP (spouse) CH (child) PA (parent), and OT (other)
                # if invalid value, default to SE
                relation = row[23]
                if relation not in ['SE', 'SP', 'CH', 'PA', 'OT']:
                    relation = 'SE'

                if relation == 'PA':
                    relationship_to_patient = 'Parent'
                if relation == 'SP':
                    relationship_to_patient = 'Spouse'
                if relation == 'CH':
                    relationship_to_patient = 'Dependent'
                if relation == 'OT':
                    relationship_to_patient = 'Other'

                print(row)


if __name__ == '__main__':
    logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)
    main()

