import boto3
import socket
import xlsxwriter
import decimal
import time
import channels.layers
import django
import logging
import os
import platform
import datetime
import sys
import serial
import json
import pathlib
import itertools
import tempfile
from dotenv import load_dotenv

from asgiref.sync import async_to_sync
from serial.tools import list_ports

from main.functions import convert_utc_to_timezone


INSTRUMENT_NAME = 'Olympus AU400'

TEXT_START_BLOCK = b'\x02'
TEXT_END_BLOCK = b'\x03'
BLOCK_END_BLOCK = b'E'
DUMMY_BLOCK = b' ' * 4

# Requisition inquiry is what the AU sends the LIS
# Requisition is what the the LIS sends back
REQUISITION_INQUIRY_START_BLOCK = b'RB'
REQUISITION_INQUIRY_END_BLOCK = b'RE'

REQUISITION_START_BLOCK = b'S '
REQUISITION_END_BLOCK = b'\x02SE\x03'

RESULTS_START_BLOCK = b'DB'
RESULTS_END_BLOCK = b'DE'

S3_BUCKET = 'dendi-lis'
SAVE_PATH = 'client_data/newstar/AU 2700 Data/'

TEST_CODE_DICTONARY = {
    '01': 'pHR',
    '02': 'SPECGRAVR',
    '03': 'CREATR',
    '04': 'AMPSR',
    '05': 'BARBSR',
    '06': 'BENZSR',
    '07': 'BUPSR',
    '08': 'OPISR',
}

TEST_CUTOFF = {
    'AMPSR': 1000,
    'BARBSR': 300,
    'BENZSR': 300,
    'BUPSR': 5,
    'OPISR': 300,
    'pHR': '-',
    'SPECGRAVR': '-',
    'CREATR': '-',
}

RESULT_XLSX_TEST_ORDER = ['AMPSR', 'BARBSR', 'BENZSR', 'BUPSR', 'OPISR', 'pHR', 'SPECGRAVR', 'CREATR']

def chunks(l, n):
    """Yield successive n-sized chunks from l."""
    for i in range(0, len(l), n):
        yield l[i:i + n]


def data_str_to_json(data_str_list, barcode_length):
    """
    Rack number --> data_str_list[2:6]
    Cup position --> data_str_list[6:8]
    Sample type --> data_str_list[8]
    Sample number (barcode) --> data_str_list[9:13]
    """
    # dummy block used for some reason
    dummy_block_length = 4
    end_of_sample_id_index = 14

    sample_number = data_str_list[10:end_of_sample_id_index].decode('utf-8')
    end_of_barcode_index = end_of_sample_id_index + dummy_block_length + barcode_length

    # parse into barcode even if empty
    barcode_str = data_str_list[end_of_sample_id_index:end_of_sample_id_index + barcode_length].decode('utf-8').strip()

    data_dict = {}
    data_dict['sample_number'] = sample_number
    data_dict['sample_barcode'] = barcode_str
    # multiple tests can be done on a sample
    data_dict['tests'] = []

    # chunk barcodes by their length
    test_code_and_results_chunked = list(chunks(data_str_list[end_of_barcode_index+1:], barcode_length))

    for test in test_code_and_results_chunked:
        test_code = test[:2]
        if test_code == b'\x03':
            continue

        test_code = test_code.decode('utf-8')

        try:
            test_result = float(test[2:8])
        except ValueError as e:
            test_result = 0
            error_text = 'Invalid/negative result {} for test code: {}\n'.format(test[2:8], test_code)
            print(error_text)

        data_dict['tests'].append({'test_code': test_code,
                                   'test_result': test_result})
    return data_dict


def requisition_mapper(req_bytes, barcode_length):
    """
    Parse requisition data and return associated test codes in string blocks
    """
    rack_number = req_bytes[3:7].decode('utf-8')

    cup_position = req_bytes[7:9].decode('utf-8')
    sample_type = bytes([req_bytes[9]]).decode('utf-8')
    sample_number = req_bytes[10:14].decode('utf-8')

    # print(sample_type)
    # parse into barcode even if empty
    barcode = req_bytes[14:14 + barcode_length].decode('utf-8')

    # get sample
    requisition_dict = {}
    requisition_dict['rack_number'] = rack_number
    requisition_dict['cup_position'] = cup_position
    requisition_dict['sample_number_block'] = sample_number
    requisition_dict['sample_type_block'] = sample_type
    requisition_dict['barcode_block'] = barcode
    test_codes = TEST_CODE_DICTONARY.keys()
    requisition_dict['test_codes_block'] = ''.join(test_codes)

    return requisition_dict


def save_xlsx_to_s3(results):
    """
    Save results to an excel file used to import into Labeaz LIS
    :param results:
    :return:
    """
    print("creating xlsx\n")
    with tempfile.NamedTemporaryFile() as temp:
        workbook = xlsxwriter.Workbook(temp.name)
        # results worksheet
        results_worksheet = workbook.add_worksheet('Results')
        results_worksheet.write(0, 0, 'Accession')
        results_worksheet.write(0, 1, 'Test')
        results_worksheet.write(0, 2, 'Result')
        results_worksheet.write(0, 3, 'Cutoff')

        row = 1
        for result_sample_barcode in results:
            accession = result_sample_barcode
            accession_results = results[result_sample_barcode]['results']

            for test_name in RESULT_XLSX_TEST_ORDER:
                try:
                    results_worksheet.write(row, 0, int(accession))
                except ValueError:  # when there's an error with accession #
                    break
                results_worksheet.write(row, 1, test_name)
                try:
                    results_worksheet.write(row, 2, float(accession_results[test_name]))
                except KeyError:  # when there's an error with accession #
                    break
                results_worksheet.write(row, 3, TEST_CUTOFF[test_name])
                row += 1

        # accession worksheet
        accession_worksheet = workbook.add_worksheet('Accessions')
        accession_worksheet.write(0, 0, 'Accession')
        row = 1
        for result_sample_barcode in results:
            accession_worksheet.write(row, 0, int(result_sample_barcode))
            row += 1

        workbook.close()
        file_name = "{}{}.xlsx".format(SAVE_PATH, datetime.datetime.now())
        print("saving as: {}\n".format(file_name))
        s3 = boto3.client(
            's3',
            aws_access_key_id=AWS_ACCESS_KEY_ID,
            aws_secret_access_key=AWS_SECRET_ACCESS_KEY,
        )
        s3.upload_file(temp.name, S3_BUCKET, file_name)
        print("save successful!\n")


def make_connection(barcode_length):
    # Create a TCP/IP socket
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    # Bind the socket to the port
    server_address = ('', 9090)
    print('Starting up on {} port {}'.format(*server_address))
    sock.bind(server_address)

    # Listen for incoming connections
    sock.listen(1)
    req_begin_status = False
    result_begin_status = False
    results = {}

    while True:
        # Wait for a connection
        print('waiting for a connection\n')
        connection, client_address = sock.accept()
        try:
            print('connection from\n', client_address)

            # Receive the data in small chunks and retransmit it
            while True:
                data = connection.recv(4096)
                print('received data is {}\n'.format(data))

                if data == b'\x06':
                    print('Requisition data successfully received by AU (ACK).\n')
                    continue

                if data == b'\x15':
                    print('Requisition data transfer error (NAK).\n')
                    continue

                # send ack
                connection.send(str.encode('\x06'))

                if data == b'\x02RB\x03':
                    print('req beginning\n')
                    req_begin_status = True
                    continue

                if data == b'\x02DB\x03':
                    print('Data transfer beginning\n')
                    result_begin_status = True
                    continue

                if data == b'\x02RE\x03':
                    req_begin_status = False
                    continue

                if data == b'\x02DE\x03':
                    print('Data transfer ending\n')
                    result_begin_status = False
                    if len(results) > 0:
                        print('Results: {}'.format(results))
                        save_xlsx_to_s3(results)
                    results = {}
                    continue

                if req_begin_status and bytes([data[1]]) == b'R' and len(data) > 4:
                    requisition_dict = requisition_mapper(data, barcode_length)
                    rack_number_block = requisition_dict['rack_number'].encode('utf-8')
                    cup_position_block = requisition_dict['cup_position'].encode('utf-8')
                    sample_type_block = requisition_dict['sample_type_block'].encode('utf-8')
                    sample_number_block = requisition_dict['sample_number_block'].encode('utf-8')
                    barcode_block = requisition_dict['barcode_block'].encode('utf-8')
                    test_codes_block = requisition_dict['test_codes_block'].encode('utf-8')
                    # requisition block
                    requisition = TEXT_START_BLOCK + REQUISITION_START_BLOCK + rack_number_block + \
                                  cup_position_block + sample_type_block + \
                                  sample_number_block + barcode_block + DUMMY_BLOCK + BLOCK_END_BLOCK + \
                                  test_codes_block + TEXT_END_BLOCK + REQUISITION_END_BLOCK

                    print('Sending requisition data...\n')
                    print("{}\n".format(requisition))
                    connection.send(requisition)
                    print("Done sending req...\n")
                    continue

                # results
                if result_begin_status and bytes([data[1]]) == b'D' and len(data) > 4:
                    data_dict = data_str_to_json(data, barcode_length)
                    print("{} {}\n".format(datetime.datetime.now(), data_dict))
                    # add results to results dictionary
                    for test_result in data_dict['tests']:
                        test_name = TEST_CODE_DICTONARY[test_result['test_code']]
                        try:
                            results[data_dict['sample_barcode']]['results'][test_name] = test_result['test_result']
                        except KeyError:
                            results[data_dict['sample_barcode']] = {}
                            results[data_dict['sample_barcode']]['sample_number'] = data_dict['sample_number']
                            results[data_dict['sample_barcode']]['results'] = {}
                            results[data_dict['sample_barcode']]['results'][test_name] = test_result['test_result']
                    continue

        finally:
            # Clean up the connection
            print("Closing current connection\n")
            connection.close()


if __name__ == '__main__':
    dotenv_path = '.env'
    load_dotenv(dotenv_path)
    AWS_ACCESS_KEY_ID = os.environ.get('S3_ACCESS_KEY')
    AWS_SECRET_ACCESS_KEY = os.environ.get('S3_SECRET_KEY')
    while True:
        try:
            make_connection(10)
        except OSError:
            print('connection failed, sleeping for 60 seconds and trying again\n')
            time.sleep(60)
