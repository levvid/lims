import os
import sys

import django
import logging

os.environ['DJANGO_SETTINGS_MODULE'] = 'main.settings'
django.setup()

from tenant_schemas.utils import tenant_context
import clients.models as clients_models
import patients.models as patients_models
import usaddress

TENANT_NAME = 'ildp'
EXCLUDED_ACCOUNT_IDS = [86, 87]


def main():
    """
    :return:
    """
    tenant = clients_models.Client.objects.get(schema_name=TENANT_NAME)
    no_zip_counter = 0
    no_zip_addresses = []
    no_city_counter = 0
    no_city_addresses = []
    no_state_counter = 0
    no_state_addresses = []
    parse_fail_counter = 0
    parse_fail_addresses = []

    with tenant_context(tenant):
        patients = patients_models.Patient.objects.exclude(address1='').exclude(address1=' ,  ')
        # exclude patients associated with Joynell and Family Practice Associates
        for account_id in EXCLUDED_ACCOUNT_IDS:
            patients = patients.exclude(providers__accounts__id=account_id)

        updated_patients = []
        for patient in patients:
            patient_address_string = patient.address1
            try:
                address = usaddress.tag(patient_address_string)[0]
            except usaddress.RepeatedLabelError:
                parse_fail_counter += 1
                parse_fail_addresses.append(patient.address1)
                continue

            try:
                zip_code = address['ZipCode']
                patient.zip_code = zip_code
                patient_address_string = patient_address_string.replace(zip_code, '')
            except KeyError:
                print(patient_address_string)
                no_zip_addresses.append(patient.address1)
                no_zip_counter += 1

            try:
                state = address['StateName']
                patient.state = state
                patient_address_string = patient_address_string.replace(state, '')
            except KeyError:
                no_state_counter += 1
                no_state_addresses.append(patient.address1)

            try:
                city = address['PlaceName']
                patient.city = city
                patient_address_string = patient_address_string.replace(city, '')
            except KeyError:
                no_city_counter += 1
                no_city_addresses.append(patient.address1)

            # address 1 cleaning
            patient_address_string = patient_address_string.strip()
            if patient_address_string[-1] == ',':
                patient_address_string = patient_address_string[:-1].strip()
            patient.address1 = patient_address_string

            updated_patients.append(patient)

        print("{} total patients with addresses.".format(patients.count()))
        print("{} failed parsing.".format(parse_fail_counter))
        print("{} no zip.".format(no_zip_counter))
        print("{} no city.".format(no_city_counter))
        print("{} no state.".format(no_state_counter))
        patients_models.Patient.objects.bulk_update(updated_patients, ['zip_code', 'state', 'city', 'address1'])


if __name__ == '__main__':
    logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)
    main()
