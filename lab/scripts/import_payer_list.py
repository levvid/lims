import codecs
import csv
import datetime
import django
import logging
import os
import sys
import tempfile

import requests

os.environ['DJANGO_SETTINGS_MODULE'] = 'main.settings'
django.setup()

from .. import models

CSV_URL = 'https://dendi.s3.amazonaws.com/payer_list_availity_2019-06-24.csv'


def main(csv_url):
    """
    download and parse payers
    """
    run_status = {'MultipleObjectsReturned_count': 0,
                  'MultipleObjectsReturned_list': [],
                  'new_row_count': 0}

    logging.debug("downloading payer list...")
    with requests.Session() as s:
        download = s.get(csv_url)

        decoded_content = download.content.decode('utf-8')

        csv_reader = csv.reader(decoded_content.splitlines(), delimiter=',')
        my_list = list(csv_reader)
        for row in my_list:
            payer_name = row[0]
            payer_code = row[1]
            payer_type = row[2]

            # parse payer types further
            if payer_type == 'Commercial':
                payer_type = 'Private'
            if payer_type == 'Blue':
                payer_type = 'Private'

            try:
                obj, created = models.Payer.objects.update_or_create(payer_code=payer_code,
                                                                     defaults={'name': payer_name,
                                                                               'payer_type': payer_type})
                if created:
                    logging.debug('New payer added: {} - {}'.format(payer_name, payer_code))
                    run_status['new_row_count'] += 1

            except models.Payer.MultipleObjectsReturned:
                run_status['MultipleObjectsReturned_count'] += 1
                run_status['MultipleObjectsReturned_list'].append(row)

    return run_status


if __name__ == '__main__':
    logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)
    main(CSV_URL)
