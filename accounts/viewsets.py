from rest_framework import viewsets
from lab_tenant.models import User
from . import models
from . import serializers
from django.http import HttpResponse, JsonResponse
from django.core.exceptions import ValidationError


class CollectorViewSet(viewsets.ModelViewSet):
    """
    External API endpoint that allows collectors to be created.
    POST request ---
    Required Fields
    first_name : String : First name of patient
    last_name : String : Last name of patient
    account_uuids : [String] : Accounts associated with patient (at least 1 uuid required)
    ex. ["f6fab634-3ac7-4d53-8ff9-52d84828e8ac", "f6fab634-3ac7-4d53-8ff9-52d84828e8ac"]

    Optional Fields
    email : String
    username : String

    Provide username and email to create logins for collector.
    Reset password at login screen.
    ----

    Get request ---
    account_uuid : filter results by collectors relevant to a specific account
    ---
    """
    serializer_class = serializers.CollectorSerializer
    http_method_names = ['post', 'get']
    lookup_field = 'uuid'

    def get_queryset(self):
        account_uuid = self.request.query_params.get('account_uuid')
        collectors = models.Collector.objects.all()
        if account_uuid:
            try:
                account = models.Account.objects.get(uuid=account_uuid)
                collectors = collectors.filter(accounts=account)
            except ValidationError:
                collectors = collectors.none()
            except models.Account.DoesNotExist:
                collectors = collectors.none()
        return collectors


class AccountViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows Accounts to be viewed
    """
    queryset = models.Account.objects.all()
    serializer_class = serializers.AccountSerializer
    http_method_names = ['get']
    lookup_field = 'uuid'


class AccountProviderViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows Providers for a specific Account to be viewed
    """
    serializer_class = serializers.ProviderSerializer
    http_method_names = ['get']
    lookup_field = 'uuid'

    def get_queryset(self):
        print(self.kwargs)
        account = models.Account.objects.get(uuid=self.kwargs['account_uuid'])
        providers = account.providers.all()
        return providers


class ProviderViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows Providers to be viewed
    """
    queryset = models.Provider.objects.all()
    serializer_class = serializers.ProviderSerializer
    http_method_names = ['get']
    lookup_field = 'uuid'


class UserAccountViewSet(viewsets.ModelViewSet):
    """
    External API endpoint that allows Accounts relevant for a specific User to be viewed
    user_id Required: Int
    """
    serializer_class = serializers.UserAccountSerializer
    http_method_names = ['get']
    lookup_field = 'uuid'

    def get_queryset(self):
        user_id = self.request.query_params.get('user_id')
        accounts = models.Account.objects.all()
        if user_id:
            try:
                user = User.objects.get(id=user_id)
            except User.DoesNotExist:
                return accounts.none()
            except ValueError:
                return accounts.none()
            if user.user_type == 1:
                pass
            elif user.user_type == 9:  # Account user
                accounts = accounts.filter(user=user)
            elif user.user_type == 10:  # Provider user
                provider = models.Provider.objects.get(user=user)
                accounts = accounts.filter(providers=provider)
            else:
                accounts = accounts.none()
        else:
            accounts = accounts.none()
        return accounts


class UserAccountAutocompleteViewSet(viewsets.ModelViewSet):
    """
    External API endpoint that allows Accounts relevant for a specific User to be viewed
    user_id Required: Int
    """
    serializer_class = serializers.UserAccountSerializer
    http_method_names = ['get']
    lookup_field = 'uuid'

    def get_queryset(self):
        user_id = self.request.query_params.get('user_id')
        query = self.request.query_params.get('q')

        accounts = models.Account.objects.all()
        if user_id:
            try:
                user = User.objects.get(id=user_id)
            except User.DoesNotExist:
                return accounts.none()
            except ValueError:
                return accounts.none()
            if user.user_type == 1:
                pass
            elif user.user_type == 9:  # Account user
                accounts = accounts.filter(user=user)
            elif user.user_type == 10:  # Provider user
                provider = models.Provider.objects.get(user=user)
                accounts = accounts.filter(providers=provider)
            else:
                accounts = accounts.none()
        else:
            accounts = accounts.none()

        if query:
            accounts = accounts.filter(name__icontains=query) | \
                       accounts.filter(alternate_id__icontains=query)
            accounts = accounts.distinct()

        return accounts


class ProviderAutocompleteViewSet(viewsets.ModelViewSet):
    """
    External API endpoint that allows providers to be queried for autocomplete
    account_uuid Optional: UUID
    q optional: query, searches for user.last_name__icontains and npi__icontains
    """
    serializer_class = serializers.UserProviderSerializer
    http_method_names = ['get']

    def get_queryset(self):
        providers = models.Provider.objects.all()
        account_uuid = self.request.query_params.get('account_uuid')
        query = self.request.query_params.get('q')

        if account_uuid:
            try:
                account = models.Account.objects.get(uuid=account_uuid)
                providers = providers.filter(accounts=account)
            except models.Account.DoesNotExist:
                return providers.none()
            except ValidationError:
                return providers.none()

        if query:
            providers = providers.filter(user__last_name__icontains=query) | providers.filter(npi__icontains=query) | \
                        providers.filter(user__first_name__icontains=query)
            providers = providers.distinct()

        return providers


class UserProviderViewSet(viewsets.ModelViewSet):
    """
    External API endpoint that allows Providers relevant for a specific User to be viewed
    user_id Required: Int
    account_uuid: String : UUID of account related to providers
    """
    serializer_class = serializers.UserProviderSerializer
    http_method_names = ['get']
    lookup_field = 'uuid'

    def get_queryset(self):
        user_id = self.request.query_params.get('user_id')
        account_uuid = self.request.query_params.get('account_uuid')

        providers = models.Provider.objects.all()
        if user_id:
            try:
                user = User.objects.get(id=user_id)
            except User.DoesNotExist:
                return providers.none()
            except ValueError:
                return providers.none()
            if user.user_type == 1:
                pass
            elif user.user_type == 9:  # Account User
                account = models.Account.objects.get(user=user)
                providers = providers.filter(accounts=account)
            elif user.user_type == 10:  # Provider User
                providers = providers.filter(user=user)
        else:
            providers = providers.none()

        if account_uuid:
            try:
                account = models.Account.objects.get(uuid=account_uuid)
            except models.Account.DoesNotExist:
                return providers.none()
            except ValidationError:
                return providers.none()
            providers = providers.filter(accounts=account)
        return providers
