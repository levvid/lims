import django
import logging
import os
import sys
import timeit

from django.db import connection, models, transaction
from django.utils import timezone
from tenant_schemas.utils import tenant_context

os.environ['DJANGO_SETTINGS_MODULE'] = 'main.settings'
django.setup()

from clients import models as client_models
from orders import models as orders_models

# import data
from .initialization_data import ADDITIONAL_USERS
from demos.initialization_functions import create_users, get_unique_schema_name
from demos.import_functions import import_test_type_from_csv_url
from tasks import initialize_client

"""
Script to set up Mako's schema
"""

DEMO = False

# schema variables
SCHEMA_NAME = 'mako'
LABORATORY_NAME = 'Mako Medical'
LABORATORY_TIMEZONE = 'US/Eastern'
EMAIL = 'sjung@dendisoftware.com'
FIRST_NAME = 'Simon'
LAST_NAME = 'Jung'
PASSWORD = 'dendipower123123'
SCHEMA_CONTEXT = {'client_name': LABORATORY_NAME,
                  'schema_name': SCHEMA_NAME,
                  'email': EMAIL,
                  'first_name': FIRST_NAME,
                  'last_name': LAST_NAME,
                  'password': PASSWORD,
                  'lab_type': '',
                  'address_1': '8461 Garvey Dr',
                  'address_2': 'Raleigh, NC 27616',
                  'phone_number': '(844) 625-6522',
                  'fax_number': '(615) 630-7798',
                  'clia_number': '44D208045',
                  'clia_director': 'Mike Horton',
                  'logo_file': 'ildp_logo.png',
                  'website': 'www.makomedical.com',
                  'results_outbound_id': 'CTRON',
                  'receiving_application': 'Telcor'
                  }

SCHEMA_SETTINGS = {"require_icd10": True,
                   "skip_patient_history": False,
                   'show_prescribed_medication': True,
                   'use_docraptor': True,
                   'hide_fda_tests': True,
                   'enable_accession_number': True,
                   'enable_date_encoded_accession_number': True,
                   'use_zebra_printer': True,
                   'enable_individual_analytes': True,
                   }

TEST_TYPE_CSV = 'https://dendi-lis.s3.amazonaws.com/client_data/mako/test_codes/' \
                'Dendi_TC_12.4.19_cleaned_20191223-13.csv'


@transaction.atomic
def main(demo=DEMO):
    """
    Create & instantiate new LIS schema
    :param demo:
    :return:
    """
    SCHEMA_CONTEXT['schema_name'] = get_unique_schema_name(SCHEMA_CONTEXT['schema_name'])
    initialize_client(SCHEMA_CONTEXT)

    tenant = client_models.Client.objects.get(schema_name=SCHEMA_CONTEXT['schema_name'])
    tenant.timezone = LABORATORY_TIMEZONE
    tenant.settings = SCHEMA_SETTINGS
    tenant.save(update_fields=['timezone', 'settings'])

    with tenant_context(tenant):
        logging.debug('Creating users')
        create_users(ADDITIONAL_USERS)

        logging.debug('Creating Counters')
        orders_models.Counter.objects.create(name='Order Code', value=str(timezone.now().year) + '-0000000')
        orders_models.Counter.objects.create(name='Accession Number', int_value=0)

        # make test types
        logging.debug('Initializing test types')
        t0 = timeit.default_timer()
        import_test_type_from_csv_url(TEST_TYPE_CSV)
        t1 = timeit.default_timer()
        logging.debug('done - {} seconds'.format(round(t1 - t0, 3)))


if __name__ == '__main__':
    logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)
    schema_name = main()
