import django_filters

from . import models

class OrderFilter(django_filters.FilterSet):
    code = django_filters.CharFilter(lookup_expr='icontains')

    class Meta:
        models = models.Order
        fields = ['code']