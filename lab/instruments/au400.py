import decimal
import time

import channels.layers
import django
import logging
import os
import platform
import datetime
import sys
import serial
import json
import pathlib
import itertools

from asgiref.sync import async_to_sync
from serial.tools import list_ports

from django.db import transaction
from tenant_schemas.utils import tenant_context
from django.utils import timezone
from django.core.exceptions import ObjectDoesNotExist

os.environ['DJANGO_SETTINGS_MODULE'] = 'main.settings'
django.setup()

from orders import models as orders_models
from clients import models as clients_models
from lab_tenant import models as lab_tenant_models

from main.functions import convert_utc_to_timezone


########################################################################################################################

PLATFORM_NAME = platform.system()
HOME = str(pathlib.Path.home())
INSTRUMENT_NAME = 'Olympus AU400'

########################################################################################################################

TEXT_START_BLOCK = ['\x02']
TEXT_END_BLOCK = ['\x03']
BLOCK_END_BLOCK = ['E']
DUMMY_BLOCK = [' '] * 4

# Requisition inquiry is what the AU sends the LIS
# Requisition is what the the LIS sends back
REQUISITION_INQUIRY_START_BLOCK = ['R', 'B']
REQUISITION_INQUIRY_END_BLOCK = ['R', 'E']

REQUISITION_START_BLOCK = ['S', ' ']
REQUISITION_END_BLOCK = ['\x02', 'S', 'E', '\x03']

RESULTS_START_BLOCK = ['D', 'B']
RESULTS_END_BLOCK = ['D', 'E']

########################################################################################################################


def chunks(l, n):
    """Yield successive n-sized chunks from l."""
    for i in range(0, len(l), n):
        yield l[i:i + n]


def data_str_to_json(instrument, data_str_list, barcode_length):
    """
    Rack number --> data_str_list[2:6]
    Cup position --> data_str_list[6:8]
    Sample type --> data_str_list[8]
    Sample number (barcode) --> data_str_list[9:13]
    """
    # dummy block used for some reason
    dummy_block_length = 4
    end_of_sample_id_index = 13

    sample_number = ''.join(data_str_list[9:end_of_sample_id_index])
    end_of_barcode_index = end_of_sample_id_index + dummy_block_length + barcode_length

    # parse into barcode even if empty
    barcode = data_str_list[end_of_sample_id_index:end_of_sample_id_index + barcode_length]
    barcode_str = ''.join(barcode)

    ####################################################################################################################
    # use for testing
    # use a series of barcodes for testing
    # barcode_str = '3333333334'
    ####################################################################################################################

    data_dict = {}
    data_dict['sample_number'] = sample_number
    data_dict['sample_barcode'] = barcode_str
    # multiple tests can be done on a sample
    data_dict['tests'] = []

    # chunk barcodes by their length
    test_code_and_results_chunked = list(chunks(data_str_list[end_of_barcode_index+1:], barcode_length))

    for test in test_code_and_results_chunked:
        test_code = ''.join(test[:2])

        try:
            # map test codes using orders_models.InstrumentTestCode
            test_code = orders_models.InstrumentTestCode.objects.get(test_code=test_code).test_code
        except Exception as e:
            error_text = 'Test code ({}) is not mapped to a test in Dendi LIS.'.format(test_code)
            print(error_text)
            orders_models.InstrumentError.objects.create(instrument_integration=instrument,
                                                         error_text=error_text)
            continue
        try:
            test_result = float(''.join(test[2:8]))
        except ValueError as e:
            test_result = 0
            error_text = 'Invalid result value for test code: {}'.format(test_code)
            print(error_text)
            orders_models.InstrumentError.objects.create(instrument_integration=instrument,
                                                         error_text=error_text)

        data_dict['tests'].append({'test_code': test_code,
                                   'test_result': test_result})
    return data_dict


def save_data_to_lis(instrument, data_dict):
    """
    Save JSON data to LIS - create a Result object
    """
    barcode = data_dict['sample_barcode']
    tests = data_dict['tests']

    # sample must be accessioned first in order to have a barcode...is that correct?
    # tenant.timezone
    sample = orders_models.Sample.objects.filter(barcode=barcode).first()

    if sample:
        # get all tests for the sample
        for test_data in tests:
            test_code = test_data['test_code']
            test_code_obj = orders_models.InstrumentTestCode.objects.filter(test_code=test_code).first()
            test_type = test_code_obj.test_type

            try:
                # get related test object - each test is only done on one sample
                test = orders_models.Test.objects.get(sample=sample, test_type=test_type)
            except Exception as e:
                error_text = 'Test: {} was not ordered for this sample.'.format(test_type.name)
                print(error_text)
                orders_models.InstrumentError.objects.create(instrument_integration=instrument,
                                                             error_text=error_text)
                test = None

            if test:
                # using timezone.now() defaults to UTC, since our settings is using UTC
                # be careful --> Test/Result is a one-to-one relationship (although it's not reflected in db yet)
                # when a Test object is created, a Result object is also created
                # Result.result is a "qualitative" field, like "+", "-"
                # compare result_quantitative to reference ranges and save result.result
                result_quantitative = test_data['test_result']
                result_obj = orders_models.Result.objects.get(test=test)

                if test.test_type.test_method.is_within_range:

                    if result_quantitative:
                        result_quantitative = decimal.Decimal(result_quantitative).quantize(decimal.Decimal('.0001'),
                                                                                            rounding=decimal.ROUND_DOWN)
                        if result_quantitative >= decimal.Decimal('999999.9999'):
                            result_quantitative = decimal.Decimal('999999.9999')
                    try:
                        low_value = decimal.Decimal(
                            result_obj.test.test_type.test_type_ranges.get().range_low)
                        high_value = decimal.Decimal(
                            result_obj.test.test_type.test_type_ranges.get().range_high)
                    except orders_models.TestTypeRange.DoesNotExist:
                        low_value = None  # if cutoff is not provided
                        high_value = None
                    if low_value and high_value and result_quantitative:
                        if result_quantitative >= low_value and result_quantitative <= high_value:
                            result = '~'
                        elif result_quantitative < low_value:
                            result = '<'
                        else:
                            result = '>'
                    else:
                        result = ''

                else:
                    try:
                        result_quantitative = decimal.Decimal(result_quantitative).quantize(
                            decimal.Decimal('.0001'), rounding=decimal.ROUND_DOWN)
                        if result_quantitative >= decimal.Decimal('999999.9999'):
                            result_quantitative = decimal.Decimal('999999.9999')
                    except TypeError:
                        result_quantitative = None
                    positive_above_cutoff = result_obj.test.test_type.test_method.positive_above_cutoff
                    try:
                        if positive_above_cutoff:  # fault tolerance for cutoff not existing
                            cutoff_value = decimal.Decimal(
                                result_obj.test.test_type.test_type_ranges.filter(default=True).get().range_low)
                        else:
                            cutoff_value = decimal.Decimal(
                                result_obj.test.test_type.test_type_ranges.filter(default=True).get().range_high)
                    except orders_models.TestTypeRange.DoesNotExist:
                        cutoff_value = None  # if cutoff is not provided

                    if cutoff_value and result_quantitative:
                        if positive_above_cutoff:
                            if result_quantitative >= cutoff_value:
                                result = '+'
                            else:
                                result = '-'
                        else:  # negative above cutoff
                            if result_quantitative >= cutoff_value:
                                result = '-'
                            else:
                                result = '+'
                    else:
                        result = ''

                result_obj.result_quantitative = result_quantitative
                result_obj.result = result
                result_obj.save()
                print(result_obj)
                print('Saving data...')


def requisition_mapper(instrument, req_char_list, barcode_length):
    """
    Parse requisition data and return associated test codes in string blocks
    """
    rack_number = req_char_list[2:6]
    cup_position = req_char_list[6:8]
    sample_type = list(req_char_list[8])
    sample_number = req_char_list[9:13]

    # parse into barcode even if empty
    barcode = req_char_list[13:13+barcode_length]


    ####################################################################################################################
    # For testing - you don't want to actually change the barcode value
    # if no barcode is provided, use a barcode with 0s
    # for index, char in enumerate(barcode):
    #     if char == ' ':
    #         barcode[index] = '0'
    ####################################################################################################################
    # get sample
    requisition_dict = {}
    requisition_dict['rack_number'] = rack_number
    requisition_dict['cup_position'] = cup_position
    requisition_dict['sample_number_block'] = sample_number
    requisition_dict['sample_type_block'] = sample_type
    requisition_dict['barcode_block'] = barcode

    barcode_str = ''.join(barcode)
    # get sample and related tests and test types
    ####################################################################################################################
    # use barcode_str
    # sample = orders_models.Sample.objects.filter(barcode='3333333334').first()
    sample = orders_models.Sample.objects.filter(barcode=barcode_str).first()
    ####################################################################################################################

    test_code_list = []
    if sample:
        # get all tests for the sample
        tests = sample.tests.all()
        for test in tests:
            test_codes = orders_models.InstrumentTestCode.objects.filter(instrument_integration=instrument,
                                                                         test_type=test.test_type)\
                .values_list('test_code', flat=True)
            # if a test code isn't mapped, then what happens???
            if test_codes:
                for test_code in test_codes:
                    test_code = list(test_code)
                    test_code_list.append(test_code)

        # flatten the list so that it's not a list of lists
        test_code_list = [item for sublist in test_code_list for item in sublist]
        requisition_dict['test_codes_block'] = test_code_list
    else:
        # if a barcode can't be found, what happens?
        print('No sample matching barcode.')
    return requisition_dict


def au400_integration(tenant, instrument, platform_name, barcode_length):
    """
    You have to receive an inquiry for the test requisition from AU400 first before actually sending the
    test requisition - it's not a one-way street, like receiving test results.

    Requisitions can be performed in the AU400 itself, or downloaded from a
    host computer (ours) to the AU400 automatically. Requisitions are processed sequentially by sample
    number (barcode reader off) or by the sample ID number (barcode reader on) or by the rack
    ID number.

    A sample number in the AU400 documentation should always start with 0001 and then increment from there.
    Each sample number is associated with a barcode, which our LIS generates. However, we want to use barcodes,
    since we don't want to be limited to a 4-digit sample number, and we generate barcodes for each sample anyway.

    Barcodes are alphanumeric and is set to 10 characters long.
    Barcode is known as "sample_id" in the AU documentation.
    The barcode length is set in "Parameter/Format/Requisition Format" as well as "Parameter/System/System"

    """
    # need tenant context
    if platform_name == 'Darwin':
        port = '/dev/cu.usbserial'
    elif platform_name == 'Linux':
        port = '/dev/ttyUSB0'
    else:
        port = '/dev/ttyUSB0'

    ser = serial.Serial(
        port=port,
        baudrate=9600,
        parity=serial.PARITY_NONE,
        stopbits=serial.STOPBITS_ONE,
        bytesize=serial.EIGHTBITS,
        timeout=0.5,
        write_timeout=50)

    print("connected to: " + ser.portstr)

    ###############################################################################################################
    char_list = []
    data_dict_list = []
    result_begin_status = False
    req_begin_status = False
    req_list = []

    while True:
        # send status message
        async_to_sync(channel_layer.group_send)(str(instrument.uuid), {
            'type': 'status_update',
            'message': '<b class=text-success>Active</b><br><i>Last Seen: {}</i>'.format(convert_utc_to_timezone(tenant.timezone, timezone.now()).strftime("%m/%d, %H:%M:%S"))
        })

        for line in ser.read(6):
            # NOTE: \x02 and \x03 are not printed using Python for some reason
            # however, you can see it when printing using print(chr(line)) using within pdb
            if chr(line) == '\x02':
                continue
            elif chr(line) == '\x03':
                # send ACK to AU400 after every block --> ACK = \x06
                # NAK = \x15
                print('Sending ACK to AU.')
                time.sleep(2)
                ser.write(str.encode('\x06'))

                joined_char_list = ''.join(char_list)
                print(joined_char_list)

                if joined_char_list == 'RB':
                    # every requisition begins with string 'RB'
                    print('Requisition data transfer start.')
                    # every transfer begins with 'RB' in the beginning
                    req_begin_status = True
                elif joined_char_list == 'RE':
                    # every requisition ends with string 'RE'
                    if req_begin_status:
                        print('Requisition data transfer end.')
                    else:
                        print('Requisition data transfer incomplete. '
                              'This program must run before requisition transfer begins. '
                              'Restart data transfer.')
                    req_begin_status = False

                elif joined_char_list == 'DB':
                    # every results transfer begins with string 'DB'
                    print('Data transfer start.')
                    result_begin_status = True
                elif joined_char_list == 'DE':
                    # every results transfer ends with string 'DE'
                    if result_begin_status:
                        print('Data transfer end.')
                        pathlib.Path(output_pathfile).mkdir(parents=True, exist_ok=True)
                        json_filename = '{}au400_data_{}.json'.format(output_pathfile, datetime.datetime.now()
                                                                      .strftime('%Y-%m-%d_%H%M%S'))
                        with open(json_filename, 'w', encoding='utf-8') as f:
                            json.dump(data_dict_list, f, ensure_ascii=False, indent=4)
                        # empty list after file is created
                        data_dict_list = []
                    else:
                        print('Transfer incomplete. This program must run before data transfer begins. '
                              'Restart data transfer.')
                    result_begin_status = False

                else:

                    if req_begin_status and joined_char_list.startswith('R'):
                        # requisition for a sample
                        # if requisition has already been sent to the AU, then just skip this one
                        if joined_char_list not in req_list:
                            # receive ACK to AU400 after every block --> ACK = \x06
                            ############################################################################################
                            # TIMEOUT REQUIRED
                            time.sleep(2)
                            ############################################################################################
                            requisition_dict = requisition_mapper(instrument, char_list, barcode_length)
                            rack_number_block = requisition_dict['rack_number']
                            cup_position_block = requisition_dict['cup_position']
                            sample_type_block = requisition_dict['sample_type_block']
                            sample_number_block = requisition_dict['sample_number_block']
                            barcode_block = requisition_dict['barcode_block']
                            test_codes_block = requisition_dict['test_codes_block']

                            # requisition block
                            requisition = TEXT_START_BLOCK + REQUISITION_START_BLOCK + rack_number_block + \
                                          cup_position_block + sample_type_block + \
                                          sample_number_block + barcode_block + DUMMY_BLOCK + BLOCK_END_BLOCK + \
                                          test_codes_block + TEXT_END_BLOCK + REQUISITION_END_BLOCK

                            print('Sending requisition data...')
                            print(requisition)
                            async_to_sync(channel_layer.group_send)(str(instrument.uuid), {
                                'type': 'status_update',
                                'message': '<b class=text-success>Active</b><br>Sending requisition data...'
                            })
                            # send test codes back to AU
                            for char in requisition:
                                ser.write(str.encode(char))

                            req_list.append(joined_char_list)
                            print(req_list)

                    if result_begin_status and joined_char_list.startswith('D'):
                        # results being transferred
                        print('Receiving data...')
                        async_to_sync(channel_layer.group_send)(str(instrument.uuid), {
                            'type': 'status_update',
                            'message': '<b class=text-success>Active</b><br>Receiving data...'
                        })
                        data_dict = data_str_to_json(instrument, char_list, barcode_length)
                        data_dict_list.append(data_dict)
                        # save data_dict to LIS
                        async_to_sync(channel_layer.group_send)(str(instrument.uuid), {
                            'type': 'status_update',
                            'message': '<b class=text-success>Active</b><br>Saving test results...'
                        })
                        save_data_to_lis(instrument, data_dict)
                        print(data_dict)

                # reset character list
                char_list = []

            elif chr(line) == '\x06':
                print('Requisition data successfully received by AU (ACK).')
            elif chr(line) == '\x15':
                error_text = 'Requisition data transfer error (NAK).'
                print(error_text)
                orders_models.InstrumentError.objects.create(instrument_integration=instrument,
                                                             error_text=error_text)
            else:
                char_list.append(chr(line))


if __name__ == '__main__':
    logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)
    output_pathfile = '{}/au400/input/'.format(HOME)
    channel_layer = channels.layers.get_channel_layer()

    try:
        tenant_name = os.environ.get('DEFAULT_TENANT_NAME')
        tenant = clients_models.Client.objects.get(schema_name=tenant_name)
        with tenant_context(tenant):
            instrument = lab_tenant_models.InstrumentIntegration.objects.get(internal_instrument_name=INSTRUMENT_NAME)
            au400_integration(tenant, instrument, PLATFORM_NAME, 10)
    except IndexError:
        print('Please specify a lab tenant name.')
    except ObjectDoesNotExist:
        tenant_name = os.environ.get('DEFAULT_TENANT_NAME')
        print('Tenant: {} does not exist.'.format(tenant_name))
    except serial.serialutil.SerialException:
        tenant_name = os.environ.get('DEFAULT_TENANT_NAME')
        tenant = clients_models.Client.objects.get(schema_name=tenant_name)
        with tenant_context(tenant):
            instrument = lab_tenant_models.InstrumentIntegration.objects.get(internal_instrument_name=INSTRUMENT_NAME)
            async_to_sync(channel_layer.group_send)(str(instrument.uuid), {
                'type': 'status_update',
                'message': 'Inactive - Disconnected'
            })
            error_text = 'The interface is not connected to the instrument or is offline.'
            print(error_text)
            orders_models.InstrumentError.objects.create(instrument_integration=instrument,
                                                         error_text=error_text)

