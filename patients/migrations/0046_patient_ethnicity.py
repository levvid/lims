# Generated by Django 2.2.2 on 2020-02-24 23:32

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('patients', '0045_auto_20200127_2136'),
    ]

    operations = [
        migrations.AddField(
            model_name='patient',
            name='ethnicity',
            field=models.CharField(blank=True, choices=[('W', 'White'), ('B', 'Black or African-American'), ('A', 'American Indian or Alaska Native'), ('S', 'Asian'), ('P', 'Native Hawaiian or Other Pacific Islander'), ('H', 'Hispanic or Latino'), ('O', 'Other'), ('U', 'Unknown')], max_length=3),
        ),
    ]
