/*
    Source: https://github.com/szimek/signature_pad
*/

var savePNGButton = document.getElementById("add-signature");

var canvas = document.getElementById("signature-pad");
var undoButton = document.getElementById("undo-signature");
var clearButton = document.getElementById("clear-signature");
var signaturePad = new SignaturePad(canvas, {
  // It's Necessary to use an opaque color when saving image as JPEG;
  // this option can be omitted if only saving as PNG or SVG
  //  backgroundColor: 'rgb(255, 255, 255)'
  minWidth: 1.5,
  maxWidth: 5
});

// Adjust canvas coordinate space taking into account pixel ratio,
// to make it look crisp on mobile devices.
// This also causes canvas to be cleared.
function resizeCanvas() {
  // When zoomed out to less than 100%, for some very strange reason,
  // some browsers report devicePixelRatio as less than 1
  // and only part of the canvas is cleared then.

  // This part causes the canvas to be cleared
//  canvas.width = canvas.offsetWidth * ratio;
//  canvas.height = canvas.offsetHeight * ratio;
//  canvas.getContext("2d").scale(ratio, ratio);
  canvas.width = $("#signature-pad-form").width()*.95; // * .95 to give canvas padding
  // This library does not listen for canvas changes, so after the canvas is automatically
  // cleared by the browser, SignaturePad#isEmpty might still return false, even though the
  // canvas looks empty, because the internal data of this library wasn't cleared. To make sure
  // that the state of this library is consistent with visual state of the canvas, you
  // have to clear it manually.
  signaturePad.clear();
}

// On mobile devices it might make more sense to listen to orientation change,
// rather than window resize events.
 window.onresize = resizeCanvas;
 resizeCanvas();

function download(dataURL, filename) {
  var blob = dataURLToBlob(dataURL);
  var url = window.URL.createObjectURL(blob);

  var a = document.createElement("a");
  a.style = "display: none";
  a.href = url;
  a.download = filename;

  document.body.appendChild(a);
  a.click();

  window.URL.revokeObjectURL(url);
}

// One could simply use Canvas#toBlob method instead, but it's just to show
// that it can be done using result of SignaturePad#toDataURL.
function dataURLToBlob(dataURL) {
  // Code taken from https://github.com/ebidel/filer.js
  var parts = dataURL.split(';base64,');
  var contentType = parts[0].split(":")[1];
  var raw = window.atob(parts[1]);
  var rawLength = raw.length;
  var uInt8Array = new Uint8Array(rawLength);

  for (var i = 0; i < rawLength; ++i) {
    uInt8Array[i] = raw.charCodeAt(i);
  }

  return new Blob([uInt8Array], { type: contentType });
}

 clearButton.addEventListener("click", function (event) {
   signaturePad.clear();
 });

 undoButton.addEventListener("click", function (event) {
   var data = signaturePad.toData();

   if (data) {
     data.pop(); // remove the last dot or line
     signaturePad.fromData(data);
   }
 });

// changeColocanvasrButton.addEventListener("click", function (event) {
//   var r = Math.round(Math.random() * 255);
//   var g = Math.round(Math.random() * 255);
//   var b = Math.round(Math.random() * 255);
//   var color = "rgb(" + r + "," + g + "," + b +")";
//
//   signaturePad.penColor = color;
// });
function dataURLtoFile(dataurl, filename) {
    var arr = dataurl.split(','), mime = arr[0].match(/:(.*?);/)[1],
        bstr = atob(arr[1]), n = bstr.length, u8arr = new Uint8Array(n);
    while(n--){
        u8arr[n] = bstr.charCodeAt(n);
    }
    return new File([u8arr], filename, {type:mime});
}


savePNGButton.addEventListener("click", function (event) {
  if (signaturePad.isEmpty()) {
    alert("Please provide a signature first.");
    return False;
  } else {
    var dataURL = signaturePad.toDataURL();
    var text_box = document.getElementById("canvasData");
    var formButton = document.getElementById("call_form");
    text_box.value = dataURL;
    formButton.click();
  }
});

$('input[name="signature-option"]').click(function(){
    // check for existing or new primary payer
    if($("input[name='signature-option']:checked").val() === 'Draw Signature') {
      $('.upload-signature').hide();
      $('.signature-pad').show();
    } else {
      $('.upload-signature').show();
      $('.signature-pad').hide();
    }
});



