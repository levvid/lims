from braces.views import LoginRequiredMixin
from django_tables2 import RequestConfig
from django.contrib import messages
from django.db import transaction
from django.forms import formset_factory
from django.http import HttpResponseRedirect
from django.shortcuts import render, redirect, get_object_or_404
from django.views.generic import View
from django.urls import reverse
from django.utils import timezone
from django.core.exceptions import ObjectDoesNotExist

import decimal

from orders import forms
from orders import models

from lab_tenant.views import LabMemberMixin, LISLoginRequiredMixin
from lab import models as lab_models
from orders import functions


class TestTypeListView(LISLoginRequiredMixin, View):
    def get(self, request):
        return render(request, 'test_type_list.html', {})


class TestTypeDetailsView(LISLoginRequiredMixin, View):
    def get(self, request, test_type_uuid):
        test_type = get_object_or_404(models.TestType, uuid=test_type_uuid)
        test_type_ranges = models.TestTypeRange.objects.filter(test_type=test_type).order_by('-sex', 'min_age_total')
        test_type_reflexes = models.TestTypeReflex.objects.filter(test_type=test_type)
        icd10_codes = test_type.icd10_codes.all()
        test_type_categories = test_type.test_type_categories.all()

        required_samples = []
        if test_type.required_samples:
            for required_sample_type_id in test_type.required_samples:
                try:
                    required_sample_type_id = int(required_sample_type_id)
                    sample_type = get_object_or_404(lab_models.CLIASampleType, id=required_sample_type_id)
                    required_samples.append({'sample_type': sample_type,
                                             'num_samples_required': test_type.required_samples[
                                                 str(required_sample_type_id)]})
                except ValueError:
                    pass
        return render(request, 'test_type_details.html', {'test_type': test_type,
                                                          'test_type_ranges': test_type_ranges,
                                                          'test_type_reflexes': test_type_reflexes,
                                                          'required_samples': required_samples,
                                                          'icd10_codes': icd10_codes,
                                                          'test_type_categories': test_type_categories})


class TestTypeUpdateView(LabMemberMixin, View):
    def get(self, request, test_type_uuid):
        test_type = get_object_or_404(models.TestType, uuid=test_type_uuid)
        # initial form values set using initial dict
        # required clia sample counts
        initial_data = []
        labels = []
        sample_types = lab_models.CLIASampleType.objects.all()
        TestTypeSampleFormSet = formset_factory(forms.TestTypeCLIASampleForm, formset=forms.BaseFormSet, extra=0)
        for sample_type in sample_types:
            if test_type.required_samples:
                initial_data.append({'sample_type': sample_type,
                                     'num_samples': test_type.required_samples.get(str(sample_type.id), 0)})
            else:
                initial_data.append({'sample_type': sample_type,
                                     'num_samples': 0})
            labels.append(sample_type.name)

        sample_formset = TestTypeSampleFormSet(prefix='sample_type', initial=initial_data)

        if test_type.min_volume == decimal.Decimal('0.00'):
            # cleaning to show 0 instead of 0E-9
            min_volume = decimal.Decimal('0.00')
        else:
            min_volume = test_type.min_volume

        test_type_alias = test_type.test_type_aliases.all()
        test_type_alias = [str(alias) for alias in test_type_alias]
        alias_str = '\r\n'.join(test_type_alias)

        form = forms.TestTypeForm(initial={'test_method': test_type.test_method,
                                           'test_target': test_type.test_target,
                                           'test_type_categories': test_type.test_type_categories.all(),
                                           'isotype': test_type.isotype,
                                           'band': test_type.band,
                                           'turnaround_time': test_type.turnaround_time,
                                           'turnaround_time_unit': test_type.turnaround_time_unit,
                                           'name': test_type.name,
                                           'aliases': alias_str,
                                           'is_research_only': test_type.is_research_only,
                                           'unit': test_type.unit,
                                           'use_round_figures': test_type.use_round_figures,
                                           'round_figures': test_type.round_figures,
                                           'manufacturer': test_type.manufacturer,
                                           'specialty': test_type.specialty,
                                           'offsite_laboratory': test_type.offsite_laboratory,
                                           'offsite_test_id': test_type.offsite_test_id,
                                           'result_type': test_type.result_type,
                                           'icd10_codes': test_type.icd10_codes.all(),
                                           'is_approval_required': test_type.is_approval_required,
                                           'min_volume': min_volume,
                                           'container_type': test_type.container_type,
                                           'is_internal': test_type.is_internal,
                                           'loinc_code': test_type.loinc_code},
                                  instance_id=test_type.id)

        return render(request, 'test_type_create.html', {'form': form,
                                                         'sample_formset_and_labels': zip(sample_formset,
                                                                                          labels),
                                                         'sample_formset': sample_formset,
                                                         'update': True, })

    def post(self, request, test_type_uuid):
        test_type = get_object_or_404(models.TestType, uuid=test_type_uuid)
        form = forms.TestTypeForm(request.POST, instance_id=test_type.id)

        TestTypeSampleFormSet = formset_factory(forms.TestTypeCLIASampleForm, formset=forms.BaseFormSet)
        sample_formset = TestTypeSampleFormSet(request.POST, prefix='sample_type')
        sample_type_labels = []
        sample_types = lab_models.CLIASampleType.objects.all().order_by('name')
        for sample_type in sample_types:
            sample_type_labels.append(sample_type.name)

        context = {'form': form,
                   'sample_formset_and_labels': zip(sample_formset,
                                                    sample_type_labels),
                   'sample_formset': sample_formset,
                   'update': True}

        if form.is_valid() and sample_formset.is_valid():
            test_type.name = form.cleaned_data['name']
            aliases = form.cleaned_data.get('aliases')
            test_type.test_method = form.cleaned_data['test_method']
            test_type.test_target = form.cleaned_data['test_target']
            test_type.test_type_categories.set(form.cleaned_data['test_type_categories'])
            test_type.isotype = form.cleaned_data['isotype']
            test_type.band = form.cleaned_data['band']
            test_type.turnaround_time = form.cleaned_data['turnaround_time']
            test_type.turnaround_time_unit = form.cleaned_data['turnaround_time_unit']
            test_type.is_research_only = form.cleaned_data['is_research_only']
            test_type.unit = form.cleaned_data['unit']
            test_type.use_round_figures = form.cleaned_data['use_round_figures']
            if test_type.use_round_figures:
                test_type.round_figures = form.cleaned_data['round_figures']
            test_type.manufacturer = form.cleaned_data['manufacturer']
            test_type.specialty = form.cleaned_data['specialty']
            test_type.result_type = form.cleaned_data['result_type']
            test_type.icd10_codes.set(form.cleaned_data['icd10_codes'])
            test_type.offsite_laboratory = form.cleaned_data['offsite_laboratory']
            test_type.offsite_test_id = form.cleaned_data['offsite_test_id']
            test_type.is_approval_required = form.cleaned_data['is_approval_required']
            test_type.min_volume = form.cleaned_data['min_volume']
            test_type.container_type = form.cleaned_data['container_type']
            test_type.is_internal = form.cleaned_data['is_internal']
            test_type.loinc_code = form.cleaned_data['loinc_code']

            # save required samples as json field
            required_samples = {}
            required_sample_count = 0
            for sample_form in sample_formset:
                if sample_form.is_valid():
                    sample_type = sample_form.cleaned_data.get('sample_type')
                    num_samples = sample_form.cleaned_data.get('num_samples')
                    if num_samples and num_samples > 0:
                        required_samples[sample_type.id] = num_samples
                        required_sample_count += num_samples
            test_type.required_samples = required_samples

            if required_sample_count == 0:
                messages.error(request, "Please specify required samples for this test type.")
                return render(request, "test_type_create.html", context)

            # save object after update
            test_type.save()

            # make sure alias list gets updated every time
            test_type_aliases = models.TestTypeAlias.objects.filter(test_type=test_type)
            for test_alias in test_type_aliases:
                test_alias.is_active = False
                test_alias.save()
            for alias in aliases:
                alias = alias.strip()
                if alias:
                    alias = models.TestTypeAlias(name=alias, test_type=test_type)
                    alias.save()

            return redirect('test_type_details', test_type_uuid=test_type.uuid)
        return render(request, 'test_type_create.html', context)


class TestTypeDeleteView(LabMemberMixin, View):
    # request object is by default required even if not used
    def get(self, request, test_type_uuid):
        test_type = get_object_or_404(models.TestType, uuid=test_type_uuid)
        test_type.is_active = False
        test_type.save()
        return redirect('test_type_list')


class TestTypeCategoryListView(LabMemberMixin, View):
    def get(self, request):
        return render(request, 'test_type_category_list.html', {})


class TestTypeCategoryDetailsView(LabMemberMixin, View):
    def get(self, request, test_type_category_uuid):
        test_type_category = get_object_or_404(models.TestTypeCategory, uuid=test_type_category_uuid)
        tests = test_type_category.test_types.filter()
        return render(request, 'test_type_category_details.html', {'test_type_category': test_type_category,
                                                                   'tests': tests})


class TestTypeCategoryCreateView(LabMemberMixin, View):
    def get(self, request):
        form = forms.TestTypeCategoryForm()
        tests = models.TestType.objects.filter()
        return render(request, 'test_type_category_create.html', {'form': form,
                                                                  'tests': tests})

    @transaction.atomic
    def post(self, request):
        form = forms.TestTypeCategoryForm(request.POST)
        tests = models.TestType.objects.filter()
        tests_selected = request.POST.getlist('tests_selected')

        if form.is_valid():
            name = form.cleaned_data.get('name')
            test_types = models.TestType.objects.filter(pk__in=tests_selected)

            test_type_category = models.TestTypeCategory(name=name)
            test_type_category.save()

            # add many-to-many fields
            test_type_category.test_types.set(test_types)

            return redirect('test_type_category_list')

        return render(request, 'test_type_category_list.html', {'form': form,
                                                                'tests': tests})


class TestTypeCategoryUpdateView(LabMemberMixin, View):
    def get(self, request, test_type_category_uuid):
        test_type_category = get_object_or_404(models.TestTypeCategory, uuid=test_type_category_uuid)
        form = forms.TestTypeCategoryForm(initial={'name': test_type_category.name})
        tests = models.TestType.objects.filter()
        tests_selected = test_type_category.test_types.filter()

        for test in tests:
            if test in tests_selected:
                test.selected = True
        return render(request, 'test_type_category_create.html', {'form': form,
                                                                  'test_type_category': test_type_category,
                                                                  'tests': tests,
                                                                  'update': True})

    @transaction.atomic
    def post(self, request, test_type_category_uuid):
        test_type_category = get_object_or_404(models.TestTypeCategory, uuid=test_type_category_uuid)
        form = forms.TestTypeCategoryForm(request.POST)
        tests = models.TestType.objects.filter()
        tests_selected = request.POST.getlist('tests_selected')

        if form.is_valid():
            name = form.cleaned_data.get('name')
            test_types = models.TestType.objects.filter(pk__in=tests_selected)

            test_type_category.name = name
            test_type_category.save()

            # add many-to-many fields
            test_type_category.test_types.set(test_types)

            return redirect('test_type_category_list')

        return render(request, 'test_type_category_list.html', {'form': form,
                                                                'test_type_category': test_type_category,
                                                                'tests': tests,
                                                                'update': True})


class TestTypeCreateView(LabMemberMixin, View):
    def get(self, request):
        form = forms.TestTypeForm(instance_id=None)
        sample_types = lab_models.CLIASampleType.objects.all().order_by('name')
        TestTypeSampleFormSet = formset_factory(forms.TestTypeCLIASampleForm, formset=forms.BaseFormSet, extra=0)
        sample_types_initial_data = []
        sample_type_labels = []
        for sample_type in sample_types:
            sample_types_initial_data += [{'sample_type': sample_type, 'num_samples': 0}]
            sample_type_labels.append(sample_type.name)
        sample_formset = TestTypeSampleFormSet(prefix='sample_type',
                                               initial=sample_types_initial_data)

        return render(request, 'test_type_create.html', {'form': form,
                                                         'sample_formset_and_labels': zip(sample_formset,
                                                                                          sample_type_labels),
                                                         'sample_formset': sample_formset})

    @transaction.atomic
    def post(self, request):
        # Primary key sequence was thrown off during creation
        # if that happens again: https://stackoverflow.com/a/3698777/3307664
        form = forms.TestTypeForm(request.POST, instance_id=None)

        TestTypeSampleformSet = formset_factory(forms.TestTypeCLIASampleForm, formset=forms.BaseFormSet)
        sample_formset = TestTypeSampleformSet(request.POST, prefix='sample_type')
        sample_type_labels = []
        sample_types = lab_models.CLIASampleType.objects.all().order_by('name')
        for sample_type in sample_types:
            sample_type_labels.append(sample_type.name)

        context = {'form': form,
                   'sample_formset_and_labels': zip(sample_formset,
                                                    sample_type_labels),
                   'sample_formset': sample_formset}

        if form.is_valid() and sample_formset.is_valid():
            name = form.cleaned_data['name']
            aliases = form.cleaned_data.get('aliases')
            test_method = form.cleaned_data['test_method']
            test_target = form.cleaned_data['test_target']
            test_type_categories = form.cleaned_data['test_type_categories']
            isotype = form.cleaned_data['isotype']
            band = form.cleaned_data['band']
            turnaround_time = form.cleaned_data['turnaround_time']
            turnaround_time_unit = form.cleaned_data['turnaround_time_unit']
            is_research_only = form.cleaned_data['is_research_only']
            unit = form.cleaned_data['unit']
            use_round_figures = form.cleaned_data.get('use_round_figures')
            if use_round_figures:
                round_figures = form.cleaned_data.get('round_figures')
            else:
                # default value
                round_figures = 2
            manufacturer = form.cleaned_data['manufacturer']
            result_type = form.cleaned_data['result_type']
            icd10_codes = form.cleaned_data['icd10_codes']
            offsite_laboratory = form.cleaned_data['offsite_laboratory']
            offsite_test_id = form.cleaned_data['offsite_test_id']
            is_approval_required = form.cleaned_data['is_approval_required']
            specialty = form.cleaned_data['specialty']
            min_volume = form.cleaned_data['min_volume']
            container_type = form.cleaned_data['container_type']
            is_internal = form.cleaned_data['is_internal']
            loinc_code = form.cleaned_data['loinc_code']

            required_samples = {}
            required_sample_count = 0
            for sample_form in sample_formset:
                if sample_form.is_valid():
                    sample_type = sample_form.cleaned_data.get('sample_type')
                    num_samples = sample_form.cleaned_data.get('num_samples')
                    if num_samples and num_samples > 0:
                        required_samples[sample_type.id] = num_samples
                        required_sample_count += num_samples

            if required_sample_count == 0:
                messages.error(request, "Please specify required samples for this test type.")
                return render(request, "test_type_create.html", context)

            test_type_obj = models.TestType(name=name,
                                            test_method=test_method,
                                            test_target=test_target,
                                            specialty=specialty,
                                            isotype=isotype,
                                            band=band,
                                            turnaround_time=turnaround_time,
                                            turnaround_time_unit=turnaround_time_unit,
                                            required_samples=required_samples,
                                            is_research_only=is_research_only,
                                            unit=unit,
                                            use_round_figures=use_round_figures,
                                            round_figures=round_figures,
                                            manufacturer=manufacturer,
                                            result_type=result_type,
                                            offsite_laboratory=offsite_laboratory,
                                            offsite_test_id=offsite_test_id,
                                            is_approval_required=is_approval_required,
                                            min_volume=min_volume,
                                            container_type=container_type,
                                            is_internal=is_internal,
                                            loinc_code=loinc_code)

            test_type_obj.save()

            if icd10_codes:
                test_type_obj.icd10_codes.set(icd10_codes)

            if test_type_categories:
                test_type_obj.test_type_categories.set(test_type_categories)

            alias_list = []
            for alias in aliases:
                alias = alias.strip()
                if alias:
                    alias_list.append(models.TestTypeAlias(name=alias, test_type=test_type_obj))
            models.TestTypeAlias.objects.bulk_create(alias_list)

            # if result type is quantitative, then redirect to create a reference ranges
            # if result type is qualitative, reference ranges don't exist. so redirect to reflex testing logic
            if result_type == 'Quantitative':
                return redirect('test_type_range_create', test_type_uuid=test_type_obj.uuid)
            else:
                # update later
                return redirect('test_type_details', test_type_uuid=test_type_obj.uuid)
        return render(request, 'test_type_create.html', context)


class TestTypeRangeCreateView(LabMemberMixin, View):

    def get(self, request, test_type_uuid):
        reference_ranges = models.TestTypeRange.objects.filter(test_type__uuid=test_type_uuid)\
            .order_by('-sex', 'min_age_total')
        test_type = models.TestType.objects.get(uuid=test_type_uuid)
        form = forms.TestTypeRangeForm(instance=None, test_type=test_type)

        # check for a "default" reference range --> if one doesn't exist, then limit the first form so that the
        # first reference range object saved is default
        default_reference_exists = reference_ranges.filter(default=True).exists()

        return render(request, 'test_type_range_create.html', {'form': form,
                                                               'test_type': test_type,
                                                               'reference_ranges': reference_ranges,
                                                               'default_reference_exists': default_reference_exists})

    def post(self, request, test_type_uuid):
        reference_ranges = models.TestTypeRange.objects.filter(test_type__uuid=test_type_uuid).order_by('-default').\
            order_by('-sex', 'min_age_total')
        test_type = models.TestType.objects.get(uuid=test_type_uuid)
        form = forms.TestTypeRangeForm(request.POST, instance=None, test_type=test_type)

        # check for a "default" reference range --> if one doesn't exist, then limit the first form so that the
        # first reference range object saved is default
        default_reference_exists = reference_ranges.filter(default=True).exists()
        if form.is_valid():
            form_data = functions.test_type_range_form_data(form)
            # check to see if another rule has the same demographics (other than itself)
            # which would make it duplicate
            min_age_total = form_data['min_age_total']
            max_age_total = form_data['max_age_total']

            reference_range_obj = models.TestTypeRange(test_type=test_type,
                                                       # if default reference range exists, then not default
                                                       # if doesn't exist, then True
                                                       default=not default_reference_exists,
                                                       sex=form_data['sex'],
                                                       min_age_days=form_data['min_age_days'],
                                                       min_age_weeks=form_data['min_age_weeks'],
                                                       min_age_months=form_data['min_age_months'],
                                                       min_age_years=form_data['min_age_years'],
                                                       min_age_total=min_age_total,
                                                       max_age_days=form_data['max_age_days'],
                                                       max_age_weeks=form_data['max_age_weeks'],
                                                       max_age_months=form_data['max_age_months'],
                                                       max_age_years=form_data['max_age_years'],
                                                       max_age_total=max_age_total,
                                                       range_low=form_data['range_low'],
                                                       range_high=form_data['range_high'],
                                                       critical_low=form_data['critical_low'],
                                                       critical_high=form_data['critical_high'],
                                                       report_comment=form_data['report_comment'],
                                                       critical_report_comment=form_data['critical_report_comment'])
            reference_range_obj.save()
            if form_data.get('accounts'):
                reference_range_obj.accounts.set(form_data['accounts'])
            return redirect('test_type_range_create', test_type_uuid=test_type_uuid)

        return render(request, 'test_type_range_create.html', {'form': form,
                                                               'test_type': test_type,
                                                               'reference_ranges': reference_ranges,
                                                               'default_reference_exists': default_reference_exists})


class TestTypeRangeUpdateView(LabMemberMixin, View):

    def get(self, request, test_type_uuid, range_uuid):
        test_type = models.TestType.objects.get(uuid=test_type_uuid)
        reference_range = get_object_or_404(models.TestTypeRange, test_type__uuid=test_type_uuid, uuid=range_uuid)

        min_age_days = reference_range.min_age_days
        min_age_weeks = reference_range.min_age_weeks
        min_age_months = reference_range.min_age_months
        min_age_years = reference_range.min_age_years
        max_age_days = reference_range.max_age_days
        max_age_weeks = reference_range.max_age_weeks
        max_age_months = reference_range.max_age_months
        max_age_years = reference_range.max_age_years

        form = forms.TestTypeRangeForm(initial={'sex': reference_range.sex,
                                                'min_age_days': min_age_days,
                                                'min_age_weeks': min_age_weeks,
                                                'min_age_months': min_age_months,
                                                'min_age_years': min_age_years,
                                                'max_age_days': max_age_days,
                                                'max_age_weeks': max_age_weeks,
                                                'max_age_months': max_age_months,
                                                'max_age_years': max_age_years,
                                                'range_low': reference_range.range_low,
                                                'range_high': reference_range.range_high,
                                                'critical_low': reference_range.critical_low,
                                                'critical_high': reference_range.critical_high,
                                                'report_comment': reference_range.report_comment,
                                                'critical_report_comment': reference_range.critical_report_comment,
                                                'accounts': reference_range.accounts.all()},
                                       instance=reference_range,
                                       test_type=test_type)

        return render(request, 'test_type_range_update.html', {'form': form,
                                                               'test_type': test_type,
                                                               'reference_range': reference_range})

    def post(self, request, test_type_uuid, range_uuid):
        test_type = models.TestType.objects.get(uuid=test_type_uuid)
        reference_range = get_object_or_404(models.TestTypeRange, test_type__uuid=test_type_uuid, uuid=range_uuid)

        form = forms.TestTypeRangeForm(request.POST, instance=reference_range, test_type=test_type)
        if form.is_valid():
            form_data = functions.test_type_range_form_data(form)

            reference_range.sex = form_data['sex']
            reference_range.min_age_days = form_data['min_age_days']
            reference_range.min_age_weeks = form_data['min_age_weeks']
            reference_range.min_age_months = form_data['min_age_months']
            reference_range.min_age_years = form_data['min_age_years']
            reference_range.min_age_total = form_data['min_age_total']

            reference_range.max_age_days = form_data['max_age_days']
            reference_range.max_age_weeks = form_data['max_age_weeks']
            reference_range.max_age_months = form_data['max_age_months']
            reference_range.max_age_years = form_data['max_age_years']
            reference_range.max_age_total = form_data['max_age_total']

            reference_range.range_low = form_data['range_low']
            reference_range.range_high = form_data['range_high']
            reference_range.critical_low = form_data['critical_low']
            reference_range.critical_high = form_data['critical_high']
            reference_range.report_comment = form_data['report_comment']
            reference_range.critical_report_comment = form_data['critical_report_comment']
            # save object
            reference_range.save()
            if form_data.get('accounts'):
                reference_range.accounts.set(form_data['accounts'])
            # redirect to reference range creation page
            return redirect('test_type_range_create', test_type_uuid=test_type_uuid)

        return render(request, 'test_type_range_update.html', {'form': form,
                                                               'test_type': test_type,
                                                               'reference_range': reference_range})


class TestTypeRangeDeleteView(LabMemberMixin, View):

    def get(self, request, test_type_uuid, range_uuid):
        reference_range = get_object_or_404(models.TestTypeRange, uuid=range_uuid)
        reference_range.is_active = False
        reference_range.save()

        return redirect('test_type_range_create', test_type_uuid=test_type_uuid)


class TestTypeReflexCreateView(LabMemberMixin, View):
    """
    Create TestTypeReflex object
    """

    def get(self, request, test_type_uuid):
        test_type = get_object_or_404(models.TestType, uuid=test_type_uuid)
        form = forms.TestTypeReflexForm(test_method=test_type.test_method,
                                        result_type=test_type.result_type)

        return render(request, 'test_type_reflex_create.html', {'form': form,
                                                                'test_type': test_type})

    @transaction.atomic
    def post(self, request, test_type_uuid):
        test_type = get_object_or_404(models.TestType, uuid=test_type_uuid)
        form = forms.TestTypeReflexForm(request.POST,
                                        test_method=test_type.test_method,
                                        result_type=test_type.result_type)

        if form.is_valid():
            reflex_test_type = form.cleaned_data['reflex_test_type']
            reflex_criteria_quantitative = form.cleaned_data['reflex_criteria_quantitative']
            quantitative_comparison_operator = form.cleaned_data['quantitative_comparison_operator']
            reflex_criteria_qualitative = form.cleaned_data['reflex_criteria_qualitative']
            notes = form.cleaned_data['notes']

            models.TestTypeReflex.objects.create(test_type=test_type,
                                                 reflex_test_type=reflex_test_type,
                                                 reflex_criteria_quantitative=reflex_criteria_quantitative,
                                                 reflex_criteria_qualitative=reflex_criteria_qualitative,
                                                 quantitative_comparison_operator=quantitative_comparison_operator,
                                                 notes=notes)

            messages.success(request, "Reflex successfully added!")
            return redirect('test_type_details', test_type_uuid=test_type_uuid)
        else:
            return render(request, 'test_type_reflex_create.html', {'form': form,
                                                                    'test_type': test_type})


class TestTypeReflexUpdateView(LabMemberMixin, View):
    """
    View to update existing TestTypeReflex object
    """

    def get(self, request, test_type_uuid, test_type_reflex_uuid):
        test_type = get_object_or_404(models.TestType, uuid=test_type_uuid)
        test_type_reflex = get_object_or_404(models.TestTypeReflex, uuid=test_type_reflex_uuid)

        form = forms.TestTypeReflexForm(
            test_method=test_type.test_method,
            result_type=test_type.result_type,
            initial={'reflex_test_type': test_type_reflex.reflex_test_type,
                     'reflex_criteria_quantitative': test_type_reflex.reflex_criteria_quantitative,
                     'reflex_criteria_qualitative': test_type_reflex.reflex_criteria_qualitative,
                     'quantitative_comparison_operator': test_type_reflex.quantitative_comparison_operator,
                     'notes': test_type_reflex.notes})

        return render(request, 'test_type_reflex_create.html', {'form': form,
                                                                'test_type': test_type})

    @transaction.atomic
    def post(self, request, test_type_uuid, test_type_reflex_uuid):
        test_type = get_object_or_404(models.TestType, uuid=test_type_uuid)
        test_type_reflex = get_object_or_404(models.TestTypeReflex, uuid=test_type_reflex_uuid)

        form = forms.TestTypeReflexForm(request.POST,
                                        test_method=test_type.test_method,
                                        result_type=test_type.result_type)

        if form.is_valid():
            test_type_reflex.reflex_test_type = form.cleaned_data['reflex_test_type']
            test_type_reflex.reflex_criteria_qualitative = form.cleaned_data['reflex_criteria_qualitative']
            test_type_reflex.reflex_criteria_quantitative = form.cleaned_data['reflex_criteria_quantitative']
            test_type_reflex.quantitative_comparison_operator = form.cleaned_data['quantitative_comparison_operator']
            test_type_reflex.notes = form.cleaned_data['notes']
            test_type_reflex.save()

            messages.success(request, "Reflex successfully updated!")
            return redirect('test_type_details', test_type_uuid=test_type_uuid)
        else:
            return render(request, 'test_type_reflex_create.html', {'form': form,
                                                                    'test_type': test_type})


class TestTypeReflexDeleteView(LabMemberMixin, View):
    """
    View to delete TestTypeReflex
    """

    def get(self, request, test_type_uuid, test_type_reflex_uuid):
        test_type_reflex = get_object_or_404(models.TestTypeReflex, uuid=test_type_reflex_uuid)
        test_type_reflex.is_active = False
        test_type_reflex.save()

        messages.success(request, "Reflex successfully deleted.")
        return redirect('test_type_details', test_type_uuid=test_type_uuid)


class TestTypeCommentCreateView(LabMemberMixin, View):
    def get(self, request, test_type_uuid):
        test_type = get_object_or_404(models.TestType, uuid=test_type_uuid)

        form = forms.TestTypeCommentForm()
        return render(request, 'test_type_comment_create.html', {'form': form,
                                                                 'test_type': test_type})

    def post(self, request, test_type_uuid):
        test_type = get_object_or_404(models.TestType, uuid=test_type_uuid)
        form = forms.TestTypeCommentForm(request.POST)

        if form.is_valid():
            comment = form.cleaned_data.get('comment')
            is_public = form.cleaned_data.get('is_public')

            test_type_comment = models.TestTypeComment(test_type=test_type,
                                                       comment=comment,
                                                       is_public=is_public)
            test_type_comment.save()

            redirect_to = request.GET.get('redirect_to')
            if redirect_to:
                return redirect(redirect_to)
            return redirect('test_type_details', test_type_uuid=test_type_uuid)

        return render(request, 'test_type_comment_create.html', {'form': form,
                                                                 'test_type': test_type})


class TestTypeCommentUpdateView(LabMemberMixin, View):
    def get(self, request, test_type_uuid, test_type_comment_uuid):
        test_type = get_object_or_404(models.TestType, uuid=test_type_uuid)
        test_type_comment = get_object_or_404(models.TestTypeComment, uuid=test_type_comment_uuid)

        form = forms.TestTypeCommentForm(initial={'comment': test_type_comment.comment,
                                                  'is_public': test_type_comment.is_public})

        return render(request, 'test_type_comment_create.html', {'form': form,
                                                                 'test_type': test_type,
                                                                 'update': True})

    def post(self, request, test_type_uuid, test_type_comment_uuid):
        test_type = get_object_or_404(models.TestType, uuid=test_type_uuid)
        test_type_comment = get_object_or_404(models.TestTypeComment, uuid=test_type_comment_uuid)

        form = forms.TestTypeCommentForm(request.POST)

        if form.is_valid():
            comment = form.cleaned_data.get('comment')
            is_public = form.cleaned_data.get('is_public')

            test_type_comment.comment = comment
            test_type_comment.is_public = is_public
            test_type_comment.save()

            redirect_to = request.GET.get('redirect_to')
            if redirect_to:
                return redirect(redirect_to)
            return redirect('test_type_details', test_type_uuid=test_type_uuid)

        return render(request, 'test_type_comment_create.html', {'form': form,
                                                                 'test_type': test_type})


class TestTypeCommentDeleteView(LabMemberMixin, View):
    def get(self, request, test_type_uuid, test_type_comment_uuid):
        test_type_comment = get_object_or_404(models.TestTypeComment, uuid=test_type_comment_uuid)
        test_type_comment.is_active = False
        test_type_comment.save()

        messages.success(request, "Comment successfully archived.")

        redirect_to = request.GET.get('redirect_to')
        if redirect_to:
            return redirect(redirect_to)
        return redirect('test_type_details', test_type_uuid=test_type_uuid)
