import os
import django



os.environ['DJANGO_SETTINGS_MODULE'] = 'main.settings'
django.setup()

from tenant_schemas.utils import tenant_context
import clients.models as clients_models
from orders import models, functions, generate_report_docraptor, generate_report, generate_results_out_hl7, generate_billing_hl7
from orders import tasks


def update_orders(tenant, order_uuids):
    with tenant_context(tenant):
        for order_uuid in order_uuids:
            print('Generating report for order uuid {}'.format(order_uuid))
            generate_report_docraptor.generate_pdf_report(tenant.id, order_uuid, False)
            print('Done generating report for order uuid {}'.format(order_uuid))


if __name__ == '__main__':
    tenant = clients_models.Client.objects.get(id=63)
    order_uuids = ['7fb1830f-0f88-4281-9979-dd9a32aad4e3', 'f9a1c35c-022a-4962-ab30-b3403f17d35a']
    update_orders(tenant, order_uuids)
