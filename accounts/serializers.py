from . import models
from rest_framework import serializers
from uuid import UUID, uuid4

import lab_tenant.serializers

from accounts import models as accounts_models
from lab_tenant import models as lab_tenant_models


class CriticalContactSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = models.CriticalContact
        fields = ('last_name', 'first_name', 'phone_number')


class UserAccountSerializer(serializers.HyperlinkedModelSerializer):
    critical_contacts = CriticalContactSerializer(many=True)

    class Meta:
        model = models.Account
        fields = ('name', 'alternate_id', 'uuid', 'report_preference', 'account_manages_signatures',
                  'notes', 'emr_account_number', 'results_out_show_pdf_link', 'critical_contacts')


class CollectorSerializer(serializers.HyperlinkedModelSerializer):
    account_uuids = serializers.JSONField(
        required=True,
        write_only=True,
    )
    user = lab_tenant.serializers.CollectorUserSerializer()
    uuid = serializers.UUIDField(read_only=True)
    accounts = UserAccountSerializer(many=True, read_only=True)

    def validate(self, attrs):
        account_uuids = attrs.get('account_uuids')
        # validate account uuids
        try:
            [UUID(x, version=4) for x in account_uuids]
            accounts = [accounts_models.Account.objects.get(uuid=x) for x in account_uuids]
            attrs['accounts'] = accounts
            attrs.pop('account_uuids')
        except ValueError:
            raise serializers.ValidationError("Invalid UUID format in account_uuids.")
        except accounts_models.Account.DoesNotExist:
            raise serializers.ValidationError("Account matching UUID in account_uuids does not exist.")
        except TypeError:
            raise serializers.ValidationError("account_uuids is required.")

        # validate username
        user_dict = attrs.get('user')
        username = user_dict.get('username')
        if lab_tenant_models.User.objects.filter(username__iexact=username).exists():
            raise serializers.ValidationError("This username already exists.")
        return attrs

    def create(self, validated_data):
        accounts = validated_data.get('accounts')
        user_dict = validated_data.get('user', None)
        [validated_data.pop(x) for x in ['user', 'accounts']]

        # create user object
        user_uuid = str(uuid4())
        user = lab_tenant_models.User.objects.create(
            uuid=user_uuid,
            first_name=user_dict.get('first_name'),
            last_name=user_dict.get('last_name'),
            user_type=11,
            email=user_dict.get('email', ''),
            username=user_dict.get('username', user_uuid)
        )
        # create collector object
        collector = models.Collector.objects.create(
            user=user,
        )
        collector.accounts.set(accounts)
        return collector

    class Meta:
        model = models.Collector
        fields = (
            # required
            'user', 'account_uuids',
            # read only
            'uuid',
            'accounts'
        )


class AccountSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = models.Account
        fields = ('name', 'uuid', 'alternate_id')


class ProviderSerializer(serializers.HyperlinkedModelSerializer):
    account = AccountSerializer()
    user = lab_tenant.serializers.UserSerializer()

    class Meta:
        model = models.Provider
        fields = ('uuid', 'user', 'npi', 'date_added', 'account')


class UserProviderSerializer(serializers.HyperlinkedModelSerializer):
    user = lab_tenant.serializers.UserSerializer()

    class Meta:
        model = models.Provider
        fields = ('uuid', 'user', 'npi')

