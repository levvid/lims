import django
import os

from tenant_schemas.utils import tenant_context

os.environ['DJANGO_SETTINGS_MODULE'] = 'main.settings'
django.setup()

from accounts import models as accounts_models
from clients import models as client_models
from lab import models as lab_models
from lab_tenant import models as lab_tenant_models
from orders import models as orders_models


TARGETS = ['Acetaminophen', 'Albumin', 'Alanine transaminase (ALT)', 'Alkaline phosphatase', 'Ammonia (NH3)', 'Amylase',
           'Aspartate transaminase (AST)',
           'Bicarbonate (CO2)', 'Bilirubin (Direct)', 'Bilirubin (Total)', 'Blood urea nitrogen (BUN)',
           'Calcium', 'Chloride', 'Cortisol', 'Creatine kinase (CK)',
           'Estradiol',
           'Ferritin',
           'Gentamycin',
           'Gamma-glutamyl transferase (GGT)', 'Glucose',
           'Hemoglobin A1C', 'Hemoglobin (Total)',
           'Insulin', 'Iron',
           'Lactate dehydrogenase (LDH)', 'Lidocaine', 'Lipase', 'Lithium',
           'Magnesium',
           'Phosphorous', 'Potassium', 'Progesterone', 'Prolactin', 'Protein (total)',
           'Rheumatoid factor',
           'Sodium',
           'Thyroxin (T4) (Free)', 'Thyroxin (T4) (Total)', 'Transferrin', 'Triglycerides',
           'Triiodothyronine (T3) (Free)', 'Triiodothyronine (T3) (Total)',
           'Uric acid',
           'Vitamin B12', 'Vitamin D'
           ]


def main():
    """
    run after deploy to production
    :return:
    """
    # set order code/ accession number counters
    initialize_counters()

    # create account logins
    initialize_account_logins()

    # create test targets
    add_test_targets(TARGETS)

    # create Toxicology Screening TestMethod
    lab_models.TestMethod.objects.update_or_create(name='Toxicology Screening',
                                                   cutoff_field='Concentration',
                                                   positive_above_cutoff=True,
                                                   display_order=4,
                                                   positive_result_label='Positive',
                                                   negative_result_label='Negative')


def add_test_targets(target_names):
    """
    Adding general chemistry targets
    """
    for target_name in target_names:
        print('Adding test target: {}'.format(target_name))
        lab_models.TestTarget.objects.update_or_create(name=target_name)


def initialize_counters():
    tenants = client_models.Client.objects.exclude(schema_name='public').all()
    for tenant in tenants:
        with tenant_context(tenant):
            try:
                order_code_counter = orders_models.Counter.objects.get(name='Order Code')
            except orders_models.Counter.DoesNotExist:
                print('Creating order code counter for {}'.format(tenant))
                last_order = orders_models.Order.objects.order_by('-id').first()
                if last_order:
                    last_code = last_order.code
                else:
                    last_code = '2019-0000000'
                orders_models.Counter.objects.create(
                    name='Order Code',
                    value=last_code
                )

            try:
                accession_number_counter = orders_models.Counter.objects.get(name='Accession Number')
            except orders_models.Counter.DoesNotExist:
                print('Creating accession number counter for {}'.format(tenant))
                orders_models.Counter.objects.create(
                    name='Accession Number',
                    int_value=0,
                )


def initialize_account_logins():
    tenants = client_models.Client.objects.exclude(schema_name='public').all()
    for tenant in tenants:
        print('Creating Account logins for {}'.format(tenant))
        with tenant_context(tenant):
            accounts = accounts_models.Account.objects.filter(user__isnull=True).all()
            for account in accounts:
                print('...Creating login for Account: {}'.format(account))
                if account.email:
                    account_email = account.email
                else:
                    account_email = str(account.uuid) + '@' + tenant.schema_name + '.com'
                try:
                    user_obj = lab_tenant_models.User(
                        user_type=9,
                        first_name=account.name[:30],
                        username=account_email,
                        email=account_email,
                    )
                    user_obj.set_password(str(account.uuid))
                    user_obj.save()
                except:
                    account_email = str(account.uuid) + '@' + tenant.schema_name + '.com'
                    user_obj = lab_tenant_models.User(
                        user_type=9,
                        first_name=account.name[:30],
                        username=account_email,
                        email=account_email,
                    )
                    user_obj.set_password(str(account.uuid))
                    user_obj.save()

                account.user = user_obj
                account.save(update_fields=['user'])


if __name__ == '__main__':
    main()
    #add_test_targets(TARGETS)
