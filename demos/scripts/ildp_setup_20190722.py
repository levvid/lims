import copy
import datetime
import decimal
import django

import logging
import os
import random
import sys
import timeit
import csv
import requests
import re
import uuid

from django.db import connection, models, transaction
from django.utils import timezone
from django.core.exceptions import ObjectDoesNotExist
from tenant_schemas.utils import tenant_context

os.environ['DJANGO_SETTINGS_MODULE'] = 'main.settings'
django.setup()

from clients import models as client_models
from orders import models as orders_models
from accounts import models as accounts_models
from lab import models as lab_models
from lab_tenant import models as lab_tenant_models
from patients import models as patients_models
from orders.functions import generate_order_code, generate_sample_code, get_sample_barcode

# import dummy data
from demos.shared_data.shared_data import INSTRUMENTS, TEST_TARGETS
from demos.dummy_data.common import ADDITIONAL_USERS, PROVIDERS, ACCOUNTS, PATIENTS
from demos.dummy_data.toxicology import CONTROLS, DATA_IN_LIST, INSTRUMENT_INTEGRATIONS, TEST_TYPES, \
    TEST_PANEL_GROUPS, SPECIMEN_VALIDITY_TEST_TARGETS, TEST_TYPE_RANGES, SCREENING_TEST_TYPES, POC_TEST_TYPES, \
    ORAL_TEST_TYPES
from demos.scripts.simon.simon_20190918 import import_panel_account_associations

from demos.initialization_functions import create_providers, create_accounts, create_users, create_instruments, \
    create_instrument_integrations, create_patients, create_controls, create_data_in_demo, create_test_types, \
    create_test_targets, increment_schema_name, create_test_type_ranges

from tasks import initialize_client

"""
Script to set up a new schema for demo purposes
"""

DEMO = False
NUM_ORDERS = 1

# schema variables
SCHEMA_NAME = 'ildpimporttest'
LABORATORY_NAME = 'Industry Lab Diagnostic Partners'
LABORATORY_TIMEZONE = 'US/Central'
EMAIL = 'sjung@dendisoftware.com'
FIRST_NAME = 'Simon'
LAST_NAME = 'Jung'
PASSWORD = 'dendipower123123'
SCHEMA_CONTEXT = {'client_name': LABORATORY_NAME,
                  'schema_name': SCHEMA_NAME,
                  'email': EMAIL,
                  'first_name': FIRST_NAME,
                  'last_name': LAST_NAME,
                  'password': PASSWORD,
                  'lab_type': '',
                  'address_1': '8122 Sawyer Brown Rd., Ste 210',
                  'address_2': 'Nashville, TN 37221',
                  'phone_number': '(844) 261-9529',
                  'fax_number': '(615) 630-7798',
                  'clia_number': '44D208045',
                  'clia_director': 'Danielle Gibson, MD',
                  'logo_file': 'ildp_logo.png',
                  'website': 'www.ildp.com',
                  'results_outbound_id': 'CTRON',
                  'receiving_application': 'LabRCM'
                  }

SCHEMA_SETTINGS = {"require_icd10": True,
                   "require_collected_by": True,
                   "skip_patient_history": False,
                   'show_prescribed_medication': True,
                   'use_docraptor': True,
                   'hide_fda_tests': True,
                   'enable_accession_number': True,
                   'enable_date_encoded_accession_number': True,
                   'use_zebra_printer': True,
                   'enable_individual_analytes': True,
                   }


ACCOUNTS_CSV_URL = 'https://dendi-lis.s3.amazonaws.com/client_data/ildp/account_list_logins_20190919.csv'
PROVIDERS_CSV_URL = 'https://dendi-lis.s3.amazonaws.com/client_data/ildp/provider_list.csv'
PAYERS_CSV_URL = 'https://dendi-lis.s3.amazonaws.com/client_data/ildp/ildp_final_payer_list.csv'
PATIENTS_CSV_URL = 'https://dendi-lis.s3.amazonaws.com/client_data/ildp/patient_list.csv'
TEST_PANELS_CSV_URL = 'https://dendi-lis.s3.amazonaws.com/client_data/ildp/Profile CSV.csv'
PANEL_ACCOUNT_CSV_URL = 'https://dendi-lis.s3.amazonaws.com/client_data/ildp/Profile%2BAssociations_oct_2019.csv'

HISTORICAL_PATIENT_RESULTS_CSV_URLS = [
    'https://dendi-lis.s3.amazonaws.com/client_data/ildp/August+2018.csv',
    'https://dendi-lis.s3.amazonaws.com/client_data/ildp/September+2018.csv',
    'https://dendi-lis.s3.amazonaws.com/client_data/ildp/October+2018.csv',
    'https://dendi-lis.s3.amazonaws.com/client_data/ildp/November+2018.csv',
    'https://dendi-lis.s3.amazonaws.com/client_data/ildp/December+2018.csv',
    'https://dendi-lis.s3.amazonaws.com/client_data/ildp/February+2019.csv',
    'https://dendi-lis.s3.amazonaws.com/client_data/ildp/January+2019.csv',
    'https://dendi-lis.s3.amazonaws.com/client_data/ildp/March+2019.csv',
    'https://dendi-lis.s3.amazonaws.com/client_data/ildp/April+2019.csv',
    'https://dendi-lis.s3.amazonaws.com/client_data/ildp/May+2019.csv',
    'https://dendi-lis.s3.amazonaws.com/client_data/ildp/June+2019.csv',
    'https://dendi-lis.s3.amazonaws.com/client_data/ildp/July+2019.csv',
]


@transaction.atomic
def main(demo=DEMO):
    t_start = timeit.default_timer()
    try:
        tenant = client_models.Client.objects.get(schema_name=SCHEMA_CONTEXT['schema_name'])
        logging.debug('{} schema exists'.format(SCHEMA_CONTEXT['schema_name']))
        schema_name = increment_schema_name(SCHEMA_CONTEXT['schema_name'])
        logging.debug('using {} as schema_name'.format(schema_name))
        SCHEMA_CONTEXT['schema_name'] = schema_name
    except client_models.Client.DoesNotExist:
        logging.debug('Creating schema: {}'.format(SCHEMA_CONTEXT['schema_name']))
        schema_name = SCHEMA_CONTEXT['schema_name']
    initialize_client(SCHEMA_CONTEXT)
    tenant = client_models.Client.objects.get(schema_name=SCHEMA_CONTEXT['schema_name'])
    tenant.timezone = LABORATORY_TIMEZONE
    tenant.settings = SCHEMA_SETTINGS
    tenant.save(update_fields=['timezone', 'settings'])

    logging.debug('Creating users')
    t0 = timeit.default_timer()
    with tenant_context(tenant):
        create_users(ADDITIONAL_USERS)
    t1 = timeit.default_timer()
    logging.debug('Setting up schema - {} seconds'.format(round(t1 - t0, 3)))

    logging.debug('Creating Counters')
    with tenant_context(tenant):
        orders_models.Counter.objects.create(name='Order Code', value=str(timezone.now().year)+'-0000000')
        orders_models.Counter.objects.create(name='Accession Number', int_value=0)

    logging.debug('Initializing required instruments')
    t0 = timeit.default_timer()
    create_instruments(INSTRUMENTS)
    t1 = timeit.default_timer()
    logging.debug('done - {} seconds'.format(round(t1 - t0, 3)))

    logging.debug('Initializing required test targets')
    create_test_targets(SPECIMEN_VALIDITY_TEST_TARGETS)

    with tenant_context(tenant):
        logging.debug('Initializing instrument integrations')
        t0 = timeit.default_timer()
        create_instrument_integrations(INSTRUMENT_INTEGRATIONS)
        t1 = timeit.default_timer()
        logging.debug('done - {} seconds'.format(round(t1 - t0, 3)))

        logging.debug("initializing offsite laboratory integrations")
        lab_tenant_models.OffsiteLaboratoryIntegration.objects.create(
            offsite_laboratory=lab_models.OffsiteLaboratory.objects.get(name='Lab Solutions')
        )

        # make test types
        logging.debug('Initializing test types')
        t0 = timeit.default_timer()
        create_test_types(TEST_TYPES, create_panel=False, tox=True)
        create_test_types(SCREENING_TEST_TYPES, create_panel=False, tox=True)
        create_test_types(POC_TEST_TYPES, create_panel=False, tox=True)
        create_test_types(ORAL_TEST_TYPES, create_panel=False, tox=True)
        t1 = timeit.default_timer()
        logging.debug('done - {} seconds'.format(round(t1 - t0, 3)))

        logging.debug("Initializing test type ranges")
        # specimen validity
        create_test_type_ranges(TEST_TYPE_RANGES)
        initialize_test_type_ranges([x for x in TEST_TYPES if x['group'] != 'Specimen Validity'])
        initialize_test_type_ranges(ORAL_TEST_TYPES)
        initialize_test_type_ranges(SCREENING_TEST_TYPES)

        logging.debug('Initializing instrument calibrations')
        t0 = timeit.default_timer()
        initialize_instrument_calibrations()
        t1 = timeit.default_timer()
        logging.debug('done - {} seconds'.format(round(t1 - t0, 3)))

        ################################################################################################################
        # make accounts first because everything else depends on accounts
        if demo:
            initialize_all_inclusive_test_panel_type()
            # make account
            logging.debug('Initializing accounts')
            t0 = timeit.default_timer()
            initialize_accounts()
            t1 = timeit.default_timer()
            logging.debug('done - {} seconds'.format(round(t1 - t0, 3)))

            initialize_test_panels_from_csv(TEST_PANELS_CSV_URL)
            import_panel_account_associations(PANEL_ACCOUNT_CSV_URL)

            # make patients
            logging.debug('Initializing dummy patients')
            t0 = timeit.default_timer()
            create_patients(PATIENTS)
            t1 = timeit.default_timer()
            logging.debug('done - {} seconds'.format(round(t1 - t0, 3)))

            # make providers
            logging.debug("Initializing real providers")
            t0 = timeit.default_timer()
            initialize_providers()
            t1 = timeit.default_timer()
            logging.debug('done - {} seconds'.format(round(t1 - t0, 3)))

            # make orders
            """
            logging.debug('Initializing {} dummy orders'.format(NUM_ORDERS))
            t0 = timeit.default_timer()
            initialize_dummy_orders(NUM_ORDERS)
            t1 = timeit.default_timer()
            logging.debug('done - {} seconds'.format(round(t1 - t0, 3)))
            """

            # make some in transit orders
            # logging.debug("Initializing in transit orders")
            # initialize_in_transit_orders()

            # make controls
            logging.debug("Initializing controls")
            t0 = timeit.default_timer()
            create_controls(CONTROLS)
            t1 = timeit.default_timer()
            logging.debug('done - {} seconds'.format(round(t1 - t0, 3)))

            # add data-in
            logging.debug("Initializing data in demo")
            t0 = timeit.default_timer()
            create_data_in_demo(DATA_IN_LIST)
            t1 = timeit.default_timer()
            logging.debug('done - {} seconds'.format(round(t1 - t0, 3)))

            # make dummy control observations
            logging.debug("Initializing control observations")
            t0 = timeit.default_timer()
            initialize_dummy_observations()
            t1 = timeit.default_timer()
            t_end = timeit.default_timer()
            logging.debug('done - {} seconds'.format(round(t1 - t0, 3)))
            logging.debug('total: {} seconds'.format(round(t_end - t_start, 3)))
        else:
            logging.debug('Initializing real test panels')
            t69 = timeit.default_timer()
            initialize_test_panels_from_csv(TEST_PANELS_CSV_URL)
            t70 = timeit.default_timer()
            logging.debug('done - {} seconds'.format(round(t69 - t70, 3)))

            logging.debug('Initializing real accounts')
            t0 = timeit.default_timer()
            initialize_accounts()
            t1 = timeit.default_timer()
            logging.debug('done - {} seconds'.format(round(t1 - t0, 3)))

            import_panel_account_associations(PANEL_ACCOUNT_CSV_URL)

            # make providers second because patients depend on providers and accounts
            logging.debug("Initializing real providers")
            t0 = timeit.default_timer()
            initialize_providers()
            t1 = timeit.default_timer()
            logging.debug('done - {} seconds'.format(round(t1 - t0, 3)))

            # make payers
            logging.debug("Initializing real payers")
            t0 = timeit.default_timer()
            initialize_payers()
            t1 = timeit.default_timer()
            logging.debug('done - {} seconds'.format(round(t1 - t0, 3)))

            # make patients
            logging.debug('Initializing real patients')
            t0 = timeit.default_timer()
            initialize_patients()
            t1 = timeit.default_timer()
            logging.debug('done - {} seconds'.format(round(t1 - t0, 3)))

            # for csv_url in HISTORICAL_PATIENT_RESULTS_CSV_URLS:
            #     initialize_historical_patient_results_from_csvs([csv_url])

            # make controls
            logging.debug("Initializing controls")
            t0 = timeit.default_timer()
            create_controls(CONTROLS)
            t1 = timeit.default_timer()
            logging.debug('done - {} seconds'.format(round(t1 - t0, 3)))

            # add data-in
            logging.debug("Initializing data in demo")
            t0 = timeit.default_timer()
            create_data_in_demo(DATA_IN_LIST)
            t1 = timeit.default_timer()
            logging.debug('done - {} seconds'.format(round(t1 - t0, 3)))

    return schema_name


def initialize_test_panel_types(test_panel_groups):
    """
    Create group panel types
    :param test_types:
    :return:
    """
    menu_categories = orders_models.TestPanelCategory.objects.bulk_create(
        [orders_models.TestPanelCategory(name=x) for x in TEST_PANEL_GROUPS]
    )
    test_panel_types = orders_models.TestPanelType.objects.bulk_create(
        [orders_models.TestPanelType(
            name=x[1],
            menu_category=x[0],
            category_order=1
        ) for x in zip(menu_categories, test_panel_groups)]
    )
    return menu_categories, test_panel_types


def initialize_all_inclusive_test_panel_type():
    # All inclusive panel
    test_panel_type, created = orders_models.TestPanelType.objects.update_or_create(
        name='All analyte test',
        menu_category_id=1
    )
    test_types = orders_models.TestType.objects.all()
    for test_type in test_types:
        test_panel_type.test_types.add(test_type)


def initialize_instrument_calibrations():
    ms_instruments = lab_tenant_models.InstrumentIntegration.objects.filter(
        internal_instrument_name__icontains='Shimadzu'
    )
    calibrations = []
    for ms_instrument in ms_instruments:
        ms_test_types = [x for x in TEST_TYPES if x['group'] != 'Specimen Validity']
        for test_type_dict in ms_test_types:
            test_type = orders_models.TestType.objects.get(name=test_type_dict['name'])
            calibration = orders_models.InstrumentCalibration(
                instrument_integration=ms_instrument,
                test_type=test_type,
                target_name=test_type.test_target.name,
                cutoff_value=test_type_dict.get('cutoff')
            )
            calibrations.append(calibration)
    orders_models.InstrumentCalibration.objects.bulk_create(calibrations)


def initialize_test_type_ranges(test_types):
    test_type_ranges = []
    for test_type_dict in test_types:
        test_type = orders_models.TestType.objects.get(name=test_type_dict['name'])
        range_low = test_type_dict.get('cutoff', None)
        critical_high = test_type_dict.get('upper_limit_of_quantification', None)
        if range_low or critical_high:
            test_type_range = orders_models.TestTypeRange(
                test_type=test_type,
                default=True,
                range_low=range_low,
                critical_high=critical_high
            )
            test_type_ranges.append(test_type_range)
    orders_models.TestTypeRange.objects.bulk_create(test_type_ranges)


def initialize_dummy_orders(num_orders):
    urine_sample_type = lab_models.CLIASampleType.objects.get(name='Urine')
    test_panel_type = orders_models.TestPanelType.objects.get(name='All analyte test')

    days_back = max(60, num_orders / 1.5)
    received_date_start = timezone.now() - datetime.timedelta(days=days_back)
    average_order_count = int(num_orders / days_back)

    for order_count in range(num_orders):
        latest_order_code = generate_order_code()
        rand_account = accounts_models.Account.objects.get(id=random.randint(1, 9))
        rand_patient = patients_models.Patient.objects.get(id=random.randint(1, 11))
        logging.debug('Order {}: {} {} '.format(order_count, rand_account.name, rand_patient.user.first_name))
        order = orders_models.Order.objects.create(
            provider_id=random.randint(1, 5),
            account=rand_account,
            patient=rand_patient,
            code=latest_order_code,
            research_consent=False,
            patient_result_request=False,
            received_date=received_date_start,
            submitted=True

        )

        latest_sample_code = generate_sample_code(order.code, urine_sample_type)
        sample = orders_models.Sample.objects.create(
            order=order,
            code=latest_sample_code,
            clia_sample_type=urine_sample_type,
            collection_date=timezone.now()
        )
        sample_barcode = get_sample_barcode(sample.id)
        sample.barcode = sample_barcode
        sample.save(update_fields=['barcode'])

        test_panel = orders_models.TestPanel.objects.create(
            order=order,
            test_panel_type=test_panel_type,
            expected_revenue=test_panel_type.cost,
        )

        for test_type in test_panel_type.test_types.all():
            test = orders_models.Test.objects.create(
                order=order,
                sample=sample,
                test_panel=test_panel,
                test_type=test_type,
                due_date=datetime.date.today() + datetime.timedelta(days=test_type.due_date_days)
            )
            result_obj = orders_models.Result.objects.create(
                order=order,
                test=test
            )

        if random.randint(1, average_order_count + 1) == average_order_count + 1:
            if received_date_start + datetime.timedelta(days=1, hours=2) < timezone.now():
                received_date_start = received_date_start + datetime.timedelta(days=1, hours=2)


def initialize_in_transit_orders():
    urine_sample_type = lab_models.CLIASampleType.objects.get(name='Urine')
    test_panel_type = orders_models.TestPanelType.objects.get(name='All analyte test')

    latest_order_code = generate_order_code()
    rand_account = accounts_models.Account.objects.get(id=random.randint(1, 9))
    rand_patient = patients_models.Patient.objects.get(id=random.randint(1, 11))
    logging.debug('In transit order Order: {} {} '.format(rand_account.name, rand_patient.user.first_name))
    order = orders_models.Order.objects.create(
        provider_id=1,
        account=rand_account,
        patient=rand_patient,
        code=latest_order_code,
        research_consent=False,
        patient_result_request=False,

    )

    latest_sample_code = generate_sample_code(order.code, urine_sample_type)
    sample = orders_models.Sample.objects.create(
        order=order,
        code=latest_sample_code,
        clia_sample_type=urine_sample_type,
        collection_date=timezone.now()
    )
    sample_barcode = get_sample_barcode(sample.id)
    sample.barcode = sample_barcode
    sample.save(update_fields=['barcode'])

    test_panel = orders_models.TestPanel.objects.create(
        order=order,
        test_panel_type=test_panel_type,
        expected_revenue=test_panel_type.cost,
    )

    for test_type in test_panel_type.test_types.all():
        test = orders_models.Test.objects.create(
            order=order,
            sample=sample,
            test_panel=test_panel,
            test_type=test_type,
            due_date=datetime.date.today() + datetime.timedelta(days=test_type.due_date_days)
        )
        result_obj = orders_models.Result.objects.create(
            order=order,
            test=test
        )
    order.submitted = True
    order.save(update_fields=['submitted'])


def initialize_dummy_observations():
    """
    create dummy observations for QC charting purposes
    :return:
    """
    days_back = 30
    date_counter = timezone.now() - datetime.timedelta(days=days_back)
    data_in = orders_models.DataIn.objects.first()
    controls = orders_models.Control.objects.all()
    targets = ['Cocaine', 'Lorazepam', 'Carisoprodol', 'Meprobamate']

    control_concentrations = [50.0, 100.0, 250.0, 500.0, 1000.0, 2000.0, 5000.0]

    observation_list = []
    while date_counter < timezone.now():
        control_index = 0
        for control in controls:
            for target in targets:
                observation = orders_models.Observation(
                    control=control,
                    target_name=target,
                    data_in=data_in,
                    result_value=random.uniform(control_concentrations[control_index] - 5.0,
                                                control_concentrations[control_index] + 5.0),
                    observation_date=date_counter,
                )
                observation_list.append(observation)
            control_index += 1
        date_counter += datetime.timedelta(days=1)
    orders_models.Observation.objects.bulk_create(observation_list)


def initialize_historical_patient_results_from_csvs(csv_url_list, batch_size=1000):
    """
    :param csv_url_list [String]:
    :param batch_size Int: Number of orders to import at a time.
    :return:
    """

    def batch(iterable, n=1):
        l = len(iterable)
        for index in range(0, l, n):
            yield iterable[index:min(index + n, l)]


    def get_patient_results_dict(csv_url_list):
        """
        :param csv_url_list:
        :return:
        """
        # save patient results by accession number
        patient_results_dict = {}
        # rows with errors
        rows_to_review = []

        with requests.Session() as session:
            for csv_url in csv_url_list:
                print('downloading {}'.format(csv_url))
                download = session.get(csv_url)
                decoded_content = download.content.decode('utf-8')
                csv_reader = csv.reader(decoded_content.splitlines(), delimiter=',')
                csv_rows = list(csv_reader)[1:]  # skip header line
                for row in csv_rows:
                    row = [x.strip() for x in row]
                    try:
                        accession_number = row[9]
                    except IndexError:
                        import pdb; pdb.set_trace()
                        print('hi')
                    try:
                        result_label = row[16]
                    except IndexError:
                        # client canceled order, save row for review and skip
                        rows_to_review.append(row)
                        continue

                    result_dict = {
                        'result_quantitative': row[15],
                        'result_label': row[16],
                        'test_name': row[14],
                        'test_code': row[13]
                    }
                    try:
                        patient_results_dict[accession_number]['results'].append(result_dict)
                    except KeyError:
                        # account for inconsistent date format
                        date_of_birth = row[11]
                        if len(date_of_birth.split('/')[-1]) == 4:
                            date_of_birth_format = '%m/%d/%Y'
                        else:
                            date_of_birth_format = '%m/%d/%y'
                        collection_date = row[7]
                        received_date = row[8]
                        if len(collection_date.split('/')[-1]) == 4:
                            collection_date_format = '%m/%d/%Y'
                        else:
                            collection_date_format = '%m/%d/%y'
                        if len(received_date.split('/')[-1]) == 4:
                            received_date_format = '%m/%d/%Y'
                        else:
                            received_date_format = '%m/%d/%y'
                        try:
                            patient_results_dict[accession_number] = {
                                'results': [result_dict, ],
                                'client': {'id': row[0], 'name': row[1]},
                                'physician': {'id': row[2], 'name': row[3]},
                                'patient': {
                                    'id': row[6],
                                    'last_name': row[4],
                                    'first_name': row[5],
                                    'date_of_birth': datetime.datetime.strptime(date_of_birth, date_of_birth_format),
                                    'sex': row[12]
                                },
                                'collection_date': datetime.datetime.strptime(collection_date, collection_date_format),
                                'received_date': datetime.datetime.strptime(received_date, received_date_format)
                            }
                        except ValueError:
                            import pdb; pdb.set_trace()
                            print('hi')
        return patient_results_dict

    patient_results = get_patient_results_dict(csv_url_list)

    # for bulk import
    orders_id_counter = (orders_models.Order.objects.all().aggregate(models.Max('id'))['id__max'] or 0) + 1
    physicians_id_counter = (accounts_models.Provider.objects.all().aggregate(models.Max('id'))['id__max'] or 0) + 1
    patients_id_counter = (patients_models.Patient.objects.all().aggregate(models.Max('id'))['id__max'] or 0) + 1
    users_id_counter = (lab_tenant_models.User.objects.all().aggregate(models.Max('id'))['id__max'] or 0) + 1
    samples_id_counter = (orders_models.Sample.objects.all().aggregate(models.Max('id'))['id__max'] or 0) + 1
    tests_id_counter = (orders_models.Test.objects.all().aggregate(models.Max('id'))['id__max'] or 0) + 1
    results_id_counter = (orders_models.Result.objects.all().aggregate(models.Max('id'))['id__max'] or 0) + 1
    accounts_id_counter = (accounts_models.Account.objects.all().aggregate(models.Max('id'))['id__max'] or 0) + 1
    test_types_id_counter = (orders_models.TestType.objects.all().aggregate(models.Max('id'))['id__max'] or 0) + 1
    # caching to reduce queries
    physicians = {}
    patients = {}
    accounts = {}
    test_types = {}
    # collect objects for bulk inserts
    batch_counter = 0

    print('{} orders pending import'.format(len(patient_results)))
    order_id_list = list(patient_results.keys())
    for patient_results_batch in batch(order_id_list, batch_size):
        print('starting batch {} (size: {})'.format(batch_counter, batch_size))
        batch_counter += 1

        orders_list = []
        physicians_list = []
        patients_list = []
        users_list = []
        samples_list = []
        tests_list = []
        results_list = []
        results_list = []
        accounts_list = []
        test_types_list = []

        for accession_number in patient_results_batch:
            print('creating objects for historical order: {}'.format(accession_number))
            historical_order = patient_results[accession_number]

            if historical_order['results'][0]['test_name'] == 'Specimen Rejected':
                continue
            elif historical_order['results'][0]['result_quantitative'] == 'NOTE  @':
                continue
            elif historical_order['results'][0]['result_quantitative'] == 'PENDING':
                continue
            elif 'Note' in historical_order['results'][0]['result_quantitative']:
                continue
            elif 'NOTE' in historical_order['results'][0]['result_quantitative']:
                continue

            physician_id = historical_order['physician']['id']
            if physician_id == '246':
                # one off for ragland
                physician_id = '26'
            try:
                physician = physicians[physician_id]
            except KeyError:
                try:
                    physician = accounts_models.Provider.objects.get(alternate_id=physician_id)
                    physicians[physician_id] = physician
                except accounts_models.Provider.DoesNotExist:
                    # skip accession if provider doesn't exist
                    continue
                    """
                    physician_uuid = uuid.uuid4()
                    physician_user = lab_tenant_models.User(
                        first_name=historical_order['physician']['name'],
                        id=users_id_counter,
                        user_type=10,
                        username=physician_uuid
                    )
                    users_id_counter += 1
                    users_list.append(physician_user)
                    physician = accounts_models.Provider(
                        id=physicians_id_counter,
                        alternate_id=physician_id,
                        user=physician_user,
                        uuid=physician_uuid
                    )
                    physicians_id_counter += 1
                    physicians_list.append(physician)
                    physicians[physician_id] = physician
                    """

            patient_id = historical_order['patient']['id']
            try:
                patient = patients[patient_id]
            except KeyError:
                try:
                    patient = patients_models.Patient.objects.get(alternate_id=patient_id)
                    patients[patient_id] = patient
                except patients_models.Patient.DoesNotExist:
                    patient_uuid = uuid.uuid4()
                    patient_user = lab_tenant_models.User(
                        id=users_id_counter,
                        first_name=historical_order['patient']['first_name'],
                        last_name=historical_order['patient']['last_name'],
                        user_type=20,
                        username=patient_uuid
                    )
                    users_id_counter += 1
                    users_list.append(patient_user)
                    patient = patients_models.Patient(
                        id=patients_id_counter,
                        alternate_id=patient_id,
                        user=patient_user,
                        sex=historical_order['patient']['sex'],
                        birth_date=historical_order['patient']['date_of_birth'],
                        uuid=patient_uuid
                    )
                    patients_id_counter += 1
                    patients_list.append(patient)
                    patients[patient_id] = patient

            account_id = historical_order['client']['id']
            try:
                account = accounts[account_id]
            except KeyError:
                try:
                    account = accounts_models.Account.objects.get(alternate_id=account_id)
                    accounts[account_id] = account
                except accounts_models.Account.DoesNotExist:
                    account_uuid = uuid.uuid4()
                    account_user = lab_tenant_models.User(
                        id=users_id_counter,
                        first_name=historical_order['client']['name'],
                        user_type=9,
                        username=account_uuid,
                    )
                    users_id_counter += 1
                    users_list.append(account_user)
                    account = accounts_models.Account(
                        id=accounts_id_counter,
                        alternate_id=account_id,
                        user=account_user,
                        name=historical_order['client']['name'],
                        uuid=account_uuid
                    )
                    accounts_id_counter += 1
                    accounts_list.append(account)
                    accounts[account_id] = account

            try:
                order_obj = orders_models.Order.objects.get(code=accession_number)
            except orders_models.Order.DoesNotExist:
                order_obj = orders_models.Order(
                    id=orders_id_counter,
                    accession_number=accession_number,
                    code=accession_number,
                    provider=physician,
                    patient=patient,
                    account=account,
                    received_date=historical_order['received_date'],
                    completed_date=historical_order['received_date'],
                    report_date=historical_order['received_date'],
                    submitted_date=historical_order['received_date'],
                    submitted=True,
                    completed=True,
                    reported=True,
                    partial=False,
                    is_archived_by_provider=True,
                )
                orders_id_counter += 1
                orders_list.append(order_obj)

            sample_obj = orders_models.Sample(
                id=samples_id_counter,
                order=order_obj,
                collection_date=historical_order['collection_date'],
                received_date=historical_order['received_date'],
                code=accession_number,
            )
            samples_id_counter += 1
            samples_list.append(sample_obj)

            for result in historical_order['results']:
                test_code = result['test_code']
                try:
                    result_quantitative = decimal.Decimal(result['result_quantitative'])
                except decimal.InvalidOperation:
                    if result['result_quantitative'][0] == '>':
                        result_quantitative = decimal.Decimal(result['result_quantitative'][1:])
                    elif 'Note' in result['result_quantitative']:
                        continue
                    elif 'NOTE' in result['result_quantitative']:
                        continue
                    elif result['result_quantitative'][0].upper() == 'X':
                        continue
                    elif '@' in result['result_quantitative']:
                        continue
                    elif result['result_quantitative'][0] == '.':  # canceled order
                        continue
                    else:
                        print(result['result_quantitative'])
                        continue

                if result['result_label'] == 'Not Detected':
                    result_label = '-'
                elif 'POS' in result['result_label']:
                    result_label = '+'
                elif result['result_label'] == '':
                    result_label = '-'
                else:
                    result_label = ''

                test_code = test_code.zfill(4)

                try:
                    test_type = test_types[test_code]
                except KeyError:
                    try:
                        test_type = orders_models.TestType.objects.get(alternate_id=test_code)
                        test_types[test_code] = test_type
                    except orders_models.TestType.DoesNotExist:
                        test_type = orders_models.TestType(
                            id=test_types_id_counter,
                            name=result['test_name'],
                            alternate_id=result['test_code']
                        )
                        test_types_id_counter += 1
                        test_types[test_code] = test_type
                        test_types_list.append(test_type)

                test_obj = orders_models.Test(
                    id=tests_id_counter,
                    test_type=test_type,
                    sample=sample_obj,
                    order=order_obj,
                    completed=True,
                    is_approved=True,
                    initial_report_date=historical_order['received_date'],
                )
                tests_id_counter += 1
                tests_list.append(test_obj)
                result_obj = orders_models.Result(
                    id=results_id_counter,
                    test=test_obj,
                    order=order_obj,
                    submitted_datetime=historical_order['received_date'],
                    result_quantitative=result_quantitative,
                    result=result_label
                )
                results_id_counter += 1
                results_list.append(result_obj)
        # save everything
        print("saving {} Users".format(len(users_list)))
        try:
            lab_tenant_models.User.objects.bulk_create(users_list)
        except:
            import pdb; pdb.set_trace()
            print('hi')
        print("saving {} Providers".format(len(physicians_list)))
        accounts_models.Provider.objects.bulk_create(physicians_list)
        print("saving {} Patients".format(len(patients_list)))
        patients_models.Patient.objects.bulk_create(patients_list)
        print("saving {} Account".format(len(accounts_list)))
        accounts_models.Account.objects.bulk_create(accounts_list)
        print("saving {} Orders".format(len(orders_list)))
        orders_models.Order.objects.bulk_create(orders_list)
        print("saving {} Samples".format(len(samples_list)))
        orders_models.Sample.objects.bulk_create(samples_list)
        print("saving {} TestTypes".format(len(test_types_list)))
        orders_models.TestType.objects.bulk_create(test_types_list)
        print("saving {} Tests".format(len(tests_list)))
        orders_models.Test.objects.bulk_create(tests_list)
        print("saving {} Results".format(len(results_list)))
        orders_models.Result.objects.bulk_create(results_list)

        # queries to reset primary key auto increment
        order_query = """SELECT setval(pg_get_serial_sequence('"order"','id'), coalesce(max("id"), 1), max("id") IS NOT null) FROM "order";"""
        user_query = """SELECT setval(pg_get_serial_sequence('"lab_tenant_user"','id'), coalesce(max("id"), 1), max("id") IS NOT null) FROM "lab_tenant_user";"""
        physician_query = """SELECT setval(pg_get_serial_sequence('"provider"','id'), coalesce(max("id"), 1), max("id") IS NOT null) FROM "provider";"""
        patients_query = """SELECT setval(pg_get_serial_sequence('"patient"','id'), coalesce(max("id"), 1), max("id") IS NOT null) FROM "patient";"""
        account_query = """SELECT setval(pg_get_serial_sequence('"account"','id'), coalesce(max("id"), 1), max("id") IS NOT null) FROM "account";"""
        sample_query = """SELECT setval(pg_get_serial_sequence('"sample"','id'), coalesce(max("id"), 1), max("id") IS NOT null) FROM "sample";"""
        test_type_query = """SELECT setval(pg_get_serial_sequence('"test_type"','id'), coalesce(max("id"), 1), max("id") IS NOT null) FROM "test_type";"""
        test_query = """SELECT setval(pg_get_serial_sequence('"test"','id'), coalesce(max("id"), 1), max("id") IS NOT null) FROM "test";"""
        result_query = """SELECT setval(pg_get_serial_sequence('"result"','id'), coalesce(max("id"), 1), max("id") IS NOT null) FROM "result";"""
        queries = [user_query, physician_query, patients_query, account_query, order_query, sample_query, test_type_query,
                   test_query, result_query]

        # reset primary keys for objects updated with this script
        with connection.cursor() as cursor:
            for query in queries:
                cursor.execute(query)


@transaction.atomic
def initialize_test_panels_from_csv(csv_url):
    with requests.Session() as s:
        download = s.get(csv_url)
        decoded_content = download.content.decode('utf-8')
        csv_reader = csv.reader(decoded_content.splitlines())
        csv_rows = list(csv_reader)

        cleaned_rows = []
        for row in csv_rows:
            copy = True
            try:
                row_string = row[0]
            except IndexError:  # empty row
                continue
            # replace arbitrary number of spaces with two spaces
            row_string = re.sub('   +', '  ', row_string)
            row_string = re.sub('\([^P][ 0-Z]*?:.*?\)', '', row_string)

            row_strings = row_string.split('  ')
            # cleaning
            if row_strings[0][:12] == 'Profile list':
                copy = False
            if row_strings[0][:3] == '===':
                copy = False
            if row_strings[0][:3] == '---':
                copy = False
            if len(row_strings) >= 2:
                if row_strings[1][:3] == '===':
                    copy = False
                if row_strings[1][:3] == '---':
                    copy = False
                if row_strings[1][:3] == '~~~':
                    copy = False
            if copy:
                cleaned_rows.append(row_strings)
        panel_dict = {}
        panel_id = ''
        for row in cleaned_rows:
            if row[0]:  # new panel
                new_panel = True
                panel_info = row[0].split(' ')
                panel_id = panel_info[0]
                if len(panel_info) > 1:
                    panel_name = ' '.join(panel_info[1:])
                else:
                    panel_name = ''
                if panel_name == '':
                    panel_name = row[1]
                panel_dict[panel_id] = {'name': panel_name, 'tests': []}
            else:
                if len(row) == 5 and row[1]:  # 2 test row
                    test_1_info = row[1].split()
                    test_1_name = ' '.join(test_1_info[1:])
                    test_2_info = row[3].split()
                    test_2_name = ' '.join(test_2_info[1:])
                    if '(P)' not in test_1_name:
                        panel_dict[panel_id]['tests'].append({'code': test_1_info[0],
                                                              'name': test_1_name,
                                                              })
                    if '(P)' not in test_2_name:
                        panel_dict[panel_id]['tests'].append({'code': test_2_info[0],
                                                              'name': test_2_name,
                                                              })
                elif len(row) == 3 and row[1]:  # 1 test row
                    test_1_info = row[1].split()
                    test_1_name = ' '.join(test_1_info[1:])
                    if '(P)' not in test_1_name:
                        panel_dict[panel_id]['tests'].append({'code': test_1_info[0],
                                                              'name': test_1_name,
                                                              })
                elif len(row) == 4 and row[1]:
                    pass # sub panel title

        test_obj_dict = {}
        for panel_code in panel_dict:
            print('creating panels now')
            name = panel_dict[panel_code]['name']
            if name:
                panel = orders_models.TestPanelType.objects.create(
                    alternate_id=panel_code,
                    name=name,
                    menu_category_id=1,
                    is_exclusive=True,
                )
                tests = []
                for test_dict in panel_dict[panel_code]['tests']:
                    try:
                        test_type_obj = test_obj_dict[test_dict['code']]
                    except KeyError:
                        try:
                            test_type_obj = orders_models.TestType.objects.get(alternate_id=test_dict['code'])
                            test_obj_dict[test_dict['code']] = test_type_obj
                        except orders_models.TestType.DoesNotExist:
                            test_type_obj = None
                            print('missing test type with {} - {}'.format(test_dict['code'], test_dict['name']))
                    if test_type_obj:
                        tests.append(test_type_obj)
                panel.test_types.set(tests)


def initialize_accounts():
    # get accounts list CSV for ILDP from S3 bucket
    # convert to list of dictionaries

    with requests.Session() as s:
        download = s.get(ACCOUNTS_CSV_URL)

        decoded_content = download.content.decode('utf-8')

        csv_reader = csv.reader(decoded_content.splitlines(), delimiter=',')
        # skip first line (columns)
        account_list = list(csv_reader)[1:]
        account_dict_list = []

        for row in account_list:
            # format phone/fax
            row[3] = re.sub("\D", "", row[3])
            row[4] = re.sub("\D", "", row[4])
            account_dict_list.append({'alternate_id': row[0],
                                      'name': row[1],
                                      'address1': row[2],
                                      'phone_number': row[3],
                                      'fax_number': row[4],
                                      'username': row[5],
                                      'password': row[6]})
        # create & save account objects
        create_accounts(account_dict_list)
        return account_dict_list


def initialize_providers():
    # get CSV from S3 bucket
    # convert to list of dictionaries
    tenant = client_models.Client.objects.get(schema_name=SCHEMA_CONTEXT['schema_name'])
    with tenant_context(tenant):

        with requests.Session() as s:
            download = s.get(PROVIDERS_CSV_URL)

            decoded_content = download.content.decode('utf-8')

            csv_reader = csv.reader(decoded_content.splitlines(), delimiter=',')
            # skip first line (columns)
            provider_list = list(csv_reader)[1:]
            provider_dict_list = []

            for row in provider_list:
                provider_id = row[0]
                provider_last_name = row[5]
                provider_first_name = row[6]
                provider_npi = row[7]
                provider_email = str(provider_id) + '@' + SCHEMA_NAME + '.com'
                provider_title = row[8]

                provider_dict_list.append({'alternate_id': provider_id,
                                           'first_name': provider_first_name,
                                           'last_name': provider_last_name,
                                           'email': provider_email,
                                           'npi': provider_npi,
                                           'title': provider_title
                                           })

            # create providers first, then add accounts
            create_providers(provider_dict_list)

            for row in provider_list:
                provider_npi = row[7]
                # clean associated account data
                account_id_list = [row[1], row[2], row[3], row[4]]
                # clean empty items from list
                account_id_list = list(filter(None, account_id_list))
                accounts = accounts_models.Account.objects.filter(alternate_id__in=account_id_list)

                # link providers to accounts
                provider = accounts_models.Provider.objects.get(npi=provider_npi)
                provider.accounts.set(accounts)
                logging.debug('Accounts ' + str(accounts) + ' added to provider ' + str(provider))


def initialize_payers():
    # get CSV from S3 bucket
    # convert to list of dictionaries
    tenant = client_models.Client.objects.get(schema_name=SCHEMA_CONTEXT['schema_name'])
    with tenant_context(tenant):
        with requests.Session() as s:
            download = s.get(PAYERS_CSV_URL)

            decoded_content = download.content.decode('utf-8')

            csv_reader = csv.reader(decoded_content.splitlines(), delimiter=',')
            # skip first line (columns)
            payer_list = list(csv_reader)[1:]
            payer_dict_list = []

            for row in payer_list:
                # simple logic to parse Medicare and Medicaid
                row[1] = str(row[1]).strip().upper()
                # simple parse
                # row0 - id, row1 - name, row2 - address, row3 - city, row4 - state, row5 - zip, row6 - phone#, host code (payer code)

                payer_dict_list.append({'alternate_id': row[0],
                                        'name': row[1],
                                        'address': row[2],
                                        'city': row[3],
                                        'state': row[4],
                                        'zip_code': row[5],
                                        'phone_number': row[6],
                                        'host_code': row[7].strip()})
            payers = []
            for payer_dict in payer_dict_list:
                payer = patients_models.Payer(alternate_id=payer_dict.get('alternate_id'),
                                              name=payer_dict.get('name'),
                                              address_1=payer_dict.get('address'),
                                              city=payer_dict.get('city'),
                                              state=payer_dict.get('state'),
                                              zip_code=payer_dict.get('zip_code'),
                                              phone_number=payer_dict.get('phone_number'),
                                              payer_code=payer_dict.get('host_code'))
                payers.append(payer)
            patients_models.Payer.objects.bulk_create(payers)

            """
            # if we are to update instead of creating from scratch
            payer, created = patients_models.Payer.objects.update_or_create(alternate_id=payer_dict.get('alternate_id'),
                                                                            defaults={
                                                                                'name': payer_dict.get('name'),
                                                                                'payer_code': payer_dict.get('host_code'),
                                                                                'address_1': payer_dict.get('address'),
                                                                                'city': payer_dict.get('city'),
                                                                                'state': payer_dict.get('state'),
                                                                                'zip_code': payer_dict.get('zip_code'),
                                                                                'phone_number': payer_dict.get('phone_number')
                                                                            })
            if created:
                logging.debug('Payer ' + str(payer) + ' added.')
            """
            logging.debug('Payers added.')


@transaction.atomic
def initialize_patients():
    # get CSV from S3 bucket
    # can create 3 objects - User, Patient, PatientPayer
    # we will ignore PatientGuarantor, since the logic is too complex to be worth it
    # we don't really care about patient duplicates either - there's no good heuristic for this, since the data is messy

    tenant = client_models.Client.objects.get(schema_name=SCHEMA_CONTEXT['schema_name'])
    with tenant_context(tenant):
        with requests.Session() as s:
            download = s.get(PATIENTS_CSV_URL)

            decoded_content = download.content.decode('utf-8')

            csv_reader = csv.reader(decoded_content.splitlines(), delimiter=',')
            # skip first line (columns)
            patient_list = list(csv_reader)[1:]

            for row in patient_list:
                # strip out whitespace
                row = list(map(str.strip, row))

                # Patient info
                patient_alternate_id = row[0]
                last_name = row[1]
                first_name = row[2]
                middle_initial = row[3]
                sex = row[4]
                birth_date = datetime.datetime.strptime(row[5], '%m/%d/%y')
                # string parsing means that if the birthday is mm/dd/2025, it'll automatically convert to 1925
                # we are assuming no one aged 100 or greater
                if birth_date.year > datetime.datetime.now().year:
                    birth_date = birth_date.replace(year=birth_date.year - 100)
                address = row[6] + row[7] + ' ' + row[8] + ', ' + row[9] + ' ' + row[10]

                # Provider info
                provider_alternate_id = row[12]
                # if provider is not listed in the provider list, then skip over everything else and continue
                # since patient is no longer with ILDP
                try:
                    provider = accounts_models.Provider.objects.get(alternate_id=provider_alternate_id)
                except ObjectDoesNotExist:
                    logging.debug('Provider with ID ' + provider_alternate_id + ' does not exist.')
                    continue

                # PatientPayer info
                payer1_member_id = row[15]
                payer1_group_number = row[16]
                # payer_id = 0 is self-pay
                try:
                    payer1_alternate_id = int(row[13])
                    if payer1_alternate_id:
                        primary_payer = patients_models.Payer.objects.get(alternate_id=payer1_alternate_id)
                    else:
                        primary_payer = None
                except:
                    primary_payer = None

                # PatientPayer info
                payer2_member_id = row[19]
                payer2_group_number = row[20]
                # payer_id = 0 is self-pay
                try:
                    payer2_alternate_id = int(row[17])
                    if payer2_alternate_id:
                        secondary_payer = patients_models.Payer.objects.get(alternate_id=payer2_alternate_id)
                    else:
                        secondary_payer = None
                except:
                    secondary_payer = None

                # SE (self) SP (spouse) CH (child) PA (parent), and OT (other)
                # In this case, CH means that the patient is a child of the insured
                # if invalid value, default to SE
                relation = row[23]
                subscriber_first_name = row[21]
                subscriber_last_name = row[20]
                if relation == 'SE':
                    subscriber_relationship = 'Self'
                elif relation == 'SP':
                    subscriber_relationship = 'Spouse'
                elif relation == 'CH':
                    subscriber_relationship = 'Parent'
                elif relation == 'OT':
                    subscriber_relationship = 'Other'
                else:
                    subscriber_relationship = 'Self'

                ########################################################################################################
                # create patient
                # generate random string for username and password initially
                user_uuid = str(uuid.uuid4())
                username = first_name + '-' + last_name + '-' + user_uuid

                # create User object
                patient_user = lab_tenant_models.User(first_name=first_name,
                                                      last_name=last_name,
                                                      username=username,
                                                      user_type=20)
                patient_user.set_password(user_uuid)
                # create User object first before creating Patient
                patient_user.save()

                patient = patients_models.Patient(alternate_id=patient_alternate_id,
                                                  user=patient_user,
                                                  middle_initial=middle_initial,
                                                  sex=sex,
                                                  birth_date=birth_date,
                                                  address1=address)
                patient.save()
                logging.debug('Patient: ' + str(patient) + ' added.')

                # add provider after patient creation (many-to-many operation)
                patient.providers.add(provider)

                # if subscriber is Self, then put in all the patient user info
                if subscriber_relationship == 'Self':
                    # add PatientPayer objects
                    if primary_payer:
                        patient_payer_primary = patients_models.PatientPayer(patient=patient,
                                                                             payer=primary_payer,
                                                                             payer_type=primary_payer.payer_type,
                                                                             is_primary=True,
                                                                             member_id=payer1_member_id,
                                                                             group_number=payer1_group_number,
                                                                             subscriber_first_name=patient.user.first_name,
                                                                             subscriber_last_name=patient.user.last_name,
                                                                             subscriber_middle_initial=patient.middle_initial,
                                                                             subscriber_relationship=subscriber_relationship)
                        patient_payer_primary.save()
                        logging.debug('Patient Payer: ' + str(patient_payer_primary) + ' added.')

                    if secondary_payer:
                        patient_payer_secondary = patients_models.PatientPayer(patient=patient,
                                                                               payer=secondary_payer,
                                                                               payer_type=secondary_payer.payer_type,
                                                                               is_secondary=True,
                                                                               member_id=payer2_member_id,
                                                                               group_number=payer2_group_number,
                                                                               subscriber_first_name=patient.user.first_name,
                                                                               subscriber_last_name=patient.user.last_name,
                                                                               subscriber_middle_initial=patient.middle_initial,
                                                                               subscriber_relationship=subscriber_relationship)
                        patient_payer_secondary.save()
                        logging.debug('Patient Payer: ' + str(patient_payer_secondary) + ' added.')
                else:
                    # add PatientPayer objects
                    if primary_payer:
                        patient_payer_primary = patients_models.PatientPayer(patient=patient,
                                                                             payer=primary_payer,
                                                                             payer_type=primary_payer.payer_type,
                                                                             is_primary=True,
                                                                             member_id=payer1_member_id,
                                                                             group_number=payer1_group_number,
                                                                             subscriber_first_name=subscriber_first_name,
                                                                             subscriber_last_name=subscriber_last_name,
                                                                             subscriber_relationship=subscriber_relationship)
                        patient_payer_primary.save()
                        logging.debug('Patient Payer: ' + str(patient_payer_primary) + ' added.')

                    if secondary_payer:
                        patient_payer_secondary = patients_models.PatientPayer(patient=patient,
                                                                               payer=secondary_payer,
                                                                               payer_type=secondary_payer.payer_type,
                                                                               is_secondary=True,
                                                                               member_id=payer2_member_id,
                                                                               group_number=payer2_group_number,
                                                                               subscriber_first_name=subscriber_first_name,
                                                                               subscriber_last_name=subscriber_last_name,
                                                                               subscriber_relationship=subscriber_relationship)
                        patient_payer_secondary.save()
                        logging.debug('Patient Payer: ' + str(patient_payer_secondary) + ' added.')


if __name__ == '__main__':
    logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)
    schema_name = main()
    """
    tenant = client_models.Client.objects.get(schema_name=schema_name)
    with tenant_context(tenant):
        initialize_test_panels_from_csv(TEST_PANELS_CSV_URL)
    """
