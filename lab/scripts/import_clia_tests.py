import boto3
import codecs
import csv
import datetime
import django
import logging
import os
import sys
import tempfile

os.environ['DJANGO_SETTINGS_MODULE'] = 'main.settings'
django.setup()

from .. import models

CSV_KEY = 'codes/clia_detail.txt'
BUCKET_NAME = 'dendi-lis'


def main():
    """
    A script to import file of CLIA vitro test systems.
    https://www.fda.gov/MedicalDevices/DeviceRegulationandGuidance/Databases/ucm142437.htm

    This script is deprecated, use import_clia_tests_update.py
    :return:
    """
    import_clia_tests()
    fill_clia_test_type_specialty()


def import_clia_tests():
    # initialize boto 3 connection
    s3 = boto3.client(
        's3',
        aws_access_key_id=os.environ.get('S3_ACCESS_KEY'),
        aws_secret_access_key=os.environ.get('S3_SECRET_KEY')
    )

    with tempfile.NamedTemporaryFile() as temp:
        # download source data from s3
        s3.download_file(BUCKET_NAME, CSV_KEY, temp.name)

        csv_reader = csv.DictReader(codecs.open(temp.name, "r", encoding="utf-8", errors='ignore'),
                                    delimiter='|', quotechar='"')
        test_list = []

        for row in csv_reader:
            test_type = models.CLIATestType(
                document_number=row['DOCUMENT_NUMBER'],
                test_system_id=row['TEST_SYSTEM_ID'],
                test_system_name=row['TEST_SYSTEM_NAME'],
                qualifier_1=row['QUALIFIER1'],
                qualifier_2=row['QUALIFIER2'],
                analyte_id=row['ANALYTE_ID'],
                analyte_name=row['ANALYTE_NAME'],
                specialty_id=row['SPECIALTY_ID'],
                complexity=row['COMPLEXITY'],
                date_effective=datetime.datetime.strptime(row['DATE_EFFECTIVE'][:10], '%Y/%M/%d')
            )
            logging.debug(row)
            test_list.append(test_type)
        models.CLIATestType.objects.bulk_create(test_list)


def fill_clia_test_type_specialty():
    models.CLIATestTypeSpecialty(specialty_id=1, specialty_name='Urinalysis').save()
    models.CLIATestTypeSpecialty(specialty_id=2, specialty_name='General Chemistry').save()
    models.CLIATestTypeSpecialty(specialty_id=3, specialty_name='General Immunology').save()
    models.CLIATestTypeSpecialty(specialty_id=4, specialty_name='Hematology').save()
    models.CLIATestTypeSpecialty(specialty_id=5, specialty_name='Immunohematology').save()
    models.CLIATestTypeSpecialty(specialty_id=6, specialty_name='Endocrinology').save()
    models.CLIATestTypeSpecialty(specialty_id=7, specialty_name='Toxicology/TDM').save()
    models.CLIATestTypeSpecialty(specialty_id=8, specialty_name='Bacteriology').save()
    models.CLIATestTypeSpecialty(specialty_id=9, specialty_name='Mycobacteriology').save()
    models.CLIATestTypeSpecialty(specialty_id=10, specialty_name='Virology').save()
    models.CLIATestTypeSpecialty(specialty_id=11, specialty_name='Parasitology').save()
    models.CLIATestTypeSpecialty(specialty_id=12, specialty_name='Mycology').save()
    models.CLIATestTypeSpecialty(specialty_id=13, specialty_name='Cytology').save()


if __name__ == '__main__':
    logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)
    main()
