# Generated by Django 2.0.2 on 2019-04-22 18:50

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('orders', '0011_test_batch_date'),
    ]

    operations = [
        migrations.AddField(
            model_name='testpanel',
            name='batch_date',
            field=models.DateTimeField(blank=True, null=True),
        ),
    ]
