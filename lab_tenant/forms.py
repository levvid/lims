from dal import autocomplete, forward
from django import forms
import decimal

from . import models

from lab import models as lab_models
from orders import models as orders_models


class OffsiteIntegrationForm(forms.Form):
    offsite_laboratory = forms.ModelChoiceField(
        queryset=lab_models.OffsiteLaboratory.objects.all(),
        widget=autocomplete.ModelSelect2(
            url='offsite_laboratory_autocomplete',
        ),
        required=True,
        label='Offsite Laboratory'
    )
    sending_facility_id = forms.CharField(required=False,
                                          widget=forms.TextInput(
                                              attrs={'class': 'form-control'},
                                          ),
                                          label='Sending Facility ID (The offsite laboratory will provide this)')


class LabDepartmentForm(forms.Form):
    lab_department_name = forms.CharField(required=True,
                                          max_length=64,
                                          label="Department Name",
                                          widget=forms.TextInput(attrs={'class': 'form-control'})
    )

    def clean_name(self):
        name = self.cleaned_data['lab_department_name']
        matching_lab_department_name = models.LabDepartment.objects.filter(name__iexact=name)
        if matching_lab_department_name:
            matching_lab_department_name = matching_lab_department_name.exclude(id=self.instance_id)
            if matching_lab_department_name:
                self.add_error('name',
                               forms.ValidationError('A Lab Department with the name {} already exists.'.format(name)))
        return name

    def __init__(self, *args, instance_id, **kwargs):
        self.instance_id = instance_id
        # this allows you to customize the required error message for all fields
        super(LabDepartmentForm, self).__init__(*args, **kwargs)
        for field in self.fields.values():
            field.error_messages = {'required': '{fieldname} is required.'.format(fieldname=field.label),
                                    'invalid': '{fieldname} is invalid.'.format(fieldname=field.label),
                                    'blank': '{fieldname} is blank.'.format(fieldname=field.label)}

class InstrumentIntegrationForm(forms.Form):
    internal_instrument_name = forms.CharField(
        required=True,
        label='Instrument Name',
        widget=forms.TextInput(attrs={'class': 'form-control'})
    )
    instrument = forms.ModelChoiceField(
        queryset=lab_models.Instrument.objects.all(),
        widget=autocomplete.ModelSelect2(
            url='instrument_autocomplete',
        ),
        required=True,
        label='Instrument Model'
    )
    serial_number = forms.CharField(
        required=False,
        label='Serial Number',
        widget=forms.TextInput(attrs={'class': 'form-control'})
    )
    after_data_import = forms.ChoiceField(
        required=True,
        choices=(('Do nothing', 'Do nothing'),
                 ('Automatically complete orders', 'Automatically complete orders'),
                 ('Automatically report orders', 'Automatically report orders')),
    )

    def clean_internal_instrument_name(self):
        internal_instrument_name = self.cleaned_data.get('internal_instrument_name')
        matching_internal_instrument_name = models.InstrumentIntegration.objects.filter(
            internal_instrument_name__iexact=internal_instrument_name
        )
        if self.instance_id:
            matching_internal_instrument_name = matching_internal_instrument_name.exclude(id=self.instance_id)
        if matching_internal_instrument_name:
            self.add_error('internal_instrument_name', forms.ValidationError('This instrument name already exists.'))
        return internal_instrument_name

    def __init__(self, *args, instance_id, **kwargs):
        super(InstrumentIntegrationForm, self).__init__(*args, **kwargs)
        self.instance_id = instance_id


class InstrumentCalibrationForm(forms.Form):
    instrument_integration = forms.ModelChoiceField(
        queryset=models.InstrumentIntegration.objects.all(),
        widget=autocomplete.ModelSelect2(
            url='instrument_integration_autocomplete',
        ),
        required=True,
        label='Instrument'
    )
    test_type = forms.ModelChoiceField(
        queryset=orders_models.TestType.objects.all(),
        widget=autocomplete.ModelSelect2(
            url='test_type_autocomplete',
        ),
        required=True,
        label='Test '
    )
    target_name = forms.CharField(
        required=True,
        label='Target/Analyte Name (Used by Instrument)',
        widget=forms.TextInput(attrs={'class': 'form-control'})
    )
    cutoff_value = forms.DecimalField(
        required=False,
        label='Cutoff Value',
        widget=forms.NumberInput(attrs={'class': 'form-control'})
    )
    use_test_cutoff_values = forms.BooleanField(
        required=False,
        label='Use Cutoffs Defined in Test',
        widget=forms.CheckboxInput(attrs={'class': 'use-test-cutoff-values'})
    )

    def clean_cutoff_value(self):
        cutoff_value = self.cleaned_data.get('cutoff_value')
        # truncate last decimals
        if cutoff_value:
            cutoff_value = decimal.Decimal(cutoff_value).quantize(
                decimal.Decimal('.00000001'), rounding=decimal.ROUND_DOWN)
            if cutoff_value >= decimal.Decimal('99999999.99999999'):
                self.add_error('cutoff_value', 'Cutoff values is too large.')
        return cutoff_value

    def clean_target_name(self):
        target_name = self.cleaned_data.get('target_name')
        instrument_integration = self.cleaned_data.get('instrument_integration')
        matching_target_name = orders_models.InstrumentCalibration.objects.filter(
            target_name__iexact=target_name,
            instrument_integration=instrument_integration
        )
        if self.calibration:
            matching_target_name = matching_target_name.exclude(id=self.calibration.id)
        if matching_target_name:
            self.add_error('target_name', forms.ValidationError('A calibration with this analyte/target already exists.'))
        return target_name

    def clean(self):
        # if not using cutoffs in Test, must require cutoff value
        use_test_cutoff_values = self.cleaned_data.get('use_test_cutoff_values')
        if not use_test_cutoff_values:
            cutoff_value = self.cleaned_data.get('cutoff_value')
            if not cutoff_value:
                self.add_error('cutoff_value', forms.ValidationError(
                    'If not using reference range values defined in the test, a cutoff value is required.'
                ))

    def __init__(self, *args, calibration, **kwargs):
        # this allows you to customize the required error message for all fields
        self.calibration = calibration
        super(InstrumentCalibrationForm, self).__init__(*args, **kwargs)


class InstrumentTestCodeForm(forms.ModelForm):

    class Meta:
        model = orders_models.InstrumentTestCode
        fields = []

    test_type_field = forms.ModelChoiceField(
        queryset=orders_models.TestType.objects.all(),
        widget=autocomplete.ModelSelect2(url='test_type_autocomplete'),
        required=False,
        label='Test '
    )

    test_code = forms.CharField(
        required=True,
        label='Test Code',
        widget=forms.TextInput(attrs={'class': 'form-control'})
    )

    def clean_test_code(self):
        test_code = self.cleaned_data.get('test_code')
        matching_test_code = orders_models.InstrumentTestCode.objects.filter(test_code=test_code)

        # self.instance is for updating form
        if self.instance:
            matching_test_code = matching_test_code.exclude(test_code=self.instance.test_code)
            if matching_test_code:
                self.add_error('test_code', forms.ValidationError('Test code already exists.'))
        return test_code

    def __init__(self, *args, **kwargs):
        # this allows you to customize the required error message for all fields
        super(InstrumentTestCodeForm, self).__init__(*args, **kwargs)
        for field in self.fields.values():
            field.error_messages = {'required': '{fieldname} is required.'.format(fieldname=field.label),
                                    'invalid': '{fieldname} is invalid.'.format(fieldname=field.label),
                                    'blank': '{fieldname} is blank.'.format(fieldname=field.label)}


class InHouseLabLocationForm(forms.Form):
    name = forms.CharField(max_length=255, label='Name', required=True)
    address1 = forms.CharField(max_length=255, label='Mailing Address', required=False)
    address2 = forms.CharField(
        max_length=255, label='Apartment, suite, unit (optional)', required=False)
    zip_code = forms.CharField(max_length=128, label='Zip Code', required=False)
    lab_director = forms.CharField(max_length=255, label='Lab Director', required=False)
    alias = forms.CharField(max_length=255, label='Alias', required=False)
    phone_number = forms.CharField(max_length=255, label='Phone', required=False)
    fax_number = forms.CharField(max_length=255, label='Fax', required=False)

    def clean_name(self):
        name = self.cleaned_data['name']
        matching_in_house_lab_name = models.InHouseLabLocation.objects.filter(name__iexact=name)
        if matching_in_house_lab_name:
            matching_in_house_lab_name = matching_in_house_lab_name.exclude(id=self.instance_id)
            if matching_in_house_lab_name:
                self.add_error('name',
                               forms.ValidationError('An In-House Lab with the name {} already exists.'.format(name)))
        return name

    def __init__(self, *args, instance_id, **kwargs):
        self.instance_id = instance_id
        # this allows you to customize the required error message for all fields
        super(InHouseLabLocationForm, self).__init__(*args, **kwargs)
        for field in self.fields.values():
            field.error_messages = {'required': '{fieldname} is required.'.format(fieldname=field.label),
                                    'invalid': '{fieldname} is invalid.'.format(fieldname=field.label),
                                    'blank': '{fieldname} is blank.'.format(fieldname=field.label)}
