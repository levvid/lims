from braces.views import LoginRequiredMixin
from django_tables2 import RequestConfig
from django.contrib import messages
from django.db import transaction
from django.forms import formset_factory
from django.http import HttpResponseRedirect
from django.shortcuts import render, redirect, get_object_or_404
from django.views.generic import View

from orders import forms
from orders import models
from ..generate_load_sheet import generate_load_sheet, generate_384_load_sheet, generate_96_plate_layout


from lab_tenant.views import LabMemberMixin, LISLoginRequiredMixin
from lab import models as lab_models


class BatchListView(LISLoginRequiredMixin, View):
    def get(self, request):
        return render(request, 'batch_list.html', {})


class BatchCreateView(LISLoginRequiredMixin, View):
    def get(self, request):
        form = forms.BatchForm()

        return render(request, 'batch_create.html', {'form': form})

    @transaction.atomic
    def post(self, request):
        form = forms.BatchForm(request.POST)
        context = {'form': form}

        if form.is_valid():
            name = form.cleaned_data['name']
            test_panel_type = form.cleaned_data['test_panel_type']
            instrument_integration = form.cleaned_data['instrument_integration']
            controls = form.cleaned_data['controls']

            batch = models.Batch.objects.create(
                name=name,
                test_panel_type=test_panel_type,
                instrument_integration=instrument_integration,
            )
            batch.controls.set(controls)
            return redirect('batch_add_samples', batch_uuid=batch.uuid)

        else:
            return render(request, 'batch_create.html', context)


class BatchDetailsView(LISLoginRequiredMixin, View):
    def get(self, request, batch_uuid):
        batch = get_object_or_404(models.Batch, uuid=batch_uuid)
        instrument_integration = batch.instrument_integration

        context = {'batch': batch}
        return render(request, 'batch_details.html', context)


class BatchAddSamplesView(LISLoginRequiredMixin, View):
    def get(self, request, batch_uuid):
        batch = get_object_or_404(models.Batch, uuid=batch_uuid)
        test_panel_type = batch.test_panel_type
        controls = batch.controls.order_by('code').all()
        num_controls = controls.count()

        sample_select_initial_data = []
        for x in range(96 - num_controls):
            sample_select_initial_data += [{'test_panel_type': test_panel_type}]

        SampleSelectFormSet = formset_factory(forms.SampleSelectForm, formset=forms.BaseFormSet, extra=0)
        sample_select_formset = SampleSelectFormSet(prefix='sample_select',
                                                    initial=sample_select_initial_data,
                                                    form_kwargs={'test_panel_type_uuid': test_panel_type.uuid})

        context = {
            'batch': batch,
            'controls': controls,
            'num_controls': num_controls,
            'sample_select_formset': sample_select_formset
        }
        return render(request, 'batch_add_samples.html', context)

    @transaction.atomic
    def post(self, request, batch_uuid):
        batch = get_object_or_404(models.Batch, uuid=batch_uuid)
        test_panel_type = batch.test_panel_type
        controls = batch.controls.order_by('code').all()
        num_controls = controls.count()

        # autofill logic
        if 'autofill_button' in request.POST:
            pending_samples = models.Sample.objects.prefetch_related('order').filter(
                order__test_panels__test_panel_type=test_panel_type,
                order__report_date__isnull=True,
            ).exclude(
                batches__test_panel_type=test_panel_type
            ).order_by('order__received_date')[:(96 - num_controls)]
            sample_select_initial_data = []
            for sample in pending_samples:
                sample_select_initial_data += [{'test_panel_type': test_panel_type,
                                                'sample': sample}]

            # fill rest of sample forms with blank initial_data
            for x in range(96 - num_controls - len(sample_select_initial_data)):
                sample_select_initial_data += [{'test_panel_type': test_panel_type}]

            SampleSelectFromSet = formset_factory(forms.SampleSelectForm, formset=forms.BaseFormSet, extra=0)
            sample_select_formset = SampleSelectFromSet(prefix='sample_select',
                                                        initial=sample_select_initial_data,
                                                        form_kwargs={'test_panel_type_uuid': test_panel_type.uuid})
            context = {
                'batch': batch,
                'controls': controls,
                'num_controls': num_controls,
                'sample_select_formset': sample_select_formset
            }
            return render(request, 'batch_add_samples.html', context)

        SampleSelectFromSet = formset_factory(forms.SampleSelectForm, formset=forms.BaseFormSet, extra=0)
        sample_select_formset = SampleSelectFromSet(request.POST, prefix='sample_select',
                                                    form_kwargs={'test_panel_type_uuid': test_panel_type.uuid})

        if sample_select_formset.is_valid():
            for sample_form in sample_select_formset:
                if sample_form.is_valid():
                    sample = sample_form.cleaned_data['sample']
                    if sample:
                        batch.samples.add(sample)

            # generate load_sheet
            generate_load_sheet(request, batch)
            generate_96_plate_layout(request, batch)
            generate_384_load_sheet(request, batch)

        return redirect('batch_details', batch_uuid=batch.uuid)


class BatchUpdateSamplesView(LISLoginRequiredMixin, View):
    def get(self, request, batch_uuid):
        batch = get_object_or_404(models.Batch, uuid=batch_uuid)
        test_panel_type = batch.test_panel_type
        controls = batch.controls.order_by('code').all()
        num_controls = controls.count()

        sample_select_initial_data = []
        batch_samples = list(batch.samples.all())
        for sample in batch_samples:
            sample_select_initial_data += [{'test_panel_type': test_panel_type,
                                            'sample': sample}]
        for x in range(96 - num_controls - len(sample_select_initial_data)):
            sample_select_initial_data += [{'test_panel_type': test_panel_type}]

        SampleSelectFormSet = formset_factory(forms.SampleSelectForm, formset=forms.BaseFormSet, extra=0)
        sample_select_formset = SampleSelectFormSet(prefix='sample_select',
                                                    initial=sample_select_initial_data,
                                                    form_kwargs={'test_panel_type_uuid': test_panel_type.uuid})

        context = {
            'batch': batch,
            'sample_select_formset': sample_select_formset,
            'controls': controls,
            'num_controls': num_controls
        }
        return render(request, 'batch_add_samples.html', context)

    @transaction.atomic
    def post(self, request, batch_uuid):
        batch = get_object_or_404(models.Batch, uuid=batch_uuid)
        test_panel_type = batch.test_panel_type
        controls = batch.controls.order_by('code').all()
        num_controls = controls.count()

        # autofill logic
        if 'autofill_button' in request.POST:
            # should retain existing samples
            sample_select_initial_data = []
            for sample in batch.samples.all():
                sample_select_initial_data += [{'test_panel_type': test_panel_type,
                                                'sample': sample}]
            pending_samples = models.Sample.objects.prefetch_related('order').filter(
                order__test_panels__test_panel_type=test_panel_type,
                order__report_date__isnull=True,
            ).exclude(
                batches__test_panel_type=test_panel_type
            ).order_by('order__received_date')[:96 - num_controls - len(sample_select_initial_data)]
            for sample in pending_samples:
                sample_select_initial_data += [{'test_panel_type': test_panel_type,
                                                'sample': sample}]

            # fill rest of sample forms with blank initial_data
            for x in range(96 - num_controls - len(sample_select_initial_data)):
                sample_select_initial_data += [{'test_panel_type': test_panel_type}]

            SampleSelectFromSet = formset_factory(forms.SampleSelectForm, formset=forms.BaseFormSet, extra=0)
            sample_select_formset = SampleSelectFromSet(prefix='sample_select',
                                                        initial=sample_select_initial_data,
                                                        form_kwargs={'test_panel_type_uuid': test_panel_type.uuid})
            context = {
                'batch': batch,
                'sample_select_formset': sample_select_formset
            }
            return render(request, 'batch_add_samples.html', context)

        SampleSelectFromSet = formset_factory(forms.SampleSelectForm, formset=forms.BaseFormSet, extra=0)
        sample_select_formset = SampleSelectFromSet(request.POST, prefix='sample_select',
                                                    form_kwargs={'test_panel_type_uuid': test_panel_type.uuid})

        if sample_select_formset.is_valid():
            # clear existing batch samples and test panel batch date
            batch.samples.clear()

            # add samples to batch
            for sample_form in sample_select_formset:
                if sample_form.is_valid():
                    sample = sample_form.cleaned_data['sample']
                    if sample:
                        batch.samples.add(sample)
            # generate load_sheet
            generate_load_sheet(request, batch)
            generate_96_plate_layout(request, batch)
            generate_384_load_sheet(request, batch)
        return redirect('batch_details', batch_uuid=batch.uuid)

