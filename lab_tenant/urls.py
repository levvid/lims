from django.urls import path

from . import views

urlpatterns = [
    path('offsite_integrations/', views.OffsiteIntegrationListView.as_view(), name='offsite_integration_list'),
    path('offsite_integrations/create/', views.OffsiteIntegrationCreateView.as_view(),
         name='offsite_integration_create'),
    path('offsite_integrations/<str:offsite_integration_uuid>/', views.OffsiteIntegrationDetailsView.as_view(),
         name='offsite_integration_details'),
    path('offsite_integrations/<str:offsite_integration_uuid>/update/', views.OffsiteIntegrationUpdateView.as_view(),
         name='offsite_integration_update'),
    path('offsite_integrations/<str:offsite_integration_uuid>/delete/', views.OffsiteIntegrationDeleteView.as_view(),
         name='offsite_integration_delete'),

    # instrument integration views
    path('instrument_integrations/',
         views.InstrumentIntegrationListView.as_view(),
         name='instrument_integration_list'),
    path('instrument_integrations/create/',
         views.InstrumentIntegrationCreateView.as_view(),
         name='instrument_integration_create'),
    path('instrument_integrations/<str:instrument_integration_uuid>/',
         views.InstrumentIntegrationDetailsView.as_view(),
         name='instrument_integration_details'),
    path('instrument_integrations/<str:instrument_integration_uuid>/update/',
         views.InstrumentIntegrationUpdateView.as_view(),
         name='instrument_integration_update'),
    path('instrument_integrations/<str:instrument_integration_uuid>/delete/',
         views.InstrumentIntegrationDeleteView.as_view(),
         name='instrument_integration_delete'),

    # instrument calibration views
    path('instrument_integrations/<str:instrument_integration_uuid>/calibrations/create/',
         views.InstrumentCalibrationCreateView.as_view(),
         name='instrument_calibration_create'),
    path('instrument_integrations/<str:instrument_integration_uuid>/calibrations/<str:instrument_calibration_uuid>/',
         views.InstrumentCalibrationDetailsView.as_view(),
         name='instrument_calibration_details'),
    path('instrument_integrations/<str:instrument_integration_uuid>/calibrations/'
         '<str:instrument_calibration_uuid>/update/',
         views.InstrumentCalibrationUpdateView.as_view(),
         name='instrument_calibration_update'),
    path('instrument_integrations/<str:instrument_integration_uuid>/calibrations/'
         '<str:instrument_calibration_uuid>/delete/',
         views.InstrumentCalibrationDeleteView.as_view(),
         name='instrument_calibration_delete'),

    # instrument test codes views
    path('instrument_integrations/<str:instrument_integration_uuid>/instrument_test_codes/create/',
         views.InstrumentTestCodeCreateView.as_view(),
         name='instrument_test_codes_create'),
    path('instrument_integrations/<str:instrument_integration_uuid>/instrument_test_codes/<str:instrument_test_code_uuid>/update/',
         views.InstrumentTestCodeUpdateView.as_view(),
         name='instrument_test_codes_update'),
    path('instrument_integrations/<str:instrument_integration_uuid>/instrument_test_codes/<str:instrument_test_code_uuid>/delete/',
         views.InstrumentTestCodeDeleteView.as_view(),
         name='instrument_test_codes_delete'),

    # autocomplete views
    path('lab_member_autocomplete/', views.LabMemberAutocomplete.as_view(), name='lab_member_autocomplete'),
    path('collector_autocomplete/', views.CollectorAutocomplete.as_view(), name='collector_autocomplete'),
    path('instrument_integration_autocomplete/', views.InstrumentIntegrationAutocomplete.as_view(),
         name='instrument_integration_autocomplete'),
    path('lab_department_autocomplete/', views.LabDepartmentAutocomplete.as_view(),
         name='lab_department_autocomplete'),
    path('in_house_lab_location/', views.InHouseLabLocationAutocomplete.as_view(),
         name='in_house_lab_location_autocomplete'),

    # in-house lab location views
    path('in_house_lab_locations/', views.InHouseLabLocationListView.as_view(),
         name='in_house_lab_location_list'),
    path('in_house_lab_locations/create/', views.InHouseLabLocationCreateView.as_view(),
         name='in_house_lab_location_create'),
    path('in_house_lab_locations/<str:in_house_lab_location_uuid>/', views.InHouseLabLocationDetailsView.as_view(),
         name='in_house_lab_location_details'),
    path('in_house_lab_locations/<str:in_house_lab_location_uuid>/update/', views.InHouseLabLocationUpdateView.as_view(),
         name='in_house_lab_location_update'),
    path('in_house_lab_locations/<str:in_house_lab_location_uuid>/delete/', views.InHouseLabLocationDeleteView.as_view(),
         name='in_house_lab_location_delete'),

    path('lab_departments/', views.LabDepartmentListView.as_view(),
         name='lab_department_list'),
    path('lab_departments/<str:lab_department_uuid>/', views.LabDepartmentDetailsView.as_view(),
         name='lab_department_details'),
    path('lab_departments/<str:lab_department_uuid>/update/', views.LabDepartmentUpdatetView.as_view(),
         name='lab_department_update'),
    path('lab_departments/<str:lab_department_uuid>/delete/', views.LabDepartmentDeleteView.as_view(),
         name='lab_department_delete')

]
