import csv
import docraptor
import django
import os
import tempfile
import time

from django.template.loader import render_to_string

os.environ['DJANGO_SETTINGS_MODULE'] = 'main.settings'
django.setup()

docraptor.configuration.username = os.environ['DOCRAPTOR_API']
docraptor.configuration.debug = True

if os.environ.get('DEBUG') == 'False':
    DEBUG = False
else:
    DEBUG = True

from . import models
from main.functions import convert_tenant_timezone_to_utc, convert_utc_to_tenant_timezone, date_string_or_none

HEADER_ROW = ['SamplePlate.SerialNumber', 'SampleInfo.Address', 'SampleInfo.SampleID', 'SampleInfo.Description',
              'SampleInfo.Dilution', 'SamplePlateSelectedArea.SelectedAreaIndex']


def sample_address_increment(sample_address):
    if sample_address:
        letter = sample_address[0]
        number = int(sample_address[1:])
        if number < 12:
            number += 1
        else:
            number = 1
            letter = chr(ord(letter) + 1)
        return letter + '{0:02d}'.format(number)
    else:
        return 'A01'


def sample_address_increment_384(sample_address):
    if sample_address:
        letter = sample_address[0]
        number = int(sample_address[1:])
        if number < 24:
            number += 1
        else:
            number = 1
            letter = chr(ord(letter) + 1)
        return letter + '{}'.format(number)
    else:
        return 'A1'


def generate_load_sheet(request, batch):
    """
    create a load sheet
    :param request:
    :param batch:
    :return:
    """
    controls = batch.controls.order_by('code').all()
    samples = batch.samples.all()
    sample_address_counter = 'A01'

    with tempfile.NamedTemporaryFile() as temp:
        with open(temp.name, 'w') as csv_file:
            file_writer = csv.writer(csv_file, delimiter=',')
            file_writer.writerow(HEADER_ROW)

            for control in controls:
                file_writer.writerow(['1', sample_address_counter, control.code, '', '', ''])
                sample_address_counter = sample_address_increment(sample_address_counter)

            for sample in samples:
                file_writer.writerow(['1', sample_address_counter, sample.code, '', '', ''])
                sample_address_counter = sample_address_increment(sample_address_counter)

            for x in range(96 - len(controls) - len(samples)):
                file_writer.writerow(['1', sample_address_counter, '', '', '', ''])
                sample_address_counter = sample_address_increment(sample_address_counter)

        tenant = request.tenant
        save_file_name = 'load_sheets/{}/{}-96.csv'.format(tenant.name, batch.name)
        batch.load_sheet.save(save_file_name, temp)

    return save_file_name


def generate_384_load_sheet(request, batch):
    """
    create a load sheet for 384 well
    :param request:
    :param batch:
    :return:
    """
    controls = batch.controls.order_by('code').all()
    samples = batch.samples.all()
    sample_address_counter = 'A1'

    with tempfile.NamedTemporaryFile() as temp:
        with open(temp.name, 'w') as csv_file:
            file_writer = csv.writer(csv_file, delimiter=',')
            file_writer.writerow(HEADER_ROW)

            for control in controls:
                file_writer.writerow(['1', sample_address_counter, control.code, '', '', '0'])
                sample_address_counter = sample_address_increment_384(sample_address_counter)

            for sample in samples:
                file_writer.writerow(['1', sample_address_counter, sample.code, '', '', '0'])
                sample_address_counter = sample_address_increment_384(sample_address_counter)

            for x in range(192 - len(controls) - len(samples)):
                file_writer.writerow(['1', sample_address_counter, '', '', '', '0'])
                sample_address_counter = sample_address_increment_384(sample_address_counter)

        tenant = request.tenant
        save_file_name = 'load_sheets_384/{}/{}-384.csv'.format(tenant.name, batch.name)
        batch.load_sheet_384.save(save_file_name, temp)

    return save_file_name


def generate_96_plate_layout(request, batch):
    """
    create a 96 well plate layout pdf
    :param request:
    :param batch:
    :return:
    """
    batch_dict = dict()
    samples = batch.samples.all()
    controls = batch.controls.order_by('code').all()
    sample_address_counter = 'A01'
    for control in controls:
        batch_dict[sample_address_counter] = control.code
        sample_address_counter = sample_address_increment(sample_address_counter)
    for sample in samples:
        batch_dict[sample_address_counter] = sample.code[5:]
        sample_address_counter = sample_address_increment(sample_address_counter)

    doc_api = docraptor.DocApi()
    rendered_html = render_to_string('plate_layout_96.html', {'batch_name': batch.name,
                                                              'batch_dict': batch_dict})
    save_file_name = 'plate_layout_96/{}/{}-96.pdf'.format(request.tenant, batch.name)

    response = doc_api.create_async_doc({
        "test": DEBUG,
        "document_content": rendered_html,
        "name": save_file_name,
        "document_type": "pdf",
        "prince_options": {
            'media': 'screen',
        },
    })

    # async loop
    while True:
        status_response = doc_api.get_async_doc_status(response.status_id)
        if status_response.status == 'completed':
            doc_response = doc_api.get_async_doc(status_response.download_id)
            with tempfile.NamedTemporaryFile() as temp:
                temp.write(doc_response)
                batch.plate_layout_96.save(save_file_name, temp)
            break
        elif status_response.status == 'failed':
            print("FAILED: {}".format(status_response))
            break
        else:
            time.sleep(1)
