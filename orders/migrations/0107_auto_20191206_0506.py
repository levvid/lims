# Generated by Django 2.2.2 on 2019-12-06 05:06

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('orders', '0106_auto_20191129_2133'),
    ]

    operations = [
        migrations.AlterField(
            model_name='orderpatientpayer',
            name='payer_type',
            field=models.CharField(choices=[('Self-Pay', 'Self-Pay'), ('Commercial', 'Commercial'), ('Medicare', 'Medicare'), ('Medicaid', 'Medicaid'), ('Workers Comp', "Workers' Compensation"), ('Other', 'Other')], default='Commercial', max_length=64),
        ),
    ]
