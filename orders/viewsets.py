from django.core.exceptions import ValidationError
from rest_framework import viewsets
from . import serializers
from . import models
from lab_tenant.models import User
from django.core.exceptions import ValidationError
from rest_framework.permissions import IsAuthenticated
from lab import serializers as lab_serializers
from accounts import models as accounts_models

from uuid import UUID


class UserOrderViewSet(viewsets.ModelViewSet):
    """
    External API endpoint that allows Orders relevant for the user to be viewed
    user_id Required
    accession_number Optional
    order_code Optional

    Note: All datetimes use UTC timezone unless specified
    """
    serializer_class = serializers.UserOrderSerializer
    http_method_names = ['get', 'put']
    lookup_field = 'code'

    def get_queryset(self):
        user_id = self.request.query_params.get('user_id')
        if user_id:
            try:
                user = User.objects.get(id=user_id)
            except User.DoesNotExist:
                return models.Order.objects.none()
            except ValueError:
                return models.Order.objects.none()
            if user.user_type == 1:
                orders = models.Order.objects.all()
            elif user.user_type == 9:  # Account user
                account = accounts_models.Account.objects.get(user=user)
                orders = models.Order.objects.filter(account=account)
            elif user.user_type == 10:  # Provider user
                provider = accounts_models.Provider.objects.get(user=user)
                orders = models.Order.objects.filter(provider=provider)
            else:
                orders = models.Order.objects.none()
        else:
            orders = models.Order.objects.none()
        accession_number = self.request.query_params.get('accession_number')
        if accession_number:
            orders = orders.filter(accession_number=accession_number)
        order_code = self.request.query_params.get('order_code')
        if order_code:
            orders = orders.filter(code=order_code)
        return orders


class UserSampleViewSet(viewsets.ModelViewSet):
    """
    External API endpoint that allows Samples to be queried
    user_id Required
    order_code Optional
    accession_number Optional
    account_uuid Optional
    provider_uuid Optional


    Note: All datetimes use UTC timezone unless specified
    """
    serializer_class = serializers.SampleSerializer
    http_method_names = ['get']
    lookup_field = 'uuid'

    def get_queryset(self):
        user_id = self.request.query_params.get('user_id')
        if user_id:
            try:
                user = User.objects.get(id=user_id)
            except User.DoesNotExist:
                return models.Sample.objects.none()
            except ValueError:
                return models.Sample.objects.none()
            if user.user_type == 1:
                samples = models.Sample.objects.all()
            elif user.user_type == 9:  # Account user
                account = accounts_models.Account.objects.get(user=user)
                samples = models.Sample.objects.filter(order__account=account)
            elif user.user_type == 10:  # Provider user
                provider = accounts_models.Provider.objects.get(user=user)
                samples = models.Sample.objects.filter(order__provider=provider)
            else:
                samples = models.Sample.objects.none()
        else:
            samples = models.Sample.objects.none()

        order_code = self.request.query_params.get('order_code')
        if order_code:
            samples = samples.filter(order__code=order_code)
        accession_number = self.request.query_params.get('accession_number')
        if accession_number:
            samples = samples.filter(order__accession_number=accession_number)
        account_uuid = self.request.query_params.get('account_uuid')
        if account_uuid:
            try:
                _ = UUID(account_uuid, version=4)
                account = accounts_models.Account.objects.get(uuid=account_uuid)
                samples = samples.filter(order__account=account)
            except ValueError:
                samples = samples.none()
            except accounts_models.Account.DoesNotExist:
                samples = samples.none()
        provider_uuid = self.request.query_params.get('provider_uuid')
        if provider_uuid:
            try:
                _ = UUID(provider_uuid, version=4)
                provider = accounts_models.Provider.objects.get(uuid=provider_uuid)
                samples = samples.filter(order__provider=provider)
            except ValueError:
                samples = samples.none()
            except accounts_models.Provider.DoesNotExist:
                samples = samples.none()
        return samples


class TestPanelTypeViewSet(viewsets.ModelViewSet):
    """
    External API endpoint that allows Test Panel Types to be viewed
    test_panel_type_uuid Optional:
    account_uuid Optional:
    """
    serializer_class = serializers.TestPanelTypeSerializer
    http_method_names = ['get']
    lookup_field = 'uuid'

    def get_queryset(self):
        queryset = models.TestPanelType.objects.all()

        account_uuid = self.request.query_params.get('account_uuid')
        if account_uuid:
            try:
                _ = UUID(account_uuid, version=4)
                account = accounts_models.Account.objects.get(uuid=account_uuid)
                queryset = queryset.filter(is_exclusive=False) | \
                    models.TestPanelType.objects.filter(is_exclusive=True, exclusive_accounts=account)
            except ValueError:
                queryset = queryset.none()
            except accounts_models.Account.DoesNotExist:
                queryset = queryset.none()

        test_panel_type_uuid = self.request.query_params.get('test_panel_type_uuid')
        if test_panel_type_uuid:
            try:
                # UUID validation
                _ = UUID(test_panel_type_uuid, version=4)
                queryset = queryset.filter(uuid=test_panel_type_uuid)
            except ValueError:
                queryset = queryset.none()

        queryset = queryset.distinct()
        return queryset


class UserTestPanelTypeAutocompleteViewSet(viewsets.ModelViewSet):
    """
    External API endpoint that allows TestPanelTypes to be viewed
    account_uuid Required:
    q Optional
    """
    serializer_class = serializers.UserTestPanelTypeSerializer
    http_method_names = ['get']
    lookup_field = 'uuid'

    def get_queryset(self):
        account_uuid = self.request.query_params.get('account_uuid')
        try:
            account = accounts_models.Account.objects.get(uuid=account_uuid)
        except accounts_models.Account.DoesNotExist:
            return models.TestPanelType.objects.none()
        except ValidationError:
            return models.TestPanelType.objects.none()
        if account:
            query = self.request.query_params.get('q')

            test_panel_types = models.TestPanelType.objects.\
                exclude(excluded_accounts=account).\
                filter(is_exclusive=True, exclusive_accounts=account) | \
                                 models.TestPanelType.objects.filter(is_exclusive=False)
            if query:
                test_panel_types = test_panel_types.filter(name__istartswith=query)
        else:
            test_panel_types = models.TestPanelType.objects.none()
        test_panel_types = test_panel_types.distinct()
        return test_panel_types


class UserIssueViewSet(viewsets.ModelViewSet):
    """
    External API endpoint that allows OrderIssues relevant for the user to be viewed
    user_id Required: Int
    account_uuid: String : UUID of account
    provider_uuid: String : UUID of provider
    """
    serializer_class = serializers.UserIssueSerializer
    http_method_names = ['get']
    lookup_field = 'uuid'

    def get_queryset(self):
        user_id = self.request.query_params.get('user_id')
        account_uuid = self.request.query_params.get('account_uuid')
        provider_uuid = self.request.query_params.get('provider_uuid')

        if user_id:
            try:
                user = User.objects.get(id=user_id)
            except User.DoesNotExist:
                return models.OrderIssue.objects.none()
            if user.user_type < 3:
                issues = models.OrderIssue.objects.filter(is_resolved=False)
            elif user.user_type == 9:  # account user
                account = accounts_models.Account.objects.get(user=user)
                issues = models.OrderIssue.objects.filter(
                    is_resolved=False,
                    order__account=account
                )
            elif user.user_type == 10:  # provider user
                provider = accounts_models.Provider.objects.get(user=user)
                issues = models.OrderIssue.objects.filter(
                    is_resolved=False,
                    order__account=provider.accounts
                )
            elif user.user_type == 11:  # collector user
                collector = accounts_models.Collector.objects.get(user=user)
                issues = models.OrderIssue.objects.filter(
                    is_resolved=False,
                    order__account=collector.accounts
                )
            else:
                issues = models.OrderIssue.objects.none()
        else:
            issues = models.OrderIssue.objects.none()

        # account query param
        if account_uuid:
            try:
                account = accounts_models.Account.objects.get(uuid=account_uuid)
            except accounts_models.Account.DoesNotExist:
                return issues.none()
            except ValidationError:
                return issues.none()
            issues = issues.filter(order__account=account)

        # provider query param
        if provider_uuid:
            try:
                provider = accounts_models.Provider.objects.get(uuid=provider_uuid)
            except accounts_models.Provider.DoesNotExist:
                return issues.none()
            except ValidationError:
                return issues.none()
            issues = issues.filter(order__provider=provider)
        return issues


class UserTestTypeAutocompleteViewSet(viewsets.ModelViewSet):
    """
    External API endpoint that allows TestTypes to be viewed
    user_id Required: Int

    """
    serializer_class = serializers.UserTestTypeSerializer
    http_method_names = ['get']
    lookup_field = 'uuid'

    def get_queryset(self):
        # currently don't filter test types by user
        user_id = self.request.query_params.get('user_id')
        query = self.request.query_params.get('q')

        test_types = models.TestType.objects.all()
        if query:
            test_types = test_types.filter(name__istartswith=query)
        return test_types


class TestPanelViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows Test Panels to be viewed
    """
    queryset = models.TestPanel.objects.all().order_by('-id')
    serializer_class = serializers.TestPanelSerializer
    http_method_names = ['get']
    lookup_field = 'uuid'


class OrderViewSet(viewsets.ModelViewSet):
    """
    External endpoint that allows Orders to be created.
    Patient insurance information will be pulled from Patient. Update patient insurance before calling this API

    Required Fields
    provider_uuid
    patient_uuid
    account_uuid
    test_panel_uuids [String] : TestPanelTypes ordered (at least 1 uuid required)
        ex. ["f6fab634-3ac7-4d53-8ff9-52d84828e8ac", "f6fab634-3ac7-4d53-8ff9-52d84828e8ac"]
    collection_date - Datetime - Date sample was collection (service date) YYYY-MM-DDThh:mm[:ss[.uuuuuu]][+HH:MM|-HH:MM|Z]

    Optional Fields
    received Bool - If true, order will be given a received_date and accession number

    """
    queryset = models.Order.objects.filter(submitted=True).order_by('-submitted_date')
    serializer_class = serializers.OrderSerializer
    http_method_names = ['get', 'post']
    lookup_field = 'code'


class OrderTestPanelViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows Test Panels for a specific Order to be viewed
    """
    serializer_class = serializers.TestPanelSerializer
    http_method_names = ['get']
    lookup_field = 'uuid'

    def get_queryset(self):
        order = models.Order.objects.get(code=self.kwargs['order_code'])
        test_panels = order.test_panels.all()
        return test_panels


class OrderSampleViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows Samples for a specific Order to be viewed
    """
    serializer_class = serializers.SampleSerializer
    http_method_names = ['get']
    lookup_field = 'code'

    def get_queryset(self):
        order = models.Order.objects.get(code=self.kwargs['order_code'])
        samples = order.samples.all()
        return samples


class TestTypeViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows TestTypes to be viewed
    """
    queryset = models.TestType.objects.all()
    serializer_class = serializers.TestTypeSerializer
    http_method_names = ['get']
    lookup_field = 'uuid'


class SampleViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows Samples to be viewed
    """
    queryset = models.Sample.objects.all().order_by('-code')
    serializer_class = serializers.SampleSerializer
    http_method_names = ['get']
    lookup_field = 'code'
