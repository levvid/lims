import re
import string

from django import forms

from clients import models as clients_models


#####################################################################################################################
PASSWORD_LENGTH = 6

#####################################################################################################################
LABORATORY_TYPES = (('POL', 'Physician Office Laboratory (POL)'),
                    ('OTHER', 'Other'))


class LoginForm(forms.Form):
    """
    Redirects to tenant login form
    """
    url = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'my-awesome-practice'}))

    def clean_url(self):
        url = self.cleaned_data['url']
        # check if url matches existing schemas
        try:
            tenant = clients_models.Client.objects.get(schema_name=url)
        except clients_models.Client.DoesNotExist:
            raise forms.ValidationError("We couldn't find your workspace. Please check your workspace URL.")
        return url


class ScheduleDemoForm(forms.Form):
    """
    Lets potential clients sign up for demo
    """
    name = forms.CharField(required=True)
    email = forms.CharField(required=True)
    recaptcha_response = forms.CharField(required=True)

    def clean_name(self):
        name = self.cleaned_data.get('name')
        name = name.strip()
        return name


class SignupForm(forms.Form):
    """
    Email verification form
    """
    email = forms.EmailField(required=True,
                             widget=forms.TextInput(attrs={'class': 'text-field w-input'}),
                             label='Email Address')


class ClientCreateForm(forms.Form):
    client_name = forms.CharField(required=True,
                                  label='Practice or Laboratory Name')
    lab_type = forms.ChoiceField(choices=LABORATORY_TYPES,
                                 label='Laboratory Type',
                                 widget=forms.RadioSelect(),
                                 initial=LABORATORY_TYPES[0],
                                 required=False)

    def clean_schema_name(self):
        schema_name = self.cleaned_data['schema_name']
        # strip whitespace and lowercase
        schema_name = schema_name.lower().strip()

        # check alphanumeric and dashes
        if not re.match('^[a-zA-Z0-9][A-Za-z0-9]*$', schema_name):
            raise forms.ValidationError('Workspace URL only accepts letters and numbers.')

        if clients_models.Client.objects.filter(schema_name=schema_name):
            raise forms.ValidationError('This workspace name is already taken. Please choose another name.')

        return schema_name


class ClientCreate2Form(forms.Form):

    first_name = forms.CharField(required=True,
                                 label='Your First Name',
                                 error_messages={'required': 'First name is required.'})
    last_name = forms.CharField(required=True,
                                label='Your Last Name',
                                error_messages={'required': 'Last name is required.'})
    new_password = forms.CharField(required=True,
                                   label='Choose a Password',
                                   widget=forms.PasswordInput(),
                                   error_messages={'required': 'Password is required.'})
    confirm_new_password = forms.CharField(required=True,
                                           label='Confirm Password',
                                           widget=forms.PasswordInput(),
                                           error_messages={'required': 'Please confirm your password.'})

    def clean_new_password(self):
        new_password = self.cleaned_data['new_password']
        if len(new_password) <= PASSWORD_LENGTH:
            raise forms.ValidationError(
                'Password must be at least {} characters long.'.format(
                    PASSWORD_LENGTH))
        return new_password

    def clean_confirm_new_password(self):
        confirm_new_password = self.cleaned_data['confirm_new_password']
        return confirm_new_password

    def clean(self):
        if not self.cleaned_data.get('new_password') == self.cleaned_data.get(
                'confirm_new_password'):
            self.add_error(
                'confirm_new_password',
                forms.ValidationError('Both passwords must match.'),
            )


