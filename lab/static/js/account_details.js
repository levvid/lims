function createAccountReportedOrdersTable(orders, table) {
  orders.forEach(element => {
    element.code.toString = () => element.code.text;
  });

  if (!table) {
    const hotElement = document.querySelector('#account-reported-orders-table');
    const hotSettings = {
      data: orders,
      columns: [
        {
          data: 'code',
          editor: false,
          renderer: linkRenderer,
        },
        {
          data: 'status',
          editor: false,
          type: 'text',
          className: 'htRight',
        },
        {
          data: 'account',
          editor: false,
          renderer: linkRenderer,
          width: 150,
        },
        {
          data: 'provider',
          editor: false,
          renderer: linkRenderer,
          width: 150,
        },
        {
          data: 'submitted_date',
          editor: false,
          type: 'date',
          dateFormat: 'MM/DD/YYYY',
          correctFormat: true,
        },
        {
          data: 'report_date',
          editor: false,
          type: 'date',
          dateFormat: 'MM/DD/YYYY',
          correctFormat: true,
        },
        {
          data: 'report_pdf',
          editor: false,
          renderer: 'html',
          className: 'htCenter',
        },
      ],
      stretchH: 'all',
      //width: 806,
      height: 600,
      autoWrapRow: true,
      manualRowResize: false,
      manualColumnResize: false,
      columnSorting: true,
      sortIndicator: true,
      rowHeaders: false,
      colHeaders: true,
      nestedHeaders: [
        [
          'Order Code',
          'Status',
          'Account',
          'Provider',
          'Submitted Date',
          'Report Date',
          'View',
        ],
      ],
      manualRowMove: false,
      manualColumnMove: false,
      contextMenu: false,
      filters: true,
      dropdownMenu: ['filter_by_condition', 'filter_action_bar'],
      afterGetColHeader: function(col, TH) {
        TH.className = 'center';
      },
    };

    return new Handsontable(hotElement, hotSettings);
  } else {
    // load new data without replacing old table
    table.getPlugin('filters').clearConditions();
    table.loadData(orders);
    return table;
  }
}
