from main.tenant_admin import tenant_admin_site

from . import models

tenant_admin_site.register(models.Order)
tenant_admin_site.register(models.Sample)
#tenant_admin_site.register(models.TestPanelType)
tenant_admin_site.register(models.TestType)

#tenant_admin_site.register(models.TestPanel)
tenant_admin_site.register(models.Test)
tenant_admin_site.register(models.ReportPDFFooter)
tenant_admin_site.register(models.OrderComment)