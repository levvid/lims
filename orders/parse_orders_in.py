import os
import datetime
import uuid
import tempfile

import boto3
import requests

import django
from django.utils import timezone

os.environ['DJANGO_SETTINGS_MODULE'] = 'main.settings'
django.setup()

from orders import generate_hl7, generate_requisition_form, models, tasks
from orders import functions
from accounts import models as accounts_models
from patients import models as patients_models
from clients import models as clients_models
from tenant_schemas.utils import tenant_context
from lab_tenant.models import User
from lab import models as lab_models




"""
Necessary Info for Order creation:
1. Account
2. Provider
3. Patient
4. ICD codes
5. Payer Info - Type, Name, Rship to subscriber, Member ID, Group #,
    a) Can be Primary, secondary, tertiary, workers comp
6. Guarantor
7. Prescribed Meds
8. Test Panels
9. Tests
10. Sample Info - type, draw type, collection date, collection_time, temp, collected by


"""
BUCKET_NAME = 'dendi-lis'
ORDERS_IN_DIRECTORY = 'Mirth/Integrations/Orders Inbound/'
TEST_HL7 = "https://dendi-lis.s3.amazonaws.com/Mirth/Integrations/test_billing_messages/Industry+Lab+Diagnostic+Partners/Orders+In/test_oml.hl7"

def return_value_or_empty(segment, index):
    try:
        return segment[index]
    except IndexError:
        return ''


def get_name(name_segment):
    name_segment = name_segment.split('^')
    last_name = name_segment[0]
    first_name = name_segment[1]
    middle_initial = name_segment[2]

    return last_name, first_name, middle_initial


def get_address(address_segment):
    address_segment = address_segment.split('^')
    address_1 = address_segment[0]
    address_2 = address_segment[1]
    city = address_segment[2]
    state = address_segment[3]
    zip = address_segment[4]

    return address_1, address_2, city, state, zip


def get_date_time(time_segment):
    """
    Converts time_segment to UTC using the offset provided in the time_segment
    Example time_segment = 20130903193451+0000
    """
    if time_segment.strip() == '':
        return ''

    if '-' in time_segment:
        date_time = datetime.datetime.strptime(time_segment[:time_segment.find('-')], '%Y%m%d%H%M%S')
        offset = datetime.datetime.strptime(time_segment.split('-')[1].strip(), '%H%S')
        date_time = date_time - datetime.timedelta(hours=offset.hour, minutes=offset.minute)
    elif '+' in time_segment:  # +
        date_time = datetime.datetime.strptime(time_segment[:time_segment.find('+')], '%Y%m%d%H%M%S')
        offset = datetime.datetime.strptime(time_segment.split('+')[1].strip(), '%H%S')
        date_time = date_time + datetime.timedelta(hours=offset.hour, minutes=offset.minute)
    return date_time


def download_directory(s3, bucket, path):
    """
    Downloads recursively the given S3 path
    :param bucket: the name of the bucket to download from
    :param path: The S3 directory to download.
    """

    # Handle missing / at end of prefix
    if not path.endswith('/'):
        path += '/'

    hl7_files_dict = {}
    paginator = s3.get_paginator('list_objects_v2')
    for result in paginator.paginate(Bucket=bucket, Prefix=path):
        # Download each file individually
        for key in result['Contents']:
            # Skip paths ending in /
            # Skip hl7_messages_processed and hl7_messages_error directories
            # Key - Mirth/Integrations/Orders Inbound/TENANT_NAME/hl7_messages/hl7_message.hl7
            if not key['Key'].endswith('/') and 'hl7_messages_processed' not in key['Key'] \
                 and 'hl7_messages_errors' not in key['Key']:
                tenant_name = key['Key'].split('/')[-3]  # get tenant name - 3rd last
                with tempfile.NamedTemporaryFile() as temp:
                    # download source data from s3
                    s3.download_file(BUCKET_NAME, key['Key'], temp.name)
                    print("Downloaded {}".format(key['Key']))
                    hl7_file = open(temp.name)
                    try:
                        hl7_files_dict[tenant_name].append(temp.name, hl7_file.readlines())
                    except KeyError:
                        hl7_files_dict[tenant_name] = []
                        hl7_files_dict[tenant_name].append(temp.name, hl7_file.readlines())

    return hl7_files_dict


def parse_orders_in_hl7(hl7_file):
    order_create_data = {}
    requested_test_panels = []
    diagnosis_codes = []
    notes = []
    for line in hl7_file:
        if line.startswith('MSH'):
            msh = parse_msh(line)
            order_create_data.update(msh)
        elif line.startswith('PID'):
            pid = parse_pid(line)
            order_create_data.update(pid)
        elif line.startswith('IN1'):
            in1 = parse_in1(line)
            order_create_data.update(in1)
        elif line.startswith('GT1'):
            gt1 = parse_gt1(line)
            order_create_data.update(gt1)
        elif line.startswith('ORC'):  # may be more than one provider per order - test with PF data
            orc = parse_orc(line)
            order_create_data.update(orc)
        elif line.startswith('TQ1'):
            parse_tq1(line)
        elif line.startswith('OBR'):
            requested_test_panel = parse_obr(line)
            requested_test_panels.append(requested_test_panel)
        elif line.startswith('PV1'):
            order_create_data.update(parse_pv1(line))
        elif line.startswith('NTE'):
            notes.append(parse_nte(line))
        elif line.startswith('DG1'):
            diagnosis_codes.append(parse_dg1(line))
        elif line.startswith('OBX'):
            parse_obx(line)
        elif line.startswith('SPM'):
            order_create_data.update(parse_spm(line))

    order_create_data['requested_test_panels'] = requested_test_panels
    order_create_data['diagnosis_codes'] = diagnosis_codes
    order_create_data['notes'] = notes

    return order_create_data


def get_orders_in_hl7():
    """
    Calls download_directory to download hl7 files in the tenants Orders In directory and passed them to
    parse_orders_in_hl7 for parsing into segments. Then calls create_order to create the orders
    :return:
    """
    print('called orders in hl7')
    # s3 = boto3.client(
    #     's3',
    #     aws_access_key_id=os.environ.get('S3_ACCESS_KEY'),
    #     aws_secret_access_key=os.environ.get('S3_SECRET_KEY'))
    #
    # hl7_files_dict = download_directory(s3, BUCKET_NAME, ORDERS_IN_DIRECTORY)

    # for tenant_name in hl7_files_dict:
    #     # tenant = clients_models.Client.objects.filter(name=tenant_name).first()
    #     tenant = clients_models.Client.objects.get(schema_name='ildptest')  # Change to above for prod
    #     with tenant_context(tenant):
    #         for hl7_file_name, hl7_file in hl7_files_dict[tenant_name]:
    #             order_create_data_dict = parse_orders_in_hl7(hl7_file)
    #             order_create_data_dict['tenant_id'] = tenant.id
    #
    #             order_created, order_code = create_order(order_create_data_dict)
    #             move_processed_hl7_files(hl7_file_name, s3, order_created, tenant_name, order_code)


def move_processed_hl7_files(hl7_file_name, s3, order_created, tenant_name, order_code):
    """
    Moves processed hl7 files from the hl7_messages directory to either the hl7_messages_processed or
    hl7_messages_errors directories depending on whether the order was successfully created
    :param hl7_file_name:
    :param s3: s3 client
    :param order_created: True or False depending on whether there was an error during order creation
    :param tenant_name:
    :param order_code:
    :return:
    """
    copy_source = {'Bucket': BUCKET_NAME, 'Key': '{}/{}/{}/{}'.format(ORDERS_IN_DIRECTORY, tenant_name, 'hl7_messages',
                                                                      hl7_file_name)}
    if order_created:
        s3.copy_object(CopySource=copy_source, Bucket=BUCKET_NAME, Key='{}/{}/{}/{}{}'
                       .format(ORDERS_IN_DIRECTORY, tenant_name, 'hl7_messages_processed', order_code, hl7_file_name))
    else:
        s3.copy_object(CopySource=copy_source, Bucket=BUCKET_NAME, Key='{}/{}/{}/{}{}'
                       .format(ORDERS_IN_DIRECTORY, tenant_name, 'hl7_messages_errors', order_code, hl7_file_name))

    # Delete original file from hl7_messages folder
    s3.delete_object(Bucket=ORDERS_IN_DIRECTORY, Key='{}/{}/{}/{}{}'
                     .format(ORDERS_IN_DIRECTORY, tenant_name, 'hl7_messages', order_code, hl7_file_name))


def parse_msh(msh):
    """
    Message Header Segment - MSH
    Mandatory: 3. Sending Application,
    4. Sending Facility (unique value assigned to clinic/provider),
    7. Date/Time of Message, 10. Message Control ID (for ack),
    15. Accept Acknowledgement Type, 16. Application Acknowledgement Type,

    Index - 1 for offset
    """

    msh = msh.split('|')
    sending_application = msh[2]
    sending_facility = msh[3]  # clinic/provider ID - request it to be clinic to use for account retrieval
    message_date_time = get_date_time(msh[6])  # order create date?
    message_control_id = msh[9]

    return {'client_alternate_id': sending_facility,
            'sending_application': sending_application,
            'message_date_time': message_date_time,
            'message_control_id': message_control_id}


def parse_pid(pid):
    """
    Patient Identification Segment - PID
    Mandatory: 3. Patient Identifier List (Patient ID (Internal ID?) 5. Patient Name, 7. DOB, 8. Sex, 11. Address
    Optional: 13. Phone #
    Maximum of one per order
    """
    pid = pid.split('|')
    patient_alternate_id = pid[3].split('^')[0]  # first segment is alternate ID
    patient_name = pid[5]
    patient_last_name, patient_first_name, patient_middle_initial = get_name(patient_name)

    patient_dob_string = pid[7]
    patient_birth_date = datetime.datetime.strptime(patient_dob_string, "%Y%m%d").date()
    patient_sex = pid[8]

    patient_address = pid[11]
    patient_address1, patient_address2, patient_city, patient_state, patient_zip = get_address(patient_address)

    patient_contact_information = pid[13].split('^')
    patient_email = patient_contact_information[3]
    patient_phone_number = patient_contact_information[5] + patient_contact_information[6].replace('~',
                                                                                                   '')  # check whether its (.5 and .6) or (13 and 14)

    return {'patient_alternate_id': patient_alternate_id,
            'patient_first_name': patient_first_name,
            'patient_last_name': patient_last_name,
            'patient_middle_initial': patient_middle_initial,
            'patient_birth_date': patient_birth_date,
            'patient_sex': patient_sex,
            'patient_address1': patient_address1,
            'patient_address2': patient_address2,
            'patient_city': patient_city,
            'patient_state': patient_state,
            'patient_zip': patient_zip,
            'patient_email': patient_email,
            'patient_phone_number': patient_phone_number}


def parse_pv1(pv1):
    """
    Patient Visit Segment - PV1
    Mandatory Fields: 20. Financial Class 'T’ (Third-party billing), ‘C’ (Client Billing), or ‘P’ (Patient billing).
    """
    pv1 = pv1.split('|')
    financial_class = pv1[20]  # if T, then in1 is included

    return {'financial_class': financial_class}


def parse_in1(in1):
    """
    Insurance Information - IN1
    Is conditional when PV1.20 is T (Third Party)
    Max of 2 are included for PracticeFusion

    Mandatory Fields: 1. Set ID, 2. Insurance Plan ID, 3. Insurance Company ID,
    4. Insurance Company Name, 5. Insurance Company Address, 16. Name of Insured,
    17. Insured’s Relationship to Patient (Self, Dependent, Spouse),
            Self—SEL^Self^HL70063
            Child—DEP^Dependent^HL70063
            Other—DEP^Dependent^HL70063
            Spouse—SPO^Spouse^HL70063
    18. Insured’s Date of Birth ,
    19. Insured’s Address, 36. Policy Number
    Optional Fields: 7. Insurance Co Phone Number, 8. Group Number, 11. Insured’s Group Emp Name,

    """

    in1 = in1.split('|')
    try:
        set_id = int(in1[1])
    except ValueError:
        print("Error")
        set_id = 1  # error in set ID, just set it to primary insurance

    insurance_plan_id = in1[2]  # other, so..??medicare, medicaid?? - patient_payer_type in dict, convert to medicare, medicaid, commercial
    insurance_company_id = in1[3]  # unique and maps to PF global payer list
    # if new value is provided, id is autogenerated
    insurance_company_name = in1[4]

    insurance_company_address = in1[5]
    insurance_company_address1, insurance_company_address2, insurance_company_city, insurance_company_state, insurance_company_zip = get_address(
        insurance_company_address)
    group_number = in1[8]
    insureds_group_employer_name = in1[11]

    subscriber_name = in1[16]
    subscriber_last_name, subscriber_first_name, subscriber_middle_initial = get_name(subscriber_name)
    relationship_to_patient = in1[17].split('^')[1]
    subscriber_dob_string = in1[18]
    subscriber_dob = datetime.datetime.strptime(subscriber_dob_string, "%Y%m%d").date()

    subsciber_address = in1[19]
    subscriber_address1, subscriber_address2, subscriber_city, subscriber_state, subscriber_zip = get_address(
        subsciber_address)

    policy_number = in1[36]  # member ID

    if set_id == 1:  # primary patient payer
        patient_primary_payer = {'primary_patient_payer_type': insurance_plan_id,
                                 'primary_payer_id': insurance_company_id,
                                 'primary_payer_name': insurance_company_name,
                                 'primary_payer_address1': insurance_company_address1,
                                 'primary_payer_address2': insurance_company_address2,
                                 'primary_group_number': group_number,
                                 'primary_policy_number': policy_number,
                                 'primary_subscriber_last_name': subscriber_last_name,
                                 'primary_subscriber_first_name': subscriber_first_name,
                                 'primary_subscriber_middle_initial': subscriber_middle_initial,
                                 'primary_subscriber_birth_date': subscriber_dob,
                                 'primary_subscriber_relationship': relationship_to_patient,
                                 'primary_subscriber_address1': subscriber_address1,
                                 'primary_subscriber_address2': subscriber_address2,
                                 'primary_subscriber_city': subscriber_city,
                                 'primary_subscriber_state': subscriber_state,
                                 'primary_subsciber_zip': subscriber_zip}
        return patient_primary_payer

    elif set_id == 2: # secondary patient payer
        patient_secondary_payer = {'secondary_patient_payer_type': insurance_plan_id,
                                 'secondary_payer_id': insurance_company_id,
                                 'secondary_payer_name': insurance_company_name,
                                 'secondary_payer_address1': insurance_company_address1,
                                 'secondary_payer_address2': insurance_company_address2,
                                 'secondary_group_number': group_number,
                                 'secondary_policy_number': policy_number,
                                 'secondary_subscriber_last_name': subscriber_last_name,
                                 'secondary_subscriber_first_name': subscriber_first_name,
                                 'secondary_subscriber_middle_initial': subscriber_middle_initial,
                                 'secondary_subscriber_birth_date': subscriber_dob,
                                 'secondary_subscriber_relationship': relationship_to_patient,
                                 'secondary_subscriber_address1': subscriber_address1,
                                 'secondary_subscriber_address2': subscriber_address2,
                                 'secondary_subscriber_city': subscriber_city,
                                 'secondary_subscriber_state': subscriber_state,
                                 'secondary_subsciber_zip': subscriber_zip}
        return patient_secondary_payer



def parse_gt1(gt1):
    """
    GT1 - Guarantor Segment
    Guarantor Information - appears once.
    For PracticeFusion, GT1 contains the patient's info, unless the practice
    provides a different person as the guarantor
    Mandatory Fields: 3. Guarantor Name, 5. Guarantor Address, 10. Guarantor Type (P, S, D, O??? not provided in PF doc)
    """
    gt1 = gt1.split('|')

    guarantor_name = gt1[3]
    guarantor_last_name, guarantor_first_name, guarantor_middle_initial = get_name(guarantor_name)

    guarantor_address = gt1[5]
    guarantor_address1, guarantor_address2, guarantor_city, guarantor_state, guarantor_zip = get_address(
        guarantor_address)
    guarantor_type_string = gt1[10].strip()

    if guarantor_type_string == 'P':
        guarantor_relationship_to_patient = 'Parent'
    elif guarantor_type_string == 'S':
        guarantor_relationship_to_patient = 'Spouse'
    elif guarantor_type_string == 'D':
        guarantor_relationship_to_patient = 'Dependant'
    elif guarantor_type_string == 'O':
        guarantor_relationship_to_patient = 'Other'


    return {'guarantor_first_name': guarantor_first_name,
            'guarantor_last_name': guarantor_last_name,
            'guarantor_middle_initial': guarantor_middle_initial,
            'guarantor_address1': guarantor_address1,
            'guarantor_address2': guarantor_address2,
            'guarantor_city': guarantor_city,
            'guarantor_state': guarantor_state,
            'guarantor_zip': guarantor_zip,
            'guarantor_relationship_to_patient': guarantor_relationship_to_patient}

def parse_orc(orc):
    """
    ORC - Order Common
    ORC segment accompanies each OBR segment.
    Appears once for each test placed in an order

    Mandatory Fields: 1. Order Control, 2. Placer Order Number, 4. Placer Group Number,
    12. Ordering Provider, 24. Ordering Provider Address

    Optional Fields: 14. Call Back Phone Number

    ORC.2 and ORC.4 not required but recommended to be used to populate the ORC.2
    and ORC.4 of the result message returned to client
    """
    ## TO-DO - Take care or order with 2 providers case????
    orc = orc.split('|')

    order_control = orc[1]
    placer_order_number = orc[2]
    placer_group_number = orc[4]

    provider_information = orc[12].split('^')
    provider_npi = provider_information[0]  # if NPI not provided, random GUID provided
    provider_last_name = provider_information[1]
    provider_first_name = provider_information[2]

    provider_phone_number = "".join(orc[14].split('^'))

    provider_address = orc[24]
    provider_address1, provider_address2, provider_city, provider_state, provider_zip = get_address(provider_address)

    return {'placer_order_number': placer_order_number,
            'placer_group_number': placer_group_number,
            'provider_npi': provider_npi,
            'provider_last_name': provider_last_name,
            'provider_first_name': provider_first_name,
            'provider_phone_number': provider_phone_number,
            'provider_address1': provider_address1,
            'provider_address2': provider_address2,
            'provider_city': provider_city,
            'provider_state': provider_state,
            'provider_zip': provider_zip}

def parse_tq1(tq1):
    """
    Timing/Quanity
    Used only to indicate when the order is a STAT order or a future order. Ommitted
    when the order is not STAT.
    Mandatory Fields: 9. Priority (S)
    Not necessary for Dendi
    """
    pass


def parse_obr(obr):
    """
    Observation Request - OBR
    Mandatory Fields: 1. Set ID, 2. Placer Order Number, 4. Universal Service Identifier,
    20. Filler Field 1

    Optional Fields: 7. Observation Date/Time

    OBR.2 and OBR.4 recommended to be sent with result message
    """
    obr = obr.split('|')

    set_id = obr[1]
    placer_order_number = obr[2]  # alternate ID
    universal_service_identifier_text = obr[4].split('^')[1]  # PF test_panel ID?
    universal_service_identifier = obr[4].split('^')[2]  # PF test_panel name?

    specimen_action_code = obr[11]
    filler_field = obr[20]

    return {'test_panel_id': universal_service_identifier,
            'test_panel_name': universal_service_identifier_text}


def parse_nte(nte):
    """
    Used to carry notes and comments from a provider regarding
    test orders
    """
    nte = nte.split('|')
    comment = nte[3]

    return {'Comment': comment}



def parse_dg1(dg1):
    """
    Diagnosis Segment - DG1
    Contains diagnoses associated with a test (required for each test).
    May appear one or more times for each OBR segment
    Mandatory Fields: 1. Set ID, 3. Diagnosis Code, 6. Diagnosis Type
    """

    dg1 = dg1.split('|')
    set_id = dg1[1]
    diagnosis_code_segment = dg1[3].split('^')
    diagnosis_code = diagnosis_code_segment[0]
    diagnosis_code_text = diagnosis_code_segment[1]

    return {'diagnosis_code': diagnosis_code,
            'diagnosis_code_text': diagnosis_code_text}


def parse_obx(obx):
    """
    Observation/Result Segment - OBX
    Not necessary for order creation on Dendi
    """
    pass


def parse_spm(spm):
    """
    Specimen Segment - SPM
    Contains information about specimens.
    Is optional and multiple SPM segments may be sent related to a single test
    order (OBR - order segment group)
    Mandatory Fields: 1. Set ID, 4. Specimen Type, 17. Specimen Collection Date/Time,
    26. Number of Specimen Containers
    14. Optional Fields: Specimen Description
    """
    spm = spm.split('|')
    set_id = spm[1]
    specimen_type = spm[4]
    specimen_description = spm[14]
    specimen_collection_date_time = get_date_time(spm[17])
    try:
        number_of_specimens = int(spm[26])
    except ValueError:
        number_of_specimens = 1 if len(specimen_type.strip()) > 0 else 0  # assume 1 sample if SPM.26 not provided


    return {'specimen_type': specimen_type,
            'specimen_description': specimen_description,
            'specimen_collection_date_time': specimen_collection_date_time,
            'number_of_specimens': number_of_specimens}

def ack_message(sending_application, client_id, vendor_code, message_control_id, order_uuid, hl7_version):
    time_received = datetime.datetime.now()  # convert to hl7 format in UTC +0000
    dendi_message_control_id = order_uuid
    ack_msh = """MSH|^~\&|{}|{}||{}|{}||ACK^ELINCS^ACK_ELINCS|{}|P|{}|||||||||ELINCS_MT-ACK-1_1.0\r""".format(
        sending_application,
        vendor_code,
        client_id,
        time_received,
        dendi_message_control_id,
        hl7_version
    )

    ack_msa = """MSA|CA|{}\r""".format(message_control_id)

"""
No new accounts allowed. Lab should create them first in LIS"""
def create_order(order_create_data):
    """

    :param order_create_data:
    :return:
    """

    tenant = clients_models.Client.objects.get(id=order_create_data['tenant_id'])
    # create order
    order_result = tasks.create_order_with_code.apply_async(args=[order_create_data['tenant_id']], queue='identifiers')
    order_id = order_result.get()

    order = models.Order.objects.get(id=order_id)

    if order_create_data['client_alternate_id']:
        account = accounts_models.Account.objects.get(alternate_id=order_create_data['client_alternate_id'])
    else:
        account = None
        raise Exception("Client with alternate ID {} not found".format(order_create_data['client_alternate_id']))

    #
    if order_create_data['provider_npi']:
        provider = accounts_models.Provider.objects.get(npi=order_create_data['provider_npi'])
        print("provider is {}".format(provider))
    else:
        # if provider is missing do not create - move to error queue???????
        # create User object before creating Provider, since provider has a username
        # provider_uuid = uuid.uuid4()
        # lab_schema = tenant.schema_name
        # provider_email = str(provider_uuid) + '@' + lab_schema + '.com'
        #
        # user_obj = User(first_name=order_create_data['provider_first_name'],
        #                 last_name=order_create_data['provider_last_name'],
        #                 username=provider_email,
        #                 email=provider_email,
        #                 user_type=10)
        # user_obj.set_password(str(provider_uuid))
        # user_obj.save()
        #
        # provider = accounts_models.Provider(
        #     user=user_obj,
        #     npi=order_create_data['provider_npi'],
        #     uuid=provider_uuid,
        # )
        # provider.save()
        # provider.accounts.add(account)
        print("Error, provider does not exist")
    #
    if order_create_data['patient_alternate_id']:
        print(order_create_data['patient_alternate_id'])
        patient = patients_models.Patient.objects.get(alternate_id=order_create_data['patient_alternate_id'])
        print("Patient is {}".format(patient))
    else:
        patient = None
    #
    # if not patient:
    #     # generate random string for username and password initially
    #     user_uuid = str(uuid.uuid4())
    #     username = order_create_data['patient_first_name'] + '-' + order_create_data[
    #         'patient_last_name'] + '-' + user_uuid
    #
    #     # create User object
    #     user_obj = User(first_name=order_create_data['patient_first_name'],
    #                     last_name=order_create_data['patient_last_name'],
    #                     username=username,
    #                     user_type=20)
    #     if order_create_data['patient_email']:
    #         user_obj.email = order_create_data['patient_email']
    #     user_obj.set_password(user_uuid)
    #     # create User object first before creating Patient
    #     user_obj.save()
    #
    #     patient = patients_models.Patient(user=user_obj,
    #                                       middle_initial=order_create_data['patient_middle_initial'],
    #                                       sex=order_create_data['patient_sex'],
    #                                       birth_date=order_create_data['patient_birth_date'],
    #                                       address1=order_create_data['patient_address_1'],
    #                                       address2=order_create_data['patient_address_2'],
    #                                       phone_number=order_create_data['patient_phone_number'])
    #     patient.save()
    #
    # ############################################################################################################
    # # patient_payer_cleaned_data must not be None (otherwise, this section should be skipped)
    if order_create_data.get('primary_payer_id'):  # patient has primary payer
        # primary payer
        try:
            payer = patients_models.Payer.objects.get(payer_code=order_create_data['primary_payer_id'])
        except patients_models.Payer.DoesNotExist:  # payer does not exist. Create payer??
            # payer = patients_models.Payer.objects.create(name=order_create_data.get('primary_payer_name'),
            #                                              payer_type=order_create_data.get(
            #                                                  'patient_payer_type'),
            #                                              payer_code=order_create_data['primary_payer_id'],
            #                                              address_1=order_create_data['primary_payer_address1'],
            #                                              address_2=order_create_data['primary_payer_address2'])
            # MOVE TO payer clean up workflow
            payer = None
        primary_patient_payer = patients_models.PatientPayer.objects.filter(is_primary=True,
                                                                            patient=patient)[0]
        if len(primary_patient_payer) == 0:  # patient does not have a payer assigned
            primary_patient_payer, created = patients_models.PatientPayer.objects.update_or_create(
                is_primary=True,
                patient=patient,
                defaults={'payer_type': order_create_data['primary_patient_payer_type'],
                          # subscriber info
                          'subscriber_last_name': order_create_data['primary_subscriber_last_name'],
                          'subscriber_first_name': order_create_data['primary_subscriber_first_name'],
                          'subscriber_middle_initial': order_create_data['primary_subscriber_middle_initial'],
                          'subscriber_address1': order_create_data['primary_subscriber_address1'],
                          'subscriber_address2': order_create_data['primary_subscriber_address2'],
                          'subscriber_birth_date': order_create_data['primary_subscriber_birth_date'],
                          'subscriber_relationship': order_create_data['primary_subscriber_relationship'],
                          # insurance details
                          'member_id': order_create_data['primary_policy_number'],
                          'group_number': order_create_data['primary_group_number']},
            )
    #
    #     ########################################################################################################
    #     # secondary payer
        if order_create_data.get('secondary_payer_id'):  # patient has secondary payer
            # secondary payer
            try:
                payer = patients_models.Payer.objects.get(payer_code=order_create_data['secondary_payer_id'])
            except patients_models.Payer.DoesNotExist:  # payer does not exist. Create payer
                # Move to payer error workflow
                pass
            secondary_patient_payer = patients_models.PatientPayer.objects.filter(is_secondary=True,
                                                                    patient=patient)[0]
            if len(secondary_patient_payer) == 0:  # patient does not have a payer assigned
                secondary_patient_payer, created = patients_models.PatientPayer.objects.update_or_create(
                    is_secondary=True,
                    patient=patient,
                    defaults={'payer_type': order_create_data['secondary_patient_payer_type'],
                              # subscriber info
                              'subscriber_last_name': order_create_data['secondary_subscriber_last_name'],
                              'subscriber_first_name': order_create_data['secondary_subscriber_first_name'],
                              'subscriber_middle_initial': order_create_data['secondary_subscriber_middle_initial'],
                              'subscriber_address1': order_create_data['secondary_subscriber_address1'],
                              'subscriber_address2': order_create_data['secondary_subscriber_address2'],
                              'subscriber_birth_date': order_create_data['secondary_subscriber_birth_date'],
                              'subscriber_relationship': order_create_data['secondary_subscriber_relationship'],
                              # insurance details
                              'member_id': order_create_data['secondary_policy_number'],
                              'group_number': order_create_data['secondary_group_number']},
                )

    #     ########################################################################################################
    #     # guarantor
        if order_create_data.get('patient_guarantor_first_name'):
            patient_guarantor_first_name = order_create_data.get('guarantor_first_name')
            patient_guarantor_last_name = order_create_data.get('guarantor_last_name')

            try:
                patient_guarantor = patients_models.PatientGuarantor.objects \
                    .filter(patient=patient, patient_guarantor_first_name__icontains=patient_guarantor_first_name,
                            patient_guarantor_lasst_name__icontains=patient_guarantor_last_name) \
                    .latest('modified_date')
            except patients_models.PatientGuarantor.DoesNotExist:
                # generate random string for username and password initially
                user_uuid = str(uuid.uuid4())
                username = user_uuid

                # create User object
                user_obj = User(first_name=patient_guarantor_first_name,
                                last_name=patient_guarantor_last_name,
                                username=username,
                                user_type=19)
                user_obj.set_password(user_uuid)
                # create User object first before creating PatientGuarantor
                user_obj.save()

                patient_guarantor, created = patients_models.PatientGuarantor.objects.update_or_create(
                    patient=patient,
                    defaults={'user': user_obj,
                              'relationship_to_patient': order_create_data['guarantor_relationship_to_patient'],
                              'middle_initial': order_create_data['guarantor_middle_initial'],
                              'address1': order_create_data['guarantor_address1'],
                              'address2': order_create_data['guarantor_address2']}
                )
    #
    # # add provider to patient.providers manually (many-to-many)
    if provider and provider not in patient.providers.all():
        try:
            patient.providers.add(provider)
        except:
            pass

    #     # assign objects to order
    #     # must do this in order to preserve order status
    order.provider = provider
    order.account = account
    order.patient = patient
    order.save()
    #
    # # create foreign key objects relating to Order
    if primary_patient_payer:
        models.OrderPatientPayer.objects.create(order=order,
                                                patient=patient,
                                                is_primary=True,
                                                payer=primary_patient_payer.payer,
                                                payer_type=primary_patient_payer.payer_type,
                                                group_number=primary_patient_payer.group_number,
                                                member_id=primary_patient_payer.member_id,
                                                subscriber_last_name=primary_patient_payer.subscriber_last_name,
                                                subscriber_first_name=primary_patient_payer.subscriber_first_name,
                                                subscriber_middle_initial=primary_patient_payer.subscriber_middle_initial,
                                                subscriber_suffix=primary_patient_payer.subscriber_suffix,
                                                subscriber_birth_date=primary_patient_payer.subscriber_birth_date,
                                                subscriber_address1=primary_patient_payer.subscriber_address1,
                                                subscriber_address2=primary_patient_payer.subscriber_address2,
                                                subscriber_sex=primary_patient_payer.subscriber_sex,
                                                subscriber_phone_number=primary_patient_payer.subscriber_phone_number,
                                                subscriber_social_security=primary_patient_payer.subscriber_social_security,
                                                subscriber_relationship=primary_patient_payer.subscriber_relationship)
    if secondary_patient_payer:
        models.OrderPatientPayer.objects.create(order=order,
                                                patient=patient,
                                                is_secondary=True,
                                                payer=secondary_patient_payer.payer,
                                                payer_type=secondary_patient_payer.payer_type,
                                                group_number=secondary_patient_payer.group_number,
                                                member_id=secondary_patient_payer.member_id,
                                                subscriber_last_name=secondary_patient_payer.subscriber_last_name,
                                                subscriber_first_name=secondary_patient_payer.subscriber_first_name,
                                                subscriber_middle_initial=secondary_patient_payer.subscriber_middle_initial,
                                                subscriber_suffix=secondary_patient_payer.subscriber_suffix,
                                                subscriber_birth_date=secondary_patient_payer.subscriber_birth_date,
                                                subscriber_address1=secondary_patient_payer.subscriber_address1,
                                                subscriber_address2=secondary_patient_payer.subscriber_address2,
                                                subscriber_sex=secondary_patient_payer.subscriber_sex,
                                                subscriber_phone_number=secondary_patient_payer.subscriber_phone_number,
                                                subscriber_social_security=secondary_patient_payer.subscriber_social_security,
                                                subscriber_relationship=secondary_patient_payer.subscriber_relationship)

    if patient_guarantor:
        models.OrderPatientGuarantor.objects.create(order=order,
                                                    patient=patient,
                                                    user=patient_guarantor.user,
                                                    relationship_to_patient=patient_guarantor.relationship_to_patient,
                                                    suffix=patient_guarantor.suffix,
                                                    middle_initial=patient_guarantor.middle_initial,
                                                    sex=patient_guarantor.sex,
                                                    birth_date=patient_guarantor.birth_date,
                                                    address1=patient_guarantor.address1,
                                                    address2=patient_guarantor.address2,
                                                    phone_number=patient_guarantor.phone_number,
                                                    social_security=patient_guarantor.social_security)


    # pick up from here - adding samples to order
    # # add samples
    order_samples = []
    # number_of_samples = order_create_data['number_of_samples']
    # for sample in number_of_samples:
    #     sample_type = order_create_data['specimen_type']
    #     collection_date = order_create_data['specimen_collection_date_time']
    #
    #     if sample_type and collection_date:
    #         latest_sample_code = generate_sample_code(order.code, sample_type)
    #         sample = models.Sample.objects.create(
    #             order=order,
    #             code=latest_sample_code,
    #             clia_sample_type=sample_type,
    #             collection_date=collection_date,
    #             draw_type=order_create_data['specimen_type']
    #         )
    #
    #         sample.barcode = get_sample_barcode(sample.id)
    #         sample.save(update_fields=['barcode'])
    #         order_samples.append(sample)
    #
    # if order_create_data['icd10_codes']:
    #     diagnosis_codes = order_create_data['diagnosis_codes']
    #     icd10_codes = []
    #     for diagnosis_code in diagnosis_codes:
    #         try:  # retrieve icd10 codes from DB and add to order icd10 code list
    #             icd10_code = models.ICD10CM.objects.get(diagnosis_code=diagnosis_code)
    #             icd10_codes.append(icd10_code)
    #         except models.ICD10CM.DoesNotExist:
    #             pass  # code does not exist, log error
    #
    #     order.icd10_codes.set(icd10_codes)
    #
    # # add requested services to order
    # order_samples = order.samples.all().prefetch_related('clia_sample_type')
    # requested_test_panels = order_create_data['requested_test_panels']  # requested_test_panels dict
    #
    # result_list = []
    # enable_individual_analytes = tenant.settings.get('enable_individual_analytes', False)
    # for test_panel_type_id in requested_test_panels:
    #     test_panel_type = models.TestPanelType.objects.prefetch_related('test_types').get(id=test_panel_type_id)
    #     if enable_individual_analytes:
    #         # try:
    #         #     test_types = models.TestType.objects.filter(
    #         #         alternate_id__in=
    #         #     ).all()
    #         #     if not test_types:
    #         #         continue
    #         # except KeyError:
    #         #     continue
    #         pass  # individual analytes not implemented
    #     else:
    #         test_types = test_panel_type.test_types.all()
    #
    #     test_panel = models.TestPanel.objects.create(
    #         order=order,
    #         test_panel_type=test_panel_type,
    #         expected_revenue=test_panel_type.cost
    #     )
    #     for test_type in test_types:
    #         required_samples = test_type.required_samples
    #         if required_samples:
    #             for required_sample_type_id in required_samples:
    #                 samples = order_samples.filter(clia_sample_type__id=required_sample_type_id).order_by(
    #                     'code')[
    #                           :required_samples[required_sample_type_id]]
    #                 for sample in samples:
    #                     result_list.append((order, sample, test_panel, test_type))
    #         else:
    #             for sample in order_samples:
    #                 result_list.append((order, sample, test_panel, test_type))
    # add_bulk_results(result_list)
    #
    # # finalize order by setting submitted=True
    # order.submitted = True
    # order.submitted_date = timezone.now()
    # order.save(update_fields=['submitted'])
    #
    # # create hl7 message
    # orders_out = generate_hl7.get_order_out_message(order.id, tenant.id)
    # if orders_out:
    #     for offsite_laboratory_id in orders_out.keys():
    #         lab_models.OrdersOut.objects.create(tenant_id=tenant.id,
    #                                             uuid=orders_out[offsite_laboratory_id]['uuid'],
    #                                             offsite_laboratory_id=int(offsite_laboratory_id),
    #                                             hl7_message=orders_out[offsite_laboratory_id]['message'])
    # # generate the order requisition form
    # generate_requisition_form.generate_requisition_PDF(request, order.uuid)


    # if error with the order, return False

    if 1==0:  # if order creation runs into an error, return False - change to error
        return False, order.code
    return True, order.code


if __name__ == '__main__':
    """Call function to get all the hl7 orders in messages"""
    get_orders_in_hl7()