import datetime
import decimal
import django

import logging
import os
import random
import sys
import timeit
import csv
import requests
import re
import uuid

from django.db import transaction
from django.utils import timezone
from django.core.exceptions import ObjectDoesNotExist
from tenant_schemas.utils import tenant_context

os.environ['DJANGO_SETTINGS_MODULE'] = 'main.settings'
django.setup()

from clients import models as client_models
from orders import models as orders_models
from accounts import models as accounts_models
from lab import models as lab_models
from lab_tenant import models as lab_tenant_models
from patients import models as patients_models
from orders.functions import generate_order_code, generate_sample_code, get_sample_barcode

# import dummy data
from demos.shared_data.shared_data import INSTRUMENTS, TEST_TARGETS
from demos.dummy_data.common import ADDITIONAL_USERS, PROVIDERS, ACCOUNTS, PATIENTS
from demos.dummy_data.toxicology import CONTROLS, DATA_IN_LIST, INSTRUMENT_INTEGRATIONS, TEST_TYPES, \
    TEST_PANEL_GROUPS, SPECIMEN_VALIDITY_TEST_TARGETS, TEST_TYPE_RANGES, SCREENING_TEST_TYPES, POC_TEST_TYPES, \
    ORAL_TEST_TYPES

from demos.initialization_functions import create_providers, create_accounts, create_users, create_instruments, \
    create_instrument_integrations, create_patients, create_controls, create_data_in_demo, create_test_types, \
    create_test_targets, increment_schema_name, create_test_type_ranges

from tasks import initialize_client

"""
Script to set up a new schema for demo purposes
"""

DEMO = True
NUM_ORDERS = 0

# schema variables
SCHEMA_NAME = 'mako'
LABORATORY_NAME = 'Mako Medical'
LABORATORY_TIMEZONE = 'US/Eastern'
EMAIL = 'sjung@dendisoftware.com'
FIRST_NAME = 'Simon'
LAST_NAME = 'Jung'
PASSWORD = 'dendipower123123'
SCHEMA_CONTEXT = {'client_name': LABORATORY_NAME,
                  'schema_name': SCHEMA_NAME,
                  'email': EMAIL,
                  'first_name': FIRST_NAME,
                  'last_name': LAST_NAME,
                  'password': PASSWORD,
                  'lab_type': '',
                  'address_1': '8461 Garvey Dr',
                  'address_2': 'Raleigh, NC 27616',
                  'phone_number': '(844) 625-6522',
                  'fax_number': '(919) 390-3059',
                  'clia_number': '34D2082106',
                  'clia_director': 'Danielle Gibson, MD',
                  'logo_file': 'mako_logo.png',
                  'website': 'www.makomedical.com'
                  }

SCHEMA_SETTINGS = {"require_icd10": True,
                   "skip_patient_history": False,
                   'show_prescribed_medication': True,
                   'use_docraptor': True,
                   'hide_fda_tests': True, }

ACCOUNTS_CSV_URL = 'https://dendi-lis.s3.amazonaws.com/client_data/ildp/account_list.csv'
PROVIDERS_CSV_URL = 'https://dendi-lis.s3.amazonaws.com/client_data/ildp/provider_list.csv'
PAYERS_CSV_URL = 'https://dendi-lis.s3.amazonaws.com/client_data/ildp/labgen_payer_list.csv'
PATIENTS_CSV_URL = 'https://dendi-lis.s3.amazonaws.com/client_data/ildp/patient_list.csv'
TEST_PANELS_CSV_URL = 'https://dendi-lis.s3.amazonaws.com/client_data/ildp/Profile CSV.csv'


@transaction.atomic
def main(demo=DEMO):
    t_start = timeit.default_timer()
    try:
        tenant = client_models.Client.objects.get(schema_name=SCHEMA_CONTEXT['schema_name'])
        logging.debug('{} schema exists'.format(SCHEMA_CONTEXT['schema_name']))
        schema_name = increment_schema_name(SCHEMA_CONTEXT['schema_name'])
        logging.debug('using {} as schema_name'.format(schema_name))
        SCHEMA_CONTEXT['schema_name'] = schema_name
    except client_models.Client.DoesNotExist:
        schema_name = SCHEMA_CONTEXT['schema_name']
        logging.debug('Creating schema: {}'.format(schema_name))
    initialize_client(SCHEMA_CONTEXT)
    tenant = client_models.Client.objects.get(schema_name=SCHEMA_CONTEXT['schema_name'])
    tenant.timezone = LABORATORY_TIMEZONE
    tenant.settings = SCHEMA_SETTINGS
    tenant.save(update_fields=['timezone', 'settings'])

    logging.debug('Creating users')
    t0 = timeit.default_timer()
    with tenant_context(tenant):
        create_users(ADDITIONAL_USERS)
    t1 = timeit.default_timer()
    logging.debug('Setting up schema - {} seconds'.format(round(t1 - t0, 3)))

    logging.debug('Initializing required instruments')
    t0 = timeit.default_timer()
    create_instruments(INSTRUMENTS)
    t1 = timeit.default_timer()
    logging.debug('done - {} seconds'.format(round(t1 - t0, 3)))

    logging.debug('Initializing required test targets')
    create_test_targets(SPECIMEN_VALIDITY_TEST_TARGETS)

    with tenant_context(tenant):
        logging.debug('Initializing instrument integrations')
        t0 = timeit.default_timer()
        create_instrument_integrations(INSTRUMENT_INTEGRATIONS)
        t1 = timeit.default_timer()
        logging.debug('done - {} seconds'.format(round(t1 - t0, 3)))

        # make test types
        logging.debug('Initializing test types')
        t0 = timeit.default_timer()
        create_test_types(TEST_TYPES, create_panel=False, tox=True)
        create_test_types(SCREENING_TEST_TYPES, create_panel=False, tox=True)
        #create_test_types(ORAL_TEST_TYPES, create_panel=False, tox=True)
        initialize_all_inclusive_test_panel_type()
        t1 = timeit.default_timer()
        logging.debug('done - {} seconds'.format(round(t1 - t0, 3)))

        logging.debug("Initializing test type ranges")
        # specimen validity
        create_test_type_ranges(TEST_TYPE_RANGES)
        initialize_test_type_ranges([x for x in TEST_TYPES if x['group'] != 'Specimen Validity'])
        #initialize_test_type_ranges(ORAL_TEST_TYPES)
        initialize_test_type_ranges(SCREENING_TEST_TYPES)

        logging.debug('Initializing instrument calibrations')
        t0 = timeit.default_timer()
        initialize_instrument_calibrations()
        t1 = timeit.default_timer()
        logging.debug('done - {} seconds'.format(round(t1 - t0, 3)))

        ################################################################################################################
        # make accounts first because everything else depends on accounts
        if demo:
            # make account
            logging.debug('Initializing accounts')
            t0 = timeit.default_timer()
            create_accounts(ACCOUNTS)
            t1 = timeit.default_timer()
            logging.debug('done - {} seconds'.format(round(t1 - t0, 3)))

            # make patients
            logging.debug('Initializing dummy patients')
            t0 = timeit.default_timer()
            create_patients(PATIENTS)
            t1 = timeit.default_timer()
            logging.debug('done - {} seconds'.format(round(t1 - t0, 3)))

            # make providers
            logging.debug("Initializing dummy providers")
            t0 = timeit.default_timer()
            create_providers(PROVIDERS)
            t1 = timeit.default_timer()
            logging.debug('done - {} seconds'.format(round(t1 - t0, 3)))

            # make orders
            logging.debug('Initializing {} dummy orders'.format(NUM_ORDERS))
            t0 = timeit.default_timer()
            initialize_dummy_orders(NUM_ORDERS)
            t1 = timeit.default_timer()
            logging.debug('done - {} seconds'.format(round(t1 - t0, 3)))

            # make some in transit orders
            # logging.debug("Initializing in transit orders")
            # initialize_in_transit_orders()

            # make controls
            logging.debug("Initializing controls")
            t0 = timeit.default_timer()
            create_controls(CONTROLS)
            t1 = timeit.default_timer()
            logging.debug('done - {} seconds'.format(round(t1 - t0, 3)))

            # add data-in
            logging.debug("Initializing data in demo")
            t0 = timeit.default_timer()
            create_data_in_demo(DATA_IN_LIST)
            t1 = timeit.default_timer()
            logging.debug('done - {} seconds'.format(round(t1 - t0, 3)))

            # make dummy control observations
            logging.debug("Initializing control observations")
            t0 = timeit.default_timer()
            initialize_dummy_observations()
            t1 = timeit.default_timer()
            t_end = timeit.default_timer()
            logging.debug('done - {} seconds'.format(round(t1 - t0, 3)))
            logging.debug('total: {} seconds'.format(round(t_end - t_start, 3)))
        else:
            logging.debug('Initializing real test panels')
            t69 = timeit.default_timer()
            initialize_test_panels_from_csv(TEST_PANELS_CSV_URL)
            t70 = timeit.default_timer()
            logging.debug('done - {} seconds'.format(round(t69 - t70, 3)))

            logging.debug('Initializing real accounts')
            t0 = timeit.default_timer()
            initialize_accounts()
            t1 = timeit.default_timer()
            logging.debug('done - {} seconds'.format(round(t1 - t0, 3)))

            # make providers second because patients depend on providers and accounts
            logging.debug("Initializing real providers")
            t0 = timeit.default_timer()
            initialize_providers()
            t1 = timeit.default_timer()
            logging.debug('done - {} seconds'.format(round(t1 - t0, 3)))

            # make payers
            logging.debug("Initializing real payers")
            t0 = timeit.default_timer()
            initialize_payers()
            t1 = timeit.default_timer()
            logging.debug('done - {} seconds'.format(round(t1 - t0, 3)))

            # make patients
            logging.debug('Initializing real patients')
            t0 = timeit.default_timer()
            initialize_patients()
            t1 = timeit.default_timer()
            logging.debug('done - {} seconds'.format(round(t1 - t0, 3)))

            # make controls
            logging.debug("Initializing controls")
            t0 = timeit.default_timer()
            create_controls(CONTROLS)
            t1 = timeit.default_timer()
            logging.debug('done - {} seconds'.format(round(t1 - t0, 3)))

            # add data-in
            logging.debug("Initializing data in demo")
            t0 = timeit.default_timer()
            create_data_in_demo(DATA_IN_LIST)
            t1 = timeit.default_timer()
            logging.debug('done - {} seconds'.format(round(t1 - t0, 3)))
    return schema_name


def initialize_test_panel_types(test_panel_groups):
    """
    Create group panel types
    :param test_types:
    :return:
    """
    menu_categories = orders_models.TestPanelCategory.objects.bulk_create(
        [orders_models.TestPanelCategory(name=x) for x in TEST_PANEL_GROUPS]
    )
    test_panel_types = orders_models.TestPanelType.objects.bulk_create(
        [orders_models.TestPanelType(
            name=x[1],
            menu_category=x[0],
            category_order=1
        ) for x in zip(menu_categories, test_panel_groups)]
    )
    return menu_categories, test_panel_types


def initialize_all_inclusive_test_panel_type():
    # All inclusive panel
    test_panel_type, created = orders_models.TestPanelType.objects.update_or_create(
        name='All analyte test',
        menu_category_id=1
    )
    test_types = orders_models.TestType.objects.all()
    for test_type in test_types:
        test_panel_type.test_types.add(test_type)


def initialize_instrument_calibrations():
    ms_instruments = lab_tenant_models.InstrumentIntegration.objects.filter(
        internal_instrument_name__icontains='Shimadzu'
    )
    calibrations = []
    for ms_instrument in ms_instruments:
        ms_test_types = [x for x in TEST_TYPES if x['group'] != 'Specimen Validity']
        for test_type_dict in ms_test_types:
            test_type = orders_models.TestType.objects.get(name=test_type_dict['name'])
            calibration = orders_models.InstrumentCalibration(
                instrument_integration=ms_instrument,
                test_type=test_type,
                target_name=test_type.test_target.name,
                cutoff_value=test_type_dict.get('cutoff')
            )
            calibrations.append(calibration)
    orders_models.InstrumentCalibration.objects.bulk_create(calibrations)


def initialize_test_type_ranges(test_types):
    test_type_ranges = []
    for test_type_dict in test_types:
        test_type = orders_models.TestType.objects.get(name=test_type_dict['name'])
        range_low = test_type_dict.get('cutoff', None)
        critical_high = test_type_dict.get('upper_limit_of_quantification', None)
        if range_low or critical_high:
            test_type_range = orders_models.TestTypeRange(
                test_type=test_type,
                default=True,
                range_low=range_low,
                critical_high=critical_high
            )
            test_type_ranges.append(test_type_range)
    orders_models.TestTypeRange.objects.bulk_create(test_type_ranges)


def initialize_dummy_orders(num_orders):
    urine_sample_type = lab_models.CLIASampleType.objects.get(name='Urine')
    test_panel_type = orders_models.TestPanelType.objects.get(name='All analyte test')

    days_back = max(60, num_orders / 1.5)
    received_date_start = timezone.now() - datetime.timedelta(days=days_back)
    average_order_count = int(num_orders / days_back)

    for order_count in range(num_orders):
        latest_order_code = generate_order_code()
        rand_account = accounts_models.Account.objects.get(id=random.randint(1, 9))
        rand_patient = patients_models.Patient.objects.get(id=random.randint(1, 11))
        logging.debug('Order {}: {} {} '.format(order_count, rand_account.name, rand_patient.user.first_name))
        order = orders_models.Order.objects.create(
            provider_id=random.randint(1, 5),
            account=rand_account,
            patient=rand_patient,
            code=latest_order_code,
            research_consent=False,
            patient_result_request=False,
            received_date=received_date_start,
            submitted=True

        )

        latest_sample_code = generate_sample_code(order.code, urine_sample_type)
        sample = orders_models.Sample.objects.create(
            order=order,
            code=latest_sample_code,
            clia_sample_type=urine_sample_type,
            collection_date=timezone.now()
        )
        sample_barcode = get_sample_barcode(sample.id)
        sample.barcode = sample_barcode
        sample.save(update_fields=['barcode'])

        test_panel = orders_models.TestPanel.objects.create(
            order=order,
            test_panel_type=test_panel_type,
            expected_revenue=test_panel_type.cost,
        )

        for test_type in test_panel_type.test_types.all():
            test = orders_models.Test.objects.create(
                order=order,
                sample=sample,
                test_panel=test_panel,
                test_type=test_type,
                due_date=datetime.date.today() + datetime.timedelta(days=test_type.due_date_days)
            )
            result_obj = orders_models.Result.objects.create(
                order=order,
                test=test
            )

        if random.randint(1, average_order_count + 1) == average_order_count + 1:
            if received_date_start + datetime.timedelta(days=1, hours=2) < timezone.now():
                received_date_start = received_date_start + datetime.timedelta(days=1, hours=2)


def initialize_in_transit_orders():
    urine_sample_type = lab_models.CLIASampleType.objects.get(name='Urine')
    test_panel_type = orders_models.TestPanelType.objects.get(name='All analyte test')

    latest_order_code = generate_order_code()
    rand_account = accounts_models.Account.objects.get(id=random.randint(1, 9))
    rand_patient = patients_models.Patient.objects.get(id=random.randint(1, 11))
    logging.debug('In transit order Order: {} {} '.format(rand_account.name, rand_patient.user.first_name))
    order = orders_models.Order.objects.create(
        provider_id=1,
        account=rand_account,
        patient=rand_patient,
        code=latest_order_code,
        research_consent=False,
        patient_result_request=False,

    )

    latest_sample_code = generate_sample_code(order.code, urine_sample_type)
    sample = orders_models.Sample.objects.create(
        order=order,
        code=latest_sample_code,
        clia_sample_type=urine_sample_type,
        collection_date=timezone.now()
    )
    sample_barcode = get_sample_barcode(sample.id)
    sample.barcode = sample_barcode
    sample.save(update_fields=['barcode'])

    test_panel = orders_models.TestPanel.objects.create(
        order=order,
        test_panel_type=test_panel_type,
        expected_revenue=test_panel_type.cost,
    )

    for test_type in test_panel_type.test_types.all():
        test = orders_models.Test.objects.create(
            order=order,
            sample=sample,
            test_panel=test_panel,
            test_type=test_type,
            due_date=datetime.date.today() + datetime.timedelta(days=test_type.due_date_days)
        )
        result_obj = orders_models.Result.objects.create(
            order=order,
            test=test
        )
    order.submitted = True
    order.save(update_fields=['submitted'])


def initialize_dummy_observations():
    """
    create dummy observations for QC charting purposes
    :return:
    """
    days_back = 30
    date_counter = timezone.now() - datetime.timedelta(days=days_back)
    data_in = orders_models.DataIn.objects.first()
    controls = orders_models.Control.objects.all()
    targets = ['Cocaine', 'Lorazepam', 'Carisoprodol', 'Meprobamate']

    control_concentrations = [50.0, 100.0, 250.0, 500.0, 1000.0, 2000.0, 5000.0]

    observation_list = []
    while date_counter < timezone.now():
        control_index = 0
        for control in controls:
            for target in targets:
                observation = orders_models.Observation(
                    control=control,
                    target_name=target,
                    data_in=data_in,
                    result_value=random.uniform(control_concentrations[control_index] - 5.0,
                                                control_concentrations[control_index] + 5.0),
                    observation_date=date_counter,
                )
                observation_list.append(observation)
            control_index += 1
        date_counter += datetime.timedelta(days=1)
    orders_models.Observation.objects.bulk_create(observation_list)

@transaction.atomic
def initialize_test_panels_from_csv(csv_url):
    with requests.Session() as s:
        download = s.get(csv_url)
        decoded_content = download.content.decode('utf-8')
        csv_reader = csv.reader(decoded_content.splitlines())
        csv_rows = list(csv_reader)

        cleaned_rows = []
        for row in csv_rows:
            copy = True
            try:
                row_string = row[0]
            except IndexError:  # empty row
                continue
            # replace arbitrary number of spaces with two spaces
            row_string = re.sub('   +', '  ', row_string)
            row_string = re.sub('\([^P][ 0-Z]*?:.*?\)', '', row_string)

            row_strings = row_string.split('  ')
            # cleaning
            if row_strings[0][:12] == 'Profile list':
                copy = False
            if row_strings[0][:3] == '===':
                copy = False
            if row_strings[0][:3] == '---':
                copy = False
            if len(row_strings) >= 2:
                if row_strings[1][:3] == '===':
                    copy = False
                if row_strings[1][:3] == '---':
                    copy = False
                if row_strings[1][:3] == '~~~':
                    copy = False
            if copy:
                cleaned_rows.append(row_strings)

        panel_dict = {}
        panel_id = ''
        for row in cleaned_rows:
            if row[0]:  # new panel
                new_panel = True
                panel_info = row[0].split(' ')
                panel_id = panel_info[0]
                if len(panel_info) > 1:
                    panel_name = ' '.join(panel_info[1:])
                else:
                    panel_name = ''
                panel_dict[panel_id] = {'name': panel_name, 'tests': []}
            else:
                if len(row) == 5 and row[1]:  # 2 test row
                    test_1_info = row[1].split()
                    test_1_name = ' '.join(test_1_info[1:])
                    test_2_info = row[3].split()
                    test_2_name = ' '.join(test_2_info[1:])
                    if '(P)' not in test_1_name:
                        panel_dict[panel_id]['tests'].append({'code': test_1_info[0],
                                                              'name': test_1_name,
                                                              })
                    if '(P)' not in test_2_name:
                        panel_dict[panel_id]['tests'].append({'code': test_2_info[0],
                                                              'name': test_2_name,
                                                              })
                elif len(row) == 3 and row[1]:  # 1 test row
                    test_1_info = row[1].split()
                    test_1_name = ' '.join(test_1_info[1:])
                    if '(P)' not in test_1_name:
                        panel_dict[panel_id]['tests'].append({'code': test_1_info[0],
                                                              'name': test_1_name,
                                                              })
                elif len(row) == 4 and row[1]:
                    pass # sub panel title

        test_obj_dict = {}
        for panel_code in panel_dict:
            print('creating panels now')
            name = panel_dict[panel_code]['name']
            if name:
                panel = orders_models.TestPanelType.objects.create(
                    alternate_id=panel_code,
                    name=name,
                    menu_category_id=1
                )
                tests = []
                for test_dict in panel_dict[panel_code]['tests']:
                    try:
                        test_type_obj = test_obj_dict[test_dict['code']]
                    except KeyError:
                        try:
                            test_type_obj = orders_models.TestType.objects.get(alternate_id=test_dict['code'])
                            test_obj_dict[test_dict['code']] = test_type_obj
                        except orders_models.TestType.DoesNotExist:
                            test_type_obj = None
                            print('missing test type with {} - {}'.format(test_dict['code'], test_dict['name']))
                    if test_type_obj:
                        tests.append(test_type_obj)
                panel.test_types.set(tests)


def initialize_accounts():
    # get accounts list CSV for ILDP from S3 bucket
    # convert to list of dictionaries

    with requests.Session() as s:
        download = s.get(ACCOUNTS_CSV_URL)

        decoded_content = download.content.decode('utf-8')

        csv_reader = csv.reader(decoded_content.splitlines(), delimiter=',')
        # skip first line (columns)
        account_list = list(csv_reader)[1:]
        account_dict_list = []

        for row in account_list:
            # format phone/fax
            row[3] = re.sub("\D", "", row[3])
            row[4] = re.sub("\D", "", row[4])
            account_dict_list.append({'alternate_id': row[0],
                                      'name': row[1],
                                      'address1': row[2],
                                      'phone_number': row[3],
                                      'fax_number': row[4]})
        # create & save account objects
        create_accounts(account_dict_list)
        return account_dict_list


def initialize_providers():
    # get CSV from S3 bucket
    # convert to list of dictionaries
    tenant = client_models.Client.objects.get(schema_name=SCHEMA_CONTEXT['schema_name'])
    with tenant_context(tenant):

        with requests.Session() as s:
            download = s.get(PROVIDERS_CSV_URL)

            decoded_content = download.content.decode('utf-8')

            csv_reader = csv.reader(decoded_content.splitlines(), delimiter=',')
            # skip first line (columns)
            provider_list = list(csv_reader)[1:]
            provider_dict_list = []

            for row in provider_list:
                provider_id = row[0]
                provider_last_name = row[5]
                provider_first_name = row[6]
                provider_npi = row[7]
                provider_email = str(provider_id) + '@' + SCHEMA_NAME + '.com'

                provider_dict_list.append({'alternate_id': provider_id,
                                           'first_name': provider_first_name,
                                           'last_name': provider_last_name,
                                           'email': provider_email,
                                           'npi': provider_npi
                                           })

            # create providers first, then add accounts
            create_providers(provider_dict_list)

            for row in provider_list:
                provider_npi = row[7]
                # clean associated account data
                account_id_list = [row[1], row[2], row[3], row[4]]
                # clean empty items from list
                account_id_list = list(filter(None, account_id_list))
                accounts = accounts_models.Account.objects.filter(alternate_id__in=account_id_list)

                # link providers to accounts
                provider = accounts_models.Provider.objects.get(npi=provider_npi)
                provider.accounts.set(accounts)
                logging.debug('Accounts ' + str(accounts) + ' added to provider ' + str(provider))


def initialize_payers():
    # get CSV from S3 bucket
    # convert to list of dictionaries
    tenant = client_models.Client.objects.get(schema_name=SCHEMA_CONTEXT['schema_name'])
    with tenant_context(tenant):
        with requests.Session() as s:
            download = s.get(PAYERS_CSV_URL)

            decoded_content = download.content.decode('utf-8')

            csv_reader = csv.reader(decoded_content.splitlines(), delimiter=',')
            # skip first line (columns)
            payer_list = list(csv_reader)[1:]
            payer_dict_list = []

            for row in payer_list:
                # simple logic to parse Medicare and Medicaid
                row[1] = str(row[1]).strip().upper()
                # simple parse
                if 'MEDICARE' in row[1]:
                    payer_type = 'Medicare'
                elif 'MEDICAID' in row[1]:
                    payer_type = 'Medicaid'
                else:
                    payer_type = 'Private'

                payer_dict_list.append({'alternate_id': row[0],
                                        'name': row[1],
                                        'payer_type': payer_type})
            payers = []
            for payer_dict in payer_dict_list:
                payer = patients_models.Payer(alternate_id=payer_dict.get('alternate_id'),
                                              name=payer_dict.get('name'),
                                              payer_type=payer_dict.get('payer_type'))
                payers.append(payer)
            patients_models.Payer.objects.bulk_create(payers)

            """
            # if we are to update instead of creating from scratch
            payer, created = patients_models.Payer.objects.update_or_create(alternate_id=payer_dict.get('alternate_id'),
                                                                            defaults={
                                                                                'name': payer_dict.get('name'),
                                                                                'payer_type': payer_dict.get(
                                                                                    'payer_type')
                                                                            })
            if created:
                logging.debug('Payer ' + str(payer) + ' added.')
            """
            logging.debug('Payers added.')


@transaction.atomic
def initialize_patients():
    # get CSV from S3 bucket
    # can create 3 objects - User, Patient, PatientPayer
    # we will ignore PatientGuarantor, since the logic is too complex to be worth it
    # we don't really care about patient duplicates either - there's no good heuristic for this, since the data is messy

    tenant = client_models.Client.objects.get(schema_name=SCHEMA_CONTEXT['schema_name'])
    with tenant_context(tenant):
        with requests.Session() as s:
            download = s.get(PATIENTS_CSV_URL)

            decoded_content = download.content.decode('utf-8')

            csv_reader = csv.reader(decoded_content.splitlines(), delimiter=',')
            # skip first line (columns)
            patient_list = list(csv_reader)[1:]

            for row in patient_list:
                # strip out whitespace
                row = list(map(str.strip, row))

                # Patient info
                patient_alternate_id = row[0]
                last_name = row[1]
                first_name = row[2]
                middle_initial = row[3]
                sex = row[4]
                birth_date = datetime.datetime.strptime(row[5], '%m/%d/%y')
                # string parsing means that if the birthday is mm/dd/2025, it'll automatically convert to 1925
                # we are assuming no one aged 100 or greater
                if birth_date.year > datetime.datetime.now().year:
                    birth_date = birth_date.replace(year=birth_date.year - 100)
                address = row[6] + row[7] + ' ' + row[8] + ', ' + row[9] + ' ' + row[10]

                # Provider info
                provider_alternate_id = row[12]
                # if provider is not listed in the provider list, then skip over everything else and continue
                # since patient is no longer with ILDP
                try:
                    provider = accounts_models.Provider.objects.get(alternate_id=provider_alternate_id)
                except ObjectDoesNotExist:
                    logging.debug('Provider with ID ' + provider_alternate_id + ' does not exist.')
                    continue

                # PatientPayer info
                payer1_member_id = row[15]
                payer1_group_number = row[16]
                # payer_id = 0 is self-pay
                try:
                    payer1_alternate_id = int(row[13])
                    if payer1_alternate_id:
                        primary_payer = patients_models.Payer.objects.get(alternate_id=payer1_alternate_id)
                    else:
                        primary_payer = None
                except:
                    primary_payer = None

                # PatientPayer info
                payer2_member_id = row[19]
                payer2_group_number = row[20]
                # payer_id = 0 is self-pay
                try:
                    payer2_alternate_id = int(row[17])
                    if payer2_alternate_id:
                        secondary_payer = patients_models.Payer.objects.get(alternate_id=payer2_alternate_id)
                    else:
                        secondary_payer = None
                except:
                    secondary_payer = None

                # SE (self) SP (spouse) CH (child) PA (parent), and OT (other)
                # In this case, CH means that the patient is a child of the insured
                # if invalid value, default to SE
                relation = row[23]
                subscriber_name = row[21] + ' ' + row[20]

                if relation == 'SE':
                    subscriber_relationship = 'Self'
                    subscriber_name = ''
                elif relation == 'SP':
                    subscriber_relationship = 'Spouse'
                elif relation == 'CH':
                    subscriber_relationship = 'Parent'
                elif relation == 'OT':
                    subscriber_relationship = 'Other'
                else:
                    subscriber_relationship = 'Self'
                    subscriber_name = ''

                ########################################################################################################
                # create patient
                # generate random string for username and password initially
                user_uuid = str(uuid.uuid4())
                username = first_name + '-' + last_name + '-' + user_uuid

                # create User object
                patient_user = lab_tenant_models.User(first_name=first_name,
                                                      last_name=last_name,
                                                      username=username,
                                                      user_type=20)
                patient_user.set_password(user_uuid)
                # create User object first before creating Patient
                patient_user.save()

                patient = patients_models.Patient(alternate_id=patient_alternate_id,
                                                  user=patient_user,
                                                  middle_initial=middle_initial,
                                                  sex=sex,
                                                  birth_date=birth_date,
                                                  address1=address)
                patient.save()
                logging.debug('Patient: ' + str(patient) + ' added.')

                # add provider after patient creation (many-to-many operation)
                patient.providers.add(provider)

                # add PatientPayer objects
                if primary_payer:
                    patient_payer_primary = patients_models.PatientPayer(patient=patient,
                                                                         payer=primary_payer,
                                                                         payer_type=primary_payer.payer_type,
                                                                         is_primary=True,
                                                                         member_id=payer1_member_id,
                                                                         group_number=payer1_group_number,
                                                                         subscriber_name=subscriber_name,
                                                                         subscriber_relationship=subscriber_relationship)
                    patient_payer_primary.save()
                    logging.debug('Patient Payer: ' + str(patient_payer_primary) + ' added.')

                if secondary_payer:
                    patient_payer_secondary = patients_models.PatientPayer(patient=patient,
                                                                           payer=secondary_payer,
                                                                           payer_type=secondary_payer.payer_type,
                                                                           is_secondary=True,
                                                                           member_id=payer2_member_id,
                                                                           group_number=payer2_group_number,
                                                                           subscriber_name=subscriber_name,
                                                                           subscriber_relationship=subscriber_relationship)
                    patient_payer_secondary.save()
                    logging.debug('Patient Payer: ' + str(patient_payer_secondary) + ' added.')


if __name__ == '__main__':
    logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)
    schema_name = main()
    """
    tenant = client_models.Client.objects.get(schema_name=schema_name)
    with tenant_context(tenant):
        initialize_test_panels_from_csv(TEST_PANELS_CSV_URL)
    """


