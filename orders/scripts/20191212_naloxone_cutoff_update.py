import os
import django
import datetime

os.environ['DJANGO_SETTINGS_MODULE'] = 'main.settings'
django.setup()

from tenant_schemas.utils import tenant_context
import clients.models as clients_models
from orders import models


SCHEMA_NAME = 'ildp'

# only update the results after go-live (2019-11-01)
START_DATE = datetime.datetime.strptime('2019-11-01', '%Y-%m-%d')


ANALYTE_LIST = [
    {
        'name': 'Naloxone - Confirmation',
        'new_cutoff': 7.5,
        'new_critical_high': 500
    },
    {
        'name': 'Naltrexone - Confirmation',
        'new_cutoff': 7.5,
        'new_critical_high': 500
    },
    {
        'name': 'Noroxycodone - Confirmation',
        'new_cutoff': 37.5,
        'new_critical_high': 2500
    },
    {
        'name': 'Paroxetine (Paxil) - Confirmation',
        'new_cutoff': 75,
        'new_critical_high': 5000
    }
]


def main():
    """
    :return:
    """
    tenant = clients_models.Client.objects.get(schema_name=SCHEMA_NAME)
    with tenant_context(tenant):
        for analyte in ANALYTE_LIST:
            update_cutoff(START_DATE, analyte)


def update_cutoff(start_date, analyte):
    test_type_name = analyte.get('name')

    results = models.Result.objects.filter(test__test_type__name=test_type_name,
                                           submitted_datetime__gt=start_date)
    # update the actual test type itself so that the cutoffs are fixed going forward
    test_type_range = models.TestTypeRange.objects.get(test_type__name=test_type_name)
    test_type_range.range_low = analyte.get('new_cutoff')
    test_type_range.range_high = None
    test_type_range.critical_low = None
    test_type_range.critical_high = analyte.get('new_critical_high')
    test_type_range.save()

    for result in results:
        print('Cutoff updated for order: {}'.format(result.order))
        result.cutoff = analyte.get('new_cutoff')

        if result.result_quantitative:
            if result.result_quantitative >= result.cutoff:
                result.result = '+'
                print('{}: {} (Positive)'.format(test_type_name, result.result_quantitative))
            else:
                result.result = '-'
                print('{}: {} (Negative)'.format(test_type_name, result.result_quantitative))

        result.save()

#
# def naloxone_cutoff(start_date):
#     naloxone_results = models.Result.objects.filter(test__test_type__name='Naloxone - Confirmation',
#                                                     submitted_datetime__gt=start_date)
#     # update the actual test type itself so that the cutoffs are fixed going forward
#     test_type_range = models.TestTypeRange.objects.get(test_type__name='Naloxone - Confirmation')
#     test_type_range.range_low = NALOXONE_CUTOFF
#     test_type_range.range_high = None
#     test_type_range.critical_low = None
#     test_type_range.critical_high = NALOXONE_CRIT_HIGH
#     test_type_range.save()
#
#     for result in naloxone_results:
#         print('Cutoff updated for order: {}'.format(result.order))
#         result.cutoff = NALOXONE_CUTOFF
#
#         if result.result_quantitative:
#             if result.result_quantitative >= result.cutoff:
#                 result.result = '+'
#                 print('Naloxone: {} (Positive)'.format(result.result_quantitative))
#             else:
#                 result.result = '-'
#                 print('Naloxone: {} (Negative)'.format(result.result_quantitative))
#
#         result.save()


if __name__ == '__main__':
    main()


