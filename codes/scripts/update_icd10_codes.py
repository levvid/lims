import boto3
import csv
import django
import logging
import os
import requests
import sys

os.environ['DJANGO_SETTINGS_MODULE'] = 'main.settings'
django.setup()

from codes import models

"""
Script used to update ICD10 along with their yearly updates.

Store addendum files in s3 dendi-lis/codes/ICD10/
See the following url for more information:
https://github.com/DendiSoftware/lis/wiki/Updating-Procedure-Diagnostics-Codes

Example usage:
python -m codes.scripts.update_icd10_codes https://dendi-lis.s3.amazonaws.com/codes/ICD10/icd10cm_codes_addenda_2019.txt
"""


def update_icd10_codes(url):
    print("Downloading and parsing addendum file")
    code_delta_list = parse_addendum_file_from_url(url)

    print("Adding ICD10 Code changes")
    new_codes = []
    deleted_codes = []
    revised_codes = []
    for code_delta in code_delta_list:
        action = code_delta['action']
        code = code_delta['code']
        description = code_delta['description']

        if action == "Revise to":
            revised_code = models.ICD10CM.objects.get(full_code=code)
            revised_code.full_description = description
            revised_codes.append(revised_code)
        elif action == "Revise from":
            # don't need to do anything here
            pass
        elif action == "Add":
            new_code = models.ICD10CM(
                full_code=code,
                full_description=description,
                abbreviated_description=description,
            )
            new_codes.append(new_code)
        elif action == "Delete":
            deleted_code = models.ICD10CM.objects.get(full_code=code)
            deleted_code.is_expired = True
            deleted_codes.append(deleted_code)
    print("Committing changes: {} new codes, {} deleted codes, and {} revised codes".format(
        len(new_codes),
        len(deleted_codes),
        len(revised_codes)
    ))
    models.ICD10CM.objects.bulk_create(new_codes)
    models.ICD10CM.objects.bulk_update(deleted_codes, ['is_expired'])
    models.ICD10CM.objects.bulk_update(revised_codes, ['full_description'])
    return new_codes, deleted_codes, revised_codes


def parse_addendum_file_from_url(url):
    """
    Download and parse icd10 code addendum file
    :param url:
    :return: [{'action': String, 'code': String, 'description': String}]
    """
    with requests.Session() as s:
        download = s.get(url)
        decoded_content = download.content.decode('utf-8')
        rows = decoded_content.splitlines()

    cleaned_rows = []
    for row in rows:
        if row == '':  # end of data
            break
        action = row[:13].strip().replace(':', '')
        code = row[13:21].strip()
        description = row[21:].strip()
        cleaned_rows.append(
            {
                'action': action,
                'code': code,
                'description': description,
            }
        )
    return cleaned_rows


if __name__ == '__main__':
    if len(sys.argv) == 1:
        print("Missing argument: Please give the URL of icd10cm_codes_addenda_<year>.txt. \n"
              "Visit the following link if confused.\n"
              "https://github.com/DendiSoftware/lis/wiki/Updating-Procedure-Diagnostics-Codes")
    elif len(sys.argv) == 2:
        url = sys.argv[1]
        update_icd10_codes(url)
    else:
        print("Unknown arguments")
