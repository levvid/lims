import django
import logging
import os
import sys

from django.db import transaction
from tenant_schemas.utils import tenant_context

os.environ['DJANGO_SETTINGS_MODULE'] = 'main.settings'
django.setup()

from clients import models as client_models
from orders.functions import get_sample_barcode
from .. import models


@transaction.atomic
def main():
    """
    Script that changes/updates each sample_barcode from either the uuid or blank to an encrypted version
    of the sample.id
    in TestTypes for each tenant
    :return:
    """

    tenants = client_models.Client.objects.exclude(schema_name='public')

    for tenant in tenants:
        with tenant_context(tenant):

            sample_list = models.Sample.objects.all()

            for sample in sample_list:
                sample.barcode = get_sample_barcode(sample.id)
                sample.save()
                logging.debug("Tenant: {} - converting sample_barcode to encrypted sample_id for sample_id {}"
                              .format(tenant, sample.id))


if __name__ == '__main__':
    logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)
    main()
