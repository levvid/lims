function createEmployeeTable(employees, table) {

  if (!table) {
    const hotElement = document.querySelector('#employee-table');
    const hotSettings = {
      data: employees,
      columns: [
        {
          data: 'name',
          editor: false,
          type: 'text',
        },
        {
          data: 'num_patients',
          editor: false,
          type: 'text',
        },
        {
          data: 'account',
          editor: false,
          renderer: linkRenderer,
        },
        {
          data: 'date_added',
          type: 'text',
        },
      ],
      stretchH: 'all',
      //width: 806,
      height: 700,
      autoColumnSize: true,
      autoWrapRow: true,
      manualRowResize: false,
      manualColumnResize: false,
      columnSorting: true,
      sortIndicator: true,
      rowHeaders: false,
      colHeaders: true,
      nestedHeaders: [
        [
          'Name',
          'Patient Count',
          'Account',
          'Date Added',
        ],
      ],
      manualRowMove: false,
      manualColumnMove: false,
      contextMenu: false,
      filters: true,
      dropdownMenu: ['filter_by_condition', 'filter_action_bar'],
      afterGetColHeader: function(col, TH) {
        TH.className = 'center';
      },
    };

    return new Handsontable(hotElement, hotSettings);
  } else {
    // load new data without replacing old table
    table.getPlugin('filters').clearConditions();
    table.loadData(employees);
    return table;
  }
}
