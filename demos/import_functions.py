import csv
import logging
import requests
import uuid

from clients import models as client_models
from orders import models as orders_models
from accounts import models as accounts_models
from lab import models as lab_models
from lab_tenant import models as lab_tenant_models
from patients import models as patients_models

"""
Functions used to import Data from a standardized format

These functions should be used under tenant_context
"""


def import_test_type_from_csv_url(csv_url):
    """
    Import test types from csv using the below format:
    https://dendi-lis.s3.amazonaws.com/client_data/Templates/Dendi_TC_Template.xlsx
    Save xlsx as csv after file is populated.
    :param csv_url: String
    :return: ([TestType], [TestTypeRange])
    """
    def get_test_type_dicts_from_csv_url(csv_url):
        with requests.Session() as s:
            download = s.get(csv_url)
            decoded_content = download.content.decode('utf-8')
            csv_reader = csv.reader(decoded_content.splitlines(), delimiter=',')

            # skip first line (columns)
            row_list = list(csv_reader)[1:]
            row_dict_list = []

            # cleaning
            for row in row_list:
                # sample_type
                sample_type_string = row[10].strip()
                if sample_type_string == 'Urine':
                    sample_type = {'Urine': 1}
                elif sample_type_string == 'Saliva':
                    sample_type = {'Saliva': 1}
                else:
                    sample_type = None

                # units
                unit = row[3].strip()
                if unit and unit.upper() in ['N/A', 'NA']:
                    unit = ''

                # round_figures
                round_figures = row[22].strip()
                try:
                    round_figures = int(round_figures)
                except ValueError:
                    round_figures = 2

                # test type range
                crit_low = row[15]
                if crit_low and crit_low.upper() in ['N/A', 'NA'] or not crit_low:
                    crit_low = None
                low = row[16]
                if low and low.upper() in ['N/A', 'NA'] or not low:
                    low = None
                crit_high = row[18]
                if crit_high and crit_high.upper() in ['N/A', 'NA'] or not crit_high:
                    crit_high = None
                high = row[17]
                if high and high.upper() in ['N/A', 'NA'] or not high:
                    high = None

                row_dict_list.append({
                    'alternate_id': row[0],
                    'name': row[1],
                    'test_target_name': row[2].strip(),
                    'unit': unit,
                    'cpt_codes': row[4],
                    'container_type': row[5],
                    'draw_info': row[6],
                    'storage': row[7],
                    'min_volume': row[8],
                    'specimen_preparation': row[9],
                    'sample_type': sample_type,
                    'offsite_laboratory': row[11],
                    'offsite_test_id': row[12],
                    'receiving_facility': row[13],
                    'loinc_code': row[14],
                    'crit_low': crit_low,
                    'low': low,
                    'high': high,
                    'crit_high': crit_high,
                    'orderable': row[19],
                    'test_method': row[20],
                    'due_date_days': row[21],
                    'round_figures': round_figures,
                    'notes': row[23],
                    'specialty': row[24],
                    'result_type': row[25]
                })
            return row_dict_list

    test_type_dicts = get_test_type_dicts_from_csv_url(csv_url)
    test_types = create_test_types(test_type_dicts)
    test_type_ranges = create_test_type_ranges(test_type_dicts, test_types)

    return test_types, test_type_ranges


def create_test_types(test_type_dicts):
    """
    :param test_type_dicts:
    :return: [TestType]
    """
    test_types = []
    offsite_laboratory_dict = dict()
    missing_test_types = []
    missing_test_methods = []
    missing_test_specialties = []

    for test_type_dict in test_type_dicts:
        # match sample type
        required_samples = {lab_models.CLIASampleType.objects.get(name=key).id: value for (key, value) in
                            test_type_dict['sample_type'].items()}

        # match test method
        try:
            test_method = lab_models.TestMethod.objects.get(name=test_type_dict['test_method'])
        except lab_models.TestMethod.DoesNotExist:
            logging.debug("{} test method does not exist".format(test_type_dict['test_method']))
            missing_test_methods.append(test_type_dict['test_method'])
            test_method = None

        # match test specialty
        try:
            test_specialty = lab_models.CLIATestTypeSpecialty.objects.get(specialty_name=test_type_dict['specialty'])
        except lab_models.CLIATestTypeSpecialty.DoesNotExist:
            logging.debug("{} specialty does not exist".format(test_type_dict['specialty']))
            missing_test_specialties.append(test_type_dict['specialty'])
            test_specialty = None

        # match test target name with database
        try:
            test_target = lab_models.TestTarget.objects.get(name__iexact=test_type_dict['test_target_name'])
        except lab_models.TestTarget.DoesNotExist:
            logging.debug("{} test target does not exist".format(test_type_dict['test_target_name']))
            missing_test_types.append(test_type_dict['test_target_name'])
            test_target = None

        test_type = orders_models.TestType(
            name=test_type_dict.get('name'),
            test_method=test_method,
            alternate_id=test_type_dict.get('alternate_id', None),
            unit=test_type_dict.get('unit', ''),
            test_target=test_target,
            result_type=test_type_dict.get('result_type', 'Quantitative'),
            round_figures=test_type_dict.get('round_figures', 2),
            required_samples=required_samples,
            specialty=test_specialty,
            due_date_days=test_type_dict.get('due_date_days', 14)
        )
        test_types.append(test_type)

    logging.debug('Missing {} test methods: {}'.format(len(missing_test_methods), missing_test_methods))
    logging.debug('Missing {} test specialties: {}'.format(len(missing_test_specialties), missing_test_specialties))
    logging.debug('Missing {} test targets: {}'.format(len(missing_test_types), missing_test_types))
    # Create TestType objects
    orders_models.TestType.objects.bulk_create(test_types)
    return test_types


def create_test_type_ranges(test_type_dicts, test_types):
    """
    :param test_type_dicts:
    :param test_types:
    :return: [TestTypeRange]
    """
    test_type_ranges = []
    for test_type_dict, test_type in zip(test_type_dicts, test_types):
        test_type_range = orders_models.TestTypeRange(
            test_type=test_type,
            default=True,
            range_low=test_type_dict.get('low', None),
            range_high=test_type_dict.get('high', None),
            critical_low=test_type_dict.get('crit_low', None),
            critical_high=test_type_dict.get('crit_high', None),
        )
        test_type_ranges.append(test_type_range)
    # Create TestTypeRange objects
    orders_models.TestTypeRange.objects.bulk_create(test_type_ranges)
    return test_type_ranges
