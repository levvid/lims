# Generated by Django 2.2.2 on 2020-02-04 17:56

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('orders', '0118_auto_20200127_2136'),
    ]

    operations = [
        migrations.AddField(
            model_name='orderissue',
            name='samples',
            field=models.ManyToManyField(related_name='order_issues', to='orders.Sample'),
        ),
        migrations.AddField(
            model_name='test',
            name='test_profiles',
            field=models.ManyToManyField(to='orders.TestPanel'),
        ),
    ]
