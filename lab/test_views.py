from django.contrib.auth.models import User
from django.shortcuts import reverse
from rest_framework import status
from rest_framework.test import APITestCase, force_authenticate

from . import models
from . import serializers


class CreateUserTest(APITestCase):
    def setUp(self):
        self.superuser = User.objects.create_superuser('john', 'john@snow.com', 'johnpassword')
        self.client.login(username='john', password='johnpassword')
        self.data = {'username': 'mike', 'first_name': 'Mike', 'last_name': 'Tyson'}

    def test_can_create_user(self):
        response = self.client.post(reverse('user-list'), self.data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)


class ReadUserTest(APITestCase):
    def setUp(self):
        self.superuser = User.objects.create_superuser('john', 'john@snow.com', 'johnpassword')
        self.client.login(username='john', password='johnpassword')
        self.user = User.objects.create(username="mike")

    def test_can_read_user_list(self):
        response = self.client.get(reverse('user-list'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_can_read_user_detail(self):
        response = self.client.get(reverse('user-detail', args=[self.user.id]))
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class DeleteUserTest(APITestCase):
    def setUp(self):
        self.superuser = User.objects.create_superuser('john', 'john@snow.com', 'johnpassword')
        self.client.login(username='john', password='johnpassword')
        self.user = User.objects.create(username="mikey")

    def test_can_delete_user(self):
        response = self.client.delete(reverse('user-detail', args=[self.user.id]))
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)


class TestOrdersApi(APITestCase):
    def setUp(self):
        self.superuser = User.objects.create_superuser('john', 'john@snow.com', 'johnpassword')
        self.client.login(username='john', password='johnpassword')
        self.order_code = 'GH18-002322'
        self.order = models.Order(code=self.order_code)
        self.order.save()
        self.test_type = models.TestType(name='test_type')
        self.test_type.save()
        self.test = models.Test(order=self.order, test_type=self.test_type)
        self.test.save()
        self.sample = models.Sample(order=self.order)
        self.sample.save()

    def test_getting_order(self):
        response = self.client.get(reverse('order-list'), format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_adding_sample_to_order(self):
        response = self.client.post(reverse('order-samples-list', args=[self.order.code]),
                                    {'collection_date': '2017-01-01',
                                     'type': 'Blood'})
        print(response.data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

