import django
import logging
import os
import sys
from django.db import connection, transaction
from demos.scripts.ildp_setup_20190722 import initialize_historical_patient_results_from_csvs
from tenant_schemas.utils import tenant_context

os.environ['DJANGO_SETTINGS_MODULE'] = 'main.settings'
django.setup()

from clients import models as client_models

"""
Script used to work on task:
Import historical patient results

Oct 18th:
2018 Aug-Sept run through
"""

HISTORICAL_PATIENT_RESULTS_CSV_URLS = [
    # 2019
    'https://dendi-lis.s3.amazonaws.com/client_data/ildp/Patient+Results/2019/Jan+-+March+2019.csv',
    'https://dendi-lis.s3.amazonaws.com/client_data/ildp/Patient+Results/2019/Apr+-+Jul+2019.csv',
    # 2018
    'https://dendi-lis.s3.amazonaws.com/client_data/ildp/Patient+Results/2018/August+1-5+2018.csv',
    'https://dendi-lis.s3.amazonaws.com/client_data/ildp/Patient+Results/2018/August+6-9+2018.csv',
    'https://dendi-lis.s3.amazonaws.com/client_data/ildp/Patient+Results/2018/August+10-14+2018.csv',
    'https://dendi-lis.s3.amazonaws.com/client_data/ildp/Patient+Results/2018/August+15-18+2018.csv',
    'https://dendi-lis.s3.amazonaws.com/client_data/ildp/Patient+Results/2018/August+15-18+2018.csv',
    'https://dendi-lis.s3.amazonaws.com/client_data/ildp/Patient+Results/2018/August+19-22+2018.csv',
    'https://dendi-lis.s3.amazonaws.com/client_data/ildp/Patient+Results/2018/August+23-28+2018.csv',
    'https://dendi-lis.s3.amazonaws.com/client_data/ildp/Patient+Results/2018/August+29-31+2018.csv',
    'https://dendi-lis.s3.amazonaws.com/client_data/ildp/Patient+Results/2018/Sept+1-5+2018.csv',
    'https://dendi-lis.s3.amazonaws.com/client_data/ildp/Patient+Results/2018/Sept+6-10+2018.csv',
    'https://dendi-lis.s3.amazonaws.com/client_data/ildp/Patient+Results/2018/Sept+11-15+2018.csv',
    'https://dendi-lis.s3.amazonaws.com/client_data/ildp/Patient+Results/2018/Sept+16-20+2018.csv',
    'https://dendi-lis.s3.amazonaws.com/client_data/ildp/Patient+Results/2018/Sept+21-25+2018.csv',
    'https://dendi-lis.s3.amazonaws.com/client_data/ildp/Patient+Results/2018/Sept+26-30+2018.csv',
    'https://dendi-lis.s3.amazonaws.com/client_data/ildp/Patient+Results/2018/Oct++1-5+2018.csv',
    'https://dendi-lis.s3.amazonaws.com/client_data/ildp/Patient+Results/2018/Oct+8-12+2018.csv',
    'https://dendi-lis.s3.amazonaws.com/client_data/ildp/Patient+Results/2018/Oct+15-19+2018.csv',
    'https://dendi-lis.s3.amazonaws.com/client_data/ildp/Patient+Results/2018/Oct+22-26+2018.csv',
    'https://dendi-lis.s3.amazonaws.com/client_data/ildp/Patient+Results/2018/Oct+29-31+2018.csv',
    'https://dendi-lis.s3.amazonaws.com/client_data/ildp/Patient+Results/2018/Nov+1-2+2018.csv',
    'https://dendi-lis.s3.amazonaws.com/client_data/ildp/Patient+Results/2018/Nov+5-9+2018.csv',
    'https://dendi-lis.s3.amazonaws.com/client_data/ildp/Patient+Results/2018/Nov+12-16+2018.csv',
    'https://dendi-lis.s3.amazonaws.com/client_data/ildp/Patient+Results/2018/Nov+19-23+2018.csv',
    'https://dendi-lis.s3.amazonaws.com/client_data/ildp/Patient+Results/2018/Nov+26-30+2018.csv',
    'https://dendi-lis.s3.amazonaws.com/client_data/ildp/Patient+Results/2018/Dec+3-7+2018.csv',
    'https://dendi-lis.s3.amazonaws.com/client_data/ildp/Patient+Results/2018/Dec+10-14+2018.csv',
    'https://dendi-lis.s3.amazonaws.com/client_data/ildp/Patient+Results/2018/Dec+17-21+2018.csv',
    'https://dendi-lis.s3.amazonaws.com/client_data/ildp/Patient+Results/2018/Dec+24-31+2018.csv',


]


def main():
    import timeit
    t0 = timeit.default_timer()
    schema_name = 'ildp'
    tenant = client_models.Client.objects.get(schema_name=schema_name)
    print("schema: {}".format(schema_name))
    with tenant_context(tenant):
        for csv_url in HISTORICAL_PATIENT_RESULTS_CSV_URLS:
            print('importing {}'.format(csv_url))
            initialize_historical_patient_results_from_csvs([csv_url])
        #initialize_historical_patient_results_from_csvs(HISTORICAL_PATIENT_RESULTS_CSV_URLS)
    t1 = timeit.default_timer()
    print("{} seconds".format(t1-t0))


if __name__ == '__main__':
    main()
