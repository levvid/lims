import datetime
import json
import uuid
import tempfile
import base64
import pyotp

from braces.views import LoginRequiredMixin
from dal import autocomplete
from django.shortcuts import render, redirect, get_object_or_404
from django.utils import timezone
from django.views.generic import TemplateView, View, FormView, ListView
from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
from django.db.models import Count, Case, When
from django.db.models.functions import TruncMonth, TruncDay
from django.db import transaction
from django.http import HttpResponse, Http404
from django.core.exceptions import ValidationError
from pdfrw import PdfWriter, PdfReader

from orders.forms import PAYER_TYPES
from patients.forms import PAYER_CHOICES, PayerForm
from .forms import LoginForm, SettingsPasswordForm
from . import forms

# custom scripts
from .scripts import view_scripts

from . import models
from lab_tenant.views import LabMemberMixin, LabAdminMixin, ProviderMixin, LISLoginRequiredMixin
from lab_tenant.models import User
from orders import models as orders_models, tasks
from accounts import models as accounts_models
from patients import models as patients_models
from lab_tenant import models as lab_tenant_models
from main import functions

from clients.scripts.initialize_galaxy_data import main as initialize_data


def get_lab_setup_status(request):
    """
    Get setup status for Dashboard and Setup views
    :return: context Dict with LIS setup status variables
    """
    lab_setup_steps = []
    context = {}

    # offsite integrations check
    num_offsite_integrations = lab_tenant_models.OffsiteLaboratoryIntegration.objects.count()
    has_offsite_integrations = num_offsite_integrations > 0
    context.update({'num_offsite_integrations': num_offsite_integrations,
                    'has_offsite_integrations': has_offsite_integrations})

    # instrument integrations check
    num_instrument_integrations = lab_tenant_models.InstrumentIntegration.objects.count()
    has_instrument_integrations = num_instrument_integrations > 0
    context.update({'num_instrument_integrations': num_instrument_integrations,
                    'has_instrument_integrations': has_instrument_integrations})

    # in-house lab location(s) check
    num_in_house_lab_locations = lab_tenant_models.InHouseLabLocation.objects.count()
    has_in_house_lab_locations = num_in_house_lab_locations > 0
    context.update({'num_in_house_lab_locations': num_in_house_lab_locations,
                    'has_in_house_lab_locations': has_in_house_lab_locations})

    # proprietary test types check
    num_test_types = orders_models.TestType.objects.count()
    has_test_types = num_test_types > 0
    lab_setup_steps.append(has_test_types)
    context.update({'num_test_types': num_test_types,
                    'has_test_types': has_test_types})

    # test panel types check
    num_test_panel_types = orders_models.TestPanelType.objects.count()
    has_test_panel_types = num_test_panel_types > 0
    lab_setup_steps.append(has_test_panel_types)

    # departments check
    num_lab_departments = lab_tenant_models.LabDepartment.objects.count()
    has_lab_departments = num_lab_departments > 0
    context.update({'num_lab_departments': num_lab_departments,
                    'has_lab_departments': has_lab_departments})

    # CLIA number check
    if request.tenant.clia_number:
        has_clia_number = True
    else:
        has_clia_number = False
    # CLIA director check
    if request.tenant.clia_director:
        has_clia_director = True
    else:
        has_clia_director = False
    # address check
    if request.tenant.address_1:
        has_address = True
    else:
        has_address = False
    # phone number check
    if request.tenant.phone_number:
        has_phone_number = True
    else:
        has_phone_number = False
    # fax number check
    if request.tenant.fax_number:
        has_fax_number = True
    else:
        has_fax_number = False
    # website check
    if request.tenant.website:
        has_website = True
    else:
        has_website = False

    # timezone check
    if request.tenant.timezone != 'UTC':
        has_timezone = True
    else:
        has_timezone = False

    lab_information = [has_clia_director, has_clia_number, has_website, has_fax_number, has_phone_number, has_address,
                       has_timezone]
    has_lab_information = len(lab_information) == len([x for x in lab_information if x])
    lab_setup_steps.append(has_lab_information)

    # logo check
    if request.tenant.logo_file:
        has_logo_file = True
    else:
        has_logo_file = False
    lab_setup_steps.append(has_logo_file)

    if lab_setup_steps.count(True) == len(lab_setup_steps):
        lab_setup_complete = True
    else:
        lab_setup_complete = False

    context.update({'num_steps_done': len([x for x in lab_setup_steps if x]),
                    'num_steps': len(lab_setup_steps),
                    'lab_setup_complete': lab_setup_complete,
                    'has_test_panel_types': has_test_panel_types,
                    'num_test_panel_types': num_test_panel_types,
                    'has_clia_number': has_clia_number,
                    'has_clia_director': has_clia_director,
                    'has_address': has_address,
                    'has_phone_number': has_phone_number,
                    'has_fax_number': has_fax_number,
                    'has_website': has_website,
                    'has_logo_file': has_logo_file,
                    'has_timezone': has_timezone,
                    'has_lab_information': has_lab_information})
    return context


class LandingPageView(View):
    def get(self, request):
        if request.user.is_authenticated:
            return redirect('dashboard')
        return render(request, 'login_tenant.html')


class LoginView(View):
    def get(self, request):
        if request.user.is_active:
            return redirect('dashboard')
        return render(request, 'login_tenant.html')

    def post(self, request):
        username = request.POST.get('username')
        password = request.POST.get('password')

        user = authenticate(username=username, password=password)

        if user is not None:
            if user.is_active:
                # Redirect to 2FA View if 2FA setting is enabled and user is lab employee.
                if request.tenant.settings.get('use_two_factor_auth', False):
                    if user.user_type == 1 or user.user_type == 2:
                        user_info = {'username': username,
                                     'password': password,
                                     'secret': user.two_factor_auth_secret}
                        request.session['user_info' ] = user_info
                        return redirect('two_factor_auth')
                    else:
                        login(request, user)
                        if user.user_type < 9:
                            return redirect('order_list')
                        return redirect('order_list')
                else:
                    login(request, user)
                    if user.user_type < 9:
                        return redirect('order_list')
                    return redirect('dashboard')
            else:
                return render(
                    request,
                    'login_tenant.html',
                    context={
                        'login_error': 'Username and/or password is incorrect.'
                    }
                )
        return render(
            request,
            'login_tenant.html',
            context={
                'login_error': 'Username and/or password is incorrect.'
            }
        )


class TwoFactorAuthView(View):
    def get(self, request):
        secret = request.session['user_info'].get('secret', None)
        if not secret:
            return redirect('two_factor_auth_setup')

        form = forms.TwoFactorAuthForm()
        return render(request, 'two_factor_auth.html', context={'form': form})

    def post(self, request):
        form = forms.TwoFactorAuthForm(request.POST)

        if form.is_valid():
            two_factor_auth_code = form.cleaned_data['two_factor_code']

            user_info = request.session['user_info']
            secret = user_info.get('secret', None)

            # Verify submitted 2FA code is correct.
            # If code is correct, delete the user_info stored in the session and login.
            if two_factor_auth_code == pyotp.TOTP(secret).now():
                username = user_info.get('username')
                password = user_info.get('password')

                user = authenticate(username=username, password=password)
                login(request, user)

                del request.session['user_info']
                if user.user_type < 9:
                    return redirect('order_list')
                return redirect('dashboard')
            else:
                return render(
                    request,
                    'two_factor_auth.html',
                    context={
                        'login_error': 'Submitted code was expired or invalid. '
                                       'Please re-submit.',
                        'form': form
                    }
                )
        else:
            return render(
                request,
                'two_factor_auth.html',
                context={
                    'login_error': 'Submitted form was invalid. '
                                   'Code should be a 6-digit number with no spaces. '
                                   'Please re-submit.',
                    'form': form},
            )


class TwoFactorAuthSetupView(View):
    def get(self, request):
        # Generate a 2FA secret key for the user's Google Authenticator app to use.
        two_factor_secret = pyotp.random_base32()
        form = forms.TwoFactorAuthSetupForm()
        context = {'two_factor_secret': two_factor_secret,
                   'form': form}
        return render(request, 'two_factor_auth_setup.html', context=context)

    def post(self, request):
        user_info = request.session['user_info']
        username = user_info.get('username')

        form = forms.TwoFactorAuthSetupForm(request.POST)

        if form.is_valid():
            secret = form.cleaned_data['two_factor_shared_secret']

            # Save the generated 2FA secret key for this user to the database.
            # Then redirect them to the 2FA verification screen.
            user_object = User.objects.get(username=username)
            user_object.two_factor_auth_secret = secret
            user_object.save(update_fields=['two_factor_auth_secret'])

            user_info = {'username': user_info.get('username'),
                         'password': user_info.get('password'),
                         'secret': user_object.two_factor_auth_secret}
            request.session['user_info'] = user_info

            return redirect('two_factor_auth')

        else:
            return render(
                request,
                'two_factor_auth_setup.html',
                context={
                    'login_error': 'Please confirm that you\'ve added the two-factor authentication code above'
                                   'to your Google Authenticator app.',
                    'form': form
                }
            )


class LogoutView(View):

    def get(self, request):
        logout(request)
        return redirect('landing_page')


class ForgotPasswordView(View):
    def get(self, request):
        form = forms.ForgotPasswordForm()
        return render(request, 'forgot-password.html', {'form': form})

    def post(self, request):
        form = forms.ForgotPasswordForm(request.POST)

        if form.is_valid():
            username = form.cleaned_data.get('username')
            user_object = User.objects.get(username__iexact=username)

            if user_object.email:
                # new random password that is 10 characters long
                new_password = view_scripts.generate_random_password(10)
                user_object.set_password(new_password)
                user_object.save()
                # if email on file, then send email to that username
                # if not email on file, then "contact support" --> nothing can be done here
                from_email = 'support@dendisoftware.com'
                to_email = user_object.email
                subject = 'Password Reset'
                # use double-quotes instead of single-quotes
                content = (
                    "<p>Hi {}, <br><br> The password for username <b>{}</b> has been reset to: <b>{}</b> <br><br>"
                    "Please log in using this password and reset your "
                    "password using the settings page on the top right navigation bar.<br><br>"
                    "Sincerely, <br>The Dendi Team</p>".format(
                        user_object.first_name, str(user_object.username), new_password))

                view_scripts.send_email_sendgrid(from_email, to_email, subject,
                                                 content)

                password_reset = 'Your password has been reset. Please check your email.'
                messages.add_message(request, messages.INFO, password_reset)
                return redirect('landing_page')
            else:
                password_reset = 'Your email is not on file. ' \
                                 'Please contact your laboratory\'s support team to retrieve your password.'
                messages.add_message(request, messages.ERROR, password_reset)
                return redirect('landing_page')
        return render(request, 'forgot-password.html', {'form': form})


class Test404View(TemplateView):
    template_name = '404.html'


class Test500View(TemplateView):
    template_name = '500.html'


class DashboardView(LISLoginRequiredMixin, View):
    login_url = '/login/'

    def get(self, request):
        # lab setup prompt
        lab_status = get_lab_setup_status(request)

        def get_monthly_tests_ordered(test_types_queryset, provider):
            test_type_dict = {}
            for test_type in test_types_queryset:
                # must filter by this calendar year as well
                tests = orders_models.Test.objects.filter(test_type=test_type,
                                                          created_date__year=datetime.datetime.now().year) \
                    .annotate(month=TruncMonth('created_date')).values('month', 'test_type__name') \
                    .annotate(count=Count('id')).order_by()
                if provider:
                    tests = tests.filter(order__provider=provider)

                # convert queryset to list for faster operations
                # a list of tests for each test type
                test_list = list(tests)

                # initiate empty list of zeroes corresponding to each month
                monthly_count_list = [0] * 12

                for test in test_list:
                    # given use month as the list index - 1 since lists start at 0
                    month = test['month'].month
                    monthly_count_list[month - 1] = test['count']

                # populate test_type_dict with test type name and a monthly count of the tests
                test_type_dict[test_type.name] = monthly_count_list

            return test_type_dict

        def get_summary_statistics():
            order_count = orders_models.Order.objects.all().count()
            account_count = accounts_models.Account.objects.all().count()
            provider_count = accounts_models.Provider.objects.all().count()
            patient_count = patients_models.Patient.objects.all().count()

            return order_count, account_count, provider_count, patient_count

        ##############################################################################################################

        if request.user.user_type == 10:
            provider = accounts_models.Provider.objects.get(user__id=request.user.id)
        else:
            provider = None
        if request.user.user_type == 9:
            account = accounts_models.Account.objects.get(user__id=request.user.id)
        else:
            account = None
        if request.user.user_type == 11:
            collector = accounts_models.Collector.objects.get(user__id=request.user.id)
        else:
            collector = None

        # if provider, then show provider template
        if provider:
            template = 'dashboard_provider.html'

            reported_order_count = orders_models.Order.objects.filter(reported=True,
                                                                      provider=provider,
                                                                      is_archived_by_provider=False). \
                exclude(issues__is_resolved=False). \
                order_by('-submitted_date', '-code').count()

            if provider.signature:
                has_provider_signature = True
            else:
                has_provider_signature = False

            context = {'reported_order_count': reported_order_count,
                       'has_provider_signature': has_provider_signature,
                       'provider': provider}
        elif account:
            template = 'dashboard_provider.html'

            reported_order_count = orders_models.Order.objects.filter(reported=True,
                                                                      account=account,
                                                                      is_archived_by_provider=False). \
                exclude(issues__is_resolved=False). \
                order_by('-submitted_date', '-code').count()

            context = {'reported_order_count': reported_order_count,
                       'has_provider_signature': True}
        elif collector:
            template = 'dashboard_provider.html'
            reported_order_count = orders_models.Order.objects.filter(reported=True,
                                                                      account__collectors=collector,
                                                                      is_archived_by_provider=False). \
                exclude(issues__is_resolved=False). \
                order_by('-submitted_date', '-code').count()

            context = {'reported_order_count': reported_order_count,
                       'has_provider_signature': True}
        # if not provider, then show lab template
        else:
            template = 'dashboard.html'

            test_types = orders_models.TestType.objects.filter()
            empty = orders_models.Order.objects.count() == 0
            if request.tenant.lab_type == 'Other':
                monthly_tests_ordered = get_monthly_tests_ordered(test_types, provider)
            else:
                monthly_tests_ordered = None
            summary_statistics = get_summary_statistics()

            num_unreceived = orders_models.Order.objects.filter(submitted=True, received_date__isnull=True)
            num_pending_results = orders_models.Order.objects.filter(submitted=True,
                                                                     received_date__isnull=False, completed=False)
            num_pending_approval = orders_models.Order.objects.filter(completed=True, reported=False)

            order_status_dict = {'unreceived': num_unreceived.count(),
                                 'pending_results': num_pending_results.count(),
                                 'pending_approval': num_pending_approval.count()}

            # aggregation template
            # Item.objects.values('date__year', 'date__month').annotate(count=Count('pk'))

            daily_orders = orders_models.Order.objects.filter(received_date__isnull=False).all().order_by(
                'received_date')
            date_orders_dict = {}
            date_orders_list = []
            for order in daily_orders:
                received_datetime = functions.convert_utc_to_tenant_timezone(request, order.received_date)
                received_date = received_datetime.date()
                # weird chart requirement
                unix_time = datetime.datetime.combine(received_date,
                                                      datetime.datetime.min.time()).timestamp() * 1000
                try:
                    date_orders_dict[unix_time] += 1
                except KeyError:
                    date_orders_dict[unix_time] = 1
            for key in date_orders_dict:
                date_orders_list.append([key, date_orders_dict[key]])
            ############################################################################################################
            context = {'lab_status': lab_status,
                       'tests': monthly_tests_ordered,
                       'summary_statistics': summary_statistics,
                       'order_status_dict': order_status_dict,
                       'daily_orders_list': date_orders_list,
                       'empty': empty}
        return render(request, template, context=context)

    @transaction.atomic
    def post(self, request):
        order_uuids = request.POST.getlist('is_archived')
        if 'batch-print-reports' in request.POST:
            if len(order_uuids) <= 1:
                messages.success(request, "Please select at least two reports to generate a batch report.")
                return redirect('dashboard')

            try:
                provider = accounts_models.Provider.objects.get(user__id=request.user.id)
            except accounts_models.Provider.DoesNotExist:
                provider = None

            try:
                account = accounts_models.Account.objects.get(user__id=request.user.id)
            except accounts_models.Account.DoesNotExist:
                account = None

            try:
                collector = accounts_models.Collector.objects.get(user__id=request.user.id)
            except accounts_models.Collector.DoesNotExist:
                collector = None

            template = 'dashboard_provider.html'

            if provider:
                reported_order_count = orders_models.Order.objects.filter(reported=True,
                                                                          provider=provider,
                                                                          is_archived_by_provider=False). \
                    exclude(issues__is_resolved=False). \
                    order_by('-submitted_date', '-code').count()
                if provider.signature:
                    has_provider_signature = True
                else:
                    has_provider_signature = False
                batch_report = orders_models.BatchReports(provider=provider)
                context = {'reported_order_count': reported_order_count,
                           'has_provider_signature': has_provider_signature,
                           'provider': provider,
                           'batch_report': batch_report}
                file_name = provider.uuid
            if account:
                reported_order_count = orders_models.Order.objects.filter(reported=True,
                                                                          account=account,
                                                                          is_archived_by_provider=False). \
                    exclude(issues__is_resolved=False). \
                    order_by('-submitted_date', '-code').count()

                batch_report = orders_models.BatchReports(account=account)
                context = {'reported_order_count': reported_order_count,
                           'has_provider_signature': True,
                           'batch_report': batch_report}
                file_name = account.uuid
            if collector:
                reported_order_count = orders_models.Order.objects.filter(reported=True,
                                                                          account__collectors=collector,
                                                                          is_archived_by_provider=False). \
                    exclude(issues__is_resolved=False). \
                    order_by('-submitted_date', '-code').count()
                batch_report = orders_models.BatchReports(collector=collector)
                context = {'reported_order_count': reported_order_count,
                           'has_provider_signature': True,
                           'batch_report': batch_report}
                file_name = collector.uuid
            if account or provider or collector:
                writer = PdfWriter()
                orders_list = []
                for uuid in order_uuids:
                    try:
                        order = get_object_or_404(orders_models.Order, uuid=uuid)
                        orders_list.append(order)
                        report = \
                            orders_models.Report.objects.filter(order=order, preliminary=False).order_by(
                                '-created_date')[0]

                        writer.addpages(PdfReader(report.pdf_file.open('rb')).pages)
                    except Http404:
                        print('Order does not exist')

                save_file_name = "batch_reports/{}/{}/{}.pdf".format(request.tenant.name,
                                                                     file_name,
                                                                     timezone.now())
                with tempfile.NamedTemporaryFile() as temp:
                    writer.write(temp.name)
                    batch_report.pdf_file.save(save_file_name, temp)
                batch_report.orders.set(orders_list)
                messages.success(request, "Batch report successfully generated")
                return render(request, template, context=context)
            else:
                return redirect('dashboard')
        else:
            for uuid in order_uuids:
                order_obj = orders_models.Order.objects.get(uuid=uuid)
                order_obj.is_archived_by_provider = True
                order_obj.save(update_fields=['is_archived_by_provider'])

        return redirect('dashboard')


class DeleteInboxView(ProviderMixin, View):
    @transaction.atomic
    def get(self, request):
        orders = orders_models.Order.objects.all()
        for order in orders:
            order.is_archived_by_provider = True
            order.save()
        return redirect('dashboard')


class DeleteInboxOrderView(ProviderMixin, View):
    @transaction.atomic
    def get(self, request, order_code):
        order_obj = orders_models.Order.objects.get(code=order_code)
        order_obj.is_archived_by_provider = True
        order_obj.save()
        return redirect('dashboard')


class UserListView(LabAdminMixin, View):
    def get(self, request):
        admins = lab_tenant_models.LabAdmin.objects.filter()
        collaborators = lab_tenant_models.LabCollaborator.objects.filter()

        return render(request, 'user_list.html', {'admins': admins, 'collaborators': collaborators})


class UserCreateView(LabAdminMixin, View):
    def get(self, request):
        form = forms.UserCreateForm()
        return render(request, 'user_create.html', {'form': form})

    @transaction.atomic
    def post(self, request):
        form = forms.UserCreateForm(request.POST)

        if form.is_valid():
            last_name = form.cleaned_data['last_name']
            first_name = form.cleaned_data['first_name']
            email = form.cleaned_data['email']
            password = form.cleaned_data['confirm_password']
            user_type = int(form.cleaned_data['user_type'])

            # create User type
            user_obj = lab_tenant_models.User(last_name=last_name,
                                              first_name=first_name,
                                              username=email,
                                              email=email,
                                              user_type=user_type)
            # save User object first
            user_obj.set_password(password)
            user_obj.save()

            if user_type == 1:
                lab_admin_obj = lab_tenant_models.LabAdmin(user=user_obj)
                lab_admin_obj.save()
            elif user_type == 2:
                lab_collaborator_obj = lab_tenant_models.LabCollaborator(user=user_obj)
                lab_collaborator_obj.save()
            return redirect('user_list')

        return render(request, 'user_create.html', {'form': form})


class LabAdminUpdateView(LabAdminMixin, View):
    def get(self, request, lab_admin_uuid):
        lab_admin = get_object_or_404(lab_tenant_models.LabAdmin, uuid=lab_admin_uuid)

        form = forms.UserUpdateForm(initial={'last_name': lab_admin.user.last_name,
                                             'first_name': lab_admin.user.first_name,
                                             'email': lab_admin.user.email,
                                             'user_type': lab_admin.user.user_type})

        return render(request, 'user_update.html', {'form': form})

    def post(self, request, lab_admin_uuid):
        lab_admin = get_object_or_404(lab_tenant_models.LabAdmin, uuid=lab_admin_uuid)
        user = lab_admin.user

        form = forms.UserUpdateForm(request.POST)

        if form.is_valid():
            last_name = form.cleaned_data['last_name']
            first_name = form.cleaned_data['first_name']

            # return None if no password is supplied by form, since it doesn't get updated
            password = form.cleaned_data.get('confirm_password')
            if password:
                user.set_password(password)

            # user types can be updated
            user_type = int(form.cleaned_data['user_type'])
            if user_type == 2:
                # if user type is updated, change the actual user type,
                # delete the associated LabAdmin object
                # then create a new LabCollaborator object
                lab_admin.user.user_type = user_type
                lab_admin.delete()
                lab_collaborator_obj = lab_tenant_models.LabCollaborator(user=user)
                lab_collaborator_obj.save()

            # save user data first
            user.last_name = last_name
            user.first_name = first_name
            user.save()

            return redirect('user_list')
        return render(request, 'user_update.html', {'form': form})


class LabAdminDeleteView(LabAdminMixin, View):
    def get(self, request, lab_admin_uuid):
        lab_admin = lab_tenant_models.LabAdmin.objects.get(uuid=lab_admin_uuid)

        lab_admin.is_active = False
        lab_admin.save()
        # Django's default user objects have is_active boolean
        lab_admin.user.is_active = False
        lab_admin.user.save()

        return redirect('user_list')


class LabCollaboratorDeleteView(LabAdminMixin, View):
    def get(self, request, lab_collaborator_uuid):
        lab_collaborator = lab_tenant_models.LabCollaborator.objects.get(uuid=lab_collaborator_uuid)

        lab_collaborator.is_active = False
        lab_collaborator.save()
        # Django's default user objects have is_active boolean
        lab_collaborator.user.is_active = False
        lab_collaborator.user.save()

        return redirect('user_list')


class LabCollaboratorUpdateView(LabAdminMixin, View):
    def get(self, request, lab_collaborator_uuid):
        lab_collaborator = get_object_or_404(lab_tenant_models.LabCollaborator, uuid=lab_collaborator_uuid)

        form = forms.UserUpdateForm(initial={'last_name': lab_collaborator.user.last_name,
                                             'first_name': lab_collaborator.user.first_name,
                                             'email': lab_collaborator.user.email,
                                             'user_type': lab_collaborator.user.user_type})

        return render(request, 'user_update.html', {'form': form})

    def post(self, request, lab_collaborator_uuid):
        lab_collaborator = get_object_or_404(lab_tenant_models.LabCollaborator, uuid=lab_collaborator_uuid)
        user = lab_collaborator.user

        form = forms.UserUpdateForm(request.POST)

        if form.is_valid():
            last_name = form.cleaned_data['last_name']
            first_name = form.cleaned_data['first_name']

            # return None if no password is supplied by form, since it doesn't get updated
            password = form.cleaned_data.get('confirm_password')
            if password:
                user.set_password(password)

            # user types can be updated
            user_type = int(form.cleaned_data['user_type'])
            if user_type == 1:
                # if user type is updated, change the actual user type,
                # delete the associated LabAdmin object
                # then create a new LabCollaborator object
                lab_collaborator.user.user_type = user_type
                lab_collaborator.delete()
                lab_admin_obj = lab_tenant_models.LabAdmin(user=user)
                lab_admin_obj.save()

            user.last_name = last_name
            user.first_name = first_name
            user.save()

            return redirect('user_list')
        return render(request, 'user_update.html', {'form': form})


class SettingsView(LISLoginRequiredMixin, View):
    def get(self, request):
        return render(request, 'settings.html')

    def post(self, request):
        user_object = User.objects.get(username=request.user.username)
        form = SettingsPasswordForm(request.POST)
        current_password = request.POST.get('current_password')
        password_error = '* Current password is incorrect. Please try again.'
        success_message = 'Password changed successfully. Please log in again.'

        if not user_object.check_password(current_password):
            return render(
                request,
                'settings.html',
                {
                    'password_error': password_error,
                    'form': form
                },
            )
        if not form.is_valid():
            return render(request, 'settings.html', {'form': form})

        # set new password from input
        user_object.set_password(form.cleaned_data['confirm_new_password'])
        user_object.save()

        return render(request, 'settings.html',
                      {'success_message': success_message})


class AdvancedSettingsView(LabAdminMixin, View):
    def get(self, request):
        settings = request.tenant.settings
        use_docraptor = settings.get('use_docraptor', False)
        skip_insurance = settings.get('skip_insurance', False)
        skip_patient_history = settings.get('skip_patient_history', False)
        require_icd10 = settings.get('require_icd10', False)
        require_collected_by = settings.get('require_collected_by', False)
        hide_instrument_integration = settings.get('hide_instrument_integration', False)
        hide_fda_tests = settings.get('hide_fda_tests', False)
        hide_specimen_validity = settings.get('hide_specimen_validity', False)
        show_prescribed_medication = settings.get('show_prescribed_medication', False)
        show_research_consent = settings.get('show_research_consent', False)
        show_patient_result_request = settings.get('show_patient_result_request', False)
        enable_accession_number = settings.get('enable_accession_number', False)
        enable_date_encoded_accession_number = settings.get('enable_date_encoded_accession_number', False)
        enable_accession_number_editing = settings.get('enable_accession_number_editing', False)
        enable_patient_vitals = settings.get('enable_patient_vitals', False)
        use_zebra_printer = settings.get('use_zebra_printer', False)
        show_abn_on_req_form = settings.get('show_abn_on_req_form', False)
        enable_individual_analytes = settings.get('enable_individual_analytes', False)
        enable_document_scanning = settings.get('enable_document_scanning', False)
        enable_document_uploading = settings.get('enable_document_uploading', False)
        use_accession_number_barcode = settings.get('use_accession_number_barcode', False)
        show_ten_digit_barcode = settings.get('show_ten_digit_barcode', False)
        hide_report_approved_by = settings.get('hide_report_approved_by', False)
        confirm_order_receipt = settings.get('confirm_order_receipt', False)
        use_two_factor_auth = settings.get('use_two_factor_auth', False)
        automatically_print_labels = settings.get('automatically_print_labels', False)
        form = forms.AdvancedSettingsForm(initial={
            'use_docraptor': use_docraptor,
            'skip_insurance': skip_insurance,
            'skip_patient_history': skip_patient_history,
            'require_icd10': require_icd10,
            'require_collected_by': require_collected_by,
            'hide_instrument_integration': hide_instrument_integration,
            'hide_fda_tests': hide_fda_tests,
            'hide_specimen_validity': hide_specimen_validity,
            'show_prescribed_medication': show_prescribed_medication,
            'show_research_consent': show_research_consent,
            'show_patient_result_request': show_patient_result_request,
            'enable_accession_number': enable_accession_number,
            'enable_date_encoded_accession_number': enable_date_encoded_accession_number,
            'enable_accession_number_editing': enable_accession_number_editing,
            'enable_patient_vitals': enable_patient_vitals,
            'use_zebra_printer': use_zebra_printer,
            'enable_individual_analytes': enable_individual_analytes,
            'enable_document_scanning': enable_document_scanning,
            'enable_document_uploading': enable_document_uploading,
            'show_abn_on_req_form': show_abn_on_req_form,
            'use_accession_number_barcode': use_accession_number_barcode,
            'show_ten_digit_barcode': show_ten_digit_barcode,
            'hide_report_approved_by': hide_report_approved_by,
            'confirm_order_receipt': confirm_order_receipt,
            'use_two_factor_auth': use_two_factor_auth,
            'automatically_print_labels': automatically_print_labels
        })
        return render(request, 'advanced_settings.html', {'form': form})

    def post(self, request):
        form = forms.AdvancedSettingsForm(request.POST)
        settings = request.tenant.settings

        if form.is_valid():
            use_docraptor = form.cleaned_data['use_docraptor']
            skip_insurance = form.cleaned_data['skip_insurance']
            skip_patient_history = form.cleaned_data['skip_patient_history']
            require_icd10 = form.cleaned_data['require_icd10']
            require_collected_by = form.cleaned_data['require_collected_by']
            hide_instrument_integration = form.cleaned_data['hide_instrument_integration']
            hide_fda_tests = form.cleaned_data['hide_fda_tests']
            hide_specimen_validity = form.cleaned_data['hide_specimen_validity']
            show_prescribed_medication = form.cleaned_data['show_prescribed_medication']
            show_research_consent = form.cleaned_data['show_research_consent']
            show_patient_result_request = form.cleaned_data['show_patient_result_request']
            enable_accession_number = form.cleaned_data['enable_accession_number']
            enable_date_encoded_accession_number = form.cleaned_data['enable_date_encoded_accession_number']
            enable_accession_number_editing = form.cleaned_data['enable_accession_number_editing']
            enable_patient_vitals = form.cleaned_data['enable_patient_vitals']
            use_zebra_printer = form.cleaned_data['use_zebra_printer']
            enable_document_scanning = form.cleaned_data['enable_document_scanning']
            enable_document_uploading = form.cleaned_data['enable_document_uploading']
            show_abn_on_req_form = form.cleaned_data['show_abn_on_req_form']
            enable_individual_analytes = form.cleaned_data['enable_individual_analytes']
            use_accession_number_barcode = form.cleaned_data['use_accession_number_barcode']
            show_ten_digit_barcode = form.cleaned_data['show_ten_digit_barcode']
            hide_report_approved_by = form.cleaned_data['hide_report_approved_by']
            confirm_order_receipt = form.cleaned_data['confirm_order_receipt']
            automatically_print_labels = form.cleaned_data['automatically_print_labels']
            use_two_factor_auth = form.cleaned_data['use_two_factor_auth']

            settings['use_docraptor'] = use_docraptor
            settings['skip_insurance'] = skip_insurance
            settings['skip_patient_history'] = skip_patient_history
            settings['require_icd10'] = require_icd10
            settings['require_collected_by'] = require_collected_by
            settings['hide_instrument_integration'] = hide_instrument_integration
            settings['show_prescribed_medication'] = show_prescribed_medication
            settings['hide_fda_tests'] = hide_fda_tests
            settings['hide_specimen_validity'] = hide_specimen_validity
            settings['show_research_consent'] = show_research_consent
            settings['show_patient_result_request'] = show_patient_result_request
            settings['enable_accession_number'] = enable_accession_number
            settings['enable_date_encoded_accession_number'] = enable_date_encoded_accession_number
            settings['enable_accession_number_editing'] = enable_accession_number_editing
            settings['enable_patient_vitals'] = enable_patient_vitals
            settings['use_zebra_printer'] = use_zebra_printer
            settings['show_abn_on_req_form'] = show_abn_on_req_form
            settings['enable_individual_analytes'] = enable_individual_analytes
            settings['enable_document_scanning'] = enable_document_scanning
            settings['enable_document_uploading'] = enable_document_uploading
            settings['use_accession_number_barcode'] = use_accession_number_barcode
            settings['show_ten_digit_barcode'] = show_ten_digit_barcode
            settings['hide_report_approved_by'] = hide_report_approved_by
            settings['confirm_order_receipt'] = confirm_order_receipt
            settings['use_two_factor_auth'] = use_two_factor_auth
            settings['automatically_print_labels'] = automatically_print_labels

            request.tenant.save(update_fields=['settings'])
            messages.success(request, "Settings saved.")
            return render(request, 'advanced_settings.html', {'form': form})
        else:
            return render(request, 'advanced_settings.html', {'form': form})


class BillingView(LabMemberMixin, View):

    def get(self, request):
        return render(request, 'billing.html')


class SetupView(LabMemberMixin, View):
    def get(self, request):
        context = get_lab_setup_status(request)

        return render(request, 'setup.html', context)

    def post(self, request):
        context = get_lab_setup_status(request)

        def validate_file_extension(file):
            valid_extensions = ['image/jpg', 'image/jpeg', 'image/png', 'image/svg', 'image/svg+xml']

            if file.content_type in valid_extensions:
                return True
            else:
                context['file_upload_error'] = 'File must be the correct type.'

        # upload laboratory logo
        uploaded_file = request.FILES.get('lab-logo', False)

        # without conditional statement, throws a key error
        if uploaded_file:
            if validate_file_extension(uploaded_file):
                client = request.tenant
                client.logo_file.save(uploaded_file.name, uploaded_file)
                return redirect('setup')
            else:
                return render(request, 'setup.html', context)
        return redirect('setup')


class LaboratoryUpdateView(LabAdminMixin, View):
    def get(self, request):
        laboratory = request.tenant
        form = forms.LaboratoryUpdateForm(initial={'clia_number': laboratory.clia_number,
                                                   'clia_director': laboratory.clia_director,
                                                   'address_1': laboratory.address_1,
                                                   'address_2': laboratory.address_2,
                                                   'phone_number': laboratory.phone_number,
                                                   'fax_number': laboratory.fax_number,
                                                   'website': laboratory.website,
                                                   'timezone': laboratory.timezone})
        return render(request, 'laboratory_update.html', {'form': form})

    def post(self, request):
        form = forms.LaboratoryUpdateForm(request.POST)

        if form.is_valid():
            clia_number = form.cleaned_data['clia_number']
            clia_director = form.cleaned_data['clia_director']
            address_1 = form.cleaned_data['address_1']
            address_2 = form.cleaned_data['address_2']
            phone_number = form.cleaned_data['phone_number']
            fax_number = form.cleaned_data['fax_number']
            website = form.cleaned_data['website']
            timezone = form.cleaned_data.get('timezone')

            request.tenant.clia_number = clia_number
            request.tenant.clia_director = clia_director
            request.tenant.address_1 = address_1
            request.tenant.address_2 = address_2
            request.tenant.phone_number = phone_number
            request.tenant.fax_number = fax_number
            request.tenant.website = website
            request.tenant.timezone = timezone
            request.tenant.save()

        return redirect('setup')


class CLIATestTypeListView(LISLoginRequiredMixin, View):
    """
    FDA-curated list of CLIA test types. A more accurate name could be "FDATestTypeListView"...
    """

    def get(self, request):
        clia_test_type_specialty_list = models.CLIATestTypeSpecialty.objects.all()
        last_updated_log = models.CLIATestTypeUpdateLog.objects.last()
        if last_updated_log:
            last_updated = last_updated_log.update_datetime
        else:
            last_updated = None
        return render(request, 'clia_test_type_list.html',
                      {'clia_test_type_specialty_list': clia_test_type_specialty_list,
                       'last_updated': last_updated})


class CLIASampleTypeDetailsView(LabMemberMixin, View):
    def get(self, request, clia_sample_type_uuid):
        clia_sample_type = get_object_or_404(models.CLIASampleType, uuid=clia_sample_type_uuid)
        return render(request, 'clia_sample_type_details.html', {'clia_sample_type': clia_sample_type})


class TestMethodAutocomplete(autocomplete.Select2QuerySetView):
    # https://django-autocomplete-light.readthedocs.io/en/master/tutorial.html
    # Displaying selected result differently than in list

    def get_queryset(self):
        # General chemistry should be the first one to show up
        qs = models.TestMethod.objects.annotate(
            relevancy=Count(
                Case(When(name='General Chemistry', then=1))),
        ).order_by('-relevancy')

        if self.q:
            qs = qs.filter(name__icontains=self.q)
        return qs


class TestTypeSpecialtyAutocomplete(autocomplete.Select2QuerySetView):
    # https://django-autocomplete-light.readthedocs.io/en/master/tutorial.html
    # Displaying selected result differently than in list

    def get_queryset(self):
        # General chemistry should be the first one to show up
        qs = models.CLIATestTypeSpecialty.objects.all()

        if self.q:
            qs = qs.filter(specialty_name__icontains=self.q)
        return qs


class TestTargetAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs = models.TestTarget.objects.all()

        control = self.forwarded.get('control', None)
        if control:
            try:
                qs = qs.filter(id__in=[x.test_target.id for x in control.known_values.all()])
            except:
                control = orders_models.Control.objects.get(id=int(control))
                qs = qs.filter(id__in=[x.test_target.id for x in control.known_values.all()])

        if self.q:
            qs = qs.filter(name__icontains=self.q)
        return qs


class CliaTestTypeAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs = models.CLIATestType.objects.all()

        if self.q:
            qs = qs.filter(document_number__icontains=self.q) | \
                 qs.filter(test_system_name__icontains=self.q) | \
                 qs.filter(analyte_name__icontains=self.q)
        return qs


class OffsiteLaboratoryAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs = models.OffsiteLaboratory.objects.all()

        if self.q:
            qs = qs.filter(name__icontains=self.q)
        return qs


class InstrumentAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs = models.Instrument.objects.all()

        if self.q:
            qs = qs.filter(name__icontains=self.q)
        return qs


class PayerAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        payer_type = self.forwarded.get('payer_type', None)
        qs = models.Payer.objects.all()

        if payer_type:
            qs = qs.filter(payer_type=payer_type)

        if self.q:
            qs = qs.filter(name__icontains=self.q) | \
                 qs.filter(abbreviated_name__icontains=self.q)
            qs = qs.distinct()
        return qs


class InitializeDataView(LISLoginRequiredMixin, View):
    def get(self, request):
        num_tests = orders_models.Test.objects.count()
        if num_tests == 0:
            initialize_data()
        return redirect('landing_page')


class ProviderSignatureUpdateView(LISLoginRequiredMixin, View):
    def get(self, request, provider_uuid):
        provider = accounts_models.Provider.objects.get(uuid=provider_uuid)

        if provider.signature:
            num_steps_done = 1
        else:
            num_steps_done = 0
        num_steps = 1
        context = {
            'provider': provider,
            'num_steps_done': num_steps_done,
            'num_steps': num_steps
        }
        return render(request, 'provider_signature.html', context)

    def post(self, request, provider_uuid):
        provider = accounts_models.Provider.objects.get(uuid=provider_uuid)

        if provider.signature:
            num_steps_done = 1
        else:
            num_steps_done = 0
        num_steps = 1
        context = {
            'provider': provider,
            'num_steps_done': num_steps_done,
            'num_steps': num_steps
        }

        def validate_file_extension(file):
            valid_extensions = ['image/jpg', 'image/jpeg', 'image/png', 'image/svg', 'image/svg+xml']

            if file.content_type in valid_extensions:
                return True
            else:
                context['file_upload_error'] = 'File must be the correct type.'

        # upload signature
        drawn_signature = request.POST.get('canvasData', False)
        if drawn_signature:
            with tempfile.NamedTemporaryFile() as temp:
                temp.write(base64.b64decode(drawn_signature.split(',')[1]))
                provider.signature.save("signature.png", temp)
        else:
            uploaded_file = request.FILES.get('provider-signature', False)
            # without conditional statement, throws a key error
            if uploaded_file:
                if validate_file_extension(uploaded_file):
                    provider.signature.save(uploaded_file.name, uploaded_file)
                    return redirect('provider_signature', provider_uuid=provider_uuid)
                else:
                    return render(request, 'provider_signature.html', context)
        return redirect('provider_details', provider_uuid=provider_uuid)


class ProviderSignatureDeleteView(LISLoginRequiredMixin, View):
    def get(self, request, provider_uuid):
        provider = accounts_models.Provider.objects.get(uuid=provider_uuid)

        if provider.signature:
            provider.signature.delete()
            provider.save()

        return redirect('provider_signature', provider_uuid=provider_uuid)


class MedicationListView(LabMemberMixin, View):
    def get(self, request):
        return render(request, 'medication_list.html', {})


class PayerListView(LabAdminMixin, View):
    def get(self, request):
        payers = patients_models.Payer.objects.all()
        payer_choices = PAYER_CHOICES

        context = {
            'payers': payers,
            'payer_choices': payer_choices
        }
        return render(request, 'payer_list.html', context)


class PayerCreateView(LabAdminMixin, View):
    def get(self, request):
        form = PayerForm()

        context = {
            'form': form,
        }
        return render(request, 'payer_create.html', context)

    @transaction.atomic
    def post(self, request):

        form = PayerForm(request.POST)
        context = {
            'form': form,
        }
        if form.is_valid():
            payer_type = form.cleaned_data['payer_type']
            payer_name = form.cleaned_data['payer_name']
            payer_address1 = form.cleaned_data['payer_address1']
            payer_address2 = form.cleaned_data['payer_address2']
            payer_zip_code = form.cleaned_data['payer_zip_code']
            payer_code = form.cleaned_data['payer_code']
            phone_number = form.cleaned_data['phone_number']
            abbreviated_name = form.cleaned_data['abbreviated_name']
            patients_models.Payer.objects.create(
                payer_type=payer_type,
                payer_code=payer_code,
                address_1=payer_address1,
                address_2=payer_address2,
                name=payer_name,
                zip_code=payer_zip_code,
                phone_number=phone_number,
                abbreviated_name=abbreviated_name
            )

            return redirect('payer_list')
        else:
            return render(request, 'payer_create.html', context)


class PayerDetailsView(LabAdminMixin, View):
    def get(self, request, payer_uuid):
        payer = get_object_or_404(patients_models.Payer, uuid=payer_uuid)
        context = {'payer': payer}

        return render(request, 'payer_details.html', context)


class PayerUpdateView(LabAdminMixin, View):
    def get(self, request, payer_uuid):
        payer = get_object_or_404(patients_models.Payer, uuid=payer_uuid)

        form = PayerForm(initial={
            'payer_type': payer.payer_type,
            'payer_name': payer.name,
            'payer_address1': payer.address_1,
            'payer_address2': payer.address_2,
            'payer_zip_code': payer.zip_code,
            'payer_code': payer.payer_code,
            'phone_number': payer.phone_number,
            'abbreviated_name': payer.abbreviated_name
        })
        update = True
        context = {'form': form, 'update': update}

        return render(request, 'payer_create.html', context)

    @transaction.atomic
    def post(self, request, payer_uuid):
        payer = get_object_or_404(patients_models.Payer, uuid=payer_uuid)
        form = PayerForm(request.POST)
        context = {
            'form': form,
        }
        if form.is_valid():
            payer.payer_type = form.cleaned_data['payer_type']
            payer.name = form.cleaned_data['payer_name']
            payer.address_1 = form.cleaned_data['payer_address1']
            payer.address_2 = form.cleaned_data['payer_address2']
            payer.zip_code = form.cleaned_data['payer_zip_code']
            payer.payer_code = form.cleaned_data['payer_code']
            payer.phone_number = form.cleaned_data['phone_number']
            payer.abbreviated_name = form.cleaned_data['abbreviated_name']

            payer.save()

            return redirect('payer_details', payer_uuid)
        else:
            return render(request, 'payer_create.html', context)


class PayerArchivedListView(LabAdminMixin, View):
    def get(self, request):
        payer_choices = PAYER_CHOICES

        context = {
            'payer_choices': payer_choices
        }

        return render(request, 'payer_archived_list.html', context)


class PayerUndoArchiveView(LabAdminMixin, View):
    def get(self, request, payer_uuid):
        payer = patients_models.Payer.all_objects.get(uuid=payer_uuid)
        payer.is_active = True
        payer.save(update_fields=['is_active'])

        messages.success(request, 'Payer: {} unarchived.'.format(payer.name))
        return redirect('payer_archived_list')


class PayerDeleteView(LabAdminMixin, View):
    def get(self, request, payer_uuid):
        payer = get_object_or_404(patients_models.Payer, uuid=payer_uuid)
        payer.is_active = False
        payer.save(update_fields=['is_active'])

        messages.success(request, 'Payer: {} archived successfully.'.format(payer.name))
        return redirect('payer_list')


class AWSDebugView(LabMemberMixin, View):
    """
    View used to debug migration to AWS from Heroku.
    Has base cases related to channels, workers, etc
    """
    def get(self, request):
        context = {}
        return render(request, 'aws_debug.html', context)


class AWSDebugIdentifierView(LabMemberMixin, View):
    """
    Base case for using identifier
    """
    def get(self, request):
        """
        A snippet from order create process
        :param request:
        :return:
        """
        order_result = tasks.create_order_with_code.apply_async(args=[request.tenant.id], queue='identifiers')
        order_id = order_result.get()

        accession_number_result = tasks.assign_accession_number.apply_async(
            args=[order_id, request.tenant.id],
            queue='identifiers')
        accession_number = accession_number_result.get()

        order = orders_models.Order.objects.get(id=order_id)
        context = {'order': order}
        return render(request, 'aws_debug_identifier.html', context)
