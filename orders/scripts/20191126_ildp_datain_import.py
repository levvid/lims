import os
import django



os.environ['DJANGO_SETTINGS_MODULE'] = 'main.settings'
django.setup()

from tenant_schemas.utils import tenant_context
import clients.models as clients_models
from orders import models, functions, generate_report_docraptor, generate_report, generate_results_out_hl7, generate_billing_hl7
from orders import tasks
from orders.functions import import_instrument_data

DATA_IN_UUID = '513aee49-28a5-4c12-aeae-f1ae4b78c2d0'


def main():
    """
    :return:
    """
    tenant = clients_models.Client.objects.get(schema_name='ildptest3')
    with tenant_context(tenant):
        import_instrument_data(DATA_IN_UUID, identifier='accession_number')


if __name__ == '__main__':
    main()


