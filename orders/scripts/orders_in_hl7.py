from orders import tasks


def call_orders_in_task():
    tasks.get_hl7_orders_in.apply_async(
        args=[],
        queue='default'
    )


if __name__ == '__main__':
    """Create task to create orders from hl7 messages in tenant s3 bucket(s)"""
    call_orders_in_task()
