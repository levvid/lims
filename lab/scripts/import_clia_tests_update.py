import codecs
import csv
import datetime
import django
import logging
import os
import sys
import tempfile

from django.utils.timezone import make_aware
from io import BytesIO
from zipfile import ZipFile
from urllib.request import urlopen

os.environ['DJANGO_SETTINGS_MODULE'] = 'main.settings'
django.setup()

from .. import models

CLIA_ZIP_URL = 'http://www.accessdata.fda.gov/premarket/ftparea/clia_detail.zip'
# to optimize runtime
DATA_AGE_CUTOFF = 180


def main():
    """
    A script to import/update/maintain CLIATestType and CLIATestTypeSpecialty tables

    This script deprecates lab.scripts.import_clia_tests
    :return:
    """
    if models.CLIATestTypeSpecialty.objects.all().count == 0:
        logging.debug("initializing CLIATestTypeSpecialty data...")
        fill_clia_test_type_specialty()

    logging.debug("downloading latest data...")
    clia_test_types = download_clia_tests(CLIA_ZIP_URL)

    if run_update(clia_test_types):
        logging.debug("running CLIATestType update")
        run_status = update_clia_test_type_table(clia_test_types, date_effective_within_days=DATA_AGE_CUTOFF)
        models.CLIATestTypeUpdateLog(num_new_rows=run_status['new_row_count']).save()
        logging.debug("update completed")
        logging.debug(str(run_status))
    else:
        logging.debug("CLIATestType update not needed")


def download_clia_tests(clia_zip_file_url):
    """
    download, unzip, and parse into a list of dicts
    :param clia_zip_file_url:
    :return: [Dict]
    """
    response = urlopen(clia_zip_file_url)
    zipfile = ZipFile(BytesIO(response.read()))
    text_file_name = zipfile.namelist()[0]  # should be 'clia_detail.txt'

    with tempfile.TemporaryDirectory() as temporary_directory:
        file_name = zipfile.extract(text_file_name, path=temporary_directory)
        csv_reader = csv.DictReader(codecs.open(file_name, 'r', encoding='utf-8', errors='ignore'),
                                    delimiter='|', quotechar='"')
        csv_reader = list(csv_reader)
        for row in csv_reader:
            row['DATE_EFFECTIVE'] = datetime.datetime.strptime(row['DATE_EFFECTIVE'][:10], '%Y/%m/%d').date()
        return list(csv_reader)


def run_update(downloaded_clia_test_types):
    """
    :return: True if update should be run
    """
    downloaded_data_count = len(downloaded_clia_test_types)
    current_data_count = models.CLIATestType.objects.all().count()

    if downloaded_data_count > current_data_count:
        return True
    else:
        return False


def update_clia_test_type_table(clia_test_types, date_effective_within_days=None):
    """
    Creates a CLIATestType object if new, otherwise updates
    :param clia_test_types:
    :param date_effective_within_days Int: if not none, only update_or_create of test types with effective date
    within x days of today
    :return: Dict of update run debug statistics
    """
    run_status = {'MultipleObjectsReturned_count': 0,
                  'MultipleObjectsReturned_list': [],
                  'new_row_count': 0}

    if date_effective_within_days:
        clia_test_types = [x for x in clia_test_types
                           if x['DATE_EFFECTIVE'] >= datetime.date.today() - datetime.timedelta(days=date_effective_within_days)]

    for row in clia_test_types:
        try:
            test_type, created = models.CLIATestType.objects.update_or_create(
                document_number=row['DOCUMENT_NUMBER'],
                test_system_id=row['TEST_SYSTEM_ID'],
                analyte_id=row['ANALYTE_ID'],
                defaults={
                    'test_system_name': row['TEST_SYSTEM_NAME'],
                    'qualifier_1': row['QUALIFIER1'],
                    'qualifier_2': row['QUALIFIER2'],
                    'analyte_name': row['ANALYTE_NAME'],
                    'specialty_id': row['SPECIALTY_ID'],
                    'complexity': row['COMPLEXITY'],
                    'date_effective': make_aware(datetime.datetime.combine(row['DATE_EFFECTIVE'], datetime.time()))
                }
            )
            if created:
                logging.debug('New CLIA Test system added {} {}'.format(row['ANALYTE_NAME'],
                                                                        row['TEST_SYSTEM_NAME']))
                run_status['new_row_count'] += 1

        except models.CLIATestType.MultipleObjectsReturned:
            run_status['MultipleObjectsReturned_count'] += 1
            run_status['MultipleObjectsReturned_list'].append(row)

    return run_status


def fill_clia_test_type_specialty():
    """
    Clia test type specialty id lookup
    :return:
    """
    models.CLIATestTypeSpecialty.objects.update_or_create(specialty_id=1, specialty_name='Urinalysis')
    models.CLIATestTypeSpecialty.objects.update_or_create(specialty_id=2, specialty_name='General Chemistry')
    models.CLIATestTypeSpecialty.objects.update_or_create(specialty_id=3, specialty_name='General Immunology')
    models.CLIATestTypeSpecialty.objects.update_or_create(specialty_id=4, specialty_name='Hematology')
    models.CLIATestTypeSpecialty.objects.update_or_create(specialty_id=5, specialty_name='Immunohematology')
    models.CLIATestTypeSpecialty.objects.update_or_create(specialty_id=6, specialty_name='Endocrinology')
    models.CLIATestTypeSpecialty.objects.update_or_create(specialty_id=7, specialty_name='Toxicology/TDM')
    models.CLIATestTypeSpecialty.objects.update_or_create(specialty_id=8, specialty_name='Bacteriology')
    models.CLIATestTypeSpecialty.objects.update_or_create(specialty_id=9, specialty_name='Mycobacteriology')
    models.CLIATestTypeSpecialty.objects.update_or_create(specialty_id=10, specialty_name='Virology')
    models.CLIATestTypeSpecialty.objects.update_or_create(specialty_id=11, specialty_name='Parasitology')
    models.CLIATestTypeSpecialty.objects.update_or_create(specialty_id=12, specialty_name='Mycology')
    models.CLIATestTypeSpecialty.objects.update_or_create(specialty_id=13, specialty_name='Cytology')

    models.CLIATestTypeSpecialty.objects.update_or_create(specialty_id=14, specialty_name='Cytogenetics')
    models.CLIATestTypeSpecialty.objects.update_or_create(specialty_id=15, specialty_name='Histocompatibility')
    models.CLIATestTypeSpecialty.objects.update_or_create(specialty_id=16, specialty_name='Pathology')
    models.CLIATestTypeSpecialty.objects.update_or_create(specialty_id=17, specialty_name='Syphilis Serology')



if __name__ == '__main__':
    logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)
    main()
