# Generated by Django 2.2.2 on 2019-07-02 15:26

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('orders', '0035_auto_20190626_1339'),
    ]

    operations = [
        migrations.AddField(
            model_name='order',
            name='is_archived_by_provider',
            field=models.BooleanField(default=False, null=True),
        ),
    ]
