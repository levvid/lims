$(document).ready(function() {
  // Add datepicker and form-control class
  let collectionDate = $("input[id*='collection_date']");
  collectionDate.each(function() {
    new Pikaday({
      field: $(this)[0],
      format: 'MM-DD-YYYY',
      maxDate: moment().toDate()
    });
  });

  // When add sample is clicked, add date picker for last collection date input
  $('div.sample-form > .add-row').first().click(function() {
    let collectionDate = $("input[id*='collection_date']");
    new Pikaday({
      field: collectionDate.last()[0],
      format: 'MM-DD-YYYY',
      maxDate: moment().toDate()
    });
  });
});
