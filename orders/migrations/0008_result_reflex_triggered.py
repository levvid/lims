# Generated by Django 2.0.2 on 2019-04-01 19:19

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('orders', '0007_auto_20190401_1236'),
    ]

    operations = [
        migrations.AddField(
            model_name='result',
            name='reflex_triggered',
            field=models.BooleanField(default=False),
        ),
    ]
