from django.contrib import messages
from django.contrib.sites.shortcuts import get_current_site
from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.shortcuts import render, redirect, get_object_or_404
from django.template.loader import render_to_string
from django.views.generic import View
from django.urls import reverse
from django.conf import settings
from .email import send_email_sendgrid
from .tokens import account_activation_token
from . import forms

import asyncio
import tasks
import re
import requests


from lab import models as lab_models
from clients import models as clients_models


class LandingPageView(View):
    def get(self, request):
        form = forms.ScheduleDemoForm()

        # Iterating over messages required to clear storage, even if there is only a single message.
        storage = messages.get_messages(request)
        display_message = ''
        for message in storage:
            display_message = message
            break

        return render(request, 'index.html', {'form': form,
                                              'message': display_message,
                                              'site_key': settings.RECAPTCHA_SITE_KEY})

    def post(self, request):
        """
        Send an email if someone requests a demo
        """
        form = forms.ScheduleDemoForm(request.POST)
        redirect_url = reverse('landing_page')

        if form.is_valid():
            # reCAPTCHA v3 score handling and validation
            recaptcha_response = form.cleaned_data.get('recaptcha_response')
            payload = {"secret": settings.RECAPTCHA_SECRET_KEY, "response": recaptcha_response}
            recaptcha_verify = requests.post("https://www.google.com/recaptcha/api/siteverify", params=payload)
            recaptcha_json = recaptcha_verify.json()

            if not recaptcha_json.get('success'):
                messages.error(request, 'CAPTCHA verification token has expired. '
                                        'Please re-submit information.')
                return redirect(redirect_url)

            if recaptcha_json.get('score', 0) <= 0.5 or recaptcha_json.get('action', '') != 'validate_captcha':
                messages.error(request, 'CAPTCHA detected abnormal user activity. '
                                        'Please re-submit information.')
                return redirect(redirect_url)

            email = form.cleaned_data.get('email')
            name = form.cleaned_data.get('name')

            subject = 'New LIS Demo Request'

            content = 'A new person has signed up for a demo.<br><br>Name: {}<br>Email: {}'.format(name, email)

            send_email_sendgrid('support@dendisoftware.com',
                                'support@dendisoftware.com',
                                subject,
                                content)

            messages.success(request, 'Thank you for your interest. '
                                      'We will be in touch shortly.')
            return redirect(redirect_url)

        messages.error(request, 'Either CAPTCHA Token or required form fields are missing. '
                                'Please fill out the entire form.')
        return redirect(redirect_url)


class CreateTenantView(View):
    def get(self, request):
        form = forms.SignupForm()
        return render(request, 'public_index.html', {'form': form})

    def post(self, request):
        form = forms.SignupForm(request.POST)

        if form.is_valid():
            user_email = form.cleaned_data['email']

            unregistered_user = lab_models.UnregisteredUser.objects.create(
                email=user_email
            )
            current_site = get_current_site(request)
            subject = 'Activate your LIS'

            content = render_to_string('email_verification_email.html',
                                       {'user': unregistered_user,
                                        'domain': current_site.domain,
                                        'uuid': unregistered_user.uuid,
                                        'token': account_activation_token.make_token(unregistered_user)})

            send_email_sendgrid('support@dendisoftware.com',
                                user_email,
                                subject,
                                content)
            return redirect('email_verification_sent')


class PricingPageView(View):

    def get(self, request):
        return render(request, 'pricing.html', )


class ProductPageView(View):

    def get(self, request):
        return render(request, 'product.html', )


class CaseStudiesPageView(View):

    def get(self, request):
        return render(request, 'case_studies.html')


class CareersListPageView(View):

    def get(self, request):
        return render(request, 'careers_list.html')


class CareersDetailsPageView(View):
    """
    This page is not tested very thoroughly. For now, this page is only for show.
    """

    def get(self, request, job_name):

        # allowed slugs
        job_names = ['software-engineer', 'software-engineer-infrastructure', 'engagement-manager']

        if job_name in job_names:
            print(job_name)

            if job_name == 'software-engineer':
                job_title = 'Software Engineer'
                job_description = 'As a platform, Dendi is both a laboratory-facing and a developer-facing ' \
                                  'product. We enable new ' \
                                  'forms of automation and integration in the clinical laboratory industry, and ' \
                                  'the infrastructure we build will extend or replace our ' \
                                  'customer infrastructure to store, manage, & process data. ' \
                                  'As an engineer, you may build systems that form a ' \
                                  'data pipeline for experimental results, an event queue to trigger customer ' \
                                  'integrations, or a compute platform to deploy and scale custom analysis.'
                responsibility_1 = 'Building complex querying and visualization tools'
                responsibility_2 = 'Work directly with the founders and other engineers on product development'
                responsibility_3 = 'Work on the frontend and backend to develop new features from start to finish'
                responsibility_4 = 'Guide team in organizational best practices regarding data intensive systems'
            elif job_name == 'software-engineer-infrastructure':
                job_title = 'Software Engineer - Infrastructure'
                job_description = 'As a platform, Dendi is both a laboratory-facing and a developer-facing ' \
                                  'product. We enable new ' \
                                  'forms of automation and integration in the clinical laboratory industry, and ' \
                                  'the infrastructure we build will extend or replace our ' \
                                  'customer infrastructure to store, manage, & process data. ' \
                                  'As an engineer, you may build systems that form a ' \
                                  'data pipeline for experimental results, an event queue to trigger customer ' \
                                  'integrations, or a compute platform to deploy and scale custom analysis.'
                responsibility_1 = 'Design, build, and maintain core infrastructure hosted on AWS'
                responsibility_2 = 'Build developer-facing systems to test, build, and deploy'
                responsibility_3 = 'Define and implement our infrastructural security posture'
                responsibility_4 = 'Develop monitoring/observability and automation as part of a lean operations function'

            else:
                job_title = 'Engagement Manager'
                job_description = 'Dendi is currently building a world-class team to drive ' \
                                  'commercialization of our platform. You will be supporting a product ' \
                                  'that transforms how clinical laboratories test human samples. Core responsibilities ' \
                                  'of this role revolve around building a strong ' \
                                  'relationship with the customer and developing a deep understanding of their ' \
                                  'laboratory testing process. ' \
                                  'You will leverage this to drive implementation, onboarding, ' \
                                  'and adoption of the Dendi platform. '
                responsibility_1 = 'Act as a trusted advisor for new customers by guiding them through implementation and adoption'
                responsibility_2 = 'Understand customer workflows and figure out how they can leverage Dendi to best meet their goals'
                responsibility_3 = 'Identify and work with sales on opportunities for account expansion'
                responsibility_4 = 'Map the customer journey, develop listening points, and standardize interactions for each point in journey'

            return render(request, 'careers_details.html', {'job_title': job_title,
                                                            'job_description': job_description,
                                                            'responsibility_1': responsibility_1,
                                                            'responsibility_2': responsibility_2,
                                                            'responsibility_3': responsibility_3,
                                                            'responsibility_4': responsibility_4})
        else:
            raise Http404('Job does not exist.')


class EmailVerificationSent(View):
    def get(self, request):
        context = {}
        return render(request, 'email_verification_sent.html', context)


class ConfirmEmailView(View):
    def get(self, request, unregistered_user_uuid, token):
        try:
            uuid = unregistered_user_uuid
            unregistered_user = lab_models.UnregisteredUser.objects.get(uuid=uuid)
        except (TypeError, ValueError, OverflowError, lab_models.UnregisteredUser.DoesNotExist):
            unregistered_user = None
        if unregistered_user is not None and account_activation_token.check_token(unregistered_user, token):
            unregistered_user.email_confirmed = True
            unregistered_user.save()
            return redirect('email_confirmed', unregistered_user_uuid=unregistered_user.uuid)
        else:
            return render(request, 'email_verification_invalid.html')


class EmailConfirmedView(View):
    def get(self, request, unregistered_user_uuid):
        # redirect to create account page
        return redirect('client_create', unregistered_user_uuid=unregistered_user_uuid)


class ClientCreateView(View):
    def get(self, request, unregistered_user_uuid):
        unregistered_user = get_object_or_404(lab_models.UnregisteredUser, uuid=unregistered_user_uuid)

        form = forms.ClientCreateForm(initial={'email': unregistered_user.email})
        context = {'form': form}

        return render(request, 'client_create.html', context)

    def post(self, request, unregistered_user_uuid):

        form = forms.ClientCreateForm(request.POST)
        context = {'form': form}

        if form.is_valid():
            client_name = form.cleaned_data['client_name']

            unique_schema_found = False
            counter = 0
            pattern = re.compile('[\W_]+')
            schema_name = pattern.sub('', client_name.lower())
            while not unique_schema_found:
                if counter > 0:
                    if clients_models.Client.objects.filter(schema_name=schema_name + str(counter)):
                        counter += 1
                    else:
                        schema_name = schema_name + str(counter)
                        unique_schema_found = True
                else:
                    if clients_models.Client.objects.filter(schema_name=schema_name):
                        counter += 1
                    else:
                        unique_schema_found = True
            lab_type = form.cleaned_data['lab_type']

            request.session['client_name'] = client_name
            request.session['schema_name'] = schema_name
            request.session['lab_type'] = lab_type

            return redirect(reverse('client_create_2', kwargs={'unregistered_user_uuid': unregistered_user_uuid}))
        else:
            return render(request, 'client_create.html', context)


class ClientCreate2View(View):
    # second page of the create client view
    def get(self, request, unregistered_user_uuid):
        form = forms.ClientCreate2Form()

        return render(request, 'client_create_2.html', {'form': form})

    def post(self, request, unregistered_user_uuid):
        unregistered_user = get_object_or_404(lab_models.UnregisteredUser, uuid=unregistered_user_uuid)
        form = forms.ClientCreate2Form(request.POST)

        if form.is_valid():
            request.session['email'] = unregistered_user.email
            request.session['first_name'] = form.cleaned_data['first_name']
            request.session['last_name'] = form.cleaned_data['last_name']
            request.session['new_password'] = form.cleaned_data['confirm_new_password']

            return redirect(reverse('client_create_3', kwargs={'unregistered_user_uuid': unregistered_user_uuid}))
        else:
            return render(request, 'client_create_2.html', {'form': form})


class ClientCreate3View(View):
    # second page of the create client view
    def get(self, request, unregistered_user_uuid):
        return render(request, 'client_create_3.html')

    def post(self, request, unregistered_user_uuid):
        unregistered_user = get_object_or_404(lab_models.UnregisteredUser, uuid=unregistered_user_uuid)

        task_context = {'client_name': request.session['client_name'],
                        'schema_name': request.session['schema_name'],
                        'email': request.session['email'],
                        'first_name': request.session['first_name'],
                        'last_name': request.session['last_name'],
                        'password': request.session['new_password'],
                        'lab_type': request.session['lab_type']}
        # create the account
        loop = asyncio.new_event_loop()
        asyncio.set_event_loop(loop)
        loop.run_in_executor(None, tasks.initialize_client, task_context)

        return redirect(reverse('account_pending'))


class AccountPendingView(View):
    def get(self, request):
        context = {}
        return render(request, 'account_pending.html', context)


class LoginView(View):
    """
    Direct clients to their subdomain's login page
    """

    def get(self, request):

        form = forms.LoginForm()
        context = {'form': form,
                   'login_page': True}

        return render(request, 'public_login.html', context)

    def post(self, request):
        form = forms.LoginForm(request.POST)

        if form.is_valid():
            subdomain = form.cleaned_data['url']
            domain = request.META['HTTP_HOST']
            if 'www.' in domain:
                domain = domain.replace('www.', '')

            return HttpResponseRedirect('http://{}.{}/login'.format(subdomain,
                                                                    domain))
        else:
            return render(request, 'public_login.html', {'form': form,
                                                         'login_page': True})
