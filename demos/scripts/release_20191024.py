import django
import os

from tenant_schemas.utils import tenant_context

os.environ['DJANGO_SETTINGS_MODULE'] = 'main.settings'
django.setup()

from accounts import models as accounts_models
from clients import models as client_models
from lab import models as lab_models
from lab_tenant import models as lab_tenant_models
from orders import models as orders_models


def main():
    """
    run before deploy to production
    :return:
    """
    convert_test_panel_exclusive_boolean()


def convert_test_panel_exclusive_boolean():
    """
    If a TestPanelType has boolean exclude_from_menu=True, copy the value to is_exclusive
    :return:
    """
    tenants = client_models.Client.objects.exclude(schema_name='public').all()
    for tenant in tenants:
        with tenant_context(tenant):

            test_panels = orders_models.TestPanelType.objects.all()

            for test_panel in test_panels:
                if test_panel.exclude_from_menu:
                    test_panel.is_exclusive = True
                    print(test_panel)
                    test_panel.save()


if __name__ == '__main__':
    main()
