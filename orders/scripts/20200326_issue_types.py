import os
import django
import logging
import sys

os.environ['DJANGO_SETTINGS_MODULE'] = 'main.settings'
django.setup()

from django.db import transaction
from tenant_schemas.utils import tenant_context

from clients import models as clients_models
from orders import models


ISSUE_TYPES = ['No Provider',
               'No Provider Signature',
               'No Account Information',
               'No Diagnosis Code(s)',
               'No Patient Signature',
               'Patient Demographics',
               'No Date of Service',
               'Invalid Medication',
               'No Test Selection Marked',
               '1 1D Hold',
               'Unapproved Client Bill Order',
               'No Requisition Form',
               'Illegible Requisition Form',
               'Incomplete BCBS Addendum',
               'Incomplete Patient Consent Form',
               'Incomplete Provider Necessity Form',
               'Insurance',
               'HIPS Specimen Rejection'
               ]


@transaction.atomic
def main():
    """

    Add order issue types to tenant schemas
    :return:
    """
    tenants = clients_models.Client.objects.exclude(schema_name='public').all()

    for tenant in tenants:
        logging.debug("Creating issue_types for schema - {}".format(tenant.schema_name))
        with tenant_context(tenant):
            for issue_type in ISSUE_TYPES:
                print('TYPE: {} '.format(issue_type))
                new_issue_type = models.OrderIssueType(issue_type_name=issue_type)
                new_issue_type.save()


if __name__ == '__main__':
    logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)
    main()
