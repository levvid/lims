import django
import logging
import os
import sys

from django.db import transaction
from tenant_schemas.utils import tenant_context

os.environ['DJANGO_SETTINGS_MODULE'] = 'main.settings'
django.setup()

from clients import models as client_models
from .. import models
"""
Script to migrate production data from having is_approved boolean in Order object to Test object
"""


@transaction.atomic
def main():
    tenants = client_models.Client.objects.exclude(schema_name='public')

    for tenant in tenants:
        with tenant_context(tenant):
            print(tenant)
            orders = models.Order.objects.all().prefetch_related('results', 'results__test')
            for order in orders:
                if order.reported:
                    results = order.results.all()
                    completed_results = order.results.filter(result__isnull=False)
                    if results.count() == completed_results.count():
                        logging.info("setting {} partial to false".format(order))
                        order.partial = False
                        order.save()

                    logging.info("setting is_approved and approved_date for {} tests".format(completed_results.count()))
                    for result in completed_results:
                        test = result.test
                        test.is_approved = True
                        test.approved_date = order.report_date
                        test.save()


if __name__ == '__main__':
    logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)
    main()
