# Generated by Django 2.2.2 on 2019-07-22 16:10

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('orders', '0050_result_cutoff'),
    ]

    operations = [
        migrations.AddField(
            model_name='datain',
            name='in_queue',
            field=models.BooleanField(default=False),
        ),
    ]
