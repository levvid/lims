import django
import logging
import os
import sys

from django.db import transaction
from tenant_schemas.utils import tenant_context

os.environ['DJANGO_SETTINGS_MODULE'] = 'main.settings'
django.setup()

from clients import models as client_models
from .. import models


@transaction.atomic
def main():
    """
    Script that copies over the data in required_clia_samples and writes it into required_samples
    in TestTypes for each tenant
    :return:
    """

    tenants = client_models.Client.objects.exclude(schema_name='public')

    for tenant in tenants:
        with tenant_context(tenant):

            test_type_list = models.TestType.objects.all()

            for test_type in test_type_list:
                test_type.required_samples = test_type.required_clia_samples
                test_type.save()
                logging.debug("Tenant: {} - copying over required_clia_samples to required samples for {}"
                              .format(tenant, test_type.name))


if __name__ == '__main__':
    logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)
    main()
