function receive_orders(is_order, order_code, sample_barcode) {
    let url = '';

    if (is_order) {
        url += order_code + '/';
    } else {
        url += sample_barcode + '/';
    }
    $.ajax({
        headers: {'X-CSRFToken': getCookie('csrftoken')},
        type: "POST",
        url: url,
        success: function (result) {
            sessionStorage.setItem("reloaded", "true");
            setTimeout(function () {
                location.reload(true);
            }, 100);  // wait for .1 seconds
        },
        error: function (err) {
            // reject(err); // Reject the promise and go to catch()
        }
    });
}

function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

$('.order-comment').click(function (e) {
    let comment = document.getElementsByName("comment")[0].value;  // there should only be one field with name comment in accessioning template

    if (comment.trim() == '') {
        // prevent page from redirecting via <a href>
        alert('Comment cannot be blank. Please try again.')
        e.preventDefault();
    }
});