from django.conf.urls import url

from . import consumers

websocket_urlpatterns = [
    url(r'^ws/data_in/(?P<data_in_uuid>[^/]+)/import/$', consumers.DataInImportConsumer),
    url(r'^ws/data_in/(?P<data_in_uuid>[^/]+)/$', consumers.DataInStatusConsumer),
    url(r'^ws/order/(?P<order_uuid>[^/]+)/$', consumers.OrderStatusConsumer),
    url(r'^ws/instrument_integration/(?P<instrument_integration_uuid>[^/]+)/$',
        consumers.InstrumentIntegrationStatusConsumer),

]
