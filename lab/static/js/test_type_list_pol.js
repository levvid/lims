function createTable(tableID, tableSettings) {
  const hotElement = document.querySelector(tableID);
  state.Table.table = new Handsontable(hotElement, tableSettings);
}

function updateTable() {
  const newTableData = filterTable(
      state.Table.data,
  );

  // load new data without replacing old table
    state.Table.table.getPlugin('filters').clearConditions();
    state.Table.table.loadData(newTableData);

    updateTableCount(newTableData, state.Table.countHtmlID);
}

function filterTable(tableData){
  return tableData.filter(element => {
    // iterate over table elements
    // for every column in the table, if the key is also an object, add a toString method
    // that allows for conversion to text so it doesn't write out [object Object] on table export
    for(let key in element) {
      if(element[key]) {
        element[key].toString = () => element[key].text;
      }
    }
    return element;
  })
}

function updateTableCount(data, htmlID) {
  const rowCount = data.length;
  j(htmlID).html(rowCount.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ','));
}
